from __future__ import print_function
from pyutils.patterns.iters import *



class Pass(object):
	''' Pass is used to defer calling a function or class.
		Initialize Pass with a callable func and its parameters.
	  	use Pass.call() to execute callable with given parameters.
	  	Similar in many ways to lambda; nestable; captures initializer args (pass-by-value).

		Usage:
		deferred_set = Pass(set,['a','b','c','a'])
		deferred_strfmt = Pass(lambda a,b,c: '%s %s %s'%(a,b,c),'d','e','f')
		deferred_httpget = Pass(requests.get, 'https://intentionallyblank.website/')
		pp(deferred_set.call())				# result: {'c', 'b', 'a'}
		pp(deferred_strfmt.call())			# result: 'd e f'
		pp(deferred_httpget.call().text)	# result: ('<!DOCTYPE html>\n'
											#  '<html>\n'
											#  '<head>\n'
											#  '\t<title>This website intentionally left blank</title>\n'
											#  ...
											# )
	'''
	def __init__(self, type, *args, **kwargs):
		self.__dict__.update({k:v for k,v in locals().items() if k != 'self'})
		self.current = -1

	def call(self):
		result = self.type(*self.args, **self.kwargs)
		return result

	def get(self, data):
		args = self.args and Struct.infuse_with_struct(data, self.args) or self.args
		kwargs = self.kwargs and Struct.infuse_with_struct(data, self.kwargs) or self.kwargs
		result = self.type(*args, **kwargs)
		return result


class Data:
	def __init__(self, value, iteration_indices):
		self.value, self.iteration_indices = value, iteration_indices

class Struct:
	''' Class to construct new data structure infused with fields taken from a data source
		Usage examples:
		data = {0:{0:0,1:1,2:2}, 1:{0:3,1:4,2:5}, 2:{0:6,1:7,2:8}, 3:10}
		keys0 = [3, K(1,2), K(2,2), K(2,0), K(0,1), [0], 0]
		keys1 = [3, K[1][2], (2,2), K(2,0), K(0,1), [0], 0]
		keys2 = {'trades':K[0][1], 'allowance':3, 'cost':2}
		pp(Struct(keys0).infuse(data))	# [3, 5, 8, 6, 1, [0], 0]
		pp(Struct(keys1).infuse(data))	# [3, 5, (2, 2), 6, 1, [0], 0]
		pp(Struct(keys2).infuse(data))	# {'allowance': 3, 'cost': 2, 'trades': 1}
	'''
	def __init__(self, iteration_keys, struct):
		self.struct = struct
		self.iteration_keys = iteration_keys

	def infuse(self, data):
		data = Data(value=data, iteration_indices=[0]*len(self.iteration_keys))
		result = self.infuse_iterable(data, self.iteration_keys)
		return result

	def infuse_iterable(self, data, iteration_keys=[]):
		result = []

		if not iteration_keys:
			result = self.infuse_with_struct(data, self.struct)

		else:
			iterable_data = iteration_keys[0].get(data)
			depth = len(iteration_keys)-len(data.iteration_indices)
			data.iteration_indices[depth] = 0
			data = Data(value=None, iteration_indices=[0]*len(self.iteration_keys))
			for item in iterable_data:
				data.value = item
				result.append(self.infuse_iterable(data, iteration_keys[1:]))
				data.iteration_indices[depth] += 1

		return result

	@staticmethod
	def infuse_with_struct(data, struct):
		''' Return struct with Keys replaced with '''
		if isinstance(struct, (Keys, Pass)):
			result = struct.get(data)
			return result

		# Handle specific subset of iterable structures recursively; otherwise return struct as is
		result_type = type(struct)
		# result = result_type()
		if isinstance(struct, (tuple, list)):
			content = []
			for item in struct:
				content += [Struct.infuse_with_struct(data, item)]
			result = result_type(content)
		elif isinstance(struct, Pass):
			result = Struct.infuse_with_struct(data, struct)
		elif isinstance(struct, dict):
			result = result_type()
			for key_n,value_n in struct.items():
				result[Struct.infuse_with_struct(data, key_n)] = Struct.infuse_with_struct(data, value_n)
		else:
			result = struct

		return result


if __name__=='__main__':
	from pprint import pprint as pp
	from collections import namedtuple


	### test setup
	Ratings = namedtuple('Ratings', 'strength intellect speed wealth')
	Person = namedtuple('Person', 'name alias friend ratings')

	clark = Person({'first':'clark','last':'kent'}, ['superman', 'man of steel', 'son of krypton', 'smallville'], None, Ratings(8.5, 6., 5.5, 3.))
	bruce = Person({'first':'bruce','last':'wayne'}, ['batman', 'dark knight', 'bats', 'badman'], [clark], Ratings(4., 7., 3., 9))


	### tested and working: Iters()
	special_iter = Iters([bruce, clark]*2)
	classname = lambda obj: type(obj).__name__
	pp(' '.join(map(classname, special_iter)))	# => 'Person Person Person Person'
	pp(list(special_iter.alias[0]))				# => ['batman', 'superman', 'batman', 'superman']
	pp(list(special_iter.ratings.wealth))		# => [9, 3.0, 9, 3.0]

	# ### work in progress: Iters() handles generator content by returning a generator not a collection iterator
	# class LogAccess(list):
	# 	def __getitem__(self, item):
	# 		value = list.__getitem__(self, item)
	# 		print('Got index %s with value %s.' % (item, value))
	# 		return value
	# # index_grid = [LogAccess([LogAccess(range(y,y+3)) for y in range(0,9,3)])]
	# index_grid = LogAccess([bruce]*5)
	# # pp(index_grid)
	# index_grid_iter = Iter(index_grid).name['last']
	# # [index_grid_iter[1]
	# print(next(index_grid_iter))
	# print(next(index_grid_iter))
	# print(next(index_grid_iter))
	# index_grid_gen = Iter((x for x in index_grid)).name['last']
	# print(next(index_grid_gen))
	# print(next(index_grid_gen))
	# print(next(index_grid_gen))
	# # list(index_grid_gen)
	# exit()


	### tested and working: Keys()
	k0 = K.name['first']
	k1 = K.alias[0]
	k2 = K.friend[0].ratings.wealth
	print(k0)				# => Keys(['name', 'first'])
	print(k1)				# => Keys(['alias', 0])
	print(k2)				# => Keys(['friend', 0, 'ratings', 'wealth'])

	# examples of invoking keys accessors on nested data structures
	print(k0.get(clark))	# => 'clark'
	print(k0.get(bruce))	# => 'bruce'
	print(k1.get(bruce))	# => 'batman'
	print(k2.get(bruce))	# => 3.0



