
class IRepr(object):
	def default_is_repr(self, obj):
		try:
			result = self.from_repr(obj)
			return not result is None
		except TypeError:
			return False
	def init(self, from_repr, is_repr=default_is_repr):
		''' constructor '''
		self.from_repr = from_repr
		self.is_repr = is_repr
	@staticmethod
	def is_repr(obj):
		''' return True if given obj can be converted to subclass, otherwise return False.
		 	intended to be overridden by subclass
		'''
		return False
	@staticmethod
	def from_repr(obj):
		''' return representaion of subclass type given an obj.
			In cases where object cannot be converted default behavior should be to raise a TypeError.
			intended to be overridden by subclass
		'''
		raise TypeError('todo')
	@staticmethod
	def select_irepr(ireprs, obj, irepr_subclass_types=list()):
		''' Return first available item in facades able to interpret obj repr '''
		for irepr in ireprs:
			for irepr_sublclass_type in [IRepr]+irepr_subclass_types:
				if not issubclass(irepr, irepr_sublclass_type) and not isinstance(irepr, irepr_sublclass_type):
					break		# this irepr does not satisfy all required irepr_subclass types; try next available irepr
			else:  # did not break so irepr matches all types
				if irepr.is_repr(obj):
					return irepr	# return 1st irepr that can interpret the obj repr; otherwise try next available irepr
				# continue		# this irepr does not satisfy all required irepr_subclass types; try next available irepr



		return None				# Failed to find obj repr interpreter in irepr list