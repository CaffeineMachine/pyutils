import re
from collections import OrderedDict as odict
from pprint import pprint as pp
from pyutils.extend.datastructures import XDict



class Regex(object):
	'''  '''
	def __init__(self, pattern):
		''' constructor '''
		self.__dict__.update(XDict(locals()) ^ ['self'])

	def iterative(self, string):
		''' '''
		subpatterns = []
		for i in range(len(self.pattern)):
			subpattern = self.pattern[:i+1]
			try:
				result = re.compile(subpattern)
				subpatterns += [subpattern]
			except: pass

		iterations = odict()
		for pattern in subpatterns:
			iterations[pattern] = ''
			for item in re.finditer(pattern, string):
				iterations[pattern] += '%s|' % item.group()

		return iterations

	# @staticmethod
	# def ___(self, ___):
	# 	'''  '''
	# 	result = None
	# 	return result

if __name__ == '__main__':
	#
	# regex = Regex(r'--\S+ ("([^"])*"|'+"'([^'])*'|\S*)|\S+")
	regex = Regex(r'"([^"])*"|'+"'([^'])*'|\S+")
	cmd = '/home/zeroone/workspacehost/checkout/module.py --start "2017-11-16 03:08:01.420547" --interval 10 --stop \'0:02:00\''
	pp(regex.iterative(cmd).items(), width=200)
	pattern = re.compile(r'"([^"])*"|' + "'([^'])*'|\S+")
	pattern = re.compile(r'"([^"])*"')
	result = pattern.search(cmd)
	pp(pattern.search(cmd))
