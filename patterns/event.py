
''' basic observer patterns using set as the collection of listeners '''
# from __future__ import print_function

from collections import defaultdict as ddict
from threading import RLock
from dataclasses import dataclass

# from pyutils.defs.alldefs import map0, infuse
# from pyutils.alldefs. import map0, infuse
from pyutils.defs.colldefs import map0, set0
from pyutils.defs.itemdefs import infuse
from pyutils.defs.typedefs import miss, iscall
from pyutils.utilities.sequtils import splitargs
from pyutils.time.thread import atomic
from pyutils.structures.maps import SetMap

#region event utils
def is_pre_event_handler_cond(publisher, handler):
	'''
		Usage:
		class SimplePreEventHandler:
			is_pre_event_handler = True
		class AdvPreEventHandler:
			is_pre_event_handler = [PreEventPubl_A(), PreEventPubl_B()].__contains__
	'''
	is_pre_event_handler = getattr(handler, 'is_pre_event_handler', False)
	if is_pre_event_handler not in (True, False):
		is_pre_event_handler = is_pre_event_handler(publisher)
	return is_pre_event_handler

def is_on_event_handler_cond(publisher, handler):
	'''
		Usage:
		class SimpleOnEventHandler:
			is_on_event_handler = True
		class AdvOnEventHandler:
			is_on_event_handler = [OnEventPubl_A(), OnEventPubl_B()].__contains__
	'''
	is_on_event_handler = getattr(handler, 'is_on_event_handler', False)
	if is_on_event_handler not in (True, False):
		is_on_event_handler = is_on_event_handler(publisher)
	return is_on_event_handler

def is_post_event_handler_cond(publisher, handler):
	'''
		Usage:
		class SimplePostEventHandler:
			is_post_event_handler = True
		class AdvPostEventHandler:
			is_post_event_handler = [PostEventPubl_A(), PostEventPubl_B()].__contains__
	'''
	is_post_event_handler = getattr(handler, 'is_post_event_handler', False)
	if is_post_event_handler not in (True, False):
		is_post_event_handler = is_post_event_handler(publisher)
	return is_post_event_handler
#endregion

#region event devcs
class PublisherIfx:
	#region utils
	def enhook(self, handler):									raise NotImplemented()
	def unhook(self, handler):									raise NotImplemented()
	def notify(self, *pa, cond=None, _augment=True,	 **ka):		raise NotImplemented()
	__iadd__ = hook			= enhook
	#endregion
	...
class Hooks:
	''' Elementary container of callback hooks to receive notify(...) or emit(...) '''
	#region defs
	_fab_hooks				= set
	hooks					= property(lambda self: self._hooks)
	#endregion
	#region utils
	def __init_subclass__(cls, fab_hooks=_fab_hooks,**ka):
		infuse(cls,ka,_fab_hooks=fab_hooks)
	def __init__(self, hooks=(), *pa, **ka):
		self._hooks = set0
		hooks and self.enhook(hooks)
		super().__init__(*pa,**ka)
	def enhook(self, *_hooks, hooks=()):
		''' enhook a callable handler to receive callbacks on notify '''
		hooks = hooks or _hooks
		if hooks and self._hooks is set0:
			self._hooks = self._fab_hooks()
		for hook in hooks:
			self._hooks.add(hook)
	def unhook(self, *_hooks, hooks=()):
		''' unhook handler to stop receiving callbacks '''
		for handler in (hooks or _hooks):
			self._hooks.discard(handler)
	def enhook_to(self, publ, *pa, **ka):		publ.enhook(self, *pa, **ka)
	def unhook_from(self, publ):				publ.unhook(self)
	
	def notify(self, *pa, cond=None, **ka):
		''' broadcast action or event to registered handlers
			:param cond: condition which determines if the handlers receive notifications
			:param pa: position args
			:param ka: keyword args
		'''
		for hook in self._hooks:
			if not cond or cond(self, hook):  # consider allowing condition to operate on args
				hook(*pa, **ka)
	def emit(self, *pa, **ka):
		''' used when self is the event notification; needs no parameters '''
		self.notify(self, *pa, **ka)
	
	hook_to					= enhook_to
	hook					= enhook
	#endregion
	...
class Publisher:
	''' Extended container of callback hooks to receive notify(...) or emit(...) with extended features '''
	#region defs
	# todo: integrate Hooks elements as base class
	_to_cb,_to_ind,_cond	= None, None, None
	_handlers				= ()
	_handlers_fab=_hook_fab	= staticmethod(lambda *pa, **ka: SetMap(*pa, to_key=id, **ka))  # default set/map hashed by id
	handlers				= property(lambda self: iter(self._handlers and self._handlers.values() or ()))
	hooks = _hooks			= property(lambda self: self._handlers)
	#endregion
	#region utils
	def __init__(self, handlers=(), *pa, to_cb=None, **ka):
		handlers = () if handlers == () else tuple(handlers)
		self._handlers = self._handlers_fab if handlers else ()	# if not empty coerce to SetMap, else keep None
		self.pal,self.par = splitargs(pa)		# todo: eliminate; perhaps use Args instead
		self.ka = ka or map0					# todo: eliminate; perhaps use Args instead
		to_cb and setattr(self, '_to_cb', to_cb or self._to_cb)
	def enhook(self, *_handlers,handlers=()):
		''' enhook a callable handler to receive callbacks on notify '''
		handlers = handlers or _handlers
		if handlers and self._handlers == ():
			self._handlers = self._handlers_fab()
		for handler in handlers:
			self._handlers.add(handler)
	def unhook(self, *_handlers,handlers=()):
		''' unhook handler to stop receiving callbacks '''
		handlers = handlers or _handlers
		for handler in handlers:
			self._handlers.discard(handler)
	def enhook_to(self, publ, *pa, **ka):
		publ.hook(self, *pa, **ka)
	def unhook_from(self, publ):
		publ.unhook(self)
	
	def notify(self, *pa, cond=None, _augment=True, **ka):
		''' broadcast action or event to registered handlers
			:param cond: condition which determines if the handlers receive notifications
			:param _augment: when true, augment args with internal publisher defaults; otherwise, do not modify args
			:param pa: position args
			:param ka: keyword args
		'''
		if _augment:
			pa,ka =  (*self.pal,*pa,*self.par), dict(self.ka,**ka)
		if cond and self._cond:
			cond = self.simplify_conditions(self._cond, cond)
			
		for handler in self.handlers:
			if not cond or cond(self, handler):  # consider allowing condition to operate on args
				callback = self._get_callback(handler, self._to_cb) if self._to_cb else handler
				callback(*pa, **ka)
	def emit(self, *pa, **ka):
		''' used when self is the event notification; needs no parameters '''
		self.notify(self, *pa, **ka)
	def notify_pre_event(self, event, *pa, **ka):
		''' notify only handlers which explicitly accept pre-event given this publisher '''
		event.event_phase = 'post_event'
		self.notify(event, *pa, cond=is_pre_event_handler_cond, **ka)
	def notify_post_event(self, event, *pa, **ka):
		''' notify only handlers which explicitly accept post-event given this publisher '''
		event.event_phase = 'post_event'
		self.notify(event, *pa, cond=is_post_event_handler_cond, **ka)
	def notify_on_event(self, event, *pa, **ka):
		''' notify only handlers which explicitly accept on-event given this publisher
			Note: not the same as notify which by default will not check handlers pre/on/post event policy
		'''
		event.event_phase = 'post_event'
		self.notify(event, *pa, cond=is_on_event_handler_cond, **ka)
	def simplify_conditions(self, *conditions):
		return conditions[0]  # todo
	
	def sequencer(self):
		return EventSeq(self)
	@staticmethod
	def _get_callback(handler, to_cb):
		callback = (
			getattr(handler,to_cb)	if isinstance(to_cb, str) else
			to_cb(handler)			if to_cb else
			handler  )
		return callback
	
	hook_to					= enhook_to
	hook					= enhook
	__iadd__				= enhook
	__isub__				= unhook
	__call__				= notify
	#endregion
	...

class EventPublisher(Publisher):
	#region defs
	event_phase				= property(
								lambda self: getattr(self, '_event_phase', 'on_event'),
								lambda self, phase: setattr(self, '_event_phase', phase), )
	#endregion
	...

# class Synced(Publisher):
class Synced(Hooks):
	# todo: rename to desynced
	#region defs
	dsyncd					= property(lambda self: not self._syncd)
	syncd					= property(lambda self:     self._syncd)
	#endregion
	#region utils
	def __init__(self,*pa,src=None,**ka):
		self.src = src
		self._syncd = True
		self._deltas = []
		super().__init__(*pa,**ka)
	def synced(self):		return     self._syncd
	def desynced(self):		return not self._syncd
	def unsync(self,delta=miss,deltas=miss):
		self._syncd = False
		delta is not miss and self._deltas.append(delta)
		deltas is not miss and self._deltas.extend(deltas)
		return True
	def resync(self,**ka):
		out = self._deltas or True  # always resolves to True
		if not self._syncd:
			# todo: get latest from source
			self.notify(**ka)
			self._syncd = True
			self._deltas = []
		return out
	def consume(self,**ka):
		syncd,deltas = self._syncd,self._deltas
		self.resync(**ka)
		return syncd,deltas
	__bool__				= desynced
	__neg__,__pos__			= resync,unsync
	#endregion
	...

@dataclass
class AdaptHandler:
	# todo: implement
	handle: object
	to_cb: iscall			= None
	to_ind: iscall			= None
	cb: iscall				= None
	ind: iscall				= None
@dataclass
class AdaptPublisher(Publisher):
	''' Like Publisher, used as container for callbacks to be invoked by notify(...) or emit(...).
		Improves on Publisher with ordered callbacks and dynamic callbacks.
		conforming any object handler to convention of having a callback and position index.
		See AdaptHandler capsule class above.
	'''
	# todo: implement
	to_cb: iscall			= None
	to_ind: iscall			= None

class EventSeq:
	''' An event sequence. Can also collect then commit events in batch as context manager for event Publishers. '''
	#region utils
	def __init__(self, events=None, publisher=None, active=False):
		self.events = events or []
		self.publisher = publisher
		if active and publisher:
			self.collect(publisher)
	def collect(self):
		''' enter event collection mode '''
		self.publisher.hook(self)
	def condition(self, handler, *pa, **ka):
		''' while collecting event sequence, do not allow other handlers to receive events '''
		return handler is self
	def add(self, *pa, **ka):
		self.events.append([pa, ka or None])
	def commit(self, exc_type=None, exc_val=None, exc_tb=None, *, publisher=None):
		''' exit event collection mode and issue events; commit() is reentrant if alternative publisher is given
			:param publisher: publisher to which collected events will be issued
		'''
		# exception occurred during 'with EventSeq():' context
		if exc_val:	raise exc_val
		
		# publish collected events to given publisher
		publisher = publisher or self.publisher
		for pa, ka in self.events:
			publisher.notify(*pa, _augment=False, **ka or {})
			
		# end event collection on self.publisher
		if self.publisher and publisher is self.publisher:
			self.publisher.unhook(self)
			self.publisher = None
			self.events = []
	
	__enter__				= collect
	__exit__				= commit
	__call__ = on_event		= add
	#endregion
	...

''' The following Handler classes show the convention for patterns involving
	a Handler of a Publisher and do not require subclassing.
'''
class Handler:
	# todo: not true. handlers need only be callable. refactor class to have qualifier like EventHandler
	#region utils
	def on_event(self, event, **ka): raise NotImplemented()
	#endregion
	...

class PreEventHandler(Handler):		is_pre_event_handler = True
class OnEventHandler(Handler):		is_on_event_handler = True
class PostEventHandler(Handler):	is_post_event_handler = True

Publ						= Publisher
EventPubl					= EventPublisher
#endregion

#region advanced event devcs
class StateUpdate:
	''' container for single state change handler '''
	# obsolete: use pyutils.Model
	def __init__(self, handler, state_updated=False):
		self.handler = handler
		self.state_updated = state_updated
	def handle(self):
		''' handle if state updated '''
		if self.state_updated:
			self.handler()
		self.state_updated = False
	def updated(self, state_updated=True):
		''' set state_updated to be handled later '''
		self.state_updated = state_updated
	def update(self):
		''' update state and handle immediately '''
		self.handler()
		self.state_updated = False

class StateUpdateCustom(StateUpdate):
	''' Container for single parameterized state change handler '''
	def __init__(self, handler, *update_args, state_updated=False, **update_kws):
		self.handler = handler
		self.state_updated = state_updated
		self.update_params = update_args, update_kws
	def handle(self):
		''' handle if state updated '''
		if self.state_updated:
			self.handler(*self.update_params[0], **self.update_params[1])
		self.state_updated = False
	def update(self):
		''' update state and handle immediately '''
		self.handler(*self.update_params[0], **self.update_params[1])
		self.state_updated = False
	def updated_params(self, *update_args, state_updated=True, **update_kws):
		''' set state_updated and update parames to be handled later '''
		self.state_updated = state_updated
		self.update_params = update_args, update_kws
	def update_params(self, *update_args, **update_kws):
		''' update state with given params and handle immediately '''
		self.update_params = update_args, update_kws
		self.handler(*self.update_params[0], **self.update_params[1])
		self.state_updated = False

class CondHandler:
	''' container defining the callback to an event and which events are applicable '''
	def __init__(self, callback=None, bound_types=None, condition=None, until=None):
		''' construct '''
		self.callback = callback or self.on_event
		bound_types = bound_types or [type(None)]  # null-coalesce to value for accepting all types
		bound_types = isinstance(bound_types, type) and [bound_types] or bound_types  # change single to iter
		self.bound_types = [(isinstance(t, type) and t or type(t)) for t in bound_types]
		self.condition = condition
		self.until = until
	# override these callbacks in subclasses or provide callback as constructor parameters
	def on_event(self, event_data=None, *args, **kws):
		''' invoke configured callback '''
		self.callback(event_data, *args, **kws)
	def _condition(self, event_data, as_type=None, *args, **kws):	raise NotImplemented()
	def _until(self, event_data, as_type=None, *args, **kws):		raise NotImplemented()
	# method aliases
	__call__ = on_event

class CondPublisher:
	''' container that invokes a callback configured for applicable events '''
	__bool__ = __nonzero__ = lambda x: True
	def __init__(self):
		''' construct '''
		self.event_handlers_map = ddict(lambda: [])
		self.count = 0
		self.lock = RLock()
	# def __len__(self):
	# 	return self.count
	@property
	def handlers(self):
		result = []
		for handlers in self.event_handlers_map.values():
			result.extend(handlers)
		return result
	def subscribe(self, event_handler):
		with atomic(self.lock):
			bound_types = isinstance(event_handler, EventHandler) and event_handler.bound_types or [type(None)]
			for bount_type in bound_types:
				self.event_handlers_map[bount_type].append(event_handler)
				self.count += 1
		return self
	def unsubscribe(self, event_handler):
		''' broadcast event to applicable handlers '''
		for bount_type in event_handler.bound_types:
			if event_handler in self.event_handlers_map[bount_type]:
				self.event_handlers_map[bount_type].remove(event_handler)
				self.count -= 1
				if len(self.event_handlers_map[bount_type]) == 0:
					with atomic(self.lock):
						self.event_handlers_map.pop(bount_type)

		return self
	def emit(self, event_data, as_type=None, *args, **kws):
		''' broadcast event to applicable handlers '''
		as_type = as_type or type(event_data)
		with atomic(self.lock):
			for bound_type, event_handlers in self.event_handlers_map.items():
				for event_handler in event_handlers:
					if not isinstance(event_handler, EventHandler):
						event_handler(event_data, *args, **kws)
					elif event_handler.until and event_handler.until(event_data, as_type=as_type, *args, **kws):
						self.unsubscribe(event_handler)
					elif ((event_handler.condition and event_handler.condition(event_data, as_type, *args, **kws))
					or (event_handler.condition is None and (bound_type is type(None) or issubclass(as_type, bound_type)))):
						event_handler(event_data, as_type=as_type, *args, **kws)

	# method aliases
	__iadd__ = subscribe
	__isub__ = unsubscribe
	publish = emit

# class Subscriber(object):
# 	def __init__(self, name):
# 		self.name = name
# 	def update(self, event):
# 		print('{} received an update for item: "{}"'.format(self.name, event))

class Handlers(list):
	def __contains__(self, handler):
		result = next((item for item in self if item[0] == handler), None)
		return result is not None
	def __iand__(self, handler, condition=None, until=None):
		return self.__iadd__(handler, condition, until, allow_duplicate=False)
	def __iadd__(self, handler, condition=None, until=None, allow_duplicate=True):
		if allow_duplicate or handler not in self:
			self.append((handler, condition, until))
		return self
	def __isub__(self, handler):
		index = next((i for i, item in enumerate(self) if item[0] == handler), None)
		if index is not None:
			self.pop(index)
		return self
	def emit(self, event):
		self.emit_many([event])
	def emit_many(self, events):
		# handlers = list(self)
		for event in events:
			for handler, condition, until in self:
				if until is not None and until(event):
					self -= handler
				elif condition is None or condition(event):
					handler(event)

EventHandler				= CondHandler
EventPublisher				= CondPublisher
#endregion

#region event tests
def test_event_subscription_basic():
	# imports
	class View(str): pass
	class Like(str): pass
	class Comment(str): pass
	class MyPublisher(EventPublisher): pass
	class MyHandler(EventHandler):
		def on_event(self, event, *args, **kws): print('{}.on_event( {}("{}") )'.format(self.__class__.__name__, event.__class__.__name__, event))
	class MyNewHandler(MyHandler): pass
	class MyImprovedHandler(MyHandler): pass

	# construct
	publ_a = MyPublisher()
	publ_b = MyPublisher()
	processor_any = MyHandler()
	processor_views = MyNewHandler(bound_types=View)
	processor_views_likes = MyImprovedHandler(bound_types=[View, Like])

	# initialize
	publ_a += processor_views
	publ_a += processor_views_likes
	publ_b += processor_any
	publ_b += processor_views_likes

	# runtime
	publ_a.emit(View('Alice'))		# View type event received 2 view processors
	publ_a.emit(Comment('Bob'))		# Comment type event ignored 2 processors
	publ_b.emit(Like('Charley'))	# Like type event received by view processor and arbitrary processor
	publ_b.emit(Comment('Doug'))	# Comment type event received by 1 arbitrary processor, ignored by other processor

	# results:
	# 	MyNewHandler.on_event( View("Alice") )
	# 	MyImprovedHandler.on_event( View("Alice") )
	# 	MyHandler.on_event( Like("Charley") )
	# 	MyImprovedHandler.on_event( Like("Charley") )
	# 	MyHandler.on_event( Comment("Doug") )
	pass

if __name__=='__main__':
	test_event_subscription_basic()
#endregion
