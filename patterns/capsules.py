from functools import lru_cache

from pyutils.utilities.collbasics import Obj
from pyutils.defs.itemdefs import at9, at1
from pyutils.patterns.props import exact
from pyutils.structures.strs import TNames

from pyutils.defs.typedefs import *
from pyutils.utilities.alyzutils import *
from pyutils.utilities.callutils import fxof
from pyutils.utilities.clsutils import *
from pyutils.patterns.members import *
from pyutils.patterns.accessors import *

#region capsule defs
_NS						= Obj()
class DefsCapsule:
	#region namespaces
	class Keys:
		kk				=      fab   = (
						  tss('fab  ') )
		kk_args			=      noop, sub,  = (
						  tss('noop  sub  ') )
	_NS.kk_args			= Keys.kk_args
	class Args:
		aa_fill			= can, cant, noop, sub, = (
						  on,  no,   *_NS.kk_args,    )
	KK,AA				= Keys,Args
	#endregion
	#region colls
	fill_accessed		= set()
	_aliases_ptrn_sub	= [[r'_[\w_0-9]+__([\w_0-9]+)', r'\1'] ]
	#endregion
	...

DDCaps = DD				= DefsCapsule
KKCaps,AACaps			= DDCaps.KK,DDCaps.AA
#endregion

#region capsule utils
def _coerce_to_from_keys(to_from_keys):
	''' coerce to_from_keys as delimited str to str list then
		coerce each str in to_from_keys to (from_key, to_key) duplicate str pair
	'''
	to_from_keys = to_from_keys.split() if isinstance(to_from_keys, str) else list(to_from_keys)
	for ind, to_from_key in enumerate(to_from_keys):
		if isinstance(to_from_key, str):
			to_from_keys[ind] = (to_from_key, to_from_key)
		elif len(to_from_key) != 2:
			raise ValueError()
	return to_from_keys

def auto_kargs(attrfab, locals, prefer_self=True):
	self_attrs = {} if prefer_self is None else getattr(locals.get('self'), '__dict__', {})
	arg_keys = func_arg_keys(attrfab)
	avail_keys = tuple(locals.keys()) + tuple(self_attrs.keys())
	# miss_keys = set(avail_keys).difference(arg_keys)
	#
	# # generate KeyError since KeyErrors are not raised below
	# if miss_keys:
	# 	raise KeyError(f'Args {miss_keys} not found in locals or locals["self"] keys: {avail_keys}')
	
	# pull args from locals or locals['self']
	prime, aux = (self_attrs, locals) if prefer_self else (locals, self_attrs)
	kargs = {arg_key: prime.get(arg_key, aux.get(arg_key)) for arg_key in arg_keys}
	return kargs

def auto_apply(attrfab, locals):
	Kargs = auto_kargs(attrfab, locals)
	obj = attrfab(**Kargs)
	return obj

def route(cls, attrgetter, to_from_keys):
	''' add router as a property *to_obj*.*to_key* <- *from_obj*.*from_key* '''
	to_from_keys = _coerce_to_from_keys(to_from_keys)
	for to_key, from_key in to_from_keys:
		prop = Reroute(attrgetter, from_key)
		setattr(cls, to_key, prop)
	return cls

def add_rerouters(cls, to_from_keys, overwrite=False):
	''' add router as a property *cls*.*to_key* = property(*from_key*) '''
	# todo: make to_key distinct from from_keys
	to_from_keys = _coerce_to_from_keys(to_from_keys)
	for to_key, from_key in to_from_keys:
		if not overwrite and hasattr(cls, to_key): continue
		
		prop = Reroute('__getattr__', from_key)
		setattr(cls, to_key, prop)
	return cls

def add_reroutes(to_obj, from_obj, to_from_keys, overwrite=False):
	''' add route as a static attribute *to_obj*.*to_key* <- *from_obj*.*from_key* '''
	# todo: make to_key distinct from from_keys
	to_from_keys = _coerce_to_from_keys(to_from_keys)
	for to_key, from_key in to_from_keys:
		if not overwrite and hasattr(to_obj, to_key): continue
		
		value = getattr(from_obj, from_key)
		setattr(to_obj, to_key, value)
	return to_obj

def routesubattr(cls):
	''' use subattr as fall back for missing attr '''
	return cls


def to_ikv_rx(rx,node):
	out = keepmaprx(callitems_or_vars_of(node) or map0,rx)
	return out
def to_ik_rx(rx,node):
	out = keeprx(map(at0,callitems_or_vars_of(node) or ()),rx)
	return out

def find_members(rx_node,rx_leaf,src=miss,fabs=None,fab=list):
	from pprint import pprint as pp
	def find_members_in_src(src):
		to_ikv = lambda node: keepmaprx(callitems_or_vars_of(node) or map0,rx_node)
		to_ik = lambda node: keeprx(map(at0,callitems_or_vars_of(node) or ()),rx_leaf)
		out = walk_keys(src, to_ikv=to_ikv, to_ik=to_ik,fab=fab)
		return out
	out = src is miss and find_members_in_src or find_members_in_src(src)
	return out
#endregion

#region capsule devcs
# todo: if this is just a decorator then convert to function decorator
class routes:
	''' apply properties to *cls* for each *to_from_key* route on the getter(inst) result.
		__init__ -> __orig_init__
		attrroutes_init -> __init__
		:param cls: class to augment with properties
		:param attrgetter:
		:param to_from_keys:
		:param attrfab:
		:param autoargs: when true auto init *attrfab* with locals()
	'''
	def __init__(self, attrgetter, to_from_keys):
		self.attrgetter = attrgetter
		self.to_from_keys = to_from_keys
	def __call__(self, cls):
		self.add(cls, **self.__dict__)
		return cls
	@classmethod
	def add(decorator, cls, attrgetter, to_from_keys, **_):
		to_from_keys = _coerce_to_from_keys(to_from_keys)
		for from_key, to_key in to_from_keys:
			setattr(cls, to_key, _AttrProp(attrgetter, from_key))
		return cls

class routesattr(routes):
	'''	Modify a class with a new member attribute property accessors to select sub-attributes.
		apply properties to *cls* for each *to_from_key* route on the getter(inst) result.
		__init__ -> __orig_init__
		attrroutes_init -> __init__
		:param attrgetter:
		:param to_from_keys:
		:param attrfab:
		:param autoargs: when true auto init *attrfab* with locals()
		:param cls: class to augment with properties
	'''
	def __init__(self, attr, to_from_keys, attrfab, autoargs=True):
		super().__init__(attr, to_from_keys)
		self.attrfab = attrfab
		self.autoargs = autoargs
	@classmethod
	def add(decorator, cls, attrgetter, to_from_keys, attrfab, autoargs=True, **_):
		attr = attrgetter
		cls.__post_init_attrs__ = decorator.post_init_attrs
		cls.__orig_init__ = cls.__init__
		cls.__init__ = decorator.attr_routes_init
		
		# store in *cls*._post_init_attrs data needed to create and apply *attr* during instantiation
		if not hasattr(cls, '_post_init_attrs'):
			cls.__post_init_attrs = {}
		cls.__post_init_attrs.setdefault(cls, ())
		cls.__post_init_attrs[cls] += ((attr, attrfab, autoargs),)
		
		# apply routes to subattrs as properties
		cls = routes.add(cls, attr, to_from_keys)
		return cls
	
	@staticmethod
	def attr_routes_init(inst, *a, **k):
		inst.__orig_init__(*a, **k)
		inst.__post_init_attrs__(*a, **k)
	@staticmethod
	def post_init_attrs(inst, *a, locals=None, **k):
		_post_init_attrs = inst.__post_init_attrs[inst.__class__]
		# construct attr given each attrfab and apply to self instance
		for attr, attrfab, autoargs in _post_init_attrs:
			if hasattr(inst, attr): continue
			
			if not autoargs:
				attr_obj = attrfab()
			# elif isinstance(attrfab, Fabricator):
			# 	attr_obj = attrfab.auto_construct(dict(self=inst, **k))
			else:
				attr_obj = auto_apply(attrfab, dict(self=inst, **k))
			setattr(inst, attr, attr_obj)

class Route(MemberKeys):
	def __init__(self,*_kk,kk=miss,fab_pick=None,fab_put=None,**ka):
		kk = kk or _kk
		super().__init__(kk)
		self.fab_pick = exact(fab_pick)
		self.fab_put = exact(fab_put)
	def pick(self,src, get=None):
		get = get or get_node
		out = get(self.kk,src)
		out = out if not self.fab_pick else self.fab_pick(out)
		return out
class Routes:
	def __init__(self,*_routes,routes=miss,fab_pick=None,fab_put=None,**ka):
		self.routes = routes or _routes
		self.fab_pick = exact(fab_pick)
		self.fab_put = exact(fab_put)
	def __repr__(self):
		s_out = sj('+',self.routes)
		return s_out
	def pick(self,src, fab=None,**ka):
		outs = (route.pick(src,**ka) for route in self.routes)
		outs = outs if not self.fab_pick else self.fab_pick(outs)
		outs = outs if not fab else fab(outs)
		return outs
#endregion

#region prop devcs
class Rename:
	''' Functions as a configurable readonly property '''
	__slots__ = ['getter', 'attr_key']
	def __init__(self, getter, attr_key):
		self.getter = getter  # todo: remove this
		self.attr_key = attr_key
	def __get__(self, obj, cls):
		if obj is None: return self
		
		got = getattr(obj, self.getter) if isinstance(self.getter, str) else self.getter(obj)
		result = getattr(got, self.attr_key)
		return result

class Reroute(Rename):
	''' Functions as a configurable read/write property '''
	def __set__(self, obj, value):
		raise NotImplementedError()

class CapsuleMember:
	def __init__(self, capsule, attr):
		self.capsule = capsule
		self.attr = attr

class CapsuleMethod(CapsuleMember):
	def __init__(self, capsule, call):
		self.member,attr = isntstr(call) and [call,call.__name__] or [None,call]
		super().__init__(capsule, attr)
	def __call__(self, *pa, **ka):
		self.member = self.member or getattr(self.capsule.obj, self.attr)
		return self.capsule.call_wrapper(self.member, *pa, **ka)

_AttrProp				= Reroute
#endregion

#region capsule devcs
class PropCapsule:
	''' _Property_ container encasing an *obj* and preempts access on _some_ attrs on *obj* via properties '''
	# todo: simplify
	# todo: add usage and examples
	# todo: replace MemberAccessor & MethodAccessor with pyutils..members classes
	# todo: replace reroutes and add_reroutes with intomap and IntoKeys
	en_get: iscall		= None
	en_set: iscall		= None
	en_call: iscall		= None
	ex_get: iscall		= None
	ex_set: iscall		= None
	ex_call: iscall		= None
	_getting=_setting	= None
	_calling=_reroutes	= None
	def __init_subclass__(cls, **ka): pass  # todo:
	def __init__(self, obj=None, *pa, getting=None, setting=None, calling=None, call=None, reroutes=None, **ka):
		self._obj = self.obj = obj
		self.assign_wrappers(*(getting or self._getting or ()) + (setting or self._setting or ()))
		self.assign_wrappers(*(calling or self._calling or ()), as_method=yes)
		self._call = (
			call					if not call else
			call(self._obj)			if iscx(call) else
			exact(lambda obj,cx=call: getattr(obj.__class__,cx))) 
		add_reroutes(self, self.obj, reroutes or self._reroutes or ())
		super().__init__(*pa, **ka)
	def assign_wrappers(self, *attrs, as_method=no):
		''' Assign *obj* accessors for given attrs '''
		for attr in attrs:
			accessor_fab = (not as_method and isstr(attr)) and CapsuleMember or CapsuleMethod
			accessor = accessor_fab(self, attr)
			self.__dict__[accessor.attr] = accessor
	def __repr__(self):				return f'Capsule({self._obj!r})'
	def __str__(self):				return f'<<{self._obj!r}>>'
	def __call__(self,*pa,**ka):
		method = (self._call and self._call(self.obj) or self.obj.__class__.__call__)
		out = method(self._obj,*pa,**ka)
		return out
	
	def get_wrapper(self, name):
		self.en_get and self.en_get(name)
		out = getattr(self._obj, name)
		self.ex_get and self.ex_get(name)
		return out
	def set_wrapper(self, name, val):
		self.en_set and self.en_set(name, val)
		out = setattr(self._obj, name, val)
		self.ex_set and self.ex_set(name, val)
		return out
	def call_wrapper(self, call, *pa, **ka):
		self.en_call and self.en_call(func, *pa, **ka)
		out = call(*pa, **ka)
		self.ex_call and self.ex_call(func, out, *pa, **ka)
		return out
class FillPropCapsule(PropCapsule): pass  # todo: detect and assign_wrappers attributes via incl & excl keys
class ViewCapsule(PropCapsule):
	def set_wrapper(self, name, value):
		self.en_set and self.en_set(name, value)
		pass	# setattr(...) replaced with noop()
		self.ex_set and self.ex_set(name, value)
class GettableCapsule(PropCapsule):
	''' produce gettable interface wrapper on any coll. obj having __getitem__() or get() '''
	__name__			= 'GetIxOn'
	def __init__(self,*pa,err=yes,**ka):
		super().__init__(*pa,**ka,call=err and '__getitem__' or 'get')

class NewCapsule:
	''' _Vars_ container encasing an *obj* and preempts access on _some_ attrs on *obj* '''
	# todo

class FillCapsuleProp:
	_fab_sub				= staticmethod(Obj)  # fixme: Obj does not have ideal functionality
	@lru_cache()
	def __new__(cls, attr):
		obj = super().__new__(cls)
		obj.attr = attr
		return obj
	def __get__(self, capsule, cls):
		result = self
		if capsule is not None:
			if capsule.can_get == AACaps.can:
				try:
					result = getattr(capsule._obj, self.attr)
				# except Exception as exc:
				except AttributeError as exc:
					raise Exception(exc)
					# raise AttributeError(self.attr)
			elif capsule.can_get == AACaps.cant:
				raise AttributeError('cannot access {0.__class__}.{1} on {0!r}'.format(capsule,self.attr))
			elif capsule.can_get == AACaps.sub:
				getattr(capsule._obj._sub,self.attr,None)
			else:
				result = capsule.can_get
		return result
	def __set__(self, capsule, value):
		result = None
		if capsule is not None:
			'_sub' in capsule._obj.__dict__ or capsule._obj.__dict__.setdefault('_sub',self._fab_sub())
			if capsule.can_set == AACaps.can:
				result = setattr(capsule._obj, self.attr, value)
			elif capsule.can_set == AACaps.cant:
				raise AttributeError('cannot access {0.__class__}.{1} on {0!r}'.format(capsule,self.attr))
			elif capsule.can_set == AACaps.sub:
				setattr(capsule._obj._sub,self.attr,value)
		return result

@dataclassfroz
class FillCapsule:
	''' _Frozen_ container encasing an *obj* and preempts access on _all_ attrs on *obj* '''
	_obj: object			= None
	vars: dict				= None
	_can_get=_can_set		= True
	_fab_prop				= FillCapsuleProp
	def __init_subclass__(cls, can_get=_can_get,can_set=_can_set):
		cls._can_get = cls.can_get = can_get
		cls._can_set = cls.can_set = can_set
	def __post_init__(self):
		self.can_access(no)
		self.vars and self.__dict__.update(self.vars)
		self.can_access(can_get=self._can_get,can_set=self._can_set)
		self._obj = getattr(self._obj,'_obj',self._obj)
		assert not hasattr(self._obj,'_obj')
	def can_access(self, can=on, can_get=miss, can_set=miss):
		self.can_get = can_get if can_get != miss else can
		self.can_set = can_set if can_set != miss else can
	def policy_of_attr(self,attr):
		result = KKCaps.fab  # todo
		result = KKCaps.noop  # todo
		return result
	def __getattr__(self,attr):
		''' should only ever be called once per (class,attr) pair '''
		fill_access = '{}.{}'.format(self.__class__.__name__,attr)
		assert fill_access not in DDCaps.fill_accessed, fill_access
		# DDCaps.fill_accessed.add(fill_access)
		
		# resolve inputs
		policy = self.policy_of_attr(attr)
		
		if policy == KKCaps.fab:
			prop = self._fab_prop(attr)
			setattr(self.__class__,attr,prop)  # fixme: instance shouldnt tamper with classes
			try:
				result = getattr(self,attr)
			except Exception:
				raise AttributeError(attr)
		elif True:
			result = getattr(self._obj,attr)
		elif False:	raise NotImplementedError()
		else:		raise AttributeError()
		return result
class FullCapsule(FillCapsule):
	''' _Frozen_ container encasing an *obj* and preempts access on _some_ attrs on *obj* '''
class StasisCapsule(FullCapsule, can_set=AACaps.cant):
	''' _Frozen_ container encasing an *obj* and preempts get and prevents set on _some_ attrs on *obj* '''
class StasisViewCapsule(FullCapsule, can_set=AACaps.sub):
	''' _Frozen_ container encasing an *obj* and preempts get and prevents set on _some_ attrs on *obj* '''

class EvalCapsule(FillCapsule):
	''' capsule for custom mapping. useful for unhashable keys '''
	_obj: object
	keys: strs			= None
	key: str			= None
	to_hash: iscall		= None
	to_val:iscall		= None
	_to_val				= None
	def __init_subclass__(cls, **ka):
		cls.__hash__ = cls._hash
	def __post_init__(self):
		self.keys = isstr(self.keys, fab=str.split)
		self.to_val = self.to_val or self._to_val
		super().__post_init__()
	def __init__(self,*pa,**ka):
		annoinit(self,*pa,**ka)
		self.keys = isstr(self.keys, fab=str.split)
		self.to_val = self.to_val or self._to_val
		# self.redirect()
		super().__post_init__()
	def eval(self, key=None, keys=None):
		key, keys = None is key is keys and (self.key,self.keys) or (key,keys)
		to_val = isinstance(self.to_val, str) and getattr(self._obj.__class__, self.to_val) or self.to_val
		val = getattr(self._obj, key) if key else tuple((to_val(self._obj, key) for key in self.keys))
		return val
	def _hash(self):
		to_hash = self.to_hash or hash
		out = to_hash(self.val_new)
		return out
	def __eq__(self, other):
		val_other = other if not isinstance(other, EvalCapsule) else other.val_new
		out = self.val_new == val_other
		return out
	val_new				= property(eval)
class VarsCapsule(EvalCapsule):
	_to_val				= getattr
class MapCapsule(EvalCapsule):
	_to_val				= '__getitem__'

class Analog:
	''' Statically reassigns attrs from matching ptrn to its sub according to **aliases_ptrn_sub** '''
	_aliases_ptrn_sub	= ()
	def __init__(self, obj, aliases_ptrn_sub=None):
		aliases_ptrn_sub = aliases_ptrn_sub or self._aliases_ptrn_sub
		attr_remap = {}
		[Analog.get_attr_remap(obj, *alias, attr_remap=attr_remap) for alias in aliases_ptrn_sub]
		Analog.rename(self, obj, attr_remap)
	@staticmethod
	def get_attr_remap(obj:object, ptrn:str, sub:str=r'\1', attr_remap:dict=None):
		''' add aliases to adapter for all obj attr matches of given patterns '''
		attr_remap = isnone(attr_remap,call=dict)
		for attr in dir(obj):
			alias, count = re.subn(ptrn, sub, attr)
			if count:
				attr_remap[attr] = alias
		return attr_remap
	@staticmethod
	def rename(obj:object, src:object, remap:dict):
		for attr, alias in remap.items():
			member = getattr(src, attr)
			setattr(obj, alias, member)
	@staticmethod
	def remap(items:dict, src:dict, remap:dict):
		for key, alias in remap.items():
			value = src[key]
			items[alias] = value

class Reroutes:
	''' reroutes top level *key* aliases to other multi-level key path '''
	#region defs
	_iface_map			= dict(get=mget,set=mset,pop=mpop)
	_iface_obj			= dict(get='__getattr__',set='__setattr__',pop='__del__')
	fab_at				= dict(
		to_key			= at9,
		# to_val			= asis,
		to_val			= lambda route: tj(isstr(sub) and [sub] or sub for sub in route),
		# to_route		= lambda route: tj(isstr(sub) and [sub] or sub for sub in route),
		# to_route		= asis,
		to_route_key	= asis,
		rx_node			= None,
		rx_leaf			= None,
		to_ikv			= None,
		to_ik			= None,
		fabs			= MembersFinal,
		to_route		= MembersFinal,
	)
	_sz_kk_new			= property(lambda self: max(lens(self.kk_into)))
	#endregion
	#region forms
	def __init__(self, src=None, iface=_iface_map, kk_into=None, resolver=None, prepare=no,**ka):
		self._src = src
		fab_at = dict(self.fab_at,**intomap(ka,self.fab_at))
		infuse(self,fab_at)
		self.iface = iface
		self.kk_into = isnone(kk_into,call=dict)
		self._resolver = resolver
		prepare and self.prepare()
	def __repr__(self):
		s = SJ.jnl(map(f'{{0[0]:{self._sz_kk}}} :{{0[1]}}'.format,sorted(zof(self.kk_into))))
		return s
	#endregion
	#region mbrs
	def prepare(self, rx_node=None,rx_leaf=None,src=None,**ka):
		# resolve inputs
		self.rx_node = rx_node or self.rx_node
		self.rx_leaf = rx_leaf or self.rx_leaf
		self._src = isnone(src,put=self._src)
		to_key,to_val,to_route = self.to_key or last, self.to_val, self.to_route or asis
		assert to_key and to_val, (to_key,to_val)
		
		# resolve output
		routes = find_members(self.rx_node,self.rx_leaf,self._src)
		for route in keep(routes):
			self.add(route,**ka)
		replace_map_links(self.kk_into)
		self._resolver = self.kk_into.get
		self._sz_kk = self._sz_kk_new
		return self
	def discard(self,drop=always,keep=None):
		drops = [[key,self.kk_into.pop(key)] for key in [*self.kk_into] if drop(key)]
		return drops
	def add(self,route,key=miss,kk_src=(),aliases=None,**ka):
		to_key,to_val,to_route = self.to_key or last, self.to_val, self.to_route or asis
		vv = to_val(route)
		kk,route_out = collany(key or to_key(vv)), to_route(vv)  # todo: to_key should convert a raw route
		self.kk_into.update(mapi(kk,val=route_out))
	def set(self,key,route,**ka):
		return self.add(key=key,route=route,**ka)
	def update(self,routes=(),kk=None,kroutes=None,kk_src=(),aliases=None,**ka):
		to_key,to_val,to_route = self.to_key or last, self.to_val, self.to_route or asis
		kroutes = zof(kroutes) or zip(routes,isnone(kk,fab=gen))
		for key,route in kroutes:
			self.add(key=key,route=route,**ka)
		
		# apply aliases
		_kroutes_alias = lj(zip(aliases.get(key,()),gen(route)) for key,route in zof(self.kk_into))
		kroutes_alias = aliases and dj(zip(aliases.get(key,()),gen(route)) for key,route in zof(self.kk_into))
		kroutes_alias and self.kk_into.update(kroutes_alias)
		self._sz_kk = self._sz_kk_new
	#endregion
	#region utils
	def find(self, incl=None,excl=None): pass
	def _resolve(self, key, fill=miss):
		''' resolve real =>route of *key* '''
		route = self.resolver(key)
		return route
	def route_of(self, key, fill=miss):
		''' resolve real =>route of *key* '''
		not self._resolver and self.prepare()
		route = self._resolver(key,*[fill][:fill is not miss])
		return route
	
	def get_item(self, key, *pa, fill=miss, src=None, iface=None, exc=KeyError, **ka):
		# note: route fabs can implement __index__ to reduce
		# resolve inputs
		route = self.route_of(key,fill=fill)
		get = self.iface['get']
		get = iscx(get,orput=None) or getattr(isnone(src,put=self._src), get)
		
		# resolve output
		out = (
			throw(exc(key))		if route is None else
			fill				if route is fill else
			get(route,self._src,*pa,**ka) )
		return out
	def set_item(self, key, *pa, fill=miss, src=None, **ka):
		# resolve inputs
		route = self.route_of(key, *ismiss(fill,put=(),orput=(...,)))
		set = getattr(isnone(src,put=self._src), self.iface['set'])
		
		# resolve output
		out = set(route,self._src,*pa,**ka) if route is not miss else fill
		return out
	def pop_item(self, key, *pa, fill=miss, src=None, **ka):
		# resolve inputs
		route = self.resolver(key, *ismiss(fill,put=(),orput=(...,)))
		pop = getattr(isnone(src,put=self._src), self.iface['pop'])
		
		# resolve output
		out = pop(route,self._src,*pa,**ka) if route is not miss else fill
		return out
	
	def __getitem__(self, key, *pa, **ka):
		assert self._src  is not None, 'missing ._src'
		return self.get_item(key,**ka, iface='__getitem__')
	def get_attr(self, key):
		self._src is None		and throw(ValueError('missing ._src'))
		key.startswith('__')	and throw(AttributeError(key))
		return self.get_item(key, iface='__getattr__', exc=AttributeError)
	
	__call__			= route_of
	__getattr__			= get_attr
	#endregion
	...
class HasReroutes:
	# todo: baseline implementation for Reroutes container
	pass

class UnmangleAnalog(Analog):
	''' Statically reassigns privately named (mangled) attrs to public attr names '''
	_aliases_ptrn_sub	= [[r'_[\w_0-9]+__([\w_0-9]+)', r'\1'] ]

Capsule					= PropCapsule
GetIxOn					= GettableCapsule
#endregion

#region other capsule devc
# todo: relo maybe theres a better place for this
class CallName:
	fmt					= lambda cls: f'<{nameof(cls)}>'
	def __init_subclass__(cls, fmt=fmt):
		cls.fmt = exact(fmt)
	def __init__(self,call,fmt=None):
		self.rep = fmtof(fmt or fxof(self.fmt))(call)
		self.call = call
	def __repr__(self):				return self.rep
	def __call__(self,*pa,**ka):	return fxof(self.call)(*pa,**ka)
class CallQual(CallName, fmt=lambda call: f'<{qualof(call)}>'): pass

namecx,qualcx			= CallName, CallQual
#endregion

#region notes
'''
- capsule: container encasing an *obj* and preempts access to _some_ attrs on *obj* via properties.
  - useful for augmenting or observing an *obj* and usually taking its place.
'''
#endregion
