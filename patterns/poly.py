''' Obsolete
there are multiple potential replacements for Poly including: callutils (primary), accessors, args
'''

from itertools import *

from pyutils.defs.colldefs import collany, redef_per_coll, DDColl as Colls
from pyutils.patterns.accessors import *
from pyutils.utilities.sequtils import *
from pyutils.structures.args import *


class PolyObj:
	''' Store an operation by name without its instance or class definition to later
		be invoked polymorphically on any instance.  Hides getattr(obj, 'operation') 
		with syntactic sugar Poly.operation
		
		Usage:
		const_op_run = Poly.run         # define operation as a constant; 'run' can be any action; no need for class or instance
		const_op_run(SomeRunnable())    # invoke const 'run' operation on arbitrary runnable instance
		const_op_run(SomeRunnable(), *[], **{})    # any other args accepted as usual
		
		Notes:
		- Normally calling a base class method on a child instance does not invoke
		any overriden methods because python does not employ polymorphism. (For example
		in static invocation with subclass type 'A.run(ab) # => A running: ...';
		see below in __main__ tests)
		- functools.partial/partialmethod also does not behave polymorphically.
		- python.ABC only enforces that subclasses implement all abstract methods.
	'''
	def __init__(self, attr=None, func=None, name=None):
		self.attr = attr
		
		# customarily defined via Polynom(a=0, ...)
		self.name = name #or f'Poly.{self.attr}(obj, ...)'
		self.func = func
		if self.func:
			self.__call__ = self.func
		
	def __repr__(self):
		result = f'Poly.{self.attr}'
		return result
	def __str__(self):
		result = self.name or object.__str__(self)
		return result
	def __getitem__(self, index):
		return self.__call__(index)
	def __call__(self, obj, *args, **kws):
		if self.func:
			result = self.func(obj, *args, **kws)
		else:
			result = getattr(obj, self.attr)(*args, **kws)
		return result
	def __get__(self, instance, owner):
		method = getattr(instance, self.attr)
		return method
	
	def resolve(self, obj):
		func = getattr(obj, self.attr)
		return func

class PolynomObj(PolyObj):
	''' Store an operation by name with some preset arguments but without its instance
		or class definition to later be invoked polymorphically on any instance.
		Similar in concept to a polynomial as it is an operation/expression in which
		some variables are known and others are not. Hides getattr(obj, 'operation')
		with syntactic sugar Polynom.operation
		Calling with preset parameters returns an function lookup lambda that takes roughly
		(obj, ..., **presets)
		
		Usage:
		const_op_pop = Polynom.pop                      # next __call__ takes preset args and returns a lambda
		op_pop_2nd = Polynom.pop(1)                     # define operation with presets as a constant; next __call__ takes instance and invokes operation
		op_pop_2nd(['keep', 'drop', 'keep', 'keep'])    # invoke const operation on arbitrary poppable instance
	'''
	def __getitem__(self, index):
		return self.__call__(index)
	def __call__(self, *pre_args, **pre_kargs):
		name = 'Polynom.%s(%s%s)' % (
			self.attr,
			(pre_args and f'{"".join(map(str, pre_args))}') or '',
			(pre_kargs and f'{",".join([str(k)+"="+str(w) for k, w in pre_kargs.items()])}') or '')
		func = lambda obj, *args, **kws: getattr(obj, self.attr)(*pre_args, *args, **pre_kargs, **kws)
		result = PolyObj(self.attr, func, name)
		return result

class PolySeq:
	def __init__(self, func_arg, get_iter=None, coll=None, frepr=None):
		self.func_arg = func_arg
		self.frepr = frepr
		self.coll = coll
		self.get_iter = get_iter
	def apply(self, seq, *pa, **ka):
		items = seq if self.get_iter is None else self.get_iter(seq)
		func = self.func_arg
		for item in items:
			if isinstance(self.func_arg, str):
				func = getattr(item.__class__, self.func_arg)
			value = func(item, *pa, **ka)
			yield value
	def apply_coll(self, seq, *pa, **ka):
		items = self.apply(seq, *pa, **ka)
		if self.coll:
			items = self.coll(items)
		return items
	# method aliases
	__call__ = apply_coll

class Fab: pass
class AttrPreFab:
	''' Takes a *prefab*ricator function then generates fabricator for each attribute name access.
		Note: the resulting fab is expected to be callable
	'''
	def __init__(self, prefab, *pa_def, **ka_def):
		self.__dict__.update(dict(prefab=prefab, pa_def=pa_def, ka_def=ka_def))
	def prefabricate(self, attr:str) -> Fab:
		fab = self.prefab(attr, *self.pa_def, **self.ka_def)
		return fab
	# method aliases
	__getattr__ = prefabricate

class AttrFab:
	''' Takes a *fab*ricator function then generates objects for each attribute name access. '''
	def __init__(self, fab, *pa_def, **ka_def):
		self.__dict__.update(dict(fab=fab, pa_def=pa_def, ka_def=ka_def))
	def fabricate(self, attr:str):
		# todo: add caching by default
		# result = self.fab(attr, *pa, *self.pa_def, **ka, **self.ka_def)
		result = self.fab(attr, *self.pa_def, **self.ka_def)
		return result
	# method aliases
	__getattr__ = fabricate

Poly = PolyFab = AttrFab(PolyObj)			# Poly is a Fab which creates a PolyObj for the accessed attr name
Polysd, Polysk, Polysl, Polyst = [AttrFab(PolySeq, coll=coll) for coll in Colls.dhlt]
Polynom = PolynomFab = AttrFab(PolynomObj)	# Polynom is a Fab which creates a PolynomObj for the accessed attr name

# todo: replace references to obsolete class aliases
_Poly = PolyObj
_Polynom = PolynomObj
PolyFactory = AttrFab

def get_polynoms(keys, polynom, *args, **kws):
	if not isinstance(keys, (list, tuple)):
		if (isinstance(keys, str) and len(keys) >= 1) or isinstance(keys, list):
			pass
		else:
			keys = [keys]
	args_list = zip(args)
	kws_list = list(kws.items())
	key, value_list = range(2)
	key_values_product = list(map(lambda k: list(product([k[key]], k[value_list])), kws.items()))
	keys_args_kws = zip_longest(keys, zip(*args), zip(*key_values_product), fillvalue=[])
	result = {key:polynom(*key_args, **dict(key_kws)) for key, key_args, key_kws in keys_args_kws}
	return result

def coerce_method(instance, func):
	''' Bind the function *func* to *instance*, with either provided name *as_name*
		or the existing name of *func*. The provided *func* should accept the
		instance as the first argument, i.e. "self".
	'''
	bound_method = func.__get__(instance, instance.__class__)
	return bound_method

def bind_method(instance, func, as_name=None):
	''' Bind the function *func* to *instance*, with either provided name *as_name*
		or the existing name of *func*. The provided *func* should accept the
		instance as the first argument, i.e. "self".
	'''
	if as_name is None:
		as_name = func.__name__
	bound_method = func.__get__(instance, instance.__class__)
	setattr(instance, as_name, bound_method)
	return bound_method

if __name__=='__main__':
	''' A simple demonstration '''
	### Configuration #############################
	op01_len = Poly.__len__
	op02_pop_first = Polynom.pop(0)
	op03_pop_last = Polynom.pop(-1)
	op04_get_item = Polynom.__call__(start=0, stop=None, step=1)
	
	### Runtime ###################################
	data = [1,2,3,4,5,6,7]
	print(f'{op01_len}')
	print(f'{op02_pop_first}')
	print(f'{op03_pop_last}')
	print(f'{op04_get_item}')
	print(op01_len(data))			# => 7
	print(op02_pop_first(data))	# => 1
	print(op03_pop_last(data))	# => 7
	# print(data[op04_get_item(slice)])					# => [2, 3, 4, 5, 6]


	''' A more real-world target scenario; invoking polynoms (canned operations+parmaters) from a dict '''
	# A prototypical inheritance scheme
	class A:
		def do(self, input, other=None):
			print('A doing: %s, %s' % (input, other))
		def run(self, input, other=None):
			print('A running: %s, %s' % (input, other))

	class AA(A):
		def do(self, input, other=None):
			print('AA doing: %s, %s' % (input, other))
		def run(self, input, other=None):
			print('AA running: %s, %s' % (input, other))

	class AB(A):
		def do(self, input, other=None):
			print('AB doing: %s, %s' % (input, other))
		def run(self, input, other=None):
			print('AB running: %s, %s' % (input, other))

	class Keys:
		lrup = (Left, Right, Up, Down) = 'LRUD'
		wens = (West, East, North, South) = 'WENS'
		adws = (A, D, W, S) = 'ADWS'
		cell_nav = []

	class Scalar:
		directions = X_pos, Y_pos, X_neg, Y_neg,  = [1,0], [0,1], [-1,0], [0,-1]
		lrup = (L, R, U, D) = (Y_neg, Y_pos, X_neg, X_pos)


	### Configuration #############################
	routines_do = get_polynoms(Keys.lrup, Polynom.do, Scalar.lrup, Scalar.lrup)                # get dict of 'obj.do' polynoms given args
	routines_run = get_polynoms(Keys.lrup, Polynom.run, input=Scalar.lrup, other=Scalar.lrup)  # get dict of 'obj.run' polynoms given kws

	### Runtime ###################################
	a = A()
	aa = AA()
	ab = AB()

	routines_do['L'](a)		# => A doing: [0, -1], [0, -1]
	routines_do['L'](aa)	# => AA doing: [0, -1], [0, -1]
	routines_do['L'](ab)	# => AB doing: [0, -1], [0, -1]
	routines_run['R'](a)	# => A running: [0, 1], [0, 1]
	routines_run['U'](aa)	# => AA running: [-1, 0], [-1, 0]
	routines_run['D'](ab)	# => AB running: [1, 0], [1, 0]
	exit()


	''' Early experimentation with different methods to store functions for polymorphic behavior '''
	from functools import *
	
	def do_op(obj, *args, **kws):
		result = obj.do(*args, **kws)
		return result
	def op(obj, name, **kws):
		func = getattr(obj, name)
		result = func(obj, **kws)
		return result
	do = partial(op, name='do', input='better')
	run = partial(op, name='run', input='faster')
	
	def foobar_template(op, other=None):
		''' needing a generated lambda taking an object
			1. parameters are predefined constants
			2. this lambda generator takes a function as input
			3. obj is given at runtime
		'''
		result = lambda obj: op(obj, 'foobar_const')
		return result
	
	### Configuration #############################
	a = A()
	aa = AA()
	ab = AB()

	### Runtime ###################################
	do(A, other=0)
	do(AA, other=1)
	do(AB, other=2)
	run(A, other=3)
	run(AA, other=4)
	run(AB, other=5)
