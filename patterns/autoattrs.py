''' Minor addition to attrs module to support creation of classes from annotations '''

# from attr import *
from dataclasses import dataclass


# def autoattrs(*args, cmp=None, **kargs):
# 	return attrs(*args, auto_attribs=True, **kargs)
def autoattrs(*args, cmp=None, **kargs):
	return dataclass(*args, **kargs)
# autoattrs = dataclass