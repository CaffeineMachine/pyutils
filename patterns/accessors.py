from dataclasses import dataclass

from pyutils.defs.itemdefs import infuse
from pyutils.defs.typedefs import *
from pyutils.patterns.props import exact
from pyutils.utilities.callbasics import redef_per_coll
from pyutils.utilities.iterutils import rep, laffix, ijoin, ss, lss, map0
from pyutils.structures.strs import Names

#region mbr defs
class DefsAccessors:
	cached_attr_getters = {}
_D = DefsAccessors
Def = Defs = DefsAccessors
#endregion

#region mbr utils
def attrs(keys, lkeys=(), rkeys=(), fab=None, mbr_cls=None, **ka_attr):
	''' fab Attr for key in keys w/ prefix lkeys and suffix rkeys. As in: [Attr(lkeys+[k]+rkeys) for k in keys] '''
	keys, lkeys, rkeys = lss(keys), lss(lkeys), lss(rkeys)
	keys = (lkeys+[key]+rkeys for key in keys)
	out = (Members(key, mbr_cls=mbr_cls, **ka_attr) for key in keys)
	out = fab(out) if fab else out
	return out
#endregion

#region mbr devcs
@dataclass(repr=False)
class MemberKey:
	key: object
	fget: iscall			= None
	fset: iscall			= None
	#region defs
	_fget,_fset				= None, None
	_call					= 'get'
	_fmt					= '<{}>'
	#endregion
	#region forms
	def __init_subclass__(cls, fget=None,fset=None,call=None,fmt=None,**ka):
		ka_dunder = dict(_fget=fget,_fset=fset,_call=call,_fmt=fmt)
		ka.update((k,v) for k,v in ka_dunder.items() if v is not None)
		infuse(cls,ka)
	def __post_init__(self):
		key = self.key
		if isstr(key) and len(key) >= 4 and key[:2] == '__' and key[-2:-1] != '_' and key[-1] == '_':
			self.key += '_'
		self.fget = isnone(self.fget, put=self._fget)
		self.fset = isnone(self.fset, put=self._fset)
	def __str__(self):				return str(self.key)
	def __repr__(self):				return self._fmt.format(self.key)
	def __hash__(self):				return hash(self.key)
	def __eq__(self,val):			return self.key == val
	def __lt__(self,val):			return self.key <  val
	#endregion
	#region utils
	def __call__(self,*pa,**ka):	return getattr(self, self._call)(*pa,**ka)
	def has(self,obj):				return hasattr(obj, self.key)	# fixme: incorrectly named; should be is_in
	def pick(self,obj,*pa,**ka):
		out = self.fget(obj, self.key,*pa,**ka)
		return out
	def put(self, obj,*pa,**ka):
		result = self.fset(obj, self.key, *pa, **ka)
		return result
	get,set					= pick,put
	#endregion
	...
class GetAttr(MemberKey, fget=getattr,fset=setattr,fmt='.{}'): pass
class GetCall(MemberKey, fmt='{}()'):
	_fget = staticmethod(lambda obj,*pa,**ka: obj(*pa,**ka))
class GetKey(MemberKey,fmt='[{}]'):
	_fget = staticmethod(lambda obj,*pa,**ka: obj.__getitem__(*pa,**ka))
class GetKeyAlt(MemberKey, fmt='.get({})'):
	_fget = staticmethod(lambda obj,*pa,**ka: obj.get(*pa,**ka))
class SetAttr(MemberKey, fget=getattr,fset=setattr,call='set',fmt='.{}'): pass

class MemberKeys:
	mbrs: tuple				= ()
	#region defs
	frozen					= no
	getters					= (get_index, get_attribute, get_callable) = GetKey, GetAttr, GetCall
	_classes				= {GetKey:(int,slice,tuple,list), GetAttr:str, GetCall:object}
	_cache					= False  # todo: consider LRUCacheMap or clsargs substitution 
	_to_key					= None
	_keys					= property(lambda self: [mbr.key for mbr in self.mbrs])
	accessors				= property(lambda self: self.mbrs)  # todo: eliminate
	#endregion
	#region forms
	def __init_subclass__(cls, **ka):
		for attr,val in ka.items():
			setattr(cls, attr, val)
	def __init__(self, keys=None, mbr_cls=None, prev_mbrs=(), cache=None, to_key=None,frozen=miss):
		self._to_key = isnone(to_key, put=self._to_key)
		self._cache = isnone(cache, put=self._cache)
		mbrs = self.coerce_keys(keys, mbr_cls)
		self.mbrs = list(prev_mbrs) + mbrs  # todo: remove keys
		self.frozen = ismiss(frozen,put=self.frozen)
	def coerce_key(self, key, to_key=None, mbr_cls=None):
		if isinstance(key, Member): return key	# shortcut for no change
		
		# resolve output
		to_key = to_key or self._to_key
		key = key if not to_key else to_key(key)
		mbr = (
			mbr_cls(key)		if mbr_cls else
			key					if istype(key,MemberKey) else
			GetAttr(key)		if istype(key, self._classes[GetAttr]) else
			GetKey(key)			if istype(key, self._classes[GetKey]) else
			GetCall(key)		if iscall(key) else
			key )
		return mbr
	def coerce_keys(self, keys, mbr_cls=None):
		if keys is None: return []
		keys = isstr(keys, fab=Names)
		keys = isiter(keys, orfab=laffix)
		mbrs = [self.coerce_key(key, mbr_cls) for key in keys]
		return mbrs
	def __repr__(self):						return ''.join(ss(self.mbrs,to_val=repr))
	def __iter__(self):						return iter(self.mbrs)
	#endregion
	#region factory
	''' once Members is frozen, factory function are disabled. negation (prefix with +) is shorthand for unfreeze. 
		Example. (-KK.apple).banana  => AttributeError: Member does not have attribute 'banana'
	'''
	# todo: simplify by using a dedicated Attrfab as a factory
	def __getitem__(self, idx):
		if self.frozen:						raise ValueError(idx)
		
		to_key = self._to_key
		idx = idx if not to_key else to_key(idx)
		out = self.__class__(idx, mbr_cls=self.get_index, prev_mbrs=self.mbrs)
		return out
	def __getattr__(self, attr):
		if hasattr(self.__class__, attr):	return getattr(self.__class__, attr)
		if self.frozen:						raise AttributeError(attr)
		
		# resolve output
		to_key = self._to_key
		attr = attr if not to_key else to_key(attr)
		attr = isstr(attr, fab=GetAttr)
		keys = (self.__class__, *self._keys, attr)
		out = self._cache and _D.cached_attr_getters.get(keys)
		out = out or self.__class__(attr, prev_mbrs=self.mbrs, cache=self._cache)
		self._cache and _D.cached_attr_getters.setdefault(keys, out)
		return out
	def freeze(self):
		''' once Members is frozen, factory function are disabled. negation (prefix with -) is shorthand for freeze. 
			Example. (-KK.apple).banana  => AttributeError: Member does not have attribute 'banana'
		'''
		return self.__class__(self,frozen=True)
	def unfreeze(self):
		''' once Members is frozen, factory function are disabled. negation (prefix with +) is shorthand for unfreeze. 
			Example. (-KK.apple).banana  => AttributeError: Member does not have attribute 'banana'
		'''
		return self.__class__(self,frozen=False)
	def reduce(self):		return self.mbrs
	__neg__,__pos__			= freeze, unfreeze
	__invert__				= reduce
	#endregion
	#region utils
	def has(self, obj, *pa, **ka):
		result = obj
		*mbrs, attr_access = self.mbrs
		for mbr in mbrs:
			result = mbr(result, *pa, **ka)
		result = attr_access.has(result)
		return result
	def add(self, key, getter_type=get_attribute):
		self.mbrs.append(self.coerce_key(key, getter_type))
	def pick(self, obj=miss, *pal0, pa=(), ka=map0, fill=miss, **ka0):
		# todo: pick should be a utility
		# todo: its most intuitive to apply extra pa,ka to depth-most mbr
		try:
			if len(self.mbrs) == 1:
				out = self.mbrs[0](obj, *pal0, **ka0)
			else:
				out = self.mbrs[0](obj, *pal0, *pa, **ka0, **ka)
				for mbr in self.mbrs[1:]:
					out = mbr(out, *pa, **ka)
			return out
		except fill != miss and Exception or ():
			return fill
	def iget(self, objs, fab=..., *_pa, pa=(),fabs=None, ka=map0,**_ka):
		vals = (self.get(obj,*pa or _pa,**ka or _ka) for obj in objs)
		vals = vals if not fabs else map(fabs,vals)
		vals = vals if not fab or fab is ... else fab(vals)
		return vals
	def __get__(self, obj, cls):
		# obj = cls if obj is None else obj
		# out = self.get(obj)
		
		out = self 
		if obj is not None:
			try:
				out = self.get(obj) if obj is not None else self
			except AttributeError:
				pass
		return out
	def put(self, obj, val, mbr=miss):
		''' Note: same as set_attrs above except not uniformly attr but any kind of mbr '''
		target = self.get(obj)
		if isstr(mbr):
			result = setattr(target, mbr, val)  # shortcut for most calls
		else:
			mbr = self.coerce_key(mbr)  # todo: get setting mbr
			result = mbr(target, val)
		return result
	def __s_et__(self, obj, cls):
		raise NotImplemented(f'Members.__set__({obj}, {cls})') # todo: fix for module reloading
	def attrs(self, keys, lkeys=(), *pa, **ka):
		''' create attr for each key in *keys* prefixed with self._keys. As in: [KK(self.keys+[k]) for k in keys] '''
		return attrs(keys, self._keys+[*lkeys], *pa, **ka)
	get = __call__			= pick
	set						= put
	#endregion
	...
class MembersFinal(MemberKeys, frozen=yes): pass

class HasMember(MemberKeys):
	__call__				= MemberKeys.has

class IMembersCapsule(MemberKeys):
	objs: object			= None
	#region utils
	def __init__(self, objs, *pa, **ka):
		self.objs = objs
		super().__init__(*pa, **ka)
	def iset(self, mbr, ival, **ka):
		'''	Usage: IAttr(corners).angle = [30, 60, 90] '''
		# ka = self.ka | ka
		if mbr in self.anno_keys:
			self.__dict__[mbr] = ival
		elif liketype(ival, IMembersCapsule):
			for obj in self.objs:
				val = ival.get(obj)
				self.set(obj, val, mbr, **ka)
		else:
			ivals = isiter(ival) and iter(ival) or rep(ival,'+')
			for obj in self.objs:
				val = next(ivals)
				self.set(obj, val, mbr, **ka)
	__setattr__				= iset
	__call__				= MemberKeys.iget
	#endregion
	...
IMembersCapsule.anno_keys	= {*ijoin((cls.__annotations__ for cls in (IMembersCapsule,)+IMembersCapsule.__bases__))}

class IMemberKeys(MemberKeys):
	#region utils
	def __init__(self, *_pa, evs=None, ev=None, **_ka):
		self.evs,self.ev = evs,ev
		super().__init__(*_pa, **_ka)
	def iget(self, objs, *pa, evs=None, ev=None, **ka):
		evs,ev = evs or self.evs, ev or self.ev
		# out = IMembersCapsule(objs, *pa+self.pa, **self.ka | ka) if not self.mbrs else super().iget(objs, *pa+self.pa, **self.ka | ka)
		out = (self.mbrs and super().iget or IMembersCapsule)(objs, *pa, **ka)	# todo: consider static fabricator IAttr instance as new UnRefCls
		out = out if not evs else map(evs,out)
		out = out if not ev else ev(out)
		return out
	__call__				= iget
	#endregion
	...

Member,Members,IMembers		= MemberKey,MemberKeys,IMemberKeys
KK,IKK=Attr,IAttr			= MemberKeys(cache=yes),IMemberKeys(cache=yes)
# HKK,LKK,TKK					= [IMemberKeys(cache=True,ev=ev) for ev in [set,list,tuple]]
Has,Has__					= HasMember(cache=yes), HasMember(cache=yes,to_key='__{}__'.format)
Members.anno_keys			= {*Members.__annotations__}
# Name,IName					= Attr,IAttr	# todo: eliminate
#endregion

#region obsolete
# obsolete infavor of RefsCls
class DataAccess:
	def __init__(self, name=None, **kws):
		self.name = name
	def renew(self, **kws):
		return self
	def __str__(self):
		return self.name
	@property
	def key(self):
		return self.name
	def get(self, data):
		''' get field series from a dataset corresponding to self.name '''
		result = data.get(self)
		return result
	def evaluate(self, data): raise NotImplementedError()
	def __add__(self, other):
		result = Evaluator(op.add, self, other)
		return result
	def __sub__(self, other):
		result = Evaluator(op.sub, self, other)
		return result
	def __mul__(self, other):
		result = Evaluator(op.mul, self, other)
		return result
	__call__ = get
	__repr__ = __str__

@dataclass
class _MemberFactory:
	# _reverse: bool = True
	def __getattr__(self, attr):
		return Member(attr)  #, _reverse=self._reverse)

Head = _MemberFactory()  #_reverse=False)
Tail = _MemberFactory()

class Iters:
	''' Class takes some iterable content and returns iterators of subcomponents when the iter is
		called with a subcoponent mbr. Use list constructor list(Iters()) or iter() function
		to resolve Iters() so that it can be indexed or traversed.
		Syntatic sugar equivalent of:
		map(lambda obj: obj.subitem, objs)  # or
		map(lambda obj: obj[0], objs)

		Usage:
		iobjs = Iters(objs)
		iobjs.subitem		# returns an iterator of subitem members for each obj
		iobjs[-1] 			# returns an iterator of the last item for each obj
		iobjs.subitem[-1]	# returns an iterator of the last item of the subitem member for each obj
	'''
	mbr_constraints = (getitem_only, getattr_only, other) = range(3)
	def __init__(self, content):
		self.content = content
		self._iter = iter(self.content)
	def __iter__(self):
		return self._iter
	def __next__(self):
		return next(self._iter)
	next = __next__
	def call(self, mbr):
		# todo
		import itertools
		if '':
			result = itertools.imap(mbr, self.content)
		if '':
			result = map(mbr, self.content)
		return result
	def __getitem__(self, index):
		return self._get_each(index, mbr_constraint=Iters.getitem_only)
	def __getattr__(self, key):
		return self._get_each(key, mbr_constraint=Iters.getattr_only)
	def _get_each(self, key, mbr_constraint=getitem_only):
		if isinstance(self.content, types.GeneratorType):
			result = Iters((item for item in self._gen_each(key, mbr_constraint)))
			return result

		if isinstance(key, Keys):
			mbr_constraint = Iters.other

		result = []
		for item in self.content:
			item_element = item
			if mbr_constraint == Iters.other:
				item = key.get(item)
			elif mbr_constraint == Iters.getitem_only:
				item = item[key]
			elif mbr_constraint == Iters.getattr_only:
				item = getattr(item, key)
			result.append(item)

		result = Iters(result)
		return result

	def _gen_each(self, key, mbr_constraint, iterator_result=False):
		if isinstance(key, Keys):
			mbr_constraint = Iters.other

		for item in self.content:
			item_element = item
			if mbr_constraint == Iters.other:
				item = key.get(item)
			elif mbr_constraint == Iters.getitem_only:
				item = item[key]
			elif mbr_constraint == Iters.getattr_only:
				item = getattr(item, key)
			yield item

def mirror_index(index):
	if isinstance(index, int):
		index = -index - 1
	else:
		cls = type(index)
		index = cls(-index.start -1, -index.stop -1, -index.step)
	return index

class Sampler:
	''' Represents a slice for a named data series.  Reinterprets slice inputs as interpolation rather than a range.
		step: interpolation step size. default=1.
		stop: Number of interpolations. default=1.
		start: where to start interpolation. default=0.
	'''
	def __init__(self, ind, reverse=False):
		self.number_index = type(ind) is int
		self.reverse = reverse
		
		# out of bound int index is processed as an empty list rather to avoid exceptions
		ind = slice(ind,ind+1 or None) if type(ind) is int else ind  # coerce to slice
		self.size = ind.stop or 1
		self.ind = Sampler.decode(ind)
		if self.reverse:
			self.ind = mirror_index(self.ind)
	@property
	def mirror(self):
		result = Sampler(self.ind, reverse=True)
		return result
	def __str__(self):
		if isinstance(self.ind, slice):
			result = f'[{self.ind.start or ""}:{self.ind.stop or ""}:{self.ind.step or ""}]'
		else:
			result = f'[{self.ind}])'
		return result
	@staticmethod
	def decode(index):
		''' given a slice for interpolation return a normal slice that produces the same result '''
		cls = type(index)
		start = index.start or 0
		step = index.step or 1
		stop = (index.stop or 1) * step  # as sample, interpret stop as total samples
		result = cls(start,stop,step)
		return result
	def get(self, data, default=0., fill=True):
		''' get field(s) from a dataset corresponding to self.ind slice '''
		result = data[self.ind]
		if fill and len(result) < self.size:
			result[10000:] = [default] * (self.size - len(result))
		if self.number_index:
			result = result[0]
		return result
	__repr__ = __str__
	__call__ = get

Window = Sampler
#endregion
