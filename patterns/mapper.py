import re
from copy import copy

from pyutils.utilities.clsutils import annovars, init_cls
from pyutils.utilities.maputils import *
from pyutils.utilities.iterutils import *
from pyutils.utilities.callutils import *
from pyutils.utilities.strutils import *
from pyutils.patterns.props import *

#region map defs
class _Args:
	def __class_getitem__(cls, key):
		return getattr(cls, key)

class DefsMaps:
	getter_precedence	= '__getitem__ __call__ __getattribute__'.split()
	log = None
	prio_a,prio_b		= 0, 1
	
	class on_miss(_Args):
		''' Presets for arg reshaping handlers in fillmap/fullmap(on_miss): '''
		key				= lambda mapped,key,*pa,fill=miss,**ka: key
		call			= lambda mapped,key,*pa,fill=miss,**ka: fill()
		full			= lambda mapped,key,*pa,fill=miss,**ka: fill
		fill			= lambda mapped,key,*pa,fill=miss,**ka: mapped.setdefault(key,fill)
		fab				= lambda mapped,key,*pa,fill=miss,**ka: fill(key,*pa,**ka)
		kargs			= lambda mapped,key,*pa,fill=miss,**ka: fill(*pa,**ka, mapped=mapped,key=key,fill=fill)
		error			= lambda mapped,key,*pa,fill=miss,**ka: throw(KeyError(key))
		
	for func_name, func in on_miss.__dict__.items():
		if func_name.startswith('_'): continue
		func.__qualname__ = f'DefsMaps.on_miss.{func_name}(...)'

DDMaps = DD				= DefsMaps
#endregion

#region map utils  # todo: relo to maputils
def _get_class_fields(inst):
	result = [name for name, var in inst.__class__.__dict__.items() if var.__class__.__name__ == 'member_descriptor']
	return result

def combine_maps(map_a, map_b,on_overlap=False,**ka):
	if not map_a or not map_b: return map_a or map_b
	
	# handle overlapping
	if not on_overlap:
		kk_a,kk_b = set(map_a), set(map_b)
		overlap = kk_a & kk_b
		overlap and throw('overlap {} found between {}:{} & {}'.format(overlap,...,kk_a,kk_b))
	# resolve output
	result,_ = under,over = on_overlap == DD.prio_a and [map_b,map_a] or [map_a,map_b]
	under.update(over)
	return result

def getter_of_items(items, getter=None, getter_precedence=None):
	getter_precedence = getter_precedence or DD.getter_precedence
	if not getter:
		getter = first((getter for getter in getter_precedence if hasattr(items, getter)))
	if isinstance(getter, str):
		getter = getattr(items, getter)
	return getter

def intos_of(*_intos,intos=None):
	''' produce ->intos layers of value converters from arbitrary inputs '''
	intos_out = recalls(map(into_of,intos or _intos))
	return intos_out

def intostr(into,s=None,**ka):
	# resolve inputs
	zinto = into if istype(into,zip) else zinto_of(into,**ka)
	if s is None:	return intostr.__get__(zinto)
	# resolve output
	s_out = s
	for s_in,s_to in zinto:
		s_out = s_out.replace(s_in,s_to)
	return s_out

def remap_of_rrx(*pa_kk, rx_ns=RXCls, **ka):
	# resolve output
	def intorrx_of(rrx_in=None,rrx_to=None,zrrx_into=None):
		zrrx_into = tuple(zrrx_into or zip(rrx_in,rrx_to))
		def intorrx_of_s(s):
			s_out = s
			for rx_in,rx_to in zrrx_into:
				s_out = rx_ns.sub(rx_in,rx_to,s_out)
			return s_out
		return intorrx_of_s
	# resolve staged output
	result = remap_of_into(*pa_kk, **ka, fab_into=intorrx_of)
	return result

def hash_any(obj,coerce=str):
	try:
		sig = hash(obj)
	except TypeError:
		sig = hash(coerce(obj))
	return sig

def hashable_of(obj,coerce=repr):
	obj_out = obj
	try:
		hash(obj)
	except TypeError:
		obj_out = coerce(obj_out)
	return obj_out

def init_factorys(sbj,factorys:list|dict|str='_factorys',slotted=no):
	''' find & apply val output of *factorys* to *sbj* '''
	# todo: detect slotted automatically 
	# todo: collect dataclass fields & other sensible conventions
	# resolve factorys for suj
	factorys = (
		getattr(sbj,factorys,None)	if isstr(factorys) else
		factorys(sbj)				if iscall(factorys) else
		factorys )				#	is iterable
	
	# apply factorys if any
	if factorys:
		vars = [(attr,factory()) for attr,factory in zof(factorys)]
		if slotted:
			for attr,val in vars:
				setattr(sbj,attr,val)
		else:
			sbj.__dict__.update(vars)

combine					= combine_maps
getrof					= getter_of_items
#endregion

#region tomap devcs
@dataclassown
class IntoKKMapper:
	''' migrate kk_in -> kk_to for mappings passed to into() '''
	kk_in: strs
	kk_to: strs			= None
	#region defs
	iter = i			= property(iter)
	rev=reverse			= property(reversed)
	keys_in				= property(lambda self: self.kk_in)
	keys_to				= property(lambda self: self.kk_to)
	#endregion
	#region form
	def __post_init__(self):
		self.kk_in = lss(self.kk_in)
		self.kk_to = self.kk_to and lss(self.kk_to)
		if dict in self.__class__.__bases__:
			super().__init__(zip(self.kk_in, self.kk_to))
	def __repr__(self):		return 'IntoKK({}, {})'.format(self.kk_in,self.kk_to)
	def __iter__(self):
		yield self.kk_in
		yield self.kk_to
	def __reversed__(self):
		yield self.kk_to
		yield self.kk_in
	def keys(self):				return iter(self.kk_in)
	def values(self):			return iter(self.kk_to)
	def items(self):			return zip(self.kk_in,self.kk_to)
	#endregion
	#region forms
	def get(self, key, *fill):
		fill, *_ = *fill, key
		result = super().get(key, fill)
		return result
	def into(self, obj, fab=True, **ka):
		if isstr(obj):
			into_of_s = self.__dict__.get('_intostr') or self.__dict__.setdefault('_intostr',intostr(self))
			obj_out,fab = into_of_s(obj,**ka), istrue(fab,put=None)
		elif isdict(obj):
			obj_out,fab = iintomap(obj,**ka), istrue(fab,put=dict)
		else:
			throw(obj)
		obj_out = obj_out if not fab else fab(obj_out)
		return obj_out
	
	__getitem__			= get
	__call__			= into
	#endregion
	...

@dataclass
class ToMap:
	to_value: callable	= None
	to_key: callable	= None
	at_key_out: bool	= True
	to_item: callable	= None
	#region form
	def of(self, mapped):
		result = tomap(mapped, self.to_key, self.to_value, self.to_item, at_key_out=self.at_key_out)
		return result
	
	__call__			= of
	#endregion
	...

# todo: made obsolete by pyutils.structures.accessors
class Gettable:
	''' A set of items and its associated getter '''
	def __init__(self, items=None, getter=None):
		self.getter = getrof(items, getter)
		self.items = items
	def get(self, key, *args, **kargs):
		result = self.getter(key, *args, **kargs)
		return result
	# member aliases
	__call__			= get
#endregion

#region map ifaces
class Mapped:
	#region defs
	_to_pick			= exact(lambda obj: getattr(obj,'pick',None))
	set = setitem		= property(lambda self: self.__setitem__)  # todo: maybe __init_subclass__ can do this statically
	iitems				= property(lambda self: self.items())
	ikeys				= property(lambda self: self.keys())
	ivals = ivalues		= property(lambda self: self.values())
	# set					= noop
	#endregion
	#region utils
	_factorys			= None
	def __init_subclass__(cls, attrs:iter=(), vals:list|object=None, **vars):
		vars_anno = cls.__annotations__ and annovars(cls)
		vars_attr = mapeach(attrs,vals)
		vars_out = vars_anno|vars_attr|vars
		init_cls(cls, **vars_out)
		...
	def __init__(self, *pa, **ka):
		self._factorys and init_factorys(self)
		super().__init__(*pa,**ka)
	@classmethod
	def map_of(cls, *items_pa, keys=None, values=None, **ka):
		''' in this module a map is composed of 2 parts a set of keys and an item accessor
			item accessor is usually composed of a set of items and a getter
		'''
		# todo: replace with a maputil
		# resolve args:  => keys,values
		if items_pa:
			if None is keys is values:
				keys, values, *items_pa = items_pa
			elif keys is None and values is not None:
				keys, *items_pa = items_pa
			elif keys is not None and values is None:
				items, *items_pa = items_pa
				values = map(items.__getitem__, keys)
		
		# resolve inputs: keys,values => items
		if keys and values:
			keys, values = isstr(keys, fab=str.split), isstr(values, fab=str.split)
			items_pa = zip(keys, values), *items_pa
		
		# resolve output
		out = cls(*items_pa, **ka)
		return out
	def remap(self, to_pick=_to_pick,**ka):
		''' replace each key which ... '''
		raise NotImplementedError()
	def reeval(self, eval, *pa, replace=no, **ka):
		''' replace each key,val pairs where ... '''
		keep = 'todo'
		replace == 1 and self.replace(**ka)
		reeval(self, eval,*pa, **ka)
		replace in (-1,2) and self.replace(**ka)
		return self
	def union(self,other):				return self.__class__(self,**other)
	def runion(self,other):				return self.__class__(other,**self)
	alias				= aliasremap
	replace				= replace_map_picks
	__or__				= union
	__ror__				= runion
	has					= dict.__contains__
	#endregion
	...

class MapBase:  # todo: obsolete favoring Mapped
	''' Map a sequence of keys to an existing item accessor.  generate mapping statically in init. '''
	#region defs
	__call__			= property(lambda self: self.__getitem__)  # todo: maybe __init_subclass__ can do this statically
	set=setitem			= property(lambda self: self.__setitem__)  # todo: maybe __init_subclass__ can do this statically
	iitems				= property(lambda self: self.items())
	ikeys				= property(lambda self: self.keys())
	ivalues				= property(lambda self: self.values())	#region forms
	#endregion
	#endregion
	#region forms
	# todo: infuse, effuse
	def __init__(self, *items_pa, keys=None, values=None, **ka):
		''' in this module a map is composed of 2 parts a set of keys and an item accessor
			item accessor is usually composed of a set of items and a getter
		'''
		# resolve => keys,values
		if items_pa:
			if None is keys is values:
				keys, values, *items_pa = items_pa
			elif keys is None and values is not None:
				keys, *items_pa = items_pa
			elif keys is not None and values is None:
				items, *items_pa = items_pa
				values = map(items.__getitem__, keys)
		
		# resolve; keys,values => items
		if keys and values:
			keys, values = isstr(keys, fab=str.split), isstr(values, fab=str.split)
			items_pa = zip(keys, values), *items_pa
		
		# init
		super().__init__(*items_pa, **ka)
	#endregion
	...

class ValMapperBase:  # todo: obsolote
	''' Map a sequence of keys to an existing item accessor.  Serves to dynamically generate
		ordered values or items from reference dict or value generator.  Implements python
		mapping interface. ie: func(**mapping)
	'''
	#region forms
	def __init__(self, items=None, getter=None, keys=None):
		''' in this module a map is composed of 2 parts a set of keys and an item accessor
			item accessor is usually composed of a set of items and a getter
			:param items:
			:param getter:
			:param keys:
		'''
		self.getter = getrof(items, getter)
		
		if isinstance(keys, str):
			keys = lstrs(keys)
		if keys is None:
			if 'getattr' in self.getter.__name__:
				keys = _get_class_fields(items)
			else:
				keys = getattr(items, 'keys', None)()
		self._keys = list(keys)
		self.is_enumerable = len([key for key in self._keys if isinstance(key, int)]) == 0
	def __len__(self):			return len(self._keys)
	#endregion
	#region utils
	def __getitem__(self, key):
		if isinstance(key, int) and self.is_enumerable:
			key = tuple(self._keys)[key]  # todo: itertools.islice
		result = self.getter(key)
		return result
	def keys(self):		return self._keys
	def values(self):	return (self.getter(key) for key in self._keys)
	def items(self):	return ([key, self.getter(key)] for key in self._keys)
	__call__			= __getitem__
	#endregion
	...

class FullMapper:
	''' Behavior emulates a dict or map that is already full with all possible keys.
		On key miss an instance will either returns a static value or generate the value with a function.
		By default an instance DOES store the missing key.  So __contains__(key) can be True
		after returning a value the missing key.
	'''
	#region defs
	_defs				= DD.on_miss
	fill 				= miss
	on_miss				= None
	store_fill			= False
	#endregion
	#region forms
	def __init_subclass__(cls, on_miss=on_miss,**ka):
		infuse(cls,ka)
		on_miss and setattr(cls,'on_miss',staticfunc(on_miss,'on_miss'))
	def __init__(self, fill=miss, items=(), on_miss=None, store_fill=None, **_items):
		''' ctor
			:param fill: defines return value when getitem misses; Accepts either default value or __missing__ func
			:param items: items passed to underlying mapping/coll along with ka
			:param on_miss: handler for *fill* and key. Can be user defined or a str: put,key,call,fab,kargs,error
			:param store_fill: when key is missing should default result be stored. default=True
			
			Presets for arg reshaping handlers in fillmap/fullmap(on_miss):
			key				= lambda mapped,key,*pa,fill=miss,**ka: key
			call			= lambda mapped,key,*pa,fill=miss,**ka: fill()
			full			= lambda mapped,key,*pa,fill=miss,**ka: fill
			fill			= lambda mapped,key,*pa,fill=miss,**ka: mapped.setdefault(key,fill)
			fab				= lambda mapped,key,*pa,fill=miss,**ka: fill(key)
			kargs			= lambda mapped,key,*pa,fill=miss,**ka: fill(*pa,**ka, mapped=mapped,key=key,fill=fill)
			error			= lambda mapped,key,*pa,fill=miss,**ka: throw(KeyError(key))
		'''
		if fill is not miss or on_miss is not None:
			self.fill = fill
			self.on_miss = staticfunc(
				DD.on_miss.call			if on_miss is None and callable(fill) else
				DD.on_miss.fill			if on_miss is None or on_miss is False else
				on_miss					if callable(on_miss) else
				DD.on_miss[on_miss],
				name='on_miss',
			)
			if store_fill is not None:  # if is None will keep static class default; todo: remove. this is now redundant
				self.store_fill = store_fill
		super().__init__(items, **_items)
	def __missing__(self, key, *pa, **ka):
		''' key is not in this collection. Return result of *on_miss*(self,*key*,fill=*fill*). '''
		if self.on_miss is miss: raise KeyError(key)
		
		# handle missing key
		on_miss = self.on_miss
		result = on_miss(self, key, *pa, **ka, fill=self.fill)
		if self.store_fill:		# todo: remove. this is now redundant
			self[key] = result
		return result
	#endregion
	...

class FullLister(FullMapper):
	''' augments lists with call to __missing__ function for out-of-range indices '''
	#region utils
	def __getitem__(self, index):
		''' return self[index]. in case of failure return self.__missing__(index). '''
		try:
			result = super(FullMapper, self).__getitem__(index)
		except:
			result = self.__missing__(index)
		return result
	#endregion
	...

class SetMapper:
	''' map a set of values as keys of a set given the function *to_key* '''
	#region defs
	# _to_key				= staticmethod(lambda item: item)
	_to_key				= None
	#endregion
	#region forms
	def __init_subclass__(cls, to_key=_to_key, **ka):
		infuse(cls,**ka, _to_key=to_key)
		cls.has_key = getattr(super(SetMapper),'__contains__',None)
	def __init__(self, items=(), to_key=None, **ka):
		''' ctor
			:param items:    sequence of items (and/or keys) in set
			:param to_key:   converter function which gets a key for each item
			:param ka_items: items/parameters passed unmodified to parent class
		'''
		super().__init__(**ka)
		self.to_key = to_key or self._to_key
		items and list(map(self.add, items))
	def __iter__(self):			return iter(self.values())
	#endregion
	#region utils
	def has_item(self, item=miss,key=miss):
		key = (
			throw(ValueError)	if item is miss != key is miss else  # item & key must include exactly 1 miss
			key					if key is not miss else
			item				if not self.to_key else
			self.to_key(item) )
		out = super().__contains__(key)
		return out
	# def get(self, key, fill=None):
	# 	out = super().get(key,fill)
	# 	return out
	def add(self, val):
		key = val if not self.to_key else self.to_key(val)
		super().__setitem__(key,val)
		return val
	def update(self,vals,delta=1,**ka):
		cxpas(self.add,vals, delta=delta)
		ka and super().update(ka)
	def combine(self, items):
		_ = list(map(self.add, items))
		return self
	def union(self, items):
		out = copy(self)
		out += items
		return out
	def discard(self, item, *fill):
		key = item if not self.to_key else self.to_key(item)
		out = self.pop(key, *fill)
		return out
	def subtract(self, items, *fill):
		_ = list(map(self.discard, items))
		return self
	def difference(self, items):
		out = copy(self)
		out -= items
		return out
	
	__contains__ = has	= has_item  # default has(_item) checks for item; use has_key for usual dict.__contains__
	__iadd__			= combine
	__add__				= union
	__isub__			= subtract
	__sub__				= difference
	#endregion
	...

class OrderMapper:
	#region forms
	def __init__(self, *pa, fifo=True, **ka):
		self.fifo = fifo
		self._order = []
		self._omitted = set()
		super().__init__(*pa, **ka)
	#endregion
	#region access
	def keys(self):
		self._omitted and self._omit()
		ikeys = not self.fifo and reversed(self._order) or self._order
		return ikeys
	def values(self):		return map(self.__getitem__,self.keys())
	def items(self):		return zip(self.keys(), map(self.__getitem__,self.keys()))
	__iter__			= values
	#endregion
	#region modify
	def __setitem__(self, key, value):
		''' set key to value and update order. '''
		if key in self:
			self.pop(key)
			self._omit()
		self._order.append(key)
		super().__setitem__(key,value)
	def pop(self, key=nothing):
		key = key if key is not nothing else self._order[not self.fifo and -1 or 0]
		result = super().pop(key)
		self._omitted.add(key)  # do lazy omission of key from _order; pop is still O(1); __iter__ is still O(n)
		return result
	def clear(self):
		super().clear()
		self._order.clear()
		self._omitted.clear()
	def _omit(self):
		''' remove previously omitted keys from ordered seq.
			Notes:
			omit = O(n) complexity so take care to invoke only at the times it is needed or else
			complexity could compound to O(n^2) instead of ~O(2n)
			add,pop = O(1);  keys,vals,__iter__ = ~O(2n);
		'''
		if self._omitted:
			self._order = [key for key in self._order if key not in self._omitted]
			self._omitted.clear()
		return self._order
	#endregion
	...

class ValsMapper:
	''' map each key to a value list '''
	#region defs
	_getter					= None  # valid getters: 'last', 'first', None or callable
	_fab_vals				= list
	_prio_add_val			= tss('append  add')
	#endregion
	#region forms
	def __init_subclass__(cls, fab_vals=None,add_val=None):
		cls._fab_vals,cls._add_val = fab_vals or ValsMapper._fab_vals, staticmethod(add_val or ValsMapper._add_val)
	def __init__(self, *pa, fab_vals=None, add_val=None, getter=None, **ka):
		self.fab_vals = exact(fab_vals or self._fab_vals)
		iadd_val = (
			ss(add_val)		if isstr(add_val) else
			add_val			if isiter(add_val) else
			[add_val]		if add_val else
			self._prio_add_val or ()  )
		self.add_val = keepfirst(iscall(add) and add or getattr(self.fab_vals.func,add,None) for add in iadd_val)
		getter = getter or self._getter
		if isinstance(getter, str):
			getter = self.__dict__[getter]
		elif hasattr(getter, '__call__'):
			getter = getter(self)
		self.getter = getter
		super().__init__()
		pa and self.update(*pa)
		ka and self.update(**ka)
	#endregion
	#region access
	def __getitem__(self, key, *pa, **ka):
		out = (self.getter or super()).__getitem__(key, *pa, **ka)
		return out
	def items(self, *pa, **ka):
		out = (self.getter or super()).items(*pa, **ka)
		return out
	def values(self, *pa, **ka):
		out = (self.getter or super()).values(*pa, **ka)
		return out
	def get_first(self, key, fill=None):
		if key not in self:		return fill
		out = super().__getitem__(key)
		
		if len(out) == 0:		return fill
		out = super().__getitem__(key)[0]
		return out
	def get_last(self, key, fill=None):
		if key not in self:		return fill
		out = super().__getitem__(key)
		
		if len(out) == 0:		return fill
		out = super().__getitem__(key)[-1]
		return out
	
	# def set(self, key, val=...,vals=...,coll=...):
	def add(self, key, val=...,vals=...,coll=...):
		''' insert one or more *val(s)* at *key*. single item entry is the default. '''
		if coll is not ...:
			super().__setitem__(key,coll)
			return
		
		if key not in self:
			fab_vals = self.fab_vals
			super().__setitem__(key, fab_vals())
		coll = result = super().__getitem__(key)
		if val is not ...:
			self.add_val(coll,val)
			result = val
		elif vals is not ...:
			for val in vals:
				self.add_val(coll,val)
		return result
	def merge(self, key, vals):
		''' insert multiple *vals* into *key* '''
		return self.add(key,vals=vals)
	# def sets(self, key, vals):	self.set(key,vals=vals)
	def update(self, vals_items=miss,val_items=miss, coll_items=miss,keys=miss,**_items):
		''' update items via a select format method
			:param vals_items: merge to each key given iterable like [(key,vals),..]. the default for **kwargs *_items*
			:param val_items:  add to each key given iterable like [(key,val),..].  
			:param coll_items: set each key given iterable like [(key,vals),..]
		'''
		# resolve inputs
		vals_items,genvals = _items or vals_items, keys and genof(call=self.fab_vals)
		vkey,ival = (
			('vals',vals_items)			if vals_items is not miss else
			('vals',lzip(keys,genvals))	if       keys is not miss else
			('val',  val_items)			if  val_items is not miss else
			('coll',coll_items)			if coll_items is not miss else
			throw(ValueError()) )
		ival = isdict(ival,call='items')
		# if isinstance(items, dict):
		# 	items = items.items()
		
		# update contents
		for key,val in ival:
			self.add(key,**{vkey:val})
		return self
	__setitem__				= add
	getl, getr				= get_first, get_last
	#endregion
	...

class Remapper:
	''' apply a function *to_key* to a map which transforms keys in set & get operations.
		alters the output values for the item getter and iterator functions: get,  __getitem__, values, & items
		use super to access underlying unmodified values.
	'''
	#region forms
	# todo: fully implement optional *to_val*
	def __init_subclass__(cls, to_key=None,to_val=None,**ka):
		cls.to_key = exact(to_key)
		cls.to_val = exact(to_val)
		ka and infuse(cls,ka)
	def __init__(self, *pa, to_key=None, to_val=None, _items=(), **ka):
		# init bases
		to_key and setattr(self,'to_key',exact(to_key))
		to_val and setattr(self,'to_val',exact(to_val))
		if pa and iscall(pa[0]) and not self.to_key:
			self.to_key,pa = exact(pa[0]), pa[1:]
		if pa and iscall(pa[0]) and not self.to_val:
			self.to_val,pa = exact(pa[0]), pa[1:]
		if pa and isiter(pa[0]) and not _items:
			_items,*pa = pa
		assert self.to_key
		super().__init__(*pa,**ka)
		
		# convert and reapply initial items
		items_in = lj(_items,[(k,dict.pop(self,k)) for k in [*self.keys()]])
		items_in and self.update(items_in)
	#endregion
	#region access
	def __contains__(self, key):			return self.to_key(key) in self
	def __setitem__(self, key, *pa, **ka):	return super().__setitem__(self.to_key(key), *pa, **ka)
	def __getitem__(self, key, *pa, **ka):	return super().__getitem__(self.to_key(key), *pa, **ka)
	def get(self, key, *pa, **ka):			return super().get(self.to_key(key), *pa, **ka)
	def pop(self, key, *pa, **ka):			return super().pop(self.to_key(key), *pa, **ka)
	def update(self, items):
		for key, val in zof(items):
			self[key] = val
	#endregion
	...

class ValRemapper:
	''' apply a function *to_value* to a map which transforms mapped values in get operations.
		alters the output values for the item getter and iterator functions: get,  __getitem__, values, & items
		use super to access underlying unmodified values.
	'''
	#region defs
	class gives:
		key, value, item, item_args = 'key', 'value', 'item', 'item_args'
		arg_inds = {key:0, value:1, item:slice(None, None)}
		give_args = classmethod(lambda cls, give, item_args: item_args[cls.arg_inds[give]])
	#endregion
	#region forms
	def __init__(self, items=None, to_value=None, given=gives.value, **kargs):
		''' ctor
			:param items:
			:param to_value:
			:param given:
		'''
		self.to_value = to_value
		self.given = given == self.gives.item_args and self.gives.item or given
		self.unpack = given == self.gives.item_args
		super().__init__(items or {}, **kargs)
	#endregion
	#region access
	def __getitem__(self, key):
		# result/value for key not needed if only key is given
		result = self.given != self.gives.key and super().__getitem__(key)
		if self.to_value:
			item_args = key, result
			given_item = self.gives.give_args(self.given, item_args)
			result = self.to_value(*given_item) if self.unpack else self.to_value(given_item)
		return result
	def values(self):
		result = given_items = getattr(super(), self.given+'s')()
		if self.to_value:
			result = (self.to_value(*v) if self.unpack else self.to_value(v) for v in given_items)
		return result
	def items(self):
		result = given_items = getattr(super(), self.given+'s')()
		if self.to_value:
			result = ((k, self.to_value(*v) if self.unpack else self.to_value(v)) for k, v in zip(self.keys(), given_items))
		return result
	#endregion
	...

class CollMapper:
	''' Augment an items sequence with map functionality and preserve the underlying structure.
		Note: ItemsMap.__getitem__ is inefficient.  Adding a hash of keys is a possible O(1) solution.
	'''
	#region defs
	to_key,to_val		= None,None
	iter				= property(lambda self: self._obj if not self._to_iter else self._to_iter(self._obj))
	#endregion
	#region forms
	def __init__(self, items,to_key=None,to_val=None,to_item=True,to_iter=None):
		self._obj = items
		self._to_key = to_key
		self._to_val = to_val
		self._to_item = to_item
		self._to_iter = to_iter
	#endregion
	#region access
	def __len__(self):		return len(self._obj)
	def keys(self):			return self._obj if not self._to_key  else map(self._to_key, self.iter)
	def values(self):		return self._obj if not self._to_val  else map(self._to_val, self.iter)
	def items(self):		return self._obj if not self._to_item else map(self._to_item,self.iter)
	def get_paired(self, search, count=0, default=miss, search_keys=True):
		''' find value for *count* occurrence of *search* key. Inefficient O(n) performance similar to list.index. '''
		# prepare items iterable
		iitems = self.iter
		if 0 > count:
			iitems = reversed(self._obj)
			count = ~count
		
		# find value for *count* occurrence of *search_key*
		result = miss
		for k,v in iitems:
			found,paired = (k,v) if search_keys else (v,k)
			if search == found:
				if count == 0:
					result = paired
					break
				count -= 1
				
		if result is miss:
			raise KeyError()
		return result
	__iter__			= keys
	__getitem__			= get_paired
	#endregion
	...

class AttrEnum:
	''' Remaps attribute assignment to dict assignment such that after assignment
		attrenum.var == dict(**attrenum)['var'].  It maintains attribute vars distinct from dict items
		within instance.
		
		One significant challenge to allowing attr access pass-through to item access is that internally the
		class instance still needs to assign member vars in a way that is simple and unambiguous.
		Attribute assignment (typically via self.__dict__.__setitem__) is used if one of the following
		conditions is satisfied:
		- an existing instance attribute
		- assignments within the constructor
		- the attr name matches the self._is_attr regular expression
		
		If none of these conditions are satisfied when assigning an attribute the variable is assigned in dict
		contents via self.__setitem__.
	'''
	#region defs
	is_attr				= '^_\w+_$'  # apply as attribute when matching this pattern
	_add_attr			= True
	_dir				= the_dir_cacher
	#endregion
	#region forms
	def __init_subclass__(cls, is_attr=None,kk_item=None,**ka):
		is_attr and ka.setdefault('is_attr',is_attr)
		ka and infuse(cls,ka)
	def __init__(self, *pa, is_attr=None, **ka):
		is_attr = is_attr or self.is_attr
		self.is_attr = is_attr if isstr(is_attr) or isntiter(is_attr) else frozenset(ss(is_attr)).__contains__
		self.__dict__.update((k,ka.pop(k)) for k,v in list(ka.items()) if self.is_attr_of_key(k))
		super().__init__(*pa,**ka)
		self._add_attr = None  # after this point use __getitem__ where possible
	#endregion
	#region access
	def is_attr_of_key(self, key):
		_ = self._dir  # initializes the property; can probably be handled within dir_cacher
		add_attr = (
			yes									if key in self._dir_set else
			no									if self.is_attr is None else
			self.__dict__['is_attr'](key)		if iscall(self.is_attr) else  # sidestep func->method transform
			bool(re.search(self.is_attr,key))	if isstr(self.is_attr) else
			throw(ValueError(self.is_attr)) )
		return add_attr
	def _real_setr_of_key(self, key):
		add_attr = self.is_attr_of_key(key)
		# add_attr = self._add_attr if isbool(self._add_attr) else self.is_attr_of_key(key)
		return add_attr and super().__setattr__ or self.__setitem__
	def setitem(self, key, val):
		''' set item unambiguously without conditional placement '''
		return self.__setitem__(key,val)
	def setattr(self, attr, val):
		''' set attr unambiguously without conditional placement '''
		return super().__setattr__(attr,val)
	def __setattr__(self, attr, val):
		add_attr = self._add_attr if isbool(self._add_attr) else self.is_attr_of_key(attr)
		setr = add_attr and super().__setattr__ or self.__setitem__
		return setr(attr, val)
	def __getattr__(self, attr):
		try:
			attr == 'missing' and print('AttrEnum[%s]' % attr, str(self))  # todo: can this be removed
			return self.__getitem__(attr)
		except Exception as exc:
			raise AttributeError(exc)
	#endregion
	...

class Gettable:
	''' getable abstract interface '''
	def __init_subclass__(cls, err=yes,**ka):
		cls._err = err
		super().__init_subclass__(**ka)
	def __call__(self,*pa,**ka):
		out = (self._err and self.__getitem__ or self.get)(*pa,**ka)
		return out

# class VarsMapperProto:
# 	#region access
# 	# todo: should satisfy isinstance(self,dict) == True without instancing another dict
# 	# todo: what are simpler ways to achieve this?  eval ...
# 	#       see alternative strategy below ...
# 	def __repr__(self):					return self.__dict__.__repr__()
# 	def __len__(self):					return self.__dict__.__len__()
# 	def __iter__(self):					return self.__dict__.__iter__()
# 	def __getitem__(self,key):			return self.__dict__[key]
# 	def __setitem__(self,key,val):		return self.__dict__.__setitem__(key,val)
# 	def __delitem__(self,key):			return self.__dict__.__delitem__(key)
# 	def get(self,*pa,**ka):				return self.__dict__.get(*pa,**ka)
# 	def clear(self):					return self.__dict__.clear()
# 	def keys(self):						return self.__dict__.keys()
# 	def values(self):					return self.__dict__.values()
# 	def items(self):					return self.__dict__.items()
# 	def union(self,other):				return self.__dict__ | other
# 	def runion(self,other):				return other | self.__dict__
# 	def update(self,*pa,prio_old=no,**ka):
# 		if prio_old:
# 			pa = ((k,v) for k,v in dict(*pa,**ka).items() if k not in self),
# 			ka = map0
# 		self.__dict__.update(*pa,**ka)
# 		return self
# 	__or__,__ror__		= union,runion
# 	#endregion
# 	...
# class VarsMapper:
# 	#region access
# 	# todo: investigate slots as solution
# 	def __init__(self,*pa,**ka):
# 		ka = self.__dict__|ka
# 		del self.__dict__
# 		self.__dict__ = self
# 		super().__init__(*pa,**ka)
# 	def update(self,*pa,prio_old=no,**ka):
# 		if prio_old:
# 			pa = ((k,v) for k,v in dict(*pa,**ka).items() if k not in self),
# 			ka = map0
# 		super().update(*pa,**ka)
# 		return self
# 	#endregion
# 	...
GetIx					= Gettable
#endregion

#region map implementations
class OrderSetMapper(SetMapper,OrderMapper): pass
class FillMapper(FullMapper, store_fill=on): pass
class FillKeyMapper(FillMapper, on_miss=DD.on_miss.key): pass
class FullKeyMapper(FullMapper, on_miss=DD.on_miss.key): pass
class FillSetMapper(SetMapper, FillMapper): pass
class FullSetMapper(SetMapper, FullMapper): pass

KeyMapper				= FullKeyMapper
KeyRemapper				= Remapper
#endregion
