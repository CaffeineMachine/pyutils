''' A collection of classes that encapsulate common or fundamental variable assignment behaviors '''

import re

from datetime import datetime, timedelta
from dataclasses import dataclass

# from pyutils.defs.worddefs import *
from pyutils.defs.funcdefs import throw
from pyutils.defs.itemdefs import infuse
from pyutils.defs.typedefs import *
from pyutils.utilities.alyzutils import func_arg_keys
from pyutils.utilities.callbasics import redef, bools, irev, hss
from pyutils.utilities.clsutils import DDClsU, dataclassown, thecls

#region prop defs
class DefsProp:
	the_attr_default		= DDClsU.the_attr_default
DDProp = _D					= DefsProp
#endregion

#region prop utils
# todo: use maputils or accessor instead or RELO
def attrgetter(*pal,par=()):
	def call_post(*pa):
		return getattr(*pal,*pa,*par)
	return call_post
#endregion

#region prop forms
def islazyattr(*pa, cls=miss, attr=miss, **ka):
	''' query if *attr* is still lazily assigned. handles attr so as not use thus triggering its assignment. '''
	# resolve inputs
	assert sum(map(isntmiss,[*pa,cls,attr])) in (1,2), f'too many or too few args. {locals()}'
	cls,*pa = (
		pa							if cls  is miss and pa and isntstr(pa[0]) else
		irev(pa)					if cls  is miss and pa and isstr(pa[0]) else
		(cls,*pa) )
	attr,*pa = pa or [attr] 
	obj = out = (
		islazyattr.__get__(attr)	if cls  is miss else
		islazyattr.__get__(cls)		if attr is miss else
		cls.__dict__.get(attr,miss) )
	
	# process obj given kwargs
	if miss not in (cls,attr,obj):
		is_lazy = out = isinstance(obj, LazyProp|AttrFab)
		if ka:
			out = isthen(is_lazy,out,**ka)
	return out
#endregion

#region prop devcs
# todo: reevaluate classes and refactor as needed
# todo: clarify usage and define unittests

@dataclassown
class FuncProp:
	''' staticmethod(func) replacement usable in any context or scope '''
	func: {str,iscall}		= None
	name: str				= None
	fmt_func_var:str		= '_{}'
	#region forms
	def __set_name__(self, cls, name):
		self.name = name
	def __post_init__(self):
		self.func = self.func if isnttype(self.func,FuncProp) else self.func.func
	def __repr__(self):		return '!%s' % (isstr(self.func,orput=self.func.__name__))
	def __hash__(self):		return hash(self.func) if iscx(self.func) else throw(f'cant hash {self.func=!r}')
	#endregion
	#region mutate
	def __set__(self, obj, val):
		assert isstr(self.name)
		setattr(obj, self.fmt_func_var.format(self.func), val)
	def __get__(self, obj, cls):
		obj = isnone(obj,put=cls)
		func = iscall(self.func) and self.func or getattr(obj, self.fmt_func_var.format(self.func))
		return func
		
	def __call__(self, *pa, once=True,**ka):
		func = self.func
		out = func(*pa,**ka)
		return out
	#endregion
	...
class staticfunc(FuncProp): pass

class HistoryProp:
	''' record history of call args '''
	#region forms
	_to_entry				= lambda *pa,**ka: (pa,ka)
	def __init__(self, to_entry=None, pa=(), ka=None):
		self.to_entry = to_entry or self._to_entry
		self.pa = tuple(pa)
		self.ka = ka or {}
		self.history = []
	#endregion
	#region mutate
	def advance(self, *pa, **ka):
		entry = self.to_entry.__func__(*pa,*self.pa, **self.ka,**ka)
		self.history.append(entry)
		return entry
	__call__				= advance  # default behavior verb
	#endregion
	...

class PropertyDelta:
	''' react to property variable updates and implement routine behaviors '''
	#region mutate
	def set(self, value):
		updated = False
		if self.value != value:
			self.value = value
			updated = True
		return updated
	#endregion
	...

class IQueryProp:
	''' ...	'''
	#region mutate
	def __call__(self, *pa,**ka): pass
	def peak(self, *pa,**ka): pass
	#endregion
	...

class ElapsedProp(IQueryProp):
	''' ...	'''
	#region forms
	def __init__(self, period=0, immediate=True, **timedelta_kws):
		''' construct
			:param period:			amount of time to satisfy query; accepts int number of seconds or timedelta
			:param immediate:		if True property query is satisfied now; otherwise it is False until period elapses
			:param timedelta_kws:	timedelta keywords to match timedelta constructor inputs
		'''
		# Note: below '_t' indicates a point in time/datetime; '_dt' indicates a time vector/timedelta
		self.previous_t = datetime.now()
		self.period_dt = period
		if not isinstance(period, timedelta):
			self.period_dt = timedelta(seconds=timedelta_kws.get('seconds', period or 0), **timedelta_kws)
		if immediate:
			self.previous_t = self.previous_t - self.period_dt
	#endregion
	#region mutate
	def __call__(self, period=None, peak=False, **timedelta_kws):
		''' evaluate and return elapsed condition; when true update property for next query unless peaking '''
		now_t = datetime.now()
		period_dt = self.period_dt
		if period or timedelta_kws:
			period_dt = period if isinstance(period, timedelta) else (
				timedelta(seconds=timedelta_kws.get('seconds', period or 0), **timedelta_kws))
		next_t = self.previous_t + period_dt

		# evaluate elapsed property
		if now_t < next_t:
			return False

		# elapsed query will return true; dont dont make changes if peak is True
		if not peak:
			self.__init__(period_dt, immediate=False)
		return True
	def peak(self, period=None, **timedelta_kws):
		''' evaluate and return elapsed condition only; dont modify query '''
		return self(period, peak=True, **timedelta_kws)
	#endregion
	...

@dataclass
class ReactionProp:
	''' ...	'''
	on_update: callable
	value: any				= None
	pass_args: bool			= False
	pass_inst: bool			= True
	#region mutate
	def __get__(self, obj, cls):
		return self.value
	def __set__(self, obj, cls):
		self.value = cls
		ka = self.pass_args and dict(value=cls, prop=self) or {}
		self.on_update(obj, **ka)
	#endregion
	...

class CachedProp:
	''' ...	'''
	sign_seq_first			= lambda seq: (len(seq), seq and hash(seq[1]))
	sign_seq_last			= lambda seq: (len(seq), seq and hash(seq[-1]))
	
	#region forms
	def __init__(self, evaluate, sign_state, initial=nothing):
		self._evaluate = evaluate
		self._sign_state = sign_state
		self._cached = initial
		self._sig = initial
	#endregion
	#region mutate
	def __get__(self, obj, cls):
		sig = self._sign_state(obj)
		if self._sig != sig:
			print('cache miss')
			self._cached = self._evaluate(obj)
			self._sig = sig
		else:
			print('cache hit')
		return self._cached
	def clear(self):
		self._cached = nothing
	#endregion
	...

class DirCacheProp:
	''' caches the dir result for classes to mitigate recomputation '''
	_cacheset_at			= {no:'',yes:'{}_set'}
	def __init__(self, cache_attr='_dir', cache_set:bool|str=no, cache_inst=no, eval_once=yes):
		''' ctor
			:param cache_attr: attr to place cache
			:param cache_set:  do or dont include cached set(dir) into cache_set.format(cache_attr), if True '_dir_set'
			:param cache_inst: 
			:param eval_once:  replace cache_attr with result after first dir() eval
		'''
		self.cache_attr = cache_attr
		self.cache_set = self._cacheset_at.get(cache_set,cache_set).format(cache_attr)
		self.cache_inst = cache_inst
		self.eval_once = eval_once
	#endregion
	#region mutate
	def __get__(self, obj, cls):
		# resolve inputs
		sbj = obj is None and cls or obj
		eval_once,cache_set = self.eval_once,self.cache_set
		# eval_once,cache_set = (self.eval_once,self.cache_set) if obj is None or self.cache_inst else (no,no)
		
		# resolve output
		dir_out = cls.__dict__.get(self.cache_attr,None)
		dir_out = dir_out or dir(cls)
		eval_once and setattr(cls,self.cache_attr,dir_out)  # first access replaces property if eval_once
		cache_set and setattr(cls,self.cache_set,set(dir_out))
		return dir_out
	#endregion
	...

class TheProp:
	''' create singleton instance of owning class under attribute '_the' on initial use
		Note: always a property which returns the_obj
	'''
	#region forms
	def __init__(self, *pa, the_attr=None,**ka):
		self.pa,self.ka = pa,ka
		self.the_obj = None
		self._attr = the_attr
	#endregion
	#region mutate
	def __set_name__(self, cls, attr):
		self._attr = self._attr or attr
	def __get__(self, obj, cls):
		if self.the_obj is None:
			self.the_obj = cls(*self.pa,**self.ka)	# first access creates the_obj
		return self.the_obj							# subsequent access yields the_obj
	def __set__(self, obj, mbr):
		pass				# assigning a new TheProp is ignored
	@classmethod
	def apply(cls, the_cls,*pa,the_attr=None,**ka):
		the_attr = the_attr or cls._attr or '_the'
		the = the_cls.__dict__.get(the_attr)
		if not the:
			the = cls(*pa,**ka)
			setattr(the_cls,the_attr,the)
		return the
	#endregion
	...

class LazyProp:
	''' create substitution of a class attribute upon its first use via __get__ attribute access '''
	_fab,_put_obj			= miss, no
	#region forms
	def __init_subclass__(cls, fab=_fab, put_obj=_put_obj):
		cls._fab = fab
		cls._put_obj = put_obj
	def __init__(self, fab=miss, kcls=miss, put_obj=miss, attrs=(), *pa, **ka):
		''' ctor
			:param fab:     fab-ricator to call upon usage 
			:param kcls:    insert cls as leading arg
			:param put_obj: insert obj as leading arg
			:param attrs:   attrs on which to set the output of fab()
			:param pa:      pos args passed to fab
			:param ka:      key args passed to fab
		'''
		# todo: attampt to combine put_obj & kcls
		self.fab = ismiss(fab,put=self._fab)
		self.put_obj = ismiss(put_obj,put=self._put_obj)
		self.kcls = kcls
		self.pa = pa
		self.ka = ka
		self.attrs = hss(attrs)
	#endregion
	#region mutate
	def __set_name__(self, cls, attr):
		not self.attrs and self.attrs.add(attr)
	def __get__(self, obj, cls):
		if not self.put_obj:
			for attr in self.attrs:
				setattr(cls,attr,miss)
		
		# resolve call inputs
		sbj = cls = cls or obj.__class__
		fab = (
			cls								if self.fab is TheProp or isstr(self.fab,orput='').lower() == 'the' else
			attrgetter(cls,self.fab)		if isstr(self.fab) else
			self.fab						if iscall(self.fab) else
			self.fab.__iter__				if isiter(self.fab) else
			throw(self.fab) )
		
		# resolve args inputs
		kk_arg = func_arg_keys(fab)
		sbj = not self.put_obj and cls or obj
		kcls,pa,ka = self.kcls,self.pa,self.ka
		kcls = (
			kcls							if kcls is not miss else
			0								if kk_arg else
			None )
		pa,ka = (
			[pa[:kcls]+(sbj,)+pa[kcls:],ka]	if isint(kcls) else
			[pa,ka|{self.kcls:cls}]			if isstr(kcls) else
			[pa,ka] )
		
		# resolve output & replace lazy members
		val = self
		if not self.put_obj or obj is not None:
			val = fab(*pa,**ka)
			for attr in self.attrs:
				setattr(sbj,attr,val)
		return val
	#endregion
	...
class LazyAtProp(LazyProp, put_obj=yes): pass
class TheLazyProp(LazyProp, fab='the'): pass

@dataclass
class CacheProp(property):
	fab:iscall				= None
	attr_src:str			= None
	attr_cache:bool|str		= None
	attr_into:bool|str		= staticfunc(lambda attr: attr.lstrip('_'))
	# fab:iscall				= None
	fill:object				= miss
	#region defs
	_fab					= None
	_rxinto_prop_src		= '^_(\w+)', r'\1'  # transform attr_prop into attr_src
	#endregion
	#region forms
	def __init_subclass__(cls, fab:iscx=_fab, rxinto_prop_src:tuple=_rxinto_prop_src, **ka):
		infuse(cls,**ka, _fab=fab, _rxinto_prop_src=rxinto_prop_src)
	def __post_init__(self):
		self.fab = (fab := self.fab or self._fab) and exact(fab)
		self.fill = ismiss(miss,put=(),orput=(self.fill,))
		self.attr_prop = None
	#endregion
	#region mutate
	def __set_name__(self, cls, attr):
		''' set data descriptor assignments given the property name '''
		self.attr_prop = attr
		self.attr_cache = isstr(self.attr_cache) and self.attr_cache or self.attr_into(self.attr_prop)
		...
	def __get__(self, obj, cls):
		''' get source val from obj. cache if needed '''
		# todo: try to get cached var
		if obj is None:					 		return self
		elif self.attr_cache in obj.__dict__:	return obj.__dict__[self.attr_cache]
		# elif hasattr(obj,self.attr_under): 		return getattr(obj,self.attr_under,None)
		
		# get val
		# todo: maybe put in separate base class
		val = obj if not self.attr_src else getattr(obj,self.attr_src,*self.fill)
		val_out = val if not self.fab else self.fab(val)
		
		# store value
		attr_cache = isstr(self.attr_cache) and self.attr_cache or self.attr_cache(self.attr_prop)
		attr_cache and obj.__dict__.__setitem__(attr_cache, val_out)
		return val_out
	# def __set__(self, obj, val):
	# 	# todo: how to replace self.__set__ on a specific obj instance?
	# 	obj.__dict__[self.attr_prop] = val
	#endregion
	...
@dataclass
class UnderProp:
	fab:iscall				= None
	attr_src:str			= None
	attr_cache:bool|str		= True		# when true caches output val into attr_prop name; subsequent access is direct
	# fab:iscall				= None
	fill:object				= miss
	#region defs
	_fab					= None
	_rxinto_prop_src		= '^_(\w+)', r'\1'  # transform attr_prop into attr_src
	#endregion
	#region forms
	def __init_subclass__(cls, fab:iscx=_fab, rxinto_prop_src:tuple=_rxinto_prop_src, **ka):
		infuse(cls,**ka, _fab=fab, _rxinto_prop_src=rxinto_prop_src)
	def __post_init__(self):
		self.fab = (fab := self.fab or self._fab) and exact(fab)
		self.fill = ismiss(miss,put=(),orput=(self.fill,))
		self.attr_prop = None
	#endregion
	#region mutate
	def __set_name__(self, cls, attr):
		''' set data descriptor assignments given the property name '''
		self.attr_prop = attr
		# self.attr_src = self.attr_src or re.sub(*self._rxinto_prop_src,self.attr_prop)
		self.attr_cache = istrue(self.attr_cache, put=self.attr_prop)
	def __get__(self, obj, cls):
		''' get source val from obj. cache if needed '''
		# todo: try to get cached var
		if obj is None:					 		return obj
		# elif hasattr(obj,self.attr_under): 		return getattr(obj,self.attr_under,None)
		
		# get val
		# todo: maybe put in separate base class
		val = obj if not self.attr_src else getattr(obj,self.attr_src,*self.fill)
		val_out = val if not self.fab else self.fab(val)
		
		# store value
		self.attr_cache and setattr(obj, self.attr_cache, val_out)
		return val_out
	# def __set__(self, obj, val):
	# 	# todo: how to replace self.__set__ on a specific obj instance?
	# 	obj.__dict__[self.attr_prop] = val
	#endregion
	...
class StrUnderProp(UnderProp, fab=str,rxinto_prop_src=('^s_(\w+)', r'\1')):
	def __set_name__(self, cls, attr):
		''' set data descriptor assignments given the property name '''
		self.attr_prop = attr
		self.attr_src = self.attr_src or re.sub(*self._rxinto_prop_src,self.attr_prop)
		self.attr_cache = istrue(self.attr_cache, put=self.attr_prop)

# todo: relo
@dataclass
class AttrFab:
	fab:iscall				= None
	prefab:iscall			= None
	#region forms
	def __post_init__(self):
		assert (self.fab is None) != (self.prefab is None), f'... {locals()=}'
		self.fab = self.fab and exact(self.fab)
		self.prefab = self.prefab and exact(self.prefab)
	#endregion
	#region mutate
	def get(self, *_pa,**_ka):
		def pre_get(*pa,**ka):
			out = self.prefab(*_pa,*pa,**_ka|ka)
			return out
		stage = self.prefab and self.pre_get or self.fab(*_pa,**_ka)
		return stage
	__getattr__ = __call__	= get
	#endregion
	...

exact						= staticfunc
the_dir_cacher				= DirCacheProp(cache_set=yes)
DirCacheProp._the			= the_dir_cacher
the,lazy,Lazy				= TheProp,LazyProp,AttrFab(LazyProp)
lazyat						= CacheProp
under						= UnderProp
thelazycls					= redef(thecls,fab_prop=lambda: TheLazyProp(kcls=None))  # using obj._the makes obj cls
islazythe					= islazyattr(_D.the_attr_default)
#endregion

#region adapter devcs
class HasErrs:
	''' subclasses can minimally provide either func:get_errs or prop:errs '''
	errs					= property(lambda self:     self.get_errs())				# generate errs
	err						= property(lambda self: not self.is_ok())					# todo: simplify
	exc						= property(lambda self: not self.is_ok(exc=on,f_back=3))	# todo: simplify
	ok						= property(lambda self:     self.is_ok())					# todo: simplify
	def is_ok(self,exc=no,**ka):
		errs = self.errs  # todo: should prolly prioritize get_errs(**ka) over errs
		ok = not errs
		exc and not ok and throw(istrue(exc,put=ValueError),errs,**ka)
		return ok
#endregion

#region tests
def test_delta():
	def changed(item):
		print('handling updated item:%s' % item)
	f = PropertyDelta(0, changed)
	if f.set(1):
		changed(f)
	f.set(2, changed)
	f.update(3)
def test_is_lazy():
	class Foo: _the = None
	isnt_lazy = islazythe(Foo,fab=throw,orput=True)
	assert isnt_lazy
def test_cache():
	from pyutils.structures.strs import LS
	
	class Foo:
		abc					= LS*'a b c'
		ten					= 10
		s_abc				= StrUnderProp()
		s_ten				= StrUnderProp()
	foo = Foo()
	print(f'{foo.s_abc=}')
	print(f'{foo.s_ten=}')
	print(f'{foo.s_ten=}')
	
if __name__=='__main__':
	# test_delta()
	# test_is_lazy()
	test_cache()
#endregion

#region scratch area
'''
history|  succession  course  record  chronology
lazy|  substitute  placeholder  onuse  setlater  post  after  deferred  delay  need
'''
#endregion


