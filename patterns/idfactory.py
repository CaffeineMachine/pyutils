class Singleton:
	''''''
	_the_instance = None
	_allow_multiple = False
	
	def __init__(self, *args, allow_multiple=None, **kargs):
		allow_multiple = allow_multiple if allow_multiple is not None else self._allow_multiple
		if self._the_instance is None:
			self._the_instance = self
		elif not allow_multiple:
			raise Exception(f'Multiple instances of {self.__class__.__name__} is disallowed.')
		super().__init__(*args, **kargs)
	@classmethod
	def get_instance(cls, *args, **kargs):
		if not cls._the_instance:
			cls._the_instance = cls(*args, **kargs)
		return cls._the_instance
	
GEN_UNIQUE_ID = ...

class IdFactory:
	''' Class overrides __new__ in order to maintain and retrieve a static instance for a given id
		Usage:
		IdFactory()       # => Create new instance with unique id 0
		IdFactory()       # => Create new instance with unique id 1
		IdFactory(...)    # => Create new instance with unique id 2
		IdFactory(2)      # => Return existing instance with id 2
		IdFactory('foo')  # => Create new instance with id 'foo'
		IdFactory('foo')  # => Return existing instance with id 'foo'
	'''
	# todo: change to IdPool

	_next_unique_id = {}
	_all_cls_instances = {}
	_default_id = GEN_UNIQUE_ID

	def __new__(cls, id=GEN_UNIQUE_ID, *args, **kargs):
		''' allocate constructor '''
		IdFactory.assert_valid_access(cls)

		# when id is GEN_UNIQUE_ID generate a unique id distinct from all other instances of this cls
		id =  cls._default_id if id is GEN_UNIQUE_ID else id
		if id is cls._default_id is GEN_UNIQUE_ID:
			id = IdFactory._next_unique_id.setdefault(cls, 0)
			IdFactory._next_unique_id[cls] += 1

		# get existing instance of cls for given id; allocate a new instance for id if it does not exist
		cls_instances = IdFactory._all_cls_instances.setdefault(cls, {})
		if id in cls_instances:
			cls_instance = cls_instances[id]
		else:
			# cls_instances[id] = cls_instance = super().__new__(cls, *args, **kargs)
			cls_instances[id] = cls_instance = super().__new__(cls)

		return cls_instance
	
		

	def __init__(self, id=None, *args, **kargs):
		'''	:param id: '''
		# prevent double initialization in the case that self was retrieved from dict of existing instances
		if hasattr(self, 'id'): return

		# initialize members
		self.id = id
		super().__init__(*args, **kargs)
		
	def release(self):
		self.cls_pop_id(self.id)
		if hasattr(self, '__del__'):
			self.__del__()

	@staticmethod
	def assert_valid_access(cls):
		''' error in accessing code '''
		if cls == IdFactory:
			raise TypeError('Creating an instance of IdFactory is prohibited.')

	@classmethod
	def cls_pop_id(cls, id):
		''' remove instance with id '''
		IdFactory.assert_valid_access(cls)
		result = None
		if cls in IdFactory._all_cls_instances and id in IdFactory._all_cls_instances[cls]:
			result = IdFactory._all_cls_instances[cls].pop(id)
		return result

	@classmethod
	def cls_get_id(cls, id):
		''' get instance of cls with given id '''
		IdFactory.assert_valid_access(cls)
		cls_instances = IdFactory._all_cls_instances.setdefault(cls, {})
		result = cls_instances.get(id)
		return result

	@classmethod
	def cls_get_keys(cls):
		IdFactory.assert_valid_access(cls)
		result = list(IdFactory._all_cls_instances.get(cls, {}).keys())
		return result

	@classmethod
	def cls_get_instances(cls):
		IdFactory.assert_valid_access(cls)
		result = IdFactory._all_cls_instances.get(cls)
		return result
FactoryIds = IdFactory

class IdFactoryOrig:
	''' Class overrides __new__ in order to maintain and retrieve a static instance for a given id
		Usage:
		print(IdFactoryOrig('foo'))  # Create new instance with id 'foo' and show id
		print(IdFactoryOrig('foo'))  # Return existing instance with id 'foo' and show exact same id
		print(IdFactoryOrig())       # Create new instance with unique id and show id
		print(IdFactoryOrig())       # Create new instance with unique id and show id
	'''

	_next_unique_id = {}
	_all_cls_instances = {}

	def __new__(cls, id=None, *args, **kwargs):
		''' allocate constructor '''
		IdFactoryOrig.assert_valid_access(cls)

		# when id is None generate a unique id distinct from all other instances of this cls
		if id is None:
			id = IdFactoryOrig._next_unique_id.setdefault(cls, 0)
			IdFactoryOrig._next_unique_id[cls] += 1

		# get existing instance of cls for given id; allocate a new instance for id if it does not exist
		cls_instances = IdFactoryOrig._all_cls_instances.setdefault(cls, {})
		if id in cls_instances:
			cls_instance = cls_instances[id]
		else:
			cls_instances[id] = cls_instance = super().__new__(cls, *args, **kwargs)
			# cls_instances[id] = cls_instance = super(IdFactoryOrig, cls).__new__(cls, *args, **kwargs)
			# cls_instances[id] = cls_instance = super(IdFactoryOrig, cls).__new__(cls)

		return cls_instance

	def __init__(self, id=None):
		''' initialize constructor '''
		# prevent double initialization in the case that self was retrieved from dict of existing instances
		if hasattr(self, 'id'): return

		# initialize members
		self.id = id

	@staticmethod
	def assert_valid_access(cls):
		''' error in accessing code '''
		if cls == IdFactoryOrig:
			raise TypeError('Creating an instance of IdFactoryOrig is prohibited.')

	@classmethod
	def cls_remove_id(cls, id):
		''' remove instance with id '''
		IdFactoryOrig.assert_valid_access(cls)
		result = None
		if cls in IdFactoryOrig._all_cls_instances and id in IdFactoryOrig._all_cls_instances[cls]:
			result = IdFactoryOrig._all_cls_instances[cls].pop(id)
		return result

	@classmethod
	def cls_get_id(cls, id):
		''' get instance of cls with given id '''
		IdFactoryOrig.assert_valid_access(cls)
		cls_instances = IdFactoryOrig._all_cls_instances.setdefault(cls, {})
		result = cls_instances.get(id)
		return result

	@classmethod
	def cls_get_keys(cls):
		IdFactoryOrig.assert_valid_access(cls)
		result = IdFactoryOrig._all_cls_instances.get(cls, {}).keys()
		return result

	@classmethod
	def cls_get_instances(cls):
		IdFactoryOrig.assert_valid_access(cls)
		result = IdFactoryOrig._all_cls_instances.get(cls)
		return result