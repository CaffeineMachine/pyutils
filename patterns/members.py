from pyutils.defs.funcdefs import asis
from string import Formatter
from dataclasses import is_dataclass

from pyutils.output.fmtrs import *
from pyutils.structures.strs import LStrs, TStrs, Names
from pyutils.utilities.maputils import *

#region member defs
class DefsMembers:
	cached_attr_getters = {}
Defs = DefsMembers
#endregion

#region member utils
# def infuse(subject, source=None, attrs=(), to_attrs=None, **coerce_ka):
# 	actual = {**{attr:getattr(source, attr) for attr in attrs if attr not in coerce_ka}, **coerce_ka}  # todo: use remap or mapmap
# 	subject.__dict__.update(actual)
# 	return subject
# def effuse(source, subject, attrs=None, to_attrs=None, **coerce_ka):
# 	actual = {**{attr:getattr(source, attr) for attr in attrs if attr not in coerce_ka}, **coerce_ka}  # todo: use remap or mapmap
# 	subject.__dict__.update(actual)
# 	return subject
def init_of_obj(obj, *pa, **ka):
	''' initialize a given *obj* based on its annotations or __dataclass_fields__.
		Similar to dataclass() without arbitrary limitations.
	'''
	# detect relevant annotations
	cls = obj.__class__
	base, *_ = cls.__bases__
	anno_cls, anno_base = cls.__annotations__, base.__annotations__

	# prevent ambiguous assignment of attr from both pa and ka as is python convention; should permit user to bypassing
	ka_cls_of_pa = dict(zip(anno_cls, pa))
	dupl = {*ka}.intersection(ka_cls_of_pa)
	assert not dupl, f'got duplicate params for attrs: {dupl}'
	
	# define pos. & key args categorized for own class members or base members
	# set of ka.keys() without locally defined fields (for base class)
	pa_base = pa[len(anno_cls):]
	k_cls, k_base = {*ka}.intersection(anno_cls), {*ka}.difference(anno_cls)
	# ka_cls, ka_base = dict(imapitems(k_cls, ka)), dict(imapitems(k_base, ka))
	ka_cls, ka_base = dintomap(ka, k_cls), dintomap(ka, k_base)
	
	# initialize members
	obj.__dict__.update({**ka_cls_of_pa, **ka_cls})
	super(cls, obj).__init__(*pa_base, **ka_base)
	hasattr(obj, '__post_init__') and obj.__post_init__()
#endregion

#region member devcs
class MemberArgs:
	''' User defined namespace object. Ctor keyword args can be accessed as attributes. '''
	def __init__(self, **ka):
		self.__dict__.update(ka)
		
	# method templates
	__repr__			= repr_obj

@dataclass
class MemberField:
	fab: object			= None
	call: object		= None
	put: object			= None
	pa: tuple			= ()
	ka: dict			= None
	def get(self, obj=None):
		result = (
			self.fab(obj, *self.pa, **self.ka or {}) if self.fab else
			self.call(*self.pa, **self.ka or {}) if self.call else
			self.put)
		return result
	
def _is_dataclass(obj):
	"""Returns True if obj is a dataclass or an instance of a
	dataclass."""
	cls = obj if isinstance(obj, type) else type(obj)
	return '__dataclass_fields__' in cls.__dict__

def _get_base_init(cls):
	base_init = None
	while cls is not object and isinstance(base_init, (MemberInit, type(None))):
		base_init = cls.__dict__.get('__init__')
		cls = cls.__base__
		# if : continue
	return base_init
@dataclass
class MemberInit:
	''' generate a class repr given vars (a map) and members (a seq) to concatenate '''
	base_kpa: tuple		= None	# kargs to pass to base self.__init__(*[pool_ka[k] for k in base_kpa])
	base_kka: tuple		= None	# kargs to pass to base self.__init__(**{k:pool_ka[k] for k in base_kka})
	post_kpa: tuple		= None	# kargs to pass to custom self.__post_init__(*[pool_ka[k] for k in post_kpa])
	post_kka: tuple		= None	# kargs to pass to custom self.__post_init__(**{k:pool_ka[k] for k in post_kka})
	init_apply: bool	= True
	init_base: bool		= True
	init_post: bool		= True
	#region setup
	def __post_init__(self):
		# self.base_kpa = isstrfab(self.base_kpa, TStrs)
		self.base_kpa = isstr(self.base_kpa, fab=TStrs)
		# self.base_kka = isstrfab(self.base_kka, TStrs)
		self.base_kka = isstr(self.base_kka, fab=TStrs)
	def __get__(self, obj, cls):
		''' Note: possible inf recursion from getattr(cls,name)->__get__().
			cls.__dict__.get(name) cannot lead to __get__()
		'''
		# todo: implement __set_name__ which apparently solves challenges due to unknown assignment
		cls = cls or obj.__class__
		cls.__mbr_init__ = self.initialize
		
		# supply __base_init__
		if '__base_init__' not in cls.__dict__:
			if cls.__dict__.get('__init__') is self and _is_dataclass(cls):
				post_init = cls.__dict__.get('__post_init__')
				post_init and delattr(cls, '__post_init__')
				delattr(cls, '__init__')
				for name, field in cls.__dataclass_fields__.items():
					setattr(cls, name, field)
				dataclass(cls)  # creates init function from fields
				post_init and setattr(cls, '__post_init__', post_init)
				
			# base_init = cls.__dict__.get('__init__')
			base_init = _get_base_init(cls)
			# base_init = getattr(cls, '__init__', None)
			if base_init in (self,None):
				self.init_base = False
			else:
				# cls.__base_init__ = cls.__init__
				cls.__base_init__ = base_init
				cls.__init__ = self
	
		result = self._wrap_init.__get__(cls if obj is None else obj)
		return result
	#endregion
	#region usage
	@staticmethod
	def _wrap_init(obj, *pa, **ka):
		obj.__class__.__mbr_init__(obj, *pa, **ka)
	def initialize(self, obj, *pool_pa, **pool_ka):
		''' sort given init pool args into initializers per config
			Note: initializers: {apply:__dict__[], base:__init__(), post:__post_init__()}
		'''
		# fixme: work-in-progress; passing tests but still needs to be more intuitive
		pool_pa, post_pa, base_pa = list(pool_pa), [], []
		if self.base_kpa is True and pool_pa:
			base_pa,pool_pa = pool_pa,()
		elif self.post_kpa is True and pool_pa:
			post_pa,pool_pa = pool_pa,()
		
		# index and migrate annotated args; for annos pool_pa => pool_ka; todo: annos and base_kka are essentially identical
		annos = obj.__class__.__annotations__
		for ind, key in enumerate(annos):
			if not pool_pa: break
			parg = pool_pa.pop(0) if pool_pa else getattr(obj.__class__, key)
			pool_ka.setdefault(key, parg)
		
		# work out field key placement into buckets: attr,base,post
		pool_ks = set(pool_ka) - set(self.base_kka or annos)
		base_kka,base_kpa = (None is self.base_kka is self.base_kpa) and (annos,False) or (self.base_kka,self.base_kpa)
		post_kka,post_kpa = (None is self.post_kka is self.post_kpa) and (False,False) or (self.post_kka,self.post_kpa)
		
		base_kpa = pool_ks if base_kpa in (True,None) else () if base_kpa is False else base_kpa
		base_kka = pool_ks if base_kka in (True,None) else () if base_kka is False else base_kka
		post_kpa = pool_ks if post_kpa in (True,None) else () if post_kpa is False else post_kpa
		post_kka = pool_ks if post_kka in (True,None) else () if post_kka is False else post_kka
		
		# from pool args build init pos args for attr,base,post
		for key in base_kpa:
			base_pa.append(pool_pa.pop(int(key)) if key.isdigit() else pool_ka.pop(key))
		for key in post_kpa:
			post_pa.append(pool_pa.pop(int(key)) if key.isdigit() else pool_ka.pop(key))
		# todo: pool_ka pops should come ofter in case user wants overlap
		
		# from pool args build init key args for attr,base,post
		pool_ka = dict(obj.__class__.__dict__, **pool_ka)
		base_ka = {key:pool_ka[key] for key in set(base_kka) & set(pool_ka)}
		post_ka = {key:pool_ka[key] for key in set(post_kka) & set(pool_ka)}
		
		# make init calls with resolved corresponding args
		self.init_apply and self.apply_attrs(obj, **pool_ka)
		self.init_base and getattr(obj, '__base_init__', super(obj.__class__, obj).__init__) (*base_pa, **base_ka)
		self.init_post and hasattr(obj, '__post_init__') and obj.__post_init__(*post_pa, **post_ka)
	def apply_attrs(self, obj, *pa, **ka):
		''''''
		# index pa by annotation order
		for key, value in obj.__class__.__annotations__.items():
			if not pa: break
			arg, *pa = pa
			assert key not in ka
			setattr(obj, key, arg)
			
		# apply attrs from anno|ka
		for key, value in ka.items():
			if key in obj.__class__.__annotations__:
				setattr(obj, key, value)
	#endregion
	# member aliases
	__call__			= initialize

class MemberInitCheck: ''' todo: '''

@dataclass
class MemberRepr:
	''' generate a class repr given vars (a map) and members (a seq) to concatenate '''
	attr_map: list		= ()
	attr_seq: list		= ()
	cls_name: bool		= None
	obj: object			= None
	fmt: str			= None
	fmt_obj: str		= None
	fmt_of: object		= None
	flt_prec: object	= None
	join: object		= ', '
	#region class members
	_cls_name			= True
	_fmt_obj			= None
	_ready				= False
	#endregion
	def __post_init__(self):
		self.attr_seq = istuple(self.attr_seq, orfab=tss)
		self.attr_map = istuple(self.attr_map, orfab=tss)
		self.cls_name = isnone(self.cls_name, put=self._cls_name)
		self.fmt = self.fmt or self.fmt_obj
		self.fmt = self.fmt or  ', '.join(ij(ss(self.attr_seq,'{{0.{0}}}'), ss(self.attr_map,'{0}={{0.{0}}}')))
		self.fmt = self.fmt or (self.cls_name and '{scls}({})') or self._fmt_obj or '{scls}<{}>'
		if isint(self.flt_prec):
			self.fmt_of = {float: f'.{self.flt_prec}f', **(self.fmt_of or {})}
		if self.fmt_of and not callable(self.fmt_of) and hasattr(self.fmt_of, 'get'):
			self._fmt_of, self.fmt_of = self.fmt_of, self.fmt_of.get
		self.join = isstr(self.join, put=self.join.join)
	def __get__(self, obj, cls):
		cls = cls or obj.__class__
		result = self
		if obj is not None:
			# must stage method as obj._repr_ = MemberRepr(self).repr(obj)
			not hasattr(obj,'_repr_') and setattr(obj,'_repr_',lambda f='': self.repr(obj,f))
			result = repr_of_obj = obj._repr_
			self is getattr(cls,'__str__',None)		and setattr(obj,'__str__',    repr_of_obj)
			self is getattr(cls,'__repr__',None)	and setattr(obj,'__repr__',   repr_of_obj)
			self is getattr(cls,'__format__',None)	and setattr(obj,'__format__', repr_of_obj)
		return result
	def __hash__(self):
		return hash((self.attr_seq, self.attr_map))
	def _repr(self, obj=miss,*pa,**ka):
		# todo: refactor
		# resolve ordered var strs:  => ss_var
		assert obj is not miss
		obj = obj if obj is not None else self.obj
		ss_var,attr = [],None
		try:
			ka_obj = {k:getattr(obj,attr) for k in self.attr_all}
			for idx, attr in enumerate(ijoin([self.attr_seq,self.attr_map])):
				value = getattr(obj,attr)
				s_attr = idx >= len(self.attr_seq) and attr+'=' or ''
				farg_value = self.fmt_of and (self.fmt_of(attr) or self.fmt_of(type(getattr(obj,attr)))) or ''
				value,farg_value = isstr(farg_value) and (value,farg_value) or (farg_value(value),'')
				s_var = '{}{:{}}'.format(s_attr, value, farg_value)
				ss_var.append(s_var)
		except Exception as exc:
			ss_var.append('{}={}("{}")'.format(attr or f'{nameof(self)}.attr..',nameof(exc),exc))
		
		# resolve composite str:  ss_var => s_vars
		cls_name = self.cls_name and isstr(self.cls_name, orput=obj.__class__.__name__) or ''
		s_vars = 'join=None' if self.join is None else self.join(ss_var)
		# s_vars = self.fmt_obj.format(s_vars, cls_name=cls_name)
		s_vars = self.fmt_obj.format(*ss_var, s=s_vars, cls_name=cls_name)  # todo: pass kargs
		return s_vars
	def repr(self, obj=miss,*pa,**ka):
		# todo: refactor
		# resolve ordered var strs:  => ss_var
		assert obj is not miss
		obj = _WontRepr(obj if obj is not None else self.obj)
		
		# resolve composite str:  ss_var => s_vars
		scls = self.cls_name and isstr(self.cls_name, orput=obj.__class__.__name__) or ''
		s_vars = self.fmt.format(obj, obj=obj, scls=scls)
		return s_vars
	# member aliases
	__call__			= repr
	
class ValueRepr(MemberRepr):
	''' generate a class repr given members (a seq) and vars (a map) to concatenate '''
	_cls_name			= False
	def __init__(self, attr_seq=(), attr_map=(), **ka):
		super().__init__(attr_map, attr_seq, **ka)

class _WontRepr:
	def __init__(self, obj):
		self.obj = obj
	def __repr__(self):					throw(f'failed to str repr obj of type {self.obj.__class__}')
	def __getattr__(self, key):			return getattr(self.obj,key)
	def __getitem__(self, key):			return self.obj[key]

MbrField,MbrInit		= MemberField, MemberInit
MbrRepr,ValRepr			= MemberRepr, ValueRepr
ReprAA,ReprVV			= MemberRepr, ValueRepr
ReprVars,ReprVals		= MemberRepr, ValueRepr  # todo: remove obsolete references
ReprMembers,ReprValues	= MemberRepr, ValueRepr  # todo: remove obsolete references
#endregion

#region adapters
def adapt_variable(cls):
	''' variable is the ability to pass an obj via doublestar notation func(**obj) '''
	# cls.__iter__ = lambda self: self.__annotations__
	cls.keys = lambda self: self.__annotations__.keys()
	cls.__getitem__ = getattr(cls, '__getattr__', cls.__getattribute__)
	return cls

@dataclass
class Adorns:
	adorns: list
	def __set__(self, cls, value):
		self.value = value
		callsargs(self.adorns, pal=cls)

class Adapts:
	adapts: dict
	def __init__(self, value_name=None, value=nothing, **adapts_ka):
		self.value = value
		self.adapts = dict(value_name and {value_name:asis} or {}, **adapts_ka)
	def __set_name__(self, cls, name):
		self.cls = cls
		if self.value != nothing:
			self.adapt(cls, self.value)
	def adapt(self, cls, value):
		self.cls = cls
		self.apply(value)
	def apply(self, value):
		self.value = value
		for name, adapt in self.adapts.items():
			value_adapted = adapt(value)
			setattr(self.cls, name, value_adapted)
	__set__ = adapt
#endregion

#region scratch area
'''
# Naming Conventions:
instance.name = member
var = (name, member)

map[key] = value
item = (key, value)

map: A collection of values each indexed with a key.
maps at times may suffice to just implement .keys() and .__getitem__() instead of all index functions.
capsule: holds reference to an obj and dynamically modifies access (get/set) in some determined way
analog: uses a given obj to statically initialize itself in some determined way
route: a single remapped attribute from source obj to an attribute on the target obj
routes: a collection of routes which if applied to an class can describe a type of capsule or analog
tree: a nested data structure of Nodes

Note: Capsule and Analog can produce the virtually the same result externally but are internally very different

Potential Terms:
analog, capsule
infuse, impart, implant, compat, contribute, populate, produce, provide, prepare, complete, equip, graft
cast, mold, inhabit, supply, accommodate

adapt, adorn, bestow, imbue, support


Note: python only requires keys() & __getitem__(), i.e. a map, for an object to be passed using star-star syntax like func(**key_args)
From these 2 functions values, items, __len__, __iter__, __contains__, __bool__ can all be derived.
all dictionaries are maps but all maps are not dictionaries. For example below cached_primes might not be
implemented as a dict.
>>> bool(cached_primes[113])

nested_map[path] = node

composite_instance.names = component
var = (name, member)

Names, Keys, Inds and any combination will qualify as a Ref
'''
#endregion
