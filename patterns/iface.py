from pyutils.defs.typedefs import *

#region iface defs
class DDInterfaces:
	#region defs
	#endregion
	...

DDIx = _D					= DDInterfaces
#endregion

#region iface devcs
class VCmpIx:
	''' implements basic sortable ops derived from enumeration function _v_cmp '''
	#region defs
	_v_cmp					= property(lambda self: self.__index__())
	#endregion
	#region utils
	def __eq__(self, arg):		return self._v_cmp == arg._v_cmp
	def __lt__(self, arg):		return self._v_cmp <  arg._v_cmp
	def __le__(self, arg):		return self._v_cmp <= arg._v_cmp
	def __gt__(self, arg):		return self._v_cmp >  arg._v_cmp
	def __ge__(self, arg):		return self._v_cmp >= arg._v_cmp
	#endregion
	...

class CmpIx:
	''' implements basic comparable ops derived from foundation ops such as: __eq__ & __lt__ '''
	#region forms
	# def __init_subclass__(cls, **ka):
	# 	cls._has_add = ...
	# 	cls._has_mul = ...
	#endregion
	#region utils
	def __compat__(self, arg):		return oftype(self, type(arg)) or oftype(arg, type(self))
	def __eq__(self, arg):			return not (self != arg)
	def __ne__(self, arg):			return not (self == arg)
	def __lt__(self, arg):			return not (self >  arg or self == arg)
	def __le__(self, arg):			return not (self >  arg)
	def __gt__(self, arg):			return not (self <  arg or self == arg)
	def __ge__(self, arg):			return not (self <  arg)
	#endregion
	...

class MathIx:
	''' implements basic math ops derived from foundation ops such as: __add__ & __mul__ '''
	#region forms
	def __init_subclass__(cls, **ka):
		cls._has_add = ...
		cls._has_mul = ...
	#endregion
	#region utils
	def __sub__(self, arg):			return  self +  -arg
	def __rsub__(self, arg):		return -arg  +   self
	def __radd__(self, arg):		return  arg  +   self
	def __rmul__(self, arg):		return  arg  *   self
	def __truediv__(self, arg):		return  self *   (1./arg)
	def __floordiv__(self, arg):
		out = self *   (1./arg)
		out = int(out) if not getattr(out,'__int__',None) else out.__int__()
		return out
	#endregion
	...
#endregion
