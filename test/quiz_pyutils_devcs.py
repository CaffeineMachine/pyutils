from pyutils.output.quiz import *

incl = 'inds:inum:fixme'
incl = 'maps:rrx'
incl = 'joins:fab_step'
incl = 'strs:fabs'
# incl = 'strs:fmt:fab'
# incl = 'strs:ssfm:util'
# incl = 'strs:ssfm:devc'
incl = 'iterutils:walk'

incl += '>!recall'

quiz = Quiz(incl=incl)

if quiz('inds'):
	from pyutils.structures.inds import *
	
	if quiz(':inum'):
		from pyutils.utilities.callutils import *
		from pyutils.utilities.strutils import *
		from pyutils.output.joins import *
		
		briefs = at0,at9,len
		abrv_of = lambda seq,briefs=briefs: tuple((brief(seq) for brief in briefs))
		
		if quiz('::basic'):
			of_20_50 = [*inum(stop=20., steps=50.)], [*inum(stop=20., steps=50.)]
			abrvs = cxpas(abrv_of, of_20_50)
			s_abrvs = cxpas(ss, abrvs, to_val='{:.2f}',fab=' '.join)
			print(s_abrvs)
			assert s_abrvs == ('0.00 20.00 50.00', '0.00 20.00 50.00')
			
		if quiz('::step steps'):
			of_4_432 = [*inum(step=3.6944444444444446, steps=432.0)],
			abrvs = cxpas(abrv_of, of_4_432)
			s_abrvs = cxpas(ss, abrvs, to_val='{:.2f}',fab=' '.join)
			print(s_abrvs)
			assert s_abrvs == ('0.00 1592.31 432.00',), s_abrvs
			
		if quiz('::opts'):
			outs = [
				[*inum(stop=1600, steps=12)],
				[*inum(stop=1600, steps=12, end=True)],
				[*inum(stop=1600, steps=12, stop_end=False)],
			]
			print(sless_fs(SJ.jlnl(outs),0))
			# abrvs = cxpas(abrv_of, of_4_432)
			# s_abrvs = cxpas(ss, abrvs, to_val='{:.2f}',fab=' '.join)
			# print(s_abrvs)
			# assert s_abrvs == ('0.00 1592.31 432.00',), s_abrvs
		
		if quiz('::fixme'):
			outs = [
				[*inum(stop=8640, steps=12, end=True)],
			]
			print(sless_fs(SJ.jlnl(outs),0))
	
	if quiz(':Ind'):
		l_nnk, i_nnk = list(range(1000)), iter(range(1000))
		at_95_100,i_at_95_100,li_at_95_100 = Ind[95:100], IInd[95:100], IInd.list[95:100]
		
		print([at_95_100(l_nnk)				for i in range(2)])
		print([list(i_at_95_100(i_nnk))		for i in range(2)])
		print([li_at_95_100(i_nnk)			for i in range(2)])

if quiz('strs'):
	if quiz(':fabs'):
		from pyutils.structures.strs import *
		i					= Vars(
			abc				= 'a  b  c',
			sfmt			= '{} {} {a} {b}',
			seq				= ss('I  II'),
			map				= dict(a=1,b=2), )
		ok					= Vars(
			seq				= 'I II {a} {b}',
			map				= '{0} {1} 1 2', )
		
		# resolve output
		s = S*'a  b  c'
		print(s)
		# o					= Vars(
		# 	iss				= ISS/i.abc,
		# 	tss				= TSS/i.abc,
		# 	lss				= LSS/i.abc,
		# 	hss				= HSS/i.abc,
		# 	dss				= DSS/i.abc,
		# 	dff				= FF /i.abc,
		# )
		# PJ.any(o)
		# assert ok == o, ok
	
	if quiz(':ssfm'):
		from pyfigurines.strings import *
		from pyutils.structures.strs import *
		from pyutils.structures.maps import *
		from pyutils.output.joins import *
		from pprint import pprint as pp
		
		# resolve inputs
		i					= Vars(
			vegan_pa		= abcs_enum_vegan[:5],
			vegan_ka		= {veg[0].lower():veg for veg in abcs_enum_vegan[:5]},
			ss_n			=   '^7    ^.4    ^5.5    ^6.4    ^7.3  ',
			ss_kn			= 'a:^7  b:^.4  c:^5.5  d:^6.4  e:^7.3  ', )
		ok					= Vars(
			nn_a			= (7, .4, 5.5, 6.4, 7.3), )
		if quiz('::util'):
			o				= Vars(
				zkn			= zvv_of_sforms(i.ss_kn,fab=list),
				zk			= zvv_of_sforms(i.ss_n,fab=list), )
			pp(o,width=40)
			
		if quiz('::devc'):
			o				= Vars(
				ffm_a		= SSFm(i.ss_kn, '{2}|{3}|{4}|{0}|{1}'),
				ffm_b		= SSFm(i.ss_n, '{2}|{3}|{4}|{0}|{1}'), )
			o.nn_a			= o.ffm_a.inn
			o.ffm_a_dupl	= SSFm(*annombrs(o.ffm_a))
			o.vegan			= o.ffm_a.format(*i.vegan_pa)
			o.ff			= o.ffm_a.format(*ff(range(10)))
			
			PJ.any_nl(o)
			pp(o)
			pp(o)
			assert o.ffm_a == o.ffm_a_dupl, o.ffm_a_dupl
			assert o.nn_a == ok.nn_a, ok.nn_a
		
		
		
		
		
	if quiz(':fmt'):
		from pyutils.output.joins import *
		if quiz('::fab'):
			# resolve input
			i				= Vars(
				sfmt		= '{} {} {a} {b}',
				seq			= ss('I  II'),
				map			= dict(a=1,b=2), )
			ok				= Vars(
				seq			= 'I II {a} {b}',
				map			= '{0} {1} 1 2', )
			# resolve output
			o				= Vars(
				seq			= F * i.sfmt * i.seq,
				map			= F * i.sfmt * i.map, )
			PJ.any(o)
			assert ok == o, ok
		
		# DD.s_enum_hdr = DD.fmt_enum/('{{{}:{}}}'.format(nth,DD.ssz_enum))
		print()

if quiz('maps'):
	from pyutils.structures.maps import *
	
	if quiz(':rxmap'):
		rxx = RXAt('word  num  symb    ',
				   '\w+   \d+  [^\w\d]+', fmt='${2}  #{1}  @{0}')
		print(rxx.at)
	
	if quiz(':rrx'):
		from pyutils.output.joins import *
		
		i					= Vars(
			s0				= 'a_bDDDb_cDDDc_a z_xWWWx_yWWWy_z',
			az				= Vars(
				_abcxyz		= RxAt(  'abc    xyz    ',
									r'a\w+a  z\w+z  ', fmt='{}\s+{}'),
				abc			= RxAt(  'b      c      ',
									r'b\w+b  c\w+c  ', fmt='{}\S+{}'),
				xyz			= RxAt(  'x      y      ',
									r'x\w+x  y\w+y  ', fmt='{}\S+{}'), ),
		)
		o = Vars(dict.fromkeys(i))
		
		reload_on_patch('pyutils.structures.maps')
		@recall_first_on_reload
		def test_find_kkrrx(arg,*pa,**ka):
			try:
				result = New.find_kkrrx(arg)(*pa,**ka)
				PJ.jjdnl(result)
				return result
			except Exception:
				tb.print_exc()
		
		o.az		= test_find_kkrrx(i.az,i.s0)			# valid
		o.az_err	= test_find_kkrrx(i.az,i.s0,deepest=2)	# error: exceeded depth
		
	if quiz(':router'):
		from pyutils.patterns.capsules import *
		# resolve inputs
		i					= Vars(
			src				= Vars(
				a			= Vars(a='aa',b='ab'),
				b			= Vars(a='ba',b='bb'), ), )
		i.rr0				= Reroutes(i.src)
		
		# resolve output
		o					= Vars(
			node			= Vars(
				a			= i.rr0.a,			# real path is a
				b			= i.rr0['b'], ),	# real path is b
			leaf			= Vars(
				ab			= i.rr0.ab,			# real path is a.b
				ba			= i.rr0['ba'], ),	# real path is b.a
		)
		print()

if quiz('iterutils'):
	if quiz(':walk'):
		from pyutils.utilities.iterutils import walk
		from pyutils.output.joins import *
		from pprint import pprint as pp
		# resolve inputs
		i					= Vars(
			src				= Vars(
				a			= Vars(b='a:b',c='a:c'),
				d			= Vars(e='d:e',f='d:f'),
			), )
		
		# resolve output
		o					= Vars(
			walk			= walk_kk(i.src,to_val='.'.join,fab=list),
			leap			= leap_kk(i.src,to_val='.'.join,fab=list),
		)
		pp(o,width=60)

if quiz('nums'):
	from pyutils.structures.nums import *
	from pyutils.utilities.iterutils import *
	
	if quiz(':has'):
		has = has_of_bound((1,3))
		print(has(0),has(1),has(2))
		print(cxpas(has,range(6)))

if quiz('joins'):
	if quiz(':fab_step'):
		from pyutils.output.joins import *
		from pyutils.structures.maps import *
		from pymatics.analysis.called import *
		
		# resolve inputs
		i					= Vars(
			walk			= {
				dict		: (dict.items,SJ.jj_dict_nl), },
			steps			= [
				[ischrs		, (None,'{!r}'.format)],
				[isiter		, (asis,SJ.j_list)], ],
			abcs			= Vars(
				a			= 'aa ab ac ',
				b			= tss('ba bb bc '),
				c			= dict(
					ca		= 'ca',
					cb		= tuple('cb'),
					cc		= dict(c='c'), ), ), )
		ok					= Vars(
			abcs			= '''{
				  a: 'aa ab ac ',
				  b: ['ba', 'bb', 'bc'],
				  c: {
				    ca: 'ca',
				    cb: ['c', 'b'],
				    cc: {
				      c: 'c' } } }'''.replace('\t',''), )
		# resolve output
		o					= Vars(abcs=SJ.any_at.default(i.abcs))				# valid
		print(o.abcs)
		assert ok.abcs == o.abcs, 'got bad output. expected:\n%s' % ok

#region scratch area

'''
inum(step, start, stop, steps, near_stop)
  -> (None, 0, 1600, 12, None) => of 13   (0..1596)
  -> (None, 0, 4320, 12, None) => of 12   (0..3960)
  -> (100.0, 14300.0, 15910, None, None) => of 17   (14300.0..15900.0)
  -> (3.6944444444444446, 0, 4320.0, None, None) => of 1170 (0..4318.80555)
  -> (3.6944444444444446, 0, None, 432.0, None) => of 433  (0..1595.99999)

inum(step, start, stop, steps, near_stop)
  -> (None, 0, 1600, 12, None) => of 13   (0..1596)
  -> (None, 0, 8640, 12, None) => of 12   (0..7920)
  -> (100.0, 10900.0, 12044, None, None) => of 12   (10900.0..12000.0)
  -> (0.184914, 0, None, 8640.0, None) => of 8640 (0..1597.47931)
  -> (3.883211, 0, None, 412.0, None) => of 412  (0..1596.00000)
'''
#endregion
