from pyutils.output.quiz import *

incl = 'colldefs:xcoll'
quiz = Quiz(incl=incl)

if quiz('colldefs'):
	from pyutils.defs.colldefs import *
	from pyutils.structures.maps import *
	from pyutils.utilities.iterutils import *
	
	nn,ss = [0,1,2,3],[*'abc']
	fmtr_out = ' == '.join(strs([0,1,2],'({{{0}.__class__.__name__:>5}}){{{0}}}')).format
	if quiz(':xcoll'):
		out = Vars(
			apt=apt(*ss),	xapt=xapt(*ss),	tuple=tuple(ss),
			apl=apl(*ss),	xapl=xapl(*ss),	list=list(ss),
			aph=aph(*ss),	xaph=xaph(*ss),	s_et=set(ss),
		)
		assert out.s_et  == out.aph == out.xaph
		assert out.list  == out.apl == out.xapl
		assert out.tuple == out.apt == out.xapt
		print(fmtr_out(out.s_et, out.aph, out.xaph))
		print(fmtr_out(out.list, out.apl, out.xapl))
		print(fmtr_out(out.tuple, out.apt, out.xapt))
		
	if quiz(':xcls'):
		class XLAbc(xaplist, pa='a b c'): pass
		class XTAbc(xaptuple, pa='a b c'): pass
		
		out = Vars(
			xlabc = XLAbc(*nn),
			xtabc = XTAbc(*nn),
		)
		for xcabc,coll in zip([out.xlabc,out.xtabc], [list,tuple]):
			assert xcabc == coll([xcabc.a,xcabc.b,xcabc.c,3]) == coll(nn), xcabc
			print(fmtr_out(coll(nn), [xcabc.a,xcabc.b,xcabc.c,3], xcabc))
		
