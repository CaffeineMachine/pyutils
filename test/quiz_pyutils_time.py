from pyutils.output.quiz import *

# incl='tm:timedeltaunit'
# incl='tt:parse-newer'
# incl='tt:parse-new'
# incl='tt:has'
incl='tt:set_ops'

# incl='t:util:on_delta'
# incl='t:util:irate'
# incl='t:util:poll_until'

quiz = Quiz(incl=incl)
if quiz('t'):
	from pyutils.time.times import *
	
	if quiz(':util'):
		if quiz('::irate'):
			i = Vars(
				xyz		= 'xyz',
				abc		= 'abcdef', )
			
			cxpas(print, irate(i.abc))
			cxpas(print, irate_t(i.abc))
			cxpas(print, irate_tid(i.abc))
			cxpas(print, irate_st(i.xyz, 1))
			cxpas(print, irate_st(iter(i.xyz).__next__, 1))
		
		if quiz('::on_delta'):
			from pyutils.time.thread import *
			
			on_delta_of(iter([0,0,0,1,0,1]).__next__, rate=1, state=False)
		
		if quiz('::poll'):
			from pymatics.store.quizzed import *
			
			work(poll_delta_paths, DDQZD.p_inbox, state=False)
			sleep(3)
			cp_path(DDQZD.p_inbox_prev,DDQZD.p_inbox)
		
		if quiz('::poll_until'):
			from pymatics.store.quizzed import *
			
			work(poll_until_delta_paths, DDQZD.p_inbox, state=False)
			sleep(3)
			cp_path(DDQZD.p_inbox_prev,DDQZD.p_inbox)

if quiz('tdefs'):
	from pyutils.time.timedefs import *
	
	if quiz(':like'):
		t = T(2000,1,2,3,4,5)
		assert like_t_s(t, '030405')
		assert like_t_s(t, '030405', '==')
		assert like_t_s(t, '040405', '>')
		assert like_t_s(t, '030400', '<')
	
if quiz('tasking'):
	if quiz(':timer'):
		from pyutils.time.thread import *
		raise NotImplementedError()

if quiz('tm'):
	from pyutils.time.timemetric import *
	
	if quiz(':timedeltaunit'):
		outs = cxpas(TU, ss('1week  2days  3hour  4minutes  5s  6second  7seconds'))
		cxpas(print, map('{0!s:6} = {0.tsecs} secs'.format, outs))

if quiz('tt'):
	from pyutils.time.times import *
	# from pprint import pprint as pp
	# from pyutils.utilities.callutils import *
	# from pyutils.time.timemetric import *
	# from pyutils.output.joins import *
	# from pyutils.output.colorize import *
	
	if quiz(':iteration'):
		j = SJ.jv_list
		
		t = DefsTime.parser.ymd_hms('111213-141516')
		tt = TT(TM.second, t)  # todo: TTI[:t:TM.s]
		nni = NNI[:20:2]
		ts = j(nni(tt))
		print(ts)
	
	if quiz(':parse-new'):
		j = Join('\n', to_str=DefsTime.fmt.ymd_hm)
		fmt = DefsTime.fmtc.ymd + '+' + DefsTime.fmtc.hm
		print(tt_of_str('2020|0730:0806:1m'))
		print(TT('2020|0730:0806:1m'))
		print(itt_of_str('2020|0730:0806:1m  2020|0130:0206:2m'))
		print(itt_of_str('2008+ 01-|2323:2359 02-|06:07'))  # [200801-2323:200801-2359:1s, 200802-06:200802-07:1s]
		
	if quiz(':parse-newer'):
		itt_at = Vars(
			short	= itt_of_str('202002|01:06:1h  202005|01:06:1h  202008|01:06:1h'),
			shorter	= itt_of_str('2020+  02|01:06  05|01:06  08|01:06  +:1h'), )
		print('{0.short}\n{0.shorter}'.format(itt_at))
		assert itt_at.short == itt_at.shorter
		
	if quiz(':has'):
		# gauge1,gauge3 = gauged_vals(1), gauged_vals(3)
		# print(gauge1(td=td0),gauge3(td=td0),'\n')
		# sleep(2)
		# print(gauge1(),gauge3(),'\n')
		# sleep(3)
		# print(gauge1(),gauge3(),'\n')
		
		gauge1,gauge3 = gauged_call(1,lambda:print('one')), gauged_call(3,lambda:print('three'))
		gauge1(td=td0),gauge3(td=td0),print()
		sleep(2)
		gauge1(),gauge3(),print()
		sleep(3)
		gauge1(),gauge3(),print()
		
	if quiz(':has'):
		itt = ITT('2008+ 01-|2323:2359 02-|06:07')  # [200801-2323:200801-2359:1s, 200802-06:200802-07:1s]
		t = t_of('200802-0659')
		has = t in itt
		print('t         =  {:{ft}}\nitt       = {}\nt in itt  = {}'.format(t,itt,has, ft=TFmtcs.ymd_hm))
		assert has, 'Failed: t in itt'
		
	if quiz(':set_ops'):
		# resolve inputs
		i = Vars(
			a			= TT('201116:201118:1d'),
			b			= TT('201117:201119:1d'), )
		ok = Vars(
			ab_and		= ITT('201117:201118:1d'),
			ab_or		= ITT('201116:201119:1d'), )
		
		# resolve output
		o = Vars(
			ab_and		= i.a & i.b,
			ab_or		= i.a | i.b, )
		PJ.any(o)
		assert ok == o, PJ.any(ok)
	
	if quiz(':traverse'):
		# traverse a timeline along with passage of time
		until = T.now() + TM.s*10
		t_2011 = DefsTime.parser.ymd_hms('111213-141516')
		tt = Times(TM.minute*15, t_2011)
		ticks_fmtr = f'The time now is:  VIRT:{cyanl(DefsTime.fmt.ymd_hms)}  REAL:{cyan(DefsTime.fmt.ymd_hms)}'.format
		
		for t_virt, is_realtime in tt.traverse():
			t_real = T.now()
			print(ticks_fmtr(t_virt, t_real))
			if t_real > until: break
			
	if quiz(':traverse modifiers'):
		# traverse a timeline along with passage of time with timeline modifiers: jump, pace, msg, callback, t_ind
		until = T.now() + TM.s*5
		t_2011 = DefsTime.parser.ymd_hms('111213-141516')
		tt = Times(TM.minute*15, t_2011)
		ticks_fmtr = f'The time now is:  VIRT:{cyanl(DefsTime.fmt.ymd_hms)}  REAL:{cyan(DefsTime.fmt.ymd_hms)}'.format
	
		for t_virt, is_realtime in tt.traverse(jump=[10,None], pace=[.5,None], msg=[None, 'after jump'], t_ind=[0,1]):
			t_real = T.now()
			print(ticks_fmtr(t_virt, t_real))
			if t_real > until: break

#region scratch area
'''
traverse
sequence, playback, reenact, keyframe, procession, begin,  synchronize, chronize, timeline, sequencer, timewarp
meanwhile, happen, wind, step, setforth, overclock,
recount, chronicle, unfold, unravel, pass, course
spectate, stage,

jump, cue, effect, event, func, mod, fastfwd, convert, transition, shift, reset, prompt, move,
point, mark, conduct, pace, step, td, tstep_ind, queue, induce
trace, track, along, follow, with
queue_td, queue_tind,
tmod_td, tmod_arg
'''
#endregion
