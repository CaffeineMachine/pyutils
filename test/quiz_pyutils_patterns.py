# from pprint import pprint as pp
from pyutils.structures.nums import *
from pyutils.structures.strs import *
from pyutils.patterns.members import *
from pyutils.utilities.sequtils import Ind
from pyutils.output.forms import *

from quantkit.assets import DefsAssets as Defs




active_cells = 0,
active_cells = set(active_cells)

i_ns = INums()  # behaves like a mutable int and thus can increment
# if all([i_ns in active_cells, next(i_ns)]):
# 	print(ljoin([Strs('a b c d e')]))



if all([i_ns in active_cells, next(i_ns)]):  # todo: support a context.  ie: with active_cells ...:
	class Foobar:
		c: list = ()
		a: str = None
		b: str = None  # todo: field(default_factory=list)
		__init__ = MbrInit()
		__repr__ = ValRepr('a b c', cls_name=0)
	fbs = Foobar(), Foobar(1,2), Foobar(1,b=2), Foobar(a=1), Foobar(b=2)
	print(fbs)
	
if all([i_ns in active_cells, next(i_ns)]):
	class Foobar(list):
		c: list = ()
		a: str = None
		b: str = None  # todo: field(default_factory=list)
		__init__ = MbrInit('c')
		__repr__ = ValRepr('_pre_rep a b', cls_name=0)
	fbs = Foobar([10,11]), Foobar(a=1), Foobar(b=2)
	print(fbs)

if all([i_ns in active_cells, next(i_ns)]):
	# todo: not a real dataclasses test
	@dataclass
	class Foobar:
		c: list = ()
		a: str = None
		b: str = None  # todo: field(default_factory=list)
		
		__init__ = MbrInit()
		__repr__ = ValRepr('a b c', cls_name=0)
	fbs = Foobar(), Foobar(1,2), Foobar(1,b=2), Foobar(a=1), Foobar(b=2)
	print(fbs)

# todo: relocate
if all([i_ns in active_cells, next(i_ns)]):
	# Testing next_tick
	from pyutils.time.timemetric import DefsTime, TM, next_tick, prev_tick
	print(prev_tick(TM.week, t_zero='sunday')-TM.week)
	print(next_tick(TM.week, t_zero='sunday')-TM.week)



# todo: relocate
def test_numiters():
	## test iternums next
	ns = iter(inum())
	print(next(ns), next(ns))
	ns = Ints()
	print(next(ns), next(ns))

	ns = IIncr()
	print(str(ns), str(next(ns)), str(next(ns)))
	
	## todo: test iternums iter
	assert ns == 2

#
# if __name__=='__main__':
# 	from pymatics.store.trackrecords import time_run, DefsTrackRecord
# 	# test_numiters()
# 	# print(list(test_cells_known_markets(0,1)))
# 	output = time_run(test_cells_known_markets, 0, 1, 2)
# 	# output = test_cells_known_markets(0, 1)
# 	print(list(output))
# 	DefsTrackRecord.inst.save()
