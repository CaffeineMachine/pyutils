from pyutils.time.thread import *
from pyutils.utilities.envs import *

exc = NotImplementedError('Bye')
def throw():
	print('\nRunning thread task: Good')
	raise exc

def test_exc_thread_timer():

	for worker in [ExcThread(target=throw), ExcTimer(0, throw)]:
		worker.start()
		worker.join()
		print('Thread ended with exception: \'%s\'' % worker.exc)
		assert worker.exc.message == exc.message
		sleep(1)

def test_restructure():
	from pprint import pprint as pp
	from collections import namedtuple


	### test setup
	Ratings = namedtuple('Ratings', 'strength intellect speed wealth')
	Person = namedtuple('Person', 'name alias friend ratings')

	clark = Person({'first':'clark','last':'kent'}, ['superman', 'man of steel', 'son of krypton', 'smallville'], None, Ratings(8.5, 6., 5.5, 3.))
	bruce = Person({'first':'bruce','last':'wayne'}, ['batman', 'dark knight', 'bats', 'badman'], [clark], Ratings(4., 7., 3., 9))


	### tested and working: Iters()
	special_iter = Iters([bruce, clark]*2)
	classname = lambda obj: type(obj).__name__
	pp(' '.join(map(classname, special_iter)))	# => 'Person Person Person Person'
	pp(list(special_iter.alias[0]))				# => ['batman', 'superman', 'batman', 'superman']
	pp(list(special_iter.ratings.wealth))		# => [9, 3.0, 9, 3.0]

	# ### work in progress: Iters() handles generator content by returning a generator not a collection iterator
	# class LogAccess(list):
	# 	def __getitem__(self, item):
	# 		value = list.__getitem__(self, item)
	# 		print('Got index %s with value %s.' % (item, value))
	# 		return value
	# # index_grid = [LogAccess([LogAccess(range(y,y+3)) for y in range(0,9,3)])]
	# index_grid = LogAccess([bruce]*5)
	# # pp(index_grid)
	# index_grid_iter = Iter(index_grid).name['last']
	# # [index_grid_iter[1]
	# print(next(index_grid_iter))
	# print(next(index_grid_iter))
	# print(next(index_grid_iter))
	# index_grid_gen = Iter((x for x in index_grid)).name['last']
	# print(next(index_grid_gen))
	# print(next(index_grid_gen))
	# print(next(index_grid_gen))
	# # list(index_grid_gen)
	# exit()


	### tested and working: Keys()
	k0 = K.name['first']
	k1 = K.alias[0]
	k2 = K.friend[0].ratings.wealth
	print(k0)				# => Keys(['name', 'first'])
	print(k1)				# => Keys(['alias', 0])
	print(k2)				# => Keys(['friend', 0, 'ratings', 'wealth'])

	# examples of invoking keys accessors on nested data structures
	print(k0.get(clark))	# => 'clark'
	print(k0.get(bruce))	# => 'bruce'
	print(k1.get(bruce))	# => 'batman'
	print(k2.get(bruce))	# => 3.0



if __name__=='__main__':
	# test_exc_thread_timer()
	from sh import cat
	
	ctx = '.envs.yaml'

	myenv = local_envs()
	print(myenv)
	print(cat(ctx))
	
	b = myenv.aa.bb.pop()
	print(myenv)
	myenv._flush()
	print(cat(ctx))
	myenv._flush()

	myenv.aa.bb = 'foobar'
	print(myenv)
	myenv._flush()
	print(cat(ctx))

	


'''
features:
170901	restructure: implemented
170901		initial implementation: Struct, Keys, Iters
170901		implemented basic tests: Iters, Keys
180215		Struct: support attribute access in addition to __getitem__
171001	datastructure:
171001		XDict: dict enchanced with arithmetic and bitwise ops; implemented
180310			supporting basic dict-key-set operations: union, intersection & difference
180310			supporting basic value-list operations: remove & matching
180310			supporting overloaded operator mappings to dict-key-set and value-list
180310			supporting reverse overloaded operators
180310			finalized operation names and operator mapping
180402			added is_superset, is_subset and polymorphic __contains__
171001		XObj: uses getattr as alternative to getitem; implemented
171001		EXDict: extended dict with OrderedDict and defaultdict and getattr behaviors; implemented
180811		XList: extends list with pattern matching/filtering and static factory functions
180811		IndexedDefault/FullList/FullDict: missing keys/indices return a given default; as inheritable behavior
180811		AttrEnum/FullEnumDict: dict key value also accessible as instance attribute; as inheritable behavior
180811		added multiple inline tests
171101	idfactory: pattern for constructing or reusing existing class by id; implemented; inline tests;
180101	thread/RepeatTimer:
180201		RepeatPolicy: enumerated strategies for controlling/enabling worker concurrency
180301		reconfigure RepeatTimer once started
180201	log: implemented
180201		tag filtering
180205		log level - priority behavior & function helpers
180205		load config file
180205		detect and reload config file update
180210		log_input: store reusable set of message tags
180210		log_output: message format
180210		log_output: map tagged message to output files
180210		runtime code reconfiguration; a. raise/lower LogInput prio, b. add/remove filters, c. recolor
180210		tag message coloring; foreground and background
180210		color mappings to tags
180210		log tags configurable with rudimentary log substitutions: prefix and suffix
181009		LogOutput: significant revamp to support different configurable message formatting
181009			msg formatting is configurable; now supporting datetime and loglevel tags formats
181009			Tlog: Log subclass with a format similar to other prominent loggers
180216	tracelog/traceprint: implemented; display executed code based on a trace show filter
180218		display frame: file, class, function, line, depth
180218		denote entering and exiting function
180222		refactored traceprint routines into separate module from tracelog
180222		configure show/hide: scopes; either files, classes or functions
180330		support trace show filter reconfiguration
180301	exceptions/InvalidTypeError: implemented; inline tests
180405		ErrorInfo: more generic frame interpreting exception; implemented
180310	propertydelta: assignment behavior returns true only when value needs to change; implmented
170901	event:
180330		EventHandler, EventPublisher: has type specific handlers, conditional handling & unsubscribing; implemented
180403	XFile: simple string like file access using python slices convention; implemented
180406		string representation is a preview of the given file; incorporated FileLock functionality
180811		added write functionality
180811	Poly/Polynom: Store an operation without its instance to invoke it on an obj polymorphically
180811	iterutils: module for iterator functions expanding on itertools
180811		subdivide: a given iterator in to specified lengths
180811	staticinstance: decorators to allow methods to double as static functions
180811	colorhighlighter: ansicode text reformatting
180811		added color sample functions
180811	charset: unicode character sets


fixes:
180115	idfactory: its possible for 2 classes inheriting idfactory to have id conflict; retrieving the same 1st declared instance result
180115	traceprint: resolved function shadowing in traceprint; now properly encapsulated
180310	datastructure/xdict: refactored XDict operations achieve expected behavior for returning self or copy of self
180406	Timestamp: fixed hardcoded input for as_int() when arg is a str

todo:
		pattern for import only whats needed from pyutils for each pyutils unit test
		pyutils:
			consolidate all pyutils tests here
		tracelog/traceprint:
			benchmark performance and overhead for simple print vs pyutils.log
			comments
		exceptions:	implement on_pre_post_mortem() and archive_post_mortem()
		restructure:
			make tests illustrate capability overlap between FieldIter, Pass, and Struct
			cleanup
			Struct:
				need expressive failures to assist with data access failure
				does not cope well with access misses
				implement inline tests
			Pass:
				should also allow args when called in addition to those in constructor
			Iters:
				does not cope with access misses
				accept slice for getitem
				simple syntactical method of resolving Iters to something indexable without list, tuple, iter functions
		log:
			asynchronous write-out behaviors
			add <file>.<module>.<function> log origin detection to log format output
		HistoryProp: initial implementation; records the series of values assigned via given function
		PropertyDelta: initial implementation; assignment behavior returns true only when value needs to change

issues:
		EXDict:
			EXDict['missing'] != EXDict.missing;  right part raises exception
		RepeatTimer:
			No known mechanism to stop long running threads; required for force stop features and action list
			in spawning thread returned result is somehow None for only a brief
			moment causing race condition if spawned thread is checking result
			Worker() function should take arbitrary arg list
		restructure:
			struct: fix constructor params and iterable keys; currently not able to support generic usecases


'''
