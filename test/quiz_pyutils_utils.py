from pyutils.output.quiz import *

# incl = '.*iter:ijoin'
# incl = '.*iter:irepeats'
# incl = '.*:general:splits'
# incl = 'ops:base'
# incl = 'ops:parse:resolve'
# ss_incl = 'str(:rxcls(:\w+)?)?'
# incl = 'map:group'
# incl = 'map:togroup'
# incl = 'map:into'
# incl = 'path:glob'
# incl = 'cls:ibasecls'
incl = 'cls:anno'
incl = 'alyz:fmts'
incl = 'str:kkss'

quiz = Quiz(incl=incl)

if quiz('alyz'):
	from pyutils.utilities.alyzutils import *
	
	if quiz(':fmts'):
		print(fq_pln_warn())
		fp_pln_warn()
		print(fq_kfuncpln_error())
		fp_kfuncpln_error()
	
if quiz('iter'):
	from pyutils.utilities.iterutils import *
	
	if quiz(':general'):
		if quiz('::splits'):
			from pyutils.output.joins import *
			
			# PJ.jlnl(splits('a b c,  d e f'))
			PJ.jlnl(sjx('a b c,  d e f'))
			PJ.jlnl(sjx('a b c,  d e f;  lmn,  xyz', ';, '))
		
	if quiz(':irep irepeat'):
		rep_add4 = lrepeat([1,2,3], '+4')
		rep_mul4 = lrepeat([1,2,3], '*4')
		print(f'lrepeat([1,2,3], \'+4\') => {rep_add4}')
		print(f'lrepeat([1,2,3], \'*4\') => {rep_mul4}')
		assert rep_mul4 == lrepeat([1,2,3], '4') == lrepeat([1,2,3], 4)
		
	if quiz(':irepeats'):
		reps_a = lrepeats([1,'abc',1], '6 1 1')		# => [1, 1, 1, 1, 1, 1, 'abc', 1]
		reps_b = lrepeats([1,'abc',1], '6 & 1')		# => [1, 1, 1, 1, 1, 1, 'a', 'b', 'c', 1]
		print(f'lrepeats([1,\'abc\',1], \'6 1 1\') => {reps_a}')
		print(f'lrepeats([1,\'abc\',1], \'6 & 1\') => {reps_b}')
	
	if quiz(':ijoin'):
		print('%-24s' % ' '.join(ijoin(['abc'], sep=','))				,'a b c')
		print('%-24s' % ' '.join(ijoin('abc', sep=','))					,'a , b , c')
		print('%-24s' % ' '.join(ijoin('abc', sep=',', depth=0))		,'a b c')
		print('%-24s' % ' '.join(ijoin('abc', sep=',', depth=1))		,'a , b , c')
		print('%-24s' % ' '.join(ijoin(['abc','def'], sep=','))			,'a b c , d e f')
		print('%-24s' % ' '.join(ijoin('abc', 'def', sep=','))			,'a b c , d e f')
		print('%-24s' % ' '.join(ijoin('abc', 'def', sep=',', depth=0))	,'abc def')
		print('%-24s' % ' '.join(ijoin('abc', 'def', sep=',', depth=1))	,'a b c , d e f')
		print('%-24s' % ' '.join(ijoin('abc', 'def', sep=',', depth=2))	,'a , b , c , d , e , f')
	
	if quiz(':sjoin'):
		print(sjoininter('abcdef', sep=',', pfx='first: ', sep_pfx='\nmid:   ', sep_sfx='\nlast:  ', sfx='!'))
	
	if quiz(':convert iters'):
		import asyncio
		async def iter_of_getter(getr,non=...):
			while True:
				v = getr()
				while v is not non:
					yield v
					v = getr()
				await asyncio.sleep(.1)  # todo: determine method to signal instantly
		async def iter_of_gen(iev,get_step,stepper,rate=.1):
			# todo:
			step_now = get_step()
			step = stepper()
			while True:	# for v in iev:
				v = iev.send(...)
				yield v
				while step_now <= step:
					await asyncio.sleep(rate)
					step_now = get_step()
				step = stepper()
				
		def gen_of_iter(iev):
			# todo:
			for v in iev:
				send(v_out)
				v_in = yield v
	
if quiz('seq'):
	from pyutils.utilities.sequtils import *
	abcs = 'abcdefghijklmnopqrstuvwxyz'
	
	if quiz(':invert'):
		print(invertj(range(3,-3), abcs))
		print(invertj(range(3,-3), list(abcs)))
		print(invert(range(3,-3), abcs))
		
	if quiz(':less'):
		print(less(abcs))
		print(lessa(abcs))
		print(lessb(abcs))
		
if quiz('str'):
	from pyutils.utilities.strutils import *
	from pyutils.output.joins import *
	
	if quiz(':kkss'):
		# resolve inputs
		ok			= Vars(
			ab		= (['name','email'],['age','height']),
			c		= ([],['age','name','height','email']), )
		# resolve outputs
		o			= Vars(
			a		= kkss('name email : age height'),
			b		= kkss(':age , name:height , email'),
			c		= kkss(':age :name :height :email'), )
		PJ.jj_dict_nl(o)
		assert o.a == o.b == ok.ab, (o.a,o.b)
		assert o.c == ok.c, o.c
	
	if quiz(':rx'):
		from pyutils.utilities.strutils import *
		print([*RX.finditer(r'(?P<chr>\w+)', 'abc 012',key=1)])
	
	if quiz(':rxcls'):
		if quiz('::abrv'):
			
			f = rxcls_of_rxabrv
			io = [
				[f('l'),			r'[a-z]', ],
				[f('l_'),			r'[a-z_]', ],
				[f('l_',None),		r'a-z_', ],
				[f('pd'),			r'[A-Z0-9]', ],
				[f('npd'),			r'[^A-Z0-9]', ],
				[f('npd',esc=1),	r'[^A-Z\d]', ],
			]
			PJ.j_list_nl(io)
			assert all(i == o for i,o in io)
			
		if quiz('::tmpl'):
			rx = rx_of_tmplrxcls(r'city/state/zip: $lp+/$p$p/$d{5}')
			assert rx ==         r'city/state/zip: [a-zA-Z]+/[A-Z][A-Z]/[0-9]{5}'
			assert re.search(rx,  'city/state/zip: Abbeville/AL/36310')
			
		if quiz('::rx_utils'):
			RX.findall(r'$l', 'a B c D')	# => ['a', 'c']
			RX.findall(r'$p', 'a B c D')	# => ['B', 'D']
			RX.findall(r'$p$', 'a B c D')	# => ['D']
			rx_findall(r'$p$', 'a B c D')	# => ['D']

if quiz('ops'):
	from pyutils.utilities.oputils import *
	from pyutils.structures.maps import *
	
	pa = Vars(
		kk		= 'abcd',
		fftf	= [False, '', 'false. psych!!', 0],  # truth:  False,False,True,False
		ss_expr	= 'or  a  (and  c  d  b)',
	)
	pa.update(at=mapeach(pa.kk,pa.fftf))
	if quiz(':base'):
		
		out_or = orbs(pa.fftf)
		out_and = andbs(pa.fftf)
		print(out_or)
		print(out_and)
		
	if quiz(':parse'):
		out_expr = parse_expr(pa.ss_expr)
		print(out_expr)
		
		if quiz('::resolve'):
			out_val = resolve(*out_expr,to_val=pa.at)
			print(repr(out_val))
		
if quiz('calls'):
	if quiz(':clone'):
		
		# todo: RELO
		def copy_func(func, default_func=None, default_pa=None, default_ka=None, **ka):
			# resolve inputs
			default_func = default_func or func
			default_pa = default_func.__defaults__ if default_pa is None else default_pa
			default_ka = default_func.__kwdefaults__ if default_ka is None and not ka else dict(default_ka or {}, **ka)
			default_ka = dict(func.__kwdefaults__, **default_ka)  # all keys are required
			
			# resolve output
			func_out = FunctionType(
				func.__code__,
				func.__globals__,
				name=func.__name__,
				closure=func.__closure__,
				argdefs=tuple(default_pa), )
			func_out.__kwdefaults__ = default_ka
			func_out = functools.update_wrapper(func_out, func)
			return func_out
		
		def func_a(pa0, pa1, pa2, ka0='ka0', *pa, ka1='ka1', ka2='ka2'):
			return locals()
		func_b = copy_func(func_a, default_pa=(6,7,8,9), ka1='OKAY1')
		
		print(func_a(1,2,3,4,5))
		print(func_b())
	
	# 	l = list(isubts(10, [4,3,2,1,1,1,1], to_out=lambda a,b,ab: atleast(0.,a,b)))
	# 	print(l)
	
	# 	print(list(segments(range(10), 2, repeats=True)))
	# 	print(list(segments(range(20), [1, 2, 3], repeats=2)))
	# 	print(list(segments(range(100), [[[5, 4], 3], 2, 1])))
	...

if quiz('map'):
	from pyutils.structures.maps import *
	from pyutils.output.testing import *
	
	if quiz(':togroup'):
		args,into_kgrp = 'abcdef', into_kgroup_of(in_grps=['a d','b e','c f'])
		out,out_fail =  cxpas(into_kgrp,args), (...,1,2,0,1,2)
		ka = dict(fmt_post="into_kgroup_of(['a d','b e','c f'])({1}) => {0}")
		
		cxpas(qual_logerr, out,out,      args, **ka)
		cxpas(qual_logerr, out,out_fail, args, **ka)
		
	if quiz(':group'):
		# resolve inputs
		args = Vars(
			a_bc		= (dict(a=0,b=1,c=2),tss('a')),
		)
		ka = dict(fmt_post="groupmap(['a d','b e','c f'])({1}) => {0}")
		
		# resolve output
		out = groupmap(*args.a_bc)
		print(out)
		
		# log output
		cxpas(qual_logerr, out,out, args, **ka)
		
	if quiz(':into'):
		# resolve input
		i						= Vars(
			modls				= Vars(
				pykit_a_defs	= 'pyutils.defs.\w+',
				pykit_b_utils	= 'pyutils.utils.\w+  {pykit_a_defs}',
				pykit_c_devcs	= 'pyutils.devcs.\w+  {pykit_b_utils}', ),
			into				= Vars(
				vv0				= remap_of_fmtk,
				vv1				= remap_of_rrx(r'[.]:\.  pykit:py(kit|utils)', at=1,seps=' :'),
				kk0				= remap_of_rrx(r'(^$l+_$l)_.* : \g<1>',seps=';:', alias=on),
				kk1				= remap_of_rrx(r'(^$l+)_$l\_(\w+) : \g<1>_\g<2>',seps=';:'), ), )
		ok						= Vars()
		o						= Vars()
		
		# resolve output
		o.vv = Vars(i.modls)
		o.vv = Vars(i.into.vv0(o.vv))
		o.vv = Vars(i.into.vv1(o.vv))
		o.kk = Vars(o.vv)
		o.kk = Vars(i.into.kk0(o.kk))
		o.kk = Vars(i.into.kk1(o.kk))
		
		PJ.any_nl(o)
		# assert ok == o, ok

if quiz('path'):
	if quiz(':glob'):
		from pyutils.utilities.pathutils import *
		from pyutils.output.joins import *
		
		pp_glob = (
			'~/.config/xfce4/*.rc '
			'~/.config/menus/applications-merged/*.menu ')
		
		pp_globs = 'globs:',*map(abs_path,ss(pp_glob))
		pp = '','paths:',*like_pp(pp_glob)
		PJ.jnl(pp_globs)
		PJ.jnl(pp)

if quiz('cls'):
	@dataclass
	class A:
		d: 'A'		= None
		e: 'A'		= None
	@dataclass
	class B(A):
		b: 'B'		= None
		c: 'B'		= None
		d: 'B'		= None
	@dataclass
	class C(B):
		a: 'C'		= None
		b: 'C'		= None
	@dataclass
	class D:
		f: 'D'		= None
	@dataclass
	class E(D,C):	pass
	
	i				= Vars(c=C(),e=E())
	
	o				= Vars()
	if quiz(':ibasecls'):
		o.all			= list(ibasecls(i.e))
		PJ.any(o)
		
	if quiz(':anno'):
		from pyutils.utilities.clsutils import *
		from pyutils.structures.maps import *
		from pyutils.output.joins import *
		
		# resolve inputs
		
		# resolve output
		o.a_cls			= annos(C, {A})
		o.a				= annos(i.c, {A})
		o.ac			= annos(i.c, hss('A C'))
		o.abc			= annos(i.c, hss('A B C'))
		o.all			= annos(i.e, None)
		o.all_prio_base	= annos(i.e, None, base_prio=no)
		o.all_prio_sub	= annos(i.e, None, base_prio=yes)
		assert o.a == o.a_cls
		assert o.all == o.all_prio_base
		PJ.any_nl(o)
		
