'''
Custom data structure extensions.
author: Lee Hailey
'''
import re
import types
import itertools
from collections import OrderedDict as odict
from collections.abc import Hashable
from pyutils.structures.odicts import *
from pyutils.patterns.decor import *
from pyutils.defs.itemdefs import canhash

class XObj:
	''' Extendable object with modifiable members '''
	def __init__(self, **kwargs):
		self.update(**kwargs)

	def update(self, **kwargs):
		self.__dict__.update({k:v for k,v in kwargs.items() if not k == 'self'})

	def __getitem__(self, item):
		result = self.__dict__[item]
		return result


class XList(list):
	''' Basic extensions to list capability.
		Augmented with:
		- pattern matching/filtering with regular expressions
		- methods double as static factory functions
	'''
	
	@staticmethod_optional_self
	def get(self, slice): pass
	
	@staticmethod_optional_self
	def get(self, slice): pass

	@staticmethod_optional_self
	def like(self, pattern, matching=True):
		''' produce subset of items in self LIKE the given pattern '''
		result = XList([i for i in self if isinstance(i, str) and (not matching ^ bool(re.search(r""+pattern, i)))])
		return result

	@staticmethod_optional_self
	def unlike(self, pattern):
		''' produce subset of in self UNLIKE the given pattern '''
		return XList.like(self, pattern, matching=False)
	
	@staticmethod_optional_self
	def split(self, delim=..., count=-1):
		''' '''
		results, result, inds = [], [], []
		for ind, item in enumerate(self):
			if item == delim:
				inds += [ind]
				count -= 1
				if count == 0:
					break
		inds += [None]
		result = [self[0:inds[0]]] + [self[start+1:stop] for start, stop in zip(inds[:-1], inds[1:])]
		return result

	@staticmethod_optional_self
	def arrangments(self, order):
		''' produce head, remainder & tail lists matching an order having one '...' (Elipsis)
			item between head and tail order items.
			XList.arrangements(range(9), [9,8, ..., 2, 1])  =>  [[9, 8], [3, 4, 5, 6, 7], [2, 1]]
		'''
		assert order.count(...) <= 1, 'found multiple remainder set keys \'...\'  Only one is allowed.'

		if ... not in order:
			return order
		
		remainder_ind = order.index(...)
		remainder = list(self)
		pre_post_groups = XList.split(order, ...)
		for group in pre_post_groups:
			for key in group:
				if key in remainder:
					remainder.remove(key)
					
		result = [group for group in [pre_post_groups[0], remainder, pre_post_groups[1]] if len(group)]
		return result
	
	@staticmethod_optional_self
	def arrangment(self, order):
		''' produce a list matching an order having one '...' (Elipsis) item between head and tail order items.
			XList.arrangement(range(9), [9,8, ..., 2, 1])  =>  [9, 8, 3, 4, 5, 6, 7, 2, 1]
		'''
		return list(itertools.chain( *XList.arrangments(self, order) ))


class XDict(dict):
	''' Basic extensions to dictionary capability.
		Augmented with:
		- several rudimentary set and list behaviors in addition to all dict behaviors
		- bitwise and arithmetic operations
		- pattern matching/filtering with regular expressions
		- methods double as static factory functions
	'''

	def __init__(self, *data, **kws):
		''' construct; multiple dict parameter inputs allowed along with key word dict '''
		reduce_op = XDict.union
		if 'reduce_op' in kws:
			reduce_op = kws.pop('reduce_op') # todo: currently not in use

		# consolidation required when multiple dict parameter inputs
		if len(data)+bool(kws) >= 2:
			for d in data:
				kws.update(d)
			data = (kws,)
		dict.__init__(self, *data)

	@staticmethod
	def init_kwargs(self, kwargs, exclude_kws=None):
		''' constructor initialization helper '''
		self.__dict__.update(XDict(kwargs) ^ (['self']+(exclude_kws or [])))

	@staticmethod_optional_self
	def is_superset(self, keys):
		''' returns True iff keys contains every item in self.keys '''
		if isinstance(self, dict):
			self = dict(self)
		result = set(self.keys()).issuperset(set(keys))

	@staticmethod_optional_self
	def is_subset(self, keys):
		''' returns True iff self.keys contains every item in keys '''
		if isinstance(self, dict):
			self = dict(self)
		result = set(self.keys()).issubset(set(keys))
		return result

	@staticmethod_optional_self
	def __contains__(self, keys):
		''' when keys is not hashable return XDict.is_subset(keys); otherwise use default dict behavior '''
		try:
			# if keys is not hashable user probably wants xdict behavior
			if not isinstance(keys, Hashable):
				result = XDict.is_subset(self, keys)
				return result
		except Exception as xdict_error:
			pass

		# resort to default dict behavior if something goes wrong
		try:
			result = dict.__contains__(self, keys)
			return result
		except:
			# maybe user intended to use xdict behavior; print xdict exception and reraise dict exception
			print(xdict_error)
			raise

	@staticmethod_optional_self
	def is_subset_identity(self, a_dict):
		''' returns true if self is a subset of a_dict with identical values '''
		if len(self) > len(a_dict):
			return False		# self cannot be a subset of a_dict with more items; avoid iterations

		for k, v in self.items():
			if v == a_dict.get(k, '"k" NOT IN a_dict'):
				return False	# exit returning False on first mismatch

		return True

	@staticmethod_optional_self
	def is_superset_identity(self, a_dict):
		''' returns true if self is a superset of a_dict with identical values '''
		result = XDict.is_subset_identity(a_dict, self)
		return result

	@staticmethod_optional_self
	def is_equal(self, a_dict):
		''' returns true if all keys and key_values are equal '''
		if not isinstance(a_dict, dict) or len(self) != len(a_dict):
			return False		# self cannot be a equal to a_dict with more items; avoid iterations

		result = self.is_subset_identity(a_dict)
		return result

	@staticmethod_optional_self
	def values(self, order=None):
		''' return values in the order provided; If order is None return native dict.values() '''
		if not isinstance(self, dict):
			self = XDict(self)
		result = dict.values(self) if order is None else [self[key] for key in order]
		return result
	
	@staticmethod_optional_self
	def set_links(self, keys, value):
		''' link multiple keys to single value '''
		for key in keys:
			self[key] = value
		return self
	
	@staticmethod_optional_self
	def get_links_map(self, to_keys=None, to_values=None):
		''' get reverse mapping of value -> keys items. Roughly [ret[v].append(k) for k, v in self.items()] '''
		links_map = XDict()
		for key, value in self.items():
			value =  value if not to_values else to_values(value)
			if not canhash(value): continue
			
			key = key if not to_keys else to_keys(key)
			links_map.setdefault(value, []).append(key)
		return links_map
	
	@staticmethod_optional_self
	def get_link_map(self, to_keys=None, to_values=None, overwrite_key=lambda old_key, new_key: True):
		''' get reverse mapping of value -> key items. Roughly {v:k for k, v in self.items()} '''
		link_map = XDict()
		for key, value in self.items():
			value = value if not to_values else to_values(value)
			if not hasattr(value, '__hash__'): continue
			
			key = key if not to_keys else to_keys(key)
			if value not in link_map or not overwrite_key or overwrite_key(link_map[value], key):
				link_map[value] = key
		return link_map

	@staticmethod_optional_self
	def union(self, a_dict):
		''' update with items from a_dict; produce dict equivalent to self.update(a_dict); in conflicts use a_dict value '''
		result = XDict(self)
		result.update(a_dict)
		return result

	@staticmethod_optional_self
	def intersection(self, keys, ctor=None):
		''' produce subset of items common to both self and keys '''
		ctor = ctor or self.__class__
		result = ctor([[k,v] for k,v in self.items() if k in keys])
		return result

	@staticmethod_optional_self
	def difference(self, keys):
		''' produce subset of items in self not found in keys '''
		result = XDict({k:v for k,v in self.items() if not k in keys})
		return result

	@staticmethod_optional_self
	def matching(self, values):
		''' return self after removing any items with a value contained in a_list '''
		result = XDict(self)
		for k, v in [*result.items()]:
			if v not in values:
				result.pop(k)
		return result

	@staticmethod_optional_self
	def remove(self, values):
		''' return self having removed any items with a value contained in a_list '''
		for k, v in [*self.items()]:
			if v in values:
				self.pop(k)
		return self

	@staticmethod_optional_self
	def remove_copy(self, values):
		''' return a copy of xdict without any items with a value contained in a_list '''
		l = list(self.items())
		result = XDict({k:v for k, v  in self.items() if v not in values})
		return result

	@staticmethod_optional_self
	def replace(self, a_dict):
		''' similar to update except will only modify existing values; does not make insertions '''
		common_keys = set(self.keys()) & set(a_dict.keys())
		for k in common_keys:
			self[k] = a_dict[k]
		return self

	@staticmethod_optional_self
	def subsets(self, *key_sets, insert_missing=True, default_value=None):
		''' separate dict with given keys and append the remaining items onto the end '''
		# print(f'XDict.subsets(self={self}, key_sets={key_sets})')
		other = XDict(self)
		results = []
		for key_set in key_sets:
			key_set_dict = XDict()
			for k in key_set:
				if k in other:
					key_set_dict[k] = other.pop(k)
				elif insert_missing:
					key_set_dict[k] = default_value
			results.append(key_set_dict)

		results.append(other)
		return results
	
	@staticmethod_optional_self
	def arrangments(self, order):
		''' separate dict with given keys and append the remaining items onto the end '''
		ordered_dict = odict(self)
		key_arrangments = XList.arrangments(ordered_dict, order)
		results = []
		for key_arrangment in key_arrangments:
			result = []
			for key in key_arrangment:
				result.append([key, ordered_dict[key]])
			results.append(result)
			
		return results
	
	@staticmethod_optional_self
	def arrangment(self, order):
		''' separate dict with given keys and append the remaining items onto the end '''
		result = XDict.arrangments(self, order)
		result = list(itertools.chain(*result))
		return result
	
	@staticmethod_optional_self
	def like(self, pattern, matching=True):
		''' produce subset of items in self with keys LIKE the given pattern '''
		result = self if isinstance(self, dict) else dict(self)
		result = XDict({k:v for k,v in result.items() if (not matching ^ bool(re.search(r""+pattern, str(k))))})
		return result

	@staticmethod_optional_self
	def unlike(self, pattern):
		''' produce subset of items in self with keys UNLIKE the given pattern '''
		return XDict.like(self, pattern, matching=False)
	
	def __dir__(self):
		names = list(set(super().__dir__()) - set(dir(XDict)))
		return names

	# method aliases
	__or__	= __ror__	= union
	__and__ = __rand__	= intersection
	__xor__ = __rxor__	= difference
	__add__ = __radd__	= matching
	__sub__ = __rsub__	= remove_copy
	__lt__ = __contains__
	__gt__ = is_superset
	__le__ = is_subset_identity
	__ge__ = is_superset_identity
	__eq__ = is_equal
	value_order = values
	
	# obsolete aliases
	link = set_links
	value_keys_links = get_links_map
	value_key_links = get_link_map


def test_arrangment():
	from pprint import pprint as pp, pformat as pfmt
	
	pangrams = [
		'GQs oft lucky whiz Dr. J, ex-NBA MVP',
		'Pack my box with five dozen liquor jugs',
		'the quick brown fox jumps over the lazy dog',  ]
	print(re.sub(r'[\.\-, ]', '', pangrams[0].lower()))
	abc_unordered = list(re.sub(r'[\.\-, ]', '', pangrams[0].lower()))
	abc_pos_unordered = [[letter, ind] for ind, letter in enumerate(abc_unordered)]
	orders = [
		[..., 'a', 'b', 'c', 'x', 'y', 'z'],
		['a', 'b', 'c', 'x', 'y', 'z', ...],
		['a', 'b', 'c', ..., 'x', 'y', 'z'],  ]
	print('abc_unordered = "{}"'.format(" ".join(abc_unordered)))
	print('abc_pos_unordered = {}'.format(pfmt(abc_pos_unordered[:8], width=60)))


	print('Testing: \'XList.arrangments(abc_pos_unordered, order)\' where')
	# print('\nabc_unordered: "{}"'.format(" ".join(abc_unordered)))
	print('abc_pos_unordered = {}'.format(pfmt(abc_pos_unordered[:8], width=60)))
	
	for order in orders:
		print(f'\norder: {order}')
		arrangments = XDict.arrangments(abc_pos_unordered, order)
		arrangment = XDict.arrangment(abc_pos_unordered, order)
		print('arrangments.keys()	= {}'.format([" ".join(odict(group).keys()) for group in arrangments]))
		print('arrangments.values() = {}'.format([" ".join(ss(odict(group).values())) for group in arrangments]))
		print('arrangments.keys()	= {}'.format(" ".join(odict(arrangment).keys())))
		print('arrangments.values() = {}'.format(" ".join(ss(odict(arrangment).values()))))

if __name__ == "__main__":
	test_arrangment()
	
	exit()
	
	from pprint import pprint as pp, pformat as pfmt
	
	
	
	dict_input = [[alpha, num] for num, alpha in enumerate('abcd')]
	def test_attr_dict_access(base_dict=None):
		attr_dict = base_dict or AttrDict(dict_input)
		attr_dict.a = 100
		attr_dict.new = 'assigned in dict items'
		print(f'\nattr_dict: \n{pfmt(attr_dict, width=60)}')
		print(f'attr_dict.a:         {attr_dict.a}')
		print(f'attr_dict.new:       {attr_dict.new}')
	def test_attr_echs_num_subclass():
		class MyAttrDict(AttrEnum, dict):
			def __init__(self, *args, **kws):
				self.internal = 'assigned as member var, not dict item'
				super().__init__(*args, **kws)
		my_dict = MyAttrDict(dict_input)
		test_attr_dict_access(my_dict)
		print(f'attr_dict.internal:  {my_dict.internal}')
		
	test_attr_dict_access()
	test_attr_enum_subclass()
	
	
	exit()
	
	# IndexedDefault: basic operations
	fl_a = FullList()
	fl_b = FullList(None, [10])
	fd_a = FullDict('oops', {1:100})
	fd_b = FullDict(lambda key: '\'%s: oops\'' % key, {1:100})
	# kd = KeysDict({1:100})

	print(fl_a[0])			# => None				; not in list return default_item
	print(fl_b[0])			# => 10					; returns item at index 0 if found
	print(fd_a['MISSING'])	# => 'oops'				; key not in dict return default_item
	print(fd_b['MISSING'])	# => 'MISSING: oops'	; key not in dict return on_miss(key)
	# print(kd['MISSING'])	# => 'MISSING'			; key not in dict return given key
	# print(kd[1])			# => 100				; returns item for key '1' if found
	print()

	# AttrEnum: basic operations
	class MyAttrDict(AttrEnum, dict):
		_ptrn_type = AttrEnum.regexp
		# _is_attrs = ['^[^a-zA-Z].*?[^a-zA-Z0-9_]']
		_is_attrs = ('_', 'bar')
		pass  # augment any dict class

	mydict = MyAttrDict(a=1, b=2)  # custom user defined attr enumerated dict
	fedict = FullAttrDict(a=1, b=2)  # predefined dict augmented with index attribution and missing key default
	print(mydict)			# => {'c': 3, 'a': 1, 'b': 2}
	print(fedict)			# => {'c': 3, 'a': 1, 'b': 2}
	print(mydict.a)			# => 1
	print(fedict.b)			# => 2
	print(fedict.c)			# => None
	mydict.foobaz = 4
	mydict.foobar = 5
	print(mydict)			# => {'c': 3, 'a': 1, 'b': 2}

	# XObj: basic operations
	tokens_a = Strs('a b c')
	tokens_a += 'd e f'
	tokens_b = tokens_a + Strs('1, 2, 3', ', ')
	print(tokens_a)			# => T"a b c d e f"
	print(tokens_b)			# => T"a b c d e f 1 2 3"
	exit()
	
	# XObj: basic operations
	pass


	# XDict: basic operations
	xd0 = XDict({0:1, 1:99})							# constructor behavior remains intact
	xd1 = XDict({0:1, 1:99}).union({1:10, 2:100})		# implements set like operations
	xd2 = XDict.union({0:1, 1:99}, {1:10, 2:100})		# all ops are methods and static functions
	xd3 = XDict(xd0) | {1:10, 2:100}					# supports bitwise ops as shorthand for dict operations; '|' == union == dict.update

	print(xd0)											# => {0:1, 1:99}
	print(xd1)											# => {0:1, 1:10, 2:200}
	print(xd2)											# => {0:1, 1:10, 2:200}
	print(xd3)											# => {0:1, 1:10, 2:200}

	xd1 += [10]											# returns subset of items having values contained in given list
	print(xd1)											# => {1:1}
	print(XDict(a=0, b=1, c=2, d=3))					# => {'a':0, 'c':2, 'b':1, 'd':3}
	print((XDict(a=0, b=1, c=2, d=3) ^ ['c']) - [0])	# => {'b':1, 'd':3}	; ^ and - returns keys and values difference respectively
	print(['c'] ^ [0] - XDict(a=0, b=1, c=2, d=3))		# => {'b':1, 'd':3}	; operations also works in reverse
	print('')


	xd4 = XDict({0:1, 1:99}, {1:10, 2:100}, a=1, b=2)
	print(xd4)
	xd5 = XDict({0:1, 1:99}, {1:10, 2:100})
	print(xd5)
	exit()

