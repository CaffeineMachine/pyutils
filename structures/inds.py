from pyutils.defs.itemdefs import Ndx
from pyutils.utilities.callutils import *
from pyutils.utilities.iterutils import *
from pyutils.utilities.alyzutils import *
# todo: rename to numbers, scales, point(er)(s), index, ranges

#region inds defs
class DefsIdx:
	okay_hash					= hash
	okay_ndx					= int,
	okay_idx					= *okay_ndx, slice
	okay_itr					= *okay_idx, nonetype
	okay_builtin				= *okay_itr, str, bytes, tuple
	N = Num						= Ndx
DDIdx = _D						= DefsIdx
#endregion

#region inds forms
def arg_is_okay(arg,okay=_D.okay_hash,cls=None):
	''' verify given *arg* satisfies *is_okay* as error_checker or set of types '''
	is_ok = True
	try:
		is_ok = (
			okay(arg) or True		if iscall(okay) else
			isinstance(arg,okay) )
	except TypeError: pass
	finally:
		if not is_ok:
			raise ValueError(f'{arg} is not okay.  {cls and cls.__name__ or "At"}._okay = {okay}')
	return is_ok

# fixme: internal members of At&Ind* needs better integration with Nums; where is start,stop,step?
# fixme: is it possible to eliminate At.args
def iindcls_of_fab(fab, atcls=None):
	def redef(vals):
		vals = atcls(vals)
		vals = fab(vals)
		return vals
	
	call = (
		redef				if atcls and fab else
		atcls				if atcls and fab is None else
		iindcls_of_fab.__get__(fab)
	)
	return call
#endregion

#region inds utils
def len_of_inds(start, stop, step=1):
	out = int((stop-start)/step + .99999999)
	return out
def str_of_inds(start=None, stop=None, step=None, *_, obj=None, fmt:str=None):
	if obj is not None:
		start,stop,step = obj.start,obj.stop,obj.step
	show_args = (
		'' if start is None else f'{start:.6f}' if not fmt and isinstance(start, float) else start,
		'' if stop is None else f'{stop:.6f}' if not fmt and isinstance(stop, float) else stop,
		'' if step is None else f'{step:.6f}' if not fmt and isinstance(step, float) else step,  )
	return ':'.join(map(fmt and ('{:%s}'%fmt).format or str, show_args))
def str_of_inds(*pa, start=nothing,stop=nothing,step=nothing,inds=nothing,fmt:str=None):
	# resolve inputs args of inds
	assert 2 <= (nothing is start is stop is step) + (nothing is inds) + (not pa), 'mix of pa,ka,inds args not allowed'
	start,stop,step = (
		pa + ((None,)*(3-len(pa)))				if nothing is start is stop is step is inds else
		[inds.start,inds.stop,inds.step]		if inds is not nothing else
		(None if i is nothing else i for i in (start,stop,step))
	)
	
	# resolve output
	show_args = (
		'' if start is None else f'{start:.6f}' if not fmt and isinstance(start,float) else start,
		'' if stop  is None else f'{stop:.6f}'  if not fmt and isinstance(stop, float) else stop,
		'' if step  is None else f'{step:.6f}'  if not fmt and isinstance(step, float) else step,  )
	return ':'.join(map(fmt and ('{:%s}'%fmt).format or str, show_args))

# todo: relo or reuse maputils
def igetitem(items, ndx=None):
	istep = iter(
		count()					if ndx is None else
		range(ndx,ndx+1)		if isnttype(ndx,slice) else
		range(ndx.start or 0,isnone(ndx.stop,put=n_max),ndx.step or 1))
	i_ind_item = enumerate(items)
	step = next(istep, None)
	while step is not None:
		ind,item = next(i_ind_item, (None,None))
		if ind is None:
			break
		elif ind == step:
			yield item
			step = next(istep, None)

str_range, len_of_range			= str_of_inds, len_of_inds
#endregion

#region inds devcs
# todo: make At, Idx, IIdx, ITo consistent with methods: init(at,to,fab,items),igetitem(..),getitem(..)
class KeyFab(Ndx):
	''' seq coll. key factory via bracket operator. otherwise called a slice '''
	# todo: consolidate with members
	#region forms
	def __class_getitem__(cls, arg):	return arg
	def __getitem__(cls, arg):			return arg
	def __getattr__(cls, arg):			return arg
	#endregion
	...

class _AtBase(Ndx):
	#region defs
	getitem,setitem				= None,None
	_okay						= _D.okay_hash
	#endregion
	#region forms
	def __init_subclass__(cls, call_get=True, to=None, fab=None, okay=None, **ka):
		# adapt interface
		cls.__call__ = cls.__call__ if cls.__call__.__name__ != '__call__' else cls.getitem
		cls.__getitem__ = cls.getitem
		cls.item = getattr(cls, 'item', cls.getitem)
		cls.set = cls.setitem
		cls.to = to or getattr(cls,'to',None)
		cls.fab = fab or getattr(cls,'fab',None)
		okay and setattr(cls,'_okay',okay)
		
		# apply members for each Ndx|NumIdx.  ie: first=0, all=slice(None,None)
		for attr in Ndx._kk:
			mbr = getattr(Ndx, attr)
			if not isinstance(mbr, cls):
				setattr(cls, attr, cls(mbr))
	#endregion
	...

class At(_AtBase):
	''' any coll. key factory via bracket operator as a container. produces any key which bracket operator accepts '''
	#region defs
	# todo: support arithmetic
	# todo: implement IndNeg as inverse of Ind
	# todo: implement AtStrs
	# todo: implement Ind01 and define l|eft,m|id.r|ight with positions .0,.5,1.
	__slots__					= ['arg']
	#endregion
	#region forms
	def __class_getitem__(cls, arg):	return cls(arg)
	def __init__(self, arg):
		assert arg_is_okay(arg, self._okay, self.__class__)
		self.arg = arg
	def __repr__(self):
		s = '%s[%s]'% (self.__class__.__name__, str_of_inds(inds=self.arg) if isinstance(self.arg,slice) else self.arg)
		return s
	def __index__(self):				return self.arg
	def __int__(self):					return self.arg.start if isinstance(self.arg, slice) else self.arg
	def __add__(self):					raise NotImplemented()
	#endregion
	#region coll
	def pick(self, items, *default):	return items.get(self.arg, *default)
	def getitem(self, items):			return items[self.arg]
	def setitem(self, items, value):	items[self.arg] = value
	def pop(self, items, *default):		return items.pop(self.arg, *default)
	def to_of(self, to):				return AtTo(self.arg, to, cls=self.__class__)
	def raw(self):						return self.arg  # yield the arg directly. use ~ or inv op as shortcut
	get							= pick
	__invert__					= raw
	#endregion
	...

class Idx(At, okay=_D.okay_idx):
	''' defines a location for indexed item coll. same as At but limited to a subset of int & slice args. '''
	#region forms
	# def __init__(self, arg):
	# 	# assert isinstance(arg, (int, slice))
	# 	assert arg_is_okay(arg, self._is_okay)
	# 	super().__init__(arg)
	#endregion
	...

class IIdx(At, okay=_D.okay_idx):
	''' defines a location for indexed item coll. same as At but limited to a subset of int & slice args.
		first arg is interpreted as iterator slice
	'''
	#region forms
	def of(self, items, to=None, fab=None):
		# resolve inputs
		to,fab = isfalse(to,orput=to or self.to), isfalse(fab,orput=fab or self.fab)
		
		# resolve output
		vals = self.igetitem(items)
		vals = vals if not to else map(to,vals)
		vals = vals if not fab else fab(vals)
		return vals
	def igetitem(self, items):		return igetitem(items,ndx=self.arg)
	__call__					= of
	#endregion
	...
class ITo(IIdx, okay=_D.okay_itr, call_get=False):
	''' get value(s) given *at* index converted with given *to*
		first arg is interpreted as converter for each iterator value
	'''
	#region defs
	at							= property(lambda self: self.arg,lambda self,val: setattr(self,'arg',val))  # todo: choose arg or at
	#endregion
	#region forms
	def __init__(self, to, items=None, at=None):
		self.to = to
		self.items = items
		super().__init__(at)
	def igetitem(self, items):		return (v[self.to] for v in igetitem(items))
	def of(self, items=None, to=None, fab=None):
		# resolve inputs
		items,to,fab = isnone(items,put=self.items), isfalse(to,orput=self.to), isfalse(fab,orput=self.fab)
		
		# resolve output
		vals = self.igetitem(items)
		vals = vals if not to else map(to,vals)
		vals = vals if not fab else fab(vals)
		return vals
	def apply(self, items): raise NotImplementedError()
	__call__					= of
	#endregion
	...

class AtTo:
	''' get value(s) given *at* key converted with given *to* '''
	#region forms
	def __init__(self, at=None, to=None, items=None, cls=At):
		self.get_at = cls(at)
		self.to = to
		self.items = items
	def apply(self, items):
		vals = self.get_at(items)
		vals = vals if self.to is None else self.to(vals)
		return vals
	__call__					= apply
	#endregion
	...

K = Key							= KeyFab()
_dict,_set,_list,_tuple			= dict,set,list,tuple
if asis(True):
	Ind,IInd					= Idx,IIdx
	(	DIdx,      HIdx,     LIdx,      TIdx,       ) = (
		IIdx.d,    IIdx.h,   IIdx.l,    IIdx.t,     ) = (
		IIdx.dict, IIdx.set, IIdx.list, IIdx.tuple, ) = (
		tuple(type(pfx+'Idx',(IIdx,),dict(fab=fab)) for pfx,fab in zip('DHLT',[dict,set,list,tuple]))  )
	(	DTo,      HTo,     LTo,      TTo,       ) = (
		ITo.d,    ITo.h,   ITo.l,    IIdx.t,    ) = (
		ITo.dict, ITo.set, ITo.list, ITo.tuple, ) = (
		tuple(type(pfx+'Idx',(ITo,),dict(fab=fab)) for pfx,fab in zip('DHLT',[dict,set,list,tuple]))  )

assert _dict is dict and _set is set and _list is list and _tuple is tuple, 'Failed builtins sanity check'
#endregion
