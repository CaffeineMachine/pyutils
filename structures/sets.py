from dataclasses import dataclass

from pyutils.structures.maps import *
from pyutils.structures.strs import *

#region set defs
_NS							= Vars()
class DefsSet:
	#region namespace defs
	class Strs:
		chs					= tss('&      |     ^    -    ~     ')
		kk_fmt				= tss('and{}  or{}  xor  sub  rsub  ', Fmt)
		kk					=      andb,  orb,  xor, sub, rsub,  = (
							  ccxarg(kk_fmt,'b') )
		_a					= repr(kk[-1])
		ops					= ccxarg(kk_fmt,'')
	
	SS						= Strs
	#endregion
	#region coll defs
	ops						= tss(SS.ops, lambda k: getattr(set,f'__{k}__'))
	
	ch_at = chs				= mapvars(SS.ops+SS.kk+SS.chs, SS.chs*3)
	op_at = at				= mapvars(SS.ops+SS.kk+SS.chs, ops*3)
	# opval_at				= mapvars(SS.ops+SS.kk+SS.chs, opval*3)
	opval_at				= Vars(
								andb	= lambda a,b:     a and     b,
								orb		= lambda a,b:     a or      b,
								xor		= lambda a,b:     a !=      b,
								sub		= lambda a,b:     a and not b,
								rsub	= lambda a,b:     b and not a,
							)
	#endregion
	...

DDH = _DD					= DefsSet
SSH							= DDH.SS
#endregion

#region set devcs
@dataclass
class Sets:
	incl: iter				= None
	excl: iter				= None
	op: any					= None
	other: set				= None
	def __post_init__(self):
		self.reconfig()
	def reconfig(self):
		self.incl = self.incl if self.incl is None or isstr(self.incl) else set(self.incl)  # todo: compile
		self.excl = self.excl if self.excl is None or isstr(self.excl) else set(self.excl)  # todo: compile
	@classmethod
	def of(cls,obj,*pa,**ka):		return obj
	def __ior__(self,other):		self.op,self.other = DDH.chs.orb,self.of(other)
	def __iand__(self,other):		self.op,self.other = DDH.chs.andb,self.of(other)
	def __isub__(self,other):		self.op,self.other = DDH.chs.sub,self.of(other)
	def __ixor__(self,other):		self.op,self.other = DDH.chs.xor,self.of(other)
	def __rsub__(self,other):		self.op,self.other = DDH.chs.rsub,self.of(other)
	
	def contain(self, vals):
		found = vals in self.incl
		return found
	def contains(self, vals):
		has_out = has_a = self.contain(vals)
		if self.op:
			has_b = self.other.contains(vals)
			set_op = DDH.opval_at[self.op]
			has_out = set_op(has_a,has_b)
		return has_out
	__contains__			= contains
#endregion
