from __future__ import annotations
from copy import copy

from pyutils.defs.typedefs import *
from pyutils.defs.funcdefs import *
from pyutils.utilities.numutils import *
from pyutils.utilities.oputils import *
from pyutils.patterns.iface import MathIx
from pyutils.output.logs import *
from pyutils.structures.inds import *

#region nums defs
bound,bounds					= lohi_for,lohis_for  # todo: purge
class Incl:
	idxs = idx_lo,idx_hi		= 0,1
	biteqs = biteq_lo,biteq_hi	= 0b10,0b01
	ne_ne = ne					= (False*biteq_lo) | (False*biteq_hi)  # => 0b00
	ne_eq						= (False*biteq_lo) | ( True*biteq_hi)  # => 0b01
	eq_ne						= ( True*biteq_lo) | (False*biteq_hi)  # => 0b10
	eq_eq = eq = default		= ( True*biteq_lo) | ( True*biteq_hi)  # => 0b11
	_ss_false					= {'0','False','F'}
	
	which_of_incl				= lambda incl,idx_incl: (
									incl & Incl.biteqs[idx_incl]	if isint(incl) else
									False							if incl[idx_incl] in Incl._ss_false else
									bool(incl[idx_incl]) )
	lohi_of_incl				= lambda incl: [Incl.which_of_incl(incl,idx) for idx in Incl.idxs]

_fmtr_arg_err					= '{}() got multiple values for argument {!r}'.format

@clscolls(no)
class DefsNums:
	# iter = i					= On and (fp_pln_warn('collect on DefsNums') and list) or iter
	class ops:
		add_mul_exp				= '+   *  **  '
		exp_mul_add				= '**  *  +   '
		default					= add_mul_exp
		
DDNN = _D						= DefsNums
_i								= DefsNums.i
_map,_zip						= collectors([map,zip],DDNN.i)
#endregion

#region num eval utils
op_of_k							= lambda k_op: globals()[k_op]  # fixme: should source from oputils.DDOP.op_at
def nto_of_opsnn(ops=DDNN.ops.default,nn=None,n=None,rev=False,flt_ops=False):
	''' stage ->nto|nterm (or num conversion) for a single polynomial term '''
	if nn is None:	return stage_call(nto_of_opsnn,ops,rev=rev)
	
	# resolve inputs
	ops = ops if isntstr(ops) else cxpas(DDOP.oprevsafe_at.__getitem__,ss(ops))
	nn = tuple(nn) if not isinstance(nn,complex) else (nn.real,nn.imag)
	nn = nn if not rev else reversed(nn)
	numops = tuple(isstr(op) and getattr(n,op) or op.__get__(n) for n,op in zip(nn,ops))
	
	# resolve output
	def n_to(n):
		n_out = n
		for num_op in numops:
			n_out = num_op(n_out)
		return n_out
	result = n_to if n is None else n_to(n)
	return result

nto_of							= nto_of_opsnn()
#endregion

#region nums forms
def snum_of(num:int|float, spec_flt='f', spec_int='d'):
	''' repr a number safely '''
	t_num = type(num)
	s_num = (
		f'{num:{spec_int}}'[:64]		if t_num is int or t_num is bool else
		f'{num:{spec_flt}}'[:64]		if t_num is float or t_num is complex else
		
		throw(ValueError(f's_of_num: Not a number:{num!r}')) )
	return s_num
def snums_of(nums, fab_pa=None):
	''' repr multiple numbers safely '''
	s_nums = map(snum_of, nums)
	s_nums = s_nums if not fab_pa else fab_pa(s_nums)
	return s_nums
def snumpka_of(*nums_pa, fab_pa=None, fab_ka=dict, **nums_ka):
	''' safe num pos & key args formatter '''
	snums_pa, snums_ka = map(snum_of, nums_pa), ((k,snum_of(num)) for k,num in nums_ka.items())
	snums_pa = snums_pa if not fab_pa else fab_pa(snums_pa)
	snums_ka = nums_ka and snums_ka if not nums_ka else fab_ka(snums_ka)  # when *nums_ka* is empty noop and return asis
	return snums_pa, snums_ka
def eval_expr(expr:str, *nums_pa, **nums_ka):
	''' safe evaluation of number expression. protects against arbitrary number args; only uses safe formatters '''
	snums_pa, snums_ka = snumpka_of(*nums_pa, **nums_ka)
	snum_expr = isstr(expr, put=expr.format)(*snums_pa, **snums_ka)
	value = eval(snum_expr)
	return value

def boundz_of_inums(inums, fab=None):
	boundz = zip(*map(boundpa_of_nums, inums))
	boundz = boundz if not fab else (fab is True and Bound or fab)(boundz)
	return boundz
def len_of_nn(nn:NN):			return len_of_inds(nn.start, nn.stop, nn.step)

def bound_if_consec(consec,fill=miss,filler=throws(ValueError),is_consec=None,atleast=4,fab=None):
	''' produce bounds when given seq *consec* satisfies *is_consec* for every adjacent pair
		:param consec:    potentially consec seq
		:param fill:      output for non-consec seq
		:param filler:    output getter for non-consec seq
		:param is_consec: qualifies each adjacent pair of vals as consec
		:param atleast:   number of vals to require at minimum to qualify as conseq 
		:param fab:       when conseq apply this to output
	'''
	# resolve inputs
	consec = tuple(consec)
	is_consec = is_consec or (lambda a,b: a<b and a.__sizeof__()==b.__sizeof__())
	
	# determine if consec:  => all_consec
	all_consec = atleast <= len(consec) 
	for l,r in all_consec and islide(consec) or ():
		if not all_consec: break
		all_consec &= is_consec(l,r)
			
	# resolve output
	out = (
		(consec[0],consec[-1])			if all_consec else
		filler(consec)					if filler else
		consec							if fill is miss else
		fill )
	out = out if not fab or not all_consec else fab(out)
	return out

def arith_transform_of(arg):
	gain, bias = (
		(arg.real,arg.imag)		if iscomplex(arg) else
		(arg, 0.)				if istype(arg, (int,float)) else
		arg						if isiter(arg) else
		(None,None) )
	return gain, bias
def arith_transform_on(arith_x, flt:float):
	gain, bias = arith_x
	result = flt*gain + bias
	return result

def imuls_of(num, muls=1, base=2):
	num_nth = num
	muls = muls if isntint(muls) else [base]*muls
	for mul in muls:
		yield num_nth
		num_nth *= mul

def idivs_of(num, denoms=1, base=2):
	num_nth = num
	denoms = denoms if isntint(denoms) else [base]*denoms
	for denom in denoms:
		yield num_nth
		num_nth /= denom

def numargs_of(args,sfx=miss, rep=2):
	args_out = (
		args		if isiter(args) else
		[args]*rep )
	return args_out
def expand_args(args,sfx=miss, rep=2):
	yield from args if isiter(args) else [args]*rep
	while sfx is not miss:
		yield sfx
def add_nums(lefts,rights,**ka):
	out = (l if r is None else r if l is None else l+r for l,r in zip(lefts,expand_args(rights,sfx=None,**ka)))
	return out
def mul_nums(lefts,rights,**ka):
	out = (l if r is None else r if l is None else l*r for l,r in zip(lefts,expand_args(rights,sfx=None,**ka)))
	return out
add_nn,mul_nn					= add_nums,mul_nums

factor,project,reflect,convert		= factor_of_bound,project_of_bound,reflect_of_bound,convert_of_bounds
s_of_num,spa_of_nums,spka_of_nums	= snum_of,snums_of,snumpka_of
into_of_nums, intoz_of_inums		= bound_of_nums, boundz_of_inums
ramp,curb							= project,reflect
mid									= project.__get__(.5)
#endregion

#region bound devcs
# todo: integrate position.X,XB,XD with nums.Bound
class Bound(list):
	''' parameters defining each end of a number range
		Note: distinct from Nums in that [*Bounds(*pa)] == [*pa]; but otherwise similar
	'''
	#region class mbrs
	_incl							= Incl.default
	a = start=low=lo				= property(lambda self: self[0], lambda self, val: self.__setitem__(0, val))
	b = stop=high=hi				= property(lambda self: self[1], lambda self, val: self.__setitem__(1, val))
	
	delta = diff					= property(lambda self: self[1] - self[0])
	vec								= property(lambda self: (self[0], self[1]-self[0]))
	range							= property(lambda self: range(*self))
	slice							= property(lambda self: slice(*self))
	len								= property(lambda self: len_of_inds(*self))
	mid								= property(mid_of_bound)
	has = __contains__				= property(has_of_bound)
	
	pa								= property(list) # todo: remove all references
	#endregion
	def __init_subclass__(cls, incl=_incl):
		cls._incl = incl
	def __init__(self, *pa, start=0, stop=None,incl=None,fabs=None):
		pa = pa if not pa or not iscoll(pa[0]) else list(pa[0])
		if len(pa) <= 1:
			start,stop,*pa = len(pa) == 2 and pa or [*pa,stop]
		pa_out = pa or [start,stop]
		pa_out = pa_out if not fabs else map(fabs,pa_out)
		self.incl = isnone(incl,put=self._incl)
		super().__init__(pa_out)
	# def has(self, num):			return project(*self, nums=factor)
	def project(self, factor):	return project(*self, nums=factor)
	def reflect(self, factor):	return reflect(*self, nums=factor)
	def factor(self, value):		return factor(self, nums=value)
#endregion

#region nums devcs
class _Nums:
	#region forms
	def __init_subclass__(cls, kk=None, attrs=None, to_num=None, **ka):
		cls._to_num = to_num or cls._to_num
		cls._kk = kk or attrs or cls._kk
		cls._n_at = dict(iidxr(ss(cls._kk)))
		init_cls(cls,**ka)
	#endregion
	...
class Nums(_Nums,MathIx):
	''' parameters defining a sequence of numbers or number range
		Note: distinct from Bound in that [*Nums(*pa)] == [*range(*pa]]; but otherwise similar
	'''
	#region defs
	_kk = _attrs				= tss('start  stop  step')
	a =start=low=lo				= property(lambda self: self.pa[0], lambda self, val: self.pa.__setitem__(0, val))
	b =stop=high=hi				= property(lambda self: self.pa[1], lambda self, val: self.pa.__setitem__(1, val))
	c =step						= property(lambda self: self.pa[2] if len(self.pa) >= 2 else None,
										   lambda self,v: self.pa.__setitem__(len(self.pa) >= 2 and 2 or slice(2,0), v))
	
	bound = lohi				= property(lambda self: self.pa[:2])
	delta = diff				= property(lambda self: self.pa[1] - self.pa[0])
	range						= property(lambda self: range(*self.pa))
	slice = idx					= property(lambda self: slice(*self.pa))
	len							= property(lambda self: len_of_inds(*self.pa))
	mid							= property(lambda self: mid(*self.pa))
	
	iter						= property(lambda self: self.iterate())
	tuple						= property(lambda self: tuple(self.iterate_safe()))
	set							= property(lambda self: set(self.iterate_safe()))
	dict						= property(lambda self: dict(self.iterate_safe()))
	list = seq					= property(lambda self: list(self.iterate_safe()))
	
	# args						= property(lambda self: self.pa)  # todo: remove
	# pa							= property(lambda self: self.args, lambda self,v: self.args.__setitem__(K.all,v))  # todo: remove
	args						= property(lambda self: self.pa, lambda self,v: self.pa.__setitem__(K.all,v))  # todo: remove
	_excl_dir					= hss('dict  list  seq  set  tuple')
	_miss = _to_num				= None
	#endregion
	#region forms
	def __i_nit__(self, *_pa, start=0, stop=None, step=miss, pa=None,args=miss):
		''' initialize a range of nums
			:param pa:    nums range inputs usually ordered as [start,stop,step]
			:param start: arg0 when missing pa
			:param stop:  arg1 when missing pa
			:param step:  arg2 when missing pa
		'''
		has_step = step is not miss
		pa = ismiss(args,put=pa or _pa)  # select appropriate input pos args
		pa = (
			pa.pa				if isnums(pa) else
			list(pa[0])			if len(pa) == 1 and iscoll(pa[0]) else
			pa )
		if len(pa) <= (1+has_step):
			start,stop,*pa = len(pa) == 2 and pa or [*pa,stop]
		self.pa = (
			list(pa)			if pa else
			[start,stop]		if not has_step else
			[start,stop,step] )
		assert len(self.pa) <= 10
	def __init__(self, *_pa, start=miss, stop=miss, step=miss, pa=None, args=miss, **ka):
		''' initialize a range of nums
			:param pa:    nums range as pos args usually ordered as (start,stop,step)
			:param start: arg0 when missing pa
			:param stop:  arg1 when missing pa
			:param step:  arg2 when missing pa
		'''
		pa = ismiss(args,put=pa or _pa)  # select appropriate input pos args
		self.pa = list(ilen(self._kk, pa, fill=miss))
		self.update(start=start, stop=stop, step=step, **ka)
		self.update(start=ismiss(start,put=0), stop=ismiss(stop,put=None), step=ismiss(step,put=None), over=no)
		assert len(self.pa) <= 10
	@classmethod
	def of(cls,obj=miss,*_pa,pa=(),args=miss,evs=None, **ka):
		if not ka and istype(obj,cls):	return obj	# noop case
		
		# resolve inputs:  => pa,ka
		pa = ismiss(args,put=pa or _pa)  # select appropriate input pos args
		pa_out,ka_out = (
			((),obj|ka)				if isdict(obj) else
			(obj.pa,ka)				if oftype(obj,Nums) else
			((obj,*pa),ka)			if obj is not miss else
			(pa,ka) )
		
		# resolve output
		out = cls(*pa_out,**ka_out)
		out = out if not evs else cxpas(evs,out)
		return out
	def new(self,**ka):				return self.of(self, pa=self.pa, **ka)
	def __repr__(self):				return str_range(*self.pa)
	def __int__(self):				return self.__class__(*(isnone(n,orfab=int) for n in self.pa[:3]),*self.pa[3:])
	def __float__(self):			return self.__class__(*(isnone(n,orfab=float) for n in self.pa[:3]),*self.pa[3:])
	def __index__(self):			return slice(*self.pa[:3])
	def __hash__(self):				return hash(tuple(self.pa))
	def __dir__(self):				return tdrop(super().__dir__(),self._excl_dir.__contains__)
	def __class_getitem__(cls, at):
		pa = oftype(at, slice) and (at.start, at.stop, at.step) or (None, at, None)
		nn = cls(*pa)
		return nn
	def __copy__(self):
		out = self.__class__(*self.pa)
		out.__dict__.update((k,copy(v)) for k,v in vars(self).items() if k != 'args')
		return out
	#endregion
	#region coll
	def __len__(self):				return len(self.pa)
	def __add__(self,val):			return self.__class__(*add_nums(self.pa,val))
	def __sub__(self,val):			return self.__class__(self.a-val, self.b-val, *self.pa[2:])
	def __mul__(self,val):			return self.__class__(self.a*val, self.b*val, (self.c or 1)*val, *self.pa[3:])
	def __eq__(self,val):
		vals = (
			val[:2]					if islist(val) else
			[val.start,val.stop]	if {'start','stop'} <= set(dir(val)) else
			list(val)[:2]			if isiter(val) else
			[val,val] )
		out = self.pa[:2] == vals
		return out
	def __lt__(self,val):
		''' compute self < val.  if None is present then return True only when self!=None and val==None '''
		vals = isiter(val) and val or [val,val]
		out = any(l<r if l!=self._miss!=r else r is self._miss for l,r in zip(self.pa[:2],vals))
		return out
	def __gt__(self,val):			return not self.__lt__(val)
	def has(self,val):
		# todo: when step is present, test contains as a set of values
		# resolve inputs
		to_n = self.__class__._to_num
		lohi = self.lohi[::self.lo<=self.hi and 1 or -1]
		v = val if isnttype(val,Nums) else val.lohi
		(lo,hi),v = [lohi,v] if not to_n else [_map(to_n,lohi),_map(to_n,collany(v))]
		
		# resolve output
		has = (
			all(lo<=v_n<=hi for v_n in v)		if isiter(v) else
			lo<=v<=hi )
		return has
	def __getitem__(self,idx):		return self.pa[idx]
	def __setitem__(self,idx,val):	return self.pa.__setitem__(idx, val)
	def __iter__(self):				return iter(self.args)
	def per(self):					return inum(start=self.start, stop=self.stop, step=self.step)
	def per_safe(self,limit=1000):	return isafe(self.per(),limit=limit)
	def update(self, over=yes, **ka):
		for k,v in ka.items():
			n = self._n_at[k]
			if v is not miss and (over or self.args[n] is miss):
				self.args[n] = v
		return self
	iterate			= per
	iterate_safe	= per_safe
	__contains__	= has
	#endregion
	#region utils
	def into(self, val):			return into(self.low, self.high, val)
	def project(self, factor):		return project(*self, factor)
	def reflect(self, factor):		return reflect(*self, factor)
	def convert(self, val):			return convert(*self, val)
	@classmethod
	def ramp(cls, start, stop, factor):
		items = (ramp(*item, factor) for item in zip(start, stop))
		items = cls(*items) if cls else items
		return items
	@classmethod
	def curb(cls, start, stop, factor):
		items = (curb(*item, factor) for item in zip(start, stop))
		items = cls(*items) if cls else items
		return items
	#endregion
	...

class INums(Nums):
	''' A mutable iterable which can also be used like an int (or float).
		Members are the same standard iteration args (floats inputs are acceptable alternative to int).
		Each call to next updates internal state of start incremented by step.
	'''
	#region forms
	def __init__(self, step=1, start=0, stop=None):
		super().__init__(start, stop, step)
	def __iter__(self):
		while True:  # todo: exit at stop
			yield next(self)
	def __next__(self):
		self.start += self.step if self.step is not None else 1
		return self
	def __eq__(self, other):
		return self.start == other
	def __index__(self):
		return int(self.start or 0)
	def __hash__(self):
		return hash(self.start)
	#endregion
	...

class NNI(Nums):
	''' number args that produces an iterable of a given sequence '''
	#region utils
	def __class_getitem__(cls, ind):
		if not isinstance(ind, slice): raise NotImplemented()
		pa = isinstance(ind, slice) and (ind.start, ind.stop, ind.step) or ('???', ind, '???')
		nni = cls(*pa)
		return nni
	def getitems(self, seq):
		assert self.step is None or (type(self.step) is int and self.step >= 1)
		
		iseq = enumerate(seq)
		for ind, value in iseq:
			if self.start is None or ind == self.start:
				if ind % self.step == 0:	yield value
				break
		for ind, value in iseq:
			if ind == self.stop: 			break
			if self.step is None: 			continue
			if ind % self.step == 0:		yield value
	__call__					= getitems
	#endregion
	...
	
class NumStep:
	''' Immutable int iteration args.
		Is iterable but cannot be iterator instance due to immutability.
		Each call to next creates a new NumIterArgs with start incremented by step.
	'''
	#region defs
	start						= property(lambda self: self)
	#endregion
	#region forms
	def __next__(self):
		return self.__class__(self+self.start, self.step)
	def __iter__(self):
		while True:
			yield next(self)
	#endregion
	...

NN,INN							= Nums, INums
isnums = isnn					= istypethen(Nums)
def nn_of_nums(*pa,**ka):		return bound_of_nums(*pa,**ka, fab=NN)		# todo: NN.of_nums
def nnz_of_inums(*pa,**ka):		return boundz_of_inums(*pa,**ka, fab=NN)	# todo: NN.of_inums
#endregion

#region other devcs
class Ints(NumStep, int):
	def __new__(cls, start=0, step=1, stop=None):
		obj = int.__new__(cls, start)
		obj.step, obj.stop = step, stop
		return obj
class Floats(NumStep, float):
	def __new__(cls, start=0, step=1, stop=None):
		obj = float.__new__(cls, start)
		obj.step, obj.stop = step, stop
		return obj

class Polynom(Nums):
	# todo: eliminate necessity for ord_of_ops
	# todo: consider more flexible means of creation
	# todo: support merging 2 or more Polynoms
	# todo: allow a duplicate math op when ord_of_ops requires a sandwich of other ops between for desired result
	# todo: alternatives:
	# 	1. use list of number functions as in 9.__mul__
	# 	2. use formatted str with re safety checks as in eval('9 * {}'.format(num))
	#region class mbrs
	add=offset=ofs		= property(lambda self: self.pa[0], lambda self, val: self.pa.__setitem__(0, val))
	mul=scale=scl		= property(lambda self: self.pa[1], lambda self, val: self.pa.__setitem__(1, val))
	pow=order=ord=exp	= property(lambda self: self.pa[2], lambda self, val: self.pa.__setitem__(2, val))
	_ord_of_ops			= '^*+'
	#endregion
	def __init__(self, add=None, mul=None, pow=None, ord_of_ops=None, *, sub=None, div=None, root=None):
		add = isnone(sub, put=add or 0., orput=sub and -sub)
		mul = isnone(div, put=mul or 1., orput=div and 1./div)
		pow = isnone(root, put=pow or 1., orput=root and 1./root)
		super().__init__(add, mul, pow)
		self.ord_of_ops = ord_of_ops
	def eval(self, num:{int,float}):
		''' this eval implementation will suffice for now '''
		add, mul, pow, ord_of_ops, *_ = *self, 1., 1., self._ord_of_ops
		result = (
			((num ** pow)  * mul)  + add	if ord_of_ops is None or ord_of_ops == '^*+' else
			((num  + add)  * mul) ** pow	if ord_of_ops == '+*^' else
			
			((num ** pow)  + add)  * mul	if ord_of_ops == '^+*' else
			((num  * mul) ** pow)  + add	if ord_of_ops == '*^+' else
			((num  * mul)  + add) ** pow	if ord_of_ops == '*+^' else
			((num  + add) ** pow)  * mul)	#if ord_of_ops == '+^*' else
		return result
	__call__ = eval

PNN					= Polynom
#endregion

#region scratch
#endregion
