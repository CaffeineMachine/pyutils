from dataclasses import dataclass
from contextlib import contextmanager

from pyutils.defs.itemdefs import Ndx
from pyutils.patterns.props import exact
from pyutils.utilities.callutils import cxpas
from pyutils.utilities.maputils import mapeach
from pyutils.utilities.pathutils import Path
from pyutils.utilities.iterutils import *
from pyutils.structures.strs import Fmt

#region fab defs


class DefsFabs:
	i						= tuple
	iclsfabs_at_clsstep		= dict()
	reentry_at				= dict()
	strfabs_known			= []
	strfabs_lrus_limit		= 1024
	
DDFabs = _D					= DefsFabs
_i							= DDFabs.i
#endregion

#region fab forms
def items_of_fabs(fabs,zitems=None,items=None,**ka):
	# todo: should be more generalized to multiple vectorized fabs instead of single fabs on only vals
	if None is zitems is items: return items_of_fabs.__get__(fabs)

	keys,vals = (
		items								if items else
		[zitems.keys(),zitems.values()]		if isdict(zitems) else
		[(),()]								if not zitems else
		zip(*zitems) )
	vals_out = _i(fabs(v,**ka) for v in vals)
	items = zip(keys,vals_out)
	return items

def ifabs_of_icls(icls_fabs=None,cls_step=None,ifabs=None):
	''' get *ifabs* conversions by selecting appropriate fabs_cls for given *icls_fabs* '''
	# resolve inputs
	icls_fabs = icls_fabs or _D.iclsfabs_at_clsstep[cls_step or FabStep]
	
	# resolve output
	def ifabs_of(ifabs):
		''' convert *ifabs* for given *icls_fabs* '''
		ifabs_out = []
		for fabs in ifabs:
			fabs_out = keepfirst((cls.of_suited(fabs,suit_other=True) for cls in icls_fabs), isntnone)
			ifabs_out.append(fabs_out)
		return ifabs_out
	result = ifabs is None and ifabs_of or ifabs_of(ifabs)
	return result
	
def fab_any_of(ifabs,obj=miss,deepest=10,icls_fabs=None,cls_step=None):
	# todo: this must extend walk utils and devcs
	# resolve inputs
	ifabs = ifabs_of_icls(icls_fabs=icls_fabs,cls_step=cls_step,ifabs=ifabs)
	
	# resolve output
	def fab_any(obj,deepest=deepest,deep=0,**ka):
		# resolve inputs
		assert deep <= deepest
		ka = dict(ka,deepest=deepest,deep=deep+1)
		
		# resolve output
		for fabs in ifabs:
			obj_out,did_qlfy = fabs(obj,fab_any,**ka)
			if did_qlfy: break
		else:
			raise ValueError(obj)
		return obj_out
	result = obj == miss and fab_any or fab_any(obj)
	return result

@contextmanager
def reentrys(obj=miss,key=miss, to_nth=None):
	'''
		:param obj: 
		:param key: 
		:param to_nth: 
	'''
	# todo: handle pushing args
	key = key is miss and id(obj) or key
	try:
		n = _D.reentry_at[key] = _D.reentry_at.get(key,0)+1
		nth = n if not to_nth else to_nth(n)
		yield nth
	finally:
		_D.reentry_at[key] -= 1
		...
#endregion

#region fab devcs
@dataclass
class FabStep:
	fab_i: iscall		= None
	fab: iscall			= None
	fabs: iscall		= None
	type: type			= None
	qlfy: iscall		= always
	as_kvs: bool		= None
	def __post_init__(self):
		self.fab_i = self.fab_i and exact(self.fab_i)
		self.fab = self.fab and exact(self.fab)
		self.fabs = self.fabs and exact(self.fabs)
		self.qlfy = exact(self.qlfy or always)
		self.as_kvs = isnone(self.as_kvs,put=self.type==dict)  # fixme: issubclass
		...
	@classmethod
	def of(cls,*pa,**ka):
		if pa and istype(pa[0],cls): return pa[0]
		
		pa = (
			pa[0]	if pa and isiter(pa) else
			pa )
		result = cls(*pa,**ka)
		return result
	
	def fab_any(self,obj,fabs=None,which_fabs=1,**ka):
		# resolve inputs
		did_qlfy = self.qlfy(obj)
		if not did_qlfy: return obj,did_qlfy
		
		# resolve output
		obj_out = obj
		if self.fab_i:  # fixme
			i_obj = obj if not self.fab_i else _i(self.fab_i(obj))
			i_obj = (
				i_obj									if not self.fabs else
				_i(self.fabs(v,**ka) for v in i_obj) )
			obj_out = (
				i_obj									if not fabs else
				_i(items_of_fabs(fabs,i_obj,**ka))		if self.as_kvs else
				_i(fabs(v,**ka) for v in i_obj) )
		obj_out = obj_out if not self.fab else self.fab(obj_out,**ka)
		return obj_out,did_qlfy
	__call__			= fab_any

class FabsBase:
	''' ... '''
	def __init_subclass__(cls, step_cls=FabStep,is_suited=None):
		cls._step_cls = step_cls
		cls._is_suited = is_suited or getattr(super(cls,cls),'_is_suited',None)
		assert not issubclass(cls,dict) or cls._is_suited
		
		icls_fabs = _D.iclsfabs_at_clsstep.get(step_cls) or _D.iclsfabs_at_clsstep.setdefault(step_cls,[])
		icls_fabs[cls._is_suited and Ndx.pfx or Ndx.sfx] = cls,
		cls.__call__ = cls.fab_any
		
	@classmethod
	def of_suited(cls,fabs,suit_other=False):
		fabs_out = (
			cls(fabs)	if cls._is_suited and cls._is_suited(fabs) or not cls._is_suited and suit_other else
			None )
		return fabs_out
	@classmethod
	def of(self,obj):		raise NotImplementedError()
	def fab_any(self,obj):	raise NotImplementedError()
	
class FabSteps(FabsBase, list):
	''' fab nested collection according to depth ordered fabs '''
	def __init__(self,fabs,**ka):
		fab_vals = list(fabs)
		fab_vals = [self._step_cls.of(fab,qlfy=cond,**ka) for cond,fab in fab_vals]
		super().__init__(fab_vals)
		...
	def fab_any(self,obj,fabs=None,err=False,**ka):
		obj_out,did_qlfy = None,False
		for fab_n in self:
			obj_out,did_qlfy = fab_n(obj,fabs,**ka)
			if did_qlfy: break
		else:
			err and throw(ValueError(obj))
		return obj_out,did_qlfy

class FabWalk(FabsBase, dict, is_suited=isdict):
	''' fab nested collection according to first qualified FabStep '''
	def __init__(self,fabs,fab_other=None,**ka):
		fab_items = dict(fabs,**ka)
		self.fab_other = fab_items.pop('fab_other',fab_other)
		fab_items = {cond:self._step_cls.of(fab,type=cond) for cond,fab in fab_items.items()}
		super().__init__(fab_items)
		...
	def fab_any(self,obj,fabs=None,**ka):
		# resolve fab_step|fab_n:  => fab_n
		fab_n = keepfirst(cxpas(self.get, afx(obj.__class__,obj.__class__.__bases__)),fill=self.fab_other)
		obj_out,did_qlfy = fab_n and fab_n(obj,fabs,**ka) or (None,False)
		return obj_out,did_qlfy
#endregion

#region helpers
# draft: in naming & placement
class Fabs:
	chs					= tss('*        /            ')
	ops					= tss('__mul__  __truediv__  ')
	op_at_ch			= mapeach(chs+ops, 2*ops)
	ch_at_op			= mapeach(chs+ops, 2*chs)
	_ch_of_kop			= lambda kop,pfx: (pfx,Fabs.ch_at_op.get(kop[0],kop[0]))
	# fixme: clsutils: stage_cls(fab,prefab=None) = stage(prefab,*,**)(fab,*,**)
	@property
	def fab_cache(self):
		if self._fab_cache is None:
			from pyutils.structures.maps import LRUMap
			self._fab_cache = LRUMap
		return self._fab_cache
	def __init__(self,fab_at,fab_cache=None,limit=_D.strfabs_lrus_limit,add_known=True,pfx='S'):
		self.fab_at = fab_at
		self.fab_at.update((self.op_at_ch[k],self.fab_at[k]) for k in [*self.fab_at] if k != self.op_at_ch[k])
		self.limit = limit
		self._fab_cache = fab_cache
		self.caches = {}
		self.pfx = pfx
		add_known and _D.strfabs_known.append(self)
	# def icached(self,to_kop=_ch_of_kop):
	# 	ka = dict(pfx=self.pfx)
	# 	for k_op,cache in zof(self.caches):
	# 		k_op = k_op if not to_kop else to_kop(k_op,**ka)
	# 		for k_arg,obj in zof(cache):
	# 			yield k_op,k_arg,obj
	def repr(self):
		ss_out = ['  {}{}{!r}'.format(*k_op,k_arg,obj) for k_op,k_arg,obj in self.icached()]
		s_out = '\n'.join(ss_out)
		return s_out
	@staticmethod
	def repr_known():				return ss(keep(_D.strfabs_known),fab='\n...\n'.join)
	
	def fab(self,op,arg,*pa,**ka):
		k_op = op,fab = op,self.fab_at[op]
		obj = fab(arg,*pa,**ka)
		return obj
	
	# fixme: populate in class init
	# fixme: populate in class init from fab ops
	def __mul__(self,arg):			return self.fab('__mul__',arg)
	def __truediv__(self,arg):		return self.fab('__truediv__',arg)
	def __xor__(self,arg):			return self.fab('__xor__',arg)
	def __pow__(self,arg):			return self.fab('__pow__',arg)
	def __mod__(self,arg):			return self.fab('__mod__',arg)
	def __bool__(self):				return bool(self.caches)
	def __getitem__(self,arg):		return arg  # todo: support assignment
	__repr__ 			= repr

# draft: in naming & placement
IFab = I				= Fabs({'*':iter})
HFab = HC = H			= Fabs({'*':set})
LFab = LC = L			= Fabs({'*':list})
TFab = TC = C			= Fabs({'*':tuple})
MFab = DC = M			= Fabs({'*':dict})

# # draft: in naming & placement
# class StrFabs:
# 	chs					= tss('*        /            ')
# 	ops					= tss('__mul__  __truediv__  ')
# 	op_at_ch			= mapeach(chs+ops, 2*ops)
# 	ch_at_op			= mapeach(chs+ops, 2*chs)
# 	_ch_of_kop			= lambda kop,pfx: (pfx,StrFabs.ch_at_op.get(kop[0],kop[0]))
# 	# fixme: clsutils: stage_cls(fab,prefab=None) = stage(prefab,*,**)(fab,*,**)
# 	@property
# 	def fab_cache(self):
# 		if self._fab_cache is None:
# 			from pyutils.structures.maps import LRUMap
# 			self._fab_cache = LRUMap
# 		return self._fab_cache
# 	def __init__(self,fab_at,fab_cache=None,limit=_D.strfabs_lrus_limit,add_known=True,pfx='S'):
# 		self.fab_at = fab_at
# 		self.fab_at.update((self.op_at_ch[k],self.fab_at[k]) for k in [*self.fab_at] if k != self.op_at_ch[k])
# 		self.limit = limit
# 		self._fab_cache = fab_cache
# 		self.caches = {}
# 		self.pfx = pfx
# 		add_known and _D.strfabs_known.append(self)
# 	def icached(self,to_kop=_ch_of_kop):
# 		ka = dict(pfx=self.pfx)
# 		for k_op,cache in zof(self.caches):
# 			k_op = k_op if not to_kop else to_kop(k_op,**ka)
# 			for k_arg,obj in zof(cache):
# 				yield k_op,k_arg,obj
# 	def repr(self):
# 		ss_out = ['  {}{}{!r}'.format(*k_op,k_arg,obj) for k_op,k_arg,obj in self.icached()]
# 		s_out = '\n'.join(ss_out)
# 		return s_out
# 	@staticmethod
# 	def repr_known():				return ss(keep(_D.strfabs_known),fab='\n...\n'.join)
# 	
# 	def fab(self,op,arg,*pa,**ka):
# 		# resolve inputs
# 		k_op = op,fab = op,self.fab_at[op]
# 		k_arg = isstr(arg,orfab=id)
# 		
# 		# resolve output
# 		cache = self.caches.get(k_op) or self.caches.setdefault(k_op, self.fab_cache(limit=self.limit))
# 		obj = cache.get(k_arg) or cache.setdefault(k_arg, fab(arg,*pa,**ka))
# 		return obj
# 	
# 	# fixme: populate in class init
# 	# fixme: populate in class init from fab ops
# 	def __mul__(self,arg):			return self.fab('__mul__',arg)
# 	def __truediv__(self,arg):		return self.fab('__truediv__',arg)
# 	def __xor__(self,arg):			return self.fab('__xor__',arg)
# 	def __pow__(self,arg):			return self.fab('__pow__',arg)
# 	def __mod__(self,arg):			return self.fab('__mod__',arg)
# 	def __bool__(self):				return bool(self.caches)
# 	__repr__ 			= repr
# 
# _prefabs				= {
# 	hss					: lambda arg,*pa: (tss(arg), pa),
# }
# # draft: in naming & placement
# _  = ISS				= StrFabs({'*':ss})
# HS = HSS				= StrFabs({'*':hss})
# LS = LSS				= StrFabs({'*':lss})
# TS = TSS				= StrFabs({'*':tss})
# S						= StrFabs({'*':tss, '/':Fmt})
# PS						= StrFabs({'/':Path})
#endregion
