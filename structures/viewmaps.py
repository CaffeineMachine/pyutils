from pyutils.structures.maps import *
from pyutils.output.forms import *
from pyutils.patterns.poly import *
from pyutils.utilities.iterutils import lseq
# from pymatics.store.trackrecords import time_run


__all__ = ['ViewMap', 'View', 'ViewMap', 'Views',  ]

index_getter = lambda index: lambda item: item[index]

class ViewMap(dict):
	def __init__(self, to_key, items=None, multi=False, **kws):
		'''	ctor
			:param to_key: func used to derive key for each item
			:param items: items to be indexed
			:param multi: indicates that to_key can produce multiple keys under which to index an item
		'''
		# todo: there is significant functional overlap between this and SetMap
		if isinstance(to_key, ViewMap):
			to_key, multi = to_key.to_key, to_key.multi
		if isinstance(to_key, int):
			index = to_key
			to_key = index_getter(index)
			
		self.to_key = to_key
		self.multi = multi
		super().__init__(**kws)
		items and lseq(items, self.add)
	def add(self, item):
		index = key = self.to_key(item)
		if not self.multi:
			entries = self.setdefault(key, [])
			entries.append(item)
		else:
			index = set(index)
			for key in index:
				entries = self.setdefault(key, [])
				entries.append(item)
		return index
	def getitems(self, keys):
		keys = not isinstance(keys, (list, set)) and [keys] or keys  # coerce keys to a list
		results = []
		for key in keys:
			result = self.get(key)
			if result:
				results.extend(result)
		# print(results)
		return set(results)
View = ViewMap


_list_factory = lambda *args, **kws: []


class ViewMaps(StrCache, FullMap):
	def __init__(self, to_keys, items=None, missing=_list_factory, **kws):
		if isinstance(to_keys, int):
			to_keys = range(to_keys)
		self.views = lseq(to_keys, ViewMap)
		self.data = set()
		StrCache.__init__(self)
		FullMap.__init__(self, missing=missing, **kws)
		self.extend(set(items))
	# def format(self): return time_run(self._format)
	# def _format(self):	# uncomment to pass through time_run; a poormans decorator
	def format(self):		# uncomment to skip time_run
		result = list(self.kk_v())
		result = ColumnsForm(result)
		result = str(result)
		return result
	__repr__ = format
	# def __len__(self):
	# 	return len(self.data)
	def values(self): return self.data
	def kk_v(self):
		''' get the derived [key0, key1, ...] to item pairs for each item in the given data '''
		for value in self.data:
			item = [view.to_key(value) for view in self.views]
			item.append(value)
			yield item
	def append(self, item):
		if item in self.data: return
		
		self.data.add(item)
		for view in self.views:
			view.add(item)
	def extend(self, items):
		for item in items:
			self.append(item)
	def __iter__(self):
		return iter(self.data)
	def __getitem__(self, key):
		return self.get(key)
	@classmethod
	def coerce_keys(cls, keys):
		invert = False
		if isinstance(keys, slice):
			# slices are interpreted as 'slice.start or not slice.stop' so start & stop must be a set or None
			if keys.step is not None:
				raise Exception(f'Found non-None slice.step value {keys} which is not interpretted by {cls.__name__}')
			if keys.start is not None and keys.stop is not None:
				raise Exception('slice.start and slice.stop cannot both be non-None')
			
			invert = keys.start is None
			keys = keys.stop if invert else keys.start
			keys = {} if keys is None else keys
			
			if not isinstance(keys, set):
				try:	keys = {keys}
				except:	keys = set(keys)
		
		return keys, invert
	def get(self, keys):
		keys = not isinstance(keys, tuple) and (keys,) or keys  # coerce given key to tuple
		
		ranking, results = self.data, set(self.data)
		for ind, (key, view) in enumerate(zip(keys, self.views)):
			if key is ...: break
			if key == slice(None, None, None): continue
			
			key_set, invert = self.__class__.coerce_keys(key)
			view_result = view.getitems(key_set)
			set_op = invert and Poly.__sub__ or Poly.__and__
			results = set_op(results, view_result)
			
			if not results:
				results = self.__missing__(key)
				break

		# when keys has more inds than available views apply the remaining inds as a slice of the result
		ranking = list(results)  # todo: rank
		result_inds_ofs = ... in keys and keys.index(...)+1 or len(self.views)
		result_inds = keys[result_inds_ofs:]
		for ind in result_inds:
			ranking = ranking[ind]
		
		return ranking
Views = ViewMaps
