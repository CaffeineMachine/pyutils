from collections import OrderedDict
from pyutils.structures.maps import *


class AttrODict(AttrEnum, OrderedDict):
	''' Extend OrderedDict to allow access to items as members. As in d['attr'] == d.attr '''
	_init_sort = True
class FullODict(FullMapper, OrderedDict):
	''' Extend OrderedDict to allow getitem key access misses fall back to __missing__ rather than throwing exception '''
	_init_sort = True
class FillODict(FillMapper, OrderedDict):
	''' Extend OrderedDict to allow getitem key access misses fall back to __missing__ rather than throwing exception '''
	_init_sort = True
class FullAttrODict(FullMapper, AttrEnum, OrderedDict):
	''' Extend OrderedDict to allow:
		1. access to items as members. As in d['attr'] == d.attr
		2. getitem key access misses fall back to __missing__ rather than throwing exception
	'''
	_init_sort = True
class FillAttrODict(FillMapper, AttrEnum, OrderedDict):
	''' Extend OrderedDict to allow:
		1. access to items as members. As in d['attr'] == d.attr
		2. getitem key access misses fall back to __missing__ rather than throwing exception
	'''
	_init_sort = True
	
class RemapODict(ValRemapper, OrderedDict): pass

# todo: obsolete; replace all instances of *ClassName*Dict with *ClassName*ODict as the former is a misnomer
AttrDict = AttrOMap = AttrODict
FillDict = FillOMap = FillODict
FullDict = FullOMap = FullODict
FullAttrDict = FullAttrOMap = FullAttrODict
FillAttrDict = FillAttrOMap = FillAttrODict
RemapDict = RemapOMap = RemapODict

