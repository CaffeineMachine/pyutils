''' obsolete: See
- pyutils.patterns.accessor
- pyutils.structures.maps
'''
import types
import itertools as it
from collections import deque, defaultdict as ddict

from pyutils.defs.itemdefs import Ndx
from pyutils.defs.typedefs import isstr, miss
from pyutils.structures.maps import Vars
from pyutils.utilities.callbasics import ss, tss, redef
from pyutils.utilities.iterutils import keep
from pyutils.utilities.maputils import mapeach,zof
from pyutils.time.thread import atomic

#region seq defs
class DefsSeq:
	pass

DDSeq = _D				= DefsSeq
#endregion

#region iter devcs
class Iterates:
	''' Has iterate as default __call__ behavior. References an iterable. '''
	__slots__ = ['_itr']
	
	def __init__(self, itr):
		self._itr = itr
	def __iter__(self):
		yield from self._itr
	
	# method aliases
	__call__ = __iter__

Iter = Iterates
#endregion

class Deque(deque):
	''' a double-ended queue with thread-safe access '''
	#region defs
	atomic			= property(atomic)
	queue			= deque.appendleft
	_sides			= Vars(
		fifo		= Vars(i=Ndx.first,o=Ndx.first),
		filo		= Vars(i=Ndx.first,o=Ndx.last ),
		lifo		= Vars(i=Ndx.last, o=Ndx.first),
		lilo		= Vars(i=Ndx.last, o=Ndx.last ), )
	#endregion
	#region utils
	def __init__(self, vals=None, maxlen=None, sides=_sides.lilo, **ka):
		super().__init__(maxlen=maxlen, **ka)
		self.sides = sides
		vals and self.extend(vals)
	def first(self, fill=None, right=None):
		# todo: allow specifying default in/out side for insert,pop,first,last
		with self.atomic:
			out = self[0] if len(self) else fill
		return out
	def last(self, fill=None, right=None):
		# todo: allow specifying default in/out side for insert,pop,first,last
		with self.atomic:
			out = self[-1] if len(self) else fill
		return out
	
	def pop(self, fill=None, right=None):
		# todo: allow specifying default side for pop & insert
		with self.atomic:
			out = super().pop() if len(self) else fill
		return out
	
	# custom sided
	def push(self,val=miss, vals=miss, right=None):
		''' insert into left side '''
		# todo: push and pull will refer to left side single value adjustmen
		#       perhaps this should migrate to insert
		assert (val is miss) != (vals is miss)
		if val is not miss:
			self.append(val)
		else:
			self.extend(vals)
		return self
	def pull(self, fill=None, right=None):
		''' pop from left side '''
		with self.atomic:
			out = self.popleft() if len(self) else fill
		return out
	
	# def extend(self, vals=()):
	# 	deque.extend
	# 	with self.atomic:
	# 		for val in vals:
	# 			self.append(val)
	# 	return self
	def take(self,n=None,right=True):
		with self.atomic:
			if n is None:
				out = list(self)
				self.clear()
			else:
				out = self[:n]
				self[:n] = []
		return out
	
	# static sided
	def left(self, fill=None):
		with self.atomic:
			out = self[0] if len(self) else fill
		return out
	def right(self, fill=None):
		with self.atomic:
			out = self[-1] if len(self) else fill
		return out
	pushl				= redef(pull, right=False)
	pushr				= redef(pull, right=True)
	pulll				= redef(pull, right=False)
	pullr				= redef(pull, right=True)
	#endregion
	...
#endregion
