from pyutils.utilities.strutils import *
from pyutils.utilities.iterutils import *
from pyutils.utilities.sequtils import *
from pyutils.utilities.maputils import *
from pyutils.utilities.clsutils import *
from pyutils.utilities.oputils import *
from pyutils.patterns.props import exact
from pyutils.patterns.accessors import *
from pyutils.structures.inds import *

#region args defs
class DefsArgs(DefsArgsUtils):
	#region colls
	#endregion
	...

DDAA = DD				= DefsArgs
AAAA,SSAA				= DDAA.AA, DDAA.SS
#endregion

#region args utils
def faa_of(*_pa, call=None, fab=None, pa=None, ka=None, pal=None, par=None, faas=(), fill_ops=SSAA.fab, **_ka):
	'''
		:param _pa: CAN fill in for ops {*call*|*fab*};
		:param call:
		:param fab:
		:param pa: when given explicitly is never altered; therefore input CANNOT fill in for ops {*call*|*fab*}
		:param pal:
		:param par:
		:param ka:
		:param faas:
	'''
	# resolve inputs:  => iparg,faas
	iparg = iter(_pa)
	faas = *faas,FAA(call=call,fab=fab,pa=pa,ka=ka or _ka,pal=pal,par=par)  # prioritize faa_of kargs; make last faa
	
	# resolve inputs:  => faas,faa_out.pa
	faa_out = FAA()
	for arg in iparg:
		if not is_pka(arg):
			faa_out.pa += arg,
		else:
			faas = arg,*iparg,*faas  # consume rest of iparg into fpkas after 1st qualified pka; exits for loop
	
	# resolve input ops; sourced only from fpkaof(*_pa) and never from pa:  fpka_out.pa => fpka_out:{call|fab}
	if SSAA.call in fill_ops and faa_out.pa and iscall(faa_out.pa[0]):
		faa_out.call, *faa_out.pa = faa_out.pa
	if SSAA.fab in fill_ops and faa_out.pa and iscall(faa_out.pa[0]):
		faa_out.fab, *faa_out.pa = faa_out.pa
	
	# resolve output:  faas => faa_out:call,fab,pa,ka,pal,par
	for aa in faas:
		assert is_aa(aa), 'all arg in pa (pos args) after 1st qualified PKA arg must also qualify as a PKA. Got:\n%s' % arg
		SSAA.call	in aa and faa_out.set(SSAA.call, aa.get(SSAA.call))
		SSAA.fab	in aa and faa_out.set(SSAA.fab, aa.get(SSAA.fab))
		SSAA.pa	in aa and faa_out.set(SSAA.pa, aa[SSAA.pa])
		SSAA.pal	in aa and faa_out.set(SSAA.pal, aa[SSAA.par] + faa_out.pal)
		SSAA.par	in aa and faa_out.set(SSAA.par, faa_out.par + aa[SSAA.par])
		SSAA.ka	in aa and faa_out.ka.update(aa[SSAA.ka])
	return faa_out

def do_faa(*pa_faa, **ka_faa):
	# resolve inputs
	faa = faa_of(*pa_faa, **ka_faa)
	pa_join_multi = (bool(faa.pal) + bool(faa.pa) + bool(faa.par)) >= 2
	pa_all = (
		faa.pal+faa.pa+faa.par				if pa_join_multi else
		faa.pal or faa.pa or faa.par or ())	# otherwise just take first
	
	# resolve output; call precedes fab:  => fab(call(pka))
	out = (
		faa									if not faa.call else
		faa.call(*pa_all, **faa.ka) )
	out = (
		out										if not faa.fab else
		faa.fab(*out.pa_all,**out.ka)			if getattr(out, 'is_pka') else
		faa.fab(out) )
	return out

do							= do_faa
fpka_of,do_fpka,is_pka		= faa_of,do_faa,is_aa
#endregion

#region args devcs
class Args:
	''' data passed to a function(*,**) stored as pos & key args '''
	pa: tuple				= ()
	ka: dict				= ()
	#region defs
	unzip = uz				= property(NotImplementedError)
	#endregion
	#region forms
	def __init_subclass__(cls, **ka):
		for k,v in ka.items():
			setattr(cls,k,v)
	def __init__(self, *_pa, pa=(), keep=None, keep_kv=None, ka=miss, **_ka):
		''' ctor
			:param pa:      pos args to store
			:param ka:      key args to store
			:param keep:    filter args given qual:int|str of arg;  also takes set of vals  
			:param keep_kv: filter args given qual & val as tuple of (qual:int|str,val:any) of arg
		'''
		# todo: include args customizations here as well, namely kk_put & kk_pop 
		self.pa = tuple(pa or _pa)
		self.ka = dict(ka or _ka)
		if keep or keep_kv:
			keep = keep if iscx(keep) else set(isstr(keep,fab=ss)).__contains__
			self.pa = tuple(v   for n,v in iidx(self.pa)   if not keep or keep(n) or not keep_kv or keep_kv(n,v))
			self.ka =      {k:v for k,v in self.ka.items() if not keep or keep(k) or not keep_kv or keep_kv(k,v)}
	def __repr__(self):								return rep_aa(self.pa,self.ka,_fmt_aa='AA(*{} **{})')
	def __eq__(self, obj):
		sigs,sigs_obj = self.sigs(tuple), obj.sigs(tuple)
		return sigs == sigs_obj
	def __hash__(self):
		out = self.sigs(xors,hash)
		return out
	def sigs(self,fab=tuple,fabs=None):
		sigs = ij(self.pa, sorted(dropmap(self.ka, rep_aa.is_local, at=0).items()))
		sigs = sigs if not fabs else map(fabs,sigs)
		sigs = sigs if not fab  else fab(sigs)
		return sigs
	@classmethod
	def of_argpair(cls, a2):
		aa = cls(*a2)
		return aa
	def add(self,other):
		az = self.__class__()
		return az
	
	def shift(self,kk_put=(),kk_pop=(),**ka):		return shift_aa(self.pa, self.ka, kk_put, kk_pop, **ka)
	def pop(self,kk_pop=(),kk_put=(),**ka):			return self.shift(kk_put, kk_pop, **ka)
	def call_of(self, call):						return CallArgs(call,pa=self.pa,ka=self.ka,after=self.after)
	def call(self,call=None,*pal,**ka):
		call,pal = (call,pal) if call else (pal[0],pal[1:])
		out = call(*pal,*self.pa,**self.ka|ka)
		return out
	def __getitem__(self, key):						return self.ka[key] if isstr(key) else (self.pa,self.ka)[key]
	
	__lshift__ = put		= shift
	__rshift__				= pop
	__add__					= add
	__call__				= call
	# __iadd__				= merge
	of_a2					= of_argpair
	a2_of					= redef(shift, at=None)
	ka_of					= redef(shift, at=ka_of_a2)
	ka2_of					= redef(shift, at=ka2star)
	az_of					= NotImplemented
	#endregion
	#region utils
	def call(self,call=None,*pa,after=None, **ka):
		call,after = call or self.cx, isnone(after,put=self.after)  # todo: 'after' in self.__dict__
		_pal,_par = after and ((),self.pa) or (self.pa,())
		
		result = call(*_pal,*pa,*_par,**self.ka,**ka)
		return result
	#endregion
	...

class _ArgsVK:
	vv: tuple				= ()
	kk: tuple				= ()
class ArgZip:
	''' data passed to a function(*,**) stored as as vals with right adjusted keys.  ie: ArgZip(kk='abc',vv=[0,1,2])(dict) == dict(a=0,b=1,c=2) '''
	kk: tuple				= ()
	vv: tuple				= ()
	fillsr: tuple			= ()
	n_miss: int				= 0
	#region defs
	pa 						= property(lambda self: self.vv[:-len(self.kk)])
	ka 						= property(lambda self: dict(zip(irev(self.kk),irev(self.vv))))
	a2 						= property(lambda self: (self.pa,self.ka))						#  a2 IS aa; aa ISNT  a2
	ka2						= property(lambda self: dict(self.ka, pa=self.pa))				# ka2 IS ka; ka ISNT ka2
	sless					= property(lambda self: '${}  ={}'.format(sj('/',self.kk), sj('/',self.vv)))
	zip						= property(NotImplementedError)
	#endregion
	#region forms
	def __init__(self,kk=(),vv=(),fillsr=(),n_miss=0,**ka):
		self.kk = tss(kk)
		self.vv = tuple(vv)
		self.fillsr = list(fillsr)
		self.n_miss = int(n_miss)
		not all(map(str.isidentifier, self.kk)) and throw(ValueError(f'{self.kk=}'))
		
		# integrate ka into members:  => kk,fillsr
		for k in ka:
			if k not in self.kk:
				self.kk.append(k)
		
		for nth,k in iidx(self.kk[::-1],0,-1):
			if k in ka:
				nth = self.kk.index(k)
				idx = slice(nth or n_max,nth or n_max)
				assert -nth<=len(self.fillsr), f'given non-contiguous fillsr. {k!r}=kk[{nth}] in ka.  {self.kk} {self.fillsr}'
				if k not in self.kk:
					self.fillsr[idx] = ka[k]
		...
	def __repr__(self):							return self.sless
	def __eq__(self, obj):
		out = all(getattr(self,k) == getattr(obj,k,...) for k in ss('kk  vv'))
		return out
	def __hash__(self):
		out = xors(map(hash,ij(self.kk,self.vv)))
		return out
	@classmethod
	def of(cls,obj=miss,*pa,**ka):
		''' coerce inputs into ->ArgZip from its precursors '''
		if not ka and istype(obj,cls):		return obj
			
		# resolve inputs
		obj,ka = (
			(miss,obj|ka)		if isdict(obj) else
			(obj,ka) )
		pa = (
			pa					if obj is miss else
			(obj,)+pa			if isstr(obj) else
			(*obj,)+pa			if istype(obj,list|tuple) and len(obj) == 2 and iscoll(obj[0]) else
			throw(ValueError(obj)) )
		
		# resolve output
		az = cls(*pa,**ka)
		return az
	@classmethod
	def of_argstar(cls,*pa,**ka):
		raise NotImplementedError()
		kk,vv = ...,...
		# unzip_star
		az = ArgZip(kk,vv,**ka)
		return az
	def remake(self,vv_pfx='todo',**ka):
		az = ArgZip(**self.__dict__|ka)
		return az
	def zip(self,vv_pfx='todo',fab=Args.of_a2,**ka):
		aa = zip_argz(**self.__dict__|ka, fab=fab)
		return aa
	def call_of(self, call):						return CallArgZip(call,vv=self.vv,kk=self.kk,after=self.after)
	
	aa_of = __call__		= zip
	a2_of					= redef(zip, at=None)
	ka_of					= redef(zip, at=ka_of_a2)	# todo
	ka2_of					= redef(zip, at=ka2star)	# todo
	#endregion
	#region utils
	def call(self,call=None,*pa,after=True, **ka):
		#resolve inputs
		call,after = call or self.cx, isnone(after,put=self.after)
		_pa,_ka = zip_az(self.kk, self.vv, **ka)
		_pal,_par = after and ((),_pa) or (_pa,())
		#resolve output
		out = call(*_pal,*pa,*_par,**_ka)
		return out
	def keys(self):				return iter(self.kk)
	def values(self):			return iter(self.vv[-len(self.kk):])
	def items(self):			return zip(self.kk, self.vv[-len(self.kk):])
	def __getitem__(self,key):	raise NotImplementedError()  # todo: get value at key offset;  O(N);  NOT O(1)
	def __iter__(self):			return iter(self.vv)
	#endregion
	...

class CallObj:
	cx						= None
	def call(self,*_pa,**_ka):
		pa,ka = (_pa or _ka) and add_args((self.pa,self.ka),(_pa,_ka)) or self
		out = self.cx(*pa,**ka)
		return out
	__call__				= call
class CallArgs(CallObj,Args):
	def __init__(self, call, *pa, after=yes, **ka):
		self.cx = exact(call)
		self.after = after
		super().__init__(*pa, **ka)
class CallArgZip(CallObj,ArgZip):
	def __init__(self, call, *pa, after=yes, **ka):
		self.cx = call
		self.after = after
		super().__init__(*pa, **ka)

AA,AZ,CAA,CAZ				= Args, ArgZip, CallArgs, CallArgZip
#endregion

#region args devcs old
class ArgMap(dict):
	@classmethod
	def fromkeys(cls, *pa, **ka):
		return cls(super().fromkeys(*pa, **ka))
	__getattr__			= dict.__getitem__

class ArgsLR:  # todo: remove obsolete
	def __init_subclass__(cls, **ka):
		cls.__iter__ = cls.keys
		cls._h_cls_attrs = set(cls.__slots__)
	def __init__(self, *pa, pal=(), par=(), ka=miss, **_ka):
		''' a value of None indicates that the arg is not present '''
		ka = ka if ka is not miss else _ka
		self.pa,self.pal,self.par,self.ka,self.is_pka = pa,pal,par,ka,is_pka
		if isiter(pa) and ... in pa:
			self.pa, self.par = splitargs(pa)
	def __contains__(self, key):		return key in self._h_cls_attrs and getattr(self, key) is not None
	def __repr__(self):					return '<*{}, **{}>'.format(self.pa_all, self.ka)  # todo: simplify output
	def __getitem__(self, key):			return getattr(self, key)
	def get(self, key, fill=None):		return getattr(self, key, fill)
	def set(self, key, value):			return setattr(self, key, value) if key in self._h_cls_attrs else throw(key)
	def keys(self):						return iter(self.__slots__)
	def values(self):					return (getattr(self,key) for key in self.__slots__)
	def items(self):					return ((key,getattr(self,key)) for key in self.__slots__)
	def __getstate__(self):
		print('PKA.__getstate__()')
		return dict(self.items())
	#region class members
	__slots__			= SSAA.kk_aa
	__dict__			= property(__getstate__)
	pa_all				= property(lambda self: self.pal+self.pa+self.par)
	#endregion
	...

class FabArgs(ArgsLR):  # todo: remove obsolete
	def __init__(self, fab=None, *_pa, call=None, **_ka):
		''' a value of None indicates that the arg is not present '''
		self.call,self.fab = call,fab
		super().__init__(*_pa,**_ka)
	def __repr__(self):					return '<{}(*{}, **{})>'.format(self.fab.__qualname__, self.pa_all, self.ka)
	
	def do(self):
		result = self.call(*self.pa, *self.par, **self.ka)
		return result
	#region class members
	__slots__			= SSAA.kk_faa
	#endregion
	...



class ArgKey:
	def __init__(self, key, default=miss):
		self.key = key
		self.default = default

class ArgKeys(list):
	''' simple list of ArgKey structs '''
	# properties
	keys			= property(lambda self: tuple(map(KK.key, self)))
	defaults		= property(lambda self: tuple(map(KK.default, self)))
	
	def __init__(self, args):
		args = list(map(lambda arg: ArgKey(*collany(arg)), args))
		super().__init__(args)

class Fabricator:
	''' Defines and facilitates a custom process of assignment during and after instantiation '''
	# todo: change rdefaults to defaults
	# todo: split defaults into largs, rargs
	# todo: support fields with leading pargs defines like: [None, None, 'third', 'fourth']
	# todo: support fields with leading pargs defines like: [2, 'third', 'fourth']
	# todo: consolidate overlapping functionality with Args or ArgKeys
	
	def __init__(self, ctor, fields, rdefaults=()):
		''' :param ctor: constructor to be called
			:param fields: sequence of arg names; also accepts for a space delimited str
			:param rdefaults: defaults for the right-most args
		'''
		if isinstance(fields, str):
			fields = fields.split()
		self.ctor = ctor
		self.fields = list(fields)
		self.rdefaults = rdefaults
	def __len__(self): return len(self.fields)
	def to_kargs(self, *args, **kargs):
		if len(args) > len(self):
			raise ValueError()
		
		# define arg values
		values = [None] * len(self.fields)				# init values as list of Nones of expected length
		values[-len(self.rdefaults):] = self.rdefaults	# overwrite right side with rdefaults
		values[:len(args)] = args						# overwrite left side with args
		kargs.update(zip(self.fields, values))			# define kargs as map of field keys to values
		return kargs
	def auto_kargs(self, locals, *args, **kargs):
		''''''
	def construct(self, *args, **kargs):
		if 'True; todo':
			args, kargs = (), self.to_kargs(*args, **kargs)
		obj = self.ctor(*args, **kargs)
		return obj
	def auto_construct(self, locals, *args, **kargs):
		''' construct with args extracted from locals'''
		args, kargs = (), self.auto_kargs(*args, **kargs)
		obj = self.ctor(*args, **kargs)
		return obj
	def set(self, obj, *args, **kargs):
		if 'True; todo':
			args, kargs = (), self.to_kargs(*args, **kargs)
		for field, value in kargs.items():
			setattr(obj, field, value)
	# method aliases
	__call__ = construct

class _WIP_Fabricator:
	'''
		Usage:
		Fab(Ctor, '-2 -1* ** * _')
		Fab(Ctor, '-2 -1* ** 0* 1', '* **')
		Fab(Ctor, '_ a _ b', )
	'''
	def __init__(self, ctor, pa_ctor, pa_set=None, setter=None, pa_ldef=None, pa_rdef=None, ka_def=None, **_ka_def):
		self.ctor = ctor
		self.setter = setter
		self.pa_ctor = pa_ctor
		self.pa_set = pa_set or pa_ctor
		self.pa_ldef = pa_ldef
		self.pa_rdef = pa_rdef
		self.ka_def = (ka_def or {}).update(_ka_def)
	def get_args(self, *pa, args_fmt='ctor', **ka):
		pa_fmt = args_fmt == 'ctor' and self.pa_ctor or self.pa_set
		return None, None  # todo
	def construct(self, *pa, **ka):
		pa, ka = self.get_args(*pa, args_fmt='ctor', **ka)
		obj = self.ctor(*pa, **ka)
		return obj
	def set(self, obj, *pa, **ka):
		pa, ka = self.get_args(*pa, args_fmt='setter', **ka)

		if self.setter:
			result = self.setter(obj, *pa, **ka)
		else:
			assert not pa, 'Cannot apply positional args without a Fabricator.setter function.'
			result = None
			for attr, value in ka.items():
				setattr(obj, attr, value)
		return result
	
	# method aliases
	__call__ = construct
	
class FabPK(_WIP_Fabricator):
	''' Same as Fab(ctor, pa_ctor='* **').	Will be removed when Fabricator can support this notation. '''
	def __init__(self, ctor, pa_ctor='* **', **_ka_def):
		super().__init__(ctor, '* **', **_ka_def)
	def get_args(self, pa, ka=None, **_):
		return pa, (ka or {})
	
class FabK(_WIP_Fabricator):
	''' Same as Fab(ctor, pa_ctor='**'). Will be removed when Fabricator can support this notation.	'''
	def __init__(self, ctor, pa_ctor='**', **_ka_def):
		super().__init__(ctor, '**', **_ka_def)
	def get_args(self, ka=None, fmt_args='ctor', **_):
		return (), (ka or {})

FAA						= FabArgs
FPKA,PKA				= FAA,AA
Fab						= Fabricator
#endregion

#region scratch area
'''
Usage:
print('hello\nworld!')
callfabargs(print, 'hello', 'world', sep='\n', end='!\n')
callfabargs('hello', 'world', sep='\n', end='!\n', fab=print)
callfabargs('world', fab=print, pal=['hello'])
callfabargs('world', fab=print, pal=['hello'])
callfabargs(dict(fab='USE NEXT fab', pa='USE NEXT pa'), dict(fab=print, par=['world!'], sep='\n'))
callfabargs(dict(fab='USE NEXT fab', par=['hello'], sep='\n'), dict(fab=print, par=['world!']))
callfabargs(dict(fab=..., pal=['world!'], sep='USE NEXT sep'), dict(fab=print, pal=['hello'], sep='\n'))


callfabargs(dict(fab=...,fab_par=...), dict(fab=..., step=...))
callfabargs(dict(fab=range,fab_par=(0,5)), dict(fab=slice, step=5))
Note: Signature of print:
print(*objects, sep=' ', end='\n', file=sys.stdout, flush=False)
'''
#endregion