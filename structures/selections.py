from dataclasses import dataclass, field
from pyutils.output.joins import *

from collections import OrderedDict as odict

from pyutils.utilities.iterutils import *
import itertools as it
import random
from _collections_abc import Sequence


def random_order(seq, rand=None):
	rand = rand or random
	result = rand.sample(seq, k=len(seq))
	return result
def random_range(*range_args, rand=None):
	rand = rand or random
	seq = range(*range_args)
	result = rand.sample(seq, k=len(seq))
	return result

class RandomSeq:
	def __init__(self, *seed):
		self.random = random.Random(*seed)
	def random_order(self, seed=None):
		if seed is not None:
			self.random.seed(seed)
		return random_order(self, self.random)
	@staticmethod
	def random_range(*range_args, seed=None):
		return random_range(*range_args, seed)
	

class IEnumeration:
	def __init__(self, generative=False):
		self.generative = generative
	def get_enumeration(self): raise NotImplemented()

	
class Enumerations(RandomSeq, IEnumeration, Sequence):
	''''''
	def __init__(self, data, randomize=None):
		if isinstance(data, Options):
			randomize = data.randomize if randomize is None else randomize
			data = [[option] for option in data]
		elif not isinstance(data, list):
			data = list(data)
		self.randomize = randomize
		self.data = data
		RandomSeq.__init__(self)
		IEnumeration.__init__(self)
	def __len__(self): return len(self.data)
	def __getitem__(self, ind): return self.data[ind]
	def __iter__(self):
		if self.randomize:
			result = iter(random_order(self.data))
			# result = iter(Reiterate(random_order, [self.data]))
		else:
			result = iter(self.data)
		return result
	def get_enumeration(self):	return self
	@staticmethod
	def coerce(items):
		if isinstance(items, IEnumeration):
			items = items.get_enumeration()
		elif not isinstance(items, Enumeration):
			items = Enumeration([[enumeration] for enumeration in items])
		return items
	def product(self, *enumerations_args):
		result = Enumerations(iperm((self,) + enumerations_args))
		return result
	__hash__ = object.__hash__
Enumerations = Enumerations

		

class Enumeration(list, RandomSeq, IEnumeration):
	''' A set of selectable items.
		example:
		{a,b,c,d,e,f}
	'''
	def __init__(self, *args, randomize=False, generative=False, **kargs):
		self.randomize = randomize
		list.__init__(self, *args)
		RandomSeq.__init__(self, **kargs)
		IEnumeration.__init__(self, generative)
	@staticmethod
	def coerce_items(items):
		if isinstance(items, Options):
			items = [[option] for option in items]
		return items
	def get_enumeration(self, gen=None):
		if self.generative:
			result = Reiterate(zip, [self])
		else:
			result = Enumerations(self)
		return result
	def __getitem__(self, inds):
		result = super().__getitem__(inds)
		if isinstance(inds, slice):
			result = Options(result)
		return result
	def __iter__(self, randomize=None):
		randomize = self.randomize if randomize is None else randomize
		if self.randomize:
			result = iter(random_order(list(super().__iter__())))
		else:
			result = super().__iter__()
		return result
	@classmethod
	def product_unpack(cls, option_args, randomize=None):
		# option_args = lseq(option_args, Enumerations)
		option_args = lseq(option_args, Enumerations.coerce)
		# if not randomize:
		if bool(True):
			# result = Reiterate(Enumerations.product, [option_args])
			result = Enumerations.product(*option_args)
		else:
			ordered_enumerations = tuple(Enumerations.product(option_args))
			result = Reiterate(random_order, [ordered_enumerations])
		return result
	def product(self, *option_args, **kargs):
		kargs.setdefault('randomize', self.randomize)
		return self.__class__.product_unpack((self,) + option_args, **kargs)
	def rproduct(self, *option_args, **kargs):
		kargs.setdefault('randomize', self.randomize)
		return self.__class__.product_unpack(reversed((self,) + option_args), **kargs)
	def repeat(self, count, **kargs):
		kargs.setdefault('randomize', self.randomize)
		return self.__class__.product_unpack([self] * count, **kargs)
	__mul__, __rmul__, __pow__ = product, rproduct, repeat
	__hash__ = object.__hash__

class Presets(Enumeration):
	def __init__(self, enumerations, shuffle=True, **kargs):
		enumerations = lseq(enumerations, Enumeration)
		super().__init__(enumerations, generative=shuffle, **kargs)
		self.shuffle = shuffle
		self.preset_ind = -1
		self.incr()
		
	presets = property(lambda self: super())
	presets_len = property(lambda self: super().__len__())
	def incr(self):
		if self.shuffle:
			self.preset_ind = int(random.random() * self.presets_len)
		else:
			self.preset_ind = (self.preset_ind + 1) % self.presets_len
	# def __len__(self): return len(self.presets[self.preset_ind])
	# def __getitem__(self, ind): return self.presets[self.preset_ind][ind]
	def __len__(self): return len(self.presets.__getitem__(self.preset_ind))
	def __getitem__(self, ind): return self.presets.__getitem__(self.preset_ind)[ind]
	def __iter__(self):
		# result = iter(self.presets[self.preset_ind])
		result = iter(self.presets.__getitem__(self.preset_ind))
		self.incr()
		return result

@dataclass
class Reiterate:
	''' Alternative implementation allowing for multiple '''
	iterator: 'Callable'
	args: tuple = None
	kargs: dict = None
	count: int = 0
	
	def __iter__(self):
		# self.count += 1
		# print(self.count)
		return iter(self.iterator(*self.args or (), **self.kargs or {}))


def subset_bits(superset, subset):
	result = [item in subset for item in superset]
	result = list(map(subset.__contains__, superset))
	return result


class EnumSelection:
	''' Selected subset from Enumeration
		example:
		subset {a,b,c} from the set {a,b,c,d,e,f}
	'''
	# properties
	selected = property(lambda self: (option for option, enable in self.select_map.items() if enable))
	deselected = property(lambda self: (option for option, enable in self.select_map.items() if not enable))

	def __init__(self, enum, selected=None, mask=None):
		self.enum = enum
		mask = mask and lseq(mask, bool)
		mask = (mask or []) if selected is None else lseq(enum, selected.__contains__)
		self.select_map = odict(izip(enum, mask, fill=False))  # todo: store selection as set: more efficient and no duplicated data
		super().__init__()
	def __repr__(self):
		selected, deselected = list(self.selected), (self.deselected)
		parts = ['{', '}']
		parts[1:1] = (selected and [f'[{" ".join(selected)}]'] or []) + (deselected and [' '.join(deselected)] or [])
		result = ' '.join(parts)
		return result
	def __iter__(self):						return iter(self.select_map.items())
	def __getitem__(self, option):			return self.select_map[option]
	def __setitem__(self, option, enable):	self.select_map[option] = bool(enable)
	def reset(self, enabled=False):
		self.select_map.update(zip(self.enum, it.repeat(enabled)))
	def set(self, selected):
		mask = lseq(self.enum, selected.__contains__)
		self.select_map.update(izip(self.enum, mask, fill=False))
	def select(self, *select_args, enabled=True, unpack=None):
		select_args = select_args[0] if unpack is True else select_args
		for option in select_args:
			if option not in self.enum:
				raise KeyError(f'Key {option!r} missing from {self.enum}')
			self[option] = bool(enabled)
	def deselect(self, *select_args, **kargs):
		self.select(*select_args, enabled=False, **kargs)
	
class EnumSelectionsDict(odict):
	def reset(self, enabled=False):
		for selection in self.values():
			selection.reset(enabled)
	def set(self, selections):
		for key, selected in selections.items():
			self[key].set(selected)
	def select(self, items, add=True):
		for key, selected in items:
			self[key].select(selected, add)
	def deselect(self, items):
		return self.select(items, add=False)
	def selected_product(self):
		selected_options = lseq(self.values(), [list, lambda selection: selection.selected])
		result = all(selected_options) and list(product(selected_options)) or []  # product of any options=[] is also []
		return result
	# method aliases
	__hash__ = object.__hash__
	
class EnumsSelection(EnumSelection):
	''' Selected subset of Enumerations
		example:
		The subset of four pairs given by ( {a,b,c} * {0,1} ) [1:5]:
			a|b|b|c    a|a|b|b|c|c
			1|0|1|0 of 0|1|0|1|0|1
	'''
	
	
### class aliases
#	Providing the less correct but more intuitive + less verbose Selection & Selections
#	as aliases to EnumSelection & EnumsSelection
Selection = EnumSelection
Selections = EnumSelectionsDict
Options = Enumeration
