import itertools as it
from pyutils.output.joins import *
from pyutils.utilities.iterutils import *
from pyutils.structures.maps import *
from pyutils.patterns.accessors import KK, Attr

from dataclasses import dataclass, field
from sortedcontainers import SortedDict, SortedSet
from pprint import pprint as pp, pformat as pfmt
from sys import maxsize
from math import ceil

chain_iter = it.chain.from_iterable


__all__ = ['Node', 'MapNode', 'DeltaList', 'Repeats', 'CycleRepeats', 'BitRepeats', 'RepeatsBase']


class Node:
	''' Interface iterating nodes of a tree. Traversal depends only on the subs property. '''
	_subs = ()
	_has_subs = None
	_convert = None  # implements to_node & to_subsource on each iteration

	def __init__(self, subs=nothing, convert=nothing):
		''' ctor
			:param subs: an iterator of Nodes. ctor assignment not needed if subs is overridden.
			:param convert: implements to_node & to_subsource in the form: convert(node, source) -> node, subsource;
		'''
		if subs is not nothing:
			self._subs = subs
		if convert is not nothing:
			self._convert = convert
	def __len__(self):
		''' get total nodes in this scope as length; includes self node and node.subs '''
		return self.subslen + 1
	def get_nodes(self, first=True):
		''' iterate over nodes in this scope (nodes directly accessible to self) '''
		if first:
			yield self
		yield from self.subs
	def walk(self, first=True, cond=None, src=None, convert=None):
		''' traverse tree of nodes nested within self node '''
		convert = convert or self._convert  # select appropriate converter
		
		# yield the top of this scope (or self node) when called for
		if first:
			node = self
			if convert:
				node, src = convert(self, src)
			yield node
		
		# yield from subs of this scope (or the iterable values in self node) if any
		for sub in self.subs:
			if cond and not cond(sub, src):
				continue
			elif hasattr(sub, 'walk'):
				yield from sub.walk(cond=None, src=src, convert=convert)
			else:
				if convert:
					sub, _ = convert(sub, src)  # todo: should this come before other checks?
				yield sub
	
	# properties
	nodes		= property(get_nodes)
	subs		= property(KK._subs)
	tree		= property(walk)
	subtree		= property(lambda self: self.walk(first=False))
	is_branch	= property(lambda self: bool(self.subs))
	is_simple	= property(lambda self: not bool(self.subs))
	subslen		= property(lambda self: len(self.subs))

	# aliases
	__iter__ = get_nodes
	has_subs = is_branch


class MapNode(Node):
	''' Node interface adapted to walk a tree of dict-like objects with a values() function '''
	subs		= property(lambda self: self.values())


@dataclass
class DeltaList:
	added: list		= field(default_factory=list)
	removed: list	= field(default_factory=list)
	
	def __str__(self):
		return pfmt(self.__getstate__())
	__repr__ = __str__
	def expand(self, before):
		result = (before - self.removed) & self.added
		return result
	
	@staticmethod
	def reduce(before, after):
		before = isinstance(before, SortedSet) and before or SortedSet(map(tuple, before))
		after = after.expand(before) if isinstance(after, DeltaList) else after
		after = isinstance(after, SortedSet) and after or SortedSet(map(tuple, after))
		result = DeltaList(added=after - before, removed=before - after)
		return result
	
	def __getstate__(self):
		added = self.added if isinstance(self.added, list) else list(self.added)
		removed = self.removed if isinstance(self.added, list) else list(self.removed)
		result = dict(added=added, removed=removed)
		return result
		


class RepeatRanges(list):
	pass
class RepeatItems(list):
	pass

class RepeatsBase:

	def __init__(self, values, lengths, *cyclic_lengths, is_cyclic=True, total_cycles=None):
		self.is_cyclic = is_cyclic if is_cyclic is not None else bool(cyclic_lengths)
		self.values = values
		self.length_args = [(isinstance(l, int) and it.cycle([l]) or l)  for l in ((lengths,) + cyclic_lengths)]
		self.length_args_count = len(self.length_args)
		self.sum_lengths = 0
		self.total_cycles = total_cycles

		if self.total_cycles is None:
			totals = [len(lengths) for lengths in self.length_args if hasattr(lengths, '__len__')]
			self.total_cycles = totals and min(totals) or None
		if len(self.length_args) > 1:
			self.length_args = self.compactor(self.length_args)
		a = 0
	# properties
	lengths = property(lambda self: self.length_args_count != 1 and chain_iter(zip(*self.length_args)) or self.length_args[0])
	items = property(lambda self: zip(self.is_cyclic and it.cycle(self.values) or self.values, self.lengths))
	iters = property(lambda self: zip(it.cycle([[0,1]]), *self.length_args))
	
	def __iter__(self, to_value=None):
		''' generate the values with the given repeats '''
		for value, length in self.items:
			if to_value:
				value = to_value(value)
			yield from [value] * length
	def __getitem__(self, inds):
		if isinstance(inds, int):
			offset = 0
			for result, length in self.items:
				if offset <= 0: break
				offset += length
		elif isinstance(inds, slice):
			result = list(self.genitems(inds))
		else:
			raise KeyError(inds)
		return result
	def genitems(self, inds):
		# if isinstance(inds, int):
		start, stop, step = inds.start or 0, maxsize if inds.stop is None else inds.stop, inds.step or 1
		value_stop, value_start = 0, start
		remainder = 0
		for value, length in self.items:
			# count = max(min(stop, ind)-prev_ind, 0)
			value_start = value_stop
			value_stop += length
			# count = max(min(stop, value_stop)-value_start, 0)
			count = max(min(stop, value_stop) - max(start, value_start), 0)
			if count <= remainder:
				remainder -= count
			else:
				count -= remainder
				remainder = count % step
				count = ceil(count / step)
				yield from it.repeat(value, count)
			
			if value_stop >= stop: break
			
			# prep for next iteration
			# value_stop += length
		
	def coerce_lists(self, values=False, lengths=True):
		if values:
			if not isinstance(lengths, list):
				self.values = list(it.islice(self.values, self.total_cycles))
		if lengths:
			for ind, lengths in enumerate(self.length_args):
				if not isinstance(lengths, list):
					self.length_args[ind] = list(it.islice(lengths, self.total_cycles))
	
	@staticmethod
	def compactor(values_args):
		row_size = len(values_args)
		cols = [[] for ind in range(row_size)]
		# values = iter(zip(values_args))
		tally_summed = None
		empty = [0] * row_size
		iter_size = True
		
		# while iter_size:
			# row = list(it.islice(values, row_size))
			# iter_size = len(row)
			# row += empty[iter_size:]
		for row in zip(*values_args):
			tally = [bool(value) for value in row]
			tally_count = sum(tally)
			
			if tally_count == 1 and tally == tally_summed:
				col_ind = tally.index(True)
				cols[col_ind][-1] += row[col_ind]
			elif tally_count:
				for col, value in zip(cols, row):
					col.append(value)
				tally_summed = tally
			
		return cols
	
			
class Repeats(RepeatsBase):
	# values = property(lambda self: self.get_iterator(index=0))
	# lengths = property(lambda self: self.get_iterator(index=1))
	''
	
class CycleRepeats(RepeatsBase):
	# def __init__(self, values, *lengths):
	# 	# if len(lengths) and not isinstance(lengths[0], int):
	# 	# 	lengths = list(it.chain.from_iterable(lengths))  # flatten matrix of repeats
	# 	# self.sum_lengths = sum(self.lengths)
	# 	super().__init__(it.cycle(values), *lengths)
	@property
	def iterators(self):
		for value, repeat in zip(it.cycle(self.values), self.lengths):
			yield value, repeat
	def compress(self):
		''' omit 0 length entries where possible '''
		cycle_len = len(self.length_args)
		compressed_length_args = [[] for i in range(cycle_len)]
		apply_sum = lambda seq, length: seq.__setitem__(-1, seq[-1] + length)
		apply_append = list.append

		sum_len, sum_len_ind = 0, ...
		for lengths in zip(*self.length_args):
			lengths = list(lengths)
			len_inds = [len_ind for len_ind, length in enumerate(lengths) if length] or [0]
			
			# current and previous lengths have different len_inds and cannot be aggregated
			if (len(len_inds) != 1 or sum_len_ind != len_inds[0]) and sum_len_ind not in (None, ...):
				sum_before_append = sum(lengths[:sum_len_ind])
				if sum_len and sum_before_append:
					if len(compressed_length_args[0]) == 0:
						[compressed_lengths.append(0) for compressed_lengths in compressed_length_args]
					compressed_length_args[sum_len_ind][-1] += sum_len
					
				for length, compressed_lengths in zip(lengths, compressed_length_args):
					compressed_lengths.append(length)
					
				if sum_len and not sum_before_append:
					compressed_length_args[sum_len_ind][-1] += sum_len
				sum_len = 0
				
			if len(len_inds) == 1:
				# aggregate current and previous lengths of same len_ind when a single length is non-zero
				sum_len_ind = len_inds[0]
				sum_len += lengths[sum_len_ind]
			else:
				sum_len_ind = None
		
		if sum_len:
			if sum([ls[-1] for ls in compressed_length_args][sum_len_ind+1:]):
				[compressed_lengths.append(0) for compressed_lengths in compressed_length_args]
			compressed_length_args[sum_len_ind][-1] += sum_len
			sum_len = 0
		# assign compressed result as needed when changes have been made
		if sum_len_ind is not ...:
			from pprint import pprint as pp
			# pp([list(zip(*self.length_args)), list(zip(*compressed_length_args))], width=20)
			self.length_args = compressed_length_args
	
class BitRepeats(CycleRepeats):
	# static members
	repr_obj = SJ.jj_obj_fmt
	_default_first_bit = 0
	
	# properties
	bit_str	= property(lambda self: ''.join(self.__iter__('01'.__getitem__)))
	ibits	= property(lambda self: self.__iter__([0,1].__getitem__))
	lbits	= property(lambda self: list(self.__iter__([0,1].__getitem__)))
	tbits	= property(lambda self: tuple(self.__iter__([0,1].__getitem__)))
	ibools	= property(lambda self: iter(self))
	lbools	= property(lambda self: list(self))
	tbools	= property(lambda self: tuple(self))
	ienum	= property(lambda self: (ind for ind, bit in enumerate(self) if bit))
	lenum	= property(lambda self: list(self.ienum))
	tenum	= property(lambda self: tuple(self.ienum))
	
	
	def __init__(self, lengths, *cyclic_lengths, first_bit=_default_first_bit, **kargs):
		self.first_bit = bool(first_bit)
		# super().__init__(it.cycle([self.first_bit, not self.first_bit]), lengths, *cyclic_lengths, **kargs)
		super().__init__([self.first_bit, not self.first_bit], lengths, *cyclic_lengths, **kargs)
	def __str__(self):
		return self.bit_str
	def __repr__(self):
		result = self.repr_obj(imapvars('first_bit lengths', self))
		result = result.format(self.__class__.__name__)
		return result
	def select(self, seq, overflow=False):
		''' apply this bit sequence as a mask to the sequence '''
		mask = self
		if overflow in (True, 1):	mask = it.chain(self, it.cycle([1]))
		elif overflow == 'repeat':	mask = it.cycle(self)
		elif overflow:				mask = it.chain(self, overflow)
		
		yield from (item for item, bit in zip(seq, mask) if bit)
	@classmethod
	def from_bits(cls, bits, compare=None, on_match=False, cast=miss):
		compare = compare or ['0', 'False', 'false']
		repeats = [0]
		prev = cls._default_first_bit
		for bit in bits:
			bit = bool(on_match if bit in compare else bit)
			if prev is bit:
				repeats[-1] += 1
			else:
				repeats.append(1)
			prev = bit

		cast = cls if cast is miss else cast
		repeats = cast(repeats) if cast else repeats
		return repeats
	@classmethod
	def from_inds(cls, inds, total=None, first_bit=1, cast=miss):
		# total = total or maxsize
		prev = -1
		repeats = [0] * (first_bit and 2 or 1)
		for ind in it.chain(inds, [total]):
			if ind is None: break
			
			incr = ind-prev
			if total and ind >= total:
				repeats.extend([total-prev-1])	# when ind reaches total append last incr and break
				break
			elif incr == 1:
				repeats[-1] += 1				# incr + 1 when ind and prev are contiguous
			else:
				repeats.extend([incr-1,1])		# append length of gap and start of next contiguous inds
			prev = ind
			
		cast = cls if cast is miss else cast
		repeats = cast(repeats) if cast else repeats
		return repeats

	
	# method aliases
	__rand__ = __and__ = select
	bits, bools, enum = ibits, ibools, ienum



# todo: move code below to generators
class Buffer:
	''' Iterable sequence buffer allowing arbitrary traversal or peeking at current value '''
	def __init__(self, value, start=0, end=None):
		self.value = value
		self.start = self.pos = start
		self.end = end
		self.given = None
		self.itr = None
	def __len__(self):
		result = self.end - self.pos
		return max(result, 0)
	def next(self):
		result = self.itr.send(None)
		return result
	def peek(self, offset=0):
		result = self.value[self.pos+offset]
		return result
	def prev(self):
		result = self.itr.send(-2)
		return result
	def __iter__(self):
		''' iterable generator method for buffer

			usage:
			#              01234567890123456789012345
			sbuf = Buffer('abcdefghijklmnopqrstuwyxyz')
			si = iter(sbuf)
			print(next(si)+next(si)+next(si))			# => 'abc'
			print(si.send(0)+si.send(0)+si.send(0))		# => 'def'
			print(si.send(1)+si.send(1)+si.send(1))		# => 'hjl'
			print(si.send(-1)+si.send(-1)+si.send(-1))	# => 'lll'
			print(si.send(-2)+si.send(-2)+si.send(-2))	# => 'kji'
		'''
		# print('\n__next__()')
		ofs, end = 0, len(self.value) if self.end is None else self.end
		while 0 <= self.pos < end:
			# print(f'__next__.pos:{self.pos}')
			self.pos += 1 + (ofs or 0)
			ofs = yield self.value[self.pos-1]

	def __getitem__(self, item):
		result = self.value[self.pos:self.end]
		result = result[item]
		return result


class IterProperty:	''' properties for augmenting iterators '''
class Accumulator(IterProperty): pass
class TrailTuple(IterProperty): pass
class Until(IterProperty): pass
class RaiseOn(IterProperty): pass
class Depth(IterProperty): pass

Converter = IterProperty  # will now refer to mutation iterators as converters


from dataclasses import dataclass, field

@dataclass
class StagedSlices:
	''' Class defines how a slice changes for each frame of iteration '''
	starts: object = None
	stops: object = None
	steps: object = None
	factory: 'typing.Any'	= field(init=False, default=slice)
	ind_types: tuple		= field(init=False, default=(type(None), int))
	index: slice			= field(init=False, default=None)
	has_next: bool			= field(init=False, default=False)
	def __post_init__(self):
		for ind_name in ['starts', 'stops', 'steps']:
			if not isinstance(self.__dict__[ind_name], self.ind_types):
				if ind_name == 'stops':
					self.__dict__[ind_name] = iter(map(lambda i: i+1,  self.__dict__[ind_name]))
				else:
					self.__dict__[ind_name] = iter(self.__dict__[ind_name])
		if not any((not isinstance(ind, self.ind_types) for ind in (self.starts, self.stops, self.steps))):
			raise UnboundLocalError('Infinite iteration detected.  One of start, stop or step must not be a index.')
	def __iter__(self):
		return self
	def __next__(self):
		try:
			args = tuple(ind if isinstance(ind, self.ind_types) else next(ind) for ind in (self.starts, self.stops, self.steps))
			self.index = self.factory(*args)
			return self.index
		except Exception as e:
			self.has_next = False
			raise StopIteration
	@staticmethod
	def grow_front(*starts_args):
		starts_args = range(*starts_args)
		return StagedSlices(reversed(starts_args), starts_args.stop)
	@staticmethod
	def grow_back(*stops_args):
		return StagedSlices(0, range(*stops_args))
	@staticmethod
	def shrink_front(*starts_args):
		starts_args = range(*starts_args)
		return StagedSlices(starts_args, starts_args.stop)
	@staticmethod
	def shrink_back(*stops_args):
		return StagedSlices(0, reversed(range(*stops_args)))
	
@dataclass
class StagedSeqences:
	''' Class defines how a sequence changes for each frame of iteration '''
	seq: list
	slices: StagedSlices
	prev: list = field(default_factory=list, init=False)
	def __post_init__(self):
		self.slices = isinstance(self.slices, StagedSlices) and self.slices or StagedSlices(*self.slices)
		self.itr = Iterable(self)
	def __str__(self):
		result = f'Seqs({self.prev})'
		return result
	__repr__ = __str__
	@property
	def has_next(self):
		return self.slices.has_next
	@property
	def __len__(self):
		return len(self.prev)
	def __iter__(self):
		return self.prev
	def __next__(self):
		slic = next(self.slices)
		self.prev = self.seq[slic]
		# self.prev[:] = self.seq[slic]
		return self
	def __getitem__(self, index):
		# result = self.seq[self.slices.index]
		result = self.prev[index]
		return result
	@staticmethod
	def grow_front(seq):
		length = len(seq)
		return StagedSeqences(seq, StagedSlices.grow_front(length))
	@staticmethod
	def grow_back(seq):
		length = len(seq)
		return StagedSeqences(seq, StagedSlices.grow_back(length))
	@staticmethod
	def shrink_front(seq):
		length = len(seq)
		return StagedSeqences(seq, StagedSlices.shrink_front(length))
	@staticmethod
	def shrink_back(seq):
		length = len(seq)
		return StagedSeqences(seq, StagedSlices.shrink_back(length))
		
	
@dataclass
class Iterable:
	''' handle to an iterable instance object '''
	state: object
	def __iter__(self):
		return self
	def __next__(self):
		result = next(self.state)
		return result

