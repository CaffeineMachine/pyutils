# todo: RELOACATE into pyutils.structures.members

import re
from pyutils.utilities.iterutils import *
from pyutils.defs.typedefs import *
from pyutils.output.forms import *

#region defs
__all__ = ['walk_items', 'AttrTree']

log = None
# log = print

def walk_items(tree_dict, parents=None):
	parents = parents or []
	
	for key, value in tree_dict.items():
		if value and isinstance(value, dict):
			yield from walk_items(value, parents+[key])
		else:
			yield [parents+[key], value]
#endregion

#region devcs
class AttrTree(dict):
	#region defs
	_is_attr = '_[^a-zA-Z0-9_]+'  # always apply as an attribute when matching this pattern
	_add_attr = False
	_join_items_s = SJoinStrs(',\n  ', '{ ', ',  }')
	_join_items_r = SJoinStrs(',\n  ', '__class__(\n  ', ',  )')
	#enregion
	#region coll
	def __init__(self, *args, parent=None, join_items=None, **kws):
		self.parent = parent
		self.root = (parent and parent.root) or parent
		self.join_items = join_items # and JoinStrs(join_items) or self._join_items
		super().__init__(*args, **kws)
		self._add_attr = True  # after this point use __setitem__ instead of __setattr__
	def __str__(self, join_items=None, delim='  : '):
		# todo: extract unpack from this function
		result = lseq(walk_items(self), unpack(lambda k, v: [f'{".".join(k)}', v]))
		widths = lseq(result, unpack(lambda k, v: len(k)))
		col_width = max(*widths, 0, 0)
		join_items = join_items or self.join_items or self._join_items_s
		# result = lseq(result, unpack(lambda k, v: f'{k:<{col_width}s}{delim}{v!r}', join_items))
		result = xseq(result, join_items, unpack(lambda k, v: f'{k:<{col_width}s}{delim}{v!r}'))
		result = re.sub(r'__class__', self.__class__.__name__, result)
		return result
	def __repr__(self, join_items=None):
		join_items = join_items or self.join_items or self._join_items_r
		result = self.__str__(join_items, '  = ')
		return result
	#enregion
	#region coll
	def __getattr__(self, attr):
		if attr in ['parent', 'root']:
			raise AttributeError(f'AttrTree.__getattr__({attr}) not expected. Likely due to failure to call __init__()')
		log and log(f'getattr({attr})')
		if attr in self.__class__.__dict__:
			print(attr,'attr in self.__class__.__dict__')
			result = self.__class__.__dict__[attr]
		elif attr in self:
			print(attr,'attr in self')
			result = self[attr]
		else:
			raise AttributeError(attr)
		# else:
		# 	print(attr,'else')
		# 	result = AttrTree(parent=self)
		# 	self[attr] = result
		return result
	def __setattr__(self, attr, value):
		prefer_setitem = self._add_attr and attr not in self.__dict__
		log and log(f'setting {prefer_setitem and "item" or "attr"} given key {attr!r}')
		target_dict = self if prefer_setitem else self.__dict__
		target_dict[attr] = value
	def __getitem__(self, key):
		key, *sub_keys = isinstance(key, str) and key.split('.', 1) or [key, None]
		item = super().__getitem__(key)
		if sub_keys:
			item = item[sub_keys[0]]
		return item
	def __setitem__(self, key, value):
		key, *sub_keys = isinstance(key, str) and key.split('.', 1) or [key, None]
		if not sub_keys:
			super().__setitem__(key, value)
		else:
			if super().__contains__(key):
				child = self[key]
			else:
				child = AttrTree(parent=self)
				super().__setitem__(key, child)
			child[sub_keys[0]] = value
	def __contains__(self, key):
		if super().__contains__(key):
			return True
		
		key, *sub_keys = isinstance(key, str) and key.split('.', 1) or [key, None]
		result = sub_keys and super(self.__class__, self).__contains__(key) and sub_keys[0] in self[key]
		return result
	def items_tree(self):
		yield from walk_items(self)
	def update_tree(self, data):
		for key, value in data.items():
			if value and isdict(value) and not typeof(value, AttrTree):
				value = AttrTree(parent=self).update_tree(value)
			if key in self and typeof(self[key], AttrTree) and typeof(value, AttrTree):
				self[key].update_tree(value)
			else:
				self[key] = value
		return self
	#endregion
	...
#endregion

#region tests
if __name__ =='__main__':
	tree = AttrTree()
	tree.a.b.c = 10
	tree.a.b.d = 'foo'
	tree.e.f = 'bar'
	
	print(f'{tree!r}\n\n{tree!s}')
#endregion
