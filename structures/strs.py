import re

from copy import copy
from dataclasses import dataclass
from string import Formatter

from pyutils.utilities.callutils import *
from pyutils.utilities.maputils import *
from pyutils.utilities.strutils import *
from pyutils.patterns.mapper import *
from pyutils.utilities.pathutils import Path

#region str defs
_str,_repr					= str,repr
_mdl_name					= __name__
dataclassrepr				= dataclass(repr=True)
class DefsStrs:
	delim_priority			= ' ,_-;:/=&%#@`~'  # todo: ensure allowed chars are re compatible
	fmt_py_group_named		= r'\{(\w+)(:[^\}]+)?\}'
	fmt_rx_group_named		= '(?P<{name}>[^{delim}]*)'
	fmt_rx_group			= '([^{delim}]*)'
	the_full_fmt_map		= None
	ka_out					= dict(call=print)
	on_miss_fmt				= lambda mapd, key, *pa,**ka: mapd._fmt(key)
	strfabs_lrus_limit		= 1024
	strfabs_known			= []

DDSS = _D					= DefsStrs
#endregion

#region fmt utils
# todo: are functions below essential/unreplaceable?
def strs_from(delim_strings:str,fab=None):
	''' Given *delim_strings* detect the custom delimiter by which to split it into Strs.
		The delim will be delim_strings[0] but only if it is found 2 or more times in *delim_string*
		When there is no qualifying delim (delim_strings[0] occurs only once) return Strs containing
		only 1 *delim_strings*. Emulates the unix sed application pattern replace syntax/behavior.
		
		Usage:
		StrsFrom(' abc def ')	# => Strs(['abc', 'def'])
		StrsFrom(':abc:def:')	# => Strs(['abc', 'def'])
		StrsFrom('::a:bc:def')	# => Strs(['', 'a', 'bc', 'def'])
		todo Usage:
		StrsFrom('abc;')		# => Strs(['abc;;'])
		StrsFrom(';abc;')		# => Strs(['abc'])
		StrsFrom('abc;def;')	# => Strs(['abc', 'def'])
		StrsFrom(';abc;def;')	# => Strs(['abc', 'def'])
		StrsFrom(';;abc;def;')	# => Strs(['', 'abc', 'def'])
		StrsFrom('  abc def  ')	# => Strs(['', 'abc', 'def'])
		StrsFrom('a,b,c,;d,e,f,;')		# => Strs(['', 'a,b,c,', 'd,e,f,'])
		
		StrsFrom2('a,b,c,,;d,e,f,,;;')	# => Strs(['a,b,c,,', 'd,e,f,,'])
	'''
	if not len(delim_strings):
		return Strs()
	
	delim, strings = delim_strings[0], delim_strings[1:]
	if not delim in strings:
		strings = delim_strings
		delim = list(set(_D.delim_priority) - set(delim_strings))[0]
	else:
		strings = strings[:-1] if strings[-1] == delim else strings
	result = (fab or Strs)(strings, delim)
	return result

def reformat(s, reformatter):
	''' produce args and kws from a string '''
	replacer = reformatter
	if not isinstance(reformatter, str):
		replacer = MatchReplacer(reformatter)
	s_out = re.sub(_D.fmt_py_group_named, replacer, s)
	return s_out
def unformat(s, formatted):
	''' produce args and kws from a string '''
	fmt = _D.fmt_rx_group_named.format(name=r'\1', delim='-')
	s_out = re.search(r''+s.reformat(fmt), formatted)
	if s_out:
		s_out = s_out.groupdict()
	return s_out

StrsFrom					= strs_from
#endregion

#region fmt devcs
class FullFmtVars(KeyMapper, AttrEnum, dict, on_miss=_D.on_miss_fmt):
	''' Dynamic map containing key format for any given key '''
	def __init__(self,*pa,_fmt='{{{}}}',**ka):
		self._fmt = isstr(_fmt,var='format')
		super().__init__(miss,*pa,**ka)
	#endregion
	...

class FullFmtr(Formatter):
	''' dynamic formatter which fills in any missing arg values '''
	def __init__(self,*pa,of_key=miss,fill=miss,**ka):
		self._of_key = of_key if not (of_key is fill is miss) else DDSS.the_full_fmt_map.__getitem__
		self._fill = fill
		super().__init__()
	@classmethod
	def of(cls, *pa, **ka): raise NotImplementedError()
	def format(self, *pa, **ka):
		s_out = super().format(self,*pa,**ka)  # overriding strange convention of Formatter.format
		return s_out
	def get_value(self, key, pa, ka):
		n_pa = len(pa)
		val = (
			ka[key]				if key in ka else
			pa[key]				if isint(key) and 0 <= key < n_pa else
			self._of_key(key)	if self._of_key else
			self._fill )
		return val
	#endregion
	...

class Fmt(str):
	''' Assigns str.format as call operation '''
	#region defs
	__name__,__qualname__	= 'Fmt', _mdl_name+'Fmt'
	__call__				= str.format
	_repr					= "F'{}'".format
	#endregion
	#region forms
	def __init_subclass__(cls, fmt=__call__,fab=None,call=None,repr=_repr):
		cls._fab = fab
		cls._call = call
		cls._fmtr = isstr(fmt, var='format')
		cls._repr = isstr(repr, var='format')
		cls.__call__ = cls.call
	def __new__(cls, s='', fab=None,call=None):
		''' ctor
			:param s:
			:param fab:
			:param call:
		'''
		fmt = super().__new__(cls,s)
		fmt._fab = fab
		fmt._call = call
		return fmt
	def __repr__(self):						return self._repr(self)
	def call(self,*pa,**ka):
		call,fab = self._call,self._fab
		sout = self._fmtr(*pa,**ka)
		call and call(sout)
		sout = sout if not fab else fab(sout)
		return sout
	def fmt_any(self,args=(),**ka):
		''' format *args* given as either *pa* iter or *ka* dict '''
		pa,ka = (
			((args,),ka)	if ischrs(args) or isntiter(args) else
			((),args|ka)	if ka and args and isdict(args) else
			((),args)		if isdict(args) else
			(args,ka) )
		pa = pa or ka.pop('pa',()) or ka.pop('*',())
		s_out = self.format(*pa,**ka)
		return s_out
	def of(self,args=(),ka=map0,**_ka):
		s_out = self.fmt_any(args,**_ka)
		fmt_out = self.__class__(s_out,**ka)
		return fmt_out
	fmt_pa=fmt_ka			= fmt_any
	format_seq=format_map	= fmt_any
	__truediv__=__mod__		= fmt_any
	__floordiv__			= of
	__add__					= str.format
	#endregion
	...

class Fmts(tuple):
	''' compound multi-formatter '''
	# todo: usages
	#region utils
	def __new__(cls, str=..., repr=..., to_str=str, to_repr=None, sep=None,**vars):
		raise NotImplementedError()
		if sep:
			str,repr = str.split(sep)
		str = to_str(str if str is not ... else repr)
		repr = (
			repr			if repr is not ... and not to_repr else
			_repr(str)		if repr is     ... and not to_repr else
			to_repr(str)	if repr is     ... and     to_repr else
			to_repr(repr) )
		s_out = str.__new__(cls,str)
		s_out.str,s_out.repr = str,repr
		for attr,mbr in vars.items():
			setattr(s_out,attr,mbr)
		return s_out
	def formats(self,*pa,**ka):
		ka = ka  # todo: extract from pa
		ss = [arg if fmt is None else fmt(arg) for fmt,arg in zip(self.fmts,pa)]
		s_out = self.fmt(*ss,**ka)
		return s_out
	__call__				= formats
	#endregion
	...

class FmtArgs(str):  # todo: obsolete; merge with existing more generalized format class
	''' Assigns format as default call operation and optionally remakes format args '''
	__name__,__qualname__	= 'FmtArgs', _mdl_name+'FmtArgs'
	to_aa					= None
	#region forms
	def __init__(self, fmt_str, to_aa=None):  # coerce_pa_ka=None
		super().__init__(fmt_str)
		if to_aa:
			self.to_aa = to_aa
	def format(self, *pa, **ka):
		try:
			if self.to_aa:
				pa,ka = self.to_aa(*pa,**ka)
			result = str.format(*pa,**ka)
		except Exception as e:
			e = e.__class__(*e.args, self, pa, ka)
			raise e
		return result
	
	# method aliases
	__call__				= format
	#endregion
	...

class FmtFab:
	''' create instance of a Format class from operations on a given string '''
	#region forms
	def __init__(self,*pa,to_str=str,**ka):
		self._pa,self._ka = pa, ka
		self.to_str = to_str
	def fmt_of(self,s):
		result = Fmt(s,*self._pa,**self._ka)
		return result
	def fmtf_of(self, s):
		result = FmtF(s,*self._pa,**self._ka)
		return result
	def fmtc_of(self, s):
		result = Fmtc(s,*self._pa,**self._ka)
		return result
	__mul__,__truediv__		= fmtf_of,fmt_of			# fixme: standardize
	#endregion
	...
	
class FmtSeq(Fmt,repr="F*'{}'",fmt=Fmt.fmt_pa):					''' default call is str.format(*arg0) '''
class FmtMap(Fmt,repr="F**'{}'",fmt=Fmt.fmt_ka):				''' default call is str.format(**arg0) '''
class FmtOut(Fmt,repr="P*'{}'",**_D.ka_out):					''' default call is print(str.format(*arg0)) '''
class FmtOutSeq(Fmt,repr="P*'{}'",fmt=Fmt.fmt_pa,**_D.ka_out):	''' default call is print(str.format(*arg0)) '''
class FmtOutMap(Fmt,repr="P**'{}'",fmt=Fmt.fmt_ka,**_D.ka_out):	''' default call is print(str.format(**arg0)) '''
class FmtFull(FullFmtr, Fmt):									''' default call is FullFmtr.format(*arg0) '''
class FmtFullOut(FullFmtr, FmtOut):								''' default call is print(FullFmtr.format(*arg0)) '''

_D.the_full_fmt_map			= FullFmtVars()
fmt_fab = _D.F = F			= FmtFab()

# Fmt,FmtP,FmtK,FmtAA			= Format,FormatSeq,FormatMap,FormatArgs
FmtP,FmtK,FmtAA				= FmtSeq,FmtMap,FmtArgs
FmtO,FmtOP,FmtOK			= FmtOut,FmtOutSeq,FmtOutMap
FmtF						= FmtFull
FullFormatsVars				= FullFmtVars
#endregion

#region indent devcs
class Indent(str):
	#region utils
	def indent(self, obj, count=1):
		result = f'{self*count}{obj}'
		return result
	__call__				= indent
	#endregion
	...
class IndentLines(str):
	#region utils
	def indent(self, text:str, depth=1, debug=''):
		prefix = str(debug)+self*depth
		result = (prefix+text).replace('\n', '\n'+prefix)
		return result
	def indent_seq(self, seq:list, depth=1):
		prefix = self*depth
		result = '\n'.join(map(str, seq))
		result = prefix+result.replace('\n', '\n'+prefix)
		return result
	__call__				= indent
	#endregion
	...
	
spaced						= IndentLines(' '*1)
spaced2						= IndentLines(' '*2)
spaced4						= IndentLines(' '*4)
spaced8						= IndentLines(' '*8)
tabbed						= IndentLines('\t')
#endregion

#region strfab devcs
# draft: naming & placement
class StrFabs:
	chs						= tss('*        /            ')
	ops						= tss('__mul__  __truediv__  ')
	op_at_ch				= mapeach(chs+ops, 2*ops)
	ch_at_op				= mapeach(chs+ops, 2*chs)
	_ch_of_kop				= lambda kop,pfx: (pfx,StrFabs.ch_at_op.get(kop[0],kop[0]))
	#region utils
	# fixme: clsutils: stage_cls(fab,prefab=None) = stage(prefab,*,**)(fab,*,**)
	@property
	def fab_cache(self):
		if self._fab_cache is None:
			from pyutils.structures.maps import LRUMap
			self._fab_cache = LRUMap
		return self._fab_cache
	def __init__(self,fab_at,fab_cache=None,limit=_D.strfabs_lrus_limit,add_known=True,pfx='S'):
		self.fab_at = fab_at
		self.fab_at.update((self.op_at_ch[k],self.fab_at[k]) for k in [*self.fab_at] if k != self.op_at_ch[k])
		self.limit = limit
		self._fab_cache = fab_cache
		self.caches = {}
		self.pfx = pfx
		add_known and _D.strfabs_known.append(self)
	def icached(self,to_kop=_ch_of_kop):
		ka = dict(pfx=self.pfx)
		for k_op,cache in zof(self.caches):
			k_op = k_op if not to_kop else to_kop(k_op,**ka)
			for k_arg,obj in zof(cache):
				yield k_op,k_arg,obj
	def repr(self):
		ss_out = ['  {}{}{!r}'.format(*k_op,k_arg,obj) for k_op,k_arg,obj in self.icached()]
		s_out = '\n'.join(ss_out)
		return s_out
	@staticmethod
	def repr_known():				return ss(keep(_D.strfabs_known),fab='\n...\n'.join)

	def fab(self,op,arg,*pa,**ka):
		# resolve inputs
		k_op = op,fab = op,self.fab_at[op]
		k_arg = isstr(arg,orfab=id)

		# resolve output
		cache = self.caches.get(k_op) or self.caches.setdefault(k_op, self.fab_cache(limit=self.limit))
		obj = cache.get(k_arg) or cache.setdefault(k_arg, fab(arg,*pa,**ka))
		return obj

	# fixme: populate in class init
	# fixme: populate in class init from fab ops
	def __mul__(self,arg):			return self.fab('__mul__',arg)
	def __truediv__(self,arg):		return self.fab('__truediv__',arg)
	def __xor__(self,arg):			return self.fab('__xor__',arg)
	def __pow__(self,arg):			return self.fab('__pow__',arg)
	def __mod__(self,arg):			return self.fab('__mod__',arg)
	def __bool__(self):				return bool(self.caches)
	__repr__ 				= repr
	#endregion
	...

_prefabs					= {
	hss						: lambda arg,*pa: (tss(arg), pa),
}
# draft: naming & placement
_  = ISS					= StrFabs({'*':ss})
HS = HSS					= StrFabs({'*':hss})
LS = LSS					= StrFabs({'*':lss})
TS = TSS					= StrFabs({'*':tss})
S							= StrFabs({'*':tss, '/':Fmt})
PS							= StrFabs({'/':Path})
#endregion

#region strs devcs
class Str(str):
	''' simple str with optional repr form '''
	#region forms
	def __new__(cls, str=..., repr=..., to_str=str, to_repr=None, sep=None,**vars):
		if sep:
			str,repr = str.split(sep)
		str = to_str(str if str is not ... else repr)
		repr = (
			repr			if repr is not ... and not to_repr else
			_repr(str)		if repr is     ... and not to_repr else
			to_repr(str)	if repr is     ... and     to_repr else
			to_repr(repr) )
		s_out = str.__new__(cls,str)
		s_out.str,s_out.repr = str,repr
		for attr,mbr in vars.items():
			setattr(s_out,attr,mbr)
		return s_out
	def __str__(self):		return self.str
	def __repr__(self): 	return self.repr
	#endregion
	...
class Repr(Str):
	#region forms
	def __new__(cls, repr, str=..., sep=..., **ka):
		return super().__new__(repr=repr, str=str, **ka)
	#endregion
	...

class StrColl:
	''' Class for operating on a string as a list of strings split with the given delimiter. '''
	fmtstr					= '{}`'
	#region forms
	def __init_subclass__(cls, **ka):
		infuse(cls,ka)
		cls.str = property(lambda self: str(self))
	@classmethod
	def init(cls, obj, strings:str='', delim:str='[, ]+', join:str=None, to_str=str, fmtstr:str=None):
		obj.delim = obj.repr_delim = delim
		obj.join = isinstance(join, str) and join.join or ' '.join
		to_str = isinstance(to_str, str) and to_str.format or to_str
		if isinstance(strings, str):
			obj.fmtstr = fmtstr or cls.fmtstr
			found = re.search(r''+delim, strings)
			obj.join = found.group().join if found and join is None else obj.join
			items = strings and re.split(r''+delim, strings) or []
			items = [to_str(item) for item in items]
		else:
			items = [to_str(item) for item in strings]
		return items
	def format(self, *spec, fmt=None,fabs=None):
		fmt = fmt is None and self.fmtstr or fmt
		s = self.join(self)
		s = s if not fmt else fmt.format(s)
		return s
	#endregion
	#region utils
	def __getattr__(self, attr):
		if attr not in self:
			raise AttributeError(attr)
		return attr
	def __add__(self, tokens, delim=None):	raise NotImplemented()
	def extend(self, tokens, delim=None):	raise NotImplemented()
	
	# __str__ = __format__ = format
	__repr__ = __format__ = format
	__iadd__ = extend
	#endregion
	...

class LStrs(StrColl,list):
	#region forms
	def __init__(self, *pa, **ka):
		items = self.init(self, *pa, **ka)
		list.__init__(self, items)
	#endregion
	#region utils
	def __add__(self, tokens, delim=None):
		result = copy(self).extend(tokens, delim=delim)
		return result
	def extend(self, tokens, delim=None):
		if not isinstance(tokens, StrColl):
			tokens = LStrs(tokens, delim=delim or self.delim)
		list.extend(self, tokens)
		return self
	# method aliases
	__iadd__ = extend
	#endregion
	...

class HStrs(StrColl,set):
	#region forms
	def __init__(self, *pa, **ka):
		items = self.init(self, *pa, **ka)
		set.__init__(self, items)
	#endregion
	...

class TStrs(StrColl,tuple):
	''' Class for operating on a string as a list of strings split with the given delimiter. '''
	#region forms
	def __new__(cls, *pa, **ka):
		obj = Obj()
		items = cls.init(obj, *pa, **ka)
		self = tuple.__new__(cls, items)
		[setattr(self, k, v) for k, v in obj.__dict__.items()]
		return self
	#endregion
	#region utils
	def __add__(self, tokens, delim=None):
		return self.__class__(list(self)+list(tokens))
	def extend(self, tokens, delim=None):
		if not isinstance(tokens, TStrs):
			tokens = TStrs(tokens, delim=delim or self.delim)
		list.extend(self, tokens)
		return self
	__iadd__ = extend
	#endregion
	...

class HNames(HStrs):
	def __init__(self, *pa, **ka):
		super().__init__(*pa, delim='\.', **ka)
class LNames(LStrs):
	def __init__(self, *pa, **ka):
		super().__init__(*pa, delim='\.', **ka)
class TNames(TStrs):
	def __new__(cls, *pa, **ka):
		out = super().__new__(cls, *pa, delim='\.', **ka)
		return out

class MatchReplacer:
	#region utils
	def __init__(self, func):
		self.func = func
	def __call__(self, match):
		if match is None: return None
		found = match[bool(match.lastindex)]
		print(f'XStr.replace({match.groups()}, length={match.lastindex}) => {found}')
		result = self.func(found)
		return result
	#endregion
	...

HSS,LSS,TSS					= HStrs,LStrs,TStrs
Strs,Names					= LStrs,LNames
strs_from.__doc__			= StrsFrom.__doc__.replace(' Strs', ' list of strs')
#endregion

#region scratch
'''
*		F*'{} {} {a} {b}'
/		F/'{} {} {a} {b}'
%		F%'{} {} {a} {b}'

*		F*'{}'*ss('I  II')
@		F*'{}'@ss('I  II')
<		F*'{}'<ss('I  II')
>		F*'{}'>ss('I  II')


|		F|'{} {} {a} {b}'
@		F@'{} {} {a} {b}'
^		F^'{} {} {a} {b}'
-		F-'{} {} {a} {b}'


*		F*'{}'*ss('I  II')
/		F*'{}'/ss('I  II')
+		F*'{}'+ss('I  II')
|		F*'{}'|ss('I  II')

<		F*'{}'<ss('I  II')
>		F*'{}'^ss('I  II')
<<		F*'{}'>ss('I  II')

<		F*'{}'<ss('I  II')
<=		F*'{}'^ss('I  II')
<<		F*'{}'>ss('I  II')

'''


'''
Consider the following as replacement for fstr:


class NamespaceFormatter(Formatter):
   def __init__(self, namespace={}):
       Formatter.__init__(self)
       self.namespace = namespace

   def get_value(self, key, args, kwds):
       if isinstance(key, str):
           try:
               # Check explicitly passed arguments first
               return kwds[key]
           except KeyError:
               return self.namespace[key]
       else:
           Formatter.get_value(key, args, kwds)
           
...

fmt = NamespaceFormatter(globals())

greeting = "hello"
print(fmt.format("{greeting}, world!"))
'''
#endregion