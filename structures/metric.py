from __future__ import annotations

import re

# from datetime import datetime as dateclock, timedelta, date, time
from dataclasses import dataclass, field
from typing import Any, Callable, Union as Or

from pyutils.defs.funcdefs import asis
# from pyutils.structures.strs import Strs, TStrs
from pyutils.structures.maps import init_cls, Vars

# todo: RELOCATE
#region misc devcs
import operator as op

@dataclass
class IterProp:
	''' Iterator from instance attribute, Immutable access. '''
	name: str
	convert: Callable	= None
	def __get__(self, obj, cls):
		out = self if obj is None else getattr(obj, self.name)
		out = self.convert and map(self.convert, out) or iter()
		return out
	def __call__(self, obj):
		out = self if obj is None else getattr(obj, self.name)
		out = self.convert and map(self.convert, out) or iter()
		return out

@dataclass
class Operand:
	''' the quantity on which an operation is to be done '''
	value: Any			= None
	operator: Callable	= asis
	
@dataclass(repr=False)
class Operands:
	values: Any			= None
	_operator: Callable	= asis
	__iter__			= IterProp('values')
	operator			= property(lambda self: self._operator, lambda self, value: setattr(self, '_operator', value))
	def __getitem__(self, ind):
		out = self.operator(self.values[ind])
		return out

class Operands(list):
	_operator: Callable = staticmethod(asis)
	values				= property(lambda self: self)
	__iter__			= IterProp('values')
	operator			= property(lambda self: self._operator, lambda self, value: setattr(self, '_operator', value))
	def __getitem__(self, ind):
		out = self.operator(super(Operands,self).__getitem__(ind))
		return out

class Accums(Operands):
	polarity = 1
class Negates(Operands):
	operator			= _operator = op.__neg__
	polarity			= -1
	__iter__			= IterProp('values', operator)
#endregion

#region unit defs
class DefsUnits:
	# present_dt			= timedelta()
	pass
DDUU = _D			= DefsUnits
#endregion

#region unit devcs
@dataclass(frozen=True)
class Denom:
	''' A singular named unit and its representative instance '''
	name: str			= ''
	abrv: str			= ''
	base: Any			= None
	metric_name: str	= 'Metric'
	#region forms
	def __getattr__(self, attr):					return getattr(self.base, attr)
	def __mul__(self, value: int) -> Unit:			return self.metric.Unit(denom=self, value=value)
	def __truediv__(self, value: int) -> Unit:		return self.metric.Unit(denom=self, value=1/value)
	def __lt__(self, other):						return self.base < other.base
	def __dir__(self):								return 'name abrv base'.split()
	@property
	def metric(self): return globals()[self.metric_name]
	#endregion
	...

class Metric:
	''' Example:
		denoms = (
			i, ki, mi,  ) = (
			ion, kilo_ion, mill_ion,  ) = (
			Denom('ion', 'i', 			Decimal(10**0), getter),
			Denom('kilo_ion', 'ki', 	Decimal(10**3), getter),
			Denom('mill_ion', 'mi', 	Decimal(10**6), getter),  )
		abrvs = Strs('i ki mi')
		names = Strs('ion kilo_ion mill_ion')
	'''
	#region defs
	getter				= lambda: Metric
	Unit				= lambda *pa, **ka: Unit(*pa, **ka)
	Units				= lambda *pa, **ka: Units(*pa, **ka)
	
	# sequence of immutable denominators
	denoms = []
	
	# associated unit strings as abbreviations and names and the associative map
	abrvs = []
	names = []
	
	# dict access to unit representations by name or abrv
	denom	= Vars({ k: v 		for k, v in zip(names+abrvs, denoms*2)})	# usage: Metric.denom.mi == Denom('mill_ion', ...)
	name	= Vars({ k: v.name	for k, v in zip(names+abrvs, denoms*2)})	# usage: Metric.name.mill_ion == 'mi'
	abrv	= Vars({ k: v.abrv	for k, v in zip(names+abrvs, denoms*2)})	# usage: Metric.abrv.mill_ion == 'mill_ion'
	base	= Vars({ k: v.base	for k, v in zip(names+abrvs, denoms*2)})	# usage: Metric.base.mi == Decimal(10**6)
	#endregion
	#region forms
	def __init_subclass__(cls, **ka):
		init_cls(cls,**ka)
	#endregion
	...

class Units:
	#region forms
	def __init_subclass__(cls, **ka):
		init_cls(cls,**ka)
	#endregion
	...
class Unit:
	''' A named unit, its representative instance and a quantity '''
	#region defs
	_metric				= Metric
	_fmt_abrv			= '{value}{abrv}'
	_fmt_name			= '{value}{name}s'
	_unit_factory		= lambda cls, name, value: cls(denom=cls._metric.denom[name], value=value)
	name				= property(lambda self: self.denom.name)
	abrv				= property(lambda self: self.denom.abrv)
	metric_name			= property(lambda self: self.denom.metric_name)
	base				= property(lambda self: self.denom.base)
	base_view			= property(lambda self: self.denom.base * self.value)
	metric				= property(lambda self: globals()[self.metric_name])
	#endregion
	#region forms
	def __init_subclass__(cls, **ka):
		init_cls(cls,**ka)
	def __init__(self, denom, value=1, **ka):
		self.value = value
		self.denom = denom
	def format(self, fmt_spec=None, abrv=True)->str:
		fmt = abrv and self._fmt_abrv or self._fmt_name
		out = fmt.format(**self.attr_dict)
		return out
	def format_name(self) -> str:	return self.format(abrv=False)
	def __repr__(self):				return f'{self.__class__.__name__}({self.format()!r})'
	def __hash__(self): 			return hash((self.denom.base, self.value))
	def __neg__(self) -> Unit:		return self.__class__(denom=self.denom, value=-self.value)
	def __bool__(self)->bool:		return bool(self.value)
	#endregion
	#region utils
	def __lt__(self, other:Unit) -> bool:
		other = other.base_view if isinstance(other, Unit) else other
		out = self.base_view < other
		return out
	def __eq__(self, other:Unit) -> bool:
		other = other.base_view if isinstance(other, Unit) else other
		out = self.base_view == other
		return out
	def __ne__(self, other:Unit) -> bool:
		return not self == other
	def __add__(self, other:Unit) -> Unit:
		out = self.metric.Units(other)
		out += self
		return out
	def __radd__(self, other:Unit) -> Unit:
		out = other + self.base_view
		return out
	def __sub__(self, other:Unit) -> Unit:
		out = -self.metric.Units(other)
		out += self
		return out
	def __rsub__(self, other:Unit) -> Unit:
		out = other - self.base_view
		return out
	def __mul__(self, val:int|float) -> Unit: return self.__class__(denom=self.denom, value=self.value * val)
	def __truediv__(self, val:int|float|Unit) -> Unit:
		out = (
			self.base_value / val.base_value							if isinstance(val, Unit) else
			self.__class__(value=self.value / val, denom=self.denom)	if isinstance(val, (int,float)) else
			self.base_view / val )
		return out
	def __rtruediv__(self, val:int|float|Unit) -> Unit:
		out = (
			val.base_value / self.base_value							if isinstance(val, Unit) else
			self.__class__(value=val / self.value, denom=self.denom)	if isinstance(val, (int,float)) else
			val / self.base_view )
		return out
	def __floordiv__(self, val:int|float|Unit) -> Unit:		return (self/val).__trunc__()
	def __rfloordiv__(self, val:int|float|Unit) -> Unit:	return (val/self).__trunc__()
	@classmethod
	def notation_units(cls, notation, factory=None):
		units = re.findall(cls._metric.notation_ptrn, notation)
		factory = factory or cls._unit_factory
		if factory:
			units = [factory(cls, name, value) for value, name in units]
		return units
	@classmethod
	def notation_unit(cls, notation, at_index=0, factory=None):
		units = cls.notation_units(notation, factory=factory)
		unit = units[at_index]
		return unit
	def __getattr__(self, attr):
		''' imbue Unit classes with methods and attributes of their base_view '''
		out = getattr(self.base_view, attr)
		return out
	def __reduce_ex__(self, protocol=None):
		''' serialize this instance '''
		return (self.__class__, (str(self),))
	@property
	def attr_dict(self):
		out = dict(**self.__dict__, **{field:self.denom.__dict__[field] for field in self.denom.__dataclass_fields__})
		return out
	__rmul__			= __mul__
	__str__				= format
	_reduce_encode		= format_name
	attrs				= attr_dict  # legacy property alias
	#endregion
	...
#endregion
