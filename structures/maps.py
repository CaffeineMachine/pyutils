from collections import Counter, OrderedDict

from pyutils.utilities.collbasics import *
from pyutils.structures.args import *
from pyutils.patterns.mapper import *

#region map utils
DD.rx_fm_pfx			= r'\s+'
DD.rx_fm				= fr'{DD.rx_fm_pfx}([a-zA-Z_]\w+)?:?[^\.\d\s]*(\d+)?(\.\d+)?'
DD.rx_fm				= fr'{DD.rx_fm_pfx}([a-zA-Z_]\w+)?:?([^\.\d\s]*)(\d+)?(\.\d+)?([^\.\d\s]*)'

def ritemz_of_mmap(kkvv,fab_vals=set,add_val=set.add,**ka):
	kkvv_links = LMMap(ivkvk_of_kkvv(kkvv),fab_vals=fab_vals,add_val=add_val,**ka)
	# kkvv_links = Map(ivkvk_of_kkvv(kkvv),fab_vals=fab_vals,add_val=add_val,**ka)
	return kkvv_links

def find_kkrrx(kkrrx,find=None,k_root=r'^_|^root_|_root$', on_overlap=False,deepest=10):
	''' ...
		:param kkrrx:
		:param find:
		:param k_root:
		:param on_overlap:
		:param deepest:
	'''
	# resolve inputs
	k_root = k_root in kkrrx and k_root or first(keeprx(kkrrx,k_root),None) or first(keeprx(kkrrx,k_root),None)
	fmterr_kroot,fmterr_type = 'find:{} as str needs a defined k_root'.format,'expected {{dict,str}} for find:{}'.format
	
	# resolve output
	def find_of(find=None,*kk_up,found=None, on_overlap=on_overlap,deepest=deepest,**ka):
		''' ...
			:param find:
			:param kk_up:
			:param found:
			:param on_overlap:
			:param deepest:
			
			NOTE: recursive loop constant: length of search string *find* is less than parent for each pass
		'''
		# todo: make recursive and format results with parent and key: key_aa,key_ab,key_aba,key_abb
		# resolve inputs
		assert not deepest <= len(kk_up), f'exceeded max depth.  {deepest} <= len({kk_up})'
		find = (
			find						if isdict(find) else
			{k_root:find}				if isstr(find) and k_root else
			throw(fmterr_kroot(find))	if isstr(find) and not k_root else
			throw(fmterr_type(find)) )
		found = found or {}
		
		# eval subsets of find
		zkfind = tkeep(find.items(), kkrrx.__contains__, at=Ndx.key)
		for nth,k,find_k in iidxj(zkfind):
			*kk,rrx = *kk_up,k, kkrrx[k]
			findk_at = rrx.find(find_k)
			found = find_of(findk_at, *kk, found=found,on_overlap=on_overlap,deepest=deepest,**dict(ka,nth=nth))
		
		# resolve output
		found = combine(found,find, kk=kk_up,on_overlap=on_overlap,**ka)
		return found
	found = find is None and find_of or find_of(find)
	return found

def zvv_of_sforms(fms=None,fab=tuple,at=None,fill_n=None,rx_fm=DD.rx_fm):
	# todo: implement single StrForm class
	at = Ndx[at]
	def ivv_of_fms(fms):
		s_fms = sj(' ',afx(' ',ss(fms)))
		ifound = re.findall(rx_fm,s_fms)
		nth = 0
		for *_,k,pfx,n_min,f_max,sfx in ifound:
			if k or n_min or f_max:
				k,nth = k or nth, nth+1
				vv = *_,n,_ = k,pfx,(
					FloatStyle(n_min+f_max)		if f_max else
					int(n_min)					if n_min else
					fill_n ),sfx
				vv = vv if at is None else at(vv) if iscall(at) else vv[at]
				yield vv
	zvv = fms is None and ivv_of_fms or ivv_of_fms(fms)
	zvv = zvv if not fab else fab(zvv)
	return zvv

mapfound_of_rrxs		= find_kkrrx
#endregion

#region map devcs
kkinto_ss				= dict.fromkeys(ss('<EMPTYSTR>  <STR0>'),'')

class FloatStyle(float):
	__str__ = __repr__	= lambda self: str(float(self)).lstrip('0')

# class Obj:
# 	''' a plain object except with mutable vars. 
# 		vars can be accessed with brackets akin to a collection.
# 		subclasses can define default vars via subclass-kwargs, annotations and/or factorys
# 	'''
# 	#region forms
# 	'''	Note: Obj and VarMap behave similar but are distinct:
# 	 	- Obj keeps items in self.__dict__ exclusively by default 
# 		- VarMap keeps items in super(dict,self) & self.__dict__ distinct and inserts to each using VarMap.is_attr  
# 	'''
# 	# todo: naming this Vars is more accurate than for VarMap
# 	__init_factorys__	= init_factorys
# 	def __init_subclass__(cls, attrs:iter=(), vals:list|object=None, **vars):
# 		vars_anno = cls.__annotations__ and annovars(cls)
# 		vars_attr = mapeach(attrs,vals)
# 		vars_out = vars_anno|vars_attr|vars
# 		for attr,val in vars_out.items():
# 			setattr(cls,attr,val)
# 	def __init__(self,items=(),pa=(),**_items):
# 		self.__init_factorys__()  # todo: use annoinit; pass ka to factorys
# 		items = dict(items)|_items
# 		self.__dict__.update(items)
# 		# super().__init__(*pa)
# 	#endregion
# 	...

class Attrs(tuple):
	#region forms
	def __new__(cls, attrs):
		attrs = tss(attrs)
		obj = super().__new__(cls, attrs)
		obj.__dict__.update(zip(attrs,attrs))
		return obj
	def get(self,*pa,**ka):			return self.__dict__.get(*pa,**ka)
	def __getattr__(self,attr):		return self.__dict__.__getitem__(attr)
	#endregion
	...

class KKS:
	kk: iter
	ss: iter
	#region forms
	def __init__(self,*pa,**ka):
		# resolve inputs
		annoinit(self,*pa,**ka)
		self.kk = Attrs(self.kk)
		self.ss = tuple(kkinto_ss.get(s,s) for s in ss(self.ss,subs=yes))
		self.at = mapvars(self.kk,self.ss)
		
		infuse(self,self.at)
	def __repr__(self):			return "{}(kk='{}',ss='{}')".format(self.__class__.__name__,' '.join(self.kk),' '.join(self.ss))
	def __len__(self):			return 2
	def __iter__(self):			return iter((self.kk,self.ss))
	def __getitem__(self,key):	return self.at[key]
	def keys(self):				return iter(self.kk)
	#endregion
	...

class StrForms:
	kk: iter			= None
	ffm: iter			= None
	fmt: str			= None
	fmt_fm: str			= None
	opts: dict			= FinalMap(join='\n',fab_hdr=None)
	#region defs
	s_kk				= property(lambda self: self.format_row(self.kk))
	zkn					= property(lambda self: zvv_of_sforms(self._ffm,at='all'))
	ikk					= property(lambda self: zvv_of_sforms(self._ffm,at='key'))
	inn					= property(lambda self: zvv_of_sforms(self._ffm,at=2))
	tffm				= property(lambda self: tuple(map(lambda fm: ss(fm,fab=''.join), zvv_of_sforms(self._ffm,at=slice(1,None)))))
	#endregion
	#region forms
	def __init__(self,*pa,**ka):
		# resolve inputs
		annoinit(self,*pa,**ka)
		self.kk,self.ffm = self.ffm and (self.kk,self.ffm) or (None,self.kk)  # ffm is required but kk is expected first
		self._ffm = tss(self.ffm)
		self.ffm = self.tffm
		self.kk = self.kk or self.ikk
		self.kk = any(self.kk) and Attrs(self.kk) or self.kk
		self._fmt_aa = self.fmt.format(*(f'{{{k}:{fm}}}' for k,fm in zip(self.kk or gen(''),self.ffm)))
		self._repr = self.fmt.format(*(f'{k}:{fm}' for k,fm in zip(self.kk or gen(''), self.ffm)))
	def __repr__(self):		return 'FFM/' + repr(self._repr)
	def __eq__(self,other):	return hasattr(other,'__dict__') and all(v == other.__dict__[k] for k,v in annovars(self).items())
	#endregion
	#region utils
	def format(self,*_pa,pa=None,ka=None,**_ka):
		pa,ka = pa or _pa, ka or _ka
		if self.kk:
			ka = dict(mapeach(self.kk,pa), **ka)
		s_out = self._fmt_aa.format(*pa,**ka)
		return s_out
	def format_row(self,row=None,fab_row=None,**ka):
		s_out = self.format(pa=row,**ka)
		s_out = s_out if not fab_row else fab_row(s_out)
		return s_out
	def format_rows(self,rows, fab=None,fab_row=None, **ka):
		ss_out = (self.format(pa=row,**ka) for row in rows)
		ss_out = ss_out if not fab_row else map(fab_row,ss_out)
		ss_out = ss_out if not fab else fab(ss_out)
		return ss_out
	def format_table(self,rows, *pa, join=None, fab_hdr=None, **ka):
		join = isstr(isnone(join,put=self.opts['join']),var='join')
		s_hdr = self.format_row(self.kk,fab_row=isnone(fab_hdr,put=self.opts['fab_hdr']))
		s_enums = join(afx(s_hdr, self.format_rows(rows,*pa,**ka)))
		return s_enums
	
	__call__			= format
	__floordiv__=__pow__= format_rows
	__xor__				= format_table
	#endregion
	...

@dataclassown
class KKRx:
	''' rx (RegeX) container with subgroup pairs of key:rx. Same as RRx except takes keys first. '''
	kk: iter						# rx|regex group name keys
	rrx: iter						# rx|regex group patterns to merge
	fmt: str			= None		# format used to merge rx|regex groups
	#region defs
	_rxc				= None
	ss					= property(lambda self: tkeep(self.kk))
	fmtk_k				= property(lambda self: self.fmt_of(fmt_krx=RXSS.fmtk_k))
	fmtk_in				= property(lambda self: self.fmt_of(fmt_krx=RXSS.fmtk_in))
	fmtk_to				= property(lambda self: self.fmt_of(fmt_krx=RXSS.fmtk_to))
	fmtk_into			= property(lambda self: self.fmts_of(ifmt_krx=RXSS.fmtk_into))
	#endregion
	#region forms
	def __post_init__(self):
		# resolve inputs
		self.kkext,self.rrx = tss(self.kk),tss(self.rrx)
		self.kk = tss(self.kk, lambda k: re.findall(RXSS.grp_k,k)[0])
		
		# resolve composite regex
		self.rx = rx_of_kkrrx(self.kkext,self.rrx,self.fmt)
		
		# assign members
		self.at = mapvars(self.kkext,self.rrx)
		if self.kk != self.kkext:
			self.at_name = Vars((name,rx) for name,rx in zip(self.kk,self.rrx) if name)
		else:
			self.at_name,self.kk = self.at,self.kkext
		infuse(self,self.at_name)
		self.s = self.name = KeyVars(keep(self.kk))  # access key strings as attrs; rxat.s.MISSING_KEY gives an error
	def __repr__(self):		return repr(self.rx)
	
	def rx_of(self,kk=miss,rrx=miss,fmt=None,fab=False,**ka):
		''' create ->rx by modifying 1 or more elements of this rx instance
			:param kk: regex|rx group name keys
			:param rrx: regex|rx group patterns to merge
			:param fmt: format used to merge regex|rx groups
			:param fab: func to create output ->rx. when given False|True output will be str|class instance resp.
		'''
		# resolve inputs:  => kk,rrx,fmt,fab
		fab = istrue(fab,put=self.__class__) or rx_of_kkrrx
		if kk is rrx is miss:
			kk,rrx,fmt = self.kk,self.rrx,self.fmt
		elif kk and rrx is miss:
			rrx = iseqmap(self.at,kk)	# must get new rrx for subset of given keys kk
		elif rrx and kk is miss:
			fmt = self.fmt				# default self.fmt is only valid when len(self.kk) == len(kk)
		
		# resolve output
		rx = fab(kk,rrx,fmt,**ka)
		return rx
	def fmt_of(self,kk=miss,rrx=miss,fmt=None,**ka):
		return self.rx_of(kk=kk,rrx=rrx,fmt=fmt,fab=rx_of_kkrrx,**ka)
	def fmts_of(self,kk=miss,rrx=miss,fmt=None,**ka):
		return self.rx_of(kk=kk,rrx=rrx,fmt=fmt,fab=rxs_of_kkrrx,**ka)
	@property
	def rxc(self):
		if not self._rxc:
			self._rxc = re.compile(self.rx)
		return self._rxc
	
	__str__				= __repr__
	#endregion
	#region utils
	def get(self,key,*fill):
		rx = (fill and self.at.get or self.at.__get__)(key,*fill)
		return rx
	def into(self,s,rx_to=None):
		rx_to = rx_to or self.rx_to
		s_to = RXC.sub(self.rxc,rx_to,s)
		return s_to
	def find(self,*pa, rxutil='search', re_ns=RX, fill=map0, **ka):
		rxutil = rxutil if isntstr(rxutil) else getattr(re_ns,rxutil)
		is_rxfind(rxutil) and ka.setdefault('key',dict)  # use match.groupdict() as default result
		result = rxutil(self.rx,*pa,**ka, fill=fill)
		return result
	#endregion
	...

@dataclass
class RRx(KKRx):
	''' rx (RegeX) container with subgroup pairs of key:rx. Same as KKRx except takes regexs first. '''
	rrx: iter			= ()
	kk: iter			= ()
	fmt: str			= None

class StackMap:
	''' multiple maps unified to behave as a single map with a superset of keys.
		key access references the first map to contain the key.
	'''
	#region forms
	def __init__(self, stack=None, missing=miss, call_missing=None, map_hint=None):
		''' ctor
			:param stack: sequence of maps with the first taking precedence
			:param missing: fall through value result when no maps contain key
			:param call_missing:
			:param map_hint: todo: a mechanism to narrow applicable maps when accessing a key
		'''
		self.stack = stack or []
		self.fall_thru = None if missing is miss else FullMap(missing, call_missing=call_missing)
		self.map_hint = map_hint
		self.known_keys = []
	#endregion
	#region access
	def locate(self, key, allow_fall_thru=True, error=False):
		''' identify the map in which a given item resides '''
		mapping = next(iter((mapping for mapping in self.stack if key in mapping)), None)
		mapping = self.fall_thru if mapping is None and allow_fall_thru else mapping
		if error and mapping is None:
			raise KeyError(key)
		return mapping
	def get(self, key, default=miss):
		mapping = self.locate(key, False)
		result = miss.fillin(default) if mapping is None else mapping[key]
		return result
	def __getitem__(self, key):
		mapping = self.locate(key, error=True)
		result = mapping[key]
		return result
	#endregion
	...

class StackRemap(ValRemapper, StackMap):
	def __init__(self, stack=None, *args, to_value=None, given=ValRemapper.gives.value, **kargs):
		ValRemapper.__init__(self, to_value, given)
		StackMap.__init__(self, stack, *args, **kargs)

class Ratio(Counter): ''' todo: allow ratio arbitrary attrs '''
class HitRatio(Ratio):
	''' track & notify changes to a hit/miss ratio '''
	#region defs
	_fmt_nvar			= 'n_{}'
	# _fmt_stats			= '< {0.hit_ratio:.0%%} with %s hits & %s misses of {0.n_used} uses >'%('{0.n_hit}','{0.n_miss}')
	# _fmt_stats			= '<{r} = {h}/{u} = ({u}-{m})/{u} >'.format('{0.hit_ratio:.0%}','{0.n_hit}','{0.n_miss}','{0.n_used}')
	_rep_stats			= mapeach('r h m u',ss('hit_ratio:03.0%  n_hit:2  n_miss:2  n_used:2','{{0.{}}}'))
	_fmt				= '<{r} = ({h} = -{m} +{u}) / {u} >'
	_fmt_stats			= _fmt.format(**_rep_stats)
	_kk					= tss('hit  miss')
	n_used				= property(lambda self: sum(self.values()))
	hit_ratio			= property(lambda self: (self.n_hit/self.n_used) if self.n_used else 1)
	miss_ratio			= property(lambda self: self.n_used and (self.n_miss/self.n_used))
	#endregion
	#region forms
	def __init__(self, log_on='', hit=0, miss=0, **ka):
		''' ctor
			:param log_on: attrs on which to log a ratio update
			:param hit:    initial hit count. usually 0
			:param miss:   initial miss count. usually 0
		'''
		self.fmt_stats = exact(fmtof(self._fmt_stats))
		self.fmt_nvar = exact(fmtof(self._fmt_nvar))
		self.kk = self._kk
		self.kk_n = hcxpas(self.fmt_nvar,self.kk)
		self.log_on = exact(
			never		if not log_on else
			log_on		if iscx(log_on) else
			hss(istrue(log_on,put=self.kk)).__contains__ )
		super().__init__()
		self.update(n_hit=hit,n_miss=miss,**ka)
	def __repr__(self,fmt=None):
		return self.fmt_stats(self)
	def __getattr__(self,attr):
		attr not in self.kk_n and throw(AttributeError(attr))
		return self[attr]
	def set_fmt(self,fmt=None):
		if not fmt:
			from pyutils.output.colorize import greenl, redl
			rep_stats = dict(self._rep_stats,**dict(h=greenl(self._rep_stats.h),m=redl(self._rep_stats.m)))
			fmt = self._fmt.format(**rep_stats)
		self.fmt_stats = exact(fmtof(fmt))
	def incr(self,attr,key='???',_fmt='{ratio!r:66} += {attr:>5} on {key!r}',_log=print,**ka):
		self.update(ss(attr,self.fmt_nvar))
		_log = self.log_on(attr) and _log
		_log and _log(_fmt.format(attr='+'.join(ss(attr)),key=key,ratio=self))
	#endregion
	...

class LRUMap(OrderedDict):
	''' ordered map with a limited length, ejecting LRU item as needed '''
	#region defs
	s_stats				= property(lambda self: self.stats)
	#endregion
	#region forms
	def __init__(self, *pa, limit:int=1024, log_on=no, **ka):
		assert limit > 0
		self.limit = limit
		self.stats = HitRatio(log_on=log_on)
		self.stats.set_fmt()
		super().__init__(*pa, **ka)
	def __repr__(self):
		s = f'{self.stats}\n{super().__repr__()}'
		return s
	def __getitem__(self, key):
		val = super().__getitem__(key)
		super().move_to_end(key)
		return val
	#endregion
	#region access
	def get(self, key, *fill):
		self.stats.incr(key in self and 'hit' or 'miss', key)
		val = super().get(key, *fill)
		return val
	def set(self, key, val):
		super().__setitem__(key, val)
		super().move_to_end(key)
		while len(self) > self.limit:
			oldkey = next(iter(self))
			super().__delitem__(oldkey)
		return val
	def clear(self, items=on,stats=on):
		stats and self.stats.reset()
		items and self.clear()
	
	__setitem__			= set
	#endregion
	...

class LRUMapArgs(LRUMap):
	to_key: iscx		= AA	# hash heavy lifting done by AA|Args; Note: use *kk_excl* in get/set to prune args 
	to_val: iscx		= None
	#region forms
	def __init__(self, *pa,deepest=2,**ka):
		annoinit(self,cls_key=0,*pa,**ka)
		self.to_key = exact(self.__dict__['to_key'])
		self.to_val = exact(self.__dict__['to_val'])
		self.deepest = deepest
		self.deep = 0
		super().__init__(*pa,**ka)
	#endregion
	#region access
	def has_args(self,pa,ka,to_key=miss):
		key = to_key or self.to_key(*pa,**ka)
		has = super().__contains__(key)
		return has
	def __contains__(self,args):		return self.has_args(*args)
	def get(self,*pa,to_key=miss,to_val=miss,**ka):
		# resolve inputs
		self.deep += 1
		if self.deep>=self.deepest:
			self.deep = 0
			raise RecursionError(f'fab failed to prevent reentry: {self.deepest=}')
		ka.update(to_val=to_val,_key=(key:=to_key or self.to_key(*pa,**ka)))
		
		# resolve output
		out = super().get(key,miss)
		out = out if out is not miss else self.set(*pa,**ka)
		self.deep -= 1
		return out
	def set(self,*pa,to_key=miss,to_val=miss,_key=miss,**ka):
		'''
			:param to_key: 
			:param to_val: 
			:param _key:   designate key to store   
		'''
		key = _key if _key is not miss else (to_key or self.to_key)(*pa,**ka)
		val = (to_val or self.to_val)(*pa,**ka)
		out = super().set(key,val)
		return out
	def wrap_cached_getr(self,getr=None,**_ka):
		''' stage or wrap *getr* if given which takes cachable args. can prepare *_ka* to be passed '''
		def cached_get_of_getr(getr):
			''' wrap given *getr* which takes cachable args *pa,**ka '''
			def cached_get(*pa,**ka):
				return self.get(*pa,**dict(_ka,**ka),to_val=getr)
			return cached_get
		wrapped = not getr and cached_get_of_getr or cached_get_of_getr(getr)
		return wrapped
	__call__			= wrap_cached_getr
	#endregion
	...

@dataclass
class KeyValMapIter:
	''' Generates a const *keys* to *vals* map at each *ivals* current iterated row *ind*ex '''
	ks: list
	llval: list			= None  # like DataFrame().values()
	ivals: list			= None
	_vals_at_key: dict	= None  # like DataFrame()
	_val_at_key: dict	= None
	ind: int			= -1
	cached: str			= 'vals'
	log: callable		= None
	#region defs
	_map_keys			= True
	iter				= property(lambda self: self.iterate())
	iitems				= property(lambda self: self.iterate(map_keys=True))
	ivalues				= property(lambda self: self.iterate(map_keys=False))
	dict				= property(lambda self: dict(self.items()))
	#endregion
	#region forms
	def __init_subclass__(cls, map_keys=True):
		cls._map_keys = map_keys
	def __post_init__(self):
		self._keyind_at = dict(iidxr(self.ks))
		if None is self.llval is self.ivals is self._val_at_key is self._vals_at_key:
			if istype(self.ks, dict):
				self._vals_at_key = self.ks
				for key, vals in self._vals_at_key.items():
					self._vals_at_key[key] = hasattr(vals, '__len__') and vals or list(vals)
		self.ks = tuple(self.ks)
		self.len = len(self._vals_at_key[self.ks[0]])
	#endregion
	#region access
	def keys(self):			return iter(self.ks)
	def values(self):		return iter(self._vals)
	def items(self):		return zip(self.ks, self._vals)
	def __iter__(self):
		result = self._val_at_key if self.map_keys else self._vals
		return iter(result)
	def iterate(self, map_keys=None):
		# todo: KeyValMapIter and values should be separate objects
		self.map_keys = isnone(map_keys, put=self._map_keys)
		while True:
			self.ind += 1
			self.log and self.log(end='\rind: %-6s' % self.ind)
			if self.ivals:
				self._vals = next(self.ivals)
				
			if self.cached in ('vals', 'maps'):
				if self.ind >= self.len: break
				self._vals = [self._vals_at_key[key][self.ind] for key in self.ks]
			if self.cached == 'maps':
				self._val_at_key = dict(zip(self.ks, self._vals))
			yield self
	def __getattr__(self, attr):
		value =  self[attr] if attr in self._keyind_at else throw(AttributeError(attr))
		return value
	def __getitem__(self, key):
		key_ind = key if not self._vals or istype(key, (int,slice)) else self._keyind_at[key]
		value = (
			self._val_at_key[key]				if self._val_at_key else
			self._vals[key_ind]					if self._vals else
			self._vals_at_key[key][self.ind]	if self._vals_at_key else
			throw()
		)
		return value
	def __setitem__(self, key, value, **ka):
		key_ind = self._keyind_at.get(key)
		key_pri, key_sec, coll = (
			(key, self.ind, self._vals_at_key)	if key_ind is None else
			(key, None, self._val_at_key)		if self._val_at_key else
			(key_ind, None, self._vals)			if self._vals else
			(key, self.ind, self._vals_at_key)	if self._vals_at_key else
			throw()
		)
		if key_sec is None:
			result = coll.__setitem__(key_pri, value, **ka)
		else:
			coll = coll.get(key_pri) or coll.setdefault(key_pri, [None]*self.len)
			result = coll.__setitem__(key_sec, value, **ka)
		return result
	def update(self, items=(), **ka):
		iitems = ijoin(isdict(items, fab=dict.items), ka and ka.items() or ())
		callpas(self.__setitem__, zpa=iitems)
	#endregion
	...

SSFm					= StrForms
StrMap					= KKS
RxAt					= KKRx
RXAt,RRX				= RxAt,RRx
#endregion

#region map implementations
# dictionary backed mapped classes
class Map(Mapped, dict): pass

class VarRemap(AttrEnum, Map): pass
class SetMap(SetMapper, Map): pass
class Remap(Remapper, Map): pass
class MapAny(Remapper, Map, to_key=hashable_of): pass

class ListValsMap(ValsMapper, Map, fab_vals=list,add_val='append'): pass
class SetValsMap(ValsMapper,  Map, fab_vals=set, add_val='add'): pass
class IntoKK(IntoKKMapper, Map): pass
class FullList(FullLister, list): pass

class OrderMap(OrderMapper, Map): pass
class FillMap(FillMapper, Map): pass
class FullMap(FullMapper, Map): pass
class FillKeyMap(FillKeyMapper, Map): pass
class FullKeyMap(FullKeyMapper, Map): pass
class FillSetMap(FillSetMapper, Map): pass
class FullSetMap(FullSetMapper, Map): pass

class OrderSetMap(SetMapper, OrderMapper, Map): pass
class SetVars(SetMapper, AttrEnum, Map): pass
class FillVars(FillMapper, AttrEnum, Map): pass
class FullVars(FullMapper, AttrEnum, Map): pass
class FillKeyVars(FullKeyMapper, AttrEnum, Map): pass
class FullKeyVars(FullKeyMapper, AttrEnum, Map): pass
class FullSetVars(FullSetMapper, AttrEnum, Map): pass

class ValMapIter(KeyValMapIter, map_keys=False): pass

class VarMap(VarsMapper,Map, name='Vars'): pass

Vars					= VarMap
mapvars					= redef(mapeach,fab=Vars)
tovars					= redef(tomap,fab=Vars)
intovars				= redef(intomap,fab=Vars)

KeyMap					= FullKeyMap
KeyVars					= FullKeyVars
MultiMap = MMap			= ListValsMap				# default multimap type
ValsMap					= ListValsMap
ListMultiMap = LMMap	= ListValsMap
SetMultiMap = HMMap		= SetValsMap
OMap,OHMap				= OrderMap, OrderSetMap
IntoKeys				= IntoKK
DDMapr.Ctls.fab_into	= IntoKK					# alters default output of into_of

ValRemap				= Remap						# todo: obsolete
HitRatio._rep_stats		= Vars(HitRatio._rep_stats)
IKVMap, IVMap			= KeyValMapIter,ValMapIter	# maybe IKVMap should be KIVMap
DDMaps.cache_args		= LRUMapArgs()
#endregion
