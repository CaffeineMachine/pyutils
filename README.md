# Pyutils

A collection of general purpose python tools & utilities born out of personal
necessity.  These utilities have been consolidated here and organized loosely
into the pyutils module hierarchy for ease of access and future expansion.

Part of what makes Python so great is the datamodel and the protocol
I.e the relationships between for example: __enter__,__exit or __iter__,__next__, or
__len__,__iter__,__getitem__.  In the same vein...
This library of modules prioritizes the interface and not optimization or implementation.

This recent update marks the beginning of a transition
- from being a collection of personal projects & source reference
- to supporting a broader community base 

Many core conventions are beginning to materialize and stabilize among the
plethora of draft names/defines/placements. Changes that break or alter 
existing interfaces is a growing concern due to the degree of dependency on 
this library. Although this library must continue to evolve more care will be
taken regarding changes going forward.

## Creators
**Lee Hailey**
- <http://caffeine-machine.com/>

## License
For anyone who does find utility here somewhere feel free to copy/paste, reformat or
whatever you feel is needed to do for your own collection of modules.  Released for
free under the MIT license, so you can use it for most purposes.
