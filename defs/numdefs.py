''' Numeric Defines:
	Defines numeric constants.
	Requires no external dependencies and incurs virtually no overhead.
'''
#region num defs
poles = neut,posv,negv	= 0, 1, -1
zero					= 0
inf,nan					= float('inf'), float('nan')

_n_sizes				= 8, 16, 24, 32, 64
_nx_max 				= ( nx_max8,   nx_max16,   nx_max24,   nx_max32,   nx_max64,   ) = (
		 					uint8_max, uint16_max, uint24_max, uint32_max, uint64_max, ) = (
							tuple(int('1'*size, 2) for size in _n_sizes) )
_nn_max					= ( n8_max8,   n_max16,    n_max24,    n_max32,    n_max64,    ) = (
							int8_max,  int16_max,  int24_max,  int32_max,  int64_max,  ) = (
							tuple(int('1'*(size-1), 2) for size in _n_sizes) )
_nn_min					= ( n_min8,    n_min16,    n_min24,    n_min32,    n_min64,    ) = (
							int8_min,  int16_min,  int24_min,  int32_min,  int64_min,  ) = (
							tuple(~int('1'*(size-1), 2) for size in _n_sizes) )

n_min,n_max,nx_max		= n_min64, n_max64, nx_max64
f_min,f_max				= -inf, inf
infn,infp				= -inf, inf
inf_negv,inf_posv		= -inf, inf

# # todo: eliminate
# max_uint8, max_uint16, max_uint24, max_uint32, max_uint64 = (
# uint8_max, uint16_max, uint24_max, uint32_max, uint64_max, )
# max_int8, max_int16, max_int24, max_int32, max_int64 = (
# int8_max, int16_max, int24_max, int32_max, int64_max, )
# min_int8, min_int16, min_int24, min_int32, min_int64 = (
# int8_min, int16_min, int24_min, int32_min, int64_min, )
#endregion

#region num utils
# above				= lambda low, *vals: max(low, *vals)
# below				= lambda high, *vals: min(high, *vals)
# atleast				= lambda low, *vals: max(low, min(vals))
# atmost				= lambda high, *vals: min(high, max(vals))
# into				= lambda low, high, val: min(max(low, val), high)
# into_z_p			= lambda val: val > 0. and val or 0.
# into_z_p1			= lambda val: (val > 1. and 1.) or (val > 0. and val) or 0.
# into_n1_p1			= lambda val: (val > 1. and 1.) or (val < -1. and -1.) or val
# into_n1_z			= lambda val: (val < -1. and -1.) or (val < 0. and val) or 0.
# into_n_z			= lambda val: val < 0. and val or 0.
# 
# into_norm, into_unit, into_unit_negv = (
# into_n1_p1, into_z_p1, into_n1_z, )
# 
# # todo: eliminate
# lohi, lohi_z_p, lohi_z_p1, lohi_n1_p1, lohi_n1_z, lohi_n_z = (
# into, into_z_p, into_z_p1, into_n1_p1, into_n1_z, into_n_z, )
#endregion
