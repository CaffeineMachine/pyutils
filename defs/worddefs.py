''' Word Defines:
	Defines additional programming keywords.
	Requires no external dependencies and incurs no overhead.
'''
from pyutils.defs.typedefs import *
