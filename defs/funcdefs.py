''' Function Defines:
	Provides fundamental function definitions.
	Requires no external dependencies and incurs no overhead.
'''

#region fundamental utils
noop			= lambda *pa,**ka: None
asis			= lambda arg,*pa,**ka: arg
aspa			= lambda *pa, **ka: pa
aska			= lambda *pa, **ka: ka
relation		= lambda obj, name: (name, getattr(obj, name))
relationarg		= lambda obj_name_arg: (obj_name_arg[0], getattr(*obj_name_arg))
negcall			= lambda call: lambda *pa, **ka: not bool(call(*pa, **ka))	# avoid use in tight loops
star			= lambda call: lambda pa, ka=None: call(*pa, **ka or {})	# avoid use in tight loops
stars			= lambda call: lambda ka, pa=None: call(*pa or (), **ka)	# avoid use in tight loops

def ascall(call,*pa,**ka):
	def on_call(*_,**__):
		return call(*pa,**ka)
	return on_call

def throw(exc=...,exc_msg='',exc_fab=Exception,f_back=1,**ka):
	''' do raise-exception '''
	exc = (
		exc_fab(exc_msg)	if exc is ... else
		exc					if isinstance(exc,Exception) else
		exc(exc_msg)		if type(exc) is type and issubclass(exc,Exception) else
		exc_fab(exc) )
	
	try:
		raise exc
	except Exception as exc:
		# modify traceback to point to throw call
		tb = exc.__traceback__
		f_exc,f_back = isinstance(f_back,int) and (tb.tb_frame,f_back) or (f_back,0)
		for n in range(f_back):
			f_exc = f_exc.f_back
		tb_exc = tb.__class__(tb_next=None,tb_frame=f_exc,tb_lasti=f_exc.f_lasti,tb_lineno=f_exc.f_lineno)
		raise exc.with_traceback(tb_exc)
def throws(msg_fab='{}',exc_fab=Exception):
	''' prepare to do raise-exception '''
	msg_fab = msg_fab if callable(msg_fab) else msg_fab.format
	def call_post(*exc_pa,**exc_ka):
		exc_msg = msg_fab(*exc_pa,**exc_ka)
		throw(exc_msg=exc_msg,exc_fab=exc_fab)
	return call_post
def attempt(on_try, *try_pa, exc_cls=Exception, on_exc=None, on_fin=None, **try_ka):
	''' do *on_try*(*,**) within try-except-finally '''
	try_out,try_exc,fin_out = ...,None,...
	try:
		try_out = on_try(*try_pa,**try_ka)
	except exc_cls as try_exc:
		try_exc = try_exc if not on_exc else on_exc(try_exc)
	finally:
		try:	fin_out = fin_out if not on_fin else on_fin(out=try_out,exc=try_exc)
		except:	pass
	return fin_out
def catch(exc_cls=Exception, on_try=None, *try_pa, **try_ka):
	return attempt(on_try, *try_pa,**try_ka, exc_cls=exc_cls)

ascx			= ascall
unpack			= star
raises			= throw
trycall			= attempt
_local_ops		= 'noop asis aspa aska relation relationarg negcall star stars'.split()
for _fname in _local_ops:
	setattr(globals()[_fname], '__qualname__', _fname)
#endregion
