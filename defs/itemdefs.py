''' Iterable Defines:
	Defines operations related to sequencing and iteration.
	Requires no external dependencies and incurs no overhead.
'''

#region ndx defs
class Ndx:
	(	(first,last), (begin,end), (all,reversed), (key,val,keyval), ) = (
		(l0,l9),      (pfx,sfx),   (_,rev),        (k,v,kv),         ) = (
		
		(0,-1),
		(slice(None,0),slice(1<<63,0)),
		(slice(None,None),slice(None,None,-1)),
		(0,1,slice(0, 2)),
	)
	r9,r0						= l0,l9
	value,keyvalue				= val,keyval
	_kk = _attrs				= ()
	_mbrs_of					= lambda cls: [getattr(cls,k) for k in NumIdx._kk]
	_set_mbrs					= lambda cls,to_mbr: [setattr(cls,k,to_mbr(getattr(cls,k))) for k in NumIdx._kk]
	
	def __class_getitem__(cls,key):		return cls.__dict__.get(key) if isinstance(key,str) else key
	get							= __class_getitem__

NumIdx = DefsIndArg				= Ndx
Ndx._kk = Ndx._attrs			= tuple(k for k in dir(Ndx) if isinstance(getattr(Ndx,k), (int,slice)))
#endregion

#region seq/iter utils
_infuse_kkidx					= dict({None:Ndx.keyval},sbj=Ndx.key,src=Ndx.val,both=Ndx.keyval)
ntall = not_all					= lambda items: not all(items)
ntany = not_any					= lambda items: not any(items)
atcoll							= lambda at,coll: coll[at]
ofat							= lambda at: atcoll.__get__(at)
at0=at1st=atfirst=atkey			= atcoll.__get__(0)  # is distinct from first(); func names imply diff args & behavior
at1=at2nd=atsecond=atval=atvalue= atcoll.__get__(1)
at2=at3rd=atthird				= atcoll.__get__(2)
at9=atlast						= atcoll.__get__(-1)

def first(seq:iter, fill=...):
	''' get first item of a *seq*uence. does advance given iter '''
	fill = fill is not ... and (fill,) or ()
	if hasattr(seq, '__getitem__') and not isinstance(seq,dict):
		out, = seq[:1] or fill
	else:
		out = next(iter(seq), *fill)
	return out
def last(seq:iter, fill=..., collect=False):
	''' get last item of a *seq*uence. can optionally *collect* all vals if given an iter. '''
	fill = fill is not ... and (fill,) or ()
	if hasattr(seq, '__getitem__'):
		out, = seq[-1:] or fill
	elif hasattr(seq, '__reversed__'):
		out = next(reversed(seq), *fill)
	elif collect:
		out, = tuple(seq)[-1:] or fill  # todo: collect all without keeping in memory
	else:
		raise TypeError(f'given cannot access last on given seq. {type(seq)}')
	return out
def ilast(seq, *fill):
	''' iterate to and return last item.  note: will collect all from an iter. '''
	# *front, out = *fill, *seq
	out, = tuple(seq)[-1:] or (fill,)  # todo: collect all without keeping in memory
	return out
def part(seq, at=1):
	''' get left iterable and right iterable parts of *seq*uence with split position *at* '''
	if hasattr(seq, '__getitem__'):
		parts = seq[:at], seq[at:]
	else:
		parts = lefts, rights = [], iter(seq)
		lefts.extend((right_n for ind, right_n in zip(range(at), rights)))
	return parts
def firstrest(seq, *default):
	''' get first item and rest iterable parts of *seq*uence '''
	at = 1
	if hasattr(seq, '__getitem__'):
		lefts, rights = seq[:at], seq[at:]
		left, = lefts or default
	else:
		rights = iter(seq)
		left = next(rights, *default)
	return left, rights

def infuse(sbj, src=None, attrs=(), to_attr=None, fill=..., slotted=False, at='sbj', **_src):
	''' infuse *sbj* with vars at *attrs* from *src* prioritizing kwargs
		:param sbj:     subject into vars will be applied
		:param src:     source of vars
		:param attrs:   subset of vars to get from *src*
		:param fill:    insert this value for any *attrs* missing from *src* instead of throwing error
		:param slotted: indicates vars should be applied via setattr
		:param at:      select output from (sbj,src) tuple; also allows 'sbj' or 'src'
		:param to_attr: ...
	'''
	# resolve inputs
	at = at if not isinstance(at,str) else at=='src'
	attrs = attrs.split() if isinstance(attrs, str) else attrs
	slotted |= isinstance(sbj, type) or not hasattr(sbj,'__dict__')		# detect slotted sbj
	sbj_dict = None if slotted else sbj if isinstance(sbj, dict) else sbj.__dict__
	getr,fill = fill is ... and ('__getitem__', ()) or ('get', [fill])
	src,attrs,getr = (
		(None,(),None)									if src is None else
		(src,attrs or src,getattr(src.__class__,getr))	if isinstance(src, dict) else
		(src,attrs,getattr)  )
	
	# resolve members:  => src_vars
	src_vars = dict(((attr,getr(src,attr,*fill)) for attr in attrs if attr not in _src), **_src)
	isrc_vars = ((attr,mbr) for attr,mbr in src_vars.items() if mbr != 'nothing' and mbr is not ...)
	if sbj_dict is None:
		[setattr(sbj,attr,mbr) for attr,mbr in isrc_vars]				# assign to slotted src
	else:
		sbj_dict.update(isrc_vars)										# assign to unslotted src
	
	# resolve output
	out = (sbj,src_vars)[_infuse_kkidx.get(at,at)]
	return out
def effuse(src, sbj, *pa, **_src):
	''' effuse vars at *attrs* from *src* into *sbj* prioritizing kwargs
		:param src:     source of vars
		:param sbj:     subject into vars will be applied
		:param attrs:   subset of vars to get from *src*
		:param fill:    insert this value for any *attrs* missing from *src* instead of throwing error
		:param slotted: indicates vars should be applied via setattr
		:param at:      select output from (sbj,src) tuple; also allows 'sbj' or 'src'
		:param to_attr: ...
	'''
	return infuse(sbj, src, *pa, **_src)

colllast,partfirst				= ilast,firstrest
fuse							= infuse
#endregion

#region hash utils
def canhash(obj):
	result = bool(getattr(obj, '__hash__', None))
	if result:
		try:
			hash(obj)
		except TypeError:
			result = False
	return result
def hashable(obj, default=None):
	result = obj if canhash(obj) else default
	return result
#endregion
