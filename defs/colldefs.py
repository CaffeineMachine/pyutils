''' Collection Defines:
	Provides defines pertaining to primative collections.
	Requires no external dependencies and incurs virtually no overhead.
'''

#region coll defs
# draft: perhaps migrate to coll for collect and colx for collection
# draft: t|tuple conflicts with time
# draft: perhaps migrate d|dict to m|map|dict
_miss								= object()
class DefsColl:
	#define colls
	idhlt=_,*dhlt=_,_,*hlt			=  iter, dict, set, list, tuple
	ch_idhlt=_,*ch_dhlt=_,_,*ch_hlt	= 'i'   'd'   'h'  'l'   't'
	
	excl							= str, bytes
	incl,ch_incl					= dhlt, ch_dhlt
	ix_iter							= '__iter__',
	ix_coll							= '__len__', '__getitem__'
	#endregion
	...
DDColl = _D							= DefsColl
#endregion

#region coll utils
fabeach = fabi						= map
empty = isempty						= lambda seq: len(seq) == 0
nonempty							= lambda seq: len(seq) != 0  # [nonempty([0]), any([0])] => [True, False]

def has(coll,item=_miss):		return item in coll if item is not _miss else has.__get__(coll)
def isin(item,coll=_miss):		return item in coll if coll is not _miss else is_in.__get__(item)

def redef_per_coll(func, colls=_D.incl, arg=None, **kdef):
	# todo: look into copying the code block and modifying the parameters such that there is no addt. frame inserted
	wraps = []
	for coll in colls:
		if arg is None:
			wrap = lambda *pa, _func=func, _coll=coll, _kd=kdef, **ka: _coll(_func(*pa, **_kd, **ka))
		elif isinstance(arg, str):
			wrap = lambda *pa, _func=func, _coll=coll, _key=arg, _kd=kdef, **ka: _func(*pa, **{arg:_coll}, **_kd, **ka)
		elif isinstance(arg, int):
			wrap = lambda *pa, _func=func, _coll=coll, _pos=arg, _kd=kdef, **ka: _func(*pa[:_pos], _coll, *pa[_pos:], **_kd, **ka)
		else:
			raise ValueError()
		wraps.append(wrap)
	return wraps

def def_coll_wraps(func, fmt:str=None, colls=_D.incl, arg=None, labels=_D.ch_incl, safe=True, **kdef):
	names = list(map((fmt or func.__name__).format, labels))
	assert safe and set(names).isdisjoint(globals()), f'formatted name in {names} collides with namespace'
	wraps = redef_per_coll(func, colls, arg, **kdef)
	named_wraps = tuple(zip(names, wraps))
	
	globals().update(dict(named_wraps))
	return named_wraps

def ismap(coll):
	result = hasattr(coll, '__getitem__') and callable(getattr(coll, 'keys', None)) and type(coll) != type
	return result
def isseq(coll):
	result = hasattr(coll, '__getitem__') and not callable(getattr(coll, 'keys', None)) and type(coll) != type
	return result
def keys_of_coll(coll, fab=None):
	# todo: untested with numpy/pandas structures which depart from python protocol conventions
	coll_keys = getattr(coll, 'keys', None)
	coll_keys = coll_keys if callable(coll_keys) else range(len(coll))
	coll_keys = coll_keys if not fab else fab(coll_keys)
	return coll_keys
def vals_of_coll(coll, fab=None):
	# todo: untested with numpy/pandas structures which depart from python protocol conventions
	coll_vals = getattr(coll, 'values', None)
	coll_vals = coll_vals if callable(coll_vals) else coll
	coll_vals = coll_vals if not fab else fab(coll_vals)
	return coll_vals

def iscoll(obj, ntcoll_types=_D.excl,**ka):
	result = not isinstance(obj, ntcoll_types) and all(hasattr(obj,name) for name in _D.ix_coll)
	return result
def iscolliter(obj, ntcoll_types=_D.excl,**ka):
	''' test if obj is a collection (a non-str sequence type) '''
	result = not isinstance(obj, ntcoll_types) and all(hasattr(obj,name) for name in _D.ix_iter)
	return result
def collany(obj, coll=tuple, empties=(), empty_types=(), *pa, **ka)->iter:
	''' Always return a collection by 1st coercing *obj* to collection items and 2nd casting the items if needed
		1. check that *obj* qualifies as collection or collection-iterable. otherwise coerce it as *coll_items*=[*obj*]
		2. check that *coll_items* qualifies as collection cast type. otherwise cast it as *fab*(*coll_items*)
		where collection is a non-str/bytes sequence type (ie: list, tuple, dict, set, ...).
	'''
	# todo: switch to single user defined is_empty arg
	obj_out = (
		coll()			if obj in empties or isinstance(obj,empty_types) else	# obj is empty
		obj				if iscoll(obj, *pa, **ka) else							# obj is coll
		coll(obj)		if iscolliter(obj, *pa, **ka) else						# obj is coll iter
		coll((obj,)) )															# obj is single
	return obj_out
def collfromany(obj,*pa,**ka)->(iter,bool):
	''' of ->(collany(),iscoll(),iscolliter()) of given *obj* '''
	from_coll = iscoll(obj,*pa,**ka)
	from_colli = from_coll or iscolliter(obj,*pa,**ka)
	coll = obj if from_coll else collany(obj,*pa,**ka)
	return coll,from_coll,from_colli

def of_callp(callp, pa=None, **ka):
	def post_call(pa, **ka):
		return callp(*pa,**ka)
	result = post_call if pa is None else callp(*pa, **ka)
	return result
def of_callap(callap):
	def post_call(*pa, **ka):
		return callap(pa,**ka)
	result = post_call
	return result

is_in								= isin
iat_of_coll							= keys_of_coll
iscolli								= iscolliter
tcollany, hcollany, lcollany, _		= collany, *redef_per_coll(collany, colls=_D.hlt, arg=1)
hyieldcoll,lyieldcoll,tyieldcoll	= redef_per_coll(collany, colls=_D.hlt, arg=1, allow_iter=True)
#endregion

#region coll devcs
# todo: coll & table vals & vars should not be distict by default
# 		vals per index|_kk & vars per attr are separate
# 		vals go into tuple which are accessed by index or corresponding key in _kk

def _init_subclass(cls, kk=None, n_eq=None, k_eq=None, name=None, **ka):
	''' subclass given *pa* attr list will assign properties getters tuple positions '''
	kk = cls._kk = kk or ka.get('pa',())
	cls.__name__ = cls.__name__ if not name else name
	cls._n_eq = (
		None			if not n_eq and not k_eq else
		n_eq			if n_eq else
		set(n for n,k in enumerate(cls._kk) if k_eq(k)).__contains__ )
	if kk and hasattr(cls,'__getitem__'):
		cls._kk = tuple(type(kk) is str and filter(bool,kk.split()) or kk)
		for idx,attr in enumerate(cls._kk):
			setattr(cls, attr, property(lambda self,idx=idx: self[idx], lambda self,v,idx=idx: self.__setitem__(idx,v)))

class xvalsbase:
	''' class with ordered attrs predefined during class definition via __init_subclass__(kk=...) '''
	kkv_vals						= property(lambda self: dict(zip(self._kk,self)))				# fixme: this must be custom to each data structure
	kkv_all							= property(lambda self: dict(zip(self._kk,self))|self.__dict__)	# fixme: this must be custom to each data structure
	vv_eq							= property(lambda self: tuple(v for n,v in enumerate(self) if self._n_eq(n)))
	_kk,_n_eq						= (), None
	__init_subclass__				= classmethod(_init_subclass)
	def __post_ctor__(self,**ka):		pass
	def __getitem__(self, key):			return getattr(self,key) if isinstance(key,str) else super().__getitem__(key)
	def __repr__(self):
		out = ' '.join(map(str,self[:]))
		return out
	def __hash__(self):
		out = hash(self[:] if not self._n_eq else tuple(v for n,v in enumerate(self) if self._n_eq(n)))
		return out
	def __eq__(self,vals):
		if isinstance(self,set):
			out = set.__eq__(self, vals)
		else:
			out = self[:]==vals if not self._n_eq else all(v==v_in for n,v,v_in in zip(range(1<<63),self,vals) if self._n_eq(n))
		return out
class xcoll(xvalsbase):
	''' mutable coll given *vals* from first arg '''
	def __init__(self, vals=(),kvals=None, _ka=None, vars=None,**_vars):
		kvals = kvals or {}
		vals = list(vals or (kvals.pop(k,None) for k in self._kk))
		vals += [None] * (len(self._kk)-len(vals))
		
		super().__init__(vals, **kvals or {})
		(vars:=vars or _vars) and [setattr(self,*kv) for kv in vars.items()]
		self.__post_ctor__(**_ka or map0)
		...
class xapcoll(xvalsbase):
	''' mutable coll given *vals* from args '''
	def __init__(self, *_vals,vals=None,kvals=None, _ka=None, vars=None,**_vars):
		kvals = kvals or {}
		vals = list(_vals or vals or (kvals.pop(k,None) for k in self._kk))
		vals += [None] * (len(self._kk)-len(vals))
		
		super().__init__(vals, **kvals or {})
		(vars:=vars or _vars) and [setattr(self,*kv) for kv in vars.items()]
		self.__post_ctor__(**_ka or map0)
		...
class xtable(xvalsbase):
	''' immutable coll given *vals* from first arg '''
	# todo: currently inconsistent with other xcoll classes
	def __new__(cls, vals=(), vars=None, _ka=None, kvals=None, **_kvals):
		kvals = kvals or _kvals
		vals = vals or [kvals.pop(k,None) for k in cls._kk]
		
		obj = super().__new__(cls, vals, **kvals or _kvals)
		vars and [setattr(obj,*kv) for kv in vars.items()]
		obj.__post_ctor__(**_ka or map0)
		return obj
class xaptable(xvalsbase):
	''' immutable coll given *vals* from args '''
	# todo: currently inconsistent with other xcoll classes
	def __new__(cls, *_vals, vals=None, vars=None, _ka=None, kvals=None, **_kvals):
		kvals = kvals or _kvals
		vals = vals or _vals or [kvals.pop(k,None) for k in cls._kk]
		
		obj = super().__new__(cls, vals, **kvals or _kvals)
		vars and [setattr(obj,*kv) for kv in vars.items()]
		obj.__post_ctor__(**_ka or map0)
		return obj

class xdict(xcoll, dict):
	def __new__(cls,*pa,_ka=None,**ka):
		obj = super().__new__(cls,*pa,**ka)
		obj.__post_ctor__(**_ka or map0)
		return obj
class xlist(xcoll, list): pass
class xset(xcoll, set): pass					# fixme: not indexable
class xfrozenset(xtable, frozenset): pass		# fixme: not indexable
class xtuple(xtable, tuple): pass

class xapdict(xapcoll, dict): pass
class xaplist(xapcoll, list): pass
class xapset(xapcoll, set): pass				# fixme: not indexable
class xapfrozenset(xaptable, frozenset): pass	# fixme: not indexable
class xaptuple(xaptable, tuple): pass

class FinalMapped:
	__dir__							= dict.__dir__
	def __init_subclass__(cls, **ka):
		cls.clear=cls.update=cls.setdefault=cls.__setitem__= cls._unmodifiable
	def __new__(cls, *pa, **ka):
		obj = dict.__new__(cls,*pa, **ka)
		return obj
	def _unmodifiable(self,*pa,**ka):
		raise TypeError('modification not allowed on {}'.format(self.__class__.__name__))
	def pop(self,key,fill=...):
		fill == ... and self._unmodifiable()
		return super().pop(key,fill)
class FinalMap(FinalMapped,dict): pass
Table								= FinalMap
def FinalMap(*pa,**ka)->FinalMapped:	return Table(*pa,**ka)				# obfuscate dict from intellisense

# class Priors(tuple):
# 	def __new__(cls,*pa,**ka):
# 		return tuple.__new__((pa,ka))
# AA0										= Priors

xd,xh,xl,xt,xfh,xfd					= xdict,xset,xlist,xtuple,xfrozenset,FinalMap
*colls_dhlt_x,_,_					= xd,xh,xl,xt
apdict,apset,aplist,aptuple			= map(of_callap, _D.dhlt)		# canvary: ap may become sfx
apd,aph,apl,apt						= apdict,apset,aplist,aptuple		# canvary: ap may become sfx 
xapd,xaph,xapl,xapt					= xapdict,xapset,xaplist,xaptuple	# canvary: ap may become sfx
str0,tuple0,set0,map0 = emptystr,*_	= '',tuple(),frozenset(),FinalMap()	# mutable defines
#endregion

#region notes
''' Naming Convention:
k|key:  an element of an iter; usually unique;  typically used in a set or dict
v|val|value:  an element of an iter; usually not unique

i|iter|iterable:  implements __iter__ or __next__
d|dict:  the dict builtin type
h|hashed|set:  the set builtin type
l|list:  the list builtin type
t|tuple:  the tuple builtin type
s|str|string:  the str builtin type

coll|collection:  implements __len__, __iter__ and typically  __getitem__ and __setitem__.
table:  a *coll* that is immutable and therefore must implement __new__ instead of __init__

seq|sequence|vv:  a pos. indexed collection
map|mapped|kkv:  a key indexed collection; the most general interpretation of a map; distinct from builtin map function
mmap|multimap|kkvv:  a key indexed collection of collections

getr|getter: callable which gets the val at key  
picker: callable which gets the val at key for a given coll arg			# DRAFT:  
at: callable which gets the val at key for a given coll arg				# DRAFT:  
'''
#endregion
