''' Type Defines:
	Provides type related defines.
	Requires no external dependencies and incurs no overhead.
'''

#region var defs
def _gen(): yield

binarys					= False,True
off_on = rising			= binarys
on_off = falling		= binarys[::-1]
off,on = no,yes			= binarys
Off,On = No,Yes			= binarys
norms					= frozenset((None,True))
IsNone=nonetype			= type(None)
IsChrs					= str|bytes
IsNum					= int|float|complex
IsIdx					= int|slice
IsStrs = Strs=SS		= list[str]
IsInts = Ints			= list[int]
IsFlts = Flts=FF		= list[float]
IsCplxs = Cplxs			= list[complex]
IsFunc = Fc=IsCall=IsCx	= type(_gen)
IsGen					= type(_gen())
IsTypes = Types			= tuple|type|type(IsChrs)
#endregion

#region type utils
truth = binary			= bool
ixcall = ixcx			= callable
in_norm					= norms.__contains__
oftype 					= isinstance
subtype = iscls			= issubclass

def noneof(*_pa,**_ka)->None:	return None
def always(*_pa,**_ka)->True:	return True
def never(*_pa,**_ka)->False:	return False
def untruth(obj)->bool:			return not obj

def isnttype(obj):				return not oftype(obj, type)
def isntstr(obj):				return not oftype(obj, str)
def isntbytes(obj):				return not oftype(obj, bytes)
def isntchrs(obj):				return not oftype(obj, IsChrs)
def isntbool(obj):				return not oftype(obj, bool)
def isntint(obj):				return not oftype(obj, int)
def isntfloat(obj):				return not oftype(obj, float)
def isntcomplex(obj):			return not oftype(obj, complex)
def isntnum(obj):				return not oftype(obj, IsNum)
def isntindex(obj):				return not oftype(obj, IsIdx)
def isntnan(obj):				return not oftype(obj, float) or obj == obj
def isntdict(obj):				return not oftype(obj, dict)
def isntlist(obj):				return not oftype(obj, list)
def isnttuple(obj):				return not oftype(obj, tuple)
def isntset(obj):				return not oftype(obj, set)
def isntfunc(obj):				return not oftype(obj, IsFunc)
def isntgen(obj):				return not oftype(obj, IsGen)
def isnttrue(obj):				return not obj is True
def isntfalse(obj):				return not obj is False
def isntnone(obj):				return not obj is None
def isntnorm(obj):				return not obj in norms
def isntcall(obj):				return not callable(obj)
def isntiter(obj):				return not (hasattr(obj, '__iter__')    and not oftype(obj,type))
def isntseq(obj):				return not (hasattr(obj, '__getitem__') and not oftype(obj,type))

def isthen(truth,obj,orput=...,*,put=...,call=...,ev=...,fab=...,at=...,var=..., orcall=...,orev=...,orfab=...,orat=...,orvar=...,pa=(),**ka):
	''' produce output from binary pair of converters for if & else
		:param truth:  selects corresponding converter from if/else -> asis-arg/or-arg group
		:param obj:    input to converters
		:param put:    when truth is True  returns obj;  the default output when missing asis-args
		:param call:   when truth is True  returns call(*pa,**ka)
		:param ev:     when truth is True  returns ev(obj,*pa,**ka)
		:param at:     when truth is True  returns obj[at]
		:param var:    when truth is True  returns obj.at
		:param orput:  when truth is False returns obj;  the default output when missing or-args
		:param orcall: when truth is False returns orcall(*pa,**ka)
		:param orev:   when truth is False returns orev(obj,*pa,**ka)
		:param orat:   when truth is False returns obj[orat]
		:param orvar:  when truth is False returns obj.orat
	'''
	# todo: switch to using non-strict getters ((get(obj,at,non)|getattr(var,non)) and raise if output is non
	if truth:
		out = (
			put 					if ... is not put else
			call(*pa,**ka)			if ... is not call else
			ev(obj,*pa,**ka)		if ... is not ev else
			fab(obj,*pa,**ka)		if ... is not fab else
			obj[at]					if ... is not at else
			getattr(obj,var,*pa)	if ... is not var else
			obj  )
	else:
		out = (
			orput					if ... is not orput else
			orcall(*pa,**ka)		if ... is not orcall else
			orev(obj,*pa,**ka)		if ... is not orev else
			orfab(obj,*pa,**ka)		if ... is not orfab else
			obj[orat]				if ... is not orat else
			getattr(obj,orvar,*pa)	if ... is not orvar else
			obj  )
	return out

def istypeas(obj,*pa,**ka):		return isthen(oftype(obj, type), obj, *pa, **ka)
def isstras(obj,*pa,**ka):		return isthen(oftype(obj, str), obj, *pa, **ka)
def isbytesas(obj,*pa,**ka):	return isthen(oftype(obj, bytes), obj, *pa, **ka)
def ischrsas(obj,*pa,**ka):		return isthen(oftype(obj, IsChrs), obj, *pa, **ka)
def isboolas(obj,*pa,**ka):		return isthen(oftype(obj, bool), obj, *pa, **ka)
def isintas(obj,*pa,**ka):		return isthen(oftype(obj, int), obj, *pa, **ka)
def isfloatas(obj,*pa,**ka):	return isthen(oftype(obj, float), obj, *pa, **ka)
def iscomplexas(obj,*pa,**ka):	return isthen(oftype(obj, complex), obj, *pa, **ka)
def isnumas(obj,*pa,**ka):		return isthen(oftype(obj, IsNum), obj, *pa, **ka)
def isindexas(obj,*pa,**ka):	return isthen(oftype(obj, IsIdx), obj, *pa, **ka)
def isdictas(obj,*pa,**ka):		return isthen(oftype(obj, dict), obj, *pa, **ka)
def islistas(obj,*pa,**ka):		return isthen(oftype(obj, list), obj, *pa, **ka)
def istupleas(obj,*pa,**ka):	return isthen(oftype(obj, tuple), obj, *pa, **ka)
def issetas(obj,*pa,**ka):		return isthen(oftype(obj, set), obj, *pa, **ka)
def isfuncas(obj,*pa,**ka):		return isthen(oftype(obj, IsFunc), obj, *pa, **ka)
def isgenas(obj,*pa,**ka):		return isthen(oftype(obj, IsGen), obj, *pa, **ka)
def istrueas(obj,*pa,**ka):		return isthen(obj is True, obj, *pa, **ka)
def isfalseas(obj,*pa,**ka):	return isthen(obj is False, obj, *pa, **ka)
def isnoneas(obj,*pa,**ka):		return isthen(obj is None, obj, *pa, **ka)
def isnormas(obj,*pa,**ka):		return isthen(obj in norms, obj, *pa, **ka)
def iscallas(obj,*pa,**ka):		return isthen(hasattr(obj, '__call__'), obj, *pa, **ka)
def isiteras(obj,*pa,**ka):		return isthen(hasattr(obj, '__iter__'), obj, *pa, **ka)
def isseqas(obj,*pa,**ka):		return isthen(hasattr(obj, '__getitem__'), obj, *pa, **ka)

def is_type(obj,**ka):	return oftype(obj,type)				if not ka else isthen(**ka, obj=obj, truth=oftype(obj,type))
def isstr(obj,**ka):	return oftype(obj,str)				if not ka else isthen(**ka, obj=obj, truth=oftype(obj,str))
def isstr(obj,**ka):	return isthen(**ka,obj=obj,truth=b)	if (b:=oftype(obj,str))&0 or ka else b 
def isbytes(obj,**ka):	return oftype(obj,bytes)			if not ka else isthen(**ka, obj=obj, truth=oftype(obj,bytes))
def ischrs(obj,**ka):	return oftype(obj,IsChrs)			if not ka else isthen(**ka, obj=obj, truth=oftype(obj,IsChrs))
def isbool(obj,**ka):	return oftype(obj,bool)				if not ka else isthen(**ka, obj=obj, truth=oftype(obj,bool))
def isint(obj,**ka):	return oftype(obj,int)				if not ka else isthen(**ka, obj=obj, truth=oftype(obj,int))
def isfloat(obj,**ka):	return oftype(obj,float)			if not ka else isthen(**ka, obj=obj, truth=oftype(obj,float))
def iscomplex(obj,**ka):return oftype(obj,complex)			if not ka else isthen(**ka, obj=obj, truth=oftype(obj,complex))
def isnum(obj,**ka):	return oftype(obj,IsNum)			if not ka else isthen(**ka, obj=obj, truth=oftype(obj,IsNum))
def isindex(obj,**ka):	return oftype(obj,IsIdx)			if not ka else isthen(**ka, obj=obj, truth=oftype(obj,IsIdx))
def isdict(obj,**ka):	return oftype(obj,dict)				if not ka else isthen(**ka, obj=obj, truth=oftype(obj,dict))
def islist(obj,**ka):	return oftype(obj,list)				if not ka else isthen(**ka, obj=obj, truth=oftype(obj,list))
def istuple(obj,**ka):	return oftype(obj,tuple)			if not ka else isthen(**ka, obj=obj, truth=oftype(obj,tuple))
def isset(obj,**ka):	return oftype(obj,set)				if not ka else isthen(**ka, obj=obj, truth=oftype(obj,set))
def isfunc(obj,**ka):	return oftype(obj,IsFunc)			if not ka else isthen(**ka, obj=obj, truth=oftype(obj,IsFunc))
def isgen(obj,**ka):	return oftype(obj,IsGen)			if not ka else isthen(**ka, obj=obj, truth=oftype(obj,IsGen))
def istruth(obj,**ka):	return not not obj					if not ka else isthen(**ka, obj=obj, truth=not not obj)
def isuntruth(obj,**ka):return not obj						if not ka else isthen(**ka, obj=obj, truth=not obj)
def istrue(obj,**ka):	return obj is True					if not ka else isthen(**ka, obj=obj, truth=obj is True)
def isfalse(obj,**ka):	return obj is False					if not ka else isthen(**ka, obj=obj, truth=obj is False)
def isnone(obj,**ka):	return obj is None					if not ka else isthen(**ka, obj=obj, truth=obj is None)
def isnorm(obj,**ka):	return obj in norms					if not ka else isthen(**ka, obj=obj, truth=obj in norms)
def iscall(obj,**ka):	return hasattr(obj,'__call__')		if not ka else isthen(**ka, obj=obj, truth=hasattr(obj,'__call__'))
def isiter(obj,**ka):	return hasattr(obj,'__iter__')		if not ka else isthen(**ka, obj=obj, truth=hasattr(obj,'__iter__'))
def isseq(obj,**ka):	return hasattr(obj,'__getitem__')	if not ka else isthen(**ka, obj=obj, truth=hasattr(obj,'__getitem__'))
def iscall(obj,**ka):	return isthen(**ka,obj=obj,truth=b) if (b:=hasattr(obj,'__call__'))&0 or ka else b
def isiter(obj,**ka):	return isthen(**ka,obj=obj,truth=b) if (b:=hasattr(obj,'__iter__'))&0 or ka else b
def isseq(obj,**ka):	return isthen(**ka,obj=obj,truth=b) if (b:=hasattr(obj,'__getitem__'))&0 or ka else b

def iscallaa(call,*pa,arg=...,orat=...,**ka):
	if arg is not ...:
		pa = arg,*pa
		orat = 0
	if orat is ...:
		orat = not pa and ka and '**' or '*'
	result = (
		call(*pa,**ka)		if iscall(call) else
		pa[orat]			if type(orat) is not str else
		pa					if orat == '*' else
		ka					if orat == '**' else
		(pa,ka)				if orat == '***' else
		ka[orat] )
	return result

truth_at				= {True:truth, False:untruth}
isoff,ison = isno,isyes = isfalse, istrue
okays, isokay			= norms, isnorm
isnt=neg				= untruth
iterable				= isiter
isflt,isntflt,isfltas	= isfloat, isntfloat, isfloatas
iscplx,isntcplx,iscplxas= iscomplex, isntcomplex, iscomplexas
isidx,isntidx,isidxas	= isindex, isntindex, isindexas
iscx,isntcx,iscxas		= iscall, isntcall, iscallas
iscallargs				= iscallaa
# todo: considering making ok-ay and deny|veto|fault|screen opposing terms and verbs; must transition {None,True} okay:
#  not okay <- warn error reject fault deny veto screen qualify flag
#  okay -> good fine alright suitable acceptable
#endregion

#region type utils
_miss					= object()

def isclsof(cls:Types=type,subcls:type=_miss,*pa,**ka):	# todo: use this func as protofunc
	if subcls is _miss:	return isclsof.__get__(cls)
	return iscls(subcls,cls,*pa,**ka)
def istypeof(cls:Types=type,obj=_miss,*pa,**ka):	# todo: use this func as protofunc
	if obj is _miss:	return istypeof.__get__(cls)
	return istype(obj,cls,*pa,**ka)
def istypethen(cls:Types=type,obj=_miss,**ka):
	if obj is _miss:	return istypethen.__get__(cls)
	return isinstance(obj, cls) if not ka else isthen(isinstance(obj, cls),obj,**ka)
def ifthen(cond,obj=_miss,**ka):
	if obj is _miss:	return ifthen.__get__(cond)
	return cond(obj) if not ka else isthen(cond(obj),obj,**ka)

def clsof(obj)->type:
	''' coerce *obj* to a class; if is an instance return type(*obj*) else return *obj* unchanged '''
	obj_cls = type(obj)
	cls = obj_cls is type and obj or obj_cls
	return cls

def eqcls(subcls:type=_miss, cls:Types=type, *, cmp_names:bool=True)->bool:
	''' test if *subcls* matches any in *cls* type tree.  iscls >= builtin.issubclass
		:param cmp_names: When True compare classes using qualified names. Useful when expecting to reimport.
	'''
	if subcls is _miss:	return isclsof.__get__(cls)
	if not hasattr(subcls, '__mro__'): return False
	cls = (hasattr(cls, '__instancecheck__') and {cls}) or {*cls}
	subcls_components = {subcls, *subcls.__mro__}
	if cmp_names:
		cls, subcls_components = set(map(qualname, cls)), set(map(qualname, subcls_components))
	out = bool(subcls_components & cls)  # fixme
	return out
def eqtype(obj, cls:Types=type)->bool:
	''' compare *obj* type name with names in *cls* type tree.  eqtype ~ builtin.isinstance '''
	return eqcls(obj.__class__, cls, cmp_names=True)

def istype(obj=_miss, cls:Types=type, *, cmp_names:bool=False)->bool:
	''' test if *obj* type matches any in *classes* type tree.  istype >= builtin.isinstance.
		:param cmp_names: When True compare classes using qualified names. Useful when expecting to reimport.
	'''
	if obj is _miss:	return istypeof.__get__(cls)
	out = (
		isinstance(obj, type)		if cls is type else
		False						if not cls or cls is _miss else
		iscls(obj.__class__, cls)	if not cmp_names else
		eqcls(obj.__class__, cls) )
	return out

def isntcls(*pa, **ka)->bool:		return not iscls(*pa, **ka)

def isnttype(*pa, **ka)->bool:		return not istype(*pa, **ka)

def istypeerr(obj, cls:Types=type, **ka):
	if istype(obj, cls, **ka):
		raise TypeError(f'Expected type NOT from: {{{", ".join(map(nameof, cls))}}} yet got obj: {obj!r}.')
	return True

def isnttypeerr(obj, cls:Types=type, **ka):
	if not istype(obj, cls, **ka):
		raise TypeError(f'Expected type from: {{{", ".join(map(nameof, cls))}}} yet got obj: {obj!r}.')
	return True

def qualname(cls:type)->str:
	module = cls.__module__
	if module is None or module == str.__class__.__module__:
		return cls.__name__
	return module + '.' + cls.__name__

def objqualname(obj)->str:
	module = obj.__class__.__module__
	if module is None or module == str.__class__.__module__:
		return obj.__class__.__name__
	return module + '.' + obj.__class__.__name__

def nameof(obj)->str:
	out = (
		obj								if isinstance(obj,str) else
		obj['__name__']					if isinstance(obj,dict) and '__name__' in obj else
		obj.f_globals['__name__']		if hasattr(obj,'f_globals') else
		getattr(obj, '__name__', obj.__class__.__name__) )
	return out
def qualof(obj)->str:
	out = (
		obj								if isinstance(obj,str) else
		obj['__qualname__']				if isinstance(obj,dict) and '__qualname__' in obj else
		getattr(obj, '__qualname__', obj.__class__.__qualname__) )
	return out

def ibasecls(cls,limit=50,prev=None):
	# todo: how again is this distinct from __mro__?
	cls = cls_in = clsof(cls)
	prev = prev or set()
	while cls:
		if cls not in prev:
			yield cls
			prev.add(cls)
		*icls_left,cls = cls.__bases__ and cls.__bases__ or (None,)
		for cls_left in icls_left:
			yield from ibasecls(cls_left,limit,prev)
		assert 0 < limit, f'exceeded base class limit for {cls_in}'
		limit -= 1

likecls,liketype		= eqcls, eqtype
typeof					= istype  # todo: remove formerly named
#endregion

#region term devcs
class NamedConst(str): pass

class NamedNil:
	def __init__(self, name):	self.name = name
	def __str__(self):			return self.name
	def __repr__(self):			return '<%s>' % self.name
	def __eq__(self, other):	return      isinstance(other,(str,NamedNil)) and self.name == str(other)
	def __ne__(self, other):	return not (isinstance(other,(str,NamedNil)) and self.name == str(other))
	def __hash__(self):			return hash(self.__class__)
	def __bool__(self):			return False

class Anything:
	def __hash__(self):			return hash(self.__class__)
	def __repr__(self):			return 'anything'
	def __eq__(self, other):	return True

class Fault:
	''' Invalidates anything it touches '''
	#region utils
	_inst			= None
	def __new__(cls):
		obj = super().__new__(cls) if cls._inst is None else cls._inst
		return obj
	def __bool__(self):
		return False
	def __repr__(self): return 'invalid'
	def invalidate(self, *pa, **ka):
		return self
	
	(	__call__, __getattr__, __getitem__, __iter__,     __hash__,
		__eq__,   __ne__,      __gt__,      __lt__,       __and__,  __or__,
		__add__,  __sub__,     __mul__,     __truediv__,
		__radd__, __rsub__,    __rmul__,    __rtruediv__, *_
	) = 30*(invalidate,)
	# todo: handle all possible interaction
	#endregion
	...

null					= NamedNil('null')
nothing					= NamedNil('nothing')
miss					= NamedNil('miss')
missing 				= NamedNil('missing')
anything				= Anything()
fault = Fault._inst 	= Fault()

isnull,isntnull			= ifthen(   null.__eq__),    null.__ne__
isnothing,isntnothing	= ifthen(nothing.__eq__), nothing.__ne__
ismiss,isntmiss			= ifthen(   miss.__eq__),    miss.__ne__
ismissing,isntmissing	= ifthen(missing.__eq__), missing.__ne__
#endregion
