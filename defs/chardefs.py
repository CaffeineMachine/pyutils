# coding=utf-8
''' Character Set Defines:
	Provides multiple character sets and sequences.
	Requires no external dependencies and incurs no overhead.
'''

#region charset defs
s_lower						= 'abcdefghijklmnopqrstuvwxyz'
s_upper						= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
s_sup_lower					= 'ᵃᵇᶜᵈᵉᶠᵍʰⁱʲᵏˡᵐⁿᵒᵖ ʳˢᵗᵘᵛʷˣʸᶻ'
s_sup_upper					= 'ᴬᴮ ᴰᴱ ᴳᴴᴵᴶᴷᴸᴹᴺᴼᴾ ᴿ ᵀᵁⱽᵂ   '
s_num_std					= '0123456789'
s_num_sub					= '₀₁₂₃₄₅₆₇₈₉'
s_num_sup					= '⁰¹²³⁴⁵⁶⁷⁸⁹'
s_num_bold					= '𝟎𝟏𝟐𝟑𝟒𝟓𝟔𝟕𝟖𝟗'
s_num_double				= '𝟘𝟙𝟚𝟛𝟜𝟝𝟞𝟟𝟠𝟡'
s_num_sans					= '𝟢𝟣𝟤𝟥𝟦𝟧𝟨𝟩𝟪𝟫'
s_num_sansbold				= '𝟬𝟭𝟮𝟯𝟰𝟱𝟲𝟳𝟴𝟵'
s_num_mono					= '𝟶𝟷𝟸𝟹𝟺𝟻𝟼𝟽𝟾𝟿'
s_num_dot					= '🄀⒈⒉⒊⒋⒌⒍⒎⒏⒐⒑⒒⒓⒔⒕⒖⒗⒘⒙⒚⒛'
s_num_comma					= '🄁🄂🄃🄄🄅🄆🄇🄈🄉🄊'
s_num_circle				= '⓪①②③④⑤⑥⑦⑧⑨'
s_num_wide					= '０１２３４５６７８９'
s_hex_lower					= '0123456789abcdef'
s_hex_upper					= '0123456789ABCDEF'
quad_zorder					= u' ▘▝▀▖▌▞▛▗▚▐▜▄▙▟█'
prog_l0r8					= u' ▏▎▍▌▋▊▉█'
prog_s0n8					= u' ▁▂▃▄▅▆▇█'
gradients_r0l4 				= u' ░▒▓█'
tris_small_4ccwe			= u'▸▴◂▾'
tris_8ccwe					= u'▶◥▲◤◀◣▼◢'
tris_line_4ccwe				= u'▷△◁▽'
tris_line_small_4ccwe		= u'▹▵◃▿'
pentagons					= '⬟⬠⭓⭔'
hexagons					= '⬢⬡⬣'
arrows_thin_8ccwe			= u'→↗↑↖←↙↓↘'
arrows_bold_8ccwe			= u'➨⬈⬆⬉⬅⬋⬇⬊'
arrows_lined_8ccwe			= u'⇨⬀⇧⬁⇦⬃⇩⬂'
arrows_hollow_8ccwe			= u'⇒⇗⇑⇖⇐⇙⇓⇘'
arrows_right_8				= '⬎⬏⬐⬑↰↱↲↳'
dashes_horz_thin_4 = dashes	= '─╌┄┈'
dashes_horz_bold_4			= '━╍┅┉'
dashes_vert_thin_4			= '│╎┆┊'
dashes_vert_bold_4			= '┃╏┇┋'
dots_horz_3 = dots			= '․‥…'
dots_vert_4					= '‧⁚⁝⁞'
dots_tri					= '⋮⋯⋰⋱'
dots_misc					= '⁖⁘⁙∴∵∶∷※⁜⁒'
quotes						= '′″‴‵‶‷'
ctrl_symbols				= '␀␁␂␃␄␅␆␇␈␉␊␋␌␍␎␏␐␑␒␓␔␕␖␗␘␙␚␛␜␝␞␟␠␡␤'
die_n6						= u'⚀⚁⚂⚃⚄⚅'
dominos_n49					= (	u'🁣🁤🁥🁦🁧🁨🁩'
								u'🁪🁫🁬🁭🁮🁯🁰'
								u'🁱🁲🁳🁴🁵🁶🁷'
								u'🁸🁹🁺🁻🁼🁽🁾'
								u'🁿🂀🂁🂂🂃🂄🂅'
								u'🂆🂇🂈🂉🂊🂋🂌'
								u'🂍🂎🂏🂐🂑🂒🂓' )
chess_n12					= u'♟♞♝♜♛♚♙♘♗♖♕♔'
cardsuits_n8				= u'♣♦♥♠♧♢♡♤'
carddeck_n56				= ( u'🃑🃒🃓🃔🃕🃖🃗🃘🃙🃚🃛🃜🃝🃞'
								u'🃁🃂🃃🃄🃅🃆🃇🃈🃉🃊🃋🃌🃍🃎'
								u'🂱🂲🂳🂴🂵🂶🂷🂸🂹🂺🂻🂼🂽🂾'
								u'🂡🂢🂣🂤🂥🂦🂧🂨🂩🂪🂫🂬🂭🂮'
								u'🃏🃟🂠🂿' )
monogram_n3 				= u'⚊⚋𝌀'
digram_n9					= u'⚌⚍⚎⚏𝌁𝌂𝌃𝌄𝌅'
trigram_n8					= u'☰☱☲☳☴☵☶☷'
hexagram_n64				= (	u'䷀䷫䷌䷠䷉䷅䷘䷋'
								u'䷈䷸䷤䷴䷼䷺䷩䷓'
								u'䷍䷱䷝䷷䷥䷿䷔䷢'
								u'䷙䷑䷕䷳䷨䷃䷚䷖'
								u'䷪䷛䷞䷰䷹䷮䷐䷬'
								u'䷄䷯䷾䷦䷻䷜䷂䷇'
								u'䷡䷟䷶䷽䷵䷧䷲䷏'
								u'䷊䷭䷣䷎䷒䷆䷗䷁' )
tetragram_81				= (	u'𝌆𝌇𝌈𝌉𝌊𝌋𝌌𝌍𝌎'
								u'𝌏𝌐𝌑𝌒𝌓𝌔𝌕𝌖𝌗'
								u'𝌘𝌙𝌚𝌛𝌜𝌝𝌞𝌟𝌠'
								u'𝌡𝌢𝌣𝌤𝌥𝌦𝌧𝌨𝌩'
								u'𝌪𝌫𝌬𝌭𝌮𝌯𝌰𝌱𝌲'
								u'𝌳𝌴𝌵𝌶𝌷𝌸𝌹𝌺𝌻'
								u'𝌼𝌽𝌾𝌿𝍀𝍁𝍂𝍃𝍄'
								u'𝍅𝍆𝍇𝍈𝍉𝍊𝍋𝍌𝍍'
								u'𝍎𝍏𝍐𝍑𝍒𝍓𝍔𝍕𝍖' )

s_num_super					= s_num_sup
tchs_lower,tchs_upper		= map(tuple,[s_lower,s_upper])
hchs_lower,hchs_upper		= map(frozenset,[s_lower,s_upper])
#endregion

#region charset colls
numsub_at_num				= dict(zip([*range(10),*s_num_std], s_num_sub*2))
numsuper_at_num				= dict(zip([*range(10),*s_num_std], s_num_super*2))
numdot_at_num				= dict(zip([*range(10),*s_num_std], s_num_dot[:10]*2))
numcircle_at_num			= dict(zip([*range(10),*s_num_std], s_num_circle*2))
map_numsub,    map_numsuper,    map_numcircle,    map_numdot = (
numsub_at_num, numsuper_at_num, numcircle_at_num, numdot_at_num, )
ind_of_ch,rank_of_ch		= [lambda ch,ind_base=ord('a')-base: ord(ch)-ind_base for base in [0,1]]
#endregion

#region ctrl char defs
_words_of_s					= lambda s: tuple(filter(bool,s.split()))
class Ctrl:
	__slots__				= _words_of_s('num name abrv ch_key name_alt ch_hex key_press')
	def __init__(self,num, name, abrv, ch_key, name_alt=None):
		self.num = num
		self.name = name
		self.abrv = abrv
		self.ch_hex = _ctrl.ch_hex_of(num)
		self.ch_key = ch_key
		self.key_press = _ctrl.keypress_of(ch_key)
		self.name_alt = name_alt
	def __str__(self):	return self.ch_hex
	def __repr__(self):	return self.key_press
class _Vars(dict):
	_fill = ()
	def __getattr__(self, key):		return self.get(key,*self._fill)
class _ctrl:
	''' first 32 non-printable char (control chars) '''
	ch_hex_of				= '\\x{:02x}'.format
	keypress_of				= 'ctrl+{}'.format
	nums = nn				= range(32)
	ch_hexs = chs			= tuple(map(ch_hex_of,nums))
	keyevt					= '@abcdefghijklmnopqrstuvwxyz[\]^_'
	names = kk				= _words_of_s(
								'null                start_of_heading    start_of_text       end_of_text               '
								'end_of_transmission end_of_query        acknowledge         beep                      '
								'backspace           horizontal_tab      line_feed           vertical_tab              '
								'form_feed           carriage_return     shift_out           shift_in                  '
								'data_link_escape    device_control_1    device_control_2    device_control_3          '
								'device_control_4    negative_ack        synchronize         end_of_transmission_block '
								'cancel              end_of_medium       substitute          escape                    '
								'file_separator      group_separator     record_separator    unit_separator            ' )
	abrvs = cchs			= _words_of_s(
								'nul                 soh                 stx                 etx '
								'eot                 enq                 ack                 bel '
								'bs                  ht                  lf                  vt  '
								'ff                  cr                  so                  si  '
								'dle                 dc1                 dc2                 dc3 '
								'dc4                 nak                 syn                 etb '
								'can                 em                  sub                 esc '
								'fs                  gs                  rs                  us  ' )
	_screen_home			= '\33[2H'
	_screen_clear			= '\33[2J'
class ctrl(_ctrl):
	vals = vv				= tuple(Ctrl(*pa) for pa in zip(_ctrl.nums,_ctrl.names,_ctrl.abrvs,_ctrl.keyevt))
	vals_at = at			= _Vars(zip(_ctrl.cchs+_ctrl.kk+vv, vals*3))
	d						= at.end_of_text
	""" todo: name_alt values
	28	1c	right arrow
	29	1d	left arrow
	30	1e	up arrow
	31	1f	down arrow
	"""
#endregion

#region scratch area

'''
U+290E	⤎	e2 a4 8e	LEFTWARDS TRIPLE DASH ARROW
U+290F	⤏	e2 a4 8f	RIGHTWARDS TRIPLE DASH ARROW
U+2911	⤑	e2 a4 91	RIGHTWARDS ARROW WITH DOTTED STEM

U+2919	⤙	e2 a4 99	LEFTWARDS ARROW-TAIL
U+291A	⤚	e2 a4 9a	RIGHTWARDS ARROW-TAIL
U+291B	⤛	e2 a4 9b	LEFTWARDS DOUBLE ARROW-TAIL
U+291C	⤜	e2 a4 9c	RIGHTWARDS DOUBLE ARROW-TAIL

U+2921	⤡	e2 a4 a1	NORTH WEST AND SOUTH EAST ARROW
U+2922	⤢	e2 a4 a2	NORTH EAST AND SOUTH WEST ARROW

U+2927	⤧	e2 a4 a7	NORTH WEST ARROW AND NORTH EAST ARROW
U+2928	⤨	e2 a4 a8	NORTH EAST ARROW AND SOUTH EAST ARROW
U+2929	⤩	e2 a4 a9	SOUTH EAST ARROW AND SOUTH WEST ARROW
U+292A	⤪	e2 a4 aa	SOUTH WEST ARROW AND NORTH WEST ARROW

U+292B	⤫	e2 a4 ab	RISING DIAGONAL CROSSING FALLING DIAGONAL
U+292C	⤬	e2 a4 ac	FALLING DIAGONAL CROSSING RISING DIAGONAL
U+292D	⤭	e2 a4 ad	SOUTH EAST ARROW CROSSING NORTH EAST ARROW
U+292E	⤮	e2 a4 ae	NORTH EAST ARROW CROSSING SOUTH EAST ARROW
U+292F	⤯	e2 a4 af	FALLING DIAGONAL CROSSING NORTH EAST ARROW
U+2930	⤰	e2 a4 b0	RISING DIAGONAL CROSSING SOUTH EAST ARROW
U+2931	⤱	e2 a4 b1	NORTH EAST ARROW CROSSING NORTH WEST ARROW
U+2932	⤲	e2 a4 b2	NORTH WEST ARROW CROSSING NORTH EAST ARROW


U+2934	⤴	e2 a4 b4	ARROW POINTING RIGHTWARDS THEN CURVING UPWARDS
U+2935	⤵	e2 a4 b5	ARROW POINTING RIGHTWARDS THEN CURVING DOWNWARDS
U+2936	⤶	e2 a4 b6	ARROW POINTING DOWNWARDS THEN CURVING LEFTWARDS
U+2937	⤷	e2 a4 b7	ARROW POINTING DOWNWARDS THEN CURVING RIGHTWARDS

U+2938	⤸	e2 a4 b8	RIGHT-SIDE ARC CLOCKWISE ARROW
U+2939	⤹	e2 a4 b9	LEFT-SIDE ARC ANTICLOCKWISE ARROW
U+293A	⤺	e2 a4 ba	TOP ARC ANTICLOCKWISE ARROW
U+293B	⤻	e2 a4 bb	BOTTOM ARC ANTICLOCKWISE ARROW
U+293E	⤾	e2 a4 be	LOWER RIGHT SEMICIRCULAR CLOCKWISE ARROW
U+293F	⤿	e2 a4 bf	LOWER LEFT SEMICIRCULAR ANTICLOCKWISE ARROW
U+2940	⥀	e2 a5 80	ANTICLOCKWISE CLOSED CIRCLE ARROW
U+2941	⥁	e2 a5 81	CLOCKWISE CLOSED CIRCLE ARROW


U+294A	⥊	e2 a5 8a	LEFT BARB UP RIGHT BARB DOWN HARPOON
U+294B	⥋	e2 a5 8b	LEFT BARB DOWN RIGHT BARB UP HARPOON
U+294C	⥌	e2 a5 8c	UP BARB RIGHT DOWN BARB LEFT HARPOON
U+294D	⥍	e2 a5 8d	UP BARB LEFT DOWN BARB RIGHT HARPOON
U+294E	⥎	e2 a5 8e	LEFT BARB UP RIGHT BARB UP HARPOON
U+294F	⥏	e2 a5 8f	UP BARB RIGHT DOWN BARB RIGHT HARPOON
U+2950	⥐	e2 a5 90	LEFT BARB DOWN RIGHT BARB DOWN HARPOON
U+2951	⥑	e2 a5 91	UP BARB LEFT DOWN BARB LEFT HARPOON

U+2962	⥢	e2 a5 a2	LEFTWARDS HARPOON WITH BARB UP ABOVE LEFTWARDS HARPOON WITH BARB DOWN
U+2963	⥣	e2 a5 a3	UPWARDS HARPOON WITH BARB LEFT BESIDE UPWARDS HARPOON WITH BARB RIGHT
U+2964	⥤	e2 a5 a4	RIGHTWARDS HARPOON WITH BARB UP ABOVE RIGHTWARDS HARPOON WITH BARB DOWN
U+2965	⥥	e2 a5 a5	DOWNWARDS HARPOON WITH BARB LEFT BESIDE DOWNWARDS HARPOON WITH BARB RIGHT

U+296E	⥮	e2 a5 ae	UPWARDS HARPOON WITH BARB LEFT BESIDE DOWNWARDS HARPOON WITH BARB RIGHT
U+296F	⥯	e2 a5 af	DOWNWARDS HARPOON WITH BARB LEFT BESIDE UPWARDS HARPOON WITH BARB RIGHT


U+2B0E	⬎	e2 ac 8e	RIGHTWARDS ARROW WITH TIP DOWNWARDS
U+2B0F	⬏	e2 ac 8f	RIGHTWARDS ARROW WITH TIP UPWARDS
U+2B10	⬐	e2 ac 90	LEFTWARDS ARROW WITH TIP DOWNWARDS
U+2B11	⬑	e2 ac 91	LEFTWARDS ARROW WITH TIP UPWARDS



U+2578	╸	e2 95 b8	BOX DRAWINGS HEAVY LEFT
U+2579	╹	e2 95 b9	BOX DRAWINGS HEAVY UP
U+257A	╺	e2 95 ba	BOX DRAWINGS HEAVY RIGHT
U+257B	╻	e2 95 bb	BOX DRAWINGS HEAVY DOWN

U+250F	┏	e2 94 8f	BOX DRAWINGS HEAVY DOWN AND RIGHT
U+2513	┓	e2 94 93	BOX DRAWINGS HEAVY DOWN AND LEFT
U+2517	┗	e2 94 97	BOX DRAWINGS HEAVY UP AND RIGHT
U+251B	┛	e2 94 9b	BOX DRAWINGS HEAVY UP AND LEFT

U+2523	┣	e2 94 a3	BOX DRAWINGS HEAVY VERTICAL AND RIGHT
U+252B	┫	e2 94 ab	BOX DRAWINGS HEAVY VERTICAL AND LEFT
U+2533	┳	e2 94 b3	BOX DRAWINGS HEAVY DOWN AND HORIZONTAL
U+253B	┻	e2 94 bb	BOX DRAWINGS HEAVY UP AND HORIZONTAL

U+254B	╋	e2 95 8b	BOX DRAWINGS HEAVY VERTICAL AND HORIZONTAL


U+2574	╴	e2 95 b4	BOX DRAWINGS LIGHT LEFT
U+2575	╵	e2 95 b5	BOX DRAWINGS LIGHT UP
U+2576	╶	e2 95 b6	BOX DRAWINGS LIGHT RIGHT
U+2577	╷	e2 95 b7	BOX DRAWINGS LIGHT DOWN

U+250C	┌	e2 94 8c	BOX DRAWINGS LIGHT DOWN AND RIGHT
U+2510	┐	e2 94 90	BOX DRAWINGS LIGHT DOWN AND LEFT
U+2514	└	e2 94 94	BOX DRAWINGS LIGHT UP AND RIGHT
U+2518	┘	e2 94 98	BOX DRAWINGS LIGHT UP AND LEFT

U+251C	├	e2 94 9c	BOX DRAWINGS LIGHT VERTICAL AND RIGHT
U+2524	┤	e2 94 a4	BOX DRAWINGS LIGHT VERTICAL AND LEFT
U+252C	┬	e2 94 ac	BOX DRAWINGS LIGHT DOWN AND HORIZONTAL
U+2534	┴	e2 94 b4	BOX DRAWINGS LIGHT UP AND HORIZONTAL

U+253C	┼	e2 94 bc	BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL


U+2550	═	e2 95 90	BOX DRAWINGS DOUBLE HORIZONTAL
U+2551	║	e2 95 91	BOX DRAWINGS DOUBLE VERTICAL
U+2552	╒	e2 95 92	BOX DRAWINGS DOWN SINGLE AND RIGHT DOUBLE
U+2553	╓	e2 95 93	BOX DRAWINGS DOWN DOUBLE AND RIGHT SINGLE
U+2554	╔	e2 95 94	BOX DRAWINGS DOUBLE DOWN AND RIGHT
U+2555	╕	e2 95 95	BOX DRAWINGS DOWN SINGLE AND LEFT DOUBLE
U+2556	╖	e2 95 96	BOX DRAWINGS DOWN DOUBLE AND LEFT SINGLE
U+2557	╗	e2 95 97	BOX DRAWINGS DOUBLE DOWN AND LEFT
U+2558	╘	e2 95 98	BOX DRAWINGS UP SINGLE AND RIGHT DOUBLE
U+2559	╙	e2 95 99	BOX DRAWINGS UP DOUBLE AND RIGHT SINGLE
U+255A	╚	e2 95 9a	BOX DRAWINGS DOUBLE UP AND RIGHT
U+255B	╛	e2 95 9b	BOX DRAWINGS UP SINGLE AND LEFT DOUBLE
U+255C	╜	e2 95 9c	BOX DRAWINGS UP DOUBLE AND LEFT SINGLE
U+255D	╝	e2 95 9d	BOX DRAWINGS DOUBLE UP AND LEFT
U+255E	╞	e2 95 9e	BOX DRAWINGS VERTICAL SINGLE AND RIGHT DOUBLE
U+255F	╟	e2 95 9f	BOX DRAWINGS VERTICAL DOUBLE AND RIGHT SINGLE
U+2560	╠	e2 95 a0	BOX DRAWINGS DOUBLE VERTICAL AND RIGHT
U+2561	╡	e2 95 a1	BOX DRAWINGS VERTICAL SINGLE AND LEFT DOUBLE
U+2562	╢	e2 95 a2	BOX DRAWINGS VERTICAL DOUBLE AND LEFT SINGLE
U+2563	╣	e2 95 a3	BOX DRAWINGS DOUBLE VERTICAL AND LEFT
U+2564	╤	e2 95 a4	BOX DRAWINGS DOWN SINGLE AND HORIZONTAL DOUBLE
U+2565	╥	e2 95 a5	BOX DRAWINGS DOWN DOUBLE AND HORIZONTAL SINGLE
U+2566	╦	e2 95 a6	BOX DRAWINGS DOUBLE DOWN AND HORIZONTAL
U+2567	╧	e2 95 a7	BOX DRAWINGS UP SINGLE AND HORIZONTAL DOUBLE
U+2568	╨	e2 95 a8	BOX DRAWINGS UP DOUBLE AND HORIZONTAL SINGLE
U+2569	╩	e2 95 a9	BOX DRAWINGS DOUBLE UP AND HORIZONTAL
U+256A	╪	e2 95 aa	BOX DRAWINGS VERTICAL SINGLE AND HORIZONTAL DOUBLE
U+256B	╫	e2 95 ab	BOX DRAWINGS VERTICAL DOUBLE AND HORIZONTAL SINGLE
U+256C	╬	e2 95 ac	BOX DRAWINGS DOUBLE VERTICAL AND HORIZONTAL

U+25AC	▬	e2 96 ac	BLACK RECTANGLE
U+25AE	▮	e2 96 ae	BLACK VERTICAL RECTANGLE
U+25B0	▰	e2 96 b0	BLACK PARALLELOGRAM
U+25AD	▭	e2 96 ad	WHITE RECTANGLE
U+25AF	▯	e2 96 af	WHITE VERTICAL RECTANGLE
U+25B1	▱	e2 96 b1	WHITE PARALLELOGRAM

U+25BA	►	e2 96 ba	BLACK RIGHT-POINTING POINTER
U+25C4	◄	e2 97 84	BLACK LEFT-POINTING POINTER
U+25BB	▻	e2 96 bb	WHITE RIGHT-POINTING POINTER
U+25C5	◅	e2 97 85	WHITE LEFT-POINTING POINTER

U+25CA	◊	e2 97 8a	LOZENGE
U+25D8	◘	e2 97 98	INVERSE BULLET


U+25C6	◆	e2 97 86	BLACK DIAMOND
U+25C7	◇	e2 97 87	WHITE DIAMOND
U+25C8	◈	e2 97 88	WHITE DIAMOND CONTAINING BLACK SMALL DIAMOND

U+25EC	◬	e2 97 ac	WHITE UP-POINTING TRIANGLE WITH DOT
U+25ED	◭	e2 97 ad	UP-POINTING TRIANGLE WITH LEFT HALF BLACK
U+25EE	◮	e2 97 ae	UP-POINTING TRIANGLE WITH RIGHT HALF BLACK



U+25F0	◰	e2 97 b0	WHITE SQUARE WITH UPPER LEFT QUADRANT
U+25F1	◱	e2 97 b1	WHITE SQUARE WITH LOWER LEFT QUADRANT
U+25F2	◲	e2 97 b2	WHITE SQUARE WITH LOWER RIGHT QUADRANT
U+25F3	◳	e2 97 b3	WHITE SQUARE WITH UPPER RIGHT QUADRANT

U+25E7	◧	e2 97 a7	SQUARE WITH LEFT HALF BLACK
U+25E8	◨	e2 97 a8	SQUARE WITH RIGHT HALF BLACK
U+25E9	◩	e2 97 a9	SQUARE WITH UPPER LEFT DIAGONAL HALF BLACK
U+25EA	◪	e2 97 aa	SQUARE WITH LOWER RIGHT DIAGONAL HALF BLACK
U+25A3	▣	e2 96 a3	WHITE SQUARE CONTAINING BLACK SMALL SQUARE
U+25EB	◫	e2 97 ab	WHITE SQUARE WITH VERTICAL BISECTING LINE

U+25A4	▤	e2 96 a4	SQUARE WITH HORIZONTAL FILL
U+25A5	▥	e2 96 a5	SQUARE WITH VERTICAL FILL
U+25A6	▦	e2 96 a6	SQUARE WITH ORTHOGONAL CROSSHATCH FILL
U+25A7	▧	e2 96 a7	SQUARE WITH UPPER LEFT TO LOWER RIGHT FILL
U+25A8	▨	e2 96 a8	SQUARE WITH UPPER RIGHT TO LOWER LEFT FILL
U+25A9	▩	e2 96 a9	SQUARE WITH DIAGONAL CROSSHATCH FILL

U+25AA	▪	e2 96 aa	BLACK SMALL SQUARE
U+25AB	▫	e2 96 ab	WHITE SMALL SQUARE



U+25F4	◴	e2 97 b4	WHITE CIRCLE WITH UPPER LEFT QUADRANT
U+25F5	◵	e2 97 b5	WHITE CIRCLE WITH LOWER LEFT QUADRANT
U+25F6	◶	e2 97 b6	WHITE CIRCLE WITH LOWER RIGHT QUADRANT
U+25F7	◷	e2 97 b7	WHITE CIRCLE WITH UPPER RIGHT QUADRANT

U+25D0	◐	e2 97 90	CIRCLE WITH LEFT HALF BLACK
U+25D1	◑	e2 97 91	CIRCLE WITH RIGHT HALF BLACK
U+25D2	◒	e2 97 92	CIRCLE WITH LOWER HALF BLACK
U+25D3	◓	e2 97 93	CIRCLE WITH UPPER HALF BLACK
U+25CB	○	e2 97 8b	WHITE CIRCLE
U+25D4	◔	e2 97 94	CIRCLE WITH UPPER RIGHT QUADRANT BLACK
U+25D1	◑	e2 97 91	CIRCLE WITH RIGHT HALF BLACK
U+25D5	◕	e2 97 95	CIRCLE WITH ALL BUT UPPER LEFT QUADRANT BLACK
U+25CF	●	e2 97 8f	BLACK CIRCLE
U+25CE	◎	e2 97 8e	BULLSEYE
U+25C9	◉	e2 97 89	FISHEYE
U+25CD	◍	e2 97 8d	CIRCLE WITH VERTICAL FILL
U+25CC	◌	e2 97 8c	DOTTED CIRCLE


U+25EF	◯	e2 97 af	LARGE CIRCLE
U+25E6	◦	e2 97 a6	WHITE BULLET



U+25DC	◜	e2 97 9c	UPPER LEFT QUADRANT CIRCULAR ARC
U+25DD	◝	e2 97 9d	UPPER RIGHT QUADRANT CIRCULAR ARC
U+25DE	◞	e2 97 9e	LOWER RIGHT QUADRANT CIRCULAR ARC
U+25DF	◟	e2 97 9f	LOWER LEFT QUADRANT CIRCULAR ARC

U+25E0	◠	e2 97 a0	UPPER HALF CIRCLE
U+25E1	◡	e2 97 a1	LOWER HALF CIRCLE

U+25D6	◖	e2 97 96	LEFT HALF BLACK CIRCLE
U+25D7	◗	e2 97 97	RIGHT HALF BLACK CIRCLE

U+25D9	◙	e2 97 99	INVERSE WHITE CIRCLE
U+25DA	◚	e2 97 9a	UPPER HALF INVERSE WHITE CIRCLE
U+25DB	◛	e2 97 9b	LOWER HALF INVERSE WHITE CIRCLE



⌚
8986	⌛

✓
10003	✔
10004	✕
10005	✖
10006	✗
10007
✘
10008	✙
10009	✚
10010	✛
10011	✜
10012	✝
10013	✞
10014	✟
10015	✠
10016	✡
10017	✢
10018	✣
10019
✤
10020	✥
10021	✦
10022	✧
10023	✨
10024	✩
10025	✪
10026	✫
10027	✬
10028	✭
10029	✮
10030	✯
10031
✰
10032	✱
10033	✲
10034	✳
10035	✴
10036	✵
10037	✶
10038	✷
10039	✸
10040	✹
10041	✺
10042	✻
10043
✼
10044	✽
10045	✾
10046	✿
10047	❀
10048	❁
10049	❂
10050	❃
10051	❄
10052	❅
10053	❆
10054	❇
10055
❈
10056	❉
10057	❊
10058	❋
10059	❌


↷

<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M3 21H21V19H3V21ZM3 17H21V15H3V17ZM3 13H21V11H3V13ZM3 9H21V7H3V9ZM3 3V5H21V3H3Z" fill="black"/>
</svg>


https://textart4u.blogspot.com/2012/03/zig-zag-text-art-ascii-art.html
㊀ ㊁ ㊂ ㊃ ㊄ ㊅ ㊆ ㊇ ㊈ ㊉ ㊊ ㊋ ㊌ ㊍ ㊎ ㊏ ㊐ ㊑ ㊒ ㊓ ㊔ ㊕ ㊖ ㊗ ㊘ ㊙
 ㊚ ㊛ ㊜ ㊝ ㊞ ㊟ ㊠ ㊡ ㊢ ㊣ ㊤ ㊥ ㊦ ㊧ ㊨ ㊩ ㊪ ㊫ ㊬ ㊭ ㊮ ㊯ ㊰ ➀ ➁ ➂ ➃ ➄ 
 ➅ ➆ ➇ ➈ ➉·¨…¦┅┆➊ ➋ ➌ ➍ ➎ ➏ ➐ ➑ ➒ ➓ α ɐ β ɔ 卐 ™ © ® ¿ ¡ ½ ⅓ ⅔ ¼ ¾ ⅛ ⅜ ⅝ 
 ⅞ ℅ № ⇨ ❝ ❞ ℃ ∃ ┈ ℑ ∧ ∠ ∨ ∩ ⊂ ⊃ ∪ ⊥ ∀ Ξ Γ ə ɘ ε ɟ ɥ ɯ и η ℵ ℘ ๏ ɹ ʁ ℜ я ʌ ʍ 
 λ ℓ ч ∞ Σ Π ⌥ ⌘ ¢ € £¥ Ⓐ Ⓑ Ⓒ Ⓓ Ⓔ Ⓕ Ⓖ Ⓗ Ⓘ Ⓙ Ⓚ Ⓛ Ⓜ Ⓝ Ⓞ Ⓟ Ⓠ Ⓡ Ⓢ Ⓣ Ⓤ Ⓥ Ⓦ Ⓧ Ⓨ 
 Ⓩ ╧ ╨ ╤ ╥ ╙ ⓐ ⓑ ⓒ ⓓ ⓔ ⓕ ⓖ ⓗ ⓘ ⓙ ⓚ ⓛ ⓜ ⓝ ⓞ ⓟ ⓠ ⓡ ⓢ ⓣ ⓤ ⓥ ⓦ ⓧ ⓨ ⓩ ╒ ╓ ╫ ╪ 
 ┘ ツ ♋ 웃 유 Σ ⊗ ♒ ☠ ☮ ☯ ♠ Ω ♤ ♣ ♧ ♥ ♡ ♦ ♢ ♔ ♕ ♚ ♛ ★ ☆ ✮ ✯ ☄ ☾ ☽ ♏ ╘ ┌ ╬ ☼ ☀
☁ ☂ ☃ ☻ ☺ ۞ ۩ ♬ ✄ ✂ ✆ ✉ ✦ ✧ ∞ ♂ ♀ ☿ ❤ ❥ ❦ ❧ ™ ® © ✗ ✘ ⊗ ♒ ▢ ╛ ┐ ─ ┼ ▲ △ ▼ 
▽ ◆ ◇ ○ ◎ ● ◯ Δ ◕ ◔ ʊ ϟ ღ 回 ₪ ✓ ✔ ✕ ✖ ☢ ☣ ☤ ☥ ☦ ☧ ☨ ☩ ☪ ☫ ☬ ☭ └ ┴ ┬ ├ ┊╱ ╲
╳ ¯ – — ≡ ჻ ░ ▒ ▓ ▤ ▥ ▦ ▧ ▨ ▩ █ ▌ ▐ ▀ ▄ ◠ ◡ ╭ ╮ ╯ ╰ │ ┤ ╡ ╢ ╖ ╕ ╣ ║ ╝ ╜ ╞ 
╟ ╚ ╔ ╩ ╦ ╠ ═ { ｡ ^ ◕ ‿ ◕ ^ ｡ } ( ◕ ^ ^ ◕ ) ✖ ✗ ✘ ♒ ♬ ✄ ✂ ✆ ✉ ✦ ✧ ♱ ♰ ♂ ♀☿ 
❤ ❥ ❦❧ ™ ® © ♡ ♦ ♢ ♔ ♕ ♚ ♛ ★ ☆ ✮ ✯ ☄ ☾ ☽ ☼ ☀ ☁ ☂ ☃ ☻ ☺ ☹ ☮ ۞ ۩ ε ї з ☎ ☏
¢ ☚ ☛ ☜ ☝ ☞ ☟ ✍ ✌ ☢ ☣ ☠ ☮ ☯ ♠ ♤ ♣ ♧ ♥ ♨ ๑ ❀ ✿ ψ ♆ ☪ ☭ ♪ ♩ ♫ ʊ ϟ ღ ツ 回 
₪ 卐 © ® ¿ ¡ ½ ⅓ ⅔ ¼ ¾ ⅛ ⅜ ⅝ ⅞ ℅ № ⇨ ❝ ❞ ℃ ◠ ◡ ╭ ╮╯╰ ★ ☆ ⊙ ¤ ㊣ ★ ☆ ♀ ◆ ◇
™ ║ ▼ ╒ ▲ ◣ ◢ ◥ ▼ △ ▽ ⊿ ◤ ◥ △ ▴ ▵ ▶ ▷ ▸ ▹ ► ▻ ▼ ▽ ▾ ▿ ◀ ◁ ◂ ◃ ◄ ◅ ▆ ▇ █
█ ■ ▓ 回 □ 〓 ≡☌ ╝╚╔╗╬ ═╓╩ ┠┨┯┷┏ ┓┗┛┳⊥ ﹃﹄┌ ┐└┘∟「 」↑↓→ ←↘↙♀ ♂┇┅﹉﹊ 
﹍﹎╭╮╰╯ *^_^* ^*^ ^-^ ^_^ ^︵^∵∴‖ ︱︳︴﹏ ﹋﹌♂♀ ♥♡☜☞☎ ☏⊙◎☺☻ ►◄▧▨ ♨◐◑↔
↕ ▪▫☼♦▀ ▄█▌▐ ░▒▬♦◊ ◦ ☼ ♠ ♣ ▣ ▤▥▦▩ ぃ◘◙◈♫ ♬♪♩♭♪ の☆→あ ￡❤｡◕‿ ◕｡✎✟ஐ ≈๑۩ ۩.. 
..۩۩๑ ๑۩۞۩๑ ✲❈➹ ~.~◕ ‿-｡☀☂☁ 【】┱┲❣ ✚✪✣ ✤✥ ✦❉ ❥❦❧❃ ❂❁❀✄☪ ☣☢☠☭♈ ➸✓✔✕ ✖㊚㊛ 
*.:｡ ✿*ﾟ‘ﾟ･ ⊙¤㊣★☆ ♀◆◇ ◣◢◥▲△▽⊿◤ ◥▆▇ ██■▓ 回□〓≡╝ ╚╔╗ ╬═╓╩ ┠┨┯┷┏ ┓┗┛ ┳⊥﹃﹄ 
┌┐└┘∟ 「」↑↓ → ← ↘ ↙♀♂┇┅﹉ ﹊﹍﹎ ╭╮╰╯ *^_^* ^*^ ^-^ ^_^ ^︵^∵ ∴‖ ︱︳ ︴﹏﹋﹌ 
♂♀♥♡☜ ☞☎☏⊙ ◎☺☻►◄ ▧▨♨◐◑ ↔↕▪▫ ☼♦▀▄█ ▌▐░▒▬ ♦◊◦☼ ♠♣▣▤▥ ▦▩ぃ◘◙ ◈♫♬♪ ♩♭♪の☆ →あ
￡❤｡ ◕‿◕｡ ✎✟ஐ≈ ๑۩۩.. ..۩ ✉ ✍ ✎ ✏ ✐๑✲❈ ➹ ~.~◕‿-｡ ☀☂☁【】 ┱┲❣✚ ✪✣✤✥ ✦❉❥❦ ❧❃❂❁❀ 
✄☪☣☢☠ ☭♈➸✓✔✕✖㊚ ㊛♧♤♧♡♬♪*.:｡✿*ﾟ ‘ﾟ･ ◊♥╠═╝▫■๑»«¶ஐ©† εïз♪ღ♣ ♠•± °•ิ.•ஐ இ * × 
○ ▫ ✑ ✒ ⌨ ۩ ๑ ๑۩ ۞ ۩ ┭┮┯♂ • ♀ ◊ © ¤ ▲ ↔ ™ ® ☎ ε ї з ♨ ☏ ☆ ★ ▽ △ ▲ ∵ ∴ ∷ ＃ 
♂ ♀ ♥ ♠ ♣ ☹ ☺ ☻┌ ┍┎ ┏ ┐ ┑┓
 ♭♫♪ﻬஐღ ↔↕↘••● ¤╬﹌▽☜♥☞ ♬✞♥♕☯☭☠☃ ─ ━ │ ┃ ┄ ┅ ┆ ┇ ┈ ┉ ┊ ┋ ≨ ≩╨╩ ╪ ╫ ╬╏═≂ ≃ ≄ ≅ 
≆ ≇ ≈ ≉ ≊ ≋ ≌ ≍ ≎ ≏ ≐ ≑ ≒ ≓ ≔ ≕ ≖ ≗ ≘ ≙ ≚ ≛ ≜ ≝ ≞ ≟ ≠ ≡≢ ≣ ≤ ≥ ≦≧␛ ␡ ␚ ␟ ␘ ␠ 
␤ ␋ ␌ ␍ ␎ ␏ ␐ ␑ ␒ ␓ ␔ ␕ ␖ ␗ ␙ ␜ ␝ ␞╣ ╤ ╥ ╦ ╧ ╗ ╘ ╙ ╚ ╛╡ ┼ ┽ ┾ ┿ ╀ ╁ ╂ ╃ ╓ ╔ 
╕ ╖ ♈ ♉ ♊ ♋ ♌ ♍ ♎ ♏ ♐ ♑ ♒ ♓ ╮ ╯ ╰ ╱ ╲ ╳ ‟ †‡•‣▀ ▁ ▂ ▃ ▄ ▅ ▆ ▇ █ ▉ ▊ ▋ ▌ ▍ ▎ 
▏ ▐ ░ ▒ ▓ ▔ ▕ ■ □ ▢ ▣ ▤ ▥ ▦ ▧ ▨ ▩ ▪ ▫ ▬ ▭▮▯╭ ◞ ◟ ◠ ◡ ◢ ◣ ♔ ♕ ♖ ♗ ♘ ♙ ♚ ♛ ♝ 
♞ ♟ ♠ ♡ ♢ ♣ ☔ ☕ ☖ ☗ ☘ ☙ ☊ ☋ ☌ ☍ ☎ ☏☐ ╴ ╵ ╶ ╷ ╸ ╹ ╺ ╻ ╼ ╽ ╾ ╿ ▰ ▱ ◆ ◇ ◈ ◉ ◊ 
○ ◌ ◍ ◎ ● ◐ ◑ ◒ ◓ ◔ ◕ ◖ ◗ ◘ ◙ ◚ ◛ ◜ ◝ ◤ ◥ ◦ ◧ ◨ ◩ ◪ ◫ ◬ ◭ ◮ ◯◽ ◾ ◿ ☀ ☁ ☂ ☃ 
☄ ★ ☆ ☇ ☈ ☉ ☑ ☒ ☓ ♅ ♆ ♇♜ ⇜ ⇝ ⇞ ⇟ ⇠ ⇡⇢⇣☟ ☠ ☡ ☢ ☣ ☤ ☥ ☦ ☧ ☨ ☩ ☪ ☫ ☬ ☭ ☮ ☯ ☰ ☱ 
☲ ☳ ☴ ☵ ☶ ☷ ☸ ☹ ☺ ☻ ☼ ☽ ☾ ☿ ♀ ♁ ♂ ♃ ♄ℕ♤ ♥ ♦ ♧ ♨ ♩ ♪ ♫ ♬ ♭ ♮ ♯ ♰ ♱ ´ ῾ ‐ ‑ ‒ 
– — ― ‖ ‗ ‘ ’ ‚ ‛ “ ” „ ℋ ℌ ℍ ℎ ℏ ℐ ℑ ℒ ℓ ℔․ ‥ … ‧ ‰ ‱ ′ ″ ‴ ‵ ‶ ‷ ‸ ‹ › ※ ‼ 
‽ ‾ ‿ ⁀ ⁁ ⁂ ⁃ ⁄ ⁅ ⁆ ⁑ ⁞ ₠ ₡ ₢ ₣ ₤ ₥ ₦ ₧ ₨ ₩ ₪ ₫ € ₭₮ ₯ ℀ ℁ ℂ ℃ ℄ ℅ ℆ ℇ ℈ ℉ ℊ 
№ ℗ ℘ ℙ ℚ ℛ ℜ ℝ ℞ ℟ ℠ ℡ ™ ℣ ℤ ℥ Ω ℧ ℨ ℩ K Å ℬ ℭ ℮ ℯℰ ℱ Ⅎ ℳ ℴ ℵ ℶ ℷ ℸ ⅍ ⅎ ⅓ 
⅔ ⅕ ⅖ ⅗ ⅘ ⅙ ⅚ ⅛ ⅜ ⅝ ⅞ ⅟ ⇍ ⇎ ≺ ≻ ≼ ≽ ≾ ≿ ⊀ ∏ ∐ ∑ −∓⁰ ⁱ ⁲ ⁳ ⁴ ⁵ ⁶ ⁷ ⁸ ⁹ ⁺ ⁻ ⁼ ⁽ 
⁾ ⁿ ₀ ₁ ₂ ₃ ₄ ₅ ₆ ₇ ₈ ₉ ₊ ₋ ₌ ₍ ₎ ⇤ ⇥ ⇦ ⇧ ⇨ ⇩ ⇪ ⇹ ⇺ ⇻ ⇼ ⇽ ∔ ↶ₐ ₑ ₒ ₓ ₔⅠ Ⅱ Ⅲ Ⅳ 
Ⅴ Ⅵ Ⅶ Ⅷ Ⅸ Ⅹ Ⅺ Ⅻ Ⅼ Ⅽ Ⅾ Ⅿ ⅰ ⅱ ⅲ ⅳ ⅴ ⅵ ⅶ ⅷ ⅸ ⅹ ⅺ ⅻ ⅼ ⅽ ⅾ ⅿ ╢ ↵ ↷← ↑ → ↓ ↔ ↕ ↖ ↗ 
↘ ↙ ↚ ↛ ↜ ↝ ↞ ↟ ↠ ↡ ↢ ↣ ↤ ↥ ↦ ↧ ↨ ↩ ↪ ↫ ↬ ↭ ↮ ↯ ↰ ↱ ↲ ↳↴ ￡ ↸ ↹ ↺ ↻ ↼ ↽ ↾ ↿ 
⇀ ⇁ ⇂ ⇃ ⇄ ⇅ ⇆ ⇇ ⇈ ⇉ ⇊ ⇋ ⇌⇏ ⇐ ⇑ ⇒ ⇓ ⇔ ⇕ ⇖ ⇗ ⇘ ⇙ ⇚ ⇛ ∽∾⊣ ⊤⇫ ⇬ ⇭ ⇮ ⇯ ⇰ ⇱ ⇲ ⇳ ⇴ 
⇵ ⇶ ⇷ ⇸ ⇾ ⇿ ∀ ∁ ∂ ∃ ∄ ∅ ∆ ∇ ∈ ∉ ∊ ∋ ∌ ∍ ∎ ◙ ▤▥▦▧▨ ▩ ♤ ♧♡∕ ∖ ∗ ∘ ∙ √ ∛ ∜ ∝ ∞ 
∟ ∠ ∡ ∢ ∣ ∤ ∥ ∦ ∧ ∨ ∩ ∪ ∫ ∬ ∭ ∮ ∯ ∰ ∱ ∲ ∳ ∴ ∵ ∶ ∷ ∸ ∹ ∺ ∻ ∼ ∿ ≀ ≁ ≪ ≫ ≬ ≭ ≮ 
≯ ≰ ≱ ≲ ≳ ≴ ≵ ≶ ≷ ≸ ≹ ⁇ ⁈ ⁉ ‼ ‽ ⁇ ⁈ ⁉ ‼ ‽ ™ © ®⍘ ⍙ ♬ ♭ ♮ ♯♰♱⊁ ⊂ ⊃ ⊄ ⊅ ⊆ ⊇ ⊈ 
⊉ ⊊ ⊋ ⊌ ⊍ ⊎ ⊏ ⊐ ⊑ ⊒ ⊓ ⊔ ⊕ ⊖ ⊗ ⊘ ⊙ ⊚ ⊛ ⊜ ⊝ ⊞ ⊟ ⊠ ⊡ ⊢⊥⌕⌖ ⊦ ⊧ ⊨ ⊩ ⊪ ⊫ ⊬ ⊭ ⊮ ⊯ 
⊰ ⊱ ⊲ ⊳ ⊴ ⊵ ⊶ ⊷ ⊸ ⊹ ⊺ ⊻ ┌ ┍ ┎ ┏ ┐ ┑ ┒ ┓⋟ ⋠ ⋡ ⋢ ⋣ ⌓⌔⊼ ⊽ ⊾ ⊿ ⋀ ⋁ ⋂ ⋃ ⋄ ⋅ ⋆ ⋇ 
⋈ ⋉ ⋊ ⋋ ⋌ ⋍ ⋎ ⋏ ⋐ ⋑ ⋒ ⋓ ⋔ ⋕ ⋖ ⋗ ⋘ ⋙ ⋚ ⋛ ⋜ ⋝ ⋞ ♤ ♥♦♧♨ ⋤ ⋥ ⋦ ⋧ ⋨ ⋩ ⋪ ⋫ ⋬ ◐ ◑ 
☢ ⊗ ⊙ ◘❃ ❂ ○ ◎ ● ◯ ◕ ◔ ┄ ┅ ┆ ┇ ┈ ┉ ┊ ┋ ♩ ♪ ♫ ♜ ♝⋭ ⋮ ⋯ ⋰ ⋱ ⋲ ⋳ ⋴ ⋵ ⋶ ⋷ ⋸ ⋹ ⋺ 
⋻ ⋼ ⋽ ⋾ ⋿ ⌀ ⌁ ⌂ ⌃ ⌄ ⌅ ⌆ ⌇ ⌈ ⌉ ⌊ ⌋ ⌌ ⌍– ぱ ⌎ ⌐ ⌑⌒♔ ♕ ⌗ ⌘ ⌙ ⌚ ⌛ ⌜ ░ ▒ ▓ ▔ ▕ ª 
ↀ ↁ ↂ Ↄ ↄ ↅ⍚ ␋ ␢ ␣ ─ ━ │ ┃ ⌾ ⌿ ⍀ ⍁♞ ♟ ♠ ♡ ♢♣⌝ ⌞ ⌟ ⌠ ⌡ ⌢ ⌣ ⌤ ⌥ ⌦ ⌧ ⌨ 〈 〉 
⌫ ⌬ ⌭ ⌮ ⌯ ⌰ ⌱ ⌲ ⌳ ⌴ ⌵ ⌶ ⌷ ⌸ ⌹ ⌺ ⌻ ⌼ ⌽ ⍂ ⍃ ⍄ ⍅ ⍆ ⍇ ⍈ ⍉ ⍊ ⍋ ⍌ ⍍ ⍎ ⍏ ⍐ ⍑ ⍒ ⍓ 
⍔ ⍕ ⍖ ⍗ ♎ ♏ ♐ ♑ ♒ ♓ ♖ ♗ ♘ ♙♚♛頹 – 衙 – 浳 – 浤 – 搰 – 煤 – 洳 – 橱 – 橱 – 煪 – 
㍱ – 煱 – 둻 – 睤 – ㌹ – 楤 – ぱ – - 椹– 畱 – 煵 – 田 – つ – 煵 – 엌 – 嫠 – 쯦 – 
案 – 迎 – 是 – 從 – 事 – 網 – 頁 – 設 – 計 – 簡


☡
☇
∩

'''
#endregion