import os,sys
from pathlib import Path

#region local path defs
kp_loc					= 'PYKIT_LOCAL'
p_loc_usr = p_home		= Path('~').expanduser()
p_loc_cfg				= p_loc_usr/'.config'
p_loc_pykit=p_loc 		= Path(os.environ.get(kp_loc, p_loc_usr/'.pykit'))  # todo: p_loc_usr -> p_loc_cfg
p_pkg_pykit=p_pkg		= Path(sys.modules[__package__.split('.')[0]].__path__[0])
#endregion
