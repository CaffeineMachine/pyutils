# fixme: obsoleted by defs.iterdefs

from collections import Iterable, Sequence

is_single = lambda obj: not isinstance(obj, (Iterable, Sequence))
is_iterable = lambda obj: isinstance(obj, Iterable)
is_sequence = lambda obj: isinstance(obj, Sequence)
def is_hashable(obj):
	try:
		hash(obj)
		return True
	except TypeError as e:
		return False

to_iterable = lambda obj: not is_iterable(obj) and [obj] or obj
def to_single(obj, on_empty=None):
	result = on_empty
	if not is_iterable(obj):
		result = obj
	elif len(obj):
		result = obj[0]
	return result

get_first = lambda seq, default=None: seq[:1] and seq[:1][0] or default
get_last = lambda seq, default=None: seq[-1:] and seq[-1:][0] or default

strs = lambda seq: [str(s) for s in seq]
# sjoin = lambda seq, delim='', prefix=None, suffix=None: delim.join([str(s) for s in seq])
# def sjoin(seq, delim='', prefix='', suffix='', tostr=str):
# 	''' join input sequence as a string with delim separator
# 
# 	:param seq: sequence to convert to a string
# 	:param delim: separator
# 	:param prefix: if str concat to front of join result; if iterable prepends to seq; otherwise error
# 	:param suffix: if str concat to end of join result; if iterable appends to seq; otherwise error
# 	'''
# 	if not isinstance(prefix, str):
# 		seq = list(prefix) + list(seq)
# 		prefix = ''
# 	if not isinstance(suffix, str):
# 		seq = list(seq) + list(suffix)
# 		suffix = ''
# 	items = delim.join([tostr(s) for s in seq])
# 	result = f'{prefix}{items}{suffix}'
# 	return result

empty = lambda seq: len(seq) == 0
anything = lambda seq: len(seq) != 0  # anything([0]) => True;  any([0]) => False
