class Eval(object):
	''' interface which implements eval(*args, **kwargs) which generally returns a bool '''
	default_result = False
	def __init__(self, default_result=None):
		''' construct '''
		if default_result is not None:  # the classes static default_result is used if default_result is None
			self.default_result = default_result

	def eval(self, *args, **kwargs):
		''' override this function in in subclasses '''
		raise NotImplementedError

class EvalList(Eval):
	''' Generic container for multiple eval items; Still requires subclass to override eval() '''
	def __init__(self, *args, **kwargs):
		''' construct '''
		Eval.__init__(self, **kwargs)
		self.evals = args

class EvalOr(EvalList):
	''' implemention of Or result for multiple eval(*args, **kwargs) calls '''
	default_result = False
	def eval(self, *args, **kwargs):
		''' return the ORed result of eval(args) for all items in evals member list '''

		result = self.default_result
		for eval in self.evals:
			result = result or eval.eval(*args, **kwargs)
			if result:
				break
		return result

class EvalAnd(EvalList):
	''' implemention of And result for multiple eval(*args, **kwargs) calls '''
	default_result = True
	def eval(self, *args, **kwargs):
		''' return the ANDed result of eval(args) for all items in evals member list '''
		result = self.default_result
		for eval in self.evals:
			result = result and eval.eval(*args, **kwargs)
			if not result:
				break
		return result