from pyutils.defs.colldefs import xaplist
from pyutils.defs.numdefs import inf, n_max
from pyutils.defs.typedefs import *
from pyutils.defs.itemdefs import *
from pyutils.utilities.callbasics import *
from pyutils.utilities.argsutils import *

#region call defs
class DefsCallUtils:
	#region colls
	uses_at									= dict()
	to_key									= None
	#endregion
	#region scopes
	class Args:
		fmt_call							= 'RUNNING: {.__qualname__}(*{!s}, **{!s})'
		triallog_at							= {yes:(yes,print), no:(no,None)}
		triallog_any						= no,print
	AA										= Args
	#endregion
	...

DDCX = _D									= DefsCallUtils
AACX = _A									= DDCX.AA
#endregion

#region call forms
def funcof(call):		return getattr(call,'__func__',call)

fxof										= funcof
isfunc=isfx									= istypethen(type(funcof))
#endregion

#region call utils
def recalls(calls:iter, *pa_calls, obj=miss, **ka):
	''' prepare or perform recursive *calls* on *obj* '''
	calls = calls if not pa_calls else (calls,*pa_calls)
	recalls_out = (
		Recalls(*calls)			if obj is miss else
		recallsarg(calls, obj, **ka) )
	return recalls_out

def icall(calls:iter, *pa_calls, obj=miss, **ka):
	''' prepare or perform series of *calls* on *obj* '''
	calls = calls if not pa_calls else (calls,*pa_calls)
	calls_out = (
		ICall(*calls)			if obj is miss else
		icallsarg(calls, obj, **ka) )
	return calls_out

def icallpas(call, *pas, pal=(), par=(), zpa=(), keys=(),ka:dict=None, **_ka):
	''' get iterable of the outputs of given pos args *pa* on an operation given *call*, *pas*.
		:param call: sequence of operations to call for each item
		:param pas: pos args as vectors; the vectors are first zipped to get each pa
		:param pal: left pos args for all operation calls
		:param par: right pos args for all operation calls
		:param zpa: zipped pos args for each operation call
		:param ka: key args for all operation calls
	'''
	# resolve inputs
	ka = (ka and _ka) and dict(ka,**_ka) or ka or _ka
	if not callable(call):
		call,*ccx = call
		while ccx:
			*ccx,cx = ccx
			pas,ka = [icallpas(cx,*pas,**ka)], map0
	ipa = iter(pas and zip(*pas) or zpa)
	ipa = ipa if not pal and not par else ((*pal,*pa_nth,*par) for pa_nth in ipa)
	
	# generate
	if len(pas)==1 and not (pal or par or zpa or keys or ka):
		yield from map(call,pas[0])  # prefer c code where possible
	elif not keys:
		for pa_nth in ipa:
			yield call(*pa_nth,**ka)
	else:
		n_keys = len(keys)
		iaa = ((pa_nth[:n_keys],dict(zip(keys,pa_nth[n_keys:]))) for pa_nth in ipa)  # pair right of pa with to keys
		for pa_nth,ka_nth in iaa:
			yield call(*pa_nth, **ka_nth, **ka)
def callpas(*pa, fab=tuple, **ka):
	out = icallpas(*pa, **ka)
	out = out if not fab else fab(out)
	return out

def icallkas(call, kas=None, *_pa, pa=(), ka:dict=None, ika:iter=None, **_kas):
	''' get iterable of the outputs of given key args *ka* on an operation given *call*, *kas*.
		:param call: sequence of operations to call for each item
		:param kas: dict of key:vals of which the shortest determines the number of operation calls
		:param pa: constant pos args tuple for each operation call iteration
		:param ka: constant key args dict for each operation call iteration
		:param ika: iterable of dicts, 1 dict for each operation call iteration
	'''
	# resolve inputs
	pa = *_pa, *pa
	ka, ka_empty = ka if ka is not None else {}, {}
	kai = {key: iter(args) for key, args in (kas or _kas).items()}  # iterables congruent to icall are iter(kas.values()[n])
	ika_iter = ika and iter(ika)
	
	# generate output
	for vals_n in kai and zip(*kai.values()):
		kai_n = dict(zip(kai, vals_n))
		ika_n = next(ika_iter, ka_empty) if ika_iter else ka_empty
		yield call(*pa, **ka, **kai_n, **ika_n)
		
	if not ika_iter: return
	for ika_n in ika_iter:
		yield call(*pa, **ka, **ika_n)
def callkas(*pa, fab=tuple, **ka):
	out = icallkas(*pa, **ka)
	out = out if not fab else fab(out)
	return out

def icallargs(call, *pas, pal=(), par=(), zpa=(), ka=None, **kas):
	''' get iterable of the outputs of given pos & key args *pas* & *ka* on an operation given *call*, *kas*, *pas*.
		:param call: sequence of operations to call for each item
		:param pas: sequence of args, one for each operation call
		:param pal: left pos args for all operation calls
		:param par: right pos args for all operation calls
		:param zpa: zipped pos args for each operation call
		:param ka: key args for all operation calls
		:param kas: a dict of key:vals of which the shortest determines the number of operation calls
	'''
	# opt for more efficient callarg iterables when either pas or kas is emtpy
	results = (
		icallpas(call, *pas, pal=pal, par=par, zpa=zpa, ka=ka)		if not kas else
		icallkas(call, kas, *pal, *par)								if not pas and not zpa else
		None )
	if results:
		yield from results
		return
	
	# resolve inputs;
	ka, ka_empty = ka or {}, {}
	ipargs = iter(pas and zip(*pas) or zpa)  # todo: change args to pas where pargs is vectorized
	ikargs = {key: iter(args) for key, args in kas.items()}  # iterables congruent to icall are iter(kas.values()[n])
	
	# generate output: combine pa_nth, ka_nth; stcall at end of shortest iterable of key & pos args; less efficient term condition
	for ind in range(n_max):
		pa_nth = next(ipargs, nothing)
		pa_nth = () if pa_nth is nothing else pa_nth
		ka_nth = [next(args, nothing) for args in ikargs.values()]
		ka_nth = ka_empty if nothing in ka_nth else dict(zip(ikargs, ka_nth))
		if 0 == len(pa_nth) == len(ka_nth): return
		yield call(*pal, *pa_nth, *par, **ka, **ka_nth)
def callargs(*pa, fab=tuple, **ka):
	out = icallargs(*pa, **ka)
	out = out if not fab else fab(out)
	return out

def icallsarg(calls, *pa, **ka):
	''' get iterable of the outputs of given pos, key args *pa*, *ka* on a sequence of operations *icall*
		:param calls: a sequence of operations to call for each item
		:param pa: position args for all operations
		:param ka: key args for all operations
	'''
	for call in calls:
		yield call(*pa, **ka)
def callsarg(*pa, fab=tuple, **ka):
	out = icallsarg(*pa, **ka)
	out = out if not fab else fab(out)
	return out

def icallsargs(calls, args=None, *pas, pal=(), par=(), zpa=(), ka=None, trunc=no, **kas):
	''' get iterable of the outputs of given pos, key args *pa*, *ka* on a
		sequence of operations with inputs *icall*, *kas*, *pas*.
		:param calls: a sequence of operations to call for each item
		:param args: a sequence of args, one for each operation call
		:param pas: a sequence of pos args, a set of args for each operation call
		:param pal: left pos args for all operation calls
		:param par: right pos args for all operation calls
		:param zpa: zipped pos args for each operation call
		:param ka: key args for all operation calls
		:param kas: a sequence of key args for each operation call
	'''
	# resolve inputs
	ka = ka or {}
	ipargs = iter(args or zpa or gen(()) )  # todo: change args to pas where pargs is vectorized
	ikargs = {key: iter(args) for key, args in kas.items()}  # iterables congruent to icall are iter(kas.values()[n])
	
	# generate output
	for call in calls:
		pa_nth = next(ipargs, nothing) if trunc else next(ipargs)
		if pa_nth is nothing: return
		pa_nth = (pa_nth,) if args else pa_nth
		ka_nth = {key: next(args) for key, args in ikargs.items()}
		yield call(*pal, *pa_nth, *par, **ka, **ka_nth)
def callsargs(*pa, fab=tuple, **ka):
	out = icallsargs(*pa, **ka)
	out = out if not fab else fab(out)
	return out

def irecallargs(call, args, arg=miss, rev=no, **ka):
	''' simple cascaded =>vals from series of *call*s per given *args*. *arg* comes first when given '''
	iarg = iter(args)
	val = arg if arg is not miss else next(iarg)
	
	# generate vals
	yield val
	for arg in iarg:
		val = call(val,arg,**ka) if not rev else call(arg,val,**ka)
		yield val
def recallargs(call, *pa, **ka):
	''' simple cascade =>val from series of *call*s per given *args*. *arg* comes first when given '''
	val = (not pa and not ka) and recallargs.__get__(call) or ilast(irecallargs(call, *pa, **ka))
	return val

def irecallpas(call, *pas, pal=(), par=(), zpa=(), ka=None, arg=miss, to_pka=None, **_ka):
	''' simple cascaded =>vals from series of *call*s per given *pas* with *ka*. *arg* comes first when given. '''
	# resolve inputs
	ipargs = iter(pas and zip(*pas) or zpa)
	ka = (ka and _ka) and dict(ka, **_ka) or ka or _ka
	val = arg if arg != miss else next(ipargs)[0]
	
	# generate vals
	yield val
	for pa_nth in ipargs:
		pa_nth,ka_nth = to_pka(val,*pa_nth) if to_pka else ((val,*pa_nth), map0)
		val = call(*pal, *pa_nth, *par, **ka_nth, **ka)
		yield val
def recallpas(call, *pa, **ka):
	''' simple cascade =>val from series of *call*s per given *pas* with *ka*. *arg* comes first when given. '''
	val = (not pa and not ka) and recallpas.__get__(call) or ilast(irecallpas(call, *pa, **ka))
	return val

def irecallsarg(calls, arg, to_aa=None, pal=(), par=(), ka=None, lifo=no, **_ka):
	''' custom cascaded =>vals per each in *calls* given static *pa*,*ka*
		:param calls: a sequence of operations to call for each item
		:param to_aa: optional result to input pos, key args converter all subsequent calls
		:param pal:   left pos args for all operation calls
		:param par:   right pos args for all operation calls
		:param lifo:  order to apply calls ...
		:param pa:    first position args to sequence of operations
		:param ka:    first key args to sequence of operations
	'''
	# resolve inputs
	ka = (ka and _ka) and dict(ka, **_ka) or ka or _ka
	pa_nth,ka_nth = ((arg,), map0)
	lifo = getattr(calls,'lifo',lifo)
	icall = calls if not lifo else [*calls][::-1]
	
	# generate vals
	for call in icall:
		if to_aa:
			pa_nth,ka_nth = to_aa(arg)
		pa_nth = value, = call(*pal, *pa_nth, *par, **ka_nth, **ka),
		yield value
def recallsarg(calls, *pa, **ka):
	''' custom cascade =>val per each in *calls* given static *pa*,*ka* '''
	val = (not pa and not ka) and recallsarg.__get__(calls) or ilast(irecallsarg(calls, *pa, **ka))
	return val

dcallpas, hcallpas, lcallpas, _				= redef_per_coll(icallpas)
dcallkas, hcallkas, lcallkas, _				= redef_per_coll(icallkas)
dcallargs, hcallargs, lcallargs, _			= redef_per_coll(icallargs)
dcallsarg, hcallsarg, lcallsarg, _			= redef_per_coll(icallsarg)
dcallsargs,hcallsargs,lcallsargs, _			= redef_per_coll(icallsargs)
tcallpas,tcallkas,tcallargs,tcallsarg,tcallsargs= callpas,callkas,callargs,callsarg,callsargs

icxpas, icxkas, icxargs, iccxarg, iccxargs	= icallpas, icallkas, icallargs, icallsarg, icallsargs
dcxpas, dcxkas, dcxargs, dccxarg, dccxargs	= dcallpas, dcallkas, dcallargs, dcallsarg, dcallsargs
hcxpas, hcxkas, hcxargs, hccxarg, hccxargs	= hcallpas, hcallkas, hcallargs, hcallsarg, hcallsargs
lcxpas, lcxkas, lcxargs, lccxarg, lccxargs	= lcallpas, lcallkas, lcallargs, lcallsarg, lcallsargs
tcxpas, tcxkas, tcxargs, tccxarg, tccxargs	= tcallpas, tcallkas, tcallargs, tcallsarg, tcallsargs

cxpas, cxkas, cxargs, ccxarg, ccxargs		= callpas,callkas,callargs,callsarg,callsargs

ccx,rcxarg									= icall, recallsarg
#endregion

#region call helpers
class Recalls(xaplist):
	#region defs
	per										= icallsarg
	per_args								= icallsargs
	iof										= irecallsarg
	of = __call__							= recallsarg
	def __repr__(self):
		s = '<-'.join(afx('',ss(map(qualof,self),' {}() ')))
		return s
	#endregion
	...
class ICall(xaplist):
	#region defs
	per = __call__							= icallsarg
	per_args								= icallsargs
	iof										= irecallsarg
	of										= recallsarg
	#endregion
	...
class LCall(xaplist):
	#region defs
	per = __call__							= lcallsarg
	per_args								= icallsargs
	iof										= irecallsarg
	of										= recallsarg
	#endregion
	...

rcx,Rcx = calls,Calls						= recalls, Recalls
#endregion

#region usage utils
# todo: replace set_uses_of with gen_usage eliminate
# todo: replace count_use with gen_usage eliminate
_atkey,_atuses								= range(2)

def limit_use(limits, on_limits=miss, repeats=miss, *pa, var=no, at_uses=Ndx.all, on_iter=None, fill=None, **ka):
	''' basic series of callbacks when resp. usage quotas are hit. unlike limit_use_of does not use external state '''
	limits_ = isiter(limits,orput=[limits])
	on_limits_ = isiter(on_limits,orput=gen(on_limits))
	repeats_ = isiter(repeats,orput=gen(1))
	limits_ = list(
		limits_						if on_limits is repeats is miss else
		zip(*limits_.items())		if isdict(limits_) else
		zip(limits_,on_limits_,repeats_) )
	def iter_on_use():
		for _limit,*right in limits_:
			on_limit,repeat = *right,*(next(on_limits_),next(repeats_))[len(right):]
			for i in range(min(n_max,repeat)):
				yield from range(_limit,0,-1)
				on_limit and on_limit(*pa,**ka)
		yield from fill is not miss and gen(fill) or ()
	def ieval_on_use():
		for _limit,*right in limits_:
			on_limit,repeat = *right,*(next(on_limits_),next(repeats_))[len(right):]
			for i in range(min(n_max,repeat)):
				remain = _limit
				uses = yield
				while 0 < remain:
					out = (remain,on_limit)
					out = out if at_uses is None else dict(zip(out,at_uses)) if isiter(at_uses) else out[at_uses]
					on_iter and on_iter(out)
					uses = yield (remain,on_limit)[at_uses]
					remain -= uses
				on_limit and on_limit(*pa,**ka)
		yield from fill is not miss and gen(fill) or ()
	def on_use(uses=1):
		next(ion_use)
		return ion_use.send(uses)
	
	# resolve output callable
	ion_use = (var and ieval_on_use or iter_on_use)()
	staged = var and on_use or ion_use.__next__
	return staged

def set_uses_of(key,uses=0, to_key=None):
	_key,key = key, (to_key or _D.to_key or asis)(key)
	_D.uses_at[key] = uses

def get_usage(key,fill=0,uses_at=None, to_key=None, **ka):
	''' get callable sequence of ->uses for given *key* '''
	_key,key = key, (to_key or _D.to_key or asis)(key)
	uses_at = uses_at or _D.uses_at
	def use(incr=1,uses_next=None,fill=fill,**ka):
		''' for given *key* get ->used_curr and advance uses to next '''
		uses_curr = uses_at.get(key,fill)						# get uses sequence for key
		used_curr = (											# get currently used amount from uses
			uses_curr(miss)		if iscall(uses_curr) else
			uses_curr )
		uses_next = (											# advances uses for key to the next
			uses_next			if uses_next is not None else
			uses_curr+incr		if isnum(uses_curr) else
			inf					if used_curr is miss else
			uses_curr )
		uses_at[key] = uses_next								# update uses at key
		return used_curr
	use.__qualname__=use.__name__ = f'use({key!r})'
	return use

def count_use_of(key,to_key=None,fab_uses=None,**ka):
	''' get ->uses or count of times given *key* has been used. '''
	# resolve inputs
	_key,key = key, (to_key or _D.to_key or asis)(key)
	
	# resolve output
	use = get_usage(key,**ka)
	used = use(**ka)
	used_out = used if not fab_uses else fab_uses(used)
	return used_out
def limit_use_of(key,limit:int,**ka):	return count_use_of(key,fab_uses=int(limit).__sub__, **ka)
def later_use_of(key,**ka):				return count_use_of(key,fab_uses=0..__ne__, **ka)
def first_use_of(key,**ka):				return count_use_of(key,fab_uses=0..__eq__, **ka)

def claim_entry(k_entry):
	_D.entrys.append(k_entry)
def reclaim_entry(k_entry, entrys=None):
	entrys = isnone(entrys,put=_D.entrys)
	is_entry,did_reclaim = _D.can_entry(k_entry), False
	if is_entry:
		entrys and entrys[0] == k_entry and entrys.pop(0)
		did_reclaim = k_entry not in entrys
	return did_reclaim,is_entry
def ndx_of_entry(k_entry, entrys=None):
	entrys = isnone(entrys,put=_D.entrys)
	nth_entry = None if entrys else entrys.index(k_entry)
	return nth_entry

def in_range_of_nums(lo, hi, eq_lo=yes,eq_hi=yes):
	out = lambda num: lo<num<hi or num<hi<lo or hi<lo<num or (eq_lo and lo==num) or (eq_hi and hi==num)
	return out
def cmp_of_num(num):
	out = (
		num							if iscall(num) else
		num.__eq__  				if istype(num,IsNum) else
		in_range_of_nums(*num)		if istype(num,tuple) else
		in_range_of_nums(**num)		if istype(num,dict) else
		throw(ValueError(num)) )
	return out
def qualify_on_uses(qlfy_uses=None,on_qlfy=None,on_else=None,if_is=True,qlfy_of_num=cmp_of_num,pa=(),ka=map0):
	''' convert variety of inputs into ->callable qualifier of uses as single arg
		:param qlfy_uses:   conditional(s) check if uses is qualified
		:param on_qlfy:     callback invoked if qlfy_uses WAS satisfied
		:param on_else:     callback invoked if qlfy_uses WAS NOT satisfied
		:param qlfy_of_num: convert any int qlfy_uses into required qualify callable. default is int.__le__
	'''
	# resolve single or multi qualifiers
	qlfy_uses = iqlfy_uses = istype(qlfy_uses, tuple|IsNum) and cmp_of_num(qlfy_uses) or qlfy_uses 
	iqlfy_uses = None if isntiter(qlfy_uses) else [iscx(qlfy_uses,orfab=qlfy_of_num) for qlfy_uses in iqlfy_uses]
	def any_of_iqlfy_uses(uses): 
		did_qlfy = any(qlfy_uses(uses) for qlfy_uses in iqlfy_uses)
		return did_qlfy
	qlfy_uses = qlfy_uses if not iqlfy_uses else any_of_iqlfy_uses
	
	# resolve branching handlers on qualified result
	def did_qlfy_of_uses(uses):
		did_qlfy = bool(qlfy_uses(uses)) == if_is
		did_qlfy and on_qlfy(*pa,**ka)
		not did_qlfy and on_else and on_else(*pa,**ka)
		return did_qlfy	
	staged = did_qlfy_of_uses
	return staged

reuses,uses,reused,unused					= count_use_of,limit_use_of,later_use_of,first_use_of
#endregion

#region tests
def test_limit_use():
	limited = limit_use([
		[3,lambda: print('primary')],
		[2,lambda: print('secondary'),3],
		[1,lambda: print('tertiary'), inf],
	],on_iter=print)
	for i in range(20):
		print(limited(),end=' ')

def trial_run(call,*pa,trial=yes,log=yes,fmt=_A.fmt_call,**ka):
	out = None
	if log:
		s_call = out = fmt.format(call,pa,ka)
		iscall(log,orput=print)(s_call)
	if not trial:
		out = call(*pa,**ka)
	return out

def trial_call(call, *pa, trial=yes, replace=miss, fmt=_A.fmt_call, **ka):
	trial,log = _A.triallog_at.get(trial,_A.triallog_any)
	log and log(sof(fmt, call,',\n  '.join(map(str,('',)+pa)),ka))
	out = call(*pa,**ka) if not trial else replace
	return out

trial_cx					= trial_call
#endregion

#region scratch
'''
is_used			used_once		did_use
not_used		used_never		first_use		unused
times_used		used_total		total_use
until_used		used_total		total_use		tobe_used		till_used

init  entry  setup  prep-are  staging
init_cls _ctx _namespace _root

'''
#endregion
