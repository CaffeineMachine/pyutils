import os
import sys
import re
import shutil
from pathlib import Path, PosixPath, WindowsPath

from pyutils.defs.colldefs import xt, collany
from pyutils.defs.itemdefs import first, Ndx, last
from pyutils.utilities.callbasics import *

#region path defs
P							= Path
P.str,P.src,P.srcs			= property(str),Path.parent, Path.parents
P.sep						= property(lambda path: path._flavour and path._flavour.sep)
Paths,PrePath = PP,PreP		= list[Path], str|Path
ispath,prepath = isp,_		= istypethen(Path), istypethen(PrePath)
path0 = p0 = p_root			= P(sys.platform.startswith('win') and P('~').expanduser().anchor or '/')

_coll						= yes
_i,_ij						= _coll and (iter,ij) or (list,lj)  # collect iterable for debugging visibility

class DefsPathUtil:
	#region scopes
	class Args:
		kk_part				= tss('file  dir  src  ')
		vv_part				=      file, dir, src   = xt(range(3))
		part_at				= dict(zip(kk_part+vv_part,2*vv_part))
	class Strs:
		rx_hdr_pfx			= r'^#+\s+'
		hdr_pfx				= '#'
	AA,SS					= Args,Strs
	#endregion
	...

DDPU = _D					= DefsPathUtil
AAPU,SSPU = _A,_S			= DDPU.AA, DDPU.SS
#endregion

#region path forms
sign_path					= lambda path: ...		# todo

def paths_of(pp:PreP, keep=None, fab=list, **ka):
	''' simple conversion to ->paths for all vals in *pp* prepaths. can optionally filter output via keep 
		:param keep: 
		:param fab: 
	'''
	pp_out = _i(map(P, ss(pp)))
	pp_out = pp_out if not keep else keep_paths(keep,pp_out,**ka)
	pp_out = pp_out if not fab else fab(pp_out)
	return pp_out

def like_paths(likes:PreP, keep=None, evs=Path, ev=None, **ka):
	''' simple conversion to ->paths for all vals in *pp* prepaths. can optionally filter output via keep
		:param likes: glob pattern to find in existing paths. Note: not the same as regex 
		:param keep:  custom condition callable to filter found paths 
		:param evs:   converts each path in output
		:param ev:    converts paths seq output
	'''
	# resolve inputs
	likes = ispath(likes) and [likes] or ss(likes)
	likes = xt(map(abs_path, likes))
	
	# resolve output
	pp = _ij(p_root.glob(P(like).relative_to(p_root).str) for like in likes)
	pp_out = pp if not keep else keep_paths(keep,pp,**ka)
	# pp_out = pp_out if not fabs else map(fabs,pp_out)
	# pp_out = pp_out if not fab else fab(pp_out)
	pp_out = pp_out if not evs and not ev else apply(pp_out, evs=evs,ev=ev)
	return pp_out

def bound_like_path(path:str, fabs=Path,fab=tuple):
	from subprocess import check_output
	
	cmd = f'ls -1 {path!s} | head -n1; ls -1 {path!s} | tail -n1'
	out = check_output(cmd,shell=yes).decode().strip().split('\n')
	out = out if not fabs else map(fabs,out)
	out = out if not fab else fab(out)
	return out

def spath_at(path, at, join=yes, fab=None, **_):
	p = P(path)
	ssp_out = p.str.split(p.sep)[at]
	p_out = ssp_out if not join else p.sep.join(ssp_out)
	p_out = p_out if not fab else fab(p_out)
	return p_out
def bisect_spath(path, at, join=yes, fabs=None, **_):
	# todo: obsolete; part_spath
	p = P(path)
	ssp_out = path.split(p.anchor)[at]
	ss2p_out = ssp_out[at:],ssp_out[:at]
	p_out = ss2p_out if not join else [p.anchor.join(ssp_out) for ssp_out in ss2p_out]
	p_out = p_out if not fabs else list(map(fabs,p_out))
	return p_out

def part_spath(path, part=0, sep='/',at=Ndx.all, fabs=None, **_):
	''' right split path into left & right strs at index *part* 
		:param path: path to split
		:param part: right index part to be the first of right parts  
		:param sep:  path dir separator
		:param at:   selector of (left,right)
		:param fabs: converter of left & right parts
		:param ofs:  todo: the caller may prefer a different part index orientation
		 
		examples given /a/b/c/d:  
		part:0 => '/a/b/c', 'd'
		part:2 => '/a', 'b/c/d'
	'''
	# resolve inputs
	sp = str(path)
	left,*right = sp.rsplit(sep,part+1)
	
	# resolve output
	parts = left,right = left and left+sep, sep.join(right)
	parts = parts if not fabs else tuple(map(fabs,parts))
	parts = parts[at]
	return parts

def sign_path_tmod(path:P, fill=miss, **_):
	try:
		out = Path(path).stat().st_mtime
	except Exception:
		if fill is miss:	raise
		out = fill
	return out

def abs_path(*dir_path:PP,dir=None,fab=None, **_):
	dir,path = len(dir_path) == 2 and dir_path or (dir,*dir_path)
	pp = Path(path).expanduser()
	pp = pp if not fab else fab(pp)
	return pp

def okay_path(path:P, fill=None, err=no, maxlen=200, fab=bool, **_):
	''' validate given *path*
		:param path: path to validate
		:param fill: invalid path substitution
		:param err: invalid path handler
		:param maxlen: path len upper bound
	'''
	# fixme: let err handle output for bad files
	err = istrue(err,put=FileNotFoundError)
	p_out = (
		path			if not path or istype(path,Path) else
		P(path)			if isstr(path) else
		throw(path)  )
	p_out = (
		p_out			if p_out and p_out.exists() else
		fill			if not err else
		throw(sof('{1}{0!s}', p_out,len(str(path))>maxlen and f'over maxlen={maxlen}: ' or ''),exc_fab=err) )
	p_out = p_out if not fab else fab(p_out)
	return p_out
def okay_paths(paths:PP, *pa,fabs=None,fab=list,**ka):
	''' validate given *paths*
		:param paths: paths to validate
		:param fill: invalid path substitution
		:param err: invalid path handler
		:param maxlen: path len upper bound
	'''
	# fixme: support grouping into ok|err|other
	are_ok = (okay_path(p,*pa,**ka) for p in paths)
	are_ok = are_ok if not fabs else map(fabs,are_ok)
	are_ok = are_ok if not fab else fab(are_ok)
	return are_ok

def header_of_path(path:P, ch_pfx=True,fab=None, **_):
	# resolve inputs
	hdr_pfx = istrue(ch_pfx,put=_S.hdr_pfx)
	path_in,path = path,okay_path(path) and path or first(paths_of_globs(path),miss)
	path is miss and throw(f'failed to find path like \'{path_in!s}\'')
	
	# resolve output
	with open(path) as f:
		hdr = f.readline()
	hdr_out = hdr if not hdr_pfx else hdr.replace(hdr_pfx,'').strip()
	hdr_out = hdr_out if not fab else fab(hdr_out)
	return hdr_out

def deltas_of_paths(paths:PP, sigs=None, to_sig=sign_path_tmod,state=None, **_):
	paths = collany(paths)
	sigs_prev = sigs or state and state.get('sigs') or rep(None)
	sigs_curr = tuple(map(to_sig,paths))
	state is not None and state.__setitem__('sigs',sigs_curr)
	
	deltas = [(p,curr,prev) for p,curr,prev in zip(paths,sigs_curr,sigs_prev) if prev and prev != curr]
	return deltas,state

def less_path(path, n=1,jx_sub='/', romits=(), intos=map0, fill='', err=yes, fab=yes, **_):
	# resolve limited
	p_out = jx_sub.join(str(path).rsplit(jx_sub,n)[1:])
	for romit in romits:
		p_out = p_out.endswith(romit) and p_out[:-len(romit)] or p_out
		p_out = intos.get(p_out,p_out)
	
	# resolve output
	assert not err or p_out, p_out
	p_out = p_out if not fab else isyes(fab,put=P)(p_out)
	return p_out
def less_paths(paths, *pa, **ka):
	raise NotImplementedError()

pp_of						= paths_of
like_pp = iglobs			= like_paths
sp_at						= spath_at
path_at = p_at				= redef(spath_at, fab=Path)
bisect_path = bisect_p		= redef(bisect_spath, fabs=Path)

partl_sp = src_sp			= redef(part_spath, at=0)
partr_sp = sub_sp			= redef(part_spath, at=1)
partl_p = src_p				= redef(part_spath, fabs=Path, at=0)
partr_p = sub_p				= redef(part_spath, fabs=Path, at=1)
part_p						= redef(part_spath, fabs=Path)

sig_sum_of_p,tmod_of_p		= sign_path, sign_path_tmod
okay_p,okay_pp				= okay_path,okay_paths
warn_path,warn_paths		= redef(okay_path,err=yes), redef(okay_paths,err=yes)
warn_p,warn_pp				= warn_path,warn_paths
missing_path = miss_p		= lambda p: not okay_p(p)  # todo: lazy solution, use redef(okay_p)
missing_paths = miss_pp		= lambda pp: [untruth(ok_p) for ok_p in okay_pp(pp)]  # todo: lazy solution, use redef(okay_pp)

le_p,le_pp					= less_path,less_paths
#endregion

#region path utils
def _ijoin(seqs):
	for seq in seqs:
		yield from seq

def _iascend_path(sub:P, top:P=None, ends=(yes,no), fabs=None, **_):
	if fabs: 
		yield from map(fabs, iascend_path(sub,top,ends))
		return
	
	# traverse paths higher
	path = P(sub)
	yield path
	while path.parent:
		path = path.parent
		yield path.parent
def iascend_path(sub:P, top:P=p_root, ends=(yes,no), fabs=None, **_):
	if fabs: 
		yield from map(fabs, iascend_path(sub,top,ends))
		return
	
	# traverse higher from sub to top
	is_top = (
		top					if iscall(top) else
		P(top).__eq__		if prepath(top) else
		never )
	path = ends[Ndx.first] and P(sub) or P(sub).parent
	while path and path!=path.parent and not is_top(path):
		yield path
		path = path.parent
	if ends[Ndx.last]:
		yield path

def okay_path_parts(path, path_sep='/', **_):
	part_ptrn = r'[^{sep}]*{sep}+|[^{sep}]+{sep}*'.format(sep=path_sep)
	parts = re.findall(part_ptrn, path)
	return parts

def trace_path(path, **_):
	''' frace filesystem path from root down to target suppath '''
	path_parts = okay_path_parts(path)
	return accumstr(path_parts)  # fixme: sourced from iterutils

def retrace_path(path, **_):
	''' refrace/ascend filesystem path from target path up to root '''
	while True:
		yield path
		path = re.sub(r'([^/]*/+|[^/]+/*)$', '', path)
		if path == '':
			break

def walk_path(path:P,up=True,fab=None,fabs=P, **_):
	pp_out = iascend_path(path)
	pp_out = pp_out if up else reversed(list(pp_out))
	pp_out = pp_out if not fabs else map(fabs,pp_out)
	pp_out = pp_out if not fab else fab(pp_out)
	return pp_out

def into_path(path:P,ext=None,fab=None,on_delta=None):
	''' create new ->path by modifying parts of given *path* '''
	p_out = Path(path)
	p_out = p_out if not ext else p_out.with_suffix('.'+ext)
	p_out = p_out if not fab else fab(p_out)
	return p_out
def alter_path(on_alter,path:P,**ka):
	pp_io = into_path(path)
	on_alter(*ss(pp_io),**ka)
	return pp_io
def copy_path(p_in:P,p_to=None,trial=False,**ka):
	from pyutils.utilities.callutils import trial_run
	pp_io = p_in, p_to or into_path(p_in)
	trial_run(shutil.copy,*ss(pp_io), **ka, trial=trial)
	return pp_io
def move_path(path:P,**ka):
	pp_io = into_path(path)
	shutil.move(*ss(pp_io),**ka)
	return pp_io

def make_dir(path:P, mode=0o777, parents=yes, exist_ok=yes, **ka):
	return _path_mkdir_orig(path, parents=parents, exist_ok=exist_ok, **ka)
def make_path(path:P, part=_A.file, **ka):
	''' make path via mkdir-parent/mkdir/touch given part as 2/1/0 resp. '''
	# resolve inputs
	ka_dir,ka = dict(parents=ka.pop('parents',yes)), dict(exist_ok=yes)|ka  # correct defaults for lazy people
	p,n_part = P(path), _A.part_at.get(part,part)
	
	# create path parts
	n_part and src_p(p,n_part-1).mkdir(**ka|ka_dir)
	n_part==_A.file and p.touch(**ka)
	return p

def write(path,text,mode='w',*pa,over=yes,mkdir=yes,log=yes,trial=no,**ka):
	''' helper with common functionalities for writing to file
		:param path:  path to be written
		:param text:  text to write. is first converted to str.
		:param mode:  open mode
		:param over:  permit saving over existing files when True
		:param mkdir: create missing parent dirs when True
		:param log:   1/2 show text as len/str. todo: transition to named flags & accept flags as str keys
		:param trial: will not make changes to filesystem when True
	'''
	# resolve inputs
	log,nlog = isint(log,put=log and print), isint(log,orfab=bool)
	p,p_le,s_text = P(path), le_p(path), str(text)
	s_nlog = nlog==1 and f', size={len(s_text)}' or nlog==2 and f', text={s_text!s}' or ''
	can_write = over or not okay_p(p)
	
	# write file|log when permitted
	if not can_write:
		log and log('not writing to {!s}{}'.format(p_le,s_nlog), file=sys.stderr)
	else:
		# trial != no and log and log('write(mode={0!r}{2}, p={1!s}){3}'.format(mode,p_le,*[s_nlog,''][::nlog==1 and 1 or -1]))
		log and log('write(mode={0!r}{2}, p={1!s}){3}'.format(mode,p_le,*[s_nlog,''][::nlog==1 and 1 or -1]))
		if trial != yes:
			mkdir and make_path(p,_A.dir)
			with p.open(mode) as f:
				f.write(text)
	return can_write

iascend_spath = iasc_sp		= redef(iascend_path, fabs=str)
_ascends					= hascend_path,lascend_path,tascend_path,hascend_spath,lascend_spath,tascend_spath,   = (
							  hasc_p,      lasc_p,      tasc_p,      hasc_sp,      lasc_sp,      tasc_sp,       ) = (
							  lj(redef_per_coll(fx,colls=DDColl.hlt) for fx in [iascend_path,iascend_spath]) )
ascend_path = asc_p			= lascend_path
ascend_spath = asc_sp		= lascend_spath
cp_path,mv_path				= copy_path, move_path
_path_mkdir_orig			= Path.mkdir
P.make_path = P.mk_p = mk_p	= make_path
P.mkdir						= make_dir
#endregion

#region prepath utils
def keep_paths(like, paths:PP=miss, *, incl:Fc|bool=yes, to_val=None, evs=None, ev=None, **_):
	''' keep subset of *paths* where *incl* & *like* bool & qualifier are satisfied
		:param like:   qualifier of eac path
		:param paths:  optional paths to check. if paths is miss then stage a func to check paths given like
		:param incl:   include if is like 
		:param to_val: converts each path for input to like
		:param evs:    converts each path in output
		:param ev:     converts paths seq output
	'''
	# resolve inputs
	like_in,like,to_val = (like,like,to_val) if iscx(like) else (like,re.compile(like).search,str)
	incl = truth_at.get(incl,incl)
	
	# resolve output
	def keep_of_paths(paths:PP):
		pp = paths if not to_val else map(to_val,paths)
		pp_out = (p for p in pp if incl(like(p)))
		# pp_out = pp_out if not fabs else map(fabs,pp_out)
		# pp_out = pp_out if not fab else fab(pp_out)
		pp_out = pp_out if not evs and not ev else apply(pp_out, evs=evs,ev=ev)
		return pp_out
	stage = paths is None and keep_of_paths or keep_of_paths(paths)
	return stage

def fmt_path(fmt:str,path:P=miss):
	fmt_in,fmt = fmt,isstr(fmt,var='format')
	def fmt_of_path(path):
		p_out = fmt(path.str)
		return p_out
	fmt_of_path.__name__ = fmt_of_path.__qualname__ = '{}({})'.format(fmt_path.__name__, fmt_in)
	stage = path is miss and fmt_of_path or fmt_of_path(path)
	return stage
#endregion

#region devcs
class PathLess(Path):
	#region defs
	_n_parts				= 2
	_omits					= None
	_str_le					= None
	_flavour				= os.name=='nt' and WindowsPath._flavour or PosixPath._flavour
	#endregion
	#region forms
	def __init_subclass__(cls, n_parts=_n_parts, omits=_omits, **ka):
		cls._n_parts = n_parts
		cls._omits = omits
		super().__init_subclass__(**ka)
	def __new__(cls,*_pa, n_parts=None, omits=None, **_ka):
		''' truncates path name via less_path()
			:param n_parts: 
			:param omits: 
		'''
		# resolve inputs
		n_parts,_pa = (_pa and isint(_pa[0])) and (_pa[0],_pa[1:]) or (n_parts,_pa)
		if not (_pa or _ka):
			return lambda *pa,**ka: cls(*pa,**dict(n_parts=n_parts,omits=omits)|ka)   
		
		# resolve output
		p = super().__new__(cls, *_pa, **_ka)
		p.omits = omits or p._omits
		p.n_parts = n_parts or p._n_parts
		return p
	def __repr__(self):
		self._str_le = self._str_le or less_path(super().__str__(), n=self.n_parts, omits=self.omits, fab=no)
		return self._str_le
	__str__					= __repr__
	#endregion
	...
Ple							= PathLess
#endregion
