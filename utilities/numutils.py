from pyutils.defs.chardefs import s_hex_lower, s_num_std
from pyutils.defs.colldefs import collany
from pyutils.defs.numdefs import *
from pyutils.utilities.callbasics import *

#region num forms
# fixme: lacking a crystal clear distinction between lohi & bound; perhaps ... 
#        - lohi is 2 args and bound is 1 pair
#        - lohi defaults are (0,0) and bound default are -abs(val),+abs(val)
Num = N							= int
IsNN							= list[N]
perc_of_flt = percof			= lambda flt,denom=1.: flt / denom * 100.
percd_of_flt = percdof			= lambda flt,denom=1.: (flt - denom) / denom * 100.
flt_of_perc = ofperc			= lambda perc: perc * .01
compliment = cplmt 				= lambda flt: 1. - flt
reciprocal = recip 				= lambda flt: 1. / flt
pole_of							= lambda num: (num > 0. and posv) or (num < 0. and negv) or neut
poleabs_of						= lambda num: num > 0. and (posv,num) or num < 0. and (negv,-num) or (neut,num)
_atlo,_athi						= 0,1

def lohi_any(*pa, bound=None, lo=miss, hi=miss, **_):
	''' resolve =>lo,hi from varied inputs
		:param bound: 
		:param lo: 
		:param hi: 
	'''
	pa = (pa_in := pa) and (oftype(pa[0],IsNum) and pa or pa[0])
	lo,hi,*_ = (
		bound						if bound else
		pa							if pa and len(pa)>1 else
		(-abs(pa[0]),abs(pa[0]))	if pa else
		(f_min,f_max)				if lo is hi is miss else
		(lo,hi) )
	lo = lo if lo is not miss else 0. if 0.<=hi else f_max
	hi = hi if hi is not miss else 0. if lo<=0. else f_max 
	return lo,hi

def bound_any(*_bound, bound=None, start=None, stop=None, fab=None):
	''' resolve =>lo,hi from varied inputs
		:param bound: 
		:param start: 
		:param stop: 
		:param fab: 
	'''
	# todo: refactor
	bound = _bound and (isiter(_bound[0]) and _bound[0] or _bound) or bound
	bound = (
		(0,0,*bound)[-2:]			if isiter(bound) else
		(bound.start,bound.stop)	if istype(bound,slice) else
		(start or 0,bound)			if bound is not None else
		(start or 0,stop) )
	bound = bound if not fab else fab(bound)
	return bound

def lohi_of_bound(bound,less=None):
	# todo: refactor
	lo,hi,less = (
		(*bound,      isnone(less,put=True))		if isiter(bound) else
		(-bound,bound,isnone(less,put=False))		if 0 <= bound else
		(bound,-bound,isnone(less,put=True)) )
	return lo,hi,less

def boundpa_of_nums(nums, nums_hi=None, nums_lo=None, to_lohi=False, fab=False):
	# todo: refactor
	nums_lo,nums_hi = nums_lo or nums, nums_hi or nums
	bound = min(nums_lo), max(nums_hi)
	bound = bound if not fab else fab(bound)
	return bound
def bound_of_nums(*pa, fab=True, **ka):
	# todo: refactor
	return boundpa_of_nums(*pa,**ka, fab=fab)

def rep_num(chs,num):
	''' translate number into given chars '''
	n_chs = len(chs)
	ich = [chs[num % n_chs]]
	while num and num != -1:
		num //= n_chs
		ich += chs[num % n_chs]
	s_out = ''.join(ich)
	return s_out

bn_any							= bound_any
str_of_num = s_of_n				= rep_num
shex_of_n						= rep_num.__get__(s_hex_lower)
s36_of_n						= rep_num.__get__(s_num_std + s_lower)
s62_of_n						= rep_num.__get__(s_num_std + s_lower + s_upper)
#endregion

#region num utils
is_positive = is_posv=isposv	= lambda val, abs_min=0.: val >= -abs(abs_min)
is_negative = is_negv=isnegv	= lambda val, abs_min=0.: val <= abs(abs_min)
is_neutral = is_neut=isneut		= lambda val, abs_min=0.: val == abs(abs_min)

def inum(step=None, start=0, stop=None, steps=None, stop_end=True, end=False, fabs=None):
	'''
		:param step:
		:param start:
		:param stop:
		:param steps:
		:param stop_end: True:num == stop is exit cond (like range()). False:stop is last num. length remains constant
		:param end:      append end as last num
	'''
	if fabs:
		yield from map(fabs,inum_unsafe(step=step,start=start,stop=stop,steps=steps,stop_end=stop_end,end=end))
		return
	
	# resolve inputs
	if stop is step is steps is None:
		step = 1
	elif step is None and stop is not None and steps is not None:
		step = (stop-start) / (steps-(not stop_end))
	elif steps is None and stop is not None and step is not None:
		steps = (stop-start) / step
	
	# generate values
	ind,num = 0,start
	while steps is None or ind < steps:
		yield num
		num += step
		ind += 1
	if end:
		yield num if stop is None else stop
def primes_of(n, fab=None):
	''' get prime factors of integer *n*;  Note: this has no optimization '''
	prime = 2
	primes = []
	while prime * prime <= n:
		if n % prime:
			prime += 1
		else:
			n //= prime
			primes.append(prime)
	if n > 1:
		primes.append(n)
	nn_primes = primes if not fab else fab(primes)
	return nn_primes

def nth_in(nth, start, stop, step, fill=miss):
	''' produce ->N as *nth* in range given as *start*,*stop*,*step* '''
	size = max(0,step and (stop-start) // step or 0)
	out = (
		fill								if (isntint(nth) or not -size < nth < size) and fill is not miss else
		min(nth*step + start, stop)			if nth >= 0 else
		max(nth*step + stop,  start)		if nth <  0 else
		throw(IndexError(nth)) )
	return out
def nths_in(idx:IsIdx, *_nn:IsNN,nn:IsNN=None,fab=None,**ka):
	''' produce ->N|NN from slice of *nn* range given *idx* '''
	# resolve inputs
	nn = nn or _nn
	step = nn[2]
	is_multi,step_idx,*nn_idx = (
		(no,     None, idx)					if isint(idx) else
		(on, idx.step, idx.start,idx.stop)	if istype(idx,slice) else
		throw(idx) )
	step = step and step_idx and step*step_idx or step or step_idx or 1
	
	# resolve output
	out = *(nth_in(n_idx,*nn[:3],fill=n) for n_idx,n in zip(nn_idx,nn)), step
	out = out if is_multi else out[0]
	out = out if not fab else fab(out)
	return out

def polar_of(val,polar_b=None):		return (val > 0 and posv) or (val < 0 and negv) or (polar_b or val)
def polarized(va,vb):				return bool(va <= 0 <= vb or va >= 0 >= vb and (va or vb))

def quantify(value:float, null_below=10.**-12, prec=None) ->float:
	''' bring value to 0. if negligible. otherwise return value unmodified; syn: least '''
	# todo: consider atleast
	null_below = 10.**-prec if prec else null_below
	val = 0. if null_below > value > -null_below else value
	return val

nth_nums,slice_nums				= nth_in, nths_in
inum,_ = inum_safe,inum_unsafe	= isafer(inum),inum
hnum,lnum,tnum					= redef_per_fab(inum,colls=DDColl.hlt)
zeroish = quantize				= quantify
nth_nn,nn_at					= nth_nums,slice_nums
#endregion

#region compare utils
# fixme: redundancies abound
is_below						= lambda v, hi,    eq=no:      -hi<v<hi or hi<v<-hi
is_above						= lambda v, lo,    eq=no:  not(-lo<v<lo or lo<v<-lo)
is_less							= lambda v, bound, eq=no:      bound[0]<v<bound[1]
is_more							= lambda v, bound, eq=no:  not bound[0]<v<bound[1]
are_below						= lambda vv,his,   eq=no: [    -hi<v<hi or hi<v<-hi for v,hi in zip(vv,his)]
are_above						= lambda vv,los,   eq=no: [not(-lo<v<lo or lo<v<-lo) for v,lo in zip(vv,los)]
are_less						= lambda vv,bounds,eq=no: [    b[0]<v<b[1] for v,b in zip(vv,bounds)]
are_more						= lambda vv,bounds,eq=no: [not b[0]<v<b[1] for v,b in zip(vv,bounds)]

inclusive_lohi  = incl_lohi		= lambda lo,hi,val,eq=yes:      lo<val<hi or (eq and lo==val|hi==val)
exclusive_lohi  = excl_lohi		= lambda lo,hi,val,eq=yes: not (lo<val<hi or (eq and lo==val|hi==val))
inclusive_bound = incl_bound	= lambda bound,val,eq=yes: incl_lohi(*bound,val,eq=eq)
exclusive_bound = excl_bound	= lambda bound,val,eq=yes: excl_lohi(*bound,val,eq=eq)

smaller							= lambda small,val: val <  -small or small <  val
smallest						= lambda small,val: val <= -small or small <= val
larger							= lambda large,val: -large <  val <  large
largest							= lambda large,val: -large <= val <= large

above							= lambda lo, *vals: max(lo, *vals)
below							= lambda hi, *vals: min(hi, *vals)
atleast							= lambda lo, *vals: max(lo, min(vals))
atmost							= lambda hi, *vals: min(hi, max(vals))
atleast_zero = atleast0			= lambda val:  val >  0. and val  or 0.
atmost_zero = atmost0			= lambda val:  val <  0. and val  or 0.

into_lohi						= lambda lo,hi,val: min(max(lo, val), hi)
into_bound						= lambda bound,val: min(max(bound[0], val), bound[1])
into_norm						= lambda val: (val > +1. and +1.) or (val < -1. and -1.) or val
into_unit						= lambda val: (val > +1. and +1.) or (val >  0. and val) or 0.
into_unitneg					= lambda val: (val < -1. and -1.) or (val <  0. and val) or 0.

cmp_vv_of						= lambda cmps,vs: all(cmp(v) for cmp,v in zip(cmps,vs))
cmp_of							= lambda cmp,to_cmp=smallest: (
									cmp						if iscx(cmp) else
									getattr(cmp,to_cmp)		if isstr(to_cmp) else
									to_cmp.__get__(cmp) )

into_n_z,into_z_p				= atmost_zero, atleast_zero
into_n1_p1,into_z_p1,into_n1_z	= into_norm, into_unit, into_unitneg
lohi_z_p, lohi_z_p1, lohi_n1_p1, lohi_n1_z, lohi_n_z = (
into_z_p, into_z_p1, into_n1_p1, into_n1_z, into_n_z, )
#endregion

#region bound utils
# fixme: redundancies abound
def compare(left, right=0):
	''' compare size of *left* to *right*. when *left* relative to *right* is <,==,> returns -1,0,+1 respectively. '''
	out = (
		zero	if left == right else
		posv	if left > right else
		negv	)
	return out

def is_bound(*pa, eq=yes, incl=yes, val=miss, **ka):
	''' stage or resolve check for *val* within *bound*. check result is compared to *incl* for output
		:param bound: 
		:param lo:    
		:param hi:    
		:param eq:    
		:param incl:  
		:param val:   
	'''
	# resolve inputs
	lo,hi = lohi_any(*pa, **ka)
	eq_lo,eq_hi = eq==yes and (yes,yes) or eq
	
	# resolve output
	def eval(val):
		incl_out = (_cmp := lo < val < hi) or (eq_lo & (lo==val) or eq_hi & (hi==val))
		return incl == incl_out
	name = val is miss and  'is_{}bound({},{})'.format('a'[incl:],str(lo)[:6],str(hi)[:6]) or '_'
	eval.__qualname__ = name
	stage = val is miss and eval or eval(val)
	return stage
def are_bound(bounds=None, vv=None, *, eq=no, iless=None,to_vv=None,to_has=all):
	''' stage or resolve check for *val* within *bounds*. check result is compared to *incl* for output
		:param bounds:
		:param vv:
		:param eq:     consider equivalence to low and high as in_bound when True. otherwise in_bound is strictly between
		:param iless:
		:param to_vv:  coerce to flattened seq from given input bounds and vals
		:param to_has:
	'''
	# todo: refactor
	# resolve inputs
	bounds = tuple(to_vv(bounds))
	lows,highs,iless = zip(*map(lohi_of_bound, bounds, iless or rep(None)))
	
	# resolve output
	def are_bound_of_vv(vv):
		vv = vv if not to_vv else to_vv(vv)
		has = ((not (lo or hi)) or less == (lo<v<hi) for v,lo,hi,less in zip(vv,lows,highs,iless))
		has_out = has if not to_has else to_has(has)
		return has_out
	out = vv is None and are_bound_of_vv or are_bound_of_vv(vv)
	return out
def has_of_bound(bound,nums=None,incl=None,is_iter=None,to_num=None):
	''' get ->has() for given bound.  has = lambda num: num in bound '''
	# fixme: isnt this the same as are_bound
	# resolve inputs
	incl = isnone(incl, put=getattr(bound,'incl',Incl.default))  # incl prio: **incl, bound.incl, Incl.default
	incl_lo,incl_hi = Incl.lohi_of_incl(incl)
	lo,hi = bound_any(bound)
	
	def eval(nums):
		''' coerce *nums* to iter from single or iterable before passing to ieval(iter) '''
		def ieval(inum):
			inum = inum if not to_num else map(to_num,inum)
			for num in inum:
				val = lo < num < hi or (incl_lo and num == lo) or (incl_hi and num == hi)
				yield val
		
		# resolve output:  nums => inum,ieval(inum)[: or 0]
		inum,from_iter = (		# from_iter = is_iter; unless is_iter is None then check for nums.__iter__
			(nums,True)			if is_iter is True else
			((nums,),False)		if is_iter is False else
			ifromany(nums) )
		out = from_iter and ieval(inum) or next(ieval(inum))  # get single|plural output depending on input
		return out
	
	# resolve output
	out = nums is None and eval or eval(nums)
	return out

def lohi_for(vv, at=None, lo=on, hi=on, to_lo=None, to_hi=None, enum=no, **ka):
	''' get bound (or low,high) of *nums* in one pass '''
	# resolve inputs: the magic happens here by defining the vv iterable of comparables
	assert lo or hi, f'you must choose one or both: {lo=} {hi=}'
	iv = vv if not enum else enumerate(vv)
	iv = (
		iter(iv)						if at is None else
		((at(v),v)   for   v in iv)		if not enum and iscall(at) else
		((v[at],v)   for   v in iv)		if not enum else
		((at(v),v,n) for n,v in iv)		if     enum and iscall(at) else
		((v[at],v,n) for n,v in iv) )	#if    enum and not iscall(at) else
	
	# iterate iv for low & high
	lo_out=hi_out = next(iv)  # use first to initialize
	for v in iv:
		if   lo_out and lo_out > v:
			lo_out = v
		elif hi_out and hi_out < v:
			hi_out = v
	
	# resolve output
	lo_out = lo_out if to_lo is None else to_lo(lo_out) if iscall(to_lo) else lo_out+to_lo
	hi_out = hi_out if to_hi is None else to_hi(hi_out) if iscall(to_hi) else hi_out+to_hi
	lohi_out = (lo_out,hi_out) if lo and hi else lo_out if lo else hi_out
	lohi_out = lohi_out if not ka else apply(lohi_out,**ka)
	return lohi_out
def lohis_for(vvs, at=None, lo=on, hi=on, to_item=no, enum=no, **ka):
	''' get multiple lows,highs (or bounds) of *vvs* in one pass '''
	# resolve inputs
	assert lo or hi, f'you must choose one or both: {lo=} {hi=}'
	# ivv = iter(vvs) if not ats else ((vv[at] for at in ats) for vv in vvs)	# todo: add arg bounds(idx) as seen above
	# ivv = vvs if not enum else zip(vvs,range(n_max))
	ivv = zip(vvs,range(n_max))
	ivv = (
		iter(ivv)						if at is None else
		# ((at(v),v)   for v   in ivv)	if not enum and iscall(at) else
		# ((v[at],v)   for v   in ivv)	if not enum else
		((at(v),v,n) for v,n in ivv)	if     enum and iscall(at) else
		((v[at],v,n) for v,n in ivv)	#if    enum and not iscall(at) else
	)
	# vvs_out = list(ivv)
	# ivv = iter(vvs_out)
	
	# iterate inums for low & highs
	vv0 = next(ivv)
	lohis_out = [[(v,*vv0[1:]),(v,*vv0[1:])] for v in vv0[0]]  # use first to initialize
	for vv,*pa in ivv:
		for bound,v in zip(lohis_out,vv):
			if lo and bound[_atlo][0] > v:
				bound[_atlo] = v,*pa
			elif hi and bound[_athi][0] < v:
				bound[_athi] = v,*pa
	
	# resolve output
	lohis_out = lohis_out if lo and hi else [bound[_atlo if lo else _athi] for bound in lohis_out]
	lohis_out = lohis_out if not to_item else (to_item is True and zip or to_item)(at,lohis_out)
	lohis_out = lohis_out if not ka else apply(lohis_out,**ka)
	return lohis_out
def low_for(*pa,**ka):		return lohi_for (*pa,**ka, hi=no)
def lows_for(*pa,**ka):		return lohis_for(*pa,**ka, hi=no)
def high_for(*pa,**ka):		return lohi_for (*pa,**ka, lo=no)
def highs_for(*pa,**ka):	return lohis_for(*pa,**ka, lo=no)
def most_for(vv, *pa, signed=no, **ka):
	''' find largest absolute quantity in *vv* values '''
	lo,hi = lohi_for(vv,*pa,**ka, lo=on, hi=on)
	if istuple(lo):
		most_out = lo if abs(lo[0]) > abs(hi[0]) else hi
		most_out = most_out if signed else (abs(most_out[0]),*most_out[1:])
	else:
		most_out = lo if abs(lo) > abs(hi) else hi
		most_out = most_out if signed else abs(most_out)
	return most_out
def mosts_for(*pa,signed=no,fab=None,**ka):
	''' find largest absolute quantities in *vvs* value sets '''
	bounds_out = lohis_for(*pa,**ka)
	mosts_out = (most_for(bn, signed=signed) for bn in bounds_out)
	mosts_out = mosts_out if not fab else fab(mosts_out)
	return mosts_out
def bound_index_for(vv, step=None, at=None, to_lo=None, to_hi=None, **ka):
	''' produce num range from bound of absolute quantities in *vv* values '''
	lohi = lohi_for(vv, at=at, to_lo=to_lo, to_hi=to_hi)
	idx_out = lohi + collany(step)
	idx_out = idx_out if not ka else apply(idx_out,**ka)
	return idx_out

cmp								= compare
is_abound = unbound				= redef(is_bound, incl=no)
isbn,isabn = _,unbn				= is_bound,is_abound
lo_for,los_for,hi_for,his_for	= low_for, lows_for, high_for, highs_for
#endregion

#region vector utils
# todo: update names to show that these are vector operations; a vector is similar but distinct from a bound

def scale_to_vector(*pa, vec=None, start=None, step=None, to_vec=None,to_val=None,ev=None, val=miss):
	''' apply vector transform to val
		:param vec:    transform as translation & scalar 
		:param start:  vector translation
		:param step:   vector scalar; usually 1 or None->1 
		:param to_vec: converter to vector transform
		:param to_val: converter to val input(s)
		:param ev:     converter to final output
		:param val:    value(s) to be translated
	'''
		# resolve inputs
	if val is miss and len(pa) == 3:
		*pa,val = pa
	if start is None:
		vec = vec or (len(pa)==2 and pa) or pa[0]
	start,step = vec if not to_vec else to_vec(vec)
	
	# resolve output
	def of(val):
		val = val if not to_val else to_val(val)
		out = (
			val*step + start	if isntiter(val) else
			(v *step + start	for v in val) )
		out = out if not ev else ev(out)
		return out
	stage = val is miss and of or of(val)
	return stage
def reduce_to_vector(*pa, vec=None, start=None, step=None, to_vec=None,to_val=None,ev=None, val=miss):
	''' invert vector transform on val
		:param vec:    transform as translation & scalar 
		:param start:  vector translation
		:param step:   vector scalar; usually 1 or None->1 
		:param to_vec: converter to vector transform
		:param to_val: converter to val input(s)
		:param ev:     converter to final output
		:param val:    value(s) to be translated
	'''
	# resolve inputs
	if val is miss and len(pa) == 3:
		*pa,val = pa
	if start is None:
		vec = vec or (len(pa)==2 and pa) or pa[0]
	start,step = vec if not to_vec else to_vec(vec)
	
	# resolve output
	def of(val):
		val = val if not to_val else to_val(val)
		out = (
			(val-start)/step	if isntiter(val) else
			((v -start)/step	for v in val) )
		out = out if not ev else ev(out)
		return out
	stage = val is miss and of or of(val)
	return stage

def project_of_bound(bound,nums=None):
	''' get factor product of 1 or more *nums* onto range *bound* '''
	def ieval(nums):
		# resolve inputs
		start,stop = bound
		for n_factor in nums:
			n_prod = (start * (1.-n_factor)) + (stop * n_factor)
			yield n_prod
	
	# prepare next stage
	out = (
		project_of_bound.__get__(bound)		if nums is None else
		ieval(nums)							if isiter(nums) else
		next(ieval([nums])) )
	return out
def reflect_of_bound(bound,nums=None):
	''' get compliment factor product of 1 or more *nums* onto range *bound*. where num_compliment is 1.-num '''
	def ieval(nums):
		# resolve inputs
		start,stop = bound
		for n_compl in nums:
			n_prod = (stop * (1.-n_compl)) + (start * n_compl)
			yield n_prod
	
	# prepare next stage
	out = (
		reflect_of_bound.__get__(bound)		if nums is None else
		ieval(nums)							if isiter(nums) else
		next(ieval([nums])) )
	return out
def factor_of_bound(bound,nums=None):
	''' get unit factor of 1 or more *nums* into range *bound* '''
	# factor quantize approximate normalize benchmark measure relate grade unitize intersect product reduce
	def ieval(nums):
		# resolve inputs
		start,stop = bound
		delta = stop - start
		for num in nums:
			factor = (num-start) / delta
			yield factor
		
	# prepare next stage
	out = (
		factor_of_bound.__get__(bound)		if nums is None else
		ieval(nums)							if isiter(nums) else
		next(ieval([nums])) )
	return out
def mid_of_bound(bound=None,start=None,stop=None):
	start,stop = bound
	if stop is None:
		start,stop,*_ = bound or start
	return (stop-start)/2. + start

def convert_of_bounds(bounds=None,nums=None,bound_in=None,bound_to=None,fab=None):
	''' get product conversion of 1 or more *nums* between ranges *bounds_into*|*bound_in*&*bound_to* '''
	bb_into = tuple(map(bound_any,bounds or (bound_in,bound_to)))
	def ieval(nums):
		# resolve inputs
		(start_in,stop_in), (start_to,stop_to) = bb_into
		delta_in,delta_to =  stop_in-start_in, stop_to-start_to
		for n_prod_in in nums:
			n_factor = (n_prod_in-start_in) / delta_in
			n_prod_to = start_to + (delta_to * n_factor)
			yield n_prod_to
	
	# prepare next stage
	out = (
		convert_of_bounds.__get__(bb_into)	if nums is None else
		ieval(nums)							if isiter(nums) else
		next(ieval([nums])) )
	out = out if not fab else fab(out)
	return out

scale_into,reduce_into		= scale_to_vector, reduce_to_vector
#endregion

#region bit utils
def shift_bits(shift:int, val=miss):
	''' shift bits by any increment '''
	out = (
		val << shift	if shift >= 0 else
		val >> -shift )
	return out

def count_ones(num:int):
	''' count bits in num using only bitwise operations
		source: https://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel
		source: https://stackoverflow.com/questions/3815165/how-to-implement-bitcount-using-only-bitwise-operators
	'''
	n_bits = num - ((num >> 1) & 0x5555555555555555)
	n_bits = ((n_bits >>  2) & 0x3333333333333333) + (n_bits & 0x3333333333333333)
	n_bits = ((n_bits >>  4) + n_bits) & 0x0f0f0f0f0f0f0f0f
	n_bits = ((n_bits >>  8) + n_bits) & 0x00ff00ff00ff00ff
	n_bits = ((n_bits >> 16) + n_bits) & 0x0000ffff0000ffff
	n_bits = ((n_bits >> 32) + n_bits) & 0x00000000ffffffff
	return n_bits

def enum_bits(stop=64, start=None, step=None, num=1, evs=None, ev=tuple):
	''' produce a bit enum sequence
		:param stop:  
		:param start: 
		:param step:  
		:param num:   
		:param evs:   
		:param ev:    
	'''
	bb = (num << shift for shift in range(n_max)[start:stop:step])  # note: slice is called on range; not the iter 
	bb = bb if not evs else map(evs,bb)
	bb = bb if not ev else ev(bb)
	return bb

def name_bits(names, bitseq=None, rev=no, ev=yes, **ka):
	''' ...
		:param names: 
		:param bitseq: 
		:param rev: 
		:param ev: 
	'''
	bitseq = bitseq or enum_bits(ev=None)
	kkbits = zip( *(ss(names),bitseq)[::-rev or 1] )
	kkbits = apply(kkbits, ev=isyes(ev,put=tuple), **ka)
	return kkbits
def repr_of_bits(names, bitseq=None, join='|', to_str=None, **ka):
	''' ...
		:param names: 
		:param bitseq: 
		:param join: 
		:param to_str: 
	'''
	kkbit = name_bits(names, bitseq)
	def rep_bits(bits):
		ss_bits = (key for key,nth_bit in kkbit if bits & nth_bit)
		ss_bits = ss_bits if to_str else map(to_str,ss_bits)
		s_bits  = ss_bits if join is None else join.join(ss_bits)
		s_bits  = s_bits  if not ka else apply(s_bits, **ka)
		return s_bits
	return rep_bits

def flags_of_masks(masks, bits=None):
	if bits is None:	return flags_of_masks.__get__(tuple(masks))
	
	# resolve output
	nn_out = tuple(mask & bits for mask in masks)
	return nn_out

nbits_of_num				= count_ones
iflag						= flags_of_masks
ibit_of						= enum_bits
#endregion

#region notes
'''
# Terms

n|num|number:    an int|float|complex but usually just int.  may include other coercible types such as Time
nn|nums|numbers: iterable of num values
idx|index:       an int|slice
ndx:             single int or int reducible index types but never a slice
vec-tor:         start,step[,stop]
lohi:            low & high arguments passed individually
bound:           (low,high) argument passed as a whole
'''
#endregion
