from pyutils.defs.colldefs import *
from pyutils.defs.typedefs import *

#region basic devcs
class VarsMapperProto:
	#region access
	# todo: should satisfy isinstance(self,dict) == True without instancing another dict
	# todo: what are simpler ways to achieve this?  eval ...
	#       see alternative strategy below ...
	def __repr__(self):					return self.__dict__.__repr__()
	def __len__(self):					return self.__dict__.__len__()
	def __iter__(self):					return self.__dict__.__iter__()
	def __getitem__(self,key):			return self.__dict__[key]
	def __setitem__(self,key,val):		return self.__dict__.__setitem__(key,val)
	def __delitem__(self,key):			return self.__dict__.__delitem__(key)
	def get(self,*pa,**ka):				return self.__dict__.get(*pa,**ka)
	def clear(self):					return self.__dict__.clear()
	def keys(self):						return self.__dict__.keys()
	def values(self):					return self.__dict__.values()
	def items(self):					return self.__dict__.items()
	def union(self,other):				return self.__dict__ | other
	def runion(self,other):				return other | self.__dict__
	def update(self,*pa,prio_old=no,**ka):
		if prio_old:
			pa = ((k,v) for k,v in dict(*pa,**ka).items() if k not in self),
			ka = map0
		self.__dict__.update(*pa,**ka)
		return self
	__or__,__ror__		= union,runion
	#endregion
	...

class VarsMapper:
	#region access
	# todo: investigate slots as solution
	def __init_subclass__(cls,**ka):
		if type(super().__init_subclass__) != type(object.__init_subclass__):
			super().__init_subclass__(**ka)
		else:
			for k,v in ka.items():
				setattr(cls,k,v)
		...
	def __init__(self,*pa,**ka):
		ka = self.__dict__|ka
		del self.__dict__
		self.__dict__ = self
		super().__init__(*pa,**ka)
	def update(self,*pa,prio_old=no,**ka):
		if prio_old:
			pa = ((k,v) for k,v in dict(*pa,**ka).items() if k not in self),
			ka = map0
		super().update(*pa,**ka)
		return self
	def __dir__(self):				return []
	#endregion
	...

class Obj(VarsMapper,dict): pass
#endregion