import types
from dataclasses import dataclass, field

from pyutils.defs.typedefs import *
from pyutils.defs.funcdefs import *
from pyutils.utilities.callbasics import *

#region cls defs
class DefsClassUtils:
	argsobj_at_cls			= {}
	argscls_at				= {}
	the_attr_default		= '_the'

DDClsU = _D					= DefsClassUtils
#endregion

#region cls utils
sign_all_any				= lambda *pa,**ka: pa + tuple(ka.items())
# sign_keys_any				= lambda keys,*pa,**ka: tuple((k,isint(k,put=pa,orput=ka)[k]) for k in keys)
sign_keys_any				= lambda keys,*pa,**ka: tuple((k,pa[n] if n<len(pa) else ka[k]) for n,k in enumerate(keys))

def dataclassown(cls=None,**ka):
	''' bypass counter-productive dataclass restrictions '''
	def call_post(cls):
		base,*_ = *cls.__bases__, object
		cx_repr = base.__repr__ != cls.__repr__ and cls.__repr__
		cx_hash = base.__hash__ != cls.__hash__ and cls.__hash__
		cx_hash and delattr(cls,'__hash__')
		cls_out = dataclass(cls, **ka)
		cx_repr and setattr(cls,'__repr__', cx_repr)
		cx_hash and setattr(cls,'__hash__', cx_hash)
		return cls_out
		
	result = call_post(cls) if cls else call_post
	return result

def thecls(cls=None, *_pa, singleton=True, make=no, fab_prop=None, _log=True, **_ka):
	''' decorates given class to be singleton by replacing initializers. decorated cls available as ._base '''
	# todo: optionally add to static mapping
	# todo: allow option to wrap as a non-singleton default instance
	_log = istrue(_log,put=print)
	the_attr = _D.the_attr_default  # todo: implement as arg
	def adapt_thecls(cls):
		''' decorator call to transform given *cls* into a singleton '''
		def new_first(cls,*pa,**ka):
			''' instantiation call for singleton *cls* '''
			cls.__new__ = getattr(cls, '__new_after__', new_after)
			the = cls._the = cls.__new_base__(cls)
			return the
		def init_first(self,*pa,**ka):
			''' constructor call for singleton *cls* '''
			the_cls.__init__ = getattr(the_cls, '__init_after__', init_after)
			the_cls.__init_base__(self,*pa,**ka)
			the_cls._the = self
		def new_after(cls,*pa,_log=_log,**ka):
			_log and _log(f'The cls:{cls.__name__}.new called again.')
			return cls._the
		def init_after(self,*pa,_log=_log,**ka):
			_log and _log(f'The cls:{the_cls.__name__}.init called again.')
		
		# modify class to be a singleton
		
		adapts				= dict(
			_base			= cls,
			__cls_base__	= cls,
			__new_base__	= cls.__new__,
			__init_base__	= cls.__init__,
			__module__		= cls.__module__,
			__new__			= new_first,
			__init__		= init_first,
			_the			= fab_prop and fab_prop(),
		)
		the_cls	= type('The'+cls.__name__, (cls,*cls.__bases__), adapts)
		the_attr in cls.__dict__ and adapts.pop(the_attr)
		for k,v in adapts.items():
			setattr(the_cls,k,v)
		
		the = make and the_cls(*_pa,**_ka)		# populate the_cls._the if needed
		return the_cls
	result = not cls and adapt_thecls or adapt_thecls(cls)
	return result

def argscls(cls=None,sign=sign_all_any):
	''' create the_cls as subclass of *cls*. subsequent calls to the_cls.__new__ with the same args returns the_obj
		for theobj_of to skip calls to __new__ & __init__ when the_obj already exists changes subclass as follows:
		moves: __new__       __init__       theobj_of  noop
		into:  __base_new__  __base_init__  __new__    __init__
	'''
	sign = sign if iscx(sign) else sign_keys_any.__get__(tss(sign))
	def argsobj_of(cls, *pa, **_ka):
		ka = dict(annodefs(cls), **_ka)
		sig_args = sign(*pa,**ka)
		obj_at_args = cls._obj_at_args = _D.argsobj_at_cls.get(cls) or _D.argsobj_at_cls.setdefault(cls, {})
		args_obj = obj_at_args.get(sig_args)
		
		if args_obj is None:
			args_obj = obj_at_args[sig_args] = cls.__base_new__(cls,*pa,**ka)
			object.__init__!=cls.__base_init__ and args_obj.__base_init__(*pa,**ka)
		args_obj._n_objs += 1
		return args_obj
	
	def argscls_of_cls(cls):
		args_cls = _D.argscls_at.get(cls) or _D.argscls_at.setdefault(cls,
			type(cls.__name__, (cls,), dict(
				__module__		= cls.__module__,
				__base_new__	= cls.__new__,
				__base_init__	= cls.__init__,
				__new__			= argsobj_of,
				__init__		= noop,
				_n_objs			= 0,
				_the			= 'cls', ), ), )
		return args_cls
	stage = cls and argscls_of_cls(cls) or argscls_of_cls
	return stage

def converter(cls,obj=miss,*pa,**ka):
	''' conventional approach to converting an object to a given class '''
	# todo: implement
	# @classmethod
	if istype(obj,cls):		return obj
	
	pa = pa if obj is miss else (obj,)+pa
	out = cls(*pa,**ka)
	return out

dataclassfroz = dclsfroz	= dataclass(unsafe_hash=False)
dataclassrepr = dclsown		= dataclassown
theclass,theobj				= thecls, redef(thecls, make=yes)
#endregion

#region anno utils
_key_at						= lambda item: item[0]
_val_at						= lambda item: item[1]
_to_item_at					= {
	0						: _key_at,
	1						: _val_at,
	False					: _val_at,	# give value when to_item is False
	True					: None, }	# give item when to_item is True; no change needed so to_item=noop

def anno(obj):
	''' get *obj* class annotations defined in obj.[__class__.]__annotations__ '''
	cls = clsof(obj)
	anno_out = getattr(cls, '__annotations__', None)
	return anno_out
def annos(obj, kcls:int|str=None, *, at=slice(0,2), base_1st=no, base_prio=no, fab=dict):
	''' get *obj* class annotations as iterable of [attr,type],..
		:param obj:       class or object of class with annotations
		:param kcls:      selects class or base class from which to collect annotations, None includes all
		:param base_1st:  when True stack annotations leading with base-most classes
		:param base_prio: when True choose base-most class annotation where there are duplicate keys
		:param fab:       output collection converter
	'''
	# resolve input qualifying classes:  base_key => cls_rseq
	cls_in = clsof(obj)
	kcls = (
		kcls			if isset(kcls) else
		set(kcls)		if isntstr(kcls) and isiter(kcls) else
		{kcls} )
	
	# filter class annos sources:  kcls => icls_eq_k
	icls_eq_k = [cls for nth,cls in enumerate(cls_in.__mro__) if kcls & {cls, cls.__name__, nth, None}]
	
	# resolve output:  cls_seq => annos
	annos = []
	[annos.extend(getattr(cls, '__annotations__', map0).items()) for cls in icls_eq_k[::base_1st and -1 or 1]]
	if isbool(base_prio):
		kk_prio,step = set(), base_prio==base_1st and 1 or -1
		annos = [(k,v)[at] for k,v in annos[::step] if (k not in kk_prio,kk_prio.add(k))[0]][::step]  # keep only 1sts
	annos = annos if not fab else fab(annos)
	return annos
def annodefs(obj, kcls:int|str=None, fabs=True, fab=None, **ka):
	''' get *obj* class annotation defaults as iterable of attr->default
		:param obj:  class or object of class with annotations
		:param kcls: selects class or base class from which to collect annotations
		:param fabs: anno output converter
		:param fab:  annos output converter
	'''
	# todo: reuse annoka instead
	# resolve inputs
	cls,fabs = clsof(obj), _to_item_at.get(fabs,fabs)
	
	# resolve output
	kkanno = annos(cls, kcls)
	anno_defs = ((k, getattr(cls, k)) for k in kkanno)
	anno_defs = anno_defs if not fabs else map(fabs,anno_defs)
	anno_defs = anno_defs if not fab else fab(anno_defs, **ka)
	return anno_defs
def annoka(obj, *_pa,pa:tuple=None, kcls:int|str=None,fabs=True,fab=dict,at=0, ka_solo:bool=no,ka:dict=None,**_ka):
	''' select subset of vars as iterable of attr->member given *obj* with class annotations and vars *pa* & *ka*
		:param obj:     class or object of class with annotations
		:param kcls:    selects class or base class from which to collect annotations
		:param ka_solo: in cases where single pos arg is dict & zero key args exist then transfer pa[0] to ka
		:param fabs:    anno output converter
		:param fab:     annos output converter
		:param at:      index used to select output from the pair (ka_anno,ka_diff)
	'''
	# resolve inputs
	pa,ka = pa or _pa, ka or _ka
	pa,ka = (ka_solo and len(pa)==1 and isdict(pa[0])) and [(),pa[0]|ka] or [pa,ka]
	cls,fabs = clsof(obj), _to_item_at.get(fabs,fabs)
	anno = annos(cls, kcls)
	
	# resolve input vars
	ka_obj = {} if istype(obj) else obj.__dict__  # todo: handle obj.__slots__
	ka_obj = {k:ka_obj.get(k,getattr(cls,k,miss)) for k in anno}
	ka_diff = dict(ka_obj, **dict(zip(anno,pa)), **ka)
	
	# resolve output
	ka_anno = ((k,ka_diff.pop(k,None)) for k in anno)
	ka_anno = ka_anno if not fabs else map(fabs,ka_anno)
	ka_anno = ka_anno if not fab else fab(ka_anno)
	ka_out = ka_anno,ka_diff
	ka_out = ka_out[:] if at is None else ka_out[at]
	return ka_out
def annoka2(obj, *pa, fab=dict, **ka):	return annoka(obj, *pa, **ka, at=slice(None), fab=fab)
def annoinit(obj, *pa, at=0, update=None, **ka):
	''' reinit (or reapply) annotation defaults to given *obj*. given *update* insert directly into __dict__ '''
	# resolve anno vars
	update = update if update is not None else '__slots__' not in obj.__class__.__dict__
	ka_out = ka_anno,ka_diff = annoka(obj, *pa, **ka, at=slice(None), fab=dict)
	missing = {k for k,v in ka_anno.items() if v is miss}
	if missing: raise ValueError(f'{nameof(obj)}.init() is missing annotations: {" ".join(missing)} ')
	
	# perform assignment
	if update:
		obj.__dict__.update((k,isgen(v,fab=list)) for k,v in ka_anno.items())  # NOTE: update consumes generators 
	else:
		for attr,val in ka_anno.items():
			setattr(obj, attr, val)
	ka_out = ka_out[:] if at is None else ka_out[at]
	return ka_out
def annoreset(obj, *pa, at=0, **ka):
	''' reinit (or reapply) annotation defaults to given *obj* '''
	ka_out = ka_anno,ka_diff = annodefs(obj, *pa, **ka, at=slice(None), fab=dict)
	for attr, value in ka_anno.items():
		setattr(obj, attr, value)
	ka_out = ka_out[:] if at is ... else ka_out[at]
	return ka_out

def annoattrs(*pa,**ka):		return annos(*pa, at=0, fab=list, **ka)
def annotypes(*pa,**ka):		return annos(*pa, at=1, fab=list, **ka)
def annofills(*pa,**ka):		return annodefs(*pa, fabs=None, **ka)
def annombrs(obj,fab=no,**ka):	return annoka(obj,ka=obj.__dict__, fabs=_val_at, fab=fab, **ka)
def annovars(obj,**ka):			return annoka(obj,ka=obj.__dict__, **ka)

annoof,annosof,annokaof,annodefsof,annoattrsof,annofillsof,annombrsof,annovarsof, = (
anno,  annos,  annoka,  annodefs,  annoattrs,  annofills,  annombrs,  annovars,   )
#endregion

#region adapt utils todo
#endregion

#region adorn utils
def method_of_sbj(cls_sbj,s_sbj,s_method,self=None,fmt_name='cls_sbj={qualname}'):
	''' get specified method from subject attr=*s_sbj* of class=*cls_sbj*
		:param cls_sbj:
		:param s_sbj:
		:param s_method:
		:param self:
		:param fmt_name:
	'''
	sbj_staticmethod = getattr(cls_sbj,s_method)
	def sbj_method(self,*pa,**ka):
		return sbj_staticmethod(getattr(self,s_sbj), *pa,**ka)
	sbj_method.__qualname__ = fmt_name.format(qualname=sbj_staticmethod.__qualname__) # todo: make into decorator
	method = self is None and sbj_method or sbj_method.__get__(self)
	return method

def adorn_sbj(cls,cls_sbj,s_sbj,incl=None,excl=None,fmt_name='{cls}={qualname}'):
	''' adorn a target *cls* with select methods from subject attr=*s_sbj* of class=*cls_sbj*
		:param cls: class to adorn
		:param cls_sbj: class of subject
		:param s_sbj: attr of subject in target *cls*
		:param incl: methods to keep
		:param excl: methods to drop
	'''
	# resolve methods from subject
	ss_method = [name for name in cls_sbj.__dict__ if istype(getattr(cls_sbj,name),types.FunctionType)]
	ss_method = ss_method if not incl else keeprx(ss_method,incl)	# todo:
	ss_method = (s for s in ss_method if s not in cls.__dict__)		# same as drop(ss_method,cls.__dict__.__contains__)
	
	# apply methods to class
	for s_method in ss_method:
		method = method_of_sbj(cls_sbj,s_sbj,s_method)
		setattr(cls,s_method,method)

def init_cls(sbj, as_base=yes, **ka):
	def init_of_cls(cls, name=None, **ka):
		cls.__name__ = name or cls.__name__
		not as_base and super(cls).__init_subclass__(**ka)
		for k,v in as_base and ka.items() or ():
			setattr(cls,k,v)
	stage = isnttype(sbj) and init_of_cls or init_of_cls(sbj,**ka)
	return stage

def alias(*pa,val=miss,names=None,vars=None):
	# resolve inputs
	val,names = (
		(val,names)		if val != miss and names else
		(val,*pa)		if not names else
		(*pa,names)		if val == miss else
		pa )
	names = tss(names)
	
	# resolve output
	def alias_of(val):
		_vars = isdict(vars,orcall=globals)
		for name in names:
			globals()[name] = val
		return val
	stage = val == miss and alias_of or alias_of(val)
	return stage

init_subclass				= init_cls
#endregion
