import asyncio
from concurrent import futures
from time import sleep
from datetime import datetime as DC

from pyutils.utilities.callbasics import *
from pyutils.utilities.iterutils import *
from pyutils.defs.typedefs import *

#region async defs
_asleep					= asyncio.sleep(0)
_asleep.close()													# silences 'coro..never awaited' warning
Coroutine = Coro		= type(_asleep)
Future					= asyncio.Future
Task					= asyncio.Task
Await					= Coro|Future|futures.Future|Task

iscoroutinefunction		= asyncio.iscoroutinefunction
iscoroutine				= asyncio.iscoroutine					# iscoro & iscoroutine differ somewhat; see istypethen  
iscoro					= istypethen(Coro)						# iscoro & iscoroutine differ somewhat; see istypethen
isfuture				= istypethen(asyncio.Future|futures.Future)
istask					= istypethen(Task)
isawait = isacx			= istypethen(Await)
isasync = isafx			= ifthen(asyncio.iscoroutinefunction)
awaitable				= isawait
#endregion

#region async forms
def await_of(acx):
	acx_out = (
		acx							if isawait(acx) else
		awaits(acx)					if isiter(acx) else			# todo: may need different wrapper
		asyncio.sleep(acx)			if isnum(acx) else
		isawait(acx(),orfab=throw)	if iscall(acx) else			# todo: may need different wrapper
		throw(acx) )
	return acx_out
#endregion

#region async utils
def slowcall(call,*pa,delay=.01,**ka):
	sleep(delay)
	call(*pa,**ka)

def waits(isecs,secs=1):
	isecs = isecs if isiter(isecs) else isecs()
	for secs_n in isecs:
		sleep(isnone(secs_n,put=secs))
async def awaits(isecs,secs=1):
	isecs = isecs if isiter(isecs) else isecs()
	for secs_n in isecs:
		await asyncio.sleep(isnone(secs_n,put=secs))

def wait_each(vals,secs=1,fabs=None):
	ival = vals if not fabs else map(fabs,vals)
	for val in ival:
		sleep(secs)
async def await_each(vals,secs=1,fabs=None):
	ival = vals if not fabs else map(fabs,vals)
	for val in ival:
		await asyncio.sleep(secs)

async def agather(*pa,**ka):
	await asyncio.gather(*pa,**ka)
def r_un_async(*pa,**ka):		return asyncio.run(agather(*pa,**ka))
def run_async(*pa,**ka):
	loop = asyncio.new_event_loop()
	loop.run_until_complete(agather(*pa,**ka))
	loop.close()
	...

slowcx					= slowcall
#endregion

#region time devcs
class timetaken:
	''' time a context operation '''
	delta_time			= '---'
	def __enter__(self):
		self.start = DC.now()
		return self
	def __exit__(self, exc_type=None, exc_val=None, exc_tb=None):
		self.delta_time = DC.now() - self.start
		print(self.delta_time)
	def __repr__(self):
		result = f'{self.__class__.__name__}({self.delta_time})'
		return result
	
timetaker = timetaken()
#endregion

#region notes
'''
afx|async func
acx|awaitable
coro|coroutine
task
'''
#endregion
