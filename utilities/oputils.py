from __future__ import annotations

from pyutils.utilities.numutils import atleast
from pyutils.utilities.callutils import *

#region op utils
'''
note: standard python operator module was considered, however this implementation is needed for its distinction
in handling arbitrarily typed args.  this implementation yields type dependent behavior.
for example:
>>> add('2','2')]	== '22'
>>> add(2,2)		== 4
'''
_ata,_atb		= 0, 1
_global_ids		= set(map(id,globals().values()))

neg				= lambda a:   - a
pos				= lambda a:   + a
invert			= lambda a:   ~ a
notb			= lambda a: not a

eq 				= lambda a,b=miss,*_: a ==  b       if b is not miss else eq(*a,...)
ne 				= lambda a,b=miss: a !=  b
ge 				= lambda a,b=miss: a >=  b
le 				= lambda a,b=miss: a <=  b
gt 				= lambda a,b=miss: a >   b
lt 				= lambda a,b=miss: a <   b
add 			= lambda a,b=miss: a +   b
sub 			= lambda a,b=miss: a -   b
mul 			= lambda a,b=miss: a *   b
truediv			= lambda a,b=miss: a /   b
floordiv		= lambda a,b=miss: a //  b
mod 			= lambda a,b=miss: a %   b
pow 			= lambda a,b=miss: a **  b
lshift 			= lambda a,b=miss: a <<  b
rshift 			= lambda a,b=miss: a >>  b
xor		 		= lambda a,b=miss: a ^   b
andi	 		= lambda a,b=miss: a &   b
ori 			= lambda a,b=miss: a |   b
andb	 		= lambda a,b=miss: a and b
orb 			= lambda a,b=miss: a or  b
truediv_safe	= lambda a,b=miss: b and a /   b
floordiv_safe	= lambda a,b=miss: b and a //  b
mod_safe		= lambda a,b=miss: b and a %   b

req 			= lambda a,b=miss: b ==  a
rne 			= lambda a,b=miss: b !=  a
rge 			= lambda a,b=miss: b >=  a
rle 			= lambda a,b=miss: b <=  a
rgt 			= lambda a,b=miss: b >   a
rlt 			= lambda a,b=miss: b <   a
radd 			= lambda a,b=miss: b +   a
rsub 			= lambda a,b=miss: b -   a
rmul 			= lambda a,b=miss: b *   a
rtruediv		= lambda a,b=miss: b /   a
rfloordiv		= lambda a,b=miss: b //  a
rmod 			= lambda a,b=miss: b %   a
rpow 			= lambda a,b=miss: b **  a
rlshift 		= lambda a,b=miss: b <<  a
rrshift 		= lambda a,b=miss: b >>  a
rxor	 		= lambda a,b=miss: b ^   a
rori 			= lambda a,b=miss: b |   a
randi	 		= lambda a,b=miss: b &   a
randb	 		= lambda a,b=miss: b and a
rorb 			= lambda a,b=miss: b or  a
rtruediv_safe	= lambda a,b=miss: a and b /   a
rfloordiv_safe	= lambda a,b=miss: a and b //  a
rmod_safe		= lambda a,b=miss: a and b %   a

remaining_of	= lambda a,b,ab: atleast(0., a,b)

div,div_safe	= truediv,truediv_safe
rdiv,rdiv_safe	= rtruediv,rtruediv_safe
#endregion

#region op cascade utils
def negs(*pa,fab=None,**ka):				return callargs( neg,    *pa,**ka, fab=fab)
def poss(*pa,fab=None,**ka):				return callargs( pos,    *pa,**ka, fab=fab)
def inverts(*pa,fab=None,**ka):				return callargs( invert, *pa,**ka, fab=fab)
def notbs(*pa,fab=None,**ka):				return callargs( notb,   *pa,**ka, fab=fab)

def eqs(*pa,**ka):							return all(irecallargs( eq, *pa,**ka))
def gts(*pa,**ka):							return all(irecallargs( gt, *pa,**ka))
def lts(*pa,**ka):							return all(irecallargs( lt, *pa,**ka))
def ges(*pa,**ka):							return all(irecallargs( ge, *pa,**ka))
def les(*pa,**ka):							return all(irecallargs( le, *pa,**ka))

def adds(*pa,**ka):							return recallargs( add,           *pa,**ka)
def subs(*pa,**ka):							return recallargs( sub,           *pa,**ka)
def muls(*pa,**ka):							return recallargs( mul,           *pa,**ka)
def floordivs(*pa,**ka):					return recallargs( floordiv,      *pa,**ka)
def truedivs(*pa,**ka):						return recallargs( truediv,       *pa,**ka)
def xors(*pa,**ka):							return recallargs( xor,           *pa,**ka)
def andis(*pa,**ka):						return recallargs( andi,          *pa,**ka)
def oris(*pa,**ka):							return recallargs( ori,           *pa,**ka)
def andbs(*pa,**ka):						return recallargs( andb,          *pa,**ka)
def orbs(*pa,**ka):							return recallargs( orb,           *pa,**ka)
def floordivs_safe(*pa,**ka):				return recallargs( floordiv_safe, *pa,**ka)
def truedivs_safe(*pa,**ka):				return recallargs( truediv_safe,  *pa,**ka)

def iadds(*pa,**ka):						return irecallargs( add,           *pa,**ka)
def isubs(*pa,**ka):						return irecallargs( sub,           *pa,**ka)
def imuls(*pa,**ka):						return irecallargs( mul,           *pa,**ka)
def ifloordivs(*pa,**ka):					return irecallargs( floordiv,      *pa,**ka)
def itruedivs(*pa,**ka):					return irecallargs( truediv,       *pa,**ka)
def ixors(*pa,**ka):						return irecallargs( xor,           *pa,**ka)
def iandis(*pa,**ka):						return irecallargs( andi,          *pa,**ka)
def ioris(*pa,**ka):						return irecallargs( ori,           *pa,**ka)
def iandbs(*pa,**ka):						return irecallargs( andb,          *pa,**ka)
def iorbs(*pa,**ka):						return irecallargs( orb,           *pa,**ka)
def ifloordivs_safe(*pa,**ka):				return irecallargs( floordiv_safe, *pa,**ka)
def itruedivs_safe(*pa,**ka):				return irecallargs( truediv_safe,  *pa,**ka)

def idebits(*pa,to_out=remaining_of,**ka):	return irecallargs( sub,       *pa,**ka, to_out=to_out)
def ldebits(*pa,to_out=remaining_of,**ka):	return list(irecallargs( sub,  *pa,**ka, to_out=to_out))
def tdebits(*pa,to_out=remaining_of,**ka):	return tuple(irecallargs( sub, *pa,**ka, to_out=to_out))
def fdebits(*pa,to_out=remaining_of,**ka):	return recallargs( sub,        *pa,**ka, to_out=to_out)

def eqspa(*pa,**ka):						return all(irecallargs( eq, pa,**ka))
def gtspa(*pa,**ka):						return all(irecallargs( gt, pa,**ka))
def ltspa(*pa,**ka):						return all(irecallargs( lt, pa,**ka))
def gespa(*pa,**ka):						return all(irecallargs( ge, pa,**ka))
def lespa(*pa,**ka):						return all(irecallargs( le, pa,**ka))

def addspa(*pa,**ka):						return recallargs( add,           pa,**ka)
def subspa(*pa,**ka):						return recallargs( sub,           pa,**ka)
def mulspa(*pa,**ka):						return recallargs( mul,           pa,**ka)
def floordivspa(*pa,**ka):					return recallargs( floordiv,      pa,**ka)
def truedivspa(*pa,**ka):					return recallargs( truediv,       pa,**ka)
def xorspa(*pa,**ka):						return recallargs( xor,           pa,**ka)
def andispa(*pa,**ka):						return recallargs( andi,          pa,**ka)
def orispa(*pa,**ka):						return recallargs( ori,           pa,**ka)
def andbspa(*pa,**ka):						return recallargs( andb,          pa,**ka)
def orbspa(*pa,**ka):						return recallargs( orb,           pa,**ka)
def floordivspa_safe(*pa,**ka):				return recallargs( floordiv_safe, pa,**ka)
def truedivspa_safe(*pa,**ka):				return recallargs( truediv_safe,  pa,**ka)

def iaddspa(*pa,**ka):						return irecallargs( add,           pa,**ka)
def isubspa(*pa,**ka):						return irecallargs( sub,           pa,**ka)
def imulspa(*pa,**ka):						return irecallargs( mul,           pa,**ka)
def ifloordivspa(*pa,**ka):					return irecallargs( floordiv,      pa,**ka)
def itruedivspa(*pa,**ka):					return irecallargs( truediv,       pa,**ka)
def ixorspa(*pa,**ka):						return irecallargs( xor,           pa,**ka)
def iandispa(*pa,**ka):						return irecallargs( andi,          pa,**ka)
def iorispa(*pa,**ka):						return irecallargs( ori,           pa,**ka)
def iandbspa(*pa,**ka):						return irecallargs( andb,          pa,**ka)
def iorbspa(*pa,**ka):						return irecallargs( orb,           pa,**ka)
def ifloordivspa_safe(*pa,**ka):			return irecallargs( floordiv_safe, pa,**ka)
def itruedivspa_safe(*pa,**ka):				return irecallargs( truediv_safe,  pa,**ka)

def idebitspa(*pa,to_out=remaining_of,**ka):return irecallargs( sub,       pa,**ka, to_out=to_out)
def ldebitspa(*pa,to_out=remaining_of,**ka):return list(irecallargs( sub,  pa,**ka, to_out=to_out))
def tdebitspa(*pa,to_out=remaining_of,**ka):return tuple(irecallargs( sub, pa,**ka, to_out=to_out))
def fdebitspa(*pa,to_out=remaining_of,**ka):return recallargs( sub,        pa,**ka, to_out=to_out)
# todo: add to_item arg

nots							= notbs
divfs,idivfs,divfspa,idivfspa	= truedivs,itruedivs,truedivspa,itruedivspa
divis,idivis,divispa,idivispa	= floordivs,ifloordivs,floordivspa,ifloordivspa
divs,idivs						= divfs,idivfs
addts, iaddts, subts, isubts, mults, imults,  = (
adds,  iadds,  subs,  isubs,  muls,  imuls, )  # aliases to disambiguate common abrvs like 'subs','divs'
#endregion

#region op defs
# todo: relo to pyutils.op or push _locals references into DDOP.initiailize
'''
solvable, operable, variables, expr-ession, query, eval
operation
Val <- Eval
'''
_global_ids				|= {id(_global_ids)}
_locals					= {k:v for k,v in globals().items() if id(v) not in _global_ids}
list(rename_call(_call,_k,True) for _k,_call in _locals.items())  # todo: merge with above
class DefsOp:
	# str defs
	s_kk				= 'eq ne ge le gt lt add sub mul truediv floordiv mod pow lshift rshift xor andi ori andb orb '
	s_ccc				= '== != >= <= >  <  +   -   *   /       //       %   **  <<     >>     ^   &    |   and  or  '
	s_kk_rec			= 'adds subs muls floordivs truedivs xors andis oris andbs orbs '
	s_ccc_rec			= '+    -    *    /         //       ^    &     |    and   or   '
	fmtr_rev,fmtr_safe	= 'r{}'.format, '{}_safe'.format
	
	# strs defs
	kk = keys 			= tuple(filter(bool,s_kk.split()))
	ccc = abrvs			= tuple(filter(bool,s_ccc.split()))
	kk_rec				= tuple(filter(bool,s_kk_rec.split()))
	ccc_rec				= tuple(filter(bool,s_ccc_rec.split()))
	kk_safe				= tuple(map('{}_safe'.format,kk))
	kk_rev				= tuple(map('r{}'.format,kk))
	kk_revsafe			= tuple(map('r{}_safe'.format,kk))
	
	# vals defs
	vv = vals 			= tuple(map(_locals.get, kk))
	vv_safe	 			= tuple(map(_locals.get, kk_safe))
	vv_rev	 			= tuple(map(_locals.get, kk_rev))
	vv_revsafe	 		= tuple(map(_locals.get, kk_revsafe))
	expr_types			= tuple, 'Expression'
	
	# mapped defs
	k_at				= FinalMap(zip(ccc+kk, kk*2))
	op_at = at			= FinalMap()
	opsafe_at			= FinalMap()
	oprev_at			= FinalMap()
	oprevsafe_at		= FinalMap()
	
	@classmethod
	def initialize(cls):
		cls.op_at=cls.at= FinalMap({k:cls.op_of(k, rev=no, safe=no) for k in cls.ccc+cls.kk})
		cls.opsafe_at	= FinalMap({k:cls.op_of(k, rev=no, safe=on) for k in cls.ccc+cls.kk})
		cls.oprev_at	= FinalMap({k:cls.op_of(k, rev=on, safe=no) for k in cls.ccc+cls.kk})
		cls.oprevsafe_at= FinalMap({k:cls.op_of(k, rev=on, safe=on) for k in cls.ccc+cls.kk})
	@classmethod
	def op_of(cls, op, rev=False, safe=False):
		#resolve inputs
		s_op = cls.k_at[op]
		s_op = s_op if not rev else cls.fmtr_rev(s_op)
		#resolve output
		op = safe and _locals.get(cls.fmtr_safe(s_op)) or _locals[s_op]
		return op
	
DD = DDOP				= DefsOp
Def = DefsOp # todo: eliminate
DD.initialize()
#endregion

#region op devcs
from pyutils.utilities.clsutils import *  # fixme: importing *utils.py is not allowed
# todo: RELO to clsutils
# def dataclassown(cls=None,**ka):
# 	def call_post(cls):
# 		cx_repr = object.__repr__ != cls.__repr__ and cls.__repr__
# 		cx_hash = object.__hash__ != cls.__hash__ and cls.__hash__
# 		# cx_repr and delattr(cls,'__repr__')
# 		cx_hash and delattr(cls,'__hash__')
# 		cls_out = dataclass(cls, **ka)
# 		cx_repr and setattr(cls,'__repr__', cx_repr)
# 		cx_hash and setattr(cls,'__hash__', cx_hash)
# 		return cls_out
#
# 	result = call_post(cls) if cls else call_post
# 	return result

class Operation:
	name: str
class Parameters(list): pass
@dataclassown
class Expression:
	op: Op
	vals: Parms			= field()
	@classmethod
	def of(cls, op, *vals, **ka):
		if islist(op):
			op,*vals = op
		out = cls(op,vals)
		return out
	def __hash__(self):
		return hash(id(self))
	def __iter__(self):
		yield self.op
		yield from self.vals
	def resolve(self,**ka):
		out = resolve(self.op,vals=self.vals,**ka)
		return out

def parse_groups(s_expr,group_chs='()',sep=' ',fab=asis):
	ch_open,ch_close = group_chs
	subs = s_expr if isntstr(s_expr) else list(filter(bool,s_expr.split(sep)))
	exprs = [[]]
	while subs:
		sub = _sub = subs.pop(0)
		if not sub: continue
		elif sub[0] == ch_open:
			exprs.append([])
			len(sub) > 1 and subs.insert(0,sub[1:])
			sub = ''
		elif sub[0] == ch_close:
			sub_r = sub[1:]
			sub_r and subs.insert(0,sub_r)
			sub = fab(exprs.pop())
		elif ch_close in sub:
			sub,sub_m,sub_r = sub.partition(ch_close)
			(sub_m or sub_r) and subs.insert(0,sub_m + sub_r)
		sub and exprs[-1].append(sub)
	# assert not exprs
	# assert len(exprs), 'unexpected closing parenthesis'
	expr, = exprs
	expr = fab(expr)
	return expr

def resolve(op_in, *_vals, vals=None, is_op=iscall,to_op=DDOP.op_at,to_val=None,is_expr=Expression):
	# resolve inputs
	_to_op,_to_val = to_op,to_val
	to_op  = to_op   if isntdict(to_op)   else lambda op,kkv=to_op: kkv.get(op,op)
	to_val = to_val  if isntdict(to_val)  else lambda v,kkv=to_val: kkv.get(v,v)
	op = op_in if is_op(op_in) else to_op(op_in)
	is_expr = is_expr if not istype(is_expr) else lambda obj,cls=is_expr: istype(obj,cls)
	ka_conf = dict(to_op=to_op,to_val=to_val,is_expr=is_expr)
	
	# resolve output:  => vv(values), value
	vv = (vals or _vals) if not to_val else list(map(to_val,vals or _vals))
	vv = [v if not is_expr(v) else resolve(*v,**ka_conf) for v in vv]
	value = recallpas(op, vv)
	return value

Op,Parms,Expr			= Operation,Parameters,Expression
parse_expr				= stage_call(parse_groups, fab=Expr.of)
#endregion

