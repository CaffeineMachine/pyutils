import re

from pyutils.defs.itemdefs import Ndx, atkey, DefsIndArg, atval
from pyutils.defs.typedefs import on,off,iscx,istrue,isyes,isnone,ismiss,isntmiss
from pyutils.defs.colldefs import collany, ismap, xt
from pyutils.utilities.callbasics import *
from pyutils.utilities.clsutils import annos

#region maputil defs


class DefsMapUtils:
	#region mode
	i					= iter
	#endregion
	#region scoeps
	class Ctls:
		fab_into		= asis
	class Args:
		cont			= 'cont'
		cont_set		= {cont, 'continue'}
		pa				= {0, *ss('pa')}
		ap				= {1, *ss('ap  *')}
		ak				= {2, *ss('ak  **')}
		itypes			=      omit, val, vals, node, nodes, zips,  = (
						  tss('omit  val  vals  node  nodes  zips  ') )
	class Keys:
		kk				=      hdr, comment, j_vals, j_lines,  = (
						  tss('hdr  comment  j_vals  j_lines  ') )
	class Strs:
		kk				=      hdr, comment, j_vals, j_lines,  = (
						  tss('#    #        \t      \n       ') )
		
	AA,KK,SS			= Args,Keys,Strs
	#endregion
	...

DDZU = DDMapr = DD = _D	= DefsMapUtils
AAZU,KKZU,SSZU			= DDZU.AA,DDZU.KK,DDZU.SS
AAMapr,KKMapr,SSMapr	= AAZU,KKZU,SSZU  # todo: eliminate
#endregion

#region map forms
_ind_key,_ind_val,_ind_link =  0,1,slice(None,None,-1)

def getr_of_map(mapped, getr=None):
	''' given *mapped* get appropriate callable mapper which gets the value for a given key '''
	getr = (
		getattr(mapped,getr)			if isinstance(getr,str) else
		getr(mapped)					if getr is not None else
		getattr(mapped,'__getitem__')	if hasattr(mapped,'__getitem__') and not hasattr(mapped,'__instancecheck__') else  # ignore classes w/ getitem
		(	getattr(mapped,'__call__', None) or
			getattr(mapped,'__getattribute__') ) )
	return getr

def ipair(pairs):
	''' all items are confirmed pairs '''
	for a,b in pairs:
		yield a,b
def items_of(items=None,*cols_r,to_pairs=no,fab=None,**ka):
	''' get items iterable given items in various forms:  dict,iter,keys->vals '''
	items_out = (
		zip(items,*cols_r)		if cols_r else
		items.items()			if iscall(getattr(items,'items',None)) else
		items.__dict__.items()	if hasattr(items,'__dict__') else
		items )
	items_out = items_out if not to_pairs else ((k,v) for k,v in items_out)
	items_out = items_out if not fab else fab(items_out)
	return items_out

def kkof(mapped, to_key=None, unpack=False, fab=None):
	seq = mapped.keys()
	seq = (
		seq							if not to_key else
		map(to_key, seq)			if not unpack else
		(to_key(*k) for k in seq)
	)
	kk = seq if not fab else fab(seq)
	return kk
def vvof(mapped, to_val=None, unpack=False, fab=None):
	seq = mapped.values()
	seq = (
		seq							if not to_val else
		map(to_val, seq)			if not unpack else
		(to_val(*v) for v in seq)
	)
	vv = seq if not fab else fab(seq)
	return vv
def kkvof(mapped, to_item=None, unpack=False, fab=None):
	# note: use tomap to use variety and combination of converts like to_key/to_val/to_items
	ikv = mapped.items()
	ikv = ikv if to_item else map(to_item,ikv)
	ikv = (
		ikv							if not to_item else
		map(to_item, ikv)			if not unpack else
		(to_item(*kv) for kv in ikv)

	)
	kkv = ikv if not fab else fab(ikv)
	return kkv

def key_item_of(item):		return item[_ind_key], item
def keyitem_of(item):		return (item[_ind_key], *item)
def keyvals_of(item):		return (item[_ind_key], *item[_ind_val])
def link_of(item):			return item[_ind_link]

def key_item_per(items):
	''' Gen key => item mapping. Aliased to k_kv.  key_values.__class__([[k, [k, v]] for k, v in key_values]) '''
	items_out = items.items() if isinstance(items, dict) else items
	items_out = ((item[_ind_key], item) for item in items_out)
	return items_out

def keyitem_per(items):
	''' Gen key => item mapping. Aliased to k_kv.  key_values.__class__([[k, [k, v]] for k, v in key_values]) '''
	items_out = items.items() if isinstance(items, dict) else items
	items_out = ((item[_ind_key], *item) for item in items_out)
	return items_out

def keyvals_per(items):
	''' Gen key => link mapping. Alias is k_vk.  key_values.__class__([[k, [v, k]] for k, v in key_values]) '''
	items_out = items.items() if isinstance(items, dict) else items
	items_out = ((item[_ind_key], *item[_ind_val]) for item in items_out)
	return items_out

def link_per(items,fab=None):
	''' Gen key => link mapping. Alias is k_vk.  key_values.__class__([[k, [v, k]] for k, v in key_values]) '''
	items_out = items.items() if isinstance(items, dict) else items
	items_out = (item[_ind_link] for item in items_out)
	items_out = items_out if not fab else fab(items_out)
	return items_out

def iflattenmmap(mmap):
	ikvv = ikvof(mmap)
	for k, vv in ikvv:
		for v in vv:  # todo: use iterutils.irepeat(key) instead
			yield k, v

def fill_map(mapped, keys, value):
	''' link multiple keys to single value '''
	for key in keys:
		mapped[key] = value
	return mapped

def rmmap_of_map(mapped):
	''' get reverse multimap of value -> keys items. Roughly [ret[v].append(k) for k, v in self.items()] '''
	rmmap = {}  # todo: replace with list and add *fab* arg
	for key, value in ikvof(mapped):
		# value =  value if not to_values else to_values(value)
		if not hasattr(value, '__hash__'): continue  # todo: handle this dynamically per user arg
		
		# key = key if not to_keys else to_keys(key)
		rmmap.setdefault(value, []).append(key)
	return rmmap

def rmap_of_map(mapped, to_keys=None, to_values=None, overwrite_key=lambda old_key, new_key: True):
	''' get reverse mapping of value -> key items. Roughly {v:k for k, v in self.items()} '''
	# todo: rename overwrite_key with remap
	rmap = {}
	for key, value in ikvof(mapped):
		# value = value if not to_values else to_values(value)
		if not hasattr(value, '__hash__'): continue  # todo: handle this dynamically per user arg
		
		# key = key if not to_keys else to_keys(key)
		if value not in rmap or not overwrite_key or overwrite_key(rmap[value], key):
			rmap[value] = key
	return rmap

def rmap_of_mmap(mmapped, to_keys=None, to_values=None, overwrite_key=lambda old_key, new_key: True):
	''' get reverse mapped of value -> key items. Roughly {v:k for k, v in self.items()} '''
	rmap = {}
	for key, value in ikv_of_ikvv(mmapped):
		# value = value if not to_values else to_values(value)
		if not hasattr(value, '__hash__'): continue  # todo: handle this dynamically per user arg
		
		# key = key if not to_keys else to_keys(key)
		if value not in rmap or not overwrite_key or overwrite_key(rmap[value], key):
			rmap[value] = key
	return rmap

mapperof												= getr_of_map
zof = ikvof 											= items_of  # todo: improve support items|ikv|z convention
pairs_of												= redef(items_of,to_pairs=yes)
dkkof,hkkof,lkkof,tkkof									= redef_per_coll(kkof, arg='fab')
dvvof,hvvof,lvvof,tvvof									= redef_per_coll(vvof, arg='fab')
dkkvof,hkkvof,lkkvof,tkkvof								= redef_per_coll(kkvof, arg='fab')
ikeysmap,dkeysmap,hkeysmap,lkeysmap,tkeysmap			= kkof,dkkof,hkkof,lkkof,tkkof
ivalsmap,dvalsmap,hvalsmap,lvalsmap,tvalsmap			= vvof,dvvof,hvvof,lvvof,tvvof
iitemsmap,ditemsmap,hitemsmap,litemsmap,titemsmap		= kkvof,dkkvof,hkkvof,lkkvof,tkkvof
k_kv_of 												= key_item_of
kvv_of													= keyvals_of
vk_of													= link_of
k_kv_per = ik_kv_of 									= key_item_per
kkv_per = ikkv_of										= keyitem_per
kvv_per = ikvv_of										= keyvals_per
vk_per = ivk_of											= link_per
ikv_of_ikvv												= iflattenmmap
#endregion

#region tree utils
def get_node(keys, tree, *pa, gets=None, get=None, sep='.', index=no, fab=None, fill=miss, err=miss, excs=miss, **ka):
	''' recursively *gets* each key in *keys* on *tree*. the final key is reserved for *get* if given. '''
	excs = excs is miss and (KeyError,AttributeError) or excs
	try:
		# resolve inputs
		gets = (
			gets					if gets else 
			'__getattr__'			if isstr(keys) and '.' in keys else 
			'__getitem__' ) 
		keys = (
			tss(keys,sep=sep)		if isstr(keys) else
			keys.__index__()		if index and iscx(getattr(keys,'__index__',None)) else
			keys )
		*keys,key_out = keys if get or pa or ka else (*keys,miss)	# get is applied to the final item 
		
		# resolve output
		out = tree
		for key in keys:
			out = (
				out[key]			if not gets or gets == '__getitem__' else
				getattr(out,key)	if gets == '__getattr__' else
				get(out,key)		if iscx(gets) else
				getattr(out,gets)(key) )
		if get or pa or ka:
			get = get or gets or '__getitem__'
			getr = iscx(get) and get or getattr(out,get)
			out = getr(key_out,*pa,**ka)
	except excs as exc:
		if miss is fill is err:		raise
		out = fill if err is miss else err(exc,keys=keys)
	return out
def has_node(keys, obj):				return get_node(keys, obj, fill=...) is not ...

def add_branch(keys, obj, err=miss, fill=miss):
	''' recursively fill(key) on *obj* for given *keys*. Fill means dict.setdefault. '''
	out = obj
	for key in keys:
		val = out.get(key, fill)
		if err is not miss and val is fill:
			val = err(key)
			out[key] = val
		out = val
	return out
def fab_branch(keys, obj, call, *pa, fill=miss, err=miss, **ka):
	''' with lead *keys* recursively getitem(key) on *obj* for a target and with last key do *call*(target, key). '''
	out = fill
	*target_keys, key = keys
	
	# do recursive get with leading keys
	target_obj = add_branch(obj, target_keys, fill=fill, err=err)
	
	# perform *call* on dest_obj with last key
	dest_op = isstr(call) and getattr(target_obj.__class__, call) or call
	out = dest_op(target_obj, key, *pa, **ka)
	return out
def get_branch(keys, objs, *pa, fab=None, **ka): ''' todo '''
def pop_branch(keys, objs, *pa, fab=None, **ka): ''' todo '''

pop_node				= redef(get_node,   get='pop')
set_node				= redef(get_node,   get='__setitem__')
del_node				= redef(get_node,   get='__delitem__')
set_branch				= redef(add_branch, get='__setitem__')

mget					= get_node
mhas					= has_node
mset					= set_node
mpop					= pop_node
mdel					= del_node
madds					= add_branch
mfabs					= fab_branch
mgets					= get_branch
mpops					= pop_branch

rget_keys				= get_node
rget_call_keys			= get_node
set_keys				= set_node
pop_keys				= pop_node
del_keys				= del_node
rfill_keys				= add_branch
rfill_call_keys			= fab_branch
#endregion

#region mvars utils
def rget_attrs(obj, attrs, default=nothing):
	''' recursively getattr(key) on *obj* for given *attrs*. '''
	attrs = isstr(attrs) and attrs.split('.') or attrs
	default_pa = (default,) if default != nothing else ()
	result = obj
	try:
		for attr in attrs:
			result = getattr(result, attr, *default_pa)
	except AttributeError:
		if default is nothing:
			raise
		result = default
	return result
def has_attrs(obj, attrs):
	''' check if *attrs* ref exists in *obj* '''
	result = rget_attrs(obj, attrs, ...)
	return result is not ...

def rget_call_attrs(obj, call, attrs, *pa, default=nothing, **ka):
	''' with lead *attrs* recursively getattr(key) on *obj* for a target and with last attr do *call*(target, attr). '''
	result = default
	attrs = isstr(attrs) and attrs.split('.') or attrs
	*target_attrs, attr = attrs
	try:
		# do recursive getattr with leading attrs
		target_obj = rget_attrs(obj, target_attrs)
		
		# perform *call* on dest_obj with last key
		default_pa = (default,) if default != nothing else ()
		dest_op = isstr(call) and getattr(target_obj.__class__, call) or call
		result = dest_op(target_obj, attr, *default_pa)
	except AttributeError:
		if default == nothing:
			raise
	return result
def set_attrs(obj, attrs, value):		return rget_call_attrs(obj, setattr, attrs, value)

def irget_attrs(objs, *pa, fab=None, **ka):
	result = (rget_attrs(obj, *pa, **ka) for obj in objs)
	result = fab(result) if fab else result
	return result

def pop_attrs(objs, *pa, fab=None, **ka):	raise NotImplementedError()
def del_attrs(objs, *pa, fab=None, **ka):	raise NotImplementedError()
def ipop_attrs(objs, *pa, fab=None, **ka):	raise NotImplementedError()

def adorn(cls, vars_fill=None, **vars_fill_ka):
	vars_fill = vars_fill_ka if not vars_fill else  vars_fill if not vars_fill_ka else dict(vars_fill, **vars_fill_ka)
	for attr, mbr_fill in vars_fill.items():
		not hasattr(cls, attr) and setattr(cls, attr, mbr_fill)
	return cls

def kagroups_of_basesub(base, sub=None, ka_attr='ka_sub'):
	''' intuitive and deterministic pattern to pass parameters to multiple target objects
		within a nested hierarchy of objects
	'''
	# todo: provide explanation and usage
	ka_base_in, ka_base, ka_sub = base.ka_sub, None, None
	if ka_attr not in ka_base_in:
		ka_base,ka_sub = intogroupmap(ka_base_in, [annos(base)])
		
		# resolve:  => ka_base_in
		base.__dict__.update(ka_base)  # infuse own attrs from ka_sub
		ka_base_in.clear()
		if sub and ka_attr in annos(sub, True):
			ka_base_in.update(ka_sub=ka_sub)
	return ka_base, ka_sub
#endregion

#region mmap forms
def iitemz_of_mmap(kkvv):
	for k,vv in kkvv.items():
		for v in vv:
			yield k,v
def iritemz_of_mmap(kkvv):
	for k,vv in kkvv.items():
		for v in vv:
			yield v,k
def iitemize(key=miss,vals=miss,keyvals=miss):
	''' get itemization iter (or ->itemz) of a multi-item given as *key*:*vals* or *keyvals* '''
	key,vals = (
		(key,vals)		if not miss is key is vals else
		vals			if not miss is vals else
		throw(ValueError('Needs exactly one of either key,vals or keyvals. Got \n%s' % locals())) )
	for v in vals:
		yield key,v
def iritemize(key=miss,vals=miss,keyvals=miss):
	''' get reverse itemization iter (or ->ritemz) of a multi-item given as *key*:*vals* or *keyvals* '''
	key,vals = (
		(key,vals)		if not miss is key is vals else
		vals			if not miss is vals else
		throw(ValueError('Needs exactly one of either key,vals or keyvals. Got \n%s' % locals())) )
	for v in vals:
		yield v,key

ikvkv_of_kkvv											= iitemz_of_mmap
ivkvk_of_kkvv											= iritemz_of_mmap
ikvkv_of_kvv											= iitemize
ivkvk_of_kvv											= iritemize
#endregion

#region map gen utils
def mapseq(seq:iter, to_item:iscall=None, to_key:iscall=None, to_val:iscall=None, fab:iscall=dict):
	'''
		Note: similar to tomap except to_item must take priority in converters
	'''
	items = seq if not to_item else map(to_item, seq)
	items = items if to_key is to_val is None else itokvseq(items, to_key=to_key, to_val=to_val)
	items = items if not fab else fab(items)
	return items

def mapiidx(): raise NotImplementedError()

def mapeach(keys:iter=None, vals:list|object=None, fab=dict, at=None, val=None, ivals:iter=None, sep=' ', to_val=None):
	''' generate map of each pair of *keys* to *vals*;  out = {k:v for k,v zip(keys,vals)} '''
	if isidx(at):		return isstr(vals,ev=tss,orev=tuple)[at]
	
	# resolve inputs
	keys,vals = ivals or (keys,isnone(val,put=vals,orput=gen(val)))
	keys_out = (sep and isstr(keys) and tss(keys,sep=sep)) or keys
	vals_out = (sep and isstr(vals) and tss(vals,sep=sep)) or (isiter(vals) and vals) or gen(vals)
	vals_out = vals_out if not to_val else map(to_val,vals_out)
	
	# resolve output
	map_out = zip(keys_out, vals_out)
	map_out = map_out if not fab else fab(map_out)
	# map_out = map_out if at is None else at_map(at,map_out)
	map_out = map_out if at is None else tseqmap(map_out,at)
	return map_out

def mapof(*items_pa, keys=None, values=None, items=None, fab:iscall=dict, pass_fab=False, **ka):
	''' generate map of each pair of *keys* to *values*;  out = {k:v for k,v zip(keys,values)} '''
	# resolve:  => keys,values,items_pa;  after which this will be True: keys is None == values is None
	if items_pa and not items:
		if keys is None and items_pa:
			keys, *items_pa = items_pa		# take *keys* from items_pa when given None
		if values is None and items_pa:
			values, *items_pa = items_pa	# take *values* from items_pa when given None
	values = values if not ismap(values) else map(values.__getitem__, keys)
	
	# resolve:  keys,values => items_pa
	if keys and values:
		keys, values = isstr(keys, fab=str.split), isstr(values, fab=str.split)
		items_pa = [*zip(keys, values)], *items_pa
	fab_ka, tomap_ka = pass_fab and (ka,{}) or ({},ka)
	
	# resolve tomap transforms if args are present:  items_pa,tomap_ka => items_out
	items,items_pa = items is not None and (items, items_pa) or (items_pa[0], items_pa[1:])
	items_out = items if not tomap_ka else tomap(items, *items_pa, **tomap_ka, fab=None)
	items_out = items_out if not fab else fab(items_out, *items_pa, **fab_ka)
	return items_out

def subclsof(*cls_pa, cls=None, name=None, to_mbr=None, to_var=None, apply=None, **ka):
	to_mbr,_name,*cls_pa, = *filter(bool,[to_mbr,name]), *cls_pa[::-1]
	cls,*cls_pa = *cls_pa,cls  # when cls_pa is empty cls will not change
	def subclsofcls(cls):
		name = (_name or '{}').format(cls.__name__)
		vars_out = ((k,v) for k,v in cls.__dict__.items() if not k.startswith('_'))
		vars_out = tomap(vars_out, to_value=to_mbr, to_item=to_var, **ka)
		vars_out = vars_out if not apply else dict(vars_out, **apply)
		subcls_out = type(name, (cls,*cls.__bases__), vars_out)
		return subcls_out
	out = cls and subclsofcls(cls) or subclsofcls
	return out

imapseq,_,kmapseq,lmapseq,tmapseq						= redef_per_coll(mapseq, arg='fab',colls=DDColl.idhlt)
dmapseq													= mapseq
mapi													= mapeach
DDMapr.Ctls.fab_into									= mapeach
tosubcls												= subclsof
#endregion

#region item utils
def items_of_vals(vv,to_kv=None,to_k=None,to_v=None,enum=no,right=yes,join=None,at=-1,**ka):
	''' generate =>(key,val).. item iterable from each element in *vv* via given converters *to_kv* or *to_k*,*to_v*
		:param vv:    iterable of input items
		:param to_kv: to_item: produce output k|key,v|value from input item 
		:param to_k:  to_key:  produce output k|key from input item 
		:param to_v:  to_val:  produce output v|value from input item 
		:param enum:  prepend or append index onto each (k,v) pair
		:param right: side to affix n|index. when False/True affix to left/right ie: (n,k,v)/(k,v,n)
		:param join:  ...
		:param at:    ...
	'''
	# resolve inputs
	evals = to_kv,to_k,to_v = to_kv or ka.get('to_item'), to_k or ka.get('to_key'), to_v or ka.get('to_val')
	
	# resolve output
	vv_out = vv if not to_kv else map(to_kv,vv)
	vv_out = (
		vv_out							if not to_k and not to_v else 
		((to_k(v),v) for v in vv_out)	if to_k else
		((v,to_v(v)) for v in vv_out)	if to_v else
		((to_k(v),to_v(v)) for v in vv_out) )
	vv_out = (
		vv_out							if not enum else
		iidx(vv_out,**ka,right=right,join=isnone(join,put=any(evals))) )
	out = (to_kv,to_k,to_v,vv_out)[at]
	return out

def vv_per(kkv=None, kk=None, getr_of=None, getr='__getitem__', to_kv=no, to_aa=no, fabs=None, fab=None, **ka):
	# resolve output
	i = iter
	i = list  # fixme
	kkv = isnone(kkv,put=kk)
	getr = (
		getr_of(kkv)		if getr_of else
		getattr(kkv,getr)	if isstr(getr) else
		getr )
	def vv_of_kk(kk=None,**ka):
		kk = isnone(kk,put=kkv)
		ikv = (
			kk								if not to_kv else
			i((k,kkv[k]) for k in kk) )
		vv = i(
			map(getr,ikv)					if to_aa in AAMapr.pa else
			(getr(*kv) for kv in ikv)		if to_aa in AAMapr.ap else
			(getr(**kv) for kv in ikv)		if to_aa in AAMapr.ak else
			throw(to_aa) )
		
		vv = vv if not fabs else map(fabs,vv)
		vv = vv if not fab else fab(vv,**ka)
		return vv
	# stage output
	vv = kk is None and vv_of_kk or vv_of_kk(kk,**ka)
	return vv

def itokvseq(mapped, to_key=None, to_val=None, **ka):
	''' migrate v->*to_key*(k), v->*to_value*(v) with given *mapped* to produce a new map
		:param mapped: mapped items source
		:param to_key: ...
		:param to_val: ...
	'''
	to_val = ka.get('to_value',to_val)
	items = items_of(mapped)
	for item in items:
		item_out = to_key(item) if to_key else item, to_val(item) if to_val else item
		yield item_out

def itokvmap(mapped, to_key=None, to_val=None, **ka):
	''' migrate k->*to_key*(k), v->*to_value*(v), (k,v)->*to_item*((k,v)) with given *mapped* to produce a new map
		:param mapped: mapped items source
		:param to_key: ...
		:param to_val: ...
	'''
	to_val = ka.get('to_value',to_val)
	items_in = items_of(mapped)
	for key_in, val_in in items_in:
		item_out = to_key(key_in) if to_key else key_in, to_val(val_in) if to_val else val_in
		yield item_out

def itokvatmap(mapped, to_key=None, to_value_at:dict=None, to_value=None, at_key_out=True, keep=None):
	''' migrate k->*to_key*(k), v->*to_value*(v), (k,v)->*to_item*((k,v)) with given *mapped* to produce a new map
		:param mapped: mapped items source
		:param to_key: to_key to use when key_in. if None then yield key_in; todo: does not handle dict -> intomap
		:param to_value_at: to_value to use for each key_in (pre-transform key) {key_in:to_value,...}
		:param to_value: default to_value to use when key_in is not in to_value_at mapped. if None then yield value_in
	'''
	if not to_value_at:  return itokvmap(mapped, to_key=to_key, to_value=to_value)
	
	items_in = isdict(mapped, call=mapped.items)
	for key_in, value_in in items_in:
		key_out = to_key(key_in) if to_key else key_in
		
		at_key = key_out if at_key_out else key_in
		to_value_key = to_value_at.get(at_key, to_value)
		value_out = to_value_key(value_in) if to_value_key else value_in
		yield key_out, value_out

ikv_of_iv					= items_of_vals
#endregion

#region imap utils
def _ivalof(keys, mapper, to_item, to_val, on_miss):
	for key in keys:
		try:
			item = mapper(key)
			# coerce each value item to appropriate type
			if type(to_val) in (tuple, list):  # todo: wrap iter instead
				for to_val_n in reversed(to_val):
					item = to_val_n(item)
			elif to_val is not None:
				item = to_val(item)
			
			# coerce each key_value item to appropriate type
			if type(to_item) in (tuple, list):  # todo: wrap iter instead
				item = (key, item)
				for to_item_n in reversed(to_item):
					item = to_item_n(item)
			elif to_item is not None:
				item = to_item((key, item))
				
			yield item
		except Exception as exc:
			if on_miss is nothing: raise
			elif on_miss == 'continue': continue
			yield on_miss
def ivalof(keys, getr, to_val=None, to_item=None, on_miss=nothing):
	*to_vals_left,to_val    = not to_val  and (None,) or collany(to_val)
	if to_val:
		yield from map(to_val, ivalof(keys,getr,to_val=to_vals_left,to_item=to_item))
		return
	elif to_item:
		yield from map(to_item, zip(keys,ivalof(keys,getr)))
		return
		
	for key in keys:
		try:
			val = getr(key)
			yield val
		except Exception as exc:
			if on_miss is nothing: raise
			elif on_miss in AAMapr.cont_set: continue
			yield on_miss

def seqmap(mapd, keys, getr=None, to_item=None, to_val=None, on_miss=nothing, fab=None):
	''' get items in order of given *keys* from a *mapped*
		:param mapd:
		:param keys: subset sequence
		:param getr:
		:param to_item:
		:param to_val:
		:param on_miss: pass value of 'continue' to ignore any key error
		:param to_seq:
	'''
	# resolve inputs
	keys = isinstance(keys, str) and filter(bool, keys.split(' ')) or keys
	getr = mapperof(mapd, getr)

	# resolve values for given keys
	vals = ivalof(keys, getr, to_item, to_val, on_miss)
	vals = vals if not fab else fab(vals)
	return vals
# todo: change to map(mapped,keys) -> [[key,value]];  seqmap(mapped,keys) -> [[value]]
# todo: imap above taking mapped second violates convention of both iterutil functions and builtin map function
# todo: replace formerly named: imap, imapvalues, imapitems, imapmbrs, imapvars and all variants
def imap(keys,mapd,*pa,**ka):					return seqmap(mapd,keys,*pa,**ka)
def imapvalues(keys,mapd,*pa,**ka):				return seqmap(mapd,keys,*pa,**ka, getter='__getitem__')
def imapitems(keys,mapd,*pa,to_item=asis,**ka):	return seqmap(mapd,keys,*pa,**ka, getter='__getitem__', to_item=to_item)
def imapmbrs(keys,mapd,*pa,**ka):				return seqmap(mapd,keys,*pa,**ka, getter='__getattribute__')
def imapvars(keys,mapd,*pa,to_item=asis,**ka):	return seqmap(mapd,keys,*pa,**ka, getter='__getattribute__', to_item=to_item)
def at_map(at, mapd, **ka):
	mapd_out = (
		mapd					if at is None else
		mapd[at]				if isstr(at) else
		tseqmap(mapd,at,**ka)	if isiter(at) else
		xt(mapd.values())[at] )
	return mapd_out

def joinmaps(maps,*_maps,rev=no,fab=dict,omit=miss,**map_end):
	''' combine items in given *maps*. right most map takes priority by default unless rev=True '''
	# resolve inputs
	i_of = rev and irev or iter
	kkvs = (_maps or map_end) and (maps,*_maps,map_end) or tuple(maps)
	kkvs = omit == miss and maps or (((k,v) for k,v in zof(kkv) if not omit(v)) for kkv in kkvs)
	# kkvs = omit == miss and maps or [[(k,v) for k,v in zof(kkv) if not omit(v)] for kkv in kkvs]
	kkv_join,*kkvs,kkv_out = fab and sfx(i_of(kkvs),None) or i_of(kkvs)
	
	# join each
	kkv_join = dict(kkv_join)
	for kkv in kkvs:
		kkv_join.update(kkv)
		
	# resolve output
	if fab:
		kkv_out = fab(kkv_join)
	else:
		kkv_out = dict(kkv_out)
		kkv_out.update(kv for kv in kkv_join.items() if kv[0] not in kkv_out)
	return kkv_out

def kkvs_of_kkvv(kkvv,pair=yes,evs=dict,ev=None):
	kk = xt(kkvv.keys())
	kkvs = ((kk,vv) for vv in zip(*kkvv.values()))
	kkvs = kkvs if not pair else (zip(kk,vv) for kk,vv in kkvs)
	kkvs = kkvs if not evs else map(evs,kkvs)
	kkvs = kkvs if not ev else ev(kkvs)
	return kkvs

joinrmaps												= redef(joinmaps, rev=on)
mjoin,mjoinr = mj,mjr									= joinmaps, joinrmaps
iseqmap, dseqmap, hseqmap, lseqmap, tseqmap				= seqmap, *redef_per_coll(seqmap, arg='fab')
iattrs													= redef(seqmap,getr=rget_attrs)
isequnmap = sequnmap = unmap							= redef(seqmap,getr='pop')
dattrs, hattrs, lattrs, tattrs							= redef_per_coll(seqmap,colls=DDColl.dhlt,getr=rget_attrs)
dsequnmap, hsequnmap, lsequnmap, tsequnmap				= redef_per_coll(seqmap,colls=DDColl.dhlt,getr='pop')
dmap, hmap, lmap, tmap									= redef_per_coll(imap)
kmap													= hmap
#endregion

#region remap utils
_D.kk_obsolete			= frozenset(ss('keys_in  keys_to  keys_into  to_value'))
_to_pick				= lambda obj: getattr(obj,'pick',None)

# fixme: some of these functions are misnomers [facepalm];
# 		tomap: create new map from input map; currently acts like remap
# 		remap: assign new vals to input map;  currently acts like tomap
def iremap(mapd, to_key=asis, to_val=asis, to_item=None, to_iter=zof, fab=None, **ka):
	''' converter modifies each key-value-pair of *mapping* with transforms *to_item*((*to_key*(k), *to_value*(v))) '''
	to_val = ka.get('to_value',to_val)
	ikv = to_iter(mapd)
	ikv = ((to_key(k),to_val(v)) for k,v in ikv)
	ikv = ikv if not to_item else (to_item(*kv) for kv in ikv)
	ikv = ikv if not fab else fab(ikv)
	return ikv
def irekey(mapping, to_key, *pa, **ka):		return iremap(mapping, *pa, to_key=to_key, **ka)
def ireeval(mapping, to_val, *pa, **ka):	return iremap(mapping, *pa, to_val=to_val, **ka)

dremap, kremap, lremap, tremap							= redef_per_coll(iremap, arg='fab')
drekey, krekey, lrekey, trekey							= redef_per_coll(irekey, arg='fab')
dreeval, kreeval, lreeval, treeval						= redef_per_coll(ireeval, arg='fab')
remap, rekey, _reeval									= dremap, drekey, dreeval

_call_verb												= lambda verb,obj,*pa,**ka: getattr(obj,verb)(*pa,**ka)
def reeval(*_pa, mapd=None, eval=None, update=yes, keep=None, fab=None, _coll=yes, pa=(),ka=map0, **_ka):
	''' apply *eval* converting each val in given *mapd* 
		:param mapd:   source map for vals
		:param eval:   callable which takes val as arg0. when is str getattr on val as callable. 
		:param pa:     eval pos args after val
		:param ka:     eval key args
		:param update: update onto given mapd if True, otherwise return new key,vals 
		:param keep:   filter for which key,val pairs to eval
		:param fab:    convert output
		:param _coll:  when True collects evaluations into list for debugging
	'''
	# todo: support passing key into eval
	# resolve inputs
	mapd,*_pa = mapd is None and _pa or (mapd,*_pa)
	eval,*_pa = eval is None and _pa or (eval,*_pa)
	eval = iscx('__call__' if eval is ... or eval is None else eval,orfab=_call_verb.__get__) 
	pa,ka = pa or _pa, ka or _ka
	assert not (mapd is eval is None), (mapd,eval)
	
	# resolve output
	ikv = (_coll and list or iter)( (k,eval(v,*pa,**ka)) for k,v in zof(mapd) if not keep or keep((k,v)) )
	ikv = ikv if not update else (mapd.update(ikv), mapd)[1]
	ikv = ikv if not fab else fab(ikv)
	return ikv
def reeval_of(eval, *pa, mapd=None, **ka):
	''' stage callable *eval* converting each val in given *mapd*
		:param mapd:   vals source
		:param eval:   callable which takes val as arg0. when is str getattr on val as callable.
		:param pa:     eval pos args after val
		:param keep:   filter chooses which key,val pairs to eval
		:param ka:     eval key args
		:param update: when True update onto and return given mapd. otherwise return new key,val pairs 
		:param fab:    convert output
		:param _coll:  when True collects evaluations into list for debugging
	'''
	def reeval_of_mapd(mapd):
		ikv = reeval(mapd,eval,pa=pa,**ka)
		return ikv
	out = mapd is None and reeval_of_mapd or reeval(mapd,eval,pa=pa,**ka)
	return out

def tomap(mapped=None, to_key=None, to_val=None, to_item=None, keep=None, fab=dict, **ka):
	''' migrate k->*to_key*(k), v->*to_value*(v), (k,v)->*to_item*((k,v)) with given *mapped* to produce a new map
		:param mapped: mapped items source
		:param to_key: ...
		:param to_val: ...
		:param to_item: ...
		:param keep: ...
		:param fab: ...
	'''
	to_val = ka.get('to_value',to_val)
	if mapped is None:
		mapped,ka = ka,map0
	items = items_of(mapped)
	items = items if not keep else keep(items)
	items = (
		itovalsmap(items, to_key, to_val, **ka)		if to_val and isdict(to_val) else
		itomap(items, to_key, to_val, **ka)			if any((to_key,to_val,ka)) else
		items	)
	items = items if not to_item else map(to_item, items)
	items = items if not fab else fab(items)
	return items

def toremap(mapd, to_key=None, to_val=None, to_item=None, keep=None,**ka):
	''' migrate k->*to_key*(k), v->*to_value*(v), (k,v)->*to_item*((k,v)) within a given *mapped*
		:param mapd: mapped items source
		:param to_key: ...
		:param to_val: ...
		:param to_item: ...
		:param keep: ...
	'''
	items_out = dict(tomap(mapd, to_key=to_key, to_val=ka.get('to_value',to_val), to_item=to_item, keep=keep))
	mapd.clear()
	mapd.update(items_out)
	return mapd

def iintomap(mapped,kk_in=None,kk_to=None,kk_into=None,getter=None,to_val=None,on_miss=no,union=no,alias=no,**ka):
	''' migrate *keys_in*->*keys_to* with given *mapped* to produce a new map
		:param mapped:  source items to remap
		:param kk_in:   keys to be replaced
		:param kk_to:   keys to replace with
		:param kk_into: iterable pair or dict of keys_in,keys_to
		:param getter:  mapped value getter
		:param to_val:  ...
		:param on_miss: ignore missing key in mapped if bool(*on_miss* or *on_miss*(key)) is False
		:param union:   include items from mapped when unmatched
		:param alias:   alias rather than replace keys? Include when True, otherwise exclude. Defaults to False.
	'''
	assert not _D.kk_obsolete & set(ka), ka
	kk_in,kk_to = (
		[kk_in,kk_to]	if kk_into is None else
		kk_into			if isntdict(kk_into) else
		[[*kk_into.keys()],[*kk_into.values()]] )
	kk_in = isstr(kk_in,fab=tss)
	kk_to = isstr(kk_to,fab=tss) or kk_in
	mapped = mapped if isdict(mapped) else mapped.__dict__
	mapper = getter or mapped.__getitem__  # todo: incomplete; should be like getter(mapped)
	keys_diff = union and set(mapped)
	
	# generate values iterable for given keys
	for key_in, key_to in zip(kk_in, kk_to):
		if key_in not in mapped and (not on_miss or not (on_miss is True or on_miss(key_in))): continue
		value = mapper(key_in) if not to_val else to_val(mapper(key_in))
		yield key_to, value
		union and not alias and keys_diff.discard(key_in)
	if not union: return
	
	# include diffs in output when appropriate
	for key_diff in keys_diff:
		value = mapper(key_diff)
		yield key_diff, value
def intomap(*pa,fab=dict,**ka):		return (fab or iter)(iintomap(*pa,**ka))

def intoremap(mapped,kk_in=None,kk_to=None,kk_into=None,mapped_dst=None,getter=None,to_val=None,alias=None,**ka):
	''' migrate *keys_in* -> *keys_to* within a given *mapped*
		:param mapped: source items to migrate FROM
		:param mapped_dst: dict to migrate TO; defaults to *mapped*
		:param kk_in: keys to replace
		:param kk_to: keys to substitute
		:param kk_into: iterable pair or dict of kk_in,kk_to
		:param getter: mapped value getter
		:param to_val: ...
		:param alias: pop *keys_in* from mapped when False; defaults to False unless *mapped_dst* is given
		:param on_miss: handler for missing *keys_in*; todo: implement
	'''
	# resolve inputs
	assert not _D.kk_obsolete & set(ka), ka
	kk_in,kk_to = (
		[kk_in,kk_to]	if kk_into is None else
		kk_into			if isntdict(kk_into) else
		[[*kk_into.keys()],[*kk_into.values()]] )
	kk_in = isstr(kk_in,fab=tss)
	kk_to = isstr(kk_to,fab=tss) or kk_in
	_map_into = dict(zip(kk_in, kk_to))
	mapper = getter	or ((not alias or None is alias is mapped_dst) and mapped.pop) or mapped.__getitem__
	mapped_dst = mapped_dst if mapped_dst is not None else mapped
	
	# resolve output; update mapped_dst with given key values
	keysin_both = set(mapped) & set(kk_in)
	items_into = ((_map_into[key_in],mapper(key_in)) for key_in in keysin_both)
	items_into = items_into if not to_val else ((k,to_val(v)) for k,v in items_into)
	mapped_dst.update(items_into)
	return mapped_dst
def aliasremap(*pa,**ka):								return intoremap(*pa,**ka, alias=True)

def into_of(*pa_into,kk_in=None,kk_to=None,kk_into=None,fabs_into=None,fab_into=True,**ka):
	''' create ->kkinto which specifies input keys to be remapped to output keys '''
	# resolve inputs
	kk_into = (
		kk_into					if kk_into else
		pa_into[0]				if len(pa_into) == 1 else
		pa_into )
	if iscall(kk_into): 		return kk_into
	
	# resolve inputs
	kk_in,kk_to,*_ = map(tss,(
		[kk_in,kk_to]			if kk_in and kk_to else
		[kk_in,kk_into]			if kk_in else
		[kk_to,kk_into]			if kk_to else
		kkss_of(kk_into,**ka)	if isstr(kk_into) else
		kk_into ))
	assert len(kk_in) == len(kk_to), (kk_in,kk_to)
	
	# resolve output
	into = (kk_in,kk_to) if not fabs_into else [map(fabs_into, zip(kk_in,kk_to))]
	into = into if not fab_into else istrue(fab_into,put=DDMapr.Ctls.fab_into)(*into)
	return into
def into_of(into,to=None,fabs_into=None,fab_into=True,**ka):
	''' create ->into which specifies input keys to be remapped to output keys '''
	# resolve inputs
	if iscall(into): 			return into
	
	# resolve inputs
	ss_in,ss_to = map(tss,(
		[into,to]				if to else
		kkss_of(into,**ka)		if isstr(into) else
		into ))
	assert len(ss_in) == len(ss_to), (ss_in,ss_to)
	
	# resolve output
	into_out = (ss_in,ss_to) if not fabs_into else [map(fabs_into, zip(ss_in,ss_to))]
	into_out = into_out if not fab_into else istrue(fab_into,put=DDMapr.Ctls.fab_into)(*into_out)
	return into_out
def zinto_of(into=miss,to=miss,fab=None):
	into_out = (
		into					if istype(into,zip) else
		[into,to]				if to else
		kkss_of(into,**ka)		if isstr(into) else
		into )
	zinto_out = (
		into_out				if istype(into_out,zip) else
		Zip(into_out)			if isdict(into_out) else
		Zip(*map(tss,into_out)) )
	zinto_out = zinto_out if not fab else fab(zinto_out)
	return zinto_out

def remap_of_into(*pa_into, mapd=None, into=None, at=0, at_put=miss, alias=False, keep=None, fab=None, **ka_into):
	''' create remapper given *into* '''
	# resolve inputs
	into = _into = into or into_of(*pa_into,**ka_into)
	into = getattr(into,'get',into)
	at_put = at if at_put == miss else at_put
	alias = alias and at == 0
	
	# resolve output
	def i_into_kk(mapd):
		ikv = ikvof(mapd)
		for kv_in in ikv:
			kv_sub,kv_out = (
				[kv_in[at],list(kv_in)]					if isinstance(at, (int,slice)) else
				[at(kv_in),kv_in]						if at is not None else
				[kv_in,kv_in] )
			
			kv_sub = into(kv_sub)
			if alias and kv_in != kv_sub:
				yield kv_in
			
			if isinstance(at_put, (int,slice)):
				kv_out.__setitem__(at_put,kv_sub)
			elif at is not None:
				at_put(kv_out,kv_sub)
			else:
				kv_out = kv_sub
			yield kv_out
	def fab_into_kk(mapd,fab=fab):
		out = fab(i_into_kk(mapd))
		return out
	
	# resolve stage
	into_kk = fab is None and i_into_kk or fab_into_kk
	result = mapd is None and into_kk or into_kk(mapd)
	return result
def remap_of_fmtk(mapd, mapd_src=None, at=1, limit=8, **ka):
	''' create remapper given *into* '''
	mapd_src = mapd_src or mapd
	def into_of_fmtk(fmtk):
		s_out = fmtk.format_map(mapd_src)
		return s_out
	
	# replace all instances of format keys:
	# todo: join all string values with delim and perform all replacements at once then split with delim
	mapd_prev = None
	count = 0
	while mapd_prev != mapd_src:
		mapd_prev = mapd_src
		mapd_src = dict(remap_of_into(**ka, mapd=mapd_src, at=at, into=into_of_fmtk))
		
		count == limit and throw(f'recursion limit reach. remap_of_fmtk(limit={limit})')
		count += 1
	return mapd_src

def replace_map_links(mapd, to_val=None):
	''' replace each value that points to existing key '''
	# todo: to_val
	# todo: allow for src and sbj maps
	kkv_new = {k:mapd[v] for k,v in mapd.items() if v in mapd}
	mapd.update(kkv_new)
	return mapd
def replace_map_picks(mapd, to_pick=_to_pick,**ka):
	''' replace each value which produces a valid picker=to_pick(val) with picked=picker(self) '''
	for k,v in [*mapd.items()]:  # todo: use vv_at_kk instead
		picker = to_pick(v)
		if picker:
			picked = picker(mapd)
			mapd[k] = picked
	return mapd

ivv_per,dvv_per,hvv_per,lvv_per,tvv_per					= vv_per, *redef_per_coll(vv_per,arg='fab')
dintomap, hintomap, lintomap, tintomap					= redef_per_coll(iintomap)
aliasmap, ialiasmap										= redef(intomap,alias=yes), redef(iintomap,alias=yes)
ontoaliasmap											= aliasremap
toonmap													= toremap
ontomap = intoonmap=intodict=intoamap					= intoremap						# todo: replace formerly named
ikeys, ivalues, iitems									= ikeysmap, ivalsmap, iitemsmap	# todo: replace formerly named
itomap													= itokvmap						# todo: replace formerly named
itovalsmap												= itokvatmap					# todo: replace formerly named
#endregion

#region group utils
def init_groups_of(in_grps=None, to_grps=None, zinto_grps=None, else_grp=miss,fabs=list,**ka):
	''' output groups as a seq or map according to args '''
	grps = (
		[fabs() for n in [...,*count(in_grps)]]		if not to_grps else
		dict({k:fabs() for k in to_grps},else_grp=fabs()) )
	else_grp = else_grp if to_grps else len(in_grps)
	return grps,else_grp
def into_kgroup_of(in_grps=None, to_grps=None, zinto_grps=None, else_grp=miss, val=miss,multi=False,**ka):
	# resolve inputs:  => in_grps,to_grps
	in_grps,to_grps = (
		[in_grps,to_grps]				if in_grps and to_grps else
		[in_grps,to_grps]				if in_grps and not to_grps else
		[*zip(*zinto_grps.items())]		if isdict(zinto_grps) else
		[*zip(zinto_grps)] )
	# resolve each group:  => in_grp...,to_grp...
	in_grps = [in_grp if iscall(in_grp) else hss(in_grp).__contains__ for in_grp in in_grps]
	to_grps = in_grps and to_grps or range(len(in_grps))
	# resolve groups:  => zinto_grps
	zinto_grps = zinto_grps or tuple(zip(in_grps,to_grps))
	
	# resolve output:  => to_group
	def togroup_of_val(val,fill=else_grp):
		kk_grp = multi and []
		for in_grp,k_grp in zinto_grps:
			if in_grp(val):
				if not multi: break
				kk_grp.append(k_grp)
		else:
			# fill is miss and throw(ValueError(val))
			# k_grp = fill
			k_grp = fill if isntcall(fill) else fill(val)
			multi and kk_grp.append(fill)
		# resolve output:  => k_grp_out
		k_grp_out = k_grp if not multi else kk_grp
		return k_grp_out
	result = val is miss and togroup_of_val or togroup_of_val(val,else_grp)
	return result

def groupmap(mapped, *pa, else_grp=miss, drop_grp=..., to_key=atkey, to_val=None, fabs=dict, fab=None,**ka):
	'''	allocate each map item into a select group;  ie. => {kgrp0:{a:0,b:1}, kgrp9:{y:8,z:9}}
		:param mapped:   mapped source items to group
		:param to_grp:   getter group key of given item;  ie. group_key = to_grp(to_key(item))
		:param else_grp: group key for unallocated values
		:param drop_grp: group key(s) to drop from groups. Note: tuples are hashable so are considered a single key
		:param to_key:   item converter to get groupable key arg
		:param to_val:   item converter to get grouped val output
		:param fabs:     items converter for each group in groups
		:param fab:      dict converter for the whole of groups dict
	'''
	# resolve inputs
	into_kgrp = into_kgroup_of(*pa,**ka, else_grp=else_grp)
	grps,else_grp = init_groups_of(*pa,**ka,else_grp=else_grp)
	pa_else = (else_grp,)
	pa_drop = (istype(drop_grp, (tuple,str,bytes)) or isntiter(drop_grp)) and [drop_grp] or drop_grp
	
	# allocate into groups:  => grps
	for item in mapped.items():
		key,val = item if not to_key else to_key(item), item if not to_val else to_val(item)
		k_grp = into_kgrp(key,*pa_else)
		grp = grps[k_grp]
		grp.append(val)
	
	# resolve output:  => grps
	drop_grp is not ... and [grps.pop(drop,None) for drop in drop_grp]
	grps = grps if not fabs else tomap(grps,to_value=fabs) if isdict(grps) else [fabs(g) for g in grps]
	grps = grps if not fab else fab(grps)
	return grps
def bisectmap(mapd,into_left,*pa,**ka):	return groupmap(mapd,[into_left],*pa,**ka)
def popped(keys,mapd, *pa, fill=miss, **ka):
	drop = lambda k,*_,right=hss(keys): k not in right
	return groupmap(mapd,[drop],*pa,**ka)
def groupintomap(mapped, into_grps, else_grp=None, *pa,**ka):
	''' allocated mapped items *into_grps* given item keys
		:param mapped: mapped source items to group
		:param into_grps: group according to {in_grp:to_grp,..} or {into_grps[nth]:nth,..} given a key map or seq resp.
		:param else_grp: as last group add a map of the ungrouped keys
		:param drop_grp: group key(s) to drop from groups. Note: tuples are hashable so are considered a single key
		:param to_key: item converter to get groupable key arg
		:param to_val: item converter to get grouped val output
		:param fabs: items converter for each group in groups
		:param fab: dict converter for the whole of groups dict
	'''
	# resolve inputs
	# resolve output:  => grps
	grps_out = groupmap(mapped,into_grps,*pa,**ka,else_grp=else_grp)
	return grps_out

def keepmap(mapd=None, keep=asis, incl:bool=True, at=Ndx.val, fabs=None, fab=dict, **_mapd):
	''' keep items subset of *mapped* *keep*( item[*item_part*] ) == *eq*. return *fab*(items) if given '''
	# resolve inputs
	mapd,at = mapd or _mapd, Ndx[at]
	keep = keep if iscx(keep) else hss(keep).__contains__
	
	# resolve output
	items = (item for item in zof(mapd) if incl == (not not keep(item[at])))
	items = items if not fabs else map(fabs,items)
	items = items if not fab else fab(items) 
	return items
def keepmaprx(mapd, rx, to_str=yes, at=Ndx.key, **ka):
	keeper = re.compile(rx).search if not to_str else lambda s: re.compile(rx).search(isstr(s) and s or '.'.join(ss(s)))  # fixme: hardcoded join
	return keepmap(mapd,keep=keeper, at=at, **ka)

def keeponmap(mapd, keep=asis, incl:bool=True, at=Ndx.val):
	''' return *mapped* keeping items where *keep*( item[*item_part*] ) == *eq* '''
	# resolve inputs
	at,incl = Ndx[at], truth_at.get(incl,incl)
	
	# resolve output
	for item in [*mapd.items()]:
		if not incl(keep(item[at])):
			mapd.pop(item[Ndx.key])
	return mapd
def keeponmaprx(mapd, rx, **ka):		return keeponmap(mapd,re.compile(rx).search, **ka)

def stackmaps(*_colls, colls=None, end=nothing):
	# todo
	''''''

def splicemap(mapd,keys,fill=miss,subs=miss,copy=no,fab=None):
	''' bisect *mapd* into two maps kept & took given *keys* (maps without & with *keys* resp.)
		:param mapd: items source from which to take *keys*
		:param keys: keys to be separated
		:param fill: value to fill map of given keys when abscent from mapd source
		:param subs: value replacing each key in *mapd* source
		:param copy: when True modify copy of *mapd* else modify *mapd* directly
		:param fab:  converts mapd_keys output
	'''
	# resolve inputs
	mapd_kept = mapd if not copy else isyes(copy,put=dict)(mapd)
	fill = ismiss(fill, put=(fill,), orput=())
	
	# resolve output
	mapd_took = {k:mapd_kept.pop(k,fill) for k in keys}
	isntmiss(subs) and mapd_kept.update(dict.fromkeys(mapd_took,subs))
	mapd_took = mapd_took if not fab else fab(mapd_took)
	return mapd_kept,mapd_took

def igroup(vv, to_k=str, keep=None, at=None, incl=yes, most=None, **ka):
	''' group *vv* values by group-key from *to_k*(val). group-keys may repeat via cycles or most.
		:param vv:   values to group
		:param to_k: produces key by which values are grouped
		:param most: upper bound on group size. creates a new once reached
		:param keep: select groups to keep based on group key 
		:param incl: group is kept if keep output equals incl. default:True
		:param at:   kgroup item selector as an index. ie: (key,group)[at]  
	'''
	if keep is not None:	yield from (kv for kv in igroup(vv,**ka, to_k=to_k,most=most,at=at) if keep(kv[0])==incl)
	elif at is not None:	yield from map(lambda kgrp: kgrp[at], igroup(vv,**ka, to_k=to_k,most=most))
	if not keep is at is None:	return
	
	k_prev,n_grp,grp = miss, 0, []
	for v in vv:
		k = to_k(v)
		if (k != k_prev and k_prev is not miss) or most and most <= len(grp):
			yield k_prev,grp
			grp = []
		grp.append(v)
		k_prev = k
	else:
		if grp:
			yield k_prev,grp
def groups_of(vv=None, *pa, ev=list, **ka):
	''' collect grouped *vv* values by group-key from *to_k*(val). group-keys may repeat via cycles or most.
		:param vv:   values to group
		:param to_k: produces key by which values are grouped
		:param most: upper bound on group size. creates a new once reached
		:param keep: select groups to keep based on group key 
		:param incl: group is kept if keep output equals incl. default:True
		:param at:   kgroup item selector as an index. ie: (key,group)[at]  
	'''
	# resolve inputs
	igroup_staged = lambda vv,**_ka: igroup(vv,*pa,**ka|_ka)
	
	# resolve outputs
	vv_out = vv is None and igroup_staged or igroup(vv, *pa, **ka)
	vv_out = apply(vv_out, **ka, ev=ev)
	return vv_out

def includes(sig,incls=None,excls=None,sign=None):
	''' get ->bool checking that *sign*ed input *sig* is missing from *excls* and is found in *incls*
		:param sig:   sig to compare in incls and/or excls
		:param incls: return True  if sig in incls 
		:param excls: return False if sig in excls. takes priority over incls if given excls
		:param sign:  if given convert input sig to desired signature before any comparisons
	'''
	# resolve inputs
	sig_in,sig = sig, sig if not sign else sign(sig)
	incls,excls = excls is True and (None,incls) or (incls,excls)
	
	# resolve output
	did_incl = (
		bool(incls)						if not excls else		# prio excls; False if neither incls or excls   
		not includes(sig,incls=excls) )
	for incl in did_incl and incls or ():
		did_incl |= (
			sig in incl					if isiter(incl) else
			bool(re.search(incl,sig))	if isstr(incl) else
			incl(sig)					if iscall(incl) else
			throw(ValueError(incl)) )
	return did_incl

intogroupmap			= groupintomap
dropmap					= redef(keepmap,   incl=False)
droponmap				= redef(keeponmap, incl=False)
keepvals				= redef(keepmap,   incl=True,  fabs=atval, fab=list)
dropvals				= redef(keepmap,   incl=False, fabs=atval, fab=list)
keeprxmap,keeprxonmap	= keepmaprx, keeponmaprx
slicemap				= redef(splicemap,onto=no)
groups					= groups_of
isincl					= includes
is_excluded=isexcl		= redef(includes, excls=True)
#endregion

#region row utils
def icascade_vvs(vvs,n_cols=None,is_filled=bool,fill_idxs=None,non=False):
	''' cascade row values to subsequent rows if not *is_filled* '''
	# resolve inputs
	ivvs = iter(vvs)
	vv_out,nons = [non]*n_cols, [non]*n_cols
	is_vv_filled,fill_idxs = None, set(fill_idxs or range(n_cols-1))
	
	# cascade row values
	for vv in ivvs:
		vv = vv if n_cols == len(vv) else vv+nons
		for idx in range(n_cols):
			v = vv[idx]
			is_vv_filled = is_filled(v)
			if is_vv_filled:
				vv_out[idx] = v
			elif idx not in fill_idxs:
				break
		
		if is_vv_filled:
			yield tuple(vv_out)
def cascade_vvs(vvs,n_cols=None,has_hdr=True,at=None,fab=None,**ka):
	# resolve inputs:  => hdrs,n_cols
	ivvs,hdrs = iter(vvs),()
	if has_hdr is not False or n_cols is None:
		hdrs = hdr0,*_ = next(ivvs,[miss])
		hdr0 is miss and throw(ValueError('Expected header but cascade iter is empty'))
		has_hdr = isbool(has_hdr, orput=hdr0.startswith(SSMapr.hdr))
		ivvs = ivvs if has_hdr else afx(hdrs,ivvs)
		n_cols = isint(n_cols) and n_cols or len(hdrs)
	
	# resolve output
	ivvs = icascade_vvs(ivvs,n_cols,**ka)
	ivvs = ivvs if not fab else fab(ivvs)								# given fab_rows convert rows
	out = (ivvs,hdrs) if at is None else (ivvs,hdrs)[at]								# given fab_rows convert rows
	return ivvs,hdrs
#endregion

#region tree utils
leaf_seq = leaf,		= NamedNil('leaf'),
_no_kk,_sfx_kk,_pfx_kk	= 0,1,-1

items_of_dict			= lambda obj: isdict(obj,fab=dict.items,orput=None)
vars_of					= lambda obj: items_of_dict(getattr(obj, '__dict__', None))
ik_of					= lambda obj: isiter(obj,orput=None) if isntstr(obj) else None
ikv_of					= lambda obj: items_of_dict(obj if isdict(obj) else getattr(obj,'__dict__',None))
call_items = cx_ikv		= lambda obj: (isnttype(obj) and iscx(getattr(obj,'items',None))) and obj.items() or None
callitems_or_vars_of	= lambda obj: call_items(obj) or vars_of(obj)

def walk(step=None, node=miss, fab=None, _deepest=10, **_ka):
	''' iterate over all nested values in *items*
		:param node: container on which to walk *vals* & *subs*
		:param step: a call which gets the next step into a given *node* as ->[[itype,iter],..],ka multi-iter & kargs
		:param fill_itype: default itype for *itype,vals in ivals with empty itype prefix
		
		## notes
		step update the args for each next step into given *node*
		step may pass & interpret custom control flow args
		
		## itypes
		val:   given val         yield val
		vals:  given vals        yield from vals
		node:  given node        yield from walk(node)
		nodes: given nodes       yield from (walk(node) for node in nodes)
		zips:  given (val,node)  yield from (ijoin([val],walk(node)) for node in nodes)  (roughly)
		
		## examples
		step() => [['val','1st'], ['vals',['2nd','3rd']], ['val','4th']], {}
		step() => [['val','1st'], [['2nd','3rd']], ['val','4th']], {}
		step() => [['1st','2nd','3rd','4th']], {}
		all above produce ('1st','2nd','3rd','4th') if fill_itype='vals'
	'''
	def walk_node(node, roots=(), step=step, to_val=None, _deepest=_deepest, **ka):
		# ensure depth *limit* is not exceeded
		assert len(roots) < _deepest, f'exceeded max depth:{_deepest} node:{node} roots:{roots}'
		
		# consume to_val converter by wrapping iterable
		if to_val:
			yield from map(to_val, walk_node(node, roots=roots, step=step, **ka))
			return
		
		# get steps for given node:  => ivals
		ivals,ka_sub = step(node, roots, **dict(_ka,**ka))
		roots = ka_sub['roots'] = *roots,node
		
		for vals in ivals:
			for sub in vals:
				if sub[-1] is leaf:
					yield sub[:-1]
				else:
					yield from walk_node(sub, **ka_sub)
	def walk_node_fab(node, *pa, fab=fab, **ka):
		out = walk_node(node, *pa,**ka)
		out = out if not fab else fab(out)
		return out
	staged = node is miss and walk_node_fab or walk_node_fab(node,step=step)
	return staged

def step_map(node, **ka):
	ivals = [
		[AAZU.val,  node]		if isstr(node) else
		[AAZU.vals, node]		if isntdict(node) else
		[AAZU.zips, (((k,v),v) for k,v in node.items())] ]
	return ivals,ka
def step_val(node, **ka):
	ivals = [
		[AAZU.val,  node]		if isstr(node) else
		[AAZU.vals, node]		if isntdict(node) else
		[AAZU.nodes,node.values()] ]
	return ivals,ka
def step_keyval(node, to_node=None, **ka):
	node_in,node = node, (to_node or asis)(node)
	ivals = [
		[AAZU.val,  node]		if isstr(node) else
		[AAZU.vals, node]		if isntdict(node) else
		[AAZU.nodes,node.items()] ]
	ka.update(to_node=lambda node: node[-2:])
	return ivals,ka
def step_keys(node, roots=(), leap=no, to_ikv=ikv_of, to_ik=ik_of, _kk=(), **ka):
	''' produce =>ivals of root & item keys from given *node*
		:param node:   node from which to step and produce ivals   (explain how node includes keys)
		:param roots:  nodes branching from root to given *node*
		:param leap:   only visit leaf nodes when True
		:param to_ikv: custom priority filter for nodes containing key,val pairs
		:param to_ik:  custom secondary filter for nodes containing vals
		:param _kk:    keys for resp. roots. (internal)
	'''
	# todo: node & keys validation could assist in avoiding errors 
	# resolve inputs
	_node = node
	kk,node,itypepfx = not roots and (_kk,node,AAZU.omit) or (_kk+tuple(node[:-1]),node[-1],leap and AAZU.omit or AAZU.val)
	ka.update(to_ikv=to_ikv,to_ik=to_ik)
	
	# resolve steps
	ikv = pairs_of(to_ikv(node) or (), fab=tuple)
	ik = () if ikv else tuple(to_ik(node) or ())
	ivals = (
		#step, afx_kk,kk,kvs,vs
		[[yes, _no_kk,kk,ikv,()]]			if ikv else
		[[ no,_pfx_kk,kk, (),ik]]			if ik else		# todo: adjust afx_kk for leap
		[[ no, _no_kk,kk,]] )
	ivals = [qual_vals(*ival) for ival in ivals]
	return ivals,ka
def qual_vals(step,afx_kk, kk,kvs=None,vs=None,fab=list):
	kk,v_r = tuple(kk), not step and leaf_seq or ()
	out = (
		(kk+tuple(kv)+v_r for kv in kvs)	if kvs else
		(kk+(v,)+v_r      for  v in  vs)	if  vs else
		[kk+v_r] )
	out = (
		afx(kk+v_r,out)						if afx_kk == _pfx_kk else
		sfx(out,kk+v_r)						if afx_kk == _sfx_kk else
		out )
	out = out if not fab else fab(out)
	return out
def unqual_vals(vals,fab=list):
	pass

walk_vals = walk_v								= walk(step_val)
walk_keyvals = walk_kv							= walk(step_keyval)
walk_keys = walk_kk								= walk(step_keys)
leap_keys = leap_kk								= walk(step_keys, leap=on)
#endregion

#region obsolete
def _walk(seq, to_items=None, least=0, limit=None, src_key=None):
	''' iterate over all nested values in *items*
		:param seq: container on which to walk all nested child *items*
		:param to_items: get *child* iterator of *seq* container
		:param least: minimum walk depth
		:param limit: maximum walk depth; when given None walk depth is unlimited
		:param conf: which part of the nested structure to walk; accepts: 'vals', 'seq', 'seqvals', 'valsseq'
		:param src_key: when evaluates as True yield nested items as (src_key, value); otherwise yield value
	'''
	# ensure depth *limit* is not exceeded
	limit = limit-1 if isinstance(limit, int) else limit
	if limit == 0: return
	
	# get next level of child items; exceptions are interpreted as val or childless item
	try:
		items = iter(seq if to_items is None else to_items(seq))
	except:
		if 'vals' in conf:
			yield seq if src_key is None else (src_key, seq)
		return
	
	# handle iter('a') => 'a' infinite recursion
	item = next(items, nothing)
	if item in (items, nothing): return
	
	# yield parent *seq* if *conf* includes it
	seq_last = ()
	if least <= 0 and conf.startswith('seq'):
		yield seq if src_key is None else (src_key, seq)
	elif least <= 0 and conf.endswith('seq'):
		seq_last = (seq if src_key is None else (src_key, seq),)
	least -= 1
	
	# walk deeper on each item in items
	src_key = src_key if src_key is None else seq
	yield from walk(item, to_items, least, limit, src_key, conf)
	for item in items:
		yield from walk(item, to_items, least, limit, src_key, conf)
	yield from seq_last

def walk_flat(content, getter='DD.getter.item', to_dict=None, ctx_key=''):
	''' obsolete transition to walk '''
	if to_dict:
		content = to_dict(content)
	getter = DD.getter.coerce(getter)
	ctx_key = (ctx_key and not ctx_key.endswith('.')) and (ctx_key+'.') or ctx_key
	
	for key, value in content.items():
		if to_dict:
			value = to_dict(value)
		if value and isinstance(value, dict):
			yield from walk_flat(value, getter, to_dict=to_dict, ctx_key=ctx_key+key)
		elif getter:
			yield getter((ctx_key + key, value))
		else:
			yield (ctx_key + key, value)
   
def walk_tree(content, getter='DD.getter.item', to_dict=None):
	''' obsolete transition to walk '''
	if to_dict:
		content = to_dict(content)
	getter = DD.getter.coerce(getter)
	
	for key, value in content.items():
		if to_dict:
			value = to_dict(value)
		if value and isinstance(value, dict):
			yield list(walk_tree(value, getter, to_dict=to_dict))
		elif getter:
			yield getter((key, value))
		else:
			yield (key, value)

class ZMap:
	# todo: remove ZMap class and make functions global scope with unique naming
	def values(keys, values, fab=None, *pa, **ka):
		values = values if not fab else fab(values, *pa, **ka)
		return values
	def keysvalues(keys, values, fab=None, *pa, **ka):
		result = (keys, values)
		result = result if not fab else fab(result, *pa, **ka)
		return result
	def ikeyvalue(keys, values, fab=None, *pa, **ka):
		result = zip(keys, values)
		result = result if not fab else fab(result, *pa, **ka)
		return result
	def dkeyvalue(keys, values, fab=dict, *pa, **ka):
		result = fab(zip(keys, values), **ka)
		return result
	def keyjvalues(keys, values, fab=None, *pa,**ka):
		result = ((key,value) for key,mvalue in zip(keys, values) for value in mvalue)
		result = result if not fab else fab(result, *pa, **ka)
		return result
	def jvalues(keys, values, fab=None, *pa,**ka):
		values = ijoin(values)
		result = values if not fab else fab(values, *pa, **ka)
		return result
	def jvalues_asc(keys, values, fab=None, *pa,**ka):
		values = ijoin(values)
		result = sorted(values, *pa, **ka)
		result = result if not fab else fab(result, *pa, **ka)
		return result
	def jvalues_dsc(keys, values, fab=None, *pa,**ka):
		values = ijoin(values)
		result = sorted(values, *pa, **ka, reverse=True)
		result = result if not fab else fab(result, *pa, **ka)
		return result
	# member aliases
	(	vv, kkvv, ikv, dkv, mkjvv, jvv, jvv_asc, jvv_dsc, ) = (
		values, keysvalues, ikeyvalue, dkeyvalue, keyjvalues, jvalues, jvalues_asc, jvalues_dsc, ) = (*map(staticmethod,[
		values, keysvalues, ikeyvalue, dkeyvalue, keyjvalues, jvalues, jvalues_asc, jvalues_dsc, ]),)
ToColls = ZMap
#endregion

#region notes
''' Notes (WIP):
ikv_of_kvs	= flattenmmap
ikv			= items, map
kvs			= mmap, multimap
ivk			= rmap, revmap, derefmap demuxmap
ivsk		= rmmap, revmmap, derefmap demuxmap
'''
#endregion

#region scratch todo: RELO
class Zip(zip):
	''' reusable zip iterable '''
	def __new__(cls,*pa):
		obj = zip.__new__(cls,())
		obj._args = isdict(pa[0],orput=pa)
		obj._zvals = None
		return obj
	def __repr__(self):		return 'Zip({!s})'.format(self._zvals is None and self._args or self._zvals)
	def __iter__(self):
		zvals = self._zvals
		if self._zvals is None:
			if isinstance(self._args, dict):
				zvals = iter(self._args.items())
			else:
				self._zvals = tuple(zip(*self._args))
				zvals = iter(self._zvals)
		return zvals

''' synonyms:
asof, asin, of, tobe, place, alter, become, tomap, ontomap
tiered, heirarchy, tree, nested, org, complex, structure, domain, division, classification, layers, strata
'''
#endregion

