import sys
import re
from string import Template, Formatter

from pyutils.defs.colldefs import xt
from pyutils.defs.funcdefs import noop
from pyutils.defs.itemdefs import first, at0, Ndx
from pyutils.utilities.callbasics import *

#region strutils defs
Rxc = RXC						= re.Pattern  # compiled RegeX type
the_fmtr						= Formatter()

class DefStrs:
	#region scopes
	class Args:
		just					= left, mid, right, = (
								  -1,   0,   1,     )
		class sz:
			global _n_base,_n_at
			_n_base				= 8
			nn					= a,b,c,d,e,f,g,h,i,j = xt(_n_base << ord for ord in range(10))
			_n_at				= dict(zip('abcdefghij',nn))
			chs_few				= 'abcegj'
			nn_few				= tuple(_n_at[ch] for ch in chs_few)
	
	AA							= Args
	#endregion
	#region mbrs
	rxfind_as_group				= re.Match.group
	rxfind_as_vals				= re.Match.groups
	rxfind_as_items				= re.Match.groupdict
	fmt_rrxclss_grp				= r'[{}]'
	rxfind_asgrp_at				= {
									None: None,  # key == None yields
									**dict(zip(['vals','values','*',tuple,list],	10*[rxfind_as_vals])),
									**dict(zip(['items','**',dict],					10*[rxfind_as_items])), }
	fills						= dict(
		cls						= '.__class__',
		scls					= '.__class__.__name__',
		kcls					= '.__class__.__qualname__',
		sfunc					= '.__name__',
		kfunc					= '.__qualname__',
	)
	#endregion
	...

class DefRXStrs:
	#region scopes
	class grp:
		fmt						= r'{}([\w\.\'\"]+)\{}'
		c1 = curly				= fmt.format('{', '}')
		c2 = curlys				= fmt.format('{{', '}}')
		d2 = dollars			= fmt.format('$$', '$$')
		p2 = percents			= fmt.format('%%', '%%')
	#endregion
	#region mbrs
	fmtp_rx						= '{}'
	fmtk_k						= r'{{{k}}}'
	fmtk_rx						= r'({rx})'
	fmtk_krx					= r'(?P<{k}>{rx})'
	fmtk_in = fmtk_extrx 		= r'(?{ext}{rx})'
	fmtk_to						= r'\g<{k}>'
	fmts_prime = _,*fmtk_into	= fmtk_k,fmtk_in,fmtk_to
	fmts_kvinto					= fmtk_k,fmtk_krx,fmtk_to
	fmts_kvinto					= fmts_prime
	
	grp_ext						= r'^\W*'
	grp_k						= r'\w*'
	grp_k						= r'\w+|$'
	rxc_grp_kext				= re.compile(r'(?P<ext>{ext})(?P<k>{k})'.format(ext=grp_ext,k=grp_k))
	
	into_cls_name				= r"<class '([^']+\.)?(\w+)'>", r'\2'
	into_obj_name				= r"<([^>]+) at 0x\w{12}>", r'\1'
	type_subs					= dict(
		type_to_typename		= [r"<class '([^']+\.)?(\w+)'>", r'\2'],
		inst_to_typename		= [r"<([^>]+) at 0x\w{12}>", r'\1'],
	)
	
	utils						=      compile, finditer, findall, fullmatch, match, search, split, sub, subn, = (
								  tss('compile  finditer  findall  fullmatch  match  search  split  sub  subn' ) )
	#endregion
	...

class RXCls:
	''' python re escape chars summary:
		
		common regex positional escape codes:
		A	the start of the string.
		Z	Matches only at the end of the string.
		b	empty string, but only at the beginning or end of a word ...
		B	empty string, but only when it is not at the beginning or end of a word.
		
		common regex char class escape codes:
		s	whitespace chars (including [ \t\n\r\f\v], and others
		S	non-whitespace chars. This is the opposite of \s.
		d	unicode decimal digit
		D	non-decimal digit. This is the opposite of \d.
		w	word chars, numbers and the underscore.
		W	non-word chars. This is the opposite of \w.
		
		custom (new) regex char class:
		l	lowercase chars
		L	non-lowercase chars  # todo
		p	uppercase chars
		P	non-uppercase chars  # todo
		c	alphabet chars
		C	non-alphabet chars   # todo
		h	horizontal space chars.  including:  [ \t]
		n	prefixes with '^' for char class inversion.  ie: l => [A-Z] and nl => [^A-Z]  # fixme: does not prefix yet
		_	underscore char
	'''
	#region scopes
	global _chs_common
	_chs_common = chs_common	= tuple('sSdDwW')
	not_okay					= lambda s: re.search(r'\w+_$',s) and 'suffix of "_" for rxcls is disallowed: ${}'
	class Getr:
		def __init__(self, items,**ka):
			self.rxcls_at_ch = dict(items,**ka)
		def get(self, rx_abrv):
			rxcls = rxcls_of_rxabrv(rx_abrv,rxcls_at=self.rxcls_at_ch)
			return rxcls
		def rx_of_tmpl(self,tmpl_rxcls):
			rx = Template(tmpl_rxcls).substitute(self)
			return rx
		__call__ = __getitem__		= get
	#endregion
	#region mbrs
	chs_new						= *tss('n  _  d    l    p    c       w         '), *tss('h    s          ')
	rxclss_new					= *tss('^  _  0-9  a-z  A-Z  a-zA-Z  a-zA-Z0-9_'), *tss(' \t__ \t\n\r\f\v', sep='_')
	rxcls_at_ch = at			= dict(zip(chs_new,rxclss_new))
	rxclsesc_at_ch = esc_at		= {ch:ch in _chs_common and f'\\{ch}' or rxcls for ch,rxcls in rxcls_at_ch.items()}
	rxcls_getr = getr			= Getr(at)
	rxcls_getr_esc = getr_esc	= Getr(esc_at)
	_utils						=      compile, finditer, findall, fullmatch, match, search, split, sub, subn, = (
									[noop]*len(DefRXStrs.utils) )  # placeholders to inform intellisense; see below
	#endregion
	...

class RX:
	#region mbrs
	_utils						=      compile, finditer, findall, fullmatch, match, search, split, sub, subn, = (
									[noop]*len(DefRXStrs.utils) )  # placeholders to inform intellisense; see below
	sinto						= noop
	#endregion
	...

DDSS = _D 						= DefStrs
AASS = _A 						= DDSS.AA
DDRX = RXSS						= DefRXStrs
RXClsGetr						= RXCls.Getr
#endregion

#region str utils
def scmp_pfx(a:str,b:str=None,*cd:[str]) ->str:
	''' resolve the ->str prefix that all args *a*,*b*,.. have in common. *a* can be multiple strs. '''
	# resolve prefix len
	# assert b is not None or isntstr(a), f'invalid args. if missing {b=} then {a=},{cd=} must be seq,empty' 
	if isntstr(a):
		a,b,*cd = *a, *[b][:bool(b)], *cd
	nch_eq = 0
	for nch_eq,ch_ab in enumerate(zip(a,b)):
		if str.__ne__(*ch_ab):
			break
	
	# resolve prefix given len
	pfx_ab = nch_eq and a[:nch_eq] or ''
	pfx = (pfx_ab and cd) and scmp_pfx(pfx_ab,*cd) or pfx_ab
	return pfx

def has_str(s_sub:list|str=None, s=None, pfx:str=None,mid:str=None,sfx:str=None, jx=None):
	''' determine if *s* has exact match of *(pfx|mid|sfx)=s_sub* via pure str ops. (likely faster than regex) '''
	if s is None:		return has_str.__get__(s_sub)
	
	# resolve inputs
	s,s_sub,mid = str(s), s_sub if isntstr(s_sub) or not jx else s_sub.split(jx), mid or not (pfx or sfx)
	pfx,mid,sfx,*_ = (
		(*s_sub,'','')			if isntstr(s_sub) else
		(isyes(pfx,put=s_sub), 
		 isyes(mid,put=s_sub),
		 isyes(sfx,put=s_sub)) )
	assert pfx or mid or sfx
	
	# resolve output
	has_sub  = not pfx or s.startswith(pfx)
	has_sub &= not sfx or s.endswith(sfx)
	has_sub &= not mid or mid in s				# note: put last to avoid inefficient full str search
	return has_sub
def has_strs(s_subs, s=None, having=all, jx=',',jx_subs=';', **ka):
	''' determine if *s* has exact match of all *(pfx|mid|sfx)=s_subs* via pure str ops. (likely faster than regex) '''
	if s is None:		return has_strs.__get__(s_subs)  # todo: allow non default jx* 
	
	s_subs = s_subs if type(s_subs) is not str else sjx(s_subs, jx_subs,jx)
	has_subs = having(has_str(s_sub, s=s, jx=jx, **ka) for s_sub in s_subs)
	return has_subs

has_s,has_ss = shas,sshas		= has_str, has_strs
#endregion

#region str mod utils
def sub_chs(chs_in='', chs_to='', chs_rm='', s=None):
	xch_table = str.maketrans(chs_in, chs_to, chs_rm)
	
	def call_post(s):
		s_out = s.translate(xch_table)
		return s_out
	out = s is None and call_post or call_post(s)
	return out

def remove_chs(chs, s=None):
	# shortcut case
	if s is None:  return remove_chs.__get__(chs)
	
	# resolve output
	s0_chs_table = str.maketrans(dict.fromkeys(chs,''))
	s_out = s.translate(s0_chs_table)
	return s_out

def _into_str(sub_into:tuple|str=None, *pa, s=None, fab=None):
	''' perform str substitution of *sub_into* on str *s* 
		:param sub_into: replace instances or sub_in with sub_to; composed of the 1st 2 strings in sub_into and pa
		:param s:        source str to find and replace instances of sub_in
		:param fab:      converter of str output
	'''
	# resolve inputs
	pa = list(pa)
	sub_in,sub_to,*_,fab = (
		(sub_into,pa.pop(0),*pa)		if isstr(sub_into) else
		(*sub_into,*[fab][not fab:])
	)
	s,*_ = *pa,s 
	if s is None:		return into_str.__get__((sub_in,sub_to,fab))
	
	# resolve output
	s_out = str(s).replace(sub_in,sub_to)
	s_out = s_out if not fab else fab(s_out)
	return s_out
def into_str(sub_into:tuple|str=None, *pa, s=None, fab=None):
	''' perform str substitution of *sub_into* on str *s* 
		:param sub_into: replace instances or sub_in with sub_to; composed of the 1st 2 strings in sub_into and pa
		:param s:        source str to find and replace instances of sub_in
		:param fab:      converter of str output
	'''
	# resolve inputs
	sub_in,sub_to,pa = (
		(*sub_into[:2],pa)				if isntstr(sub_into) else	# when *sub_into* ISNT str
		(sub_into,pa[0],pa[1:]) )									# when *sub_into* IS   str
	s,*_pa,fab = (
		(*[s][not s:],*pa,None,fab)		if isntstr(sub_into) else	# when *sub_into* ISNT str then fill *s* with pa
		(s,None,*pa,*[fab][not fab:]) )								# when *sub_into* IS   str then fill *fab* with pa
	if s is None:		return into_str.__get__((sub_in,sub_to,fab))
	
	# resolve output
	s_out = str(s).replace(sub_in,sub_to)
	s_out = s_out if not fab else fab(s_out)
	return s_out
def intos_str(subs, s=None, fab=None):
	# resolve inputs
	if s is None:		return intos_str.__get__(subs)
	
	# resolve output
	s_out = s
	for sub in subs:
		s_out = into_str(sub,s=s_out)
	s_out = s_out if not fab else fab(s_out)
	return s_out

def s_of_cls(cls,to_str=repr):	return re.sub(*RXSS.into_cls_name, to_str(cls))
def s_of_obj(obj,to_str=repr):	return re.sub(*RXSS.into_obj_name, to_str(obj))
def s_of_any(obj,to_str=repr):
	result = re.sub(*RXSS.into_cls_name, to_str(obj))
	result = re.sub(*RXSS.into_obj_name, result)
	return result

def unjoin_first(s,jj:list[chr],must_have=no)->list:
	''' unjoin with first valid char in *jj* joins found in given *s*tr
		simple (& faster) regex alternative for splitting input by ONE of multiple potential separators.     
	'''
	# resolve first valid join
	jj = type(jj) is str and (jj,) or jj
	chs = set(s)
	j = first((j for j in jj if j in chs), miss)
	assert not must_have or j, f'all joins {jj=} are missing from given {s=!r}'
	
	# resolve output
	ss = s.split(j) if j else [s]
	return ss

trans_str						= sub_chs
scls,sobj,sany					= s_of_cls,s_of_obj,s_of_any
reprcls,reprobj,reprany			= s_of_cls,s_of_obj,s_of_any
sinto,ssinto					= into_str, intos_str
#endregion

#region misc str utils
def join_strs(j:str, ss:iter=None, to_iter=None, to_str=str, fab=None, **ka) ->str:
	# resolve output
	def sjoin_of_ss(ss):
		ss = ss if not to_iter else getattr(ss,to_iter)(**ka) if oftype(to_iter,str) else to_iter(ss)
		ss = ss if not to_str else map(isstr(to_str,var='format'), ss)
		s_out = (ixcx(j) and j or j.join)(ss)
		s_out = s_out if not fab else (oftype(fab,str) and fab.format or fab)(s_out)
		return s_out
	stage = ss is None and sjoin_of_ss or sjoin_of_ss(ss)
	return stage
def sjoinstrs(ss:iter, sep:str='', to_str=str, **ka) ->str:
	ss = ss if not to_str else map(to_str, ss)
	s_out = (ixcx(sep) and sep or sep.join)(ss)
	return s_out
def sjoinsepss(*pa,**ka) ->str:	return sjoin(*pa,**ka,	to_strs=strs)

def i_sjoinfirst(seq:iter, sep='', pfx='', join_first=None, **ka):
	'''
		:param seq:
		:param sep:
		:param pfx:
		:param join_first: str fmt or callable; default:'{2}{0}{3}{1}' where args 0-3 are seq[0],seq[1][,pfx,sep]
	'''
	iseq = iter(seq)
	val_prev,val = next(iseq,nothing),next(iseq,nothing)
	join_first = (isstr(join_first) and join_first.format) or join_first or '{2}{0}{3}{1}'.format

	if val_prev is nothing:
		return
	elif val is nothing:
		val = join_first(val_prev,'',pfx,'')
		yield val
	else:
		val = join_first(val_prev,val,pfx,sep)
		yield val
		yield from iseq
def i_sjoinlast(seq:iter, sep='', sfx='', join_last=None, **ka):
	'''
		:param seq:
		:param sep:
		:param sfx:
		:param join_last: str fmt or callable; default:'{0}{3}{1}{2}' where args 0-3 are seq[-2],seq[-1][,sfx,sep]
	'''
	# Note: suspected of incurring some overhead to the iteration in val_prev2,val_prev1 assignments
	iseq = iter(seq)
	val_prev1,val = next(iseq,nothing),next(iseq,nothing)
	join_last = (isstr(join_last) and join_last.format) or join_last or '{0}{3}{1}{2}'.format

	if val_prev1 is nothing:
		return
	elif val is nothing:
		val = join_last(val_prev1,'',sfx,'')
		yield val
	else:
		val_prev2,val_prev1 = val_prev1,val
		for val in iseq:
			yield val_prev2
			val_prev2,val_prev1 = val_prev1,val
		val = join_last(val_prev2,val,sfx,sep)
		yield val
def sjoininter(ss:iter, sep='', pfx=None, sfx=None, sep_pfx=None, sep_sfx=None, **ka):
	''' join with *pfx* and *sfx* in a way that makes use of the highly optimized str.join
		todo: join(or affix?) between first and/or last with unique *sep*
	'''
	add_pfx,add_sfx = not (pfx is sep_pfx is None), not (sfx is sep_sfx is None)
	sep_pfx,sep_sfx = sep if sep_pfx is None else sep_pfx, sep if sep_sfx is None else sep_sfx
	
	# apply unique seps *sep_pfx/sfx* exclusively to first and last pairs as needed
	ss = ss if not add_pfx else i_sjoinfirst(ss,sep_pfx,pfx,**ka)
	ss = ss if not add_sfx else i_sjoinlast(ss,sep_sfx,sfx,**ka)  # might be faster to preempt this somehow
	
	# join resulting strs iterator;  Note: no iteration is performed till now
	s_out = sep.join(ss)
	return s_out
def iinterseq(seq:iter, sep=nothing, pfx=nothing, sfx=nothing, term_val=nothing):
	if pfx is not term_val:
		yield pfx

	if seq is term_val:
		yield from seq
	else:
		iseq = iter(seq)
		val = next(iseq, nothing)
		if val is not term_val:
			yield val
		for val in iseq:
			yield sep
			yield val

	if sfx is not term_val:
		yield sfx

sjoin = sj						= join_strs
pjoin = pj = join_print_strs	= redef(join_strs, fab=slog)
sjinter,i_sj0,i_sj9				= sjoininter,i_sjoinfirst,i_sjoinlast
sjss							= sjoinstrs
#endregion

#region str truncate utils
_fmtc_rx_float_prec				= r'(?<![.\w])(\d*\.\d{%s})\d+'
sless_fs						= lambda s,prec=4: re.sub(_fmtc_rx_float_prec % prec, r'\1', str(s))

def sless(s_in, limit:int, to_str=str, tail='',just=no):
	# resolve inputs
	limit = just and -limit or limit
	s_out = s_in if not to_str else to_str(s_in)
	sz = len(s_out)
	
	# resolve output
	if abs(limit) < sz:
		tail = tail and isstr(tail,orput=sz < 100000 and '..{:5}' or '..{:.0e}')  # all sizes with repr with len 2+5
		s_out = just and s_out[limit:] or s_out[:limit]
		s_out = s_out if not tail else (just and '{1}{0}' or '{0}{1}').format(s_out,tail.format(sz))
	return s_out
def sless(s, limit:int, to_str=str, tail='',rjust=no):
	# resolve inputs
	# limit = just and -limit or limit
	s_out = s if not to_str else to_str(s)
	sz = len(s_out)
	limit,rjust = (
		([*limit],rjust)		if isiter(limit) else
		(-limit,on)				if limit < 0 else
		(limit,rjust) )
	
	# resolve output
	if isiter(limit) and sum(limit) < sz:
		s_out = sless(s_out,limit[0],rjust=no,tail='',to_str=no) + sless(s_out,limit[1],rjust=on,tail=tail,to_str=no)
	elif isint(limit) and abs(limit) < sz:
		tail = tail and isstr(tail,orput=sz < 100000 and '..{:5}' or '..{:.0e}')  # all sizes with repr with len 2+5
		s_out = rjust and s_out[-limit:] or s_out[:limit]
		s_out = s_out if not tail else (rjust and '{1}{0}' or '{0}{1}').format(s_out,tail.format(sz))
	return s_out
def slessa(*pa,**ka):			return sless(*pa,**ka, limit=AASS.sz.a)
def slessb(*pa,**ka):			return sless(*pa,**ka, limit=AASS.sz.b)
def slessc(*pa,**ka):			return sless(*pa,**ka, limit=AASS.sz.c)
def slesse(*pa,tail=on,**ka):	return sless(*pa,**ka, limit=AASS.sz.e, tail=tail)
def slessg(*pa,tail=on,**ka):	return sless(*pa,**ka, limit=AASS.sz.g, tail=tail)
def slessj(*pa,tail=on,**ka):	return sless(*pa,**ka, limit=AASS.sz.j, tail=tail)

def ssless(ss_in, limit:int, to_str=str, tail='', rjust=None, fab=None):
	ka = dict(limit=limit,to_str=to_str,tail=tail,rjust=rjust)
	ss_out = (sless(s_in,**ka) for s_in in ss_in)
	ss_out = ss_out if not fab else fab(ss_out)
	return ss_out
def sslessa(*pa,**ka):			return ssless(*pa,**ka, limit=AASS.sz.a)
def sslessb(*pa,**ka):			return ssless(*pa,**ka, limit=AASS.sz.b)
def sslessc(*pa,**ka):			return ssless(*pa,**ka, limit=AASS.sz.c)
def sslesse(*pa,tail=on,**ka):	return ssless(*pa,**ka, limit=AASS.sz.e, tail=tail)
def sslessg(*pa,tail=on,**ka):	return ssless(*pa,**ka, limit=AASS.sz.g, tail=tail)
def sslessj(*pa,tail=on,**ka):	return ssless(*pa,**ka, limit=AASS.sz.j, tail=tail)

_sz_few							= [dict(limit=sz,tail=AASS.sz.c <= sz and '..' or '') for sz in AASS.sz.nn_few]
slessr,sslessr					= redef(sless,rjust=True), redef(ssless,rjust=on)
slessra, slessrb, slessrc, slessre, slessrg, slessrj	= [redef(sless,**sz,rjust=on) for sz in _sz_few]
sslessra,sslessrb,sslessrc,sslessre,sslessrg,sslessrj	= [redef(ssless,**sz,rjust=on) for sz in _sz_few]
#endregion

#region fmt utils
# todo: need better formalization of naming
def fmtstar(fmt,*pa,**ka):		return (isinstance(fmt,str) and fmt.format or fmt)(*pa,**ka)
def fmt_pa(pa,ka=map0):			return fmtstar(*pa,**ka)
def fmt_ka(ka,pa=()):			return fmtstar(*pa,**ka)
def fmt_a2(a2):					return fmtstar(*a2[0],**a2[1])
def ifmt_ia(fmt,ia):
	_fmt,fmt = fmt,fmt_of(fmt)
	for a in ia:
		yield fmt(a)
def ifmt_ipa(fmt,ipa):
	_fmt,fmt = fmt,fmt_of(fmt)
	for pa,*ka in ipa:
		yield fmt(*pa,**ka or map0)
def ifmt_ika(fmt,ika):
	_fmt,fmt = fmt,fmt_of(fmt)
	for ka,*pa in ika:
		yield fmt(*pa and pa[0],**ka)
def ifmt_iaa(fmt,iaa,to_aa=no):
	_fmt,fmt = fmt,fmt_of(fmt)
	iaa = iaa if not to_aa else map(to_aa,iaa)
	for (pa,ka) in iaa:
		yield fmt(*pa,**ka)

def ifmt(fmt, vals=None, fab=None):
	fmt = fmt_of(fmt)
	ss = map(fmt,vals)
	ss = ss if not fab else fab(ss)
	return ss
def ifmtmap(fmt, vals=None, *pa,**ka):	return ifmt(fmt, vals and vals.items(), *pa,**ka)

def refmt(*fmts,rep=None, pa=None,ka=None,**_ka):
	''' recursive formatter, also last format output will be reformat *rep* times if given
		Note: prev & prev-prev format outputs are stored in ka as '_' & '__' resp.
	'''
	fmts = fmts + (rep and (rep,) or ())
	# def fmt_of_argsany(*_pa,pa=None,ka=None,**_ka):
	# 	pa,ka = pa or _pa, ka or _ka
	def refmt_of_args(*pa,**ka):
		assert fmts
		ka = dict(ka,**_ka)
		for fmt in fmts:
			if isntint(fmt):
				fmt_out = fmt.format(*pa,**ka)
				ka.update(dict(_=fmt_out,__=ka.get('_')))
			else:
				for i in range(fmt):
					fmt_out = fmt_out.format(*pa,**ka)
		return fmt_out
	staged = pa is ka is None and refmt_of_args or refmt_of_args(pa,ka)
	return staged

def fmt_terms(fmtt,terms=None,**_terms):
	''' get *fmtt* template formatted output of ^terms^ '''
	terms = _terms|terms if terms else _terms
	if not terms:	return fmtt.__get__(fmtt) 
	
	# resolve output
	fmtt = Template(fmtt)
	s_out = fmtt.safe_substitute(terms)
	return s_out

def fmt_terms_fills(fmtt,terms=None,**_terms):
	return fmt_terms(fmtt,terms=terms,**_terms|_D.fills)

_at_literal_text,_at_field_name,_at_format_spec,_at_conversion = range(4)
def scan_fmt(fmt, **ka):
	''' identify key args in given str format *fmt* '''
	ka.setdefault('ev',list)
	args = pyparse_fmt(fmt)
	keys = (arg[_at_field_name].strip('= ') for arg in args)
	keys = keys if not ka else apply(keys,**ka)
	return keys

pyparse_fmt						= the_fmtr.parse
ifmtz							= ifmtmap
substitute = fmtt				= fmt_terms
substitute_fills = fmtt_fills	= fmt_terms_fills
#endregion

#region str utils todo: refactor
# todo: are functions below essential/unreplaceable?
def left_of_s(s, sep, rx=False):
	s_out = (rx and re.split or str.split)(s, sep, 1)[0]
	return s_out
def right_of_s(s, sep, rx=False):
	s_out = (
		re.split(sep, s)[-1]		if rx else
		str.rsplit(s, sep, 1)[-1] )
	return s_out

def repr_obj(obj):
	result = SJ.jjobj_fmt(obj.__dict__.items())
	result = result.format(obj.__class__.__name__)
	return result

def repr_anno(obj):
	obj_rels =  relation.__get__(obj)
	result = SJ.jjobj_fmt(lseq(obj.__annotations__.keys(), obj_rels))
	result = result.format(obj.__class__.__name__)
	return result

def slog_of_fmt(fmt,log=print,obj=miss, **ka_log):
	# resolve inputs
	s_fmt = str(fmt)
	fmt = isstr(fmt, var='format')
	log = getattr(log,'write',log)
	
	# resolve output
	def s_of_log(*pa,**ka):
		s_out = fmt(*pa,**ka)
		log(s_out, **ka_log)
		return s_out
	s_of_log.__qualname__ = s_fmt
	result = obj is miss and s_of_log or s_of_log(obj)
	return result
def fmt_fab_of(fmt,fab, obj=miss,at_out=1,**ka_fab):
	# resolve inputs
	s_fmt = str(fmt)
	fmt = isstr(fmt, var='format')
	
	# resolve output
	def fmt_fab(*pa,**ka):
		fmt_out = fmt(*pa,**ka)
		fab_out = fab(fmt_out, **ka_fab)
		fmtfab_out = (fmt_out,fab_out)[at_out]
		return fmtfab_out
	fmt_fab.__qualname__ = s_fmt
	result = obj is miss and fmt_fab or fmt_fab(obj)
	return result

def ifmt_za(fmt,za,*pa,**ka):		return ifmt_ia(fmt,zof(za),*pa,**ka)
def ifmt_zpa(fmt,zpa,*pa,**ka):		return ifmt_ipa(fmt,zof(zpa),*pa,**ka)

ifmt_iakv,ifmt_ipakv		= ifmt_za,ifmt_zpa
slog_of_fmt					= stage_call(fmt_fab_of,  fab=print,at_out=0)
#endregion

#region rx forms
_irx_to_found_group				= lambda val, found, *pa, **ka: found.group(*pa, *ka)
_irx_to_val						= lambda val, found, *pa, **ka: val
_irx_to_rxo_val					= lambda val, found, *pa, **ka: (found,val)
# _D.not_okay						= lambda s: re.search(r'\w+_$',s) and 'suffix of "_" for rxcls is disallowed: ${}'

def _irxseq(rx, seq, to_str=str, to_val=None, if_found=True, fabs=None, pa=(), **ka):
	if fabs:
		yield from irxseq(rx,seq,to_str=to_str,to_val=to_val)
		return
	# resolve inputs
	rxc = re.compile(rx) if isinstance(rx, str) else rx
	
	# check all strs against rx
	for val in seq:
		s_val = val if not to_str else to_str(val)
		found = rxc.search(s_val)
		if bool(if_found) == bool(found):
			val = val if not to_val else to_val(val, *pa,**ka, found=found)
			yield val
# def irx_strs(rx, ss, grp=0, to_val=None, to_str=str, if_found=True, fabs=None, pa=(), **ka):
def irx_strs(rx, ss, grp=None, to_val=None, to_str=str, if_found=True, fabs=None, pa=(), **ka):
	if fabs:
		yield from irxseq(rx,ss,to_str=to_str,to_val=to_val)
		return
	# resolve inputs
	rxc = re.compile(rx) if isinstance(rx, str) else rx
	# grp = istype(grp,int|str) and (grp,) or tuple(grp) 
	grp = istype(grp,int|str) and (grp,) or isnone(grp,put=(),orfab=tuple) 
	
	# check all strs against rx
	for val in ss:
		s_val = val if not to_str else to_str(val)
		found = rxc.search(s_val)
		if bool(found) == bool(if_found):
			val = val if not grp else found.group(*grp)
			val = val if not to_val else to_val(val, *pa,**ka, found=found)  # todo: remove
			yield val
def rx_strs(*pa, ev=list, **ka):
	out = apply(irx_strs(*pa,**ka),**ka, ev=ev)
	return out

def irxmap(rxin, kvs, to_str=at0, to_item=_irx_to_rxo_val, if_found=True, grp=None, *pa, **ka):
	# resolve inputs
	rxc = re.compile(rxin) if isinstance(rxin, str) else rxin
	kvs = isdict(kvs, fab=dict.items)
	
	# check all strs against rx
	for kv_in in kvs:
		s_kv = to_str(kv_in) if to_str else kv_in
		found = rxc.search(s_kv)
		if bool(if_found) == bool(found):
			kv_out = s_kv,found.group(isnone(grp, put=found))
			out = kv_out if not to_item else to_item(kv_out)
			yield out

def keeprx(seq,rx,*pa, fab=None, **ka):
	out = irx(rx,seq, *pa,**ka)
	out = out if not fab else fab(out)
	return out
def _keepmaprx(mapd,rx,*pa, fab=None, **ka):
	out = irxmap(rx,mapd, *pa,**ka)
	out = out if not fab else fab(out)
	return out

def rx_of_rrx(rrx, exact=yes, fmt=None,fmts=None):
	''' flatten *rrx* regex ptrns to single ->rx '''
	# resolve intputs
	fmt,fmts = isstr(fmt or exact and '^({})$',var='format'), isstr(fmts,var='format')
	# resolve output
	irrx = ss(rrx) if not fmts else map(fmts,ss(rrx))
	rx = '|'.join(irrx)
	rx = rx if not fmt else fmt(rx)
	return rx

def rxcls_of_rxabrv(rx_abrv,fmt=_D.fmt_rrxclss_grp,esc=False,rxcls_at=None,not_okay=RXCls.not_okay):
	''' get regex class from given *rx_abrv* derived from char->rxcls pairs in RxCls or *rxcls_at* arg
		
		Usage: all below are quite common patterns without existing regex escape parallels
		f = rxcls_of_rxabrv
		f('l')			# => '[a-z]'
		f('_l')			# => '[_a-z]'
		f('_l',None)	# => '_a-z'
		f('pd')			# => '[A-Z0-9]'
		f('npd')		# => '[^A-Z0-9]'
		f('npd',esc=1)	# => '[^A-Z\d]'
	'''
	bad_rx = not_okay(rx_abrv)
	bad_rx and throw(bad_rx.format(rx_abrv))
	rxcls_at = rxcls_at or (esc and RXCls.esc_at) or RXCls.at
	
	rx = ''.join(rxcls_at[ch] for ch in rx_abrv)
	rx = rx if not fmt else fmt.format(rx)
	return rx
def rx_of_tmplrxcls(tmpl_rxcls,rxcls_getr=None,**ka):
	''' get template formatted regex string where each rxcls key (rx_abbr) is prefixed with '$'
		
		Usage:
		rx = rx_of_tmplrxcls(r'city/state/zip: $lp+/$p$p/$d{5}')
		assert rx ==         r'city/state/zip: [a-zA-Z]+/[A-Z][A-Z]/[0-9]{5}'
		assert re.search(rx,  'city/state/zip: Abbeville/AL/36310')
	'''
	rx = (rxcls_getr or RXCls.getr).rx_of_tmpl(tmpl_rxcls)
	return rx

def _rx_ext_of_k(k):
	ext_name = f'P<{k}>'
	ext = (
		ext_name		if k.isalnum() else		# key has only letters; shortcut condition
		RXSS.rxc_grp_kext.search(k).group('ext') or ext_name )	# key not limited to just letters; parse with regex
	return ext
def irx_of_rrxkk(rrx,kk, fmt=None, ifmt_krx=RXSS.fmts_kvinto,rxc=False):
	''' format regex ptrns & keys *rrx*,*kk*  with *fmt* with each *ifmt_krx*.  Roughly [fmt(*(map(f,rrx)) for f in ifmt_krx] '''
	if rxc:
		yield from map(re.compile, irx_of_rrxkk(rrx, fmt=fmt, kk=kk, ifmt_rx=ifmt_krx, rxc=False))
		return
	
	# resolve inputs
	kk,rrx = tss(kk), tss(rrx)
	fmtr,ifmt_krx = (fmt or ('{}'*len(rrx))).format, (fmt.format for fmt in ifmt_krx)
	
	# resolve output
	for fmtr_krx in ifmt_krx:
		rx = fmtr(*(fmtr_krx(k=k,rx=rx,ext=_rx_ext_of_k(k)) for k,rx in zip(kk,rrx)))
		yield rx
def rxs_of_rrxkk(*pa,fab=tuple,**ka):						return fab( irx_of_rrxkk(*pa,**ka))
def rx_of_rrxkk(*pa,fmt_krx=RXSS.fmtk_extrx,**ka):			return next(irx_of_rrxkk(*pa,**ka,ifmt_krx=[fmt_krx]))
def rxs_of_kkrrx(kk,rrx,*pa,fab=tuple,**ka):				return fab( irx_of_rrxkk(rrx,kk,*pa,**ka))
def rx_of_kkrrx(kk,rrx,*pa,fmt_krx=RXSS.fmtk_extrx,**ka):	return next(irx_of_rrxkk(rrx,kk,*pa,**ka,ifmt_krx=[fmt_krx]))

def sub_rrxinto(rrx_into, string):
	s_out = str(string)
	for rx_in,rx_to in rrx_into:
		s_out = re.sub(rx_in,rx_to,s_out)
	return s_out

def s_of_rxinto(rx:str, sinto:IsCx|SS, grp=0, s:str=miss):
	''' replace *rx* matches given str converter *sinto*. like re.sub except *sinto* takes str, not re.Match.
		:param rx:    regex pattern to replace
		:param sinto: match converter; usually given string but depends on grp
		:param grp:   group selector passed to sinto derived from re.Match.group(grp). default of 0 gets whole match
		:param s:     source string to search for *rx* matches
	'''
	repl_into = lambda match: sinto(match.group(grp))
	rxinto = lambda s: re.sub(rx, repl_into, s)
	out = ismiss(s, put=rxinto, orfab=rxinto)
	return out

droprx							= redef(keeprx, if_found=False)
rx_ss							= rx_strs
irx,hrx,lrx,trx					= irx_strs, *redef_per_coll(irx_strs, colls=DDColl.hlt)
drxmap,hrxmap,lrxmap,trxmap		= redef_per_coll(irxmap, colls=DDColl.dhlt)
irxz,drxz,hrxz,lrxz,trxz		= irxmap,drxmap,hrxmap,lrxmap,trxmap
srrx							= rx_of_rrx

irxseq,hrxseq,lrxseq,trxseq		= irx,hrx,lrx,trx  # todo: purge
#endregion

#region rx wrappers
is_rxfind						= lambda rxutil: getattr(rxutil,'is_rxfind',False)
s_of_fabmatch					= lambda fab,match: fab(match.group())
sofmatch_of_repl				= lambda repl: isstr(repl, orfab=s_of_fabmatch.__get__)

def _rxfind_of_rxutil(rxutil,to_repl=on):
	''' find matches of rx args with given *rxutil* and optionally get group(s) for *key(s)* in single roundtrip
		Usage:
		RX.search(rx,s)				== re.search(rx,s)
		RX.search(rx,s,key=k)		== re.search(rx,s).group(k)
		RX.finditer(rx,s,keys=kk)	== (found.group(*kk) for found in re.finditer(rx,s))
	'''
	# just give us the answer. most ppl want the str output and not some obtuse Union[re.Match,None]
	n_rxutil_args = rxutil.__code__.co_argcount
	to_repl = rxutil.__name__ == 'sub' and istrue(to_repl, put=sofmatch_of_repl)
	exc,fmt_err = ValueError,'keys={} not applicable on found={} for given re args={}'.format
	@ft.wraps(rxutil)
	def rxfind(*pa,key=None,keys=(),fill=miss,default=None,to_repl=to_repl,fabs=None,fab=None,**ka):
		'''
			:param pa:   rxutil pos args
			:param ka:   rxutil key args
			:param key:  process matches as re.Match(found).groups(key)...
			:param keys: process matches as re.Match(found).groups(*keys)...
			:param fill:
			:param fabs:
			:param fab:
			
			Note:
			key in ['vals','values','*',tuple,list]	=> re.Match(found).groups()
			key in ['items','**',dict]				=> re.Match(found).groupdict()
			otherwise								=> re.Match(found).groups(key) or re.Match(found).groups(*keys)
		'''
		# apply *key* from pa before *flags* rxutil(...,key,flags); only if there are enough pos args
		if 1 <= len(pa) >= n_rxutil_args and key is None and keys == ():
			key,*pa = pa[n_rxutil_args-1], *pa[:n_rxutil_args-1], *pa[n_rxutil_args:]
		if to_repl and rxutil.__name__ == 'sub':
			k_repl,args = 'repl' in ka and ('repl',ka) or (1,list(pa))
			args[k_repl] = to_repl(args[k_repl])
			pa,ka = islist(args) and (args,ka) or (pa,args)
		
		# find rx pattern in str
		found = _found = rxutil(*pa,**ka)
		
		# resolve found groups formatter
		*keys,_ = key,*_ = *isnone(key,put=keys,orput=[key]),None  # populates both from either: key|keys => key,keys
		grp_of_found = bool(keys) and (islist(found) and tuple.__getitem__ or _D.rxfind_asgrp_at.get(key, _D.rxfind_as_group))
		keys = keys if grp_of_found not in {_D.rxfind_as_vals,_D.rxfind_as_items} else ()  # does rxfind accept keys?
		ka_grp = dict(default=default)
		
		# resolve output as group substring(s) of keys when present
		grp = (
			found								if not grp_of_found else
			fill								if not found and fill is not miss else
			throw(exc(fmt_err(keys,found,pa)))	if not found else
			grp_of_found(found,*keys,**ka_grp)	if isntiter(found) else		# handle re.Match
			(grp_of_found(f,*keys) for f in found) )						# handle [re.Match,] or [(str,),]
		grp = grp if not fabs else map(fabs,grp)
		grp = grp if not fab else fab(grp)
		return grp
	rxfind.is_rxfind = True
	return rxfind

def stage_rxcls_util(rx_util,*pa,fab=rx_of_tmplrxcls,re_ns=re,**ka):
	rx_util = rx_util if isntstr(rx_util) else getattr(re_ns,rx_util)
	def rxutil_of_rx(*pa_rx,**ka_rx):
		# resolve inputs:  => rx
		rx = ka_rx.pop('rx',None) or ka_rx.pop('pattern',None)
		if rx is None:
			rx,*pa_rx = pa_rx
		
		# resolve output
		rx_out = rx if not fab else fab(rx)
		rxutil = rx_util(rx_out,*pa_rx,**ka_rx)
		return rxutil
	as_call = not (pa or ka)
	result = as_call and ft.update_wrapper(rxutil_of_rx,rx_util) or rxutil_of_rx(*pa,**ka)
	return result
def stage_rxcls_utils(utils=RXSS.utils,**ka):
	staged_utils = [stage_rxcls_util(util,**ka) for util in utils]
	return staged_utils

_rx_utils						= tuple(map(re.__dict__.get,RXSS.utils))
_rxfind_utils					= tuple(map(_rxfind_of_rxutil,_rx_utils))
_rxclsfind_utils				= stage_rxcls_utils(_rxfind_utils)
rx_compile,rx_finditer,rx_findall,rx_fullmatch,rx_match,rx_search,rx_split,rx_sub,rx_subn = _rxfind_utils
#endregion

#region rx adorns
def adorn_func(ns,func,fmt='{}'):
	setattr(ns,fmt.format(func.__qualname__),func)
	func.__qualname__ = '.'.join((*(istype(ns) and (ns.__name__,) or ()),fmt.format(func.__name__)))
	return func

def adorn_dir_filters(drops=(),keeps=(),obj=miss):
	drops = drops and [drop if iscx(drop) else RX.compile(drop).search for drop in tss(drops)]
	keeps = keeps and [keep if iscx(keep) else RX.compile(keep).search for keep in tss(keeps)]
	def adorn_obj_dir(obj):
		def dir_of_obj(_obj=None):
			print('dir_of_obj')
			attrs = obj.__dir_orig__()
			for drop in drops:
				attrs = (attr for attr in attrs if not drop(attr))
			for keep in keeps:
				attrs = (attr for attr in attrs if     keep(attr))
			attrs = list(attrs)
			return attrs
		def _dir_(*pa,**ka):			return ['OVERRIDDEN']
		
		if not hasattr(obj,'__dir_orig__'):
			obj = type(obj.__name__,(obj,),dict(obj.__dict__,obj))
			obj.__dir_orig__ = obj.__dir__
			obj.__dir__ = _dir_
			has_log = 'log' in dir(obj)
			assert not has_log
		return obj
	staged = obj is miss and adorn_obj_dir or adorn_obj_dir(obj)
	return staged

RX.sinto						= s_of_rxinto
adorn_funcs						= lambda ns,funcs,adorn=adorn_func,**ka: [adorn(ns,func,**ka) for func in funcs]
adorn_funcs(RX,    _rxfind_utils)
adorn_funcs(RXCls, _rxclsfind_utils)
#endregion

#region fmt utils old
def fmtaa_of(fmt, *pa, **ka):
	# obsolete
	fmt_out = fmt_of(fmt)
	def fmt_of_aa(*pa, **ka):
		s_out = fmt_out(*pa,**ka)
		return s_out
	out = (not pa and not ka) and fmt_of_aa or fmt_of_aa(*pa,**ka)
	return out

def fmt_vars(locals, fmt_str, var_ptrn=RXSS.grp.curly, sub_fmt='({} ={}); ', globals=None, **ka):
	# todo: move into or integrate with alyzutils or upgrade to python 3.10
	result = fmt_str
	
	var = re.search(var_ptrn, result)
	while var:
		value = eval(var[1], globals, locals)
		sub_str = sub_fmt.format(var[1], value)
		result = re.sub(var_ptrn, sub_str, result, 1)
		
		# search again for another potential pass
		var = re.search(var_ptrn, result)
	
	return result
def fmt_varsc2(*pa,**ka):		return fmt_vars(*pa,**ka, var_ptrn=RXSS.grp.curlys)
def fmt_varsd2(*pa,**ka):		return fmt_vars(*pa,**ka, var_ptrn=RXSS.grp.dollars)
def fmt_varsp2(*pa,**ka):		return fmt_vars(*pa,**ka, var_ptrn=RXSS.grp.percents)
def fslog(*pa,**ka):			return slog_of(**ka, msg=fmt_vars(*pa,**ka))
def fslogc2(*pa,**ka):			return slog_of(**ka, msg=fmt_vars(*pa,**ka, var_ptrn=RXSS.grp.curlys))
def fslogd2(*pa,**ka):			return slog_of(**ka, msg=fmt_vars(*pa,**ka, var_ptrn=RXSS.grp.dollars))
def fslogp2(*pa,**ka):			return slog_of(**ka, msg=fmt_vars(*pa,**ka, var_ptrn=RXSS.grp.percents))
#endregion

#region notes
'''
Template Convention
fmtt|templateformat
# snym: inform assign apply supply provide present fill
# snym: term token tag label fmtflag fmtsym

note: pyparse_fmt() derives from _string.formatter_parser(format_string)
      it yeild iterable of [literal_text, field_name, format_spec, conversion]

example:
>>> fmt = '{} {a}  {} {b!r} {c.c!s} {d=!s}'
>>> pyparse_fmt(fmt) 
[('', '2', '', None),
 (' ', 'a', '', None),
 ('  ', '0', '', None),
 (' ', 'b', '+f', 'r'),
 (' ', 'c.c', '>4s', 's'),
 (' ', 'd=', '', 's')]
>>> keys_of_fmt(fmt) 
['a', 'b', 'c.c', 'd=']
'''
#endregion

#region scratch
# snym: scan query qualify cast parse deform inform extract excerpt cull quote cite gather invert itemize account
# snym: term prerequisite
	
# subd = sinto(('time','t'), s='time timestamp close')
# subd1 = ssinto([('time','t'),('close','y')], s='time timestamp close')
# sub = ssinto([('time','t'),('close','y')])
# subd2 = sub('time timestamp close')
...
#endregion
