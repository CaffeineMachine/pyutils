import re

from pyutils.defs.colldefs import vals_of_coll, collany
from pyutils.defs.itemdefs import atcoll, at0
from pyutils.defs.numdefs import n_max
from pyutils.utilities.callbasics import *

#region iter defs
class DefsIterUtils:
	#region mode defs
	log, __dbg__		= None, False
	#endregion
	#region base defs
	repinfargs			= repinf, repinfind = 'inf', -1		# repeat args representing infinity
	repstarargs			= repstar, repstarind = '*', -2		# repeat args representing unpacked sequence
	repargs				= *repinfargs, *repstarargs
	# todo: RELO
	list_to_dict		= lambda obj: {'[0]':obj[0]} if obj and type(obj) is list else obj
	dict_unzip			= lambda d: [list(d.keys()), list(d.values())]
	dict_rezip			= lambda d: list(zip(*d))
	#endregion
	#region scopes
	class Keys:
		kk_ch			=      hdr, j_vals, j_lines,  = (
						  tss('hdr  j_vals  j_lines  ') )
	class Strs:
		ss_ch			=      hdr, j_vals, j_lines,  = (
						  tss('#    \t      \n       ') )
	class Args:
		infs			= inf, infind = 'inf', -1			# repeat args representing infinity
		stars			= star, starind = '*', -2			# repeat args representing unpacked sequence
		adds			= add, addind = '+', -3				# repeat args representing unpacked sequence
		opts			= *infs, *stars
		# todo: add '..' and ... as inf and allow * to be suffixed with a number or '..'
	class Ch:
		opts			= append, extend = ',*'
		nums			= '0123456789'
		inf				= 'i'
	class getter:
		default			= None
		key				= lambda item: item[0]
		value			= lambda item: item[1]
		item			= lambda item: item
		@classmethod
		def coerce(cls, getter):
			if isinstance(getter, str):
				getter = getattr(cls, getter)
			return getter
	
	KK,SS,AA			= Keys, Strs, Args
	reparg,repopt		= Args, Ch
	#endregion
	...

DDIU = _D				= DefsIterUtils
KKIU,SSIU,AAIU=_K,_S,_A	= DDIU.KK, DDIU.SS, DDIU.AA
#endregion

#region core utils
def coll_of_iter(iter_in,coll=list,fmt:str='_{}'):
	''' get ->coll-ector of *iter_in*. iterables given to resulting collector are sequenced immediately. '''
	if not coll:	return iter_in
	
	# resolve output collector
	@ft.wraps(iter_in)
	def colx_of(*pa,**ka):
		colx = coll(iter_in(*pa,**ka))  # collect vals into a collection|colx
		return colx
	colx_of.__name__ = fmt.format(colx_of.__name__)
	return colx_of
def colls_of_iters(iters_in,coll=list,fmt:str='_{}'):
	''' get ->coll-ectors of *iters_in*. iterables given to a resulting collector are sequenced immediately. '''
	colls = tuple(coll_of_iter(iter_in,coll,fmt=fmt) for iter_in in iters_in)
	return colls

def xseq(seq, fab, *to_items, all_items=None, pre_all_items=None, deep=None, **ka):
	''' Apply a set of functions or transforms to each layer of iterators.
		Is capable of processing any nested structure of iterables.
		:param seq: the sequence(s) of any depth on which to operate.
		:param fab: transform applied to top level iterator last but before resulting items are returned.
			None or ... behaves as a noop for current layer returning a generator of the iterator at current depth.
			In the case of None or ... subsequent to_items are applied as needed to nested items.
		:param to_items: sequence of transforms to apply to each nested iterator item. len(to_items) determines depth.
			(cast & to_items) is a single sequence of transforms applied to current & recursive scopes respectively.
		:param all_items: functions applied to each nested iterator item before item_ops. DOES NOT influence depth.
	'''
	# get result of available to_items transforms on nested item iterator first
	items, pre_to_items = seq, pre_all_items
	if pre_to_items:
		items  = pre_to_items(items)
		
	if to_items:
		ka_sub = dict(ka,all_items=all_items, pre_all_items=pre_all_items, deep=isint(deep) and deep+1 or deep)
		items  = [xseq(item, *to_items, **ka_sub) for item in items]
	
	# prepare order of transforms to apply to current item iterator
	fabs = []
	if fab not in (None, ..., 0):
		(type(fab) in (tuple, list) and fabs.extend or fabs.append)(fab)
	if all_items not in (None, ..., 0):
		(type(fab) in (tuple, list) and fabs.extend or fabs.append)(all_items)
		
	# apply ordered transforms to seq iterator
	deep is not None and ka.update(deep=deep)
	for fab_n in reversed(fabs):
		items = fab_n(items,**ka)

	return items
def xstrs(seq, fab, *pa, **ka):			return xseq(seq, fab, *pa, str, **ka)
# todo: need to rename xseq to walk+ grid, complex, plex, mux, page, matrix, seqs, lattice, toseqs; tree has different meaning

collector,collectors							= coll_of_iter,colls_of_iters
dseq, hseq, lseq, tseq							= redef_per_coll(xseq, arg=1)
iseq											= lambda seq, *pa, **ka: xseq(seq, iter, *pa, *ka)
#endregion

#region item convert utils
# todo: RELO
def atfirstright(item:list):	return item[0],		item[1:]
def atrightfirst(item:list):	return item[0],		item[1:]
def atleftlast(item:list):		return item[:-1],	item[-1]
def atlastleft(item:list):		return item[-1],	item[:-1]
def atfirstlast(item:list):		return item[0],		item[-1]
def atfirstmidlast(item:list):	return item[0],		item[1:-1],		item[-1]
def atmid(item:list):			return item[1:-1]

# todo: RELO
def toseqargs(to_items, seq:iter=None, fab=None):
	''' prepare or perform series of transforms *to_items* on *seq* '''
	if seq is None:  return toseqargs.__get__(to_items)  # prepare func which performs transforms on *seq* as first arg
	
	# perform *to_items* transforms on *seq*
	result = seq
	for to_item in to_items:
		result = map(to_item, result)
	result = result if not fab else fab(result)
	return result
def toseq(seq,*_to_items,to_items=None,**ka):	return toseqargs(to_items or _to_items,seq,**ka)
def rvvseq(seq,*to_items,to_item=tuple,**ka):	return toseqargs((*to_items,rev,*(to_item and (to_item,) or ())),seq,**ka)
def at0vvseq(seq,*to_items,to_item=None,**ka):	return toseqargs((*to_items,at0vv,*(to_item and (to_item,) or ())),seq,**ka)
def atvv0seq(seq,*to_items,to_item=None,**ka):	return toseqargs((*to_items,atvv0,*(to_item and (to_item,) or ())),seq,**ka)
def atvv9seq(seq,*to_items,to_item=None,**ka):	return toseqargs((*to_items,atvv9,*(to_item and (to_item,) or ())),seq,**ka)
def at9vvseq(seq,*to_items,to_item=None,**ka):	return toseqargs((*to_items,at9vv,*(to_item and (to_item,) or ())),seq,**ka)
def at09seq(seq,*to_items,to_item=None,**ka):	return toseqargs((*to_items,at09,*(to_item and (to_item,) or ())),seq,**ka)
def at0vv9seq(seq,*to_items,to_item=None,**ka):	return toseqargs((*to_items,at0vv9,*(to_item and (to_item,) or ())),seq,**ka)
def atmidseq(seq,*to_items,to_item=None,**ka):	return toseqargs((*to_items,atmid,*(to_item and (to_item,) or ())),seq,**ka)
# atr0lseq = lambda seq,*to_items,to_item=None,**ka:	toseqargs((*to_items,atr0l)+to_item and (to_item,) or (),seq,**ka)

at0vv,atvv0,atvv9,at9vv,at09,at0vv9 = (
atfirstright,atrightfirst,atleftlast,atlastleft,atfirstlast,atfirstmidlast, )
atfirstrightseq,atrightfirstseq,atleftlastseq,atlastleftseq,atfirstlastseq,atfirstmidlastseq = (
at0vvseq,atvv0seq,atvv9seq,at9vvseq,at09seq,at0vv9seq, )
rev												= reversed
#endregion

#region iter chars utils
def chrsl(strs, fab=None, count=1):
	''' iterate over strs and get leftmost (gets initialisms for given strs) '''
	result = (s[:count] for s in strs)
	result = fab(result) if fab else result
	return result
def chrsr(strs, fab=None, count=1):
	''' iterate over strs and get rightmost '''
	result = (s[-count:] for s in strs)
	result = fab(result) if fab else result
	return result
#endregion

#region index utils
def iidx(seq, join=False, right=False, start=0, step=1, **_):
	''' insert index with items in seq.  like enumerate with improvements. '''
	ind = start
	if not join and not right and step==1:
		yield from enumerate(seq,start)
		
	elif not join and not right:
		for val in seq:
			yield (ind, val)
			ind += step
	elif not join and right:
		for val in seq:
			yield (val, ind)
			ind += step
	elif join and not right:
		for val in seq:
			yield (ind, *val)
			ind += step
	elif join and right:
		for val in seq:
			yield (*val, ind)
			ind += step
def iidxr(*pa, right=True, **ka):				return iidx(*pa, right=right, **ka)
def iidxj(*pa, join=True, **ka):				return iidx(*pa, join=join, **ka)
def iidxjr(*pa, join=True, right=True, **ka):	return iidx(*pa, join=join, right=right, **ka)

didx,hidx,lidx,tidx								= redef_per_coll(iidx)
#endregion

#region filter utils
def groupseq(seq:iter, to_grp=bool, else_grp=miss, drop=miss, fab=None):
	''' get ->dict of groups by assigning each val in *seq* to key from *to_grp*. Roughly {to_grp(v):v for v in seq} '''
	to_grp = isint(to_grp, fab=atcoll.__get__)
	grps = else_grp != drop and {else_grp:[]} or {}
	for v in seq:
		k_grp = to_grp(v)
		grp = grps.get(k_grp) or grps.setdefault(k_grp,[])
		grp.append(v)
	drop is not miss and [grps.pop(k,None) for k in collany(drop)]
	grps_out = grps if not fab else fab(grps) if iscall(fab) else [grps.get(k,()) for k in fab]
	return grps_out

def keepseq(seq:iter, cond=None, at=None, if_is=True, fabs=None, fab=None, **_):
	''' keep vals in *seq* which satisfy *cond*
		:param seq: input values to filter from
		:param cond: converter for each value in seq input before equality is checked
		:param if_is: output of cond must equal if_is to this to keep the value
		:param at: gets input arg to *cond*;  accepts int,slice or callable
		:param fabs: converter for each value in output
		:param fab: converter for the values output
	'''
	# resolve inputs
	cond = (
		bool					if cond in (None,True) else
		neg						if cond is False else
		atcoll.__get__(cond)	if istype(cond,(int,slice)) else  # todo: remove redundancy; use at instead
		cond )
	get_qual = (
		atcoll.__get__(at)		if istype(at,(int,slice)) else
		at )
	
	# resolve output
	vals = (
		(val for val in seq if bool(cond(val)) == if_is)		if not get_qual else
		(val for val in seq if bool(cond(get_qual(val))) == if_is) )
	vals = vals if not fabs else map(fabs,vals)
	vals = vals if not fab else fab(vals)
	return vals

def keepseqs(seqs:iter, cond=True, at=0, fab=None, fabs=None):
	# resolve inputs
	cond = (
		bool					if cond in (None,True) else
		isnt					if cond is False else
		cond )
	get_qual = (
		atcoll.__get__(at)		if istype(at,(int,slice)) else
		at )
	
	# resolve output
	vals = (val for val in zip(*seqs) if cond(get_qual(val)))
	vals = vals if not fabs else map(fab,vals)
	vals = vals if not fab else fab(vals)
	return vals

def keepenum(seq:iter, *pa, as_nums=False, **ka):
	''' keep enumeration of vals in *seq* which satisfy *cond* '''
	return keepseq(enumerate(seq), *pa, at=1, fabs=as_nums and at0 or None, **ka)
def keepnum(seq:iter, *pa, **ka):
	''' keep numbering of vals in *seq* which satisfy *cond* '''
	return keepseq(enumerate(seq), *pa, at=1, fabs=at0, **ka)
def keepfirst(seq:iter, cond=bool, fill=miss, **ka):
	''' keep leftmost val in *seq* which satisfies *cond* '''
	vals = keep(seq, cond=cond, **ka)
	for val in vals:
		return val
	if fill is miss:
		raise ValueError()
	return fill
def keeplast(seq:iter, cond=bool, put=nothing, if_is=True):
	''' keep rightmost of val in *seq* which satisfies *cond* '''
	return keepfirst(reversed(seq), cond=cond, put=put, if_is=if_is)
def keepflags(seq:iter, flags, if_is=True, fill_flag=False, fab=None):
	''' keep vals in *seq* where its corresponding *flag* equals True '''
	iseq = iter(seq)
	vals = (val for flag,val in zip(flags,iseq) if flag)
	vals = vals if fill_flag else ijoin(vals,iseq)
	vals = vals if not fab else fab(vals)
	return vals
def keepats(coll, ats=None, fab=None):
	vals = (
		vals_of_coll(coll)		if ats is None else
		(coll[at] for at in ats)	)
	vals = vals if not fab else fab(vals)
	return vals

def keepof(cond,seq=None,*pa,**ka):		return keepof.__get__(cond) if seq is None else keepseq(seq,cond,*pa,**ka)
def dropof(cond,seq=None,*pa,**ka):		return dropof.__get__(cond) if seq is None else dropseq(seq,cond,*pa,**ka)

def keeps(zvals, *cond_args, keepmulti=any, if_is=True, **ka):
	''' keep each row of zipped *zvals* if they qualify together '''
	for vv in zvals:
		each_eval = [cond(item) == if_is for item, cond in zip(vv, cond_args)]
		if keepmulti(each_eval):
			yield vv

def keepnths(vals, to_key=None, nth=1, if_is=True, fabs=None, fab=None):
	''' keep *n_first* occurrences in *vals*  '''
	def ikeepnths_of(vals):
		kkn_prev = dict()
		for v in vals:
			k = v if not to_key else to_key(v)
			n_prev = kkn_prev.get(k,0)
			if n_prev < nth == if_is:
				yield v
				kkn_prev[k] = n_prev+1
	vv_out = ikeepnths_of(vals)
	vv_out = vv_out if not fabs else map(fabs,vv_out)
	vv_out = vv_out if not fab else fab(vv_out)
	return vv_out

def maskseq(seq, mask):
	for val,is_keep in zip(seq,mask):
		if is_keep:
			yield val

def maskseqs(seqs, mask, fab=None):
	result = (items for keep_mask,*items in zip(mask,*seqs) if keep_mask)
	result = result if not fab else fab(result)
	return result

def maskallseqs(seqs, mask, fab=None, **ka):
	result = [maskseq(seq,mask,**ka) for seq in seqs]
	result = result if not fab else fab(result)
	return result

# todo: RELO Keep & Drop devices
class Keep:
	invert = False
	def __init__(self, cond, fab=None, invert=None):
		self.cond = cond
		self.fab = fab
		invert is not None and setattr(self, 'invert', invert)
	def __call__(self, seq):
		items = (item for item in seq if self.cond(item) ^ self.invert)
		if self.fab:
			items = self.fab(items)
		return items
class Drop(Keep):
	invert = True

group,keep,zkeep								= groupseq, keepseq, keepseqs
dropseq = drop									= redef(keepseq, if_is=False)
ikeep, dkeep, hkeep, lkeep, tkeep				= keepseq, *redef_per_coll(keepseq)
idrop, ddrop, hdrop, ldrop, tdrop				= dropseq, *redef_per_coll(keepseq, if_is=False)
keep0,keep9										= keepfirst, keeplast
keepsall										= redef(keeps, keepmulti=all)
drops											= redef(keeps, if_is=False, keepmulti=any)
dropsall										= redef(keeps, if_is=False, keepmulti=all)
dropnths										= redef(keepnths, if_is=False)
keepif,dropif									= keepof, keepof
keepsany,dropsany								= keeps, drops
ikeepnth,dkeepnth,hkeepnth,lkeepnth,tkeepnth	= keepnths, *redef_per_coll(keepnths)
idropnth,ddropnth,hdropnth,ldropnth,tdropnth	= dropnths, *redef_per_coll(dropnths)
ifirsts,dfirsts,hfirsts,lfirsts,tfirsts			= ikeepnth,dkeepnth,hkeepnth,lkeepnth,tkeepnth
iafters,dafters,hafters,lafters,tafters			= idropnth,ddropnth,hdropnth,ldropnth,tdropnth
firsts,afters									= ifirsts,iafters
#endregion

#region iter nested utils
# todo: relo
_D.iter_seq				= iter

def accum(seq, op, init=nothing):
	items = iter(seq)
	result = next(items) if init is nothing else init
	for item in items:
		result = op(result, item)
		yield result

def accumstr(seq, op=str.__add__, init=''):
	return accum(seq, op, init)
#endregion

#region construct iter utils
def ipermute(*_seqs, seqs=None, less=True, kk=None, evs=None):
	''' generate all permutations of *seqs*
		:param seqs: sequences to permute together. each iteration selects one from each sequence
		:param less: when True/False gen least variation on the left/right like symbols <:less-than/>:greater-than
		:param evs:  converts each permutation
	'''
	# iteration modifiers
	seqs = seqs and tuple(seqs) or _seqs
	if kk and (kk:=tss(kk)):	yield from (list(zip(kk,vv)) for vv in ipermute(seqs=seqs,less=less))
	elif evs:					yield from map(evs,              ipermute(seqs=seqs,less=less))
	if kk or evs:				return
	
	# iterate: handle singular
	if len(seqs) <= 1:
		yield from seqs and seqs[0]  # handle singular
		
	# iterate: handle multiple
	elif less:
		# prepare left and last seq products
		ka = dict(less=less)
		*seqs_left,seq_last = seqs
		xseqs_left,seq_last = len(seqs_left) == 1 and zip(*seqs_left) or ipermute(*seqs_left,**ka), list(seq_last)
		
		# produce all permuations of left seq product with last seq
		for left in xseqs_left:
			for last in seq_last:
				yield (*left, last)
	else:
		# prepare first and right seq products
		ka = dict(less=less)
		seq_first,*seqs_right = seqs
		seq_first,xseqs_right = list(seq_first), len(seqs_right) == 1 and zip(*seqs_right) or ipermute(*seqs_right,**ka)
		
		# produce all permuations of right seq product with first seq
		for right in xseqs_right:
			for first in seq_first:
				yield (first, *right)

def isegments(iterable, lengths, repeats=no, **_):
	''' split iterable into segments with given lengths		segments(range(10), 2, 3) --> [[0, 1], [2, 3], [4, 5]]
		:param iterable: sequence to subdivide
		:param lengths: int or or series of ints. For the case that it is a series of ints result it may exhaust before iterable
		:param repeats: number of sub iterables to produce. Defaults to infinity when omitted.
	'''
	iterable = iter(iterable)
	if repeats:
		repeats = [] if repeats is True else [repeats]
		lengths = gens(lengths, *repeats)
	if isinstance(lengths, int):
		lengths = [lengths]
		
	for length in lengths:
		if isinstance(length, int):
			items = []
			for item in range(length):
				items.append(next(iterable))
			else:
				if items:
					yield items
		else:
			items = list(segments(iterable, length, False))
			if not items: break
			yield items
def segments(*pa, ev=list, **ka):		return (ev or iter)(isegments(*pa, **ka))

def slices(size=1, limit=None, hi=None, start=0, to_stop=0, step=None, evs=yes, ev=None, vals=None):
	# resolve inputs
	hi = hi if hi is not None or vals is None else len(vals)
	hi_start, hi_stop = (
		(n_max,n_max)		if hi is None else
		(hi,hi+size) )
	istart,istop = range(start, hi_start, size), range(start+size+to_stop, hi_stop+to_stop, size)
	
	# resolve output
	outs = zip(istart,istop, gen(step))
	outs = outs if not evs else (slice(*out) for out in outs) if evs is yes else map(evs,outs)
	outs = outs if vals is None else (vals[out] for out in outs)
	outs = outs if not ev else ev(outs)
	return outs

iperml,ipermg = iperm,_ 						= ipermute, redef(ipermute, less=False)
iperm										= ipermute	# todo: purge
#endregion

#region combine iter utils
def ijoins_cols(col,*cols, evs=tuple, **_):
	for vvv_row in zip(col,*cols):
		# row = evs(v for vvv in row_ivals for vv in vvv for v in vv)
		row = evs(v for vv in vvv_row for v in vv)
		yield row
def ijoin_cols(col,*cols, **_):
	for vvv_row in zip(col,*cols):
		for vv in vvv_row:
			yield from vv
def ijoin_sides(lefts, rights, **_):
	''' Note: similar to join_cols but simpler & faster '''
	for left, right in zip(lefts, rights): 
		yield from left
		yield from right
def ajoin_left(vals, lefts, **_):
	for left, val in zip(lefts, vals): 
		yield from left
		yield      val
def ajoin_right(vals, rights, **_):
	for val, right in zip(vals, rights): 
		yield      val
		yield from right
def ajoin_sides(vals, lefts, rights, **_):
	for left, right in zip(lefts, vals, rights): 
		yield from left
		yield      val
		yield from right
def ajoin(lefts=None, vals=None, rights=None, evs=None, ev=None):
	ka = dict(lefts=lefts, vals=vals, rights=rights)
	vv_out = (
		ijoin_sides(**ka)		if lefts and vals and rights else
		ajoin_left(**ka)		if lefts and vals            else
		ajoin_right(**ka)		if           vals and rights else
		ajoin_sides(**ka)		if lefts          and rights else
		ij(lefts or rights)		if lefts          or  rights else
		vals					if           vals            else
		throw(locals()) )
	vv_out = vv_out if not evs else map(evs,vv_out)
	vv_out = vv_out if not ev  else ev(vv_out)
	return vv_out

def izip(*seqs, fill=nothing, fills=nothing):
	''' a flexible zip function capable of both zip or itertools.zip_longest given fill(s). '''
	if fill == fills == nothing:
		yield from zip(*seqs)
	else:
		seqs = [iter(seq) for seq in seqs]
		fills = fills or ([fill] * len(seqs))
		unfinished_seq_ind = 0
		count = 0
		while True:
			zip_item = [next(seq, nothing) for seq in seqs]
			while len(seqs) > unfinished_seq_ind and zip_item[unfinished_seq_ind] is nothing:
				unfinished_seq_ind += 1
			if unfinished_seq_ind >= len(seqs):
				break

			zip_item = tuple((fill if i is nothing else i for i, fill in zip(zip_item, fills)))
			yield zip_item
			if count > 10:
				raise ValueError()
def izipr(*seqs, fill=None, fills=None, maxlen=None):
	''' zip *seqs* except aligned to the end. *fill(s)* is prefixed to iterables shorter than maxlen.
		Note: Each iterable in *seqs* without a length hint will first be resolved to a list.
	'''
	seqs = [seq if hasattr(seq, '__len__') else list(seq) for seq in seqs]
	maxlen = max(*map(len, seqs)) if maxlen is None else maxlen
	fills = fills or ([fill] * len(seqs))
	
	seqs_affixed = [iaffix(fill, seq, reps=maxlen-len(seq)) for seq, fill in zip(seqs, fills)]
	yield from zip(*seqs_affixed)

def stepper(seq, starts=2, *stop_step):
	import itertools as it
	starts = list(range(starts, *stop_step) if isinstance(starts, int) else starts)
	iterators = it.tee(seq, len(starts))
	result = zip(*[it.islice(iterator, start, None) for start, iterator in zip(starts, iterators)])
	return result

def stacked(*_seqs,seqs=None,n_max=None,fab=tuple):
	''' get vals from *seqs* with DEFINED len from 1st with val at index. len equals largest of *seqs*. '''
	# todo: n_max
	nseqs = [(len(seq),seq) for seq in map(collany,seqs or _seqs)]
	def joins_of_nseqs():
		start = 0
		for n,seq in nseqs:
			if start <= n:
				yield seq[start:n]
			start = n
	seq_out = ijoin(joins_of_nseqs())
	seq_out = seq_out if not fab else fab(seq_out)
	return seq_out
def stackwith(seq,seq_fill=None,n=None,**ka):
	''' get vals from 2 *seqs* with DEFINED len from 1st with val at index. len equals largest of *seqs*. '''
	seq_fill = seq_fill if n is None else n*(seq_fill,)
	return stacked(seq,seq_fill,**ka)

def istacked(*_iters, iters=None, end=nothing):
	''' get vals from *iters* with UNDEFINED len from 1st with val at index. len equals largest of *iters* '''
	# resolve inputs
	iters = iters or _iters
	iters, item_out = list(map(iter,iters)), end
	
	# generate items for 3 or more iters
	while len(iters) > 2:
		for ind in range(len(iters),-1,-1):
			item = next(iters[ind],end)
			if item is end:
				iters.pop(ind)
			else:
				item_out = item
		if item_out is end:
			return  #  all iterators are empty; exit immediately
		yield item_out
	
	# iterate more simply 
	if   len(iters) == 2:
		yield from istackwith(*iters)
	elif len(iters) == 1:
		yield from iters[0]

def istackwith(iter_a=None,iter_b=None, n=None, *, iters=None, end=nothing):
	''' get vals from 2 *iters* with UNDEFINED len from 1st with  val at index. len equals largest of *iters* '''
	iter_b = iter_b if n is None else ngen(n,iter_b)
	iter_a,iter_b = map(iter,iters is None and (iter_a,iter_b) or iters)
	while True:
		item_a,item_b = next(iter_a,end),next(iter_b,end)
		if   item_a is not end:
			yield item_a
		elif item_b is not end:
			yield item_b
		else:
			yield from item_b is end and iter_a or iter_b
			break

def stackseqs(*_seqs, seqs=None, end=nothing):
	# todo
	''''''

dzip,hzip,lzip,tzip								= redef_per_coll(izip)
dzipr,hzipr,lzipr,tzipr							= redef_per_coll(izipr)
#endregion

#region iter mutate utils
#endregion utils

#region misc iter utils
def islide(seq, size=2,start=0,pfxs=None,sfxs=None,loop=no,fabs=None,**_):
	''' yield each consecutive window with *size* in *seq*.  ie: [*slide(range(4))] = [[0,1], [1,2], [2,3]] '''
	assert size >= 1, f'sliding window size must be at least 1. Got {size}'
	if fabs:
		yield from map(fabs,islide(seq, size=size,start=start,pfxs=pfxs,sfxs=sfxs))
		return
	
	# resolve inputs
	iseq = iter(seq) if not (pfxs or sfxs) else ij(pfxs or (), seq, sfxs or ())
	iseq = iseq if not loop else gens(iseq,loop)
	start and [next(iseq) for i in range(start)]
	if size == 2:
		# resolve values: optimized loop for most common case where size == 2
		v_left = next(iseq)
		for v_right in iseq:
			yield v_left,(v_left:=v_right)
	else:
		# resolve values: generalized loop for all cases given any size
		vv_left = [v_left for ind,v_left in zip(range(size-1), iseq)]
		for v_right in iseq:
			vv_left.append(v_right)
			yield tuple(vv_left)  # note: must yield each new iter & not many pointers to single iter
			vv_left.pop(0)
def islide3(seq, start=0,loop='todo',fabs=None,**_):
	''' yield each consecutive window with *size* in *seq*.  ie: [*slide(range(4))] = [[0,1], [1,2], [2,3]] '''
	if fabs:
		yield from map(fabs,islide3(seq, start=start))
		return
	
	# resolve inputs
	iseq = iter(seq)
	start and [next(iseq) for i in range(start)]
	# resolve values: optimized loop for case where size == 3
	v_left = next(iseq)
	v_mid = next(iseq)
	for v_right in iseq:
		yield v_left,v_mid,v_right
		v_left,v_mid = v_mid,v_right

def irotate(*pa,loop=yes,**ka):				return islide(*pa,**ka,loop=loop)

def ilen(n:int, seq, fill=None, fills=()):
	''' get iter of given *len*gth '''
	if n is None:
		yield from seq
		return
	
	iseq = fills and ijoin(seq, fills) or iter(seq)
	count,n = 0, isiter(n,fab=len)
	assert n >= 0
	while count != n:
		item, count = next(iseq, fill), count + 1
		yield item

def ilen_stack(length, *_seqs, seqs=None, fill=None, empty=..., decr_of=None):
	''' get iter of given *len*gth from source seqs in given priority '''
	# resolve inputs
	inds = length
	seqs = seqs or _seqs
	seq = (len(seqs) == 1 and seqs[0]) or (len(seqs) >= 2 and stackseqs(*seqs,fill=fill)) or ()  # todo: stackseqs
	
	# yield from *seq* until *seq* or inds are exhausted
	iinds, iseq = iter(range(length,0,-1)), iter(seq)
	value = next(iseq,empty)
	inds -= 1 if not decr_of else decr_of(value)
	while inds >= 0 and value is not empty:
		yield value
		value = next(iseq,empty)
		inds -= 0 if value is empty else 1 if not decr_of else decr_of(value)
	
	# yield *fill* until inds are exhausted
	for inds in range(inds is not empty and inds or 0,0,-1):
		yield fill

def every_nth(n, stop=DDBasic.istop_default, start=0, first=False, off=False, on=True, seq=None, not_nth=nothing):
	n = int(n)
	if stop is not None:
		# apply istop to every_nth iter to get subset every_nth[start:stop]
		yield from istop(stop, every_nth(n,None,start=start,first=first,off=off,on=on,seq=seq,not_nth=not_nth))
	
	elif seq is not None:
		# apply every_nth as mask of *seq* when given
		i_isnth = every_nth(n,None,start=start,first=first,off=off,on=on)
		if not_nth is nothing:
			yield from maskseq(seq, i_isnth)
		else:
			for val in seq:
				for is_nth in i_isnth:
					if is_nth: break
					yield not_nth
				yield val
	
	else:
		# perform iteration:  yield once every n
		if start:
			yield from genof(off,start)
		if first:
			yield on
		offs = (off,)*(n-1)
		while True:
			yield from offs
			yield on
def every_nth_1st(*pa, first=True, **ka):	return every_nth_1st(*pa, first=True, **ka)
def every_nth_of_nseq(n,*pa,seq=None,fab=None,**ka):
	def call_post(seq,fab=fab):
		ievery_n = every_nth(n,*pa,seq=seq,**ka)
		ievery_n = ievery_n if not fab else fab(ievery_n)
		return ievery_n
	result = call_post if seq is None else call_post(seq)
	return result

def bundler(seqs:[iter], size:int, schedule:{int,iter}):
	''' at most *size* ... '''
	# bundler: at_most_per
	scheduler = (iter(schedule) if isntint(schedule) else every_nth(schedule, first=False)).__next__
	queue = []
	for seq in seqs:
		queue.extend(seq)
		cue = scheduler()
		if cue:
			bundle,queue[:] = queue[:size],queue[size:]
			yield bundle

seqlen,itolenseq								= ilen, ilen_stack
slide,dslide,hslide,lslide,tslide				= islide,*redef_per_coll(islide)
rotate,drotate,hrotate,lrotate,trotate			= irotate,*redef_per_coll(islide,loop=True)  # fixme
#endregion

# #region misc str utils
# def join_strs(j:str, ss:iter=None, to_iter=None, to_str=str, fab=None, **ka) ->str:
# 	# resolve output
# 	def sjoin_of_ss(ss):
# 		ss = ss if not to_iter else getattr(ss,to_iter)(**ka) if oftype(to_iter,str) else to_iter(ss)
# 		ss = ss if not to_str else map(to_str, ss)
# 		s_out = (ixcx(j) and j or j.join)(ss)
# 		s_out = s_out if not fab else (oftype(fab,str) and fab.format or fab)(s_out)
# 		return s_out
# 	stage = ss is None and sjoin_of_ss or sjoin_of_ss(ss)
# 	return stage
# def join_strs_out(j:str, ss:iter=None, to_iter=None, to_str=str, fab=None, **ka) ->str:
# 	# resolve output
# 	def sjoin_of_ss(ss):
# 		ss = ss if not to_iter else getattr(ss,to_iter)(**ka) if oftype(to_iter,str) else to_iter(ss)
# 		ss = ss if not to_str else map(to_str, ss)
# 		s_out = (ixcx(j) and j or j.join)(ss)
# 		s_out = s_out if not fab else (oftype(fab,str) and fab.format or fab)(s_out)
# 		return s_out
# 	stage = ss is None and sjoin_of_ss or sjoin_of_ss(ss)
# 	return stage
# def sjoinstrs(ss:iter, sep:str='', to_str=str, **ka) ->str:
# 	ss = ss if not to_str else map(to_str, ss)
# 	s_out = (ixcx(sep) and sep or sep.join)(ss)
# 	return s_out
# def sjoinsepss(*pa,**ka) ->str:	return sjoin(*pa,**ka,	to_strs=strs)
# 
# def i_sjoinfirst(seq:iter, sep='', pfx='', join_first=None, **ka):
# 	'''
# 		:param seq:
# 		:param sep:
# 		:param pfx:
# 		:param join_first: str fmt or callable; default:'{2}{0}{3}{1}' where args 0-3 are seq[0],seq[1][,pfx,sep]
# 	'''
# 	iseq = iter(seq)
# 	val_prev,val = next(iseq,nothing),next(iseq,nothing)
# 	join_first = (isstr(join_first) and join_first.format) or join_first or '{2}{0}{3}{1}'.format
# 
# 	if val_prev is nothing:
# 		return
# 	elif val is nothing:
# 		val = join_first(val_prev,'',pfx,'')
# 		yield val
# 	else:
# 		val = join_first(val_prev,val,pfx,sep)
# 		yield val
# 		yield from iseq
# def i_sjoinlast(seq:iter, sep='', sfx='', join_last=None, **ka):
# 	'''
# 		:param seq:
# 		:param sep:
# 		:param sfx:
# 		:param join_last: str fmt or callable; default:'{0}{3}{1}{2}' where args 0-3 are seq[-2],seq[-1][,sfx,sep]
# 	'''
# 	# Note: suspected of incurring some overhead to the iteration in val_prev2,val_prev1 assignments
# 	iseq = iter(seq)
# 	val_prev1,val = next(iseq,nothing),next(iseq,nothing)
# 	join_last = (isstr(join_last) and join_last.format) or join_last or '{0}{3}{1}{2}'.format
# 
# 	if val_prev1 is nothing:
# 		return
# 	elif val is nothing:
# 		val = join_last(val_prev1,'',sfx,'')
# 		yield val
# 	else:
# 		val_prev2,val_prev1 = val_prev1,val
# 		for val in iseq:
# 			yield val_prev2
# 			val_prev2,val_prev1 = val_prev1,val
# 		val = join_last(val_prev2,val,sfx,sep)
# 		yield val
# def sjoininter(ss:iter, sep='', pfx=None, sfx=None, sep_pfx=None, sep_sfx=None, **ka):
# 	''' join with *pfx* and *sfx* in a way that makes use of the highly optimized str.join
# 		todo: join(or affix?) between first and/or last with unique *sep*
# 	'''
# 	add_pfx,add_sfx = not (pfx is sep_pfx is None), not (sfx is sep_sfx is None)
# 	sep_pfx,sep_sfx = sep if sep_pfx is None else sep_pfx, sep if sep_sfx is None else sep_sfx
# 
# 	# apply unique seps *sep_pfx/sfx* exclusively to first and last pairs as needed
# 	ss = ss if not add_pfx else i_sjoinfirst(ss,sep_pfx,pfx,**ka)
# 	ss = ss if not add_sfx else i_sjoinlast(ss,sep_sfx,sfx,**ka)  # might be faster to preempt this somehow
# 
# 	# join resulting strs iterator;  Note: no iteration is performed till now
# 	s_out = sep.join(ss)
# 	return s_out
# def iinterseq(seq:iter, sep=nothing, pfx=nothing, sfx=nothing, term_val=nothing):
# 	if pfx is not term_val:
# 		yield pfx
# 
# 	if seq is term_val:
# 		yield from seq
# 	else:
# 		iseq = iter(seq)
# 		val = next(iseq, nothing)
# 		if val is not term_val:
# 			yield val
# 		for val in iseq:
# 			yield sep
# 			yield val
# 
# 	if sfx is not term_val:
# 		yield sfx
# 
# sjoin = sj										= join_strs
# pjoin = pj										= join_strs_out
# sjinter,i_sj0,i_sj9								= sjoininter,i_sjoinfirst,i_sjoinlast
# sjss											= sjoinstrs
# #endregion

#region iter helpers
class Iter:
	''' simple wrapper which provides default for empty iter and propertys to get the next '''
	__slots__									= '_iter', '_fill', '_fills'
	next_val									= property(lambda self: self._iter.__next__())
	# _fill,_fills								= (), None
	@property
	def next(self):
		fill = self._fill if self._fill else next(self._fills,*self._fill) or self._fill
		val = next(self._iter,*fill)
		return val
	def __init__(self, vals,fill=...,fills=...):
		self._iter = iter(vals)
		if fill is not ...:
			self._fill = (fill,)
		if fills is not ...:
			self._fills = iter(fills)
	def __next__(self):					return next(self._iter,*self._fill)
	def __iter__(self, *pa,**ka):		return self
#endregion

#region notes
''' Function based Iterables & Accumulators
Naming Convention:
- leading 'i' returns iterable of each operation called
- no leading 'i' means the last presumably aggregate result is returned
- 'calls' plural means function uses multiple operations
- 'arg' sgl. means input args do not change for each operation called
- 'args' plr. means inputs are a sequence of args corresponding to each operation called
- 'recall' means use result as input arg for the following operation called
- 'recalls' ...

For example:
- builtin map is represented as icallargs.
- builtin reduce is represented as recallargs.
- 'icallsargs' behaves like zip but paired inputs yields a0(b0) for each item rather than tuple (a0, b0)
'''

''' synonyms/associations:
roll  rotate, slide, conveyor, funnel, pipe, span, belt
'''

'''
- todo: it would be really nice to have safe finite iterators to avoid crashes due to iterator bugs or erroneous args

'''
#endregion
