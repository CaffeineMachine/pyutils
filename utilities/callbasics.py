import functools as ft
from types import FunctionType

from pyutils.defs.chardefs import s_lower,s_upper
from pyutils.defs.funcdefs import asis,throw
from pyutils.defs.colldefs import DDColl,map0,xaptuple,redef_per_coll
from pyutils.defs.typedefs import *

#region callbasics defs
irev								= reversed

class DefsBasic:
	#region colls
	istop_default					= 100_000
	kkinto_ss						= {'<EMPTYSTR>':'', '<STR0>':''}
	kk_sub							= '<SEP>', '<J>', '<JOIN>'
	mod_prio						= (*'ltok rtok ktok ktol ktor'.split(),)
	mod_set							= set(mod_prio)
	#endregion
	#region calls
	excl_k							= lambda k: not k or '-' in k
	#endregion
	#region namespaces
	class ch:
		opts						= append, extend = ',*'
		nums						= '0123456789'
		inf							= 'i'
	class HasArg:
		pa_quota					= lambda quota=1,*pa,**ka:				len(pa) >= quota
		stages_quota				= lambda quota=1,*pa,_stages=0,**ka:	_stages >= quota
		pa_1						= pa_quota.__get__(1)
		pa_2						= pa_quota.__get__(2)
		stages_1					= stages_quota.__get__(1)
		stages_2					= stages_quota.__get__(2)
	#endregion
	...

DDBasic = _D						= DefsBasic
HasArg								= DefsBasic.HasArg
#endregion

#region iter basics
def counter(start=0,step=1):
	''' count indefinitely by *step* from *start* '''
	# todo: this is known to be slower than range. can this be replaced with range or remix of range
	idx = start
	while True:
		yield idx
		idx += step
def count(length=None,start=None,stop=None,rev=None):
	''' create int iter of given *length* between *start* OR *stop*
		:param length: number of vals in resulting iterable
		:param start: ... (mutually exclusive to stop)
		:param stop: ... (mutually exclusive to start)
	'''
	# resolve inputs
	assert length is None or start is None or stop is None, "vector int iter requires no more than 2 of: a,b,len(a,b)"
	length = (
		stop-start						if stop is not None and start is not None else
		length							if length is None else
		len(length)						if isiter(length) else
		int(length or 0) )
	step,adv = (rev or (length and length < 0)) and (-1,reversed) or (1,asis)
	length = length and abs(length)
	
	# resolve output: (assume start of None to be 0)
	icount = (
		counter(start or 0,step)		if length is stop is None else
		range(start,stop,step)			if stop is not None and start is not None else
		adv(range(stop-length,stop))	if stop is not None else
		adv(range(start or 0,(start or 0)+length)) )
	return icount

def genof(val=miss,call=miss,fab=miss,pa=(),ka=map0):
	''' generate infinite *val*s '''
	if val is not miss:
		while True:
			yield val
	elif call is not miss:
		while True:
			yield call()
	elif fab is not miss:
		while True:
			yield fab(*pa,**ka)
	else:
		throw(ValueError())
def ngen(n, val=None):
	''' generate *n* total *val*s '''
	for nth in range(n-1,-1,-1):
		yield val

def gen(val=None, n:[int,str]=_D.ch.inf):
	''' generate *val* a total of *n* times
		:param val: item to repeat in iterable
		:param n: string like regex '[,*]?[\d+]?' or int count times to duplicate part; numbers imply append by count
	'''
	# resolve inputs
	extend = False  # else append
	if isinstance(n, str):
		insertion,*num = n or _D.ch.inf
		extend,num = (
			( True,num or '')	if insertion == _D.ch.extend else
			(False,num or '')	if insertion == _D.ch.append else
			(False,n)  )
		n = (
			1					if num == '' else
			None				if num == _D.ch.inf else
			int(num)  )
	finite,n = isinstance(n,int) and (True,n) or (False,None)
	
	# generate
	if finite:
		if extend:
			for i in range(n):
				yield from val
		else:
			for i in range(n):
				yield val
	else:
		if extend:
			while True:
				yield from val
		else:
			while True:
				yield val
def gens(vals, ns:list|str=_D.ch.inf):
	''' create iterable from *vals* repeated *reps* times
		:param vals: item to repeat in iterable
		:param ns: string of pattern '([*+&]\d* )+' or int count times to duplicate part
		todo: support: '6' => append_by 6, ':6' => extend_by 6
	'''
	ns = isstr(ns, fab=strs)		# resolve inputs
	for val,n in zip(vals,ns):
		yield from gen(val,n)		# generate

def islice(seq, *slice_pa, start=None,stop=None,step=None,slice=...):
	'''
		:param ival:
		:param slice_pa:
		:param start:
		:param stop:
		:param step: todo
		:param slice:
	'''
	n_missing = (not slice_pa) + (... is slice)
	assert n_missing >= 1, locals()
	if slice_pa or slice is not ...:
		start,stop,step,*_ = (
			(None,slice_pa[0],None)					if len(slice_pa) == 1 else	# handle islice([],10)
			(*slice_pa,None)						if len(slice_pa) > 1 else	# handle islice([],0,10)
			(*slice,None)							if isiter(slice) else		# handle islice([],slice=(0,10))
			(slice.start,slice.stop,slice.step)									# handle islice([],slice=slice(0,10))
		)
	
	# iterate values
	idx,iseq = 0,iter(seq)
	for drop_val in start and iseq or ():
		idx += 1
		if idx >= start: break
	for val in iseq:
		idx += 1
		if idx >= stop: break
		yield val
def istop(stop,seq):	return islice(seq,stop=stop)

def isafe(ival, limit=_D.istop_default, stop_dbg=3, error=True):
	ival = iter(ival)
	try:
		for ind in range(int(limit/10)):
			yield next(ival)
			yield next(ival)
			yield next(ival)
			yield next(ival)
			yield next(ival)
			yield next(ival)
			yield next(ival)
			yield next(ival)
			yield next(ival)
			yield next(ival)
		for ind in range(int(stop_dbg)):
			val = next(ival)
			yield val
		if error:
			raise ValueError(f'exceeded {limit=} within {ival=!r}')
	except StopIteration:
		pass
def isafer(iterate,*pa_lim,**ka_lim):
	def call_post(*pa,**ka):
		ival = iterate(*pa,**ka)
		ival_safe = isafe(ival,*pa_lim,**ka_lim)
		return ival_safe
	ft.update_wrapper(call_post, iterate)
	return call_post

def iterany(obj)->iter:
	''' produce ->iter given any *obj* '''
	# todo: does this handle __next__?
	iobj = getattr(obj,'__iter__',None)
	if iobj:
		yield from iobj()
	else:
		yield obj
def itermany(*_objs,objs=())->iter:
	''' produce ->iter for each obj in *objs* '''
	return map(iterany,objs or _objs)
def iterfromany(obj,fab=None)->[iter,bool]:
	''' produce iterable & verdict ->(iter,from_iter) given any *obj* '''
	iobj_in = getattr(obj,'__iter__',None)
	iobj,from_iter = iobj_in and (iobj_in(),True) or (iterany(obj),False)  # limits some processing if obj was iter
	iobj = iobj if not fab else fab(iobj)
	return iobj,from_iter

def iterof(iter_in, prepgen=False, **ka):
	itr = (
		iter(iter_in)	if hasattr(iter_in,'__iter__') else
		iter_in			if hasattr(iter_in,'__next__') else   # fixme: this doesnt seem right
		iter_in(**ka)
	)
	prepgen and next(itr)  # discard first value of each if needed
	return itr
def itersof(iiter_in, prepgen=False, **ka):
	''' get iters for each in *iiter_in*
		:param iiter_in: sequence of iter objs to be converted
		:param prepgen: when True discard first value in all iters; generators in python must be pumped once before use
	'''
	iters = tuple(map(iterof,iiter_in))
	prepgen and nexts(iters)  # discard first value of each if needed
	return iters
def iterofgen(*pa,**ka):		return iterof(*pa,prepgen=True,**ka)
def itersofgens(*pa,**ka):		return itersof(*pa,prepgen=True,**ka)

def nexts(iters,fab=tuple):		return fab(map(next,iters))

def iaffix(lpart, mpart=(), *rparts, rep=1, mrep=_D.ch.extend):
	''' create iterable from *mpart* with prefix *lpart* and optional suffix(es) *rpart(s)*
		:param lpart: item to prefix to iterable
		:param mpart: iterable body to be affixed
		:param rparts: items suffixed to iterable
		:param rep: number of times to repeat each affixed arg in *lpart* and *rparts*
		:param mrep: number of times to repeat the central *mpart* iterable
	'''
	if rep == 1 and mrep == '*':	# simple flat iterable
		yield lpart
		yield from mpart
		yield from rparts
	else:							# dynamic iterable with variable number of reps
		yield from gen(lpart,  rep)
		yield from gen(mpart,  mrep)
		yield from gen(rparts, rep)
def isuffix(mpart, *rparts, rep=1, mrep='*'):
	''' create iterable like mpart with suffix(es) rpart(s)
		:param mpart: iterable body to be affixed
		:param rparts: items suffixed to iterable
		:param rep: number of times to repeat each suffixed arg in *rparts*
		:param mrep: number of times to repeat the central *mpart* iterable
	'''
	if rep == 1 and mrep == '*':	# simple flat iterable
		yield from mpart
		yield from rparts
	else:							# dynamic iterable with variable number of reps
		yield from gen(mpart, mrep)
		yield from gen(rparts, rep)

def ijoin(seq, *seqs, seps=(), sep=nothing, depth=1):
	''' default behavior is same as itertools.chain
		:param seq:
		:param seps:
		:param sep: default
		:param depth: number of times to join on sub-sequences
		
		Usage:
		' '.join(ijoin(['abc'], sep=','))					== 'a b c'					# top seqs len == 1
		' '.join(ijoin('abc', sep=','))						== 'a , b , c'				# top seqs len == 3
		' '.join(ijoin('abc', sep=',', depth=0))			== 'a b c'					# top seqs len == 1
		' '.join(ijoin('abc', sep=',', depth=1))			== 'a , b , c'				# top seqs len == 3
		' '.join(ijoin(['abc','def'], sep=','))				== 'a b c , d e f'			# top seqs len == 2
		' '.join(ijoin('abc', 'def', sep=','))				== 'a b c , d e f'			# top seqs len == 2
		' '.join(ijoin('abc', 'def', sep=',', depth=0))		== 'abc def'				# top seqs len == 2
		' '.join(ijoin('abc', 'def', sep=',', depth=1))		== 'a b c , d e f'			# top seqs len == 2
		' '.join(ijoin('abc', 'def', sep=',', depth=2))		== 'a , b , c , d , e , f'	# top seqs len == 2
	'''
	seqs = (seq,*seqs) if seqs or depth is None else seq  # treat seq as
	depth = depth is None and 1 or depth
	if depth >= 1:
		sep_depth, *seps = *seps, sep
		iseq = iter(seqs)
		seq = next(iseq, nothing)
		if depth == 1:
			if seq is not nothing:
				yield from seq
			for seq in iseq:
				if sep_depth is not nothing:
					yield sep_depth
				yield from seq
		
		else:
			if seq is not nothing:
				yield from ijoin(seq, seps=seps, sep=sep, depth=depth-1)
			for seq in iseq:
				if sep_depth is not nothing:
					yield sep_depth
				yield from ijoin(seq, seps=seps, sep=sep, depth=depth-1)
	elif depth == 0:
		yield from seqs
	else:
		raise ValueError(f'Given depth of {str(depth)}')

def duplicates(vals,n):
	return n==1 and zip(vals) or (val*n for val in zip(vals))

infof											= genof
igetidx											= islice
iany,imany,ifromany								= iterany,itermany,iterfromany
iof,iofgen,iiof,iiofgens						= iterof,iterofgen,itersof,itersofgens
dgen,hgen,lgen,tgen								= redef_per_coll(gen)
dgens,hgens,lgens,tgens							= redef_per_coll(gens)
rep,drep,hrep,lrep,trep							= gen,dgen,hgen,lgen,tgen
reps,dreps,hreps,lreps,treps					= gens,dgens,hgens,lgens,tgens
repeat,drepeat,hrepeat,lrepeat,trepeat			= gen,dgen,hgen,lgen,tgen		# draft: may rename repeat to dupl
repeats,drepeats,hrepeats,lrepeats,trepeats		= gens,dgens,hgens,lgens,tgens	# draft: may rename repeat to dupl
affix,daffix,haffix,laffix,taffix				= iaffix,*redef_per_coll(iaffix)
suffix,dsuffix,hsuffix,lsuffix,tsuffix			= isuffix,*redef_per_coll(isuffix)
afx,dafx,hafx,lafx,tafx							= affix,daffix,haffix,laffix,taffix
sfx,dsfx,hsfx,lsfx,tsfx							= suffix,dsuffix,hsuffix,lsuffix,tsuffix
djoin,hjoin,ljoin,tjoin							= redef_per_coll(ijoin)
ij,dj,hj,lj,tj									= ijoin,djoin,hjoin,ljoin,tjoin
dupls											= duplicates
#endregion

#region mod call basics
a2 = A2										= xaptuple

def a2_of(pa:list=(), ka:dict=map0, fab=None) -> a2:
	'''	given *pa* & *ka* create ->a2 (or pa,ka typed pair) '''
	# todo: need conversion between pa & ka
	a2_out = (pa,ka)
	a2_out = a2_out if not fab else fab(a2_out)
	return a2_out
def a2_of_aa(*_pa, pa:iter=(), ka:dict=map0, fab=None, **_ka) -> a2:
	'''	pack args given as *pa,**ka (or all args or aa) into ->a2 tuple (or pa,ka typed pair) '''
	# todo: need conversion between pa & ka
	a2_out = (_pa or pa,_ka or ka)
	a2_out = a2_out if not fab else fab(a2_out)
	return a2_out

def passap(call):
	''' pass unpacked pos args in *pa* to staged *call* '''
	def call_post(pa, ka=None,**_kargs):
		return call(*pa,**ka or map0,**_kargs)
	return call_post
def passak(call):
	''' pass unpacked key args in *ka* to staged *call* '''
	def call_post(ka,pa=()):
		return call(*pa,**ka)
	return call_post
def passaa(call):
	''' pass unpacked pos & key args in *a2* to staged *call* '''
	def call_post(a2):
		return call(*a2[0],**a2[1])
	return call_post

def ipassap(call,pas=None,ka=map0):
	''' produce iterable of each unpacked pos args in *pas* passed to staged *call*.
		Note: Functionally the same as itertools.starmap but takes kwargs *ka*
	'''
	def call_post(pas,ka=map0):
		for pa in pas:
			yield call(*pa,**ka)
	call_out = pas is None and call_post or call_post(pas,ka)
	return call_out
def ipassak(call,kas=None,pa=()):
	''' produce iterable of each unpacked kos args in *kas* passed to staged *call* '''
	def call_post(kas,pa=()):
		for ka in kas:
			yield call(*pa,**ka)
	call_out = kas is None and call_post or call_post(kas,pa)
	return call_out
def ipassaa(call,a2s=None):
	''' produce iterable of each unpacked pos & key args in *a2s* passed to staged *call* '''
	def call_post(a2s):
		for a2 in a2s:
			yield call(*a2[0],**a2[1])
	call_out = a2s is None and call_post or call_post(a2s)
	return call_out

def passpa(call):
	''' pack and pass pos args in **pa* to staged *call* '''
	def call_post(*pa,**ka):
		return call(pa,**ka)
	return call_post
def passka(call):
	''' pack and pass key args in ***ka* to staged *call* '''
	def call_post(*pa,**ka):
		return call(ka,*pa)
	return call_post
def passa2(call):
	''' pack and pass pos & key args in **pa*,***ka* as a2 arg to staged *call* '''
	def call_post(*pa,**ka):
		return call((pa,ka))  # convert to a2 arg form
	return call_post

def rename_call(call,name=None,if_lambda=False,namespace=None):
	# resolve inputs
	name_out = name or (isntstr(call) and throw(f'call={call!r} or name={name!r} must be a str.')) or call
	namespace = namespace if namespace is None else globals()
	call_out = iscall(call) and call or namespace[call]
	
	# change name
	if not if_lambda or call_out.__qualname__ == '<lambda>':
		call_out.__qualname__ = name_out
	return call_out

setap,listap,tupleap						= passap(set), passap(list), passap(tuple)
mapap,mapak,mapaa							= ipassap,ipassak,ipassaa
#endregion

#region call basics
def apply(obj, ev=None, evs=None, evvs=None, fabs=None, fab=None, **_):
	''' apply all converters to given values *vv*
		:param obj:  input values
		:param evvs: converters for each sub-value; applied first as the deepest 
		:param evs:  converter for each value; applied second as it is second deepest 
		:param ev:   converter for the values iterator to output; applied last as it is the least deep
	'''
	evs,ev = evs or fabs, ev or fab
	out = obj if not evvs else ((e(v) if e else v for e,v in zip(evvs,vv)) for vv in obj)
	out = out if not evs  else map(evs,out)
	out = out if not ev   else ev(out)
	return out

def call_of_prio(prio, obj):
	''' get first available call given an *obj* list of potential *prio*rity verbs
		:param prio: a list of potential verbs (function names) on obj
		:param obj: obj on which to check for *prio*rity verbs
	'''
	call = None
	for verb in prio:
		call = getattr(obj,verb,None)
		if call: break
	assert call
	return call

def copy_func(func, default_func=None, default_pa=None, default_ka=None, **ka):
	''' copy a function and apply custom defaults args and attributes
		:param func: function from which to clone the code and information
		:param default_func: function signature with new default pos. and key args
		:param default_pa: new default pos. args
		:param default_ka: new default key args
		:param ka: new default key args;  same as default_ka but with higher priority
	'''
	# todo: support custom docs
	# resolve inputs
	default_func = default_func or func
	default_pa = default_func.__defaults__ if default_pa is None else default_pa
	default_ka = default_func.__kwdefaults__ if default_ka is None and not ka else dict(default_ka or {}, **ka)
	default_ka = dict(func.__kwdefaults__, **default_ka)  # all keys are required
	
	# resolve output
	func_out = FunctionType(
		func.__code__,
		func.__globals__,
		name=func.__name__,
		closure=func.__closure__,
		argdefs=tuple(default_pa), )
	func_out.__kwdefaults__ = default_ka
	func_out = ft.update_wrapper(func_out, func)
	return func_out

def wrap_per_coll(func, colls=DDColl.incl, arg=None, **kdef):
	redefs = []
	for coll in colls:
		redef = lambda *pa, _func=func, _coll=coll, _kd=kdef, **ka: _coll(_func(*pa, **_kd, **ka))
		redef = ft.update_wrapper(redef,func)
		redefs.append(redef)
	return redefs

def redef_per_coll(func, colls=DDColl.incl, arg=None, **kdef):
	''' clone func modified with each in *colls* '''
	# fixme: this is a call wrapper or stagger not a redef; redef implies copy_func
	redefs = []
	for coll in colls:
		if arg is None:
			# fixme: wrap is not allowed in redef
			redef = lambda *pa, _func=func, _coll=coll, _kd=kdef, **ka: _coll(_func(*pa, **_kd, **ka))
		elif isinstance(arg, str):
			# fixme: replace with copy_func
			redef = lambda *pa, _func=func, _coll=coll, _key=arg, _kd=kdef, **ka: _func(*pa, **{arg:_coll}, **_kd, **ka)
		elif isinstance(arg, int):
			# fixme: replace with copy_func
			redef = lambda *pa, _func=func, _coll=coll, _pos=arg, _kd=kdef, **ka: _func(*pa[:_pos], _coll, *pa[_pos:], **_kd, **ka)
		else:
			raise ValueError()
		redef = ft.update_wrapper(redef,func)
		redefs.append(redef)
	return redefs
def def_coll_wraps(func, fmt:str=None, colls=DDColl.incl, arg=None, labels=DDColl.ch_incl, safe=True, **kdef):
	names = list(map((fmt or func.__name__).format, labels))
	assert safe and set(names).isdisjoint(globals()), f'formatted name in {names} collides with namespace'
	wraps = redef_per_coll(func, colls, arg, **kdef)
	named_wraps = tuple(zip(names, wraps))
	
	globals().update(dict(named_wraps))
	return named_wraps

def redef_call(call, *_pal,pal=None,par=(), fm=32, ka=None,**_ka):
	''' fixme: clone function and modify pos and key arg defaults '''
	_pal,_par,_ka = pal or _pal, par, ka or _ka
	@ft.wraps(call)
	def call_of(*pa, **ka):
		return call(*_pal, *pa, *_par, **dict(_ka, **ka))
	call_of.__qualname__ += '(%s)'%','.join(ss(_pal+('..',)+_par+tuple(f'{k}={v}' for k,v in _ka.items()),'{!s:.%ss}'%fm))
	return call_of

def stage_call(call, *_pal, staged_if=HasArg.stages_1,_stages=1, pal=(),par=(),ka=None,**_ka):
	''' produce final ->call satisfying *staged_if*(*,**) over series of preparation calls '''
	_pal,_par = par is True and [(),pal or _pal] or [pal or _pal, par]
	_ka = ka or _ka
	
	@ft.wraps(call)
	def call_of(*pa, is_staged=None, **ka):
		pa_all,ka_all = _pal+pa+_par, dict(_ka, **ka)
		is_staged = is_staged if is_staged is not None else staged_if(*pa_all,**ka_all, _stages=_stages)
		
		if is_staged:
			ka_into_args = {k:ka_all.pop(k) for k in [*ka_all] if k in _D.mod_set}
			if ka_into_args:
				pa_all,ka_all = into_args(**ka_into_args, pa=pa_all,ka=ka_all)
			result = call(*pa_all,**ka_all)
		else:
			result = stage_call(call,*pa_all,**ka_all, staged_if=staged_if,_stages=_stages+1)
		return result
	return call_of

stage = staging						= stage_call
redef = pass_call					= redef_call		# fixme: temp naming; implement redef_call; with copy func
redef_per_fab						= redef_per_coll	# todo: create generic redef_per_fab
#endregion

#region char basics
_nch_lo_a,_nch_up_a				= ord('a'), ord('A')
chlower_of_n,chupper_of_n		= s_lower.__getitem__, s_upper.__getitem__

def b_of_a(ch,delta=1,alpha=yes):
	''' get new ->char as *ch* plus *delta*. enable *alpha* to wrap around between a-z or A-Z '''
	nch_b = ord(ch) + delta
	if alpha:
		nch_0 = ('a'<=ch<='z' and _nch_lo_a) or ('A'<=ch<='Z' and _nch_up_a) or throw(f'given ch={ch!r} is not in alphabet')
		nch_b = ((nch_b - nch_0) % 26) + nch_0
	ch_b = chr(nch_b)
	return ch_b

def abc_of_ac(a,c='',chs=s_lower):
	return ''.join(map(chs.__getitem__,range(chs.index(a[0]),chs.index(c or a[1])+1)))

ch_of_n							= chlower_of_n
chs_of_chch	= ch_range			= abc_of_ac
ch_of_chd						= b_of_a
#endregion

#region seq basics
def types(seq,fab=None):
	''' produce sequence of types from *seq* with arbitrary items as iter or *fab* '''
	out = map(type,seq)
	out = out if not fab else fab(out)
	return out
def strs(seq:iter, to_val=str, sep:str=' ', keep=True, subs=no, fab=None, **ka)->[str]:
	''' produce sequence of strs from *seq* with arbitrary items as iter or *fab*. when *seq* is str split by *sep* '''
	assert 'to_str' not in ka, 'change strs(to_str) -> strs(to_val)'
	subs = subs if subs is not True else dict.fromkeys(_D.kk_sub,sep)  # when val==sep use a kk_sub to convert to sep 
	
	vv_out = seq if isntstr(seq) and isiter(seq) else str(seq).split(sep)
	vv_out = vv_out if not keep else filter(callable(keep) and keep or ''.__ne__, vv_out)
	vv_out = vv_out if not to_val else map(isstr(to_val,var='format'), vv_out)
	vv_out = vv_out if not subs else (subs.get(s,s) for s in vv_out)
	vv_out = vv_out if not fab else fab(vv_out)
	return vv_out
def ints(seq:iter, to_val=int, sep:str=' ', keep=''.__ne__, ofs=0., fab=None)->[int]:
	''' produce sequence of ints from *seq* with arbitrary items as iter or *fab*. when *seq* is str split by *sep* '''
	vv_out = seq if isntstr(seq) and isiter(seq) else str(seq).split(sep)
	vv_out = vv_out if not keep else filter(callable(keep) and keep or bool, vv_out)
	vv_out = vv_out if not ofs else map(ofs.__add__, vv_out)
	vv_out = vv_out if not to_val else map(to_val, vv_out)
	vv_out = vv_out if not fab else fab(vv_out)
	return vv_out
def flts(seq:iter, to_val=float, sep:str=' ', keep=''.__ne__, fab=None)->[float]:
	''' produce sequence of floats from *seq* with arbitrary items as iter or *fab*. when *seq* is str split by *sep* '''
	vv_out = seq if isntstr(seq) and isiter(seq) else str(seq).split(sep)
	vv_out = vv_out if not keep else filter(callable(keep) and keep or bool, vv_out)
	vv_out = vv_out if not to_val else map(to_val, vv_out)
	vv_out = vv_out if not fab else fab(vv_out)
	return vv_out
def cplxs(seq:iter, to_val=complex, sep:str=' ', keep=''.__ne__, fab=None)->[complex]:
	''' produce sequence of complexes from *seq* with arbitrary items as iter or *fab*. when *seq* is str split by *sep* '''
	vv_out = seq if isntstr(seq) and isiter(seq) else str(seq).split(sep)
	vv_out = vv_out if not keep else filter(callable(keep) and keep or bool, vv_out)
	vv_out = vv_out if not to_val else map(to_val, vv_out)
	vv_out = vv_out if not fab else fab(vv_out)
	return vv_out
def bools(seq:iter, to_val=bool, sep:str=' ', keep=''.__ne__, ofs=0., fab=None)->[bool]:
	''' produce sequence of bools from *seq* with arbitrary items as iter or *fab*. when *seq* is str split by *sep* '''
	vv_out = seq if isntstr(seq) and isiter(seq) else str(seq).split(sep)
	vv_out = vv_out if not keep else filter(callable(keep) and keep or bool, vv_out)
	vv_out = vv_out if not ofs else map(ofs.__add__, vv_out)
	vv_out = vv_out if not to_val else map(to_val, vv_out)
	vv_out = vv_out if not fab else fab(vv_out)
	return vv_out
def lens(seq, to_val=len, keep=None, fab=None) ->[int]:
	''' produce sequence of lengths from *seq* with arbitrary items as iter or *fab* '''
	vv_out = seq if keep is None else filter(callable(keep) and keep or bool, seq)
	vv_out = map(to_val, vv_out)
	vv_out = vv_out if not fab else fab(vv_out)
	return vv_out

def nn_of_s(s_nn,fab,*fabs_pa,sep_of=None,**ka):
	# todo: generalize better
	sep = sep_of(fab)
	vv_out = s_nn if not sep else list(filter(bool,s_nn.split(sep)))
	vv_out = vv_out if not fabs_pa else [nn_of_s(n,*fabs_pa,sep_of=sep_of,**ka) for n in vv_out]
	vv_out = fab(vv_out,**ka)
	return vv_out

def strsof(*pa,keep=None,**ka):
	''' get strs of seq without filtering '''
	return strs(*pa,**ka,keep=keep)

itypes,_,htypes,ltypes,ttypes		= types, *redef_per_fab(types, arg='fab')
istrs,_,hstrs,lstrs,tstrs			= strs, *redef_per_fab(strs, arg='fab')
iints,_,hints,lints,tints			= ints, *redef_per_fab(ints, arg='fab')
iflts,_,hflts,lflts,tflts			= flts, *redef_per_fab(flts, arg='fab')
icplxs,_,hcplxs,lcplxs,tcplxs		= cplxs, *redef_per_fab(cplxs, arg='fab')
ilens,_,hlens,llens,tlens			= lens, *redef_per_fab(lens, arg='fab')
iss,hss,lss,tss = ss,*_				= strs, hstrs, lstrs, tstrs
_,hii,lii,tii						= ints, hints, lints, tints  # draft: ii as ints or iter([iter()])
iff,hff,lff,tff = ff,*_				= flts, hflts, lflts, tflts
eqlens								= redef(lens, fab=lambda vv: len(set(vv)) <= 1)
sslens								= redef(strs,to_val=len)
lenspa								= passpa(lens)
ssof								= strsof
#endregion

#region str basics
fmt_of = fmtof = fmtr				= lambda fmt: isstr(fmt,var='format')  # todo: change __name__ to reflect str

def s_of(fmt, *pa, **ka):
	fmt = (type(fmt) is str and fmt.format or fmt)
	out = not pa and not ka and fmt or fmt(*pa,**ka)
	return out

def slog(*_pa, fmt='{}', logr=print, **_ka):
	fmt = type(fmt) is str and fmt.format or fmt
	def slog_of_args(*pa,**ka):
		s = fmt(*pa,**ka)
		logr(s)
		return s
	out,logr_now = (_pa or _ka) and (fmt(*_pa,**_ka),logr) or (slog_of_args,None)
	logr_now and logr_now(out)
	return out

def j_of_zs(zstr,from_left=False):
	''' identify sep|arator in the given zstr if present
		:param zstr:
		:param from_left: todo
	'''
	if not zstr: return None
	sep = zstr[-1]
	if zstr.index(sep) == len(zstr)-1 or sep.isalnum():
		sep = None
	return sep
def sss_of_zs(zstr:str,from_left=False,keep=None,depth_max=None,fabs=None,fab=None,**ka):
	''' Given *zstr* detect the custom delimiter by which to split it into Strs.
		The delim will be delim_strings[0] but only if it is found 2 or more times in *delim_string*
		When there is no qualifying delim (delim_strings[0] occurs only once) return Strs containing
		only 1 *delim_strings*. Emulates the unix sed application pattern replace syntax/behavior.
		
		Usage:
		sss_of_zs('abc;')				# => ('abc;;')
		sss_of_zs(';abc;')				# => ('abc')
		sss_of_zs('abc;def;')			# => ('abc', 'def')
		sss_of_zs(';abc;def;')			# => ('abc', 'def')
		sss_of_zs(';;abc;def;')			# => ('', 'abc', 'def')
		sss_of_zs('  abc def ')			# => ('', 'abc', 'def')
		sss_of_zs('a,b,c,;d,e,f,;')		# => ('a,b,c,', 'd,e,f,')
		sss_of_zs('a,b,c,,;d,e,f,,;;')	# => ('a,b,c,,', 'd,e,f,,', '')
	'''
	# resolve inputs:  => sep
	sep = j_of_zs(zstr,from_left=from_left)
	if sep is None or depth_max == 0: return zstr
	
	# resolve output:  => sss
	sss_out = ss_out = zstr.split(sep)
	ss_out = ss_out if keep is None else filter(callable(keep) and keep or ''.__ne__, ss_out)
	ss_out.pop(not from_left and -1 or 0)  # must omit empty element opposite *sep* at the end
	if depth_max != 0:
		ka = dict(fab=fabs or list,from_left=from_left,depth_max=depth_max if depth_max is None else depth_max-1)
		sss_out = (sss_of_zstr(s,**ka) for s in ss_out)
		sss_out = sss_out if not fab else fab(sss_out)
	return sss_out

def sdisjoin(s:str, *_jjx, jjx:str=None, to_jx=None, keep=None, fabs=None, **ka):
	''' disjoin|jx or split given *s* str into nested sub sequences with nest depth equal to total *jjx* disjoin chars
		:param s:     string to be disjoined|split
		:param jjx:   args controlling how *s* is disjoined at deeper levels
		:param to_jx: converter of each in *jjx* to disjoin char
		:param fabs:  converter of output seq
	'''
	if not (jjx:=jjx or _jjx):		return sss_of_zs(s, fabs=None, **ka)
	
	# resolve inputs:  => jx,fab,fabs
	jx,*jjx = jjx
	jx = jx if not to_jx else to_jx(jx)
	fab,fabs = (
		[fabs,fabs]				if istype(fabs) else
		[fabs[jx],fabs]			if iscall(getattr(fabs, 'values', None)) and not istype(fabs) else
		[fabs[0],fabs[1:]]		if isiter(fabs) and len(fabs) >= 2 else
		[fabs,tuple] )
	
	# resolve output:  => sss
	sss_out = s if isntstr(s) else s.split(jx)
	sss_out = sss_out if not keep else filter(''.__ne__ if keep is True else keep, sss_out)
	sss_out = sss_out if not jjx else (sss_of_jx(val,jjx=jjx,to_jx=to_jx,fabs=fabs,is_pa=0) for val in sss_out)
	sss_out = sss_out if not fab else fab(sss_out)
	return sss_out

def kkss_of(ss_in, *_seps, seps=',:',at=None,fab=None,**ka):
	''' distinguish key strs (or kk) from non-key strs (or ss) by prefix *sep_iskey* or *sep_ntkey* resp.
		
		usage:
		kkss('name email : age height',*',:')	=>  [['name','email'],['age','height']]  # define keys/strs via delim
		kkss(':age, name :height, email',*',:')	=>  [['name','email'],['age','height']]  # or define keys/strs via pfxs
	'''
	# resolve inputs
	sep_key,sep_val,*_ = [seps[0],*_seps,seps[1]][-3:]				# get 1st 2 priority: all _seps, seps[1], seps[0]
	
	# resolve key/val strs: assign each str to kk_out or ss_out depending on preceding sep
	if not isstr(ss_in):
		# *_,kk_out,ss_out = (),*ss_in
		kk_out,ss_out = isstr(ss_in[0]) and (ss_in,()) or ss_in
	else:
		kk_out,ss_out = [],[] 										# kk_out is default in absence of any sep
		for kk_ss_ith in strs(ss_in,sep=sep_key,to_val=None):
			seq_out = kk_out										# first strs in i_kk_ss_nth are key strs
			for ss_ith in strs(kk_ss_ith,sep=sep_val,keep=False):
				seq_out.extend(strs(ss_ith,**ka))
				seq_out = ss_out									# subsequent strs in i_kk_ss_nth are non-key strs
	
	# resolve output
	kkss_out = (kk_out,ss_out)
	kkss_out = kkss_out if at is None else at(kkss_out) if iscall(at) else kkss_out[at]
	kkss_out = kkss_out if not fab else fab(kkss_out)
	return kkss_out
def s_of_kkss(kkss, sep_val='-', sep_key='+', join=' ', **ka):
	kk,ss = kkss
	iss = ss(kk,f'{sep_key}{{}}'), ss(ss,f'{sep_val}{{}}')
	ss_out = (s for ss in iss for s in ss)  # join both str iters
	s_out = join.join(ss_out)
	return s_out
def ss_of_kkss(*pa,**ka):
	ss = list.__add__(*kkss_of(*pa,**ka))
	return ss
def kkss_kk_ss_of(*pa,**ka):
	kk_out,ss_out = kkss_of(*pa,**ka)
	return kk_out+ss_out,kk_out,ss_out

sof									= s_of
sss_of_zstr							= sss_of_zs
sss_of_jx = sjx						= sdisjoin
sss_of_zstr							= sss_of_zs
sep_of_zstr							= j_of_zs
keys_strs_of = kkss					= kkss_of
str_of_keys_strs					= s_of_kkss
kkss_kk_ss							= kkss_kk_ss_of
sss									= sdisjoin  # todo: obsolete; eliminate
#endregion

#region modify arg basics
def to_args(to_at,*_pa,pa=...,ka=...,**_ka):
	''' todo: change arg values.  placements remain unchanged. '''
	return pa,ka

def into_args(ltok=(),rtok=(),ktok=map0,ktol=(),ktor=(), mod_prio=_D.mod_prio,excl_k=_D.excl_k, pa=(),ka=map0):
	''' change arg placements.  values remain unchanged.
		:param ltok: add to ka with values taken from left of pa
		:param rtok: add to ka with values taken from right of pa
		:param ktok: add to ka with values taken from ka at key
		:param ktol: add to left of pa with values taken from ka at key
		:param ktor: add to right of pa with values taken from ka at key
		:param mod_prio: order in which mods are performed. the default:[ltok rtok ktok ktol ktor] is recommended
		:param excl_k: callable to check if dest key should be excluded rather than added
		:param pa: pos args to modify
		:param ka: key args to modify
	'''
	# exit case; nothing to do
	if not (pa and (ltok or rtok)) and not (ka and (ktok or ktol or ktor)):		return pa,ka
	
	# resolve inputs
	at_left,at_right,at_end = 0,-1,2<<32  # indices of first, last & end
	ltok,rtok,ktol,ktor = tss(ltok),tss(rtok),tss(ktol),tss(ktor)
	ktok = isdict(map0,orfab=dict)
	
	# resolve output
	pa_out,ka_out = list(pa),dict(ka)
	for mod_arg in mod_prio:
		if pa_out and mod_arg == 'ltok':
			for k_to in ltok:
				val = pa_out.pop(at_left)
				not excl_k(k_to) and ka_out.__setitem__(k_to,val)
		elif pa_out and mod_arg == 'rtok':
			for k_to in rtok:
				val = pa_out.pop(at_right)
				not excl_k(k_to) and ka_out.__setitem__(k_to,val)
		elif ka_out and mod_arg == 'ktok':
			for k_in,k_to in ktok.items():
				val = ka_out.pop(k_in)
				not excl_k(k_to) and ka_out.__setitem__(k_to,val)
		elif ka_out and mod_arg == 'ktor':
			for k_in in ktor:
				pa_out.insert(at_end,ka_out.pop(k_in))
		elif ka_out and mod_arg == 'ktol':
			for k_in in ktol:
				pa_out.insert(at_left,ka_out.pop(k_in))
	return pa_out,ka_out
#endregion

#region scratch area
''' Name Conventions: Call
in python there are primarily 2 ways to modify or adapt a call (or function)
1. call wrap: call given to wrapper function which makes modifications before/after call invocation
  - requires 1 or more closures or additional frames
  - call staging is considered a kind of call wrap but implies preparing a wrapper depending on given args
2. call redef-ine: will copy the code object and overwrite the parameter defines.
  - more efficient in that it does not grow the stack when invoked
  - less common and maybe less intuitive
'''
# fixme: naming and implementation here needs to be simplified and made consistent; wrap vs redef vs stage
#   low priority since redef, stage and wrap all achieve the same effect with only difference being under the hood
#endregion