import sys
import time
import select
from pyutils.defs.funcdefs import throw


class TimeoutExpired(Exception): pass

def input_timed_unix(prompt, timeout, fill=None):
	sys.stdout.write(prompt)
	sys.stdout.flush()
	ready, _, _ = select.select([sys.stdin], [],[], timeout)
	result = (
		sys.stdin.readline().rstrip('\n')	if ready else
		fill								if fill is not None else
		
		throw(TimeoutExpired)  )
	print(result)
	return result
	
def input_timed_win(prompt, timeout, timer=time.monotonic):
	sys.stdout.write(prompt)
	sys.stdout.flush()
	endtime = timer() + timeout
	result = []
	while timer() < endtime:
		if msvcrt.kbhit():
			result.append(msvcrt.getwche()) #XXX can it block on multibyte characters?
			if result[-1] == '\n':   #XXX check what Windows returns here
				return ''.join(result[:-1])
		time.sleep(0.04) # just to yield to other processes/threads
	raise TimeoutExpired()

try:
	import msvcrt
	input_timed = input_timed_win
except:
	input_timed = input_timed_unix


def is_jupyter_runtime():
	import os
	from pyutils.extend.datastructures import XDict
	
	result = XDict.like(os.environ, 'JUPYTER_PATH|JPY_PARENT_PID')  # check for 2 known jupyter env vars
	return bool(result)


if __name__=='__main__':
	result = is_jupyter_runtime()
	print(f'is_jupyter_runtime() == {is_jupyter_runtime()}')
	assert(result)