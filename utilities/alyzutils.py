import sys
import os
import inspect
import gc
from io import StringIO

from pyutils.defs.typedefs import *
from pyutils.defs.colldefs import collany
from pyutils.utilities.callutils import ss, tss, tj, fmtr, redef
from pyutils.utilities.strutils import refmt
from pyutils.utilities.pathutils import *

#region analyze defs
class DefsAnalyze:
	#region defs
	log_sz				= 48
	fmt_log				= '{{!s:{fm}.{fm}}}'.format(fm=log_sz)
	fmt_msg0			= '{}'
	fmt_printinfo		= '{}  {}'
	#endregion
	#region scopes
	class Fmt:
		fm_n_lt			= 28
		fm_n			= 48
		
		kfuncln			= '{func}:{ln}'.format
		kfuncpln		= refmt('{func}','{_:{fm1}s}|{pln}',fm1=fm_n_lt)
		
		fmt_msg=msg		= '{{msg!s:{n}.{n}}} | '.format(n=fm_n)
		fmt_finfo=finfo	= '{msg}{finfo}'
		
		fm_msg_ge		= '{0}'.format(fm_n)
		fm_msg_le		= '.{0}'.format(fm_n)
		fm_msg_eq		= '{0}.{0}'.format(fm_n)
		fm_msg			= fm_msg_eq
		fmt_finfo=finfo	= '{msg!s:{fm}} | {finfo}'
		msg				= refmt('{msg!s:{fm}} | {fq}',fm=fm_n)
		
		status_colrs	= (None, )
		status_keys		= tss('plain notice passed warn error fatal')
		lvls			= tss('note  warn  error')
		to_lvlfmts_fill	= tuple(f'{lvl.upper()+":":6} {{fmt_finfo}}'.format for lvl in lvls)
		
		s_fileln_std	= 'File "{}", line {} '
		s_fileln_std_ka	= s_fileln_std.format('{filename}', '{line}')
		fileln_std		= fmtr('File "{}", line {} ')
		fileln_std_ka	= fmtr(fileln_std('{filename}', '{line}'))
		
		@classmethod
		def set_lvls(cls,to_lvlfmts=None,fmt_finfo=fmt_finfo,ns_vars=None):
			kk_fq = tss(
				'fq_pln_note       fq_pln_warn       fq_pln_error      '
				'fp_pln_note       fp_pln_warn       fp_pln_error      '
				'fq_kfuncpln_note  fq_kfuncpln_warn  fq_kfuncpln_error '
				'fp_kfuncpln_note  fp_kfuncpln_warn  fp_kfuncpln_error ' )
			
			fmt_lvls = tuple(lvlfmt_of(fmt_finfo,fmt_finfo=fmt_finfo) for lvlfmt_of in to_lvlfmts or cls.to_lvlfmts_fill)
			fq_lvls = tj(
				(redef(fquery_of,fq_pln,      fab=fmt,frame_ofs=2            ) for fmt in fmt_lvls),
				(redef(fquery_of,fq_pln,      fab=fmt,frame_ofs=2,logr=print ) for fmt in fmt_lvls),
				(redef(fquery_of,fq_kfuncpln, fab=fmt,frame_ofs=2            ) for fmt in fmt_lvls),
				(redef(fquery_of,fq_kfuncpln, fab=fmt,frame_ofs=2,logr=print ) for fmt in fmt_lvls),
			)
			
			if ns_vars is not None:
				for k_fq,fq_lvl in zip(kk_fq,fq_lvls):
					ns_vars[k_fq] = fq_lvl
			return fq_lvls
	#endregion
	...

DDAlyz = _D				= DefsAnalyze
FAlyz = _F				= DDAlyz.Fmt
_i						= iter
#endregion

#region analyze forms
is_main_frame			= lambda frame=None,finfo=None: (frame or finfo.frame).f_globals['__name__'] == '__main__'
_atall,_atrev			= slice(None), slice(None,None,-1)
_ndx_fqframe_at			= dict(other=0, frame=1, all=_atall, rev=_atrev)

def bump_fquery_ka(ka=None,bump=1,*,fquery=None,frame=True,other=False,**_ka):
	''' modify fq_frame kargs to ignore the current frame and raise the target frame to be fetched in order '''
	# resolve inputs
	ka_other = ka or _ka
	ka_frame = 'frame' in ka_other and dict(frame=ka_other.pop('frame')) or dict()
	ka_frame.update(frame_ofs = ka_other.pop('frame_ofs',1) + bump + bool(fquery))
	
	# resolve output
	out = (
		fquery(**ka_frame)		if fquery else
		ka_frame				if not other else
		ka_other				if not frame else
		dict(ka_other,**ka_frame) )
	return out
def bump_fquery_ka2(ka=None,bump=1,*,fquery=None,at=_atrev,**_ka):
	''' modify fq_frame kargs to ignore the current frame and raise the target frame to be fetched in order '''
	# resolve inputs
	at = at if istype(at,slice) else _ndx_fqframe_at.get(at,at)  # todo: if fquery then default at should be other
	ka_other = ka or _ka
	ka_frame = 'frame' in ka_other and dict(frame=ka_other.pop('frame')) or dict()
	ka_frame.update(frame_ofs = ka_other.pop('frame_ofs',1) + bump + bool(fquery))

	# resolve output
	out = (
		fquery(**ka_frame)			if fquery else
		(ka_other,ka_frame)[at] )
	return out

def fileln_of(fileln=None,file=None,ln=None,code=None,to_fileln=_F.fileln_std):
	'''
		:param fileln: object containing file & ln
		:param file:
		:param ln:
		:param code: a python code object
		:param to_fileln: convert output then return. opting for 'to_fileln' to preserve 'fab' for derived functions'
		get function str in standard format. (some programs recognize and hyperlink to target function)
	'''
	# resolve inputs
	code = code or getattr(fileln,'__code__',None)
	file,ln = (
		(file,ln)									if fileln is code is None else
		(code.co_filename,code.co_firstlineno)		if code else
		fileln )
	
	# resolve output
	fileln = fileln if not to_fileln else to_fileln(file,ln)
	return fileln

def pln_of_rel(p,ln,p_dir=True,ln_ofs=0):
	p_dir = p_dir == True and os.path.abspath('.') or p_dir
	p_rel = (p_dir and p.startswith(p_dir)) and p.replace(p_dir, './') or p
	ln_rel = ln + ln_ofs
	return p_rel,ln_rel

def keys_of_func(func,ndxs=(0,None),ofss=(0,1),**ka):
	''' get keys of given *func*. accepts *func* as function or frame from function or code of function '''
	# resolve inputs
	fxcode = getattr(func,'f_code',None) or getattr(func,'__code__',None) or func
	start,stop = isiter(ndxs) and ndxs or (0,ndxs)
	start = ofss[0] + isstr(start, fab=fxcode.co_varnames.index, orput=start)
	stop  = ofss[1] + isstr(stop,  fab=fxcode.co_varnames.index, orput=isnone(stop,put=fxcode.co_argcount,orput=stop))
	
	# resolve output
	keys = fxcode.co_varnames[start:stop]
	return keys

bump_fqka,bump_fqka2	= bump_fquery_ka,bump_fquery_ka2
#endregion

#region analyze utils
# todo: trasition to single unified function with flags selecting for output
def fq_frame(frame=0, frame_ofs=1,**ka):
	return isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame

def fq_file(frame=0, frame_ofs=1):
	frame = isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	out = frame.f_code.co_filename
	out = out[-4:] in ('.pyc', '.pyo') and out[:-1] or out  # change .pyc & .pyo extensions to .py
	return out

def fq_line(frame=0, frame_ofs=1):
	frame = isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	return frame.f_lineno

def fq_fileln(frame=0, frame_ofs=1, fab=_F.fileln_std, rel_to=None):
	# resolve inputs
	frame = isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	filename = frame.f_code.co_filename
	if rel_to and filename.startswith(rel_to):
		filename = filename.replace(rel_to, './')
	filename = filename[-4:] in ('.pyc', '.pyo') and filename[:-1] or filename  # change .pyc & .pyo extensions to .py
	line = frame.f_lineno
	
	# resolve output
	out = filename, line
	fab_file_ln = type(fab) is str and fab.format or fab
	out = out if not fab_file_ln else fab_file_ln(*out)
	return out

def fq_file_ln(*pa, frame_ofs=1, fab=None, **ka):
	return fq_pln(*pa, **ka, frame_ofs=frame_ofs+1,fab=fab)

def fq_modulename(frame=0, frame_ofs=1,name_idx=None):
	# resolve inputs
	frame = isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	# resolve output
	out = frame.f_globals.get('__name__')  # tbd: is there a way to enforce __class__ presence?
	if name_idx is not None:
		out = isstr(out.split('.')[name_idx], orfab='.'.join)
	return out
def fq_packagename(frame=0, frame_ofs=1):
	# resolve inputs
	frame = isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	# resolve output
	out = fq_modulename(frame,name_idx=0)
	return out

def _fq_cls(frame=0, frame_ofs=1):
	# resolve inputs
	frame = isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	# resolve output
	out = frame.f_locals.get('self')  # tbd: is there a way to enforce __class__ presence?
	out = frame.f_locals.get('cls') if out is None else out.__class__
	return out
def fq_cls(frame=0, frame_ofs=1,fill=None):
	# resolve inputs
	frame = isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	arg0 = (args:=frame.f_code.co_varnames) and args[0] or None
	# resolve output
	out = (
		frame.f_locals.get('cls')				if arg0 == 'cls' else
		frame.f_locals.get('self').__class__	if arg0 == 'self' else
		fill )
	return out

def fq_clsname(frame=0, frame_ofs=1):
	# resolve inputs
	frame = isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	cls = fq_cls(frame)
	# resolve output
	out = getattr(cls, '__name__', '')
	return out

def fq_clsqual(frame=0, frame_ofs=1):
	# resolve inputs
	frame = isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	cls = fq_cls(frame)
	# resolve output
	out = getattr(cls, '__qualname__', '')
	return out

def fq_func(frame=0, frame_ofs=1):
	# Note: potentially intensive call to gc.get_referrers
	# resolve inputs
	frame = isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	code  = frame.f_code
	gvars = frame.f_globals
	type_fx = type(fq_func)
	
	# resolve output
	func = None
	for func in gc.get_referrers(code):
		if type_fx is type(func) and code is getattr(func,'__code__',None):# and gvars is getattr(func,'__globals__',None):
			break
	return func

def fq_funcname(frame=0, frame_ofs=1):
	frame = isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	return frame.f_code.co_name

def fq_funcqual(frame=0, frame_ofs=1, join='.'.join, **ka):
	# resolve inputs
	frame		= isinstance(frame, int) and sys._getframe(frame+frame_ofs) or frame
	scls		= ka['scls']  if 'scls'  in ka else fq_clsname(frame)
	sfunc		= ka['sfunc'] if 'sfunc' in ka else fq_funcname(frame)
	# resolve output
	out			= isstr(join,var='join')((scls and [scls] or [])+[sfunc])
	return out

def fq_funcqualln(frame=0, frame_ofs=1, fmt=_F.kfuncln, join='.'.join, **ka):
	# resolve inputs
	frame		= isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	scls		= ka['scls']  if 'scls'  in ka else fq_clsname(frame)
	sfunc		= ka['sfunc'] if 'sfunc' in ka else fq_funcname(frame)
	ln			= ka['ln']    if 'ln'    in ka else fq_line(frame)
	# resolve output
	k_func		= isstr(join,var='join')((scls and [scls] or [])+[sfunc])
	out			= fmt(func=k_func,ln=ln)
	return out

def fq_funcqualfileln(frame=0, frame_ofs=1, fmt=_F.kfuncpln, join='.'.join, **ka):
	# resolve inputs
	frame		= isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	scls		= ka['scls']  if 'scls'  in ka else fq_clsname(frame)
	sfunc		= ka['sfunc'] if 'sfunc' in ka else fq_funcname(frame)
	pln			= ka['pln']   if 'pln'   in ka else fq_fileln(frame)
	# resolve output
	k_func		= isstr(join,var='join')((scls and [scls] or [])+[sfunc])
	out			= fmt(func=k_func,pln=pln)
	return out

def fq_funckeys(frame=0, frame_ofs=1,**ka):
	frame = isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	kk = keys_of_func(frame,**ka)
	return kk

def fq_globals(frame=0, frame_ofs=1):
	frame = isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	return frame.f_globals

def fq_locals(frame=0, frame_ofs=1):
	frame = isinstance(frame,int) and sys._getframe(frame+frame_ofs) or frame
	return frame.f_locals

def find_frame(frame=0,keep=None,skip=None,first=True,i_keep_ifis=(),fab=None,**ka):
	# resolve input
	f,ff = f_in,_ = fq_frame(frame,**ka), None
	i_keep_ifis = (
		iscall(first,put=((first,1),),orput=()) +
		(keep and ((keep,1),) or ()) +
		(skip and ((skip,0),) or ()) +
		i_keep_ifis )
	assert i_keep_ifis, 'no keep conditions were given'
	
	# resolve output
	while f:
		is_keep = _i(keep(f) == if_is for keep,if_is in i_keep_ifis)
		is_keep = all(is_keep)
		if is_keep:
			if first: break
			ff = ff or []
			ff.append(f)
		f = f.f_back
	f_out = f if not fab else fab(f)
	return f_out

fq_smodl				= fq_modulename
fq_kpkg					= fq_packagename
frame_of				= fq_frame
fq_funckk				= fq_funckeys
find_main_frame			= redef(find_frame, keep=is_main_frame)
_fquerys				= (	fq_p,   fq_ln,  fq_pln,   fq_scls,   fq_kcls,   fq_sfunc,   fq_kfunc,   fq_kfuncln,   fq_kfuncpln,       ) = (
							fq_file,fq_line,fq_fileln,fq_clsname,fq_clsqual,fq_funcname,fq_funcqual,fq_funcqualln,fq_funcqualfileln, )

# todo: eliminate following aliases
fframe					= fq_frame
ffile_ln				= fq_file_ln
ffuncname				= fq_funcname
ffuncqual				= fq_funcqual
ffuncqualln				= fq_funcqualln
#endregion

#region frame logrs
def fquery_of(fquery, *msgs, frame=0, frame_ofs=1, ka_fq=None, fab=_F.finfo, fm=_F.fm_msg, logr=None,**ka_log):
	''' print *fquery*() output with optional *msgs* args
		:param fquery: frame utility function above
		:param msgs: optional msg args to pass into *fab*
		:param fab: convert combines finfo & msgs
		:param logr: optional logger function.
		:param ka_log: logger kwargs user input
	'''
	# resolve inputs
	msg,fm = msgs and ', '.join(ss(msgs)) or '', fm or ''
	fab = isstr(fab,var='format')
	
	# resolve output
	finfo = fquery(frame, frame_ofs=frame_ofs+1, **(ka_fq or {}))
	finfo_out = fab(msg, finfo, msg=msg, finfo=finfo, fm=fm)
	logr and logr(finfo_out, **ka_log)
	return finfo_out

_ka						= dict(frame_ofs=2, logr=print)
_fprints				= (	fp_p,   fp_ln,  fp_pln,   fp_scls,   fp_kcls,   fp_sfunc,   fp_kfunc,   fp_kfuncln,   fp_kfuncpln,       ) = (
							fp_file,fp_line,fp_fileln,fp_clsname,fp_clsqual,fp_funcname,fp_funcqual,fp_funcqualln,fp_funcqualfileln, ) = (
							[redef(fquery_of,fq,**_ka) for fq in _fquerys] )
_fprintlvls				= (	fq_pln_note,     fq_pln_warn,     fq_pln_error,
							fp_pln_note,     fp_pln_warn,     fp_pln_error,
							fq_kfuncpln_note,fq_kfuncpln_warn,fq_kfuncpln_error,
							fp_kfuncpln_note,fp_kfuncpln_warn,fp_kfuncpln_error, ) = (
							
							fq_pln_note,     fq_pln_warn,     fq_pln_error,
							fp_pln_note,     fp_pln_warn,     fp_pln_error,
							fq_kfuncpln_note,fq_kfuncpln_warn,fq_kfuncpln_error,
							fp_kfuncpln_note,fp_kfuncpln_warn,fp_kfuncpln_error, ) = (
							
							_F.set_lvls())
#endregion

#region analyze helpers
def member_order(obj, predicate='ismethod'):
	predicate = isinstance(predicate, str) and getattr(inspect, predicate) or predicate
	members = [ (name,meth) for name, meth in inspect.getmembers(obj, predicate)]
	members = sorted(members, key=lambda t: t[1].__code__.co_firstlineno)
	return members

def method_order(obj):
	return member_order(obj, 'ismethod')

def get_func():
	frame = inspect.currentframe().f_back
	code = frame.f_code
	return [referer
		for referer in gc.get_referrers(code)
		if getattr(referer, '__code__', None) is code][0]

def get_closure():
	frame = inspect.currentframe().f_back
	code = frame.f_code
	return [referer
		for referer in gc.get_referrers(code)
		if getattr(referer, '__code__', None) is code and
		set(inspect.getclosurevars(referer).nonlocals.items()) <=
		set(frame.f_locals.items())][0]

def method_class(meth):
	if inspect.ismethod(meth):
		for cls in inspect.getmro(meth.__self__.__class__):
			if cls.__dict__.get(meth.__name__) is meth:
				return cls
		meth = meth.__func__  # fallback to __qualname__ parsing
		
	if inspect.isfunction(meth):
		class_name = meth.__qualname__.split('.<locals>', 1)[0].rsplit('.', 1)[0]
		cls = getattr(inspect.getmodule(meth), class_name, None)
		if cls is None:
			cls = meth.__globals__.get(class_name)
		if isinstance(cls, type):
			return cls
	
	return None

def func_operable_code(code, verbose=False):
	''' extracts all noop lines from the given function or code string '''
	import tokenize
	# todo: implement option to include function signature
	
	if type(code) is not str:
		code = inspect.getsource(code)               # coerce code into a string of python code
	
	NOOPS = [tokenize.COMMENT, tokenize.INDENT, tokenize.NEWLINE, tokenize.STRING, tokenize.NL]
	result_tokens, line_tokens = [], []
	is_signature, is_noop_line = False, True
	
	# pass in stringio.readline to generate_tokens
	for toktype, tokval, begin, end, line in tokenize.generate_tokens(StringIO(code).readline):
		# determine qualifiers for line given current token
		is_noop_line &= toktype in NOOPS
		is_signature |= tokval == 'def'
		if verbose:
			print(f'{line.strip():30s} noop_line:{is_noop_line:b}; signature:{is_signature:b}; '
				  f'{tokenize.tok_name[toktype]:8s}; \'{tokval.strip()}\'')
		
		# operate on token lists
		if toktype is not tokenize.COMMENT:
			line_tokens += [(toktype, tokval)]
		if toktype in [tokenize.NEWLINE, tokenize.NL]:
			if not  is_noop_line and not is_signature:
				result_tokens.extend(line_tokens)     # add token list to operable code tokens
				
			line_tokens = []                          # reset token list for next line
			is_signature, is_noop_line = False, True  # reset qulifiers for next line
			
	result = tokenize.untokenize(result_tokens)
	return result

def is_datadescriptor(obj):
	result = hasattr(obj, '__get__') and hasattr(obj, '__set__')
	return result

def cls_names(inst):
	result = [name for name, var in inst.__class__.__dict__.items() if var.__class__.__name__ == 'member_descriptor']
	return result

def cls_vars(inst):
	result = [[name, var] for name, var in inst.__class__.__dict__.items() if is_datadescriptor(var)]
	return result

def func_arg_keys(func):
	result = method_arg_keys(func) if isinstance(func, type) else func.__code__.co_varnames
	return result

def method_arg_keys(func):
	if isinstance(func, type):
		func = func.__init__
	result = func.__code__.co_varnames[1:]
	return result
#endregion

#region meta utils
def define(calls=None,to_name='{.__name__}',scope_vars=None,**ka_frame):
	# resolve inputs
	calls,to_name =  collany(calls),isstr(to_name,var='format')
	scope_vars = scope_vars if scope_vars is not None else bump_fqka(ka_frame,fquery=fframe).f_globals
	
	# resolve output
	calls_at = {to_name(call):call for call in calls}
	
	# update globals where needed
	if scope_vars is not False:
		scope_vars.update(calls_at)
	return calls_at
#endregion
