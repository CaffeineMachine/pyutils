''' Sequence Utilities
Contains a collection of functions and classes operating on a sequence.
'''
# todo: naming in this module needs to be consistent with or incorporate names from pyutils..itemdefs
# 	specifically: clip,chop vs part,partfirst,firstrest

from pyutils.defs.typedefs import *
from pyutils.defs.colldefs import *
from pyutils.utilities.iterutils import *

#region coll defs
IsKey = IsK					= str|int|slice
IsKeys = IsKK				= list[IsKey]
#endregion

#region coll utils
def getr_of(s_getr,coll=miss):
	''' prepare getr for a given func named *s_getr* '''
	def getr_of_coll(coll,key:IsK=miss):
		''' get =>val or =>getr for given *coll*ection & optional *key* via predefined func named *s_getr* '''
		# NOTE: getr_of_coll is similar to picker 
		getr = getattr(coll,s_getr)
		out = getr if key is miss else getr(key)
		return out
	stage = coll is miss and getr_of_coll or getr_of_coll(coll)
	return stage

def geteach(coll, keys:IsKK, getr:str='__getitem__', fabs=None, fab=None):
	''' get =>vals with each val corresponding to each key in *coll* '''
	getr_in,getr = getr, getr_of(getr,coll)
	
	vv_out = fabeach(getr,keys)
	vv_out = vv_out if not fabs else fabeach(fabs,vv_out)
	vv_out = vv_out if not fab else fab(vv_out)
	return vv_out
def getzip(colls, keys:IsKK, getr:str='__getitem__', fabs=None, fab=None):
	''' get =>vals with each val from every pair of *colls* & *keys* '''
	picker = getr_of(getr)  # 
	
	vv_out = (picker(coll,key) for coll,key in zip(colls,keys))
	vv_out = vv_out if not fabs else fabeach(fabs,vv_out)
	vv_out = vv_out if not fab else fab(vv_out)
	return vv_out

def keys_op_of(op):
	# resolve inputs
	op = op if iscx(op) else getattr(set,op)
	def op_of_keys(kk_a, kk_b, *colls, fab=None):
		# resolve output
		kk_out = op(set(kk_a), set(kk_b))
		kk_out = kk_out if not colls else op_of_keys(kk_out,*colls)
		kk_out = kk_out if not fab else fab(kk_out)
		return kk_out
	return op_of_keys

gets,getz					= geteach,getzip
and_keys = and_kk			= keys_op_of(set.__and__)
or_keys  = or_kk			= keys_op_of(set.__or__)
gt_keys  = gt_kk			= keys_op_of(set.__gt__)
lt_keys  = lt_kk			= keys_op_of(set.__lt__)
ge_keys  = ge_kk			= keys_op_of(set.__ge__)
le_keys  = le_kk			= keys_op_of(set.__le__)
eq_keys  = eq_kk			= keys_op_of(set.__eq__)
#endregion

#region index utils
def iterany(obj,fill=()):
	try:
		return iter(obj)
	except Exception:
		return fill
def lenany(obj,fill=False):
	try:
		return len(obj)
	except TypeError:
		return fill

def iinvert(ind, seq, *, len_hint:int=None):
	''' invert(At[3:-3], 'abcdefghijklmnopqrstuvwxyz')		# => ('abc','xyz') '''
	# todo: handle iterable *seq* arg
	# resolve inputs
	ind = (
		ind								if isinstance(ind, range) else
		range(int(ind), int(ind)+1)		if isinstance(ind, (int,float)) else
		range(ind.start, ind.stop)		if isinstance(ind, slice) else
		range(*ind)
	)
	if ind.stop < 0:
		seq = seq if ischrs(seq) else tuple(seq)  # unpacking iterable is required when indexing from last
		ind = range(ind.start, max(ind.start, ind.stop+len(seq)))
		
	# resolve output
	if ind.step in (None, 1):
		yield from (seq[None:ind.start], seq[ind.stop:None])
	else:
		start = 0
		for stop in ind:
			yield seq[start:stop]
			start = stop
		yield seq[start:]

def invertj(ind, seq, join=None):
	''' invertjoin(At[3:-3], 'abcdefghijklmnopqrstuvwxyz')	# => 'abcxyz' '''
	# resolve inputs
	_join = join or ''.join if isinstance(seq, str) else join
	
	# resolve output
	if getattr(ind, 'step', ...) in (None, 1) and not join:
		result = seq[None:ind.start] + seq[ind.stop:None]  # optimization not allowed if *join* arg is given
	elif _join:
		result = _join(iinvert(ind, seq))
	else:
		iseq_inv = iinvert(ind, seq)
		result = next(iseq_inv)
		for sub in iseq_inv:
			result = result + sub
	return result

def less_seq(seq, trim:int|list=None, most=4, join='..'):
	''' truncate given *seq* to *trim*med ends its len exceeds *most*
		:param seq:  vals to truncate
		:param trim: number of vals to get from seq ends. gets left|right|both ends given +int|-int|[+int,+int] resp.
		:param most: len beyond which to trim output
		:param join: insert between trimmed ends when given val != miss 
	'''
	join = join != miss and (join,) or ()
	head,tail,most = (
		[most//2,(most+1)//2,most]			if trim is None else
		[*trim,most or sum(trim)]			if isiter(trim) else
		throw((trim,most))					if isntint(trim) else
		[+trim,0,most or abs(trim)]			if 0 <= trim else
		[0,-trim,most or abs(trim)] )
	out = (
		[*seq[:head],*join,*seq[-tail:]]	if most < len(seq) else
		seq )
	return out

def less(seq, size=3):		return invertj(range(size,-size), seq, list)
def lessa(seq):				return invertj(range(8**1,-8**1), seq, list)
def lessb(seq):				return invertj(range(8**2,-8**2), seq, list)
def lessc(seq):				return invertj(range(8**3,-8**3), seq, list)

dinvert, kinvert, linvert, tinvert = redef_per_coll(iinvert)
invert = linvert
#endregion

#region seq modify utils
def mux(seq, into) -> list:
	''' allocate items in *seq* to channels defined by cyclic indexes *into*; produce _list_ of channels. '''
	into = range(into) if isinstance(into, int) else into
	channels = [[] for ind in range(max(into))]
	for key, item in zip(seq, irepeat(into)):
		channels[key].append(item)
	return channels

def muxjoin(seq, into, join=ijoin) -> list:
	''' allocate items in *seq* to channels defined by cyclic indexes *into*; produce _joined list_ of channels. '''
	channels = multiplex(seq, into)
	channels = [join(channel) for channel in range(max(into))]
	return channels

def muxmap(seq, into) -> dict:
	''' allocate items in *seq* to channels defined by cyclic keys *into*; produce _map_ of channels. '''
	channels = {key:[] for key in into}
	for key, item in zip(seq, irepeat(into)):
		channels[key].append(item)
	return channels

def muxmapjoin(seq, into, join=ijoin) -> dict:
	''' allocate items in *seq* to channels defined by cyclic keys *into*; produce _combined map_ of channels. '''
	channels = multiplexmap(seq, into)
	channels = {key:join(channel) for key, channel in channels.items()}
	return channels

def to_front(seq, values):
	seq[:0] = map(take.__get__(seq), values)
	return seq

def to_back(seq, values):
	seq[max_uint64:] = map(take.__get__(seq), values)
	return seq

def take(seq, value, count='todo') ->object:
	''' Find and remove given *value* from *seq*. Returns removed item; unlike list.remove() counterpart. '''
	result = seq.pop(seq.index(value))
	return result

def clip(seq, ind=None, *ind_pa_todo):
	''' take and return item(s) from *seq* at given *ind* '''
	if ind is None: return clip.__get__(seq)
	
	result = seq[ind]
	ind = ind if isinstance(ind, slice) else slice(ind, ind+1 or None)
	seq[ind] = []
	return result

def chop(seq, ind=None, *ind_pa_todo, modify=True):
	''' take and return item(s) from *seq* at given *ind* and the remaining '''
	# fixme: resolve functions purpose distinct from splitby and clip list.__getitem__
	if ind is None: return chop.__get__(seq)
	
	remain = seq
	result = seq[ind]
	ind = ind if isinstance(ind, slice) else slice(ind, ind+1 or None)
	if modify and hasattr(seq, '__setitem__'):
		remain[ind] = []
	else:
		remain = remain[0:ind.start] + remain[ind.stop:]
	return result, remain

def icut(inds, seq=None):
	''' return all parts of *seq* separated at given *ind* '''
	if seq is None: return icut.__get__(inds)
	
	inds = isiter(inds, orput=[inds])
	ind_l = 0
	for ind_r in inds:
		part = seq[ind_l:ind_r]
		yield part
		ind_l = ind_r
	yield seq[ind_l:None]

def isplitby(seq, sep=nothing, sep_cmp=nothing, count_todo=None, fab=None):
	''' split given *seq*uence into subseqences delimited by each matching *sep*arator
		:param seq: sequence to split by separator
		:param sep: identifies separators as being equal to *sep* if given; takes priority over sep_cmp
		:param sep_cmp: identifies separators with bool(*sep_cmp*(item)) if given
		:param cast_subseq: used to casts each subseq if not None
	'''
	fab = None if fab == list else fab  # does not need to cast to list
	subseq = []
	for item in seq:
		is_sep_match = sep_cmp(item) if sep is miss else item == sep
		if is_sep_match:
			subseq = fab(subseq) if fab else subseq
			yield subseq
			subseq = []
		else:
			subseq.append(item)
			
	subseq = fab(subseq) if fab else subseq
	yield subseq

def lsplitby(*pa, **ka): return list(isplitby(*pa, **ka))

def splitargs(seq, fab=None):
	''' split given args *seq*uence by '...' separator into largs and rargs (usually; but not limited to 1 sep) '''
	result = list(isplitby(seq, ..., fab=fab))
	if len(result) < 2:
		result.append([])
	return result

multiplex, multiplexjoin, multiplexmap, multiplexmapjoin = mux, muxjoin, muxmap, muxmapjoin
#endregion

#region scratch area
''' Naming Convention

# pop
take, pop, oust, remove, drop, expel, slice, cut

# insert
place, depo, add, put, apply, affix, tack, pin, give, store, push

# by index
put, cut

# by value
puteq, cuteq
puteq, take

# by id
putid, cutid

# by condition
putif, cutif

# ...
chop
'''

'''
Inv[1:-1].set('abcd', '<>')						# => '<abcd>'
Inv[3:-3].get('abcdefghijklmnopqrstuvwxyz')		# => ['abc','xyz']
Invj[3:-3].get('abcdefghijklmnopqrstuvwxyz')	# => 'abcxyz'
Sep[3:-3].get('abcdefghijklmnopqrstuvwxyz')		# => ['abc','defghijklmnopqrstuvw','xyz']
Ind[3:-3].cut('abcdefghijklmnopqrstuvwxyz')		# => ['defghijklmnopqrstuvw','abcxyz']
Inv[3:-3].cut('abcdefghijklmnopqrstuvwxyz')		# => ['abcxyz','defghijklmnopqrstuvw']
'''
#endregion