from pyutils.defs.funcdefs import noop
from pyutils.defs.colldefs import iscoll, str0
from pyutils.defs.itemdefs import Ndx, at0, at1
from pyutils.utilities.collbasics import *
from pyutils.utilities.callbasics import *

#region args defs
A2							= (tuple,dict)
KA2							= dict
_D							= Obj()
class DefsArgsUtils:
	#region namespaces
	class Strs:
		kk_aa = _D.kk		= tss('pa  ka  aa  pal  par  is_pka')
		kk_faa				= tss('call  fab') + kk_aa
		_					= call,fab,pa,ka,aa,pal,par,is_pka = kk_faa
		h_aa				= set(kk_aa)
		h_faa				= set(kk_faa)
	class Args:
		nndx				= pa, ka, aa,      = (
							  0,  1,  Ndx.all, )
		ndx_at = at			= dict(zip(_D.kk,nndx))
		is_key_fmtaa		= noop
	
	AA,SS					= Args,Strs
	#endregion
	...

DDAAU = _D					= DefsArgsUtils
AAAAU,SSAAU = _A,_S			= DDAAU.AA,DDAAU.SS
#endregion

#region args forms
_D.fmt_at					= Obj(
	pa						= '*{} ',
	ka						= '**{}',
)

def a2_of_args(pa,ka) ->A2:
	''' coerce given args *pa*,*ka* into standard form ->(tuple,dict) or A2 '''
	a2 = tss(pa), dict(ka)
	return a2
def aa_of_argstar(*_pa, pa=None, ka=None, fab=None, **_ka):
	''' coerce given any args into standard form ->(tuple,dict) or A2 '''
	aa = pa or _pa, ka or _ka
	aa = aa if not fab else a2_of(*aa) if fab==True else fab((pa,ka))
	return aa
def aa_of_ka2(ka2=None, key_pa='pa', fab=None, **_ka2):
	''' coerce given args *pa*,*ka* into standard form ->(tuple,dict) or A2 '''
	ka2 = ka2 or _ka2
	aa = ka2.pop(key_pa,()), ka2
	aa = aa if not fab else a2_of(*aa) if fab==True else fab(aa)
	return aa
def ka2_of_argstar(*_pa, pa=None, ka=None, **_ka):
	''' coerce given any args into ->dict or KA2 containing item for pa '''
	pa,ka2 = pa or _pa, ka or _ka
	ka2.update(pa=pa)
	return ka2
def ka2_of_a2(a2, key_pa='pa') ->KA2:
	''' coerce given args *pa*,*ka* into ->dict or KA2 containing item for pa '''
	pa,ka2 = a2
	ka2[key_pa] = pa
	return ka2

def ipa_pai(pai):
	ipa = zip(pai)
	return ipa
def ika_kai(kai):
	# ika = [dict(zip(kai.keys(), values)) for values in zip(*kai.values())]
	ika = (dict(zip(kai.keys(), values)) for values in zip(*kai.values()))
	return ika
def iargs_argsi(pai, kai):
	ipa, ika = ipa_pai(pai), ika_kai(kai)
	empties = [[]]*max(len(ipa), len(ika))
	iargs = [*zip(ipa or empties, ika or empties)]
	return iargs
def argsi_iargs(iargs):
	pai, kai = [], []  # todo
	for pa, ka in iargs:
		for ind, arg in enumerate(pa):
			pai[ind].append(arg)
		for key, arg in ka.items():
			kai[key].append(arg)
	return pai, kai

def iitem_args(pa,ka, order=None):
	''' iterate each ->..(key,val) in given args '''
	yield from enumerate(pa)
	yield from ka.items() if not order else ((k,ka[k]) for k in (sorted(ka) if order == True else ss(order)))
def ikey_args(pa,ka, order=None):
	''' iterate each ->..key in given args '''
	yield from range(len(pa))
	yield from ka if not order else sorted(ka) if order == True else ss(order)
def ival_args(pa,ka, order=None):
	''' iterate each ->..val in given args '''
	yield from pa
	yield from ka.values() if not order else map(ka.__getitem__,sorted(ka) if order == True else ss(order))
	
def add_args(args,args_new,left=no,onto=no,fab=yes):
	''' get ->Args by adding *args_new* to *args* prioritizing newest '''
	pa,ka,pa_new,ka_new = tj(args,args_new)
	aa = tj([pa,pa_new][::left and -1 or 1]), dict(ka,**ka_new)
	aa = aa if not fab or onto else istrue(fab,put=type(args))(*aa)  # fab is not allowed if modifying *args* directly
	# todo: onto
	return aa

def rep_args(pa,ka, _kk=None, _fmt_aa='{pa} {ka}', _fmt_at=map0) ->str:
	''' get ->str repr for pa:tuple|ka:dict *args* based on fmt found at *_kkfmt*[int|str] resp. '''
	# represent args
	def irep_args(args):
		kkarg = (
			enumerate(args)					if isntdict(args) else
			args.items()					if not _kk and isdict(args) else
			((k,args[k]) for k in _kk if k in args) )
		for k,arg in kkarg:
			if rep_args.is_local(k):		continue
			fmt = _fmt_at.get(k)
			s_arg = (
				''							if fmt == str0 else					# noop when fmt is str0
				str(arg)					if fmt is None and isint(k) else	# as pa when k is int
				s_of(fmt or '{k}={}',arg,k=k) )									# as given fmt
			if s_arg:
				yield s_arg
	ka = dict(ka)
	_kk,_fmt_aa,_fmt_at = _kk and tss(_kk), ka.pop('_fmt_aa',_fmt_aa), ka.pop('_fmt_at',istrue(_fmt_at,put=_D.fmt_at))
	pa,ka = map(' '.join, map(irep_args,(pa,ka)))
	pa,ka = ss_a2 = [a and (fmt:=_fmt_at.get(k)) and sof(fmt,a) or a for k,a in (('pa',pa),('ka',ka))]
	
	# resolve output
	s_out = s_of(_fmt_aa, *ss_a2, pa=pa,ka=ka)
	return s_out
def rep_argstar(*pa,**ka) ->str:	return rep_args(pa,ka)

def unzip_argstar(kk,*_pa,pa=None, fab=None, eq_keys=no, ka=None,**_ka):
	# resolve inputs
	kk,ka = tuple(kk),ka or _ka
	assert not eq_keys or set(kk) == set(ka), f'expected kk:{kk} not in ka:{tuple(ka)}'
	
	# resolve output
	az = vv,_ = _pa + tuple(ka[k] for k in kk), kk
	az = az if not fab else fab(az)
	return az

# def zip_argz(vv,kk=(),fillsr=(),n_miss=0, miss=..., at=None, fab=None,ka=None,**_ka):
def zip_argz(kk,vv,fillsr=(),n_miss=0, miss=..., at=None, fab=None,ka=None,**_ka):
	''' create args ->pa,ka from given keys and vals *vv*,*kk*,*after*,*n_miss* '''
	# resolve inputs
	kk = kk and tss(kk)
	n_vv,n_kk = len(vv), len(kk)
	# at = AAAA.ndx_at.get(at,at)
	assert n_vv >= n_kk
	
	# resolve input vals:  => vv
	fills = list(n_miss*(miss,) + fillsr)
	_vv,vv = vv, list(vv)+fills[n_vv:]			# append defaults to vals
	assert miss not in vv
	
	# resolve args:  => aa,pa,ka
	aa = pa,ka = vv[:-n_kk], ka or _ka		# get pa
	ka.update(zip(irev(kk),irev(vv)))		# get ka
	aa = aa if at is None else at(aa) if iscx(at) else aa[at]
	aa = aa if not fab else fab(aa)			# get aa
	return aa
def args_of_anyargzip(kk,*_pa,pa=None,ka=None,**_ka): pass

def defaults_of_func(func, lpad:int=False, fill=nothing):
	defaults = (func.__defaults__ or ()) + (func.__kwdefaults__ and tuple(func.__kwdefaults__.values()) or ())
	lpad = nargs_of_func(func)-len(defaults) if lpad is True else lpad
	defaults = defaults if not lpad else ((fill,)*lpad)+defaults
	return defaults

def nargs_of_func(func, rjust=True,fill=None) ->int:
	return func.__code__.co_argcount + func.__code__.co_kwonlyargcount

def resolve_args(keys,pa,ka=(),defaults_lpad=(),defaults=None,keys_excl=(),fab=tuple):
	''' resolve passed ->args as a sequence with keys stripped '''
	# resolve inputs
	keys_excl = keys_excl and set(keys_excl)
	defaults_lpad = defaults_lpad if defaults is None else ij(ngen(len(keys)-len(defaults)), defaults)
	defaults_lpad = defaults_lpad if not ka else [ka.get(k,v) for k,v in zip(keys,defaults_lpad)]
	
	# resolve output
	# args = istacked(pa,defaults_lpad,end=...)
	pa_out = tj(pa,defaults_lpad[:len(pa)])
	pa_out = (arg for key,arg in zip(keys,pa_out) if key not in keys_excl)
	pa_out = pa_out if not fab else fab(pa_out)
	return pa_out

def annovars_of_args(*pa,**ka):  # todo
	raise NotImplementedError

def is_args(obj):
	''' *obj* qualifies as Args structure if one of the following apply:
		- obj DOES HAVE 'is_aa' attr or key equal to True
		- obj DOES NOT HAVE 'is_aa' attr or key and has one of following keys:  {pa|ka|pal|par}
	'''
	isaa_out = (
		obj.is_aa			if hasattr(obj, _S.is_pka) else
		False				if not istype(obj, dict) else
		obj[_S.is_pka]		if _S.is_pka in obj else
		_S.h_aa.issuperset(obj) )
	return isaa_out

pa_of_a2,ka_of_a2			= at0,at1
ikvaa,ikaa,ivaa				= iitem_args,ikey_args,ival_args
a2_of_aa = a2_of			= a2_of_args
aastar						= aa_of_argstar
ka2star						= ka2_of_argstar
repstar						= rep_argstar
unzipstar = azstar			= unzip_argstar
zip_az						= zip_argz
rep_aa						= rep_args
rep_aa.is_local				= hss('_fmt_at  _fmt_aa').__contains__
_D.is_key_fmtaa				= rep_aa.is_local
is_aa						= is_args

aa_of_anyaz					= args_of_anyargzip	# todo: eliminate all
# zip_aa						= zip_argz			# todo: eliminate all
# zip_args					= zip_argz			# todo: eliminate all
#endregion

#region remap args
def into_pos_args(pa,ka, kk_pop, left=no, fab_pa=tuple, fab=None, **_ka):
	''' remap ka into pa
		:param pa:     pos args seq
		:param ka:     key args map
		:param kk_pop: keys to pop from ka and put into pa
		:param left:   add args to right|left of pa when False|True resp. defaults to False or append right
	'''
	pa2 = [pa, list(map(ka.pop,kk_pop))][::left and -1 or 1]
	pa = ij(pa2) if not fab_pa else fab_pa(ij(pa2))
	aa = pa,ka
	aa = aa if not fab else fab(aa)
	return aa

def into_key_args(pa,ka, kk_put, rev=no, fab=None, **_ka):
	''' remap pa into ka
		:param pa:     pos args seq
		:param ka:     key args map
		:param kk_put: keys to put into ka and pop from pa
		:param rev:
	'''
	# resolve inputs
	aa = pa,_ = list(pa),ka
	kk_put,n_pop = list(kk_put), rev and -1 or 0
	
	# resolve output
	while pa and kk_put:
		k_put,arg = kk_put.pop(0),pa.pop(n_pop)
		ka[k_put] = arg
	aa = aa if not fab else fab(aa)
	return aa

def into_keycmp_args(pa,ka, kkcmp, fab=None, over=yes, **_ka):
	''' remap per type into ka
		:param pa:    pos args seq
		:param ka:    key args map
		:param kkcmp: keys to put into ka and pop from pa where cmp(arg) equals True. as list|dict like [(key,cmp),..]
	'''
	aa = pa,_ = list(pa),ka
	kkcmp = list(kkcmp.items()) if isdict(kkcmp) else kkcmp if iscoll(kkcmp) else list(kkcmp)
	assert kkcmp
	
	for ndx,arg in enumerate(pa):  # todo: idx inconsistency after first pop if not reverse order
		for key,cmp in kkcmp:
			if cmp(arg):
				not over and key in ka and throw('conflict on {!r} with: {} {}'.format(key,pa,ka))
				ka[key] = pa.pop(ndx)
	aa = aa if not fab else fab(aa)
	return aa

def into_args(pa,ka, kkcmp=None, kk_put=None, kk_pop=None, fab=None, **_ka):
	''' remap between pa & ka
		:param pa:     pos args seq
		:param ka:     key args map
		:param kkcmp:  keys to put into ka and pop from pa where cmp(arg) equals True. as list|dict like [(key,cmp),..]
		:param kk_put: keys to put into ka and pop from pa
		:param kk_pop: keys to pop from ka and put into pa
	'''
	aa = pa,ka
	aa = aa if not kkcmp  else into_keycmp_args(*aa, kkcmp,  **_ka)
	aa = aa if not kk_put else into_key_args   (*aa, kk_put, **_ka)
	aa = aa if not kk_pop else into_pos_args   (*aa, kk_pop, **_ka)
	aa = aa if not fab    else a2_of(*aa) if fab==True else fab(aa)
	return aa

def shift_args(pa,ka, kk_put=(), kk_pop=(), at=None, fab=None, ka_fill=None, err=on, fill=miss):
	''' create ->args from given *pa*,*ka* by adding|removing keys *kk_put*|*kk_pop* resp. '''
	# todo: obsoleted by into_args
	# if not kk_put and not kk_pop:	return pa,ka  # noop case
	# resolve inputs
	kk_put,kk_pop = kk_put and tss(kk_put), kk_pop and tss(kk_pop)
	at,fill_pa = _A.at.get(at,at), fill is not miss and (fill,) or ()
	assert not (kk_put and kk_pop), [kk_put,kk_pop]
	
	# resolve output args:  => aa
	aa = (
		zip_az(kk_put,pa,ka=ka)									if kk_put and len(kk_put)<=len(pa) else
		((*pa,*(ka.pop(k,*fill_pa) for k in kk_pop)), ka)		if kk_pop and len(kk_pop)<=len(ka) else
		throw(f'pa:{pa} not long enough for kk_put:{kk_put}')	if kk_put and err else
		throw(f'ka:{ka} not long enough for kk_pop:{kk_pop}')	if kk_pop and err else
		(pa,ka) )
	aa = aa if at is None else at(aa) if iscx(at) else aa[at]
	aa = aa if not fab else fab(aa)
	return aa

def into_args_of(*kk_into,kk_in=None,kk_to=None):
	''' get args transforms as reindexer and remapper '''
	# resolve inputs
	kk_in,kk_to = (
		[kk_in or (),kk_to or ()]	if len(kk_into) == 0 else
		[kk_in or (),*kk_into]		if len(kk_into) == 1 and kk_to is None else
		[*kk_into,kk_to or ()]		if len(kk_into) == 1 and kk_in is None else
		kk_into )
	n_to = len(kk_to)
	
	# resolve output
	pa_into,ka_into = kk_in[:-n_to], (kk_in[-n_to:],kk_to)
	return pa_into,ka_into

def convert_args_into(*pa_into,aa_into=None,ins_pos=on,args=None,_args=None,**ka_into):
	''' transform any given args *pa* & *ka* into any other set of args '''
	# resolve inputs
	pa,ka,_pa,_ka = *(args or (None,None)), *(_args or ((),{}))
	pa_into,ka_into = aa_into if aa_into is not None else into_args_of(*pa_into,**ka_into)
	ka_into = ka_into or () if isntdict(ka_into) else [ka_into.keys(),ka_into.values()]
	n_at = not ins_pos and 1 or 0
	
	# resolve output
	def convert_args(*pa,**ka):
		# todo: accept function converters
		pa_out,ka_out = list(_pa),dict(_ka)
		for k_in,k_to in ij(enumerate(pa_into),zip(*ka_into)):
			out,k_to = (
				(pa_out,at_ins(k_to,n_at))	if isint(k_to) or isstr(k_to) and k_to.isnumeric() else
				(ka_out,k_to) )
			v = (
				pa[int(k_in)]				if isint(k_in) or isstr(k_in) and k_in.isnumeric() else
				ka[k_in] )
			out[k_to] = istype(k_to,slice) and [v] or v
		return pa_out,ka_out
	result = None is pa is ka and convert_args or convert_args(*pa or (),**ka or {})
	return result

intopa						= into_pos_args
intoka						= into_key_args
intokacmp					= into_keycmp_args
intoaa						= into_args
shift_aa					= shift_args
#endregion

#region args utils
at_ins						= lambda at,n_at=0: slice(int(at),int(at)+n_at)
ins_v						= lambda seq,k,v: seq.__setitem__(slice(int(k),int(k)),[v])

def kkstage_args(kk_stage,pa=None,ka=None,fab=None):
	# resolve inputs
	kk_stage = isset(kk_stage,orfab=hss)
	def kkstage_of_args(pa=(),ka=map0):
		kk_stage_out = kk_stage - set(ikaa(pa,ka))
		kk_stage_out = kk_stage_out if not fab else fab(kk_stage_out)
		return kk_stage_out
	
	# resolve output
	staged = (None is pa is ka) and kkstage_of_args or kkstage_of_args(pa,ka)
	return staged

def ieval_stage_args(kk_stage,kk_take=(),on_new=no):
	''' generator which evaluates args staging readiness and updates args with each new stage
		:param kk_stage: required keys for ka or count of pa
		:param kk_take: transfer keys from opposite args if matching (optional)
		:param on_new:  keep new or old args when True,False resp. is used to validate when callable. on_new(ka,ka_new)
	'''
	# resolve & validate inputs
	kk_stage = set(kk_stage)
	cls_take = set(map(type,kk_take))
	assert kk_take is None or set(map(type,kk_take)) in {}
	
	# reeval on each new args:  => kk_stage
	pa_new,ka_new			= yield kk_stage, [], {}
	while kk_stage:
		pa,ka = (
			on_new((ka,ka_new))				if iscx(on_new) else
			(pa+[*pa_new],{**ka_new,**ka})	if on_new == True else
			([*pa_new]+pa,{**ka,**ka_new}) )
		
		# remap args amongst pa & ka
		kk_stage -= set(ikaa(pa,ka))
		for k_take in kk_stage and kk_take:
			k_stage			= kk_stage[0]
			if pa and isint(k_take):
				k_stage		= kk_stage.pop(0)
				ka[k_stage]	= pa.pop(k_take)
			elif k_take in ka:
				k_stage		= kk_stage.pop(0)
				pa[k_stage]	= ka.pop(k_take)
			else:
				break
				
				# sbj,src			= isstr(k_in) and (pa,ka) or (ka,pa)
				# sbj[k_stage]		= src.pop(k_take)
			
		pa_new,ka_new		= yield kk_stage, pa, ka

def staging(kk_stage,kk_take=(),on_new=no,fab=None):
	iev_stage = ieval_stage_args(kk_stage,kk_take,on_new)
	def stage(*pa,**ka):
		kk_stage,pa,ka = iev_stage.send((pa,ka))
		staged = kk_stage and stage or fab(*pa,**ka)
		return staged
	return stage

kkunstaged_args				= kkstage_args  # todo: this is preferred
is_staging,is_staged		= redef(kkstage_args,fab=truth),redef(kkstage_args,fab=untruth)
#endregion

#region notes
'''
todo: resolve notations

a2|argsbase			(tuple,dict)
pa|pargs			tuple
ka|kargs			dict
ka2					dict			dict(c=2,d=3,pa=(0,1))
aa|args				(tuple,dict)
aas|star|argstar	*pa,**ka
az|argz|argmap

* aap
* pka
* aa2
* aaa

'''
#endregion
