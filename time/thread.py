#!/usr/bin/env python

import sys
import threading
from threading import Thread, RLock, Timer, _RLock, _CRLock
from time import sleep
from contextlib import contextmanager
from collections import OrderedDict as odict, deque
from datetime import datetime

from pyutils.defs.colldefs import *
from pyutils.utilities.taskutils import *
from pyutils.utilities.pathutils import *
from pyutils.extend.datastructures import XDict
from pyutils.output.colorize import *

#region tasking defs
class DefsTasking:  # task timer schedule
	atomic_rsrc_lock_at	= {}
	fmt_worker_name		= '{s_func}(*{pa!s:.32}, **{ka!s:.32})'

DDTask = _D				= DefsTasking
#endregion

#region tasking utils
def work(func, *pa, name=_D.fmt_worker_name, delay=.01, start=True, **ka):
	''' simple Timer wrapper for those wanting single line immediate thread creation '''
	worker = Timer(delay, func, args=pa, kwargs=ka)
	worker._name = name.format(s_func=qualof(func), pa=pa, ka=ka)
	start and worker.start()
	start and fp_kfuncpln(frame=1)
	return worker
def workd(func, *pa, **ka):	work(func, *pa, daemon=True, **ka)

def on_ticks(ticks, call, *pa, reps=0,sleep=sleep,is_abs=no, start=no, now=yes, **ka):
	''' issue *call*(*pa,**ka) after each delay in *ticks*. if *is_abs* ticks are relative to start '''
	# resolve inputs
	ticks = collany(ticks)
	ticks = ticks if not is_abs else throw()
	
	# resolve output
	def call_on_tick():
		for _ in range(reps+1):
			for tick in ticks:
				sleep(tick)
				call(*pa,**ka)
	staged = (
		work(call_on_tick)	if start else
		call_on_tick()		if now else
		call_on_tick )
	return staged

@contextmanager
def atomic(rsrc, blocking=True, timeout=-1):
	# produce RLock from resource
	rlock = rsrc
	if not isinstance(rsrc, (_RLock, _CRLock or _RLock)):
		k_rsrc = rsrc if isinstance(rsrc, int) else id(rsrc)
		rsrc,rlock = DDTask.atomic_rsrc_lock_at.get(k_rsrc) or DDTask.atomic_rsrc_lock_at.setdefault(k_rsrc, (str(rsrc),RLock()))
	
	# acquire exclusive access to lock
	locked = rlock.acquire(blocking=blocking, timeout=timeout)
	try:
		yield locked
	finally:
		rlock.release()  # always release lock

def exc_handler_no_catch(exc):
	print('exc_handler_no_catch()')
	try:
		raise exc
	finally:
		pass

def register_thread_excepthook():
	''' Workaround for sys.excepthook thread bug From
		http://spyced.blogspot.com/2007/06/workaround-for-sysexcepthook-bug.html
		(https://sourceforge.net/tracker/?func=detail&atid=105470&aid=1230540&group_id=5470).
		
		Call once from __main__ before creating any threads.
		If using psyco, call psyco.cannotcompile(threading.Thread.run)
		since this replaces a new-style class method.
	'''
	init_old = threading.Thread.__init__
	def init(self, *args, **kwargs):
		init_old(self, *args, **kwargs)
		run_old = self.run
		
		def run_with_except_hook(*args, **kw):
			try:
				run_old(*args, **kw)
			except (KeyboardInterrupt, SystemExit):
				raise
			except:
				print('installThreadExcepthook')
				sys.excepthook(*sys.exc_info())
		self.run = run_with_except_hook
	threading.Thread.__init__ = init

worker,workerd			= work,workd
#endregion

#region tasking devcs
class ExcTimer(Timer):
	''' Timer extended to record the terminating exception '''
	def __init__(self, *pa, **ka):
		''' ctor '''
		super(ExcTimer, self).__init__(*pa, **ka)
		self.exc = None
	def run(self):
		''' wraps call to parent run() to store any exception '''
		try:
			super(ExcTimer, self).run()
		except Exception as exc:
			self.exc_info = sys.exc_info()
			raise

class ExcInfoThread(Thread):
	''' Thread extended to record the terminating exception '''
	def __init__(self, *pa, **ka):
		''' ctor '''
		super(ExcThread, self).__init__(*pa, **ka)
		self.exc_info = None
	def run(self):
		''' wraps call to parent run() to store any exception '''
		try:
			super(ExcThread, self).run()
		except Exception as exc:
			self.exc_info = sys.exc_info()
			# raise

class ExcThread(Thread):
	''' Thread extended to record the terminating exception '''
	def __init__(self, *pa, **ka):
		''' ctor '''
		super(ExcThread, self).__init__(*pa, **ka)
	def run(self):
		''' run without suppressing exceptions '''
		self._target(*self._args, **self._kwargs)

class ResultsThread(Thread):
	''' source: https://stackoverflow.com/questions/6893968/how-to-get-the-return-value-from-a-thread-in-python
		author: user2426679, kindall
	'''
	def __init__(self, *pa, **ka):
		super(ResultsThread, self).__init__(*pa, **ka)
		self._result = None
	def run(self):
		if self._Thread__target is not None:
			self._result = self._Thread__target(*self._Thread__pa, **self._Thread__ka)
	def join(self, *pa, **ka):
		super(ResultsThread, self).join(*pa, **ka)
		return self._result

class RepeatTimerDynamicConfig:
	undefined = 'undefined'
	def __init__(self, interval=undefined, max_repeat=undefined, max_concurrency=undefined, is_finished=undefined):
		ka = XDict(locals()).remove([RepeatTimerDynamicConfig.undefined]) - ['self'] # pa w/o self or undefined values
		self.__dict__.update(ka)
		self.__dict__.setdefault('max_repeat', None)
		self.__dict__.setdefault('max_concurrency', None)
		self.__dict__.setdefault('is_finished', False)

class RepeatPolicy(RepeatTimerDynamicConfig):
	''' Strategies for managing RepeatTimer thread concurrency
		1. notify: warn on concurrent detected (default)
		2. basic: interrupt timeout
		4. sequential: interval is an idle timeout; conclusion of previous task triggers timer countdown
		3. managed concurrency: interval must elapse and concurrency must be less than max workers
		5. user defined: worker is created if a a given condition is satisfied
		6. none: No intervention logic
	'''
	enum_max_runtime_actions = (show_warning, force_stop) = range(2)
	default = dict(
		sequential_timing=False,	# interval countdown start at completion of previous task
		max_repeat = None,			# limit to number of total worker created
		max_concurrency = None,		# limit to enforce for simultaneous running workers
		max_concurrency_msg = None,	# message when concurrency limit prevents new worker creation
		max_runtime = None,  		# total runtime limit for each worker
		max_runtime_actions = [],	# policy actions for when a thread has exceeded max_runtime
		condition = None,			# user defined condition for allowing creation of new workers
		exc_handler_repeat = None,	# user defined condition for allowing creation of new workers
		exc_handler_join = None,	# user defined condition for allowing creation of new workers
	)
	predefined = (no_policy, basic, notify, sequential, concurrent) = (
		default.copy(),
		default | XDict(max_runtime_actions=[show_warning, force_stop]),
		default | XDict(max_runtime_actions=[show_warning]),
		default | XDict(sequential_timing=True),
		default | XDict(max_concurrent=10))
	undefined = RepeatTimerDynamicConfig.undefined
	def __init__(
		self,
		interval,
		predefined = sequential,  # choose from pre-defined RepeatPolicy.predefined
		sequential_timing = undefined,
		max_repeat = undefined,
		max_concurrent = undefined,
		max_concurrent_msg = undefined,
		max_runtime_actions = undefined,
		max_runtime = undefined,
		condition = undefined,
		exc_handler_repeat = undefined,
		exc_handler_join = undefined,
	):
		''' contruct '''
		params = (XDict(locals()) ^ ['self']) - [RepeatPolicy.undefined]
		if isinstance(predefined, dict):
			params = predefined | XDict(params)
		self.__dict__.update(XDict(params) ^ ['self'])
		RepeatTimerDynamicConfig.__init__(self, interval, max_repeat)
		self.started_count = 0
		self.stopped_count = 0
	@staticmethod
	def from_repr(*pa, **ka):
		''' create class given a valid repr '''
		if len(pa)  and isinstance(pa[0], RepeatPolicy):
			return pa[0]
		return RepeatPolicy(*pa, **ka)
	def can_repeat(self):
		''' return if repeat is allowed based on the repeat policy and state '''
		allow_repeat = ((self.max_repeat is None								# (repeat count is undefined
			or self.started_count < self.max_repeat)							# | below max_repeat)
			and (self.max_concurrency is None									# & (max_concurrency is undefined
			or self.started_count - self.stopped_count < self.max_concurrency))	# | below max_concurrency)
		return allow_repeat
	def on_start_owner(self, owner):
		''' handle owner start event '''
		self.owner = owner
	def on_start_worker(self, worker, start_time):
		''' handle owner start event '''
		if self.max_concurrency and self.can_repeat():
			self.started_count += 1
			self.owner.queue_next_worker(self.interval)

		if self.max_runtime_actions:
			worker.join(self.max_runtime)
			if worker.is_alive():
				for action in self.max_runtime_actions:
					pass  # todo: No known mechanism to stop long running threads; required for force stop feature
	def on_stop_worker(self, worker, start_time):
		''' handle owner start event '''
		self.stopped_count += 1
		if not self.max_concurrency and self.can_repeat():
			remaining_interval = self.interval - (datetime.now() - start_time).total_seconds()
			self.owner.queue_next_worker(remaining_interval)

class RepeatTimer:
	''' what developer does not expect have the basic capability of triggering a timer more than once? '''
	__nonzero__ = __bool__ = lambda self: self.active
	_sys_trace_func = sys.gettrace()
	def __init__(self, repeat, task, pa=(), ka=map0, deamon=False, timer_cls=ExcTimer):
		self.task = task
		self.pa = pa
		self.ka = ka
		self.interval = repeat
		self.timer_cls = timer_cls
		self.repeat_policy = RepeatPolicy.from_repr(repeat)
		self.active = False
		self.is_daemon = deamon
		self.timer = None
		self.count = 0
		self.workers = odict()
		self.timer_name = 'RepeatTimer'
		self.worker_name_fmt = 'RepeatWorker-{count}'

		# update system trace function for all threads if it gets modified
		if RepeatTimer._sys_trace_func != sys.gettrace():
			RepeatTimer._sys_trace_func = temp_trace_func = sys.gettrace()
			threading.settrace(temp_trace_func)
	def reconfigure(self, *pa, **ka):
		''' change RepeatTimer parameters during its runtime '''
		RepeatTimerDynamicConfig.__init__(self.repeat_policy, *pa, **ka)
	@staticmethod
	def start_new(interval, task, pa=(), ka=map0, now=True, deamon=False):
		''' static factory equivalent of RepeatTimer().start() '''
		result = RepeatTimer(interval, task, pa, ka, deamon)
		result.start(now)
		return result
	def start(self, now=True):
		''' calling methods context; must creat thread and return '''
		self.is_alive = True
		self.repeat_policy.on_start_owner(self)
		self.queue_next_worker(not now and self.interval or 0)
		return self
	def stop(self):
		''' stop triggering any new workers '''
		self.is_alive = False
		return self
	def join(self, timeout):
		''' block until repeat timer is done with creating workers and all existing workers have stopped '''
		while self.is_alive and len(self.workers):
			sleep(.1)
		return True
	def queue_next_worker(self, interval=0):
		''' queue function to create worker after given interval '''
		# print('RepeatTimer.queue_next_worker(%s)' % interval)
		self.timer = self.timer_cls(interval, self._on_interval)
		self.timer.name = self.timer_name
		self.timer.daemon = self.is_daemon
		self.timer.start()
	def _on_interval(self):
		''' function triggered after timer interval elapses; handles worker lifecycle which is created immediately '''
		# exit condition
		if not self.is_alive:
			return

		# start worker thread for RepeatTimer's task
		self.count = self.count + 1
		worker = self.timer_cls(0, self.task, pa=self.pa, ka=self.ka)
		worker.name = self.worker_name_fmt.format(count=self.count)
		worker.daemon = self.is_daemon
		worker.start()

		# delegate worker on_start event behavior to repeat_policy
		start_time = datetime.now()
		self.workers[worker] = start_time
		self.repeat_policy.on_start_worker(worker, start_time)	# queue on_interval()

		# block for worker completion; then delegate worker on_stop event behavior to repeat_policy
		worker.join()
		self.on_worker_finished(worker)
	def on_worker_finished(self, worker):
		''' remove worker from index of active workers '''
		start_time = self.workers.pop(worker)
		exc = worker.__dict__.get('exc')
		if exc and self.repeat_policy.exc_handler_repeat:
			self.repeat_policy.exc_handler_repeat(exc)
		self.repeat_policy.on_stop_worker(worker, start_time)
#endregion
