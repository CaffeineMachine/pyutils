from __future__ import annotations
import asyncio
from time import sleep
from dataclasses import dataclass

from pyutils.defs.typedefs import *
from pyutils.defs.worddefs import *
from pyutils.utilities.iterutils import *
from pyutils.utilities.pathutils import deltas_of_paths
from pyutils.patterns.accessors import Has
from pyutils.patterns.event import *
from pyutils.patterns.props import *
from pyutils.structures.nums import *
from pyutils.time.timedefs import *
from pyutils.time.timestamp import *
from pyutils.time.timemetric import *
from pyutils.time.thread import *

#region time defs
@clscolls(no)
class DefsTimes:
	#region consts
	sep_t					= ' ,'
	sep_tpfx				= '|'
	sep_ttarg				= ':'
	sep_taffix				= '+'
	sep_ymh_hms				= ' tT-/;'  # dateutil.parser accepted delim chars between date & time
	#endregion
	#region namespaces
	class Args:
		irate				= Vars(
			vals			= always,
			rate			= .1, )
		
	AA						= Args
	#endregion
	...

DDTT = _D					= DefsTimes
AATT						= DDTT.AA
_i							= _D.i
#endregion

#region time forms
_t_of						= t_of  # alias for original time converter

def t_of(t:T=None,*pa,**ka) ->T:
	t_out = (
		TT(t).start				if isstr(t) and ':' in t else
		_t_of(t,*pa,**ka) )
	return t_out

def td_of(*pa,**ka) ->TD:
	''' coerce *arg* to a timedelta (aka: TD) '''
	t = None if ka or len(pa) != 1 else pa[0]
	td = (
		td0						if isnone(t) else  # todo: change to a functional conversion
		t						if istd(t) else
		TU(t).base_view			if isstr(t) else  # todo: change to a functional conversion
		t.td					if istt(t) else  # todo: change to a functional conversion
		TD(*pa, **ka) )
	return td

def td_of_tt(tstart,tstop=None,fab=TUU):
	tstart,tstop = t_of(tstart), t_of(tstop)
	td = tstop - tstart
	td = td if not fab else fab(td)
	return td

def tdu_of(t) ->TU:
	''' coerce *arg* to TimeUnit (aka: TU) '''
	# todo: eliminate
	# td = t if typeof(t, TU) else TU(t)
	tu = istu(t,orfab=TU)
	return tu

def ttd_of_tt(tt:TT, tstart:T=None, td:TD=td0, to_td=None, fab=None):
	''' converts times to set of timedeltas ->TTD  todo: is this needed '''
	tstart = tstart or tt.start
	ttd = [
		None if tt.start is None else tt.start-tstart+td,
		None if tt.stop is None else tt.stop-tstart+td,
		tt.step, ]
	ttd = ttd if not to_td else [*map(to_td, ttd)]
	ttd = ttd if not fab else fab(ttd)
	return ttd

def mod_t(tstep:TD=None, tofs=None, tt:TT=None, tn_of=tn_of, fab=None, t=None):
	''' get or prepare modulus of given *t*
		:param tstep: denominator of time modulus or tick
		:param tofs:  adjust given *t* by this offset before mod eval
		:param tt:    alternate source of *tstep* & *tofs*
		:param tn_of: timestamp converter
		:param fab:   output converter
		:param t:     numerator of time modulus
	'''
	# resolve inputs
	tstep,tofs = tstep or tt.step, tofs or tt and tt.start,
	tn_step,tn_ofs = tn_of(tstep), tofs and tn_of(tofs) or 0
	
	# resolve output
	def mod_of_t(t):
		tn_in = tn_of(t)
		tn_mod = (tn_in - tn_ofs) % tn_step
		tn_mod = tn_mod if not fab else fab(tn_mod)
		return tn_mod
	tmod = t is None and mod_of_t or mod_of_t(t)
	return tmod

def _idx_of_tt(tt:TT,idx:T|TT|int,fill=miss):
	idx_tt = (
		fill					if not tt or not tt.start else
		idx						if isidx(idx) else
		slice					if istt(idx) else
		(idx-tt.start)/tt.step	if ist(idx) else
		idx.__index__()			if Has.__index__(idx) else
		idx.__int__()			if Has.__int__(idx) else
		fill					if fill is not miss else
		throw(idx) )
	
	if idx_tt is slice:
		idx_tt = slice(
			int((idx.start-tt.start)/tt.step),
			int((idx.stop-tt.start)/tt.step), )
	return idx_tt

def tt_of_str(stt:str, sep_tpfx=_D.sep_tpfx, sep_ttarg=_D.sep_ttarg, base_pfx='', base_sfx='', to_tt=yes, fill=miss) ->TT:
	''' Usage: interprets a str like: '2020|0730:0806:1m' as TT() '''
	try:
		stt_out = '{}{}{}'.format(base_pfx or '', stt, base_sfx or '')
		*_, t_pfx, st_arg, t_sfx  = '', *stt_out.split(sep_tpfx), ''
		t_start, t_stop, t_step, *_ = *st_arg.split(sep_ttarg), None, None  #, nothing, nothing
		tt_args = [t_pfx+t_start+t_sfx, (t_pfx+(t_stop or '')+t_sfx) or None, t_step]
		tt = tt_args if not to_tt else isyes(to_tt,put=TT)(*tt_args)
		return tt
	except Exception:
		if fill != miss:
			return fill
		raise
def itt_of_str(s_itt:str, sep_tpfx=_D.sep_tpfx, sep_ttarg=_D.sep_ttarg, sep_taffix=_D.sep_taffix, fab=list,**ka) ->TT:
	''' Usage: interprets a str like: '2020+  02|01:06  05|01:06  +:1h' as [TT[200201:200206:'1h'], TT[200501:200506:'1h']] '''
	_s_tt = s_itt.split(sep_taffix)
	ka['base_pfx'], _s_tt, ka['base_sfx'] = (
		(None, *_s_tt, None)	if len(_s_tt) == 1 else
		(*_s_tt, None)			if len(_s_tt) == 2 else
		_s_tt  )
	i_s_tt = _i(strs(_s_tt))
	itt = icallpas(tt_of_str, i_s_tt, sep_tpfx=sep_tpfx, sep_ttarg=sep_ttarg, **ka)
	itt = itt if not fab else fab(itt)
	return itt

def istrtimerange_strs(kvs=None, sep_k_vs=':', sep_kv_kv=',', sep_vs=' ', sep_range='-'):
	''' obsolete '''
	if isstr(kvs):
		kvs = kvs.split(sep_kv_kv)
	elif isdict(kvs):
		kvs = sorted(kvs.items())
		
	for kv in kvs:
		k, vs = kv.split(sep_k_vs)
		for v in vs.split(sep_vs):
			kv_range = sep_range.join((k+arg for arg in v.split(sep_range)))
			yield kv_range

def t_strtime(s_time:str, fmt:str) ->T:
	t = T.strptime(s_time, fmt)
	return t
def it_strtime(idateclocks=None, idateclock=None, dates=None, clocks=None):
	raise NotImplemented()

def tt_strtimerange(str_timeset:str, step, fmt=None) ->TT:
	''' obsolete '''
	t_start, t_stop, *_ = *icallargs(t_strtime, str_timeset.split('-'), ka=dict(fmt=fmt)), None
	t_stop = t_stop or t_start
	tt = TT(start=t_start, stop=t_stop, step=step)
	return tt
def itt_timeranges(idateclocks, idateclock=None, dates=None, clocks=None, step=TD(seconds=60), fmt=None, **sep_ka) ->[TT]:
	''' obsolete '''
	if not idateclocks: return
	elif idateclocks:
		istrtimerange = istrtimerange_strs(idateclocks, **sep_ka)
	# elif idateclock:
	# 	istrtimerange = '(sep_seq.join((pfx+sfx for sfx in sfxs.split(sep_seq))) for pfx, sfxs in idateclock)'
	else:
		raise ValueError()
	
	for str_timerange in istrtimerange:
		tt = tt_strtimerange(str_timerange, step=step, fmt=fmt)
		yield tt

def innmiss_of_nseq(n_seq, n_step=None, n_start=None, n_stop=None, fab_nn:callable=Nums):
	''' get all TT time ranges of existing ohlc entries for a set of paths '''
	# todo: dont convert tid to t until later
	# todo: generalize this as a util which gets all ITT for a sequence of times
	i_n_seq = iter(n_seq)
	n = next(i_n_seq, nothing)
	n_next = n_start if n is nothing else (n if n_start is None else n_start) + n_step
	
	if n_start is not None and n != n_start:
		nn = fab_nn[n_start:n:n_step]
		yield nn
		
	for n in i_n_seq:
		if n_next != n:
			nn = fab_nn[n_next:n:n_step]
			yield nn
		n_next = n + n_step
		
	if n_stop is not None and n_next != n_stop:
		nn = fab_nn[n_next:n_stop:n_step]
		yield nn

def inmiss_of_nseq(n_seq, n_inds):
	''' get all TT time ranges of existing ohlc entries for a set of paths '''
	# todo: dont convert tid to t until later
	# todo: generalize this as a util which gets all ITT for a sequence of times
	# inn = innmiss_of_nseq(n_seq, *pa, **ka)
	# for nn in inn:
	# 	yield from nn.iter
	n_next = next(n_seq)
	for n_ind in n_inds:
		if n_ind == n_next:
			n_next = next(n_seq)
		else:
			yield n_ind

def time_args_of(tt:TT,start=None,stop=None,step=None):
	time_args = (
		tt.pa[:2]			if istimes(tt) else
		map(T,tt.pa)		if isnums(tt) else
		tt					if istime(tt) else
		T(tt)				if isnum(tt) else
		map(T,tt)			if isiter(tt) else
		throw(tt) )
	return time_args
def timestamp_args_of(tt:TT,start=None,stop=None,step=None):
	time_args = (
		tt.tn.pa[:2]		if istimes(tt) else
		tt.pa				if isnums(tt) else
		tt.tn				if istime(tt) else
		tt					if isnum(tt) else
		# (t and tn_of(t) for t in tt)		if isiter(tt) else
		[t and tn_of(t) for t in tt]		if isiter(tt) else
		throw(tt) )
	return time_args
def _vec_of_tt(tt):
	start,*_,step = (
		tt.pa[:3]			if istimes(tt) else
		tt.pa[:3]			if isnums(tt) else
		tt					if isiter(tt) else
		throw(tt) )
	start = T(start)
	step = step and td_of(step)
	return start,step
def _vec_of_ttn(ttn):
	start,*_,step = (
		ttn.ttn.pa[:3]		if istimes(ttn) else
		ttn.pa[:3]			if isnums(ttn) else
		ttn					if isiter(ttn) else
		throw(ttn) )
	start = TN(start)
	step = step and tn_of(step)
	return start,step

reduce_into_times			= redef(reduce_into, to_vec=_vec_of_tt,  to_val=time_args_of)
reduce_into_timestamps		= redef(reduce_into, to_vec=_vec_of_ttn, to_val=timestamp_args_of)
rdx_tt,rdx_ttn				= reduce_into_times,reduce_into_timestamps

tt_of_pre					= lambda tt_pre,t: TT(t, t+tt_pre.delta, tt_pre.td)
len_of_tt					= len_of_nn
slice_nums					= slice_nums
ok_t = is_tick				= redef(mod_t, fab=neg)
#endregion

#region time utils
def itime(step:TD, start:T, stop:T=None, evs=None):
	if evs:
		yield from map(evs,itime(step, start, stop))
		return
	
	# gen each time
	step = isinstance(step, int|float) and TD(seconds=step) or step
	t = start
	while not stop or stop > t:
		yield t
		t += step

def time_on(t_of=T.now,args=True,ka_t=map0):
	def time_on_call(*args):	return t_of(**ka_t)
	def time_on_args(*args):	return (*args,t_of(**ka_t))
	
	result = (
		time_on_call		if args is False else
		time_on_call()		if args is None else
		time_on_args		if args is True else
		time_on_args(*args) )
	return result

def iter_rate(vals=always,rate=.1,sleep=sleep,to_val=None,is_next=bool):
	''' iterate given *vals*
		:param vals:
		:param rate: when vals is float|int then overwrite rate and reset vals back to default
		:param sleep:
		:param to_val:
		:param is_next:
	'''
	# todo: sleep should be ATMOST rate and not ALWAYS rate
	# todo: timeout
	if to_val:
		yield from map(to_val,iter_rate(vals=vals,rate=rate,sleep=sleep))
		return
		
	# resolve inputs
	vals,rate = istype(vals,(int,float)) and (iter_rate.__defaults__[0],vals) or (vals,rate)
	ival = isiter(vals) and iter(vals).__next__ or vals
	
	# resolve output
	try:
		val = ival()
		has_next = is_next(val)
		while has_next:
			yield val
			val = ival()
			has_next = is_next(val)
			has_next and sleep and sleep(rate)
		# if timeout and timeout <= td:
		# 	err and throw(TimeoutError())
	except StopIteration: pass

def wait_until(t_until:T, td_real:TD=None, t_real:T=None, break_cond=None, tsec_step:float=.01):
	''' ... '''
	assert None in (t_real, td_real), 'Mutually exclusive args given'
	# resolve inputs
	# td_real = tdreal_t(t_real)
	td_real = (t_real and tdreal_t(t_real)) or td_real
	tsec_step = break_cond and tsec_step or TD.max.total_seconds()
	
	# proceed
	while True:
		# t_real = td_real and td_real+get_t_real() or get_t_real()
		t_real = t_tdreal(td_real)
		if t_real >= t_until or break_cond and break_cond(): break
		
		tsec_step = min(tsec_step, (t_until-t_real).total_seconds())
		sleep(tsec_step)
	return t_until
async def await_until(t_until:T, td_real:TD=None, break_cond=None, tsec_step:float=.01):
	tsec_step = break_cond and tsec_step or timedelta.max.total_seconds()
	while True:
		t_now = td_real and td_real+get_t_real() or T.now()
		if t_now >= t_until or break_cond and break_cond(): break
		
		tsec_step = min(tsec_step, (t_until - t_now).total_seconds())
		await asyncio.sleep(tsec_step)
	return t_until

def t_tdreal(td_real=None, *, t_real=None, **ka):
	t_real = t_real or get_t_real(**ka)
	t = t_real + (td_real or td0)
	return t
def tdreal_t(t=None, *, t_real=None, **ka):
	if not t: return TD()
	t_real = t_real or get_t_real(**ka)
	td_real = t - t_real
	return td_real

def on_delta_of(delta_poll,rate=None,until=bool,state=True,kargs=None,**_kargs):
	# resolve inputs
	irater = (
		irate()			if isnone(rate) else
		irate(rate) )
	use_state,state = isbool(state) and (state,None) or (True,state)
	has_kargs,kargs = bool(_kargs or kargs is not None), kargs or _kargs
	
	# resolve output
	def on_poll(*pa,state=state,**ka):
		ka_poll = {**ka,**kargs}
		ka_poll = use_state and dict(ka_poll,state=state or {}) or ka_poll
		for now in irater:
			is_delta = delta_poll(*pa,**ka_poll)
			print(is_delta)
			if use_state:
				is_delta,ka['state'] = is_delta
			yield is_delta, now
	def on_delta(*pa,until=until,**ka):
		delta = keepfirst(on_poll(*pa,**ka),until,at=0)
		return delta
	result = until and on_delta or on_poll
	result = result if not has_kargs else result()
	return result

def timed_vals(gauges:TD, vals=miss, since=T.min, eq=yes, fill=miss, fab=None):
	''' select index of *vals* corresponding to first in *gauges* timedeltas smaller than time *since* last usage.
		if gauges[n] is 2 then val[n] will never be return more often than once ever 2 seconds  
		:param gauges: timedeltas as rate limit for each vals. must have ascending order, gauge means backoff
		:param vals:   vals limited by given gauges timedeltas
		:param since:  initial time since
		:param eq:     get first gauge where timedelta-since <= gauge. else use timedelta-since < gauge
		:param fill:   default output when timedelta-since last use is less than all guages
		:param fab:    convert output val
	'''
	# resolve inputs
	since_init,since_b = since,None
	gauges = 0,*collany(gauges)
	vals = vals is miss and gauges or (fill,*collany(vals))
	assert lts(gauges), f'given {gauges=} must be in ascending order'
	
	# resolve output
	def timed_val_of_td(*pa, td:int=None, **ka):
		nonlocal since_b
		now,since_b = T.now(), since_b or since_init
		td_sec = (td if td is not None else now-since_b).total_seconds()
		since_b = now
		for nth,gauge in zip(*map(irev,[range(len(gauges)),gauges])):
			if gauge < td_sec or (eq and gauge==td_sec):
				break
		# todo: nth = first(for nth,gauge in enumerate(gauges))
		val = vals[nth]
		val = nth if not fab and val is not miss else fab(val,*pa,**ka)
		return val
	staged = timed_val_of_td
	return staged

def mutual_times(ttt:ITT|TT, *_ttt)->TT:
	# resolve inputs
	ttt = (_ttt or istt(ttt)) and (ttt,*_ttt) or ttt
	
	# resolve output
	tt_out,*ttt = ttt
	for tt in ttt:
		tt_out &= tt
	return tt_out
def union_times(ttt:ITT|TT, *_ttt)->TT:
	# resolve inputs
	ttt = (_ttt or istt(ttt)) and (ttt,*_ttt) or ttt
	
	# resolve output
	tt_out,*ttt = ttt
	for tt in ttt:
		tt_out |= tt
	return tt_out

def group_times(tt:ITT|TT, most=30, step_in=None, step_to=None, to_lo=None, to_hi=1, limit=10000, **ka):
	tt = TT(tt_in:=tt).update(step=step_in)
	assert not limit or not (limit <= len(tt)), f'{limit=} <= len({tt})={len(tt)}'
	
	tt_grps = slices(most, vals=tt, ev=list)
	ttt = cxpas(bt_of, tt_grps, to_lo=to_lo,to_hi=to_hi and to_hi*tt.step)  # fixme: finding min/max is excessive
	ttt = ttt if step_to is None else [tt.update(step=step_to) for tt in ttt]
	return ttt

grp_tt						= group_times
get_t_real					= T.now
nth_times,slice_times		= nth_nums,slice_nums
nth_tt,tt_at				= nth_nums,slice_nums
timejoin_on					= time_on(args=True)
tidjoin_on					= time_on(args=True,t_of=tid_of)
stjoin_on					= time_on(args=True,t_of=st_of)
tj_on,tidj_on,stj_on		= timejoin_on,tidjoin_on,stjoin_on
irate						= iter_rate
irate_t						= redef(iter_rate,to_val=timejoin_on)
irate_tid					= redef(iter_rate,to_val=tidjoin_on)
irate_st					= redef(iter_rate,to_val=stjoin_on)
poll_delta_paths			= on_delta_of(deltas_of_paths,rate=1,until=None)
poll_until_delta_paths		= on_delta_of(deltas_of_paths,rate=1)
timed_calls					= redef(timed_vals, fab=lambda call,*pa,**ka: call and call(*pa,**ka))
mutual_tt,union_tt			= mutual_times, union_times
#endregion

#region time devcs
@dataclass
class Ticker:
	pace: TD				= TD(seconds=1)
	t_next					= property(lambda self: self.pace + self.t_last)
	#region utils
	def __post_init__(self):
		self.ind = 0
		self.t_last = None
	def start(self, clock=False, ind=1):
		self.t_last = T.now()
		if clock:
			self.t_last = prev_tick(self.pace, self.t_last)
		return self
	def tick(self):
		T.now() < self.t_last and wait_until(self.t_last)
		
		result = self.ind, self.t_last
		self.ind, self.t_last = self.ind+1, self.t_next
		return result
	#endregion
	...

@dataclass
class TimeTrace:
	forwards: int			= None
	t_ind: int				= 0  # age, elapse, passed, once, timer, timing, after
	pace: TD				= None
	callback:iscall			= None
	msg: str				= None
	#region utils
	def __post_init__(self):
		self.pace = self.pace if typeof(self.pace, (TD,type(None))) else TD(seconds=self.pace)
	def activate(self, tseq, ticker):
		# ticker.pace = isnoneput(self.pace, ticker.pace)
		ticker.pace = isnone(self.pace, put=ticker.pace)
		self.forwards is not None and tseq.forward(self.forwards)
		self.callback and self.callback()
		self.msg and print(self.msg)
	#endregion
	...

class TimesArgs(Nums, to_num=tid_of):
	#region defs
	_kk						= tss('start  stop  step  evs')
	td						= property(lambda self: self.stop - self.start)
	tdu						= property(lambda self: TU(self.stop - self.start))
	tsecs					= property(lambda self: int((self.stop - self.start).total_seconds()))
	tn						= property(lambda self: NN(self.a.tn, self.b.tn, self.c and tn_of(self.c), *self.args[3:]))
	i						= property(lambda self: itime(step=self.step, start=self.start, stop=self.stop))
	zero					= lazy(lambda cls: cls(T.zero,T.zero,td0))
	_all					= lazy(lambda cls: cls(T.zero,T.max,td0))
	_miss					= None
	#endregion
	#region forms
	def __new__(cls, start:T=NN._miss, stop:T=Nums._miss, step:TD=None, *pa, **ka):
		not_empty = bool(start or stop or step or pa or ka)
		obj = super().__new__(cls) if not_empty or cls.zero is miss else cls.zero
		return obj
	def __init__(self, start:[T,str]=NN._miss, stop:T=Nums._miss, step:TD=None, *pa, evs=None, **ka):
		''' 
			:param step:  
			:param start: 
			:param stop:  
			:param evs:   converter of each time step in iterate
		'''
		super().__init__(start, stop, step, *pa, **ka)
		self.parse(stop=stop, step=step)
		self.step = self.step if isntnone(self.step) else TM.sec
		self.evs = evs and exact(evs)
		...
	def __bool__(self):		return any(self.args[:2])
	def rep(self, *fmt_pa, fmtc=TFmtcs.ymd_hms):
		if self == self._all:	return f'*'
		start = self.start and format(self.start, fmtc)
		stop = self.stop and format(self.stop, fmtc)
		step = self.step
		out = str_range(start, stop, step)
		out = re.sub(r'-?(00)+(?=:)', '', out)  # remove least significant 0s on start and stop
		return out
	def reset(self, step:TD=miss, start:T=miss, stop:T=miss, t_real:T=miss, td_real:TD=miss, t_frame=miss, **ka):
		t_frame != nothing and self.clock.reset(t_frame)
		ka.update(locals())
		for key, value in ka.items():
			if value != miss and key not in ('self','ka'):
				setattr(self, key, value)
	def parse(self, stop=miss, step=miss):
		self.start, self.stop, self.step, *_ = (
			(*self.start,step)					if istuple(self.start) else
			self.start.pa						if isnums(self.start) else
			tt_of_str(self.start,to_tt=None)	if isstr(self.start) else
			(self.start,self.stop,self.step) )
		self.start = isnone(self.start, orfab=t_of)  # fixme: dont reporocess a time
		self.stop = isnone(stop or self.stop, orfab=t_of)
		self.step = isnone(step or self.step, orfab=tdu_of)
	@classmethod
	def every(cls, *, td=None):
		''' get an unbounded time span. all unbounded time ranges can differ by a given *td*|timedelta '''
		_td,td = td,istt(td,var='step',orfab=td_of)
		tt = TimesArgs(T.zero,T.max,td)
		return tt
	__repr__=__format__		= rep
	#endregion
	#region utils
	def iterate(self):		return itime(step=self.step, start=self.start, stop=self.stop, evs=self.evs)
	def finish(self):		self.tseq.stop = T.min
	def gen_ticker(self):	pass  # todo
	def fab_tseq(self, clock=False, **ka):
		if clock:
			ka['start'] = prev_tick(self.step, ka.get('start', self.start))
		return TSeq(**{**dict(stop=self.stop, step=self.step), **ka})
	def traverse(self, *ttrace_pai, ttraces=(), clock=True, **ttrace_kai):
		''' map running clock time to a timeline '''
		if ttrace_pai or ttrace_kai:
			is_vector = any(filter(isiter, [*ttrace_pai, *ttrace_kai.values()]))
			if is_vector:
				ttraces = [TimeTrace(*pa, **ka) for pa, ka in iargs_argsi(ttrace_pai,ttrace_kai)]
			else:
				ttraces = [TimeTrace(*ttrace_pai, **ttrace_kai)]
		ttrace, *ttraces = *ttraces, None
			
		# begin times series iteration
		ticker = Ticker(self.step).start(clock=clock)
		self.tseq = self.fab_tseq(start=self.start or ticker.t_last, clock=clock)
		while not self.tseq.stopped:
			while ttrace and ttrace.t_ind <= self.tseq.ind_last:  # todo: can be encapsulated into tseq
				ttrace.activate(self.tseq, ticker)
				ttrace, *ttraces = *ttraces, None
			
			# realtime procession over generated timeline
			# ticker.tick() # wait configured amount of time
			yield from self.tseq.sync()
	def to_local(self, timezone=DefsTime.tz_default):
		start, stop = (timezone.localize(t.replace(tzinfo=None)) for t in (self.start,self.stop))
		tt = Times(start, stop, self.step)
		return tt
	def mutual(self, val, step=None) -> TT:
		_pas = starts,stops,steps,*_ = tzip(self.pa, val.pa)  # todo: step selector
		out = clsof(self)(max(starts),min(stops),min(steps))
		return out
	def union(self, val, step=None) -> TT:
		_pas = starts,stops,steps,*_ = tzip(self.pa, val.pa)  # todo: step selector
		out = clsof(self)(min(starts),max(stops),min(steps))
		return out
	
	__and__ = intersection	= mutual
	__or__					= union
	#endregion
	...
class Times(TimesArgs, to_num=tid_of):
	#region defs
	__iter__				= TimesArgs.iterate
	__len__					= len_of_tt
	zero					= lazy(lambda cls: cls(T.zero,T.zero,td0))
	_all					= lazy(lambda cls: cls(T.zero,T.max,td0))
	#endregion
	#region coll
	def __getitem__(self,idx):
		assert self.step, f'Times with zero step size are not indexable {tss(self.pa)}'
		out = slice_times(idx,*self.pa,fab=isntint(idx) and clsof(self))
		return out
	#endregion
	...
class ITime(Times):
	#region forms
	def __i_nit__(self, step:TD|str=None, start:T=miss, stop:T=miss, *pa, step_last=no, **ka):
		'''
			:param step:  
			:param start: 
			:param stop:  
			:param evs:   converter of each time step in iterate
		'''
		pa = start is stop is miss and [step] or [start,stop,step]
		super().__init__(*pa, **ka)
	def __init__(self, *pa, step_last=no, **ka):
		'''
			:param step:  
			:param start: 
			:param stop:  
			:param evs:   converter of each time step in iterate
		'''
		# pa = start is stop is miss and [step] or [start,stop,step]
		ka = mapi(step_last and 'start stop step' or 'step start stop', pa) | ka
		super().__init__(**ka)
	@classmethod
	def of(cls,*pa,**ka):		return super().of(*pa,**ka,step_last=yes)
	def per_tt(self,ev=None):
		raise NotImplementedError()
	#endregion
	...
class ITimes(ITime):
	_evs				= tt_of_pre
	def __init__(self, tts=miss, step:TT|TD|str=None, start:T=miss, stop:T=miss, *, vals=miss, evs=_evs,**_ka):
		'''
			:param step:  
			:param start: 
			:param stop:  
			:param tts:   vals precursor for each step initially coerced into tt|times
			:param vals:  vals precursor for each step
			:param evs:   converter of each time step in iterate
		'''
		# resolve inputs
		self.vals = vals if vals is not miss else TT(ismiss(tts,fab=throw))
		start,stop = [start,stop] if not istype(step,TD|TU) else [self.vals.start,self.vals.start+step]
		# evs = self.vals and evs.__get__(self.vals) or evs
		evs_out = self.vals and (lambda *pa,**ka: evs(self.vals, *pa,**ka)) or evs
		# evs = not self.vals and evs or (lambda *pa,vv=self.vals,evs=evs,**ka: evs(vv,*pa,**ka))
		
		# resolve output
		super().__init__(step, start, stop, evs=evs_out)
	#endregion
	...
class TSeq(Times):
	#region defs
	t_last					= property(lambda self: self.ind_last*self.step + self.start)
	is_realtime				= property(lambda self: self.ind_last == self.ind)
	stopped					= property(lambda self: self.stop and self.t_last >= self.stop)
	#endregion
	#region utils
	def __init__(self, *pa, forwards=0, **ka):
		self.ind = forwards
		self.ind_last = -1
		super().__init__(*pa, **ka)
	def forward(self, forwards=1):
		self.ind += forwards  # ticks, leap, incr, count
		return self
	def sync(self):
		while self.ind_last < self.ind:
			self.ind_last += 1
			if self.stopped: return
			yield self.t_last, self.is_realtime
		# not self.stopped and self.forward()
		self.forward()
	
	__next__				= forward
	__iter__				= sync
	#endregion
	...

class TimesSeq(list):
	#region forms
	zero					= lazy(lambda cls: cls())
	_sjoin					= True
	def __new__(cls, itt=()):
		obj = super().__new__(cls,itt) if itt or cls.zero is miss else cls.zero
		return obj
	def __init__(self, itt=()):
		itt = isstr(itt,fab=itt_of_str)
		self.which_step = keepfirst
		super().__init__(itt)
	@classmethod
	def of(cls,*pa,**ka):
		raise NotImplementedError()
	#endregion
	#region utils
	def has(self,t_in):
		has = (
			all(t in self for t in t_in)		if isiter(t_in) and isnttype(t_in,Nums) else
			any(t_in in tt for tt in self) )
		return has
	def mutual(self,itt) -> ITT:
		''' find time range which all times have in common '''
		# resolve inputs
		itt_in = self + list(
			()			if itt == tt0 else
			[itt]		if istt(itt) else
			itt )
		starts,stops,steps,*_ = *zip(*(tt.args for tt in itt_in)), None, None, None
		
		# resolve output
		tt_out = starts and TT(max(starts),min(stops),min(steps))
		itt_out = ITT(starts and tt_out.start <= tt_out.stop and [tt_out] or ())
		return itt_out
	def union(self,itt) -> ITT:
		''' combine overlapping Times '''
		# resolve inputs
		# itt_in = self + (istt(itt) and [itt] or itt)
		itt_in = self + list(
			()			if itt == tt0 else
			[itt]		if istt(itt) else
			itt )
		itt_in = sorted(itt_in,key=lambda itt: itt.start)
		
		# resolve output
		itt_out,itt_in = ITT(itt_in[:1]), itt_in[1:]
		if itt_in:
			tt_nth, = itt_out
			for tt in itt_in:
				if tt_nth.stop < tt.start:
					tt_nth = TT(tt)
					itt_out.append(tt_nth)
				elif tt_nth.stop < tt.stop:
					tt_nth.stop = tt.stop
					tt_nth.step = self.which_step([tt_nth.step,tt.step])
		return itt_out
	
	__contains__			= has
	__and__ = intersection	= mutual
	__or__					= union
	#endregion
	...

TTAA						= TimesArgs
TT							= Times
PreTimes = PreTT			= Time|Times|nonetype
IT							= ITime
LTT = LTimes				= TimesSeq
ITT							= TimesSeq
istimes=istt=is_tt			= istypethen(TimesArgs)
pretimes=prett				= istypethen(PreTimes)
_consts						= TTAA.zero,TTAA._all  # lazy const assignments
ttaa0,tt0,itt0				= TTAA.zero,TT.zero,ITT.zero
bound_times_of = bt_of		= redef(bound_index_for, step=TM.min, ev=TT)
...
#endregion

#region tests
def test_redux():
	from pyutils.output.joins import PJ
	t0 = t_of(0)
	t6 = t0+TM.hr*6
	
	rdx6a = rdx_tt(t0,TM.hr,t6)
	rdx = rdx_tt(t0,TM.hr)
	rdx6b = rdx(t6)
	PJ.jj_dict_nl(locals())
# test_redux()
# exit(fq_kfuncpln())
#endregion

