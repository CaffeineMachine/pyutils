from __future__ import annotations
from copy import copy
from sortedcontainers import SortedDict

from pyutils.utilities.callutils import *
from pyutils.structures.metric import *
from pyutils.patterns.accessors import KK
from pyutils.time.timestamp import *
from pyutils.time.timestamp import *

#region devcs
# todo: refactor all.  TimeUnit & TimeUnits should inherit from Unit & timedelta

class TDenom(Unit): pass
class TimeUnit(Unit,timedelta,  name='TU'):
	''' A discrete delta time unit and quantity.
		An interpretation of Unit as an amount of change in time.
	'''
	#region defs
	denom_value_factory	= lambda cls, name, value: (TM.denom[name], int(value))
	td					= property(lambda self: self.base_view)
	tdn = tsecs			= property(lambda self: int(self.denom.total_seconds() * self.value))
	base_value=seconds	= property(lambda self: self.denom.total_seconds() * self.value)
	metric				= property(lambda self: globals()[self.metric_name])
	
	days				= property(lambda self: self.base_view.days)
	seconds				= property(lambda self: self.base_view.seconds)
	microseconds		= property(lambda self: self.base_view.microseconds)
	seconds_total		= property(lambda self: (td:=self.base_view) and  td.days*60*60*24 + td.seconds or 0)
	microseconds_total	= property(lambda self: (td:=self.base_view) and (td.days*60*60*24 + td.seconds)*1000 + td.microseconds or 0)
	val,ch				= KK.value, KK.abrv
	#endregion
	#region forms
	def __new__(cls, unit=None, denom=None, value=1, **ka):
		if oftype(unit, timedelta):
			unit = TUU.timedelta_units(unit, **ka)[0]  # use 1st and largest unit
		
		if unit is denom is None:
			denom, value = TM.second, 0
		elif oftype(unit, Unit):
			denom, value = unit.denom, unit.value
		elif oftype(unit, str):
			denom, value = cls.notation_unit(unit, factory=cls.denom_value_factory)
		elif not oftype(unit, nonetype):
			raise ValueError(unit)
		
		base_view = denom.base*value
		obj = timedelta.__new__(cls, base_view.days, base_view.seconds, base_view.microseconds)
		obj.denom,obj.value = denom, value
		return obj
	def __init__(self, *pa, **ka):
		Unit.__init__(self,denom=self.denom, value=self.value)
	def __repr__(self):				return f'{self.format()}'
	def __add__(self, val):
		out = (
			val + self.base_view		if istype(val, DateClock|Date|Clock) else
			copy(self).__iadd__(val) )  # todo: must instead get sum of smallest common demon unit
		return out
	def __sub__(self, val):
		out = (
			-val + self.base_view		if istype(val, DateClock|Date|Clock) else
			copy(self).__isub__(val) )  # todo: must instead get sum of smallest common demon unit
		return out
	__iadd__			= __add__
	#endregion
	...

class TimeUnits(Units,  name='TUU'):
	''' An aggregate of distinct DeltaTimeUnits '''
	#region defs
	_fmt_abrv 			= '{value}{abrv}'
	_fmt_name 			= '{value}{name}s'
	units				= property(lambda self: self._units)
	values				= property(lambda self: self._values.items())
	seconds				= property(lambda self: self.total_tdelta.total_seconds())
	tsecs				= property(lambda self: int(self.total_tdelta.total_seconds()))
	td					= property(lambda self: self.total_tdelta)
	major				= property(lambda self: self._units[-1])
	#endregion
	#region forms
	def __init__(self, units=None, cond=None, **timedelta_kws):
		# coerce accepted inputs
		if  isinstance(units, TUU):
			self.__dict__.update(units.__dict__)
			return
		elif isinstance(units, TU):
			units = [units]
		elif isinstance(units, Denom):
			units = [TU(denom=units)]
		elif isinstance(units, type(None)):
			units = TUU.timedelta_units(timedelta(**timedelta_kws),cond=cond)
		elif isinstance(units, timedelta):
			units = TUU.timedelta_units(units,cond=cond)
		elif isinstance(units, str):
			units = TU.notation_units(units)
		elif not isinstance(units, (list, tuple)):
			raise ValueError(f'{units=!r}, **{timedelta_kws}')
		
		for ind, unit in enumerate(units):
			if not isinstance(unit, TU):
				units[ind] = TU(unit)
		
		# member variable assignment
		units.sort()
		self._units = units
		self._values = SortedDict([(unit.denom, unit.value) for unit in units or []])
		self.total_tdelta = sum([denom.base * int(value) for denom, value in self.values], timedelta())
		self.polarity = 1
	def __iter__(self):		return iter(self.units)
	def total_seconds(self):	return self.total_tdelta.total_seconds()
	def __str__(self):
		result = ' '.join([self._fmt_abrv.format(**unit.attr_dict) for unit in self.units])
		return result
	def __repr__(self):
		result = f'{self.__class__.__name__}({str(self)!r})'
		return result
	def __iadd__(self, other):
		other = self.__class__(other)
		for denom, val in other.values:
			val = self._values.get(denom, 0) + val
			if val:
				self._values[denom] = val
			else:
				self._values.pop(denom)
		return self
	def __add__(self, other):
		result = copy(self)
		if isinstance(other, DateClock|Date|Clock):
			result = self.total_tdelta
		result += other
		return result
	def __lt__(self, other):
		return self.total_tdelta < other.total
	@staticmethod
	def timedelta_units(t_delta, join=None,cond=None,**ka):
		''' takes a timedelta and returns a list of discrete time units.  timedelta(seconds=5000)  =>  [1H. 23M. 20s] '''
		operands_fab = t_delta >= td0 and Accums or Negates
		t_delta = abs(t_delta)
		
		units = []
		for base, abrv in zip(DeltaTime.denoms, DeltaTime.abrvs):
			if not t_delta: 		break
			if t_delta < base.base: continue
			
			count = int(t_delta / base.base)
			unit = Unit(denom=base, value=count)
			add = not cond or cond(unit)
			add and units.append(unit)
			t_delta -= count * base.base
			
		# prepare result: ensure non-empty and apply given join to unit sequence
		units = operands_fab(units or [Unit(Dt.denom.s, 0)])
		units = join(units) if join else units
		return units
	__radd__			= __add__
	#endregion
	...

class TimeMetric(Metric):
	''' Time delta unit names and abbreviations '''
	#region basic defs
	Unit,Units			= TimeUnit,TimeUnits
	notation_ptrn		= r'([+-]?\d*)(\w+)s?'
	fmt_name,fmt_abrv	= '{value}{name}s', '{value}{abrv}'
	cch = abrvs			= tss( 'y      mo      w      d     h      m        s        ms            us           ')
	names = kk			= tss( 'year   month   week   day   hour   minute   second   millisecond   microsecond  ')
	kk_plural			= tss(names,'{}s')
	#endregion
	#region colls
	denoms				= (
		Denom('year', 'y',			timedelta(days=365), 'TimeMetric'),	# todo: cope with variable deltatime quantities
		Denom('month', 'mo',		timedelta(days=31), 'TimeMetric'),	# todo: cope with variable deltatime quantities
		Denom('week', 'w',			timedelta(days=7), 'TimeMetric'),
		Denom('day', 'd',			timedelta(days=1), 'TimeMetric'),
		Denom('hour', 'h',			timedelta(hours=1), 'TimeMetric'),
		Denom('minute', 'm',		timedelta(minutes=1), 'TimeMetric'),
		Denom('second', 's',		timedelta(seconds=1), 'TimeMetric'),
		Denom('millisecond', 'ms',	timedelta(milliseconds=1), 'TimeMetric'),
		Denom('microsecond', 'us',	timedelta(microseconds=1), 'TimeMetric'), )
	denoms_addt			= (
		Denom('leapyear', 'ly',		timedelta(days=366), 'TimeMetric'), )
	units				= (
								y,     _,      w,     d,    h,     m,       s,       ms,           us,           ) = (
								yr,    mo,     wk,    _,    hr,    min,     sec,     msec,         usec,         ) = (
								year,  month,  week,  day,  hour,  minute,  second,  millisecond,  microsecond,  ) = (
								years, months, weeks, days, hours, minutes, seconds, milliseconds, microseconds, ) = (
							cxkas(TimeUnit, denom=denoms) )
						# ) = (*(TimeUnit(denom=denom) for denom in denoms),  )
	name				= Vars({ k: v.name	for k, v in zip(kk+kk_plural+cch, denoms*3)})	# usage: TM.name.year == 'year'
	abrv				= Vars({ k: v.abrv	for k, v in zip(kk+kk_plural+cch, denoms*3)})	# usage: TM.abrv.month == 'm'
	base				= Vars({ k: v.base	for k, v in zip(kk+kk_plural+cch, denoms*3)})	# usage: TM.base.day == timedelta(day=1)
	denom				= Vars({ k: v 		for k, v in zip(kk+kk_plural+cch, denoms*3)})	# usage: TM.denom.day == Denom('day', ...)
	unit				= Vars({ k: v 		for k, v in zip(kk+kk_plural+cch,  units*3)})	# usage: TM.unit.day == Unit('day', ...)
	#endregion
	#region utils
	@staticmethod
	def td_of(obj:str|TD|TU|TUU) ->TD:
		''' return a timedelta coerced from possible input types: str, DeltaTimeUnit, DeltaTimeUnits, timedelta '''
		if isinstance(obj, TD):
			return obj
		elif isinstance(obj, TU):
			result = obj.base_view
		elif isinstance(obj, TUU):
			result = obj.total_tdelta
		elif isinstance(obj, str):
			result = TU(obj).base_view
		else:
			raise TypeError(str(obj))
		return result
	
	coerce_timedelta	= td_of
	coerce_deltatime	= td_of
	#endregion
	...

TM,TU,TUU				= TimeMetric, TimeUnit, TimeUnits
istd,istu,istuu			= cxpas(istypethen, (TD,TU,TUU))
tuu_since				= redef(td_since, fab=TUU)
Time.tuu				= property(lambda self: TUU(T.now()-self).major)
TU._metric=TUU._metric	= TimeMetric		# todo: refactor

DeltaTimeUnit			= TimeUnit			# todo: remove formerly named
DeltaTimeUnits			= TimeUnits			# todo: remove formerly named
DeltaTime = Dt			= TimeMetric 		# todo: remove formerly named
#endregion

#region inline test
def test_tm_props():
	from pyutils.output.colorize import redl
	o					= Vars(
		secs			= Vars(
			day			= TM.day.seconds,
			week		= TM.week.seconds, ) )
	ok					= Vars(
		secs			= Vars(
			day			= 60 * 60 * 24,
			week		= 60 * 60 * 24 * 7, ) )
	print(o.secs)
	assert ok.secs == o.secs, redl(f'expected {ok.secs=}')
	
def test_tm_math():
	# test timeunit
	print(TM.w)									# unit shorthand
	print(TM.w*2)								# unit multiplication
	print(DC.min + TM.w)						# unit dateclock arithmetic
	
	# test timeunits
	deltat = TM.mo*1 + TM.d*2 + TM.h*3 + TM.m*4	# advanced unit arithmetic
	deltat_tmin = DC.min + deltat				# dateclock arithmetic like timedelta
	print(f'{"deltat":14s} = {deltat!r:34s} <= TM.mo*1 + TM.d*2 + TM.h*3 + TM.m*4')  # FIXME: units listed in reverse
	print(f'{"tmin + deltat":14s} = {deltat_tmin!r:34s} <= {deltat_tmin}')  # FIXME: didnt add hours or minutes
	
def test_tm_rep():
	str_input = '3m 4y'
	timedelta_input = TD(seconds=5000)
	
	str_dt = TU(str_input)
	str_dts = TUU(str_input)
	td_dts = TUU(timedelta_input)
	
	print(f'{str_input!r:32s} => str: {str_dt!s:10s} repr: {str_dt!r}')
	print(f'{str_input!r:32s} => str: {str_dts!s:10s} repr: {str_dts!r}')
	print(f'{timedelta_input!r:32s} => str: {td_dts!s:10s} repr: {td_dts!r}')
	
def test_tm():
	test_tm_props()
	test_tm_math()
	test_tm_rep()
	
	
if __name__=='__main__':
	test_tm()
#endregion


