from __future__ import annotations
import re
import time
import dateutil
from dataclasses import dataclass
from datetime import datetime, date, time, timedelta, timezone
from dateutil import parser

from pyutils.defs.itemdefs import first
from pyutils.defs.typedefs import *
from pyutils.defs.funcdefs import throw
from pyutils.utilities.callutils import tss

#region time defs
try:
	import pytz as _pytz
except:
	_pytz = None

Clock						= time
Date						= date
TimePy = TPy=DateClock=DC	= datetime
DateClockDelta = DCD		= timedelta
TimeDeltaDPy = TDPy			= timedelta
clock0						= TPy.min.time()

def _str_time(fmtc, str_t):
	t = T.strptime(str_t, fmtc)
	return t

class DefsTime:
	# weekday str defines
	weekday_abrvs			= tss('Sun     Mon     Tue      Wed        Thu       Fri     Sat       ')
	weekday_names			= tss('Sunday  Monday  Tuesday  Wednesday  Thursday  Friday  Saturday  ')
	dc_args					= tss('year  month  day  hour  minute  second  microsecond  tzinfo     ')
	dcd_args				= tss('days  seconds  microseconds  ')
	
	# unix timestamp defines
	epoch_naive,epoch		= TPy(1970,1,1), TPy(1970,1,1, tzinfo=timezone.utc),   # unix timestamp epoch; a thursday
	first_epoch_days		= [epoch + TDPy(days=ind) for ind, epoch in enumerate([epoch]*7)]		#  0:thu -> 6:wed
	_thu_day_abrvs			= [wk_day.lower() for wk_day in weekday_abrvs[-3:]+weekday_abrvs[:4]]	# -3:thu -> 3:wed
	_thu_day_names			= [wk_day.lower() for wk_day in weekday_names[-3:]+weekday_names[:4]]	# -3:thu -> 3:wed
	first_epoch_day_map		= dict(zip(_thu_day_abrvs+_thu_day_names, first_epoch_days*2))
	tz_default				= _pytz and _pytz.timezone('US/Central')  # todo: detect local timezone

class TFmtcs:
	''' fmtc as in C style time formats.  ie: '%Y' '''
	# standard formats
	weekdayid				= '%A'	# e.g. Sunday, Monday, ... Saturday
	weekdayid_abrv			= '%a'	# e.g. Sun, Mon, ... Sat
	monthid					= '%B'	# e.g. January, February, ... December
	monthid_abrv			= '%b'	# e.g. Jan, Feb, ... Dec
	
	weekday					= '%w'	# e.g. 0, 1, ... 6
	locale_dateclock		= '%c'  # e.g. "Tue Aug 16 21:30:00 1988" (en_US); "Di 16 Aug 21:30:00 1988" (de_DE);
	locale_date				= '%x'	# e.g. "08/16/88" (None); "08/16/1988" (en_US); "16.08.1988" (de_DE);
	locale_clock			= '%X'	# e.g. "21:30:00" (en_US & de_DE)
	microsecond				= '%f'	# e.g. 000000, 000001, ... 999999
	week_sunday				= '%U'	# week number of the year from Sunday.  e.g. 00, 01, ... 53
	week_monday				= '%W'	# week number of the year from Monday.  e.g. 00, 01, ... 53
	
	year					= '%Y'	# year with century.  e.g. 0001, ... 2013, 2014, ... 9999
	year_abrv				= '%y'	# Year without century.  e.g. 00, 01, ... 99
	month					= '%m'	# e.g. 01, 02, ... 12
	day = day_month			= '%d'
	day_year				= '%j'  # day of the year.  e.g. 001, 002, ... 366
	hour					= '%H'
	minute					= '%M'
	second					= '%S'
	# todo:					= '%Z	# Time zone name (empty string if the object is naive). e.g.: (empty), UTC, EST, CST '''
	date					= '%Y-%m-%d'
	clock					= '%H:%M:%S'
	ymd						= '%y%m%d'
	yymd					= '%Y%m%d'
	hms						= '%H%M%S'
	hm						= '%H%M'
	
	# composite formats
	clock_ms				= '%s.%s'    % (clock,microsecond)
	dateclock_iso			= '%sT%s'    % (date,clock)
	dateclock				= '%s+%s'    % (date,clock)
	dateclock_ms_iso		= '%sT%s.%s' % (date,clock,microsecond)
	dateclock_ms			= '%s+%s.%s' % (date,clock,microsecond)
	ymd_hms					= '%s-%s'    % (ymd,hms)
	ymdhms					= '%s%s'     % (ymd,hms)
	ymd_hm					= '%s-%s'    % (ymd,hm)
	ymdhm					= '%s%s'     % (ymd,hm)
	yymd_hms				= '%s-%s'    % (yymd,hms)
	yymdhms					= '%s%s'     % (yymd,hms)
	
	# aliased formats
	(	wd, mon, yyyy, yy, week,
		ms, # t,
		dc, dc_ms, d, c,
		locale_dc, locale_d, locale_c,
	) = (
		weekdayid_abrv, monthid_abrv, year, year_abrv, week_sunday,
		microsecond, # timestamp,
		dateclock, dateclock_ms, date, clock,
		locale_dateclock, locale_date, locale_clock,
	)
	
	_to_member				= None
	def __init_subclass__(cls, member_of_fmtc=None):
		if not member_of_fmtc: return
		for attr, member in cls.__base__.__dict__.items():
			if attr.startswith('_'): continue
			member = member_of_fmtc(member)
			setattr(cls, attr, member)

class _cmps:
	abrvs					= tss('gt  lt  ge  le  eq  ne  isin  rx')
	symbs					= tss('>   <   >=  <=  ==  !=  ')
	calls					=      gt, lt, ge, le, eq, ne, isin, rx,  = (  # todo: replace with oputils
								lambda a, b: a >  b,
								lambda a, b: a <  b,
								lambda a, b: a >= b,
								lambda a, b: a <= b,
								lambda a, b: a == b,
								lambda a, b: a != b,
								lambda a, b: a in b,
								lambda a, b: re.search(b, a),
	)
	calls_at				= dict(zip(abrvs + symbs, calls*2))

DDT = _D					= DefsTime
#endregion

#region time forms
_parser 					= lambda str_t, fmtc: TPy.strptime(str_t, fmtc)
_fmt_of_fmtc				= lambda fmtc: '{:%s}' % fmtc
_fmtr_of_fmtc				= lambda fmtc: ('{:%s}' % fmtc).format
_parser_of_fmtc				= lambda fmtc: _str_time.__get__(fmtc)

def _cmp_t_rx_eq(): pass
def _tunits_of_tsub(s_t, s_tsub):
	s_trx = re.sub(r'\d', '\d', s_tsub)
	s_tunits, *_ = *re.findall(s_trx, s_t), None
	return s_tunits
def _cmp_t_tsub(cmp, s_t, s_tsub):
	s_trx = re.sub(r'\d', '\d', s_tsub)
	s_tunits, *_ = *re.findall(s_trx, s_t), None
	result = cmp(s_tunits, s_tsub)
	return result

def today():
	return T.combine(Date.today(),clock0)

def t_of_s(t:T=None,yearfirst=True,fab_t=miss,**ka) ->T:  # todo: remove
	out = parser.parse(t, yearfirst=yearfirst,**ka)
	return parser.parse(t, yearfirst=yearfirst,**ka)
def t_of(t:T=None,yearfirst=True, fab_t=True, **ka) ->T:
	''' coerce *arg* to a pyutils..tTme (aka: T, dateclock, builtin.datetime) '''
	# fixme: add awareness of timezone
	# resolve inputs
	ka.update(yearfirst=yearfirst)
	
	# resolve output
	t_out 	= (
		t								if ist(t) else
		T.now()							if t is None else
		T.now() + t						if istd(t) else
		parser.parse(t,**ka)			if isstr(t) else
		T.combine(t,clock0)				if istype(t,Date) else
		T.combine(DDT.epoch.date(),t)	if istype(t,Clock) else
		T.fromtimestamp(float(t))		if istype(t, (int,float)) or iscx(getattr(t,'__int__',0) or getattr(t,'__float__',0)) else
		throw(t) )
	t_out = t_out if not fab_t else (fab_t is True and T.of or fab_t)(t_out)
	return t_out
def st_of(t:T=None, fmtc:str=None) ->str:
	fmtc = fmtc or _D.fmtr.ymd_hms
	t_out = t_of(t)
	s_t = t_out.strftime(fmtc) if isinstance(fmtc, str) else fmtc(t_out)
	return s_t

def td_of(td:TD=None,*pa,**ka) ->TD:
	''' coerce *arg* to a timedelta (aka: TD) '''
	pa = (td,)[td is None:] + pa
	td_out = (
		# TD(*pa,**ka)					if pa or ka else
		td0								if isnone(td) and not ka else
		td								if istd(td) else
		TD.of(td)						if isdcd(td) else
		TD(*pa,**ka) )
	return td_out

def like_t(t:T, tnum, *tnum_pa):
	t_cmp = isinstance(tnum, str) and t_like_rx or t_like_eq
	return t_cmp(t, tnum, *tnum_pa)
def like_t_s(t:T, s_tsub:str, cmp=_cmps.isin, fmtr=None):
	s_t = (fmtr or ':' in s_tsub and _D.fmtr.dateclock_ms_iso or _D.fmtr.yymdhms)(t)
	if not callable(cmp):
		s_t = s_t[-len(s_tsub):]
	cmp = callable(cmp) and cmp or _cmps.calls_at[cmp]
	alike = cmp(s_tsub, s_t)
	return alike
def like_t_rx(t:T, s_trx:str, cmp=_cmps.isin, fmtr=None):
	s_t = (fmtr or ':' in s_trx and _D.fmtr.dateclock_ms_iso or _D.fmtr.yymdhms)(t)
	# alike = re.search(s_trx, s_t)
	cmp = callable(cmp) and cmp or _cmps.calls_at[cmp]
	alike = cmp(s_trx, s_t)
	return alike
def like_t_eq(t:T, tnum, *tnum_pa):
	*_, year, month, day, hour, minute = *[None]*6, tnum, *tnum_pa
	alike = t.hour == hour and t.minute == minute
	return alike
def like_t_gt(t:T, *pa):
	targs, pargs = t.timetuple(), ((None,)*6 + pa)[-5:]
	result, *_ = *(targ > parg for targ, parg in zip(targs, pargs) if parg is not None and targ != parg), False
	return result
def like_t_lt(t:T, *pa):
	targs, pargs = t.timetuple(), ((None,)*6 + pa)[-5:]
	result, *_ = *(targ < parg for targ, parg in zip(targs, pargs) if parg is not None and targ != parg), False
	return result
def like_t_ge(t:T, *pa):
	targs, pargs = t.timetuple(), ((None,)*6 + pa)[-5:]
	result, *_ = *(targ > parg for targ, parg in zip(targs, pargs) if parg is not None and targ != parg), True
	return result
def like_t_le(t:T, *pa):
	targs, pargs = t.timetuple(), ((None,)*6 + pa)[-5:]
	result, *_ = *(targ < parg for targ, parg in zip(targs, pargs) if parg is not None and targ != parg), True
	return result

def td_since(t:T,fab=None):
	td = T.now() - t
	td = td if not fab else fab(td)
	return td

def prev_tick(td_tick:TD, t:T=None, t_zero:T=None, eq=no)->T:
	''' get previous tick ->T counting by *td_tick* from *t_zero* before given time *t*. the output mod *td_tick* will be 0
		:param td_tick: clock interval to round result down to
		:param t:       find tick preceding this time
		:param t_zero:  start time for clock ticks
		:param eq:      when True return input t if it is exactly a tick interval
	'''
	# resolve inputs
	tn_tick = int(td_tick.total_seconds())
	t = t or T.now()
	t_zero = (
		_D.epoch									if not t_zero else
		t_zero										if istime(t_zero) else
		_D.first_epoch_day_map[t_zero]				if isstr(t_zero) else
		_D.epoch + TM.coerce_deltatime(t_zero)		if istd(t_zero) else # todo: secondary path
		throw(ValueError(t_zero)) )
	if (t.tzinfo is None) != (t_zero.tzinfo is None):
		t_zero = t_zero.replace(tzinfo=t.tzinfo and t.tzinfo or None)
	
	# resolve output:  timestamp delta => previous clock tick 
	tf      = (t - t_zero).total_seconds() + (eq and .000001)
	tn_prev = (tf//tn_tick) * tn_tick		# round timestamp to whole interval
	t_prev  = t_zero + TD(seconds=tn_prev)
	return t_prev
def next_tick(td_tick:TD, t:T=None, t_zero:T=None, eq=no)->T:
	''' get next tick ->T counting by *td_tick* from *t_zero* after given time *t*. the output mod *td_tick* will be 0
		:param td_tick: clock interval to round result up to
		:param t:       result tick must follow this time
		:param t_zero:  start time for clock such that result mod *td_tick* is 0
	'''
	t_prev = prev_tick(td_tick, t, t_zero, eq)
	t_next = t_prev + ((eq and t==t_prev) and td0 or td_tick)
	return t_next

def is_daylight_savings_time(t:T, tz:'timezone'=None):
	t = t or T.now()
	tz = (
		_D.tz_default		if not tz else
		_pytz.timezone(tz)	if isinstance(tz, str) else
		tz	)
	
	dst = tz.localize(t).dst()
	return bool(dst.second)

def pargs_of_t(t,fab=None):		return (fab or tuple)(getattr(t,k) for k in _D.dc_args)
def kargs_of_t(t,fab=None):		return (fab or dict)((k,getattr(t,k)) for k in _D.dc_args)

def pargs_of_td(td,fab=None):	return (fab or tuple)(getattr(td,k) for k in _D.dcd_args)
def kargs_of_td(td,fab=None):	return (fab or dict)((k,getattr(td,k)) for k in _D.dcd_args)

since						= td_since
pa_of_t,ka_of_t=_,tvars		= pargs_of_t,kargs_of_t
pa_of_td,ka_of_td=_,tdvars	= pargs_of_td,kargs_of_td
is_dst						= is_daylight_savings_time
t_like, t_like_s, t_like_rx, t_like_eq, t_like_gt, t_like_lt, t_like_ge, t_like_le, = (
like_t, like_t_s, like_t_rx, like_t_eq, like_t_gt, like_t_lt, like_t_ge, like_t_le, ) # todo: remove formerly named
#endregion

#region time devcs
@dataclass
class TForm:
	fmtc: str
	#region defs
	t = t_now				= property(lambda self: self.fmtr(T.now()))
	#endregion
	#region forms
	def __post_init__(self):
		self.fmt = _fmt_of_fmtc(self.fmtc)
		self.fmtr = _fmtr_of_fmtc(self.fmtc)
		self.parser = _parser_of_fmtc(self.fmtc)
		self.fmtr_of = lambda t_obj=None, fmtr=self.fmtr: fmtr(t_of(t_obj))
	def _fmtr_of(self, t_obj=None):
		t = t_of(t_obj)
		st = self.fmtr(t)
		return st
	#endregion
	...

class TForms(TFmtcs,member_of_fmtc=TForm):				''' ... ie: '...' '''
class TFmts(TFmtcs,member_of_fmtc=_fmt_of_fmtc):		''' fmt as in Python bracket style time formats. ie: '{:%Y}' '''
class TFmtrs(TFmtcs,member_of_fmtc=_fmtr_of_fmtc):		''' fmtr as in time formatters. ie: fmtr(s_time:str) -> T '''
class TParsers(TFmtcs,member_of_fmtc=_parser_of_fmtc):	''' parser as in time parser funcs. ie: parser(t:T) -> str '''

class Time(DateClock):
	#region defs
	_fmt					= TFmtrs.ymd_hms
	zero:TimeZero			= None
	tf = ft					= property(lambda self: self.timestamp())
	tn = nt					= property(lambda self: int(self.timestamp()))
	str = s					= property(lambda self: str(self))
	dataclock = dc			= property(lambda self: DC(**kargs_of_t(self)))
	#endregion
	#region forms
	def __new__(cls,t=None,*pa,fmt=_fmt,**ka):
		# resolve inputs
		ka_t = (
			t|ka					if isdict(t) else
			tvars(t)|ka				if istpy(t) else
			tvars(t_of(t))			if not pa and not ka else
			tvars(TPy(t,*pa,**ka))	if pa else
			tvars(t_of(t).replace(**ka)) )
		
		# resolve output
		t_out = super().__new__(cls,**ka_t)
		t_out.fmt = iscx(fmt,orvar='format')
		t_out._str = ''
		return t_out
	@classmethod
	def of(cls,obj=miss,*pa,**ka):
		# resolve inputs
		if istype(obj,cls):			return obj
		pa,ka = (
			(pa,ka)					if obj is miss else
			((obj,)+pa,ka) )
		
		# resolve output
		out = cls(*pa,**ka)
		return out
	def __repr__(self):
		if not self._str:
			self._str = self.fmt(self)
		return self._str
	__str__					= __repr__
	#endregion
	#region utils
	def __add__(self,obj):
		out = TPy.__add__(self,obj) if not istpy(obj) else throw(f'cant add times. got {self=} + {obj=}')
		return out
	def __radd__(self,obj):
		out = TPy.__add__(obj,self) if not istpy(obj) else throw(f'cant add times. got {obj=} + {self=}')
		return out
	def __sub__(self,obj):
		obj_out = isnum(obj,fab=T)
		out =  istdpy(TPy.__sub__(self,obj_out),fab=TD.of)
		return out
	def __rsub__(self,obj):
		obj_out = isnum(obj,fab=T)
		out =  istdpy(TPy.__sub__(obj_out,self),fab=TD.of)
		return out
	#endregion
	...
class TimeZero(Time):
	#region forms
	def __new__(cls):
		obj = super().__new__(cls,T.min,fmt='*')
		return obj
	__bool__				= never
	#endregion
	...

class TimeDelta(timedelta):
	#region defs
	tdf						= property(lambda self: self.total_seconds())
	tdn						= property(lambda self: int(self.total_seconds()))
	#endregion
	#region forms
	def __new__(cls,td=None,*pa,**ka):
		# resolve inputs
		pa = (td,)[td is None:] + pa
		ka_td = (
			td|ka					if isdict(td) else
			tdvars(td)|ka			if istdpy(td) else
			tdvars(timedelta(*pa,**ka)) )
		
		# resolve output
		td_out = super().__new__(cls,**ka_td)
		return td_out
	@classmethod
	def of(cls,obj=miss,*pa,**ka):
		if istype(obj,cls):			return obj
		
		# resolve inputs
		pa = (obj,)[obj is None:] + pa
		
		# resolve output
		out = cls(*pa,**ka)
		return out
	#endregion
	...

T,TD,T0						= Time, TimeDelta, TimeZero
istimepy = istpy=is_tpy		= istypethen(TPy)
istimedeltapy=istdpy=is_tdpy= istypethen(TDPy)
istime = ist=is_t			= istypethen(T)
istimedelta = istd=is_td	= istypethen(TD)

T.zero = tzero = t0  		= TimeZero()
td0 = td_none				= TD(TDPy())
now							= T.now
_TCCls						= _D.form, _D.fmt, _D.fmtc, _D.fmtr, _D.parser, = (
							  TForms,  TFmts,  TFmtcs,  TFmtrs,  TParsers, )

isdateclock = isdc=is_dc	= istypethen(TPy)
isdateclockdelta=isdcd=is_dcd= istypethen(TDPy)
#endregion
