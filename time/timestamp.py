from pyutils.time.timedefs import *
from pyutils.utilities.callbasics import *
from pyutils.patterns.decor import staticmethod_optional_self

#region timestamp utils
TN,TF							= int, float

def tn_of(t:T=None,fab=int) ->TN:
	''' coerce *t* to timestamp ->tn '''
	tn = (
		t							if typeof(t, (int,float)) else
		T.now().timestamp()			if t is None else
		t.timestamp()				if is_t(t) and DDT.epoch_naive <= t else
		-1							if is_t(t) else
		t.total_seconds()			if hasattr(t,'total_seconds') else
		t_of(t).timestamp() )
	tn = tn if not fab else fab(tn)
	return tn
def t_of_tn(tn:[TN,str]) ->T:
	tn = tn if not isinstance(tn, str) else float(tn)
	t = T.fromtimestamp(tn)
	return t
def tf_of_td(td:TD) ->TF:
	if isinstance(td, float):  return td
	
	tf = td.total_seconds()
	return tf

tn_now,tf_now = _,tf_of			= tn_of, redef(tn_of,fab=float)
tdn_since						= redef(td_since, fab=tn_of)
tdf_since						= redef(td_since, fab=tf_of)
tid_of,tidf_of = tid_now,_		= tn_of, tf_of				# todo: eliminate
t_of_tid,tidf_of,tidf_of_td		= t_of_tn, tf_of, tf_of_td	# todo: eliminate
#endregion

#region timestamp devcs
class TimestampIx:
	''' Unix timestamp interface '''
	#region forms
	_fmt_t						= TFmts.ymd_hms
	_fmt						= ('@{}->(%s)' % _fmt_t)
	s							= None
	_t							= property(lambda self: T.fromtimestamp(self))
	_st							= property(lambda self: sof(self._fmt_t, T.fromtimestamp(self)))
	_s							= property(lambda self: sof(self._fmt, self._num, T.fromtimestamp(self)))
	_num						= property(lambda self: self.to_num(self))
	#endregion
	#region forms
	def __init_subclass__(cls, to_num=None,**ka):
		cls.to_num = to_num or issubclass(cls,int) and int or issubclass(cls,float) and float or throw()
	def __new__(cls,val):
		tn = tf_of(val)
		obj = super().__new__(cls,tn)
		return obj
	def __repr__(self):
		self.s = self.s or self._s
		return self.s
	def format(self,fm=''):
		s = (
			self._s				if not fm else
			self._st			if fm == 't' else
			str(self._num)		if fm == 'n' else
			str(int(self))		if fm == 'd' else
			str(float(self))	if fm == 'f' else
			throw(fm) )
		return s
	__format__					= format
	of							= __new__
	#endregion
	#region utils
	def __add__(self, val):
		out = (
			self+val	if isnum(val) else
			self+self.of(val) )
		out = self.of(out)
		return out
	#endregion
	#region predicates
	@staticmethod_optional_self
	def is_today(obj):
		result = T.now().date() == Timestamp.t_of(obj).date()
		return result
	(	as_int, ns_to_dc, to_float, dt_to_float, _, ) = (  # fixme: remove formerly named
		tid_of, t_of_tid, tidf_of, tidf_of_td, now, ) = (
		tid_of, t_of_tid, tidf_of, tidf_of_td, tid_now, )
	#endregion
	...

class TimeNum(TimestampIx, int):	pass
class TimeFlt(TimestampIx, float):
		_fmt						= '@{:.4}->(%s)' % TFmts.ymd_hms

class TSecs(TimeNum):
	''' TSecs is to TID as TD is to T '''
	# todo
	@staticmethod
	def of(td:TD):
		tsecs = int(
			td.total_seconds()	if isinstance(td, TD) else
			td					if isinstance(td, (int,float)) else
			
			throw(TypeError('Expected data param to be number or timedelta.  Got %s' % type(td).__name__))
		)
		return tsecs
	tsecs_of = of

Timestamp,_	= TN,TF				= TimeNum, TimeFlt
TID,TIDF						= TN, TF	# todo: eliminate
#endregion
