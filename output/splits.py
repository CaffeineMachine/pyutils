

class Split(str):
	''' Splits a text by given separator string '''
	def split(self, text, **ka):
		result = text.split(self)
		return result
	
	# method aliases
	__call__ = split

class DKSplit(Split):
	''' Similar to Split but only splits text when given **ind**ex is 0 '''
	def split(self, text, to_seq=tuple, _ind=0, **ka):
		assert _ind in (0,1), f'Got unexpected key-value tuple _ind{_ind} for {text}'
		if _ind == 1: return text
		
		result = text.split(self)
		result = result if not to_seq else to_seq(result)
		return result
	
	# method aliases
	__call__ = split

class DVSplit(Split):
	''' Similar to Split but only splits text when given **ind**ex is 1 '''
	def split(self, text, to_seq=None, _ind=1, **ka):
		assert _ind in (0,1), f'Got unexpected key-value tuple _ind{_ind} for {text}'
		if _ind == 0: return text
		
		result = text.split(self)
		result = result if not to_seq else to_seq(result)
		return result
	
	# method aliases
	__call__ = split

class Splits:
	''' Splits a text into nested sequences corresponding to the given separator strings '''
	_basic_fab = Split
	
	def __init__(self, *seps, fabs=(...,)):
		'''	ctor
			:param seps: optional subsequent separator strings
			:param fabs: Split classes to match with each given sep; use ... to fill between with Split up to len(seps)
		'''
		if ... in fabs:
			assert fabs.index(...) != 1, 'multiple ellipsis in split_fabs: {split_fabs} is not allowed'
			fill_ind, fill_count = fabs.index(...), len(seps)-len(fabs)+1
			fabs = [*fabs[:fill_ind]] + [self._basic_fab]*fill_count + [*fabs[fill_ind+1:]]
		self.splits = [split_fab(sep) for split_fab, sep in zip(fabs, seps)]
		
	def split(self, text, _depth=0, **ka):
		results = self.splits[_depth](text, **ka)
		if _depth+1 < len(self.splits):
			results = [self.split(result, _depth=_depth+1, _ind=ind) for ind, result in enumerate(results)]
		return results
	
	# def __iter__(self): pass

	# method aliases
	__call__ = split