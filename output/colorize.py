import re
import colorama as cm									# todo: remove external dependency on ansi codes

from pyutils.defs.colldefs import xdict
from pyutils.utilities.alyzutils import *
from pyutils.utilities.clsutils import *
from pyutils.utilities.iterutils import *
from pyutils.utilities.maputils import mapeach,dintomap

#region color defs
_NS						= xdict()
class DefsColorize:
	''' struct for all available color enumerations '''
	
	# todo: consolidate this class with Fore and Back below
	# available text colors
	names				= tstrs('black  white  red  magenta  blue  yellow  green  cyan')
	names_layer			= (fore,back) = tstrs('fore  back')
	names_shade			= (normal,light) = tstrs('normal  light')

	# generated from all template permutations of colors, layer={ForeG, BackG} and shades={Normal, Light}
	tags				= {}						# generated abbreviated tag,colorama-value pairs like 'redl':Fore.LIGHTRED_EX, 'cyanb':Back.CYAN
	layers				= {fore:{}, back:{}}		# generated same as tags but nested in a dict of groups_names
	shades				= {normal:{}, light:{}}		# generated same as tags but nested in a dict of groups_names
	dbg = _NS.dbg		= yes
	dbg = _NS.dbg		= no
	normal				= dbg and 'normal' or cm.Fore.RESET
	normalb				= dbg and 'normalb' or cm.Back.RESET
	reset_all			= dbg and 'plain' or cm.Style.RESET_ALL
	
	# color finding patterns
	# reset_at_isback	= {None:reset_all,False:normal,True:normalb,}
	
	#region scopes
	class Strs:
		fmt_colr		= _NS.dbg and '<{name_u}|{{}}|{reset}>' or '{code}{{}}{reset}'
		fmt_colr		= '{code}{{}}{reset}'
		rx_colorcode	= r'\x1b\[\d+m'
		rx_strcolorcode	= fr'({rx_colorcode})?(.*?(?={rx_colorcode}))?'
		rx_into_plain	= rx_colorcode, ''
	class Vals:
		debugging		= False
		reset_f			= _NS.dbg and 'reset_f' or cm.Fore.RESET
		reset_b			= _NS.dbg and 'reset_b' or cm.Back.RESET
		reset_fb=reset	= _NS.dbg and 'reset_fb' or cm.Style.RESET_ALL
		
		layers			= unset, fore,    back,    both  = (
						  0b00,  0b01,    0b10,    0b11,   )
		resets			= '',    reset_f, reset_b, reset_fb,
		reset_at		= mapeach(layers,resets)
	SS,VV				= Strs, Vals
	#endregion
	...

DDColr = _D				= DefsColorize
SSColr,VVColr = _S,_V	= DDColr.SS, DDColr.VV
#endregion

#region repr devcs
# @dataclassown
class ColorRepr:
	code:str
	name:str			= ''
	reset:str			= None
	layer:int			= VVColr.unset
	fmt:str				= SSColr.fmt_colr
	next:iscx			= None
	#region defs
	_layer				= VVColr.unset
	name_u				= property(lambda self: self.name.upper())
	codes				= property(lambda self: (self.code,self.reset))
	#endregion
	#region forms
	def __init_subclass__(cls, layer=VVColr.unset, call='repr',**ka):
		cls._layer = layer
		cls.__call__ = cls.__truediv__ = getattr(cls,call,cls.repr)
	def __post_init__(self):
		self.layer = self.layer or self._layer
		self.reset = self.reset or VVColr.reset_at[self.layer]
		if _D.dbg:
			self.code = self.name_u
			# self.reset = self.reset or VVColr.reset_at[self.layer]
		self._fmtr = self.fmt.format(**self.__dict__,name_u=self.name_u).format
	def __init__(self,*pa,**ka):
		annoinit(self,*pa,**ka)
		self.layer = self.layer or self._layer
		self.reset = self.reset or VVColr.reset_at[self.layer]
		if _D.dbg:
			self.code = self.name_u
			# self.reset = self.reset or VVColr.reset_at[self.layer]
		self._fmtr = self.fmt.format(**self.__dict__,name_u=self.name_u).format
	def __repr__(self):			return f'{self.__class__.__name__}({self.name or self.code})'
	def __hash__(self):			return hash((self.__class__,self.code,self.name))
	def __dir__(self):			return annos(self)
	def repr(self, msg, fm='', **ka):
		if self.next:
			msg = self.next(msg)
		sc_msg = self._fmtr(format(msg,fm).replace(self.reset or 'DONT REPLACE', self.code))
		return sc_msg
	def __add__(self, next):
		ka,next = annovars(self), self.next and self.next+next or next
		colr = self.__class__(**dict(ka,**{isstr(next) and 'fmt' or 'next' :next}))
		return colr
	def __sub__(self, logr):
		ka = annovars(self)
		colr = ColrLogr(**ka,logr=logr)
		return colr
	def __neg__(self):			return self - print
	__call__			= repr
	#endregion
	...
class ColrFore(ColorRepr, layer=VVColr.fore):	pass
class ColrBack(ColorRepr, layer=VVColr.back):	pass
class ColrPlain(ColorRepr):
	code:str			= DDColr.reset_all
	name:str			= 'plain'
	#region forms
	def __init__(self):
		super().__init__(DDColr.reset_all, 'plain')
	def repr(self, msg, fm='', **ka):
		sc_msg = self._fmtr(format(msg,fm))
		if self.next:
			sc_msg = self.next(sc_msg)
		return sc_msg
	#endregion
	...
class ColorsRepr(ColorRepr):
	''' todo: ColrRepr to apply fore & back layers in single pass rather than resorting to chaining '''
	#region forms
	def __init__(self, *codes,**ka):
		super().__init__(DDColr.reset_all, 'plain')
	def repr(self, msg, fm='', **ka):
		sc_msg = self._fmtr(format(msg,fm))
		if self.next:
			sc_msg = self.next(sc_msg)
		return sc_msg
	#endregion
	...

class ColrLogr(ColorRepr, call='log'):
	''' formats and writes to a given logger '''
	#region defs
	_j_msgs				= ' '.join
	#endregion
	#region forms
	def __init__(self, *pa, logr=print, ka_log=None, to_str=format, **ka):
		self.logr = logr
		self.ka_log = ka_log or {}
		self.to_str = to_str
		if pa and isinstance(pa[0], Colr):
			colr, *pa = pa
			assert not pa
			ka.update(dintomap(colr.__dict__, 'code  name  layer'))
		super().__init__(*pa, **ka)
	def repr(self, msg='', *msg_pa, fm='', **ka):  #, **ka_log):
		sc_msg = msg if not msg_pa else self._j_msgs(istrs((msg, *msg_pa)))
		if self.next:
			sc_msg = self.next(sc_msg)
		sc_msg = self._fmtr(self.to_str(sc_msg,fm).replace(self.reset or 'DONT REPLACE', self.code))
		return sc_msg
	def log(self, msg='', *msg_pa, fm='', **ka_log):
		# resolve inputs
		ka_log = self.ka_log and ka_log and dict(self.ka_log, **ka_log) or self.ka_log or ka_log
		
		# resolve output
		sc_msg = self.repr(msg, *msg_pa, fm=fm)
		self.logr(sc_msg, **ka_log)
		return sc_msg
	__call__			= log
	#endregion
	...

_get_dark_code			= lambda layer,col: getattr(layer, f'{col.upper()}')
_get_light_code			= lambda layer,col: getattr(layer, f'LIGHT{col.upper()}_EX')
_fore_dark_fmtr			= lambda col: ColrFore(_get_dark_code(cm.Fore, col), f'{col}')
_fore_light_fmtr		= lambda col: ColrFore(_get_light_code(cm.Fore, col), f'light{col}')
_back_dark_fmtr			= lambda col: ColrBack(_get_dark_code(cm.Back, col), f'{col}bg')
_back_light_fmtr		= lambda col: ColrBack(_get_light_code(cm.Back, col), f'light{col}bg')

Colr,Colrs				= ColorRepr,ColorsRepr
#endregion

#region color code defs
class DefsCode:
	#region scopes
	class style:
		reset			= '\u001b[0m'
		bold			= '\u001b[1m'
		faint			= '\u001b[2m'
		italic			= '\u001b[3m'
		under=underline	= '\u001b[4m'
		blink			= '\u001b[5m'
		blink2			= '\u001b[6m'
		flip=rev		= '\u001b[7m'
		hide=conceal	= '\u001b[8m'
		strike			= '\u001b[9m'
		primary			= '\u001b[10m'
		fraktur			= '\u001b[20m'
		under2			= '\u001b[21m'
		framed			= '\u001b[51m'
		circled			= '\u001b[52m'
		over=overline	= '\u001b[53m'
	class unset:
		bold			= '\u001b[0m'
		italic			= '\u001b[23m'
		fraktur			= '\u001b[23m'
		under			= '\u001b[24m'
		under2			= '\u001b[24m'
		blink			= '\u001b[25m'
		blink2			= '\u001b[25m'
		flip=rev		= '\u001b[27m'
		hide=conceal	= '\u001b[28m'
		strike			= '\u001b[29m'
		framed			= '\u001b[54m'
		circled			= '\u001b[54m'
		over=overline	= '\u001b[55m'
	class fore:
		class dark:
			black		= '\u001b[30m'
			red			= '\u001b[31m'
			green		= '\u001b[32m'
			yellow		= '\u001b[33m'
			blue		= '\u001b[34m'
			purple		= '\u001b[35m'
			cyan		= '\u001b[36m'
			white		= '\u001b[37m'
		class light:	# todo
			yellow		= '\u001b[93m'
	class back:
		class light:	# todo
			pass
		class dark:		# todo
			pass
		
	on,un				= style,unset
	#endregion
	...
DDCode					= DefsCode

class Style:
	colr_of				= lambda name: Colr(name=name,code=getattr(DDCode.on,name),reset=getattr(DDCode.un,name,None))
	
	bold				= colr_of('bold')
	flip = rev			= colr_of('flip')
	over = overline		= colr_of('over')
	under = underline	= colr_of('under')
	under2 = underline2	= colr_of('under2')
	blink				= colr_of('blink')
	hide				= colr_of('hide')
	strike				= colr_of('strike')
	italic				= colr_of('italic')
	circled				= colr_of('circled')
	framed				= colr_of('framed')
	fraktur				= colr_of('fraktur')
	
	_compat_ide			= bold,italic,under,under2,strike,framed,flip

class ColorLayer:
	@classmethod
	def of(cls, attr):		return getattr(cls,attr)
	__class_getitem__	= of
class Fore(ColorLayer):
	class Dark(ColorLayer):
		_all = black, white, red, magenta, blue, yellow, green, cyan	= [*map(_fore_dark_fmtr, DDColr.names)]
	class Light(ColorLayer):
		_all = black, white, red, magenta, blue, yellow, green, cyan	= [*map(_fore_light_fmtr, DDColr.names)]
	# defaults
	reset = normal = clear												= ColrFore(cm.Fore.RESET, 'normal')
	black, white, red, magenta, blue, yellow, green, cyan				= Dark._all
	blackl, whitel, redl, magental, bluel, yellowl, greenl, cyanl		= Light._all

class Back(ColorLayer):
	class Dark(ColorLayer):
		_all = black, white, red, magenta, blue, yellow, green, cyan	= [*map(_back_dark_fmtr, DDColr.names)]
	class Light(ColorLayer):
		_all = black, white, red, magenta, blue, yellow, green, cyan	= [*map(_back_light_fmtr, DDColr.names)]
	# defaults
	reset = normal = clear												= ColrBack(cm.Back.RESET, 'clear')
	black, white, red, magenta, blue, yellow, green, cyan				= Dark._all

# layer aliases
black, white, red, magenta, blue, yellow, green, cyan					= Fore.Dark._all
blackl, whitel, redl, magental, bluel, yellowl, greenl, cyanl			= Fore.Light._all
blackb, whiteb, redb, magentab, blueb, yellowb, greenb, cyanb			= Back.Dark._all
blacklb, whitelb, redlb, magentalb, bluelb, yellowlb, greenlb, cyanlb	= Back.Light._all
gray, grayb, grayl, graylb												= blackl, blacklb, white, whiteb
normal, clear, plain = resetf,resetb,reset								= Fore.normal, Back.normal, ColrPlain()
bold,italic,underline,underline2,strike,framed,flipd					= Style._compat_ide

pblack,pwhite,pred,pmagenta,pblue,pyellow,pgreen,pcyan					= map(ColrLogr, Fore.Dark._all)
pblackl,pwhitel,predl,pmagental,pbluel,pyellowl,pgreenl,pcyanl			= map(ColrLogr, Fore.Light._all)
pblackb,pwhiteb,predb,pmagentab,pblueb,pyellowb,pgreenb,pcyanb			= map(ColrLogr, Back.Dark._all)
pblacklb,pwhitelb,predlb,pmagentalb,pbluelb,pyellowlb,pgreenlb,pcyanlb	= map(ColrLogr, Back.Light._all)
pgray,pgrayb,pgrayl,pgraylb												= map(ColrLogr, [blackl,blacklb,white,whiteb])
pnormal, pclear, pplain = p_resetf,p_resetb,p_reset						= [*map(ColrLogr, [normal,clear,plain])]
pbold,pitalic,punderline,punderline2,pstrike,pframed,pflipd				= map(ColrLogr, Style._compat_ide)
#endregion

#region frame logr utils
FAlyz.set_lvls([bluel,yellowl,redl],ns_vars=globals())
#endregion

#region examine colored text
_globals				= globals()
class ColorId:
	sfxs				= tstrs(' l b lb',keep=None)
	scolrs				= [*map(''.join,iperm(DDColr.names,sfxs)),'normal','clear','plain']
	scolrs_id			= ['<%s>' % scolr.upper() for scolr in scolrs]
	rxs_in_scol			= [_globals[scolr].code for scolr in scolrs]
	rxs_in_scol_raw		= [re.sub(r'\[',r'\[', re.sub(r'\x1b',r'\\x1b', _globals[scolr].code)) for scolr in scolrs]
	rxs_to_scol_id		= tstrs(scolrs_id, r'\g<0>{}')
	rxs_into_scolr		= tzip(rxs_in_scol,rxs_in_scol_raw,scolrs_id)
	rxs_into_scol_id	= tzip(rxs_in_scol,rxs_in_scol_raw,rxs_to_scol_id)

class StrColor(str):
	_rx_fmt_scol_sz		= r'(.*?)(\d+)(.*)'
	def repr(self, fmt_size='', size=None) ->str:
		
		if not fmt_size and not size:
			return self
		
		len_color = len_color_text(self)
		if size:
			fmt_spec_resize = size + len_color
		elif fmt_size:
			found = re.search(self._rx_fmt_scol_sz, fmt_size)
			left, size, right = found.groups()
			fmt_spec_resize = '{}{}{}'.format(left, int(size) + len_color, right)

		strout = '{:{}}'.format(str(self), fmt_spec_resize)
		return strout
	# method aliases
	__format__			= repr
SCol					= StrColor

def show_color_ids(text, id_only=False):
	rxs_into_scolr = id_only and ColorId.rxs_into_scolr or ColorId.rxs_into_scol_id
	for rx_in_scol_raw,*rx_into_scolr in rxs_into_scolr:
		if rx_in_scol_raw in text:
			text = re.sub(*rx_into_scolr, text)
	return text

# text color formatter functions; wraps text with ansi code of given color
def color_fmt(msg, color):		return getattr(Fore, color, Fore.normal)(msg)
def cleared(text):
	result = re.sub(DDColr.rx_colorcode, '', text)
	return result
def len_parts_text(text):
	''' get lengths of text parts: normal text, ansi format text, and whole text (in that order) '''
	text = str(text)
	len_text = len(text)
	color_parts = re.findall(SSColr.rx_colorcode, text)
	len_color_parts = sum(map(len, color_parts))
	len_str_parts = len_text - len_color_parts
	return len_str_parts, len_color_parts, len_text
def len_plain_text(text):
	''' get str len of normal text without ansi format text '''
	len_plain, _, _ = len_parts_text(text)
	return len_plain
def len_color_text(text):
	''' get str len of ansi format text without normal text '''
	_, len_colors, _ = len_parts_text(text)
	return len_colors

def parts_strcolor_text(text, fab=None, group=False):
	found = re.findall(SSColr.rx_strcolorcode, text)
	found[-1] == ('','') and found.pop()
	found = group and zip(*found) or ijoin(found)
	result = fab and fab(found) or found
	return result
def parts_str_color_text(text, fab=tuple):
	color_seq, str_seq = parts_strcolor_text(text=text, fab=fab, group=True)
	return str_seq, color_seq
def lens_str_color_text(text, fab=tuple):
	groups = color_seq, str_seq = parts_strcolor_text(text=text, fab=fab, group=True)
	len_colors, len_strs = [sum(map(len, group)) for group in groups]
	return len_strs, len_colors
len_cleared, len_ansifmt, len_text_parts = len_plain_text, len_color_text, len_parts_text
#endregion

#region tests
def print_styles():
	print('\n<style>')
	for name,style in Style.__dict__.items():
		if name == getattr(style,'name',''):
			print(style(f'{style.name:^{15}}')+'   ', end='\n')
	print()

def print_colors_basic():
	print('\n<color basic>')
	for layer_level in Fore.Dark, Fore.Light, Back.Dark, Back.Light:
		for ansi_fmtr in layer_level._all:
			print(ansi_fmtr(f'{ansi_fmtr.name:^{15}}'), end='')
		print()
	print()

def print_colors_swatch():
	# todo: this fails to illustrate the 256 color cube arrangement 
	print('\n<color swatch>')
	import sys
	for i in range(0, 16):
		for j in range(0, 16):
			if not (i * 16 + j+2) % 6:
				print(u"\x1b[0m")
			code = u'\x1b[48;5;'+str(i * 16 + j)
			sys.stdout.write(code + "m " + repr(code).replace('\'', '').ljust(14))
	print(u"\x1b[0m\n")

def print_colors_raw():
	print('\nsamples <color raw>')
	print(u"\x1b[30m A \x1b[31m B \x1b[32m C \x1b[33m D \x1b[34m E \x1b[35m F \x1b[36m G \x1b[37m H \x1b[0m")
	print(u"\x1b[90m A \x1b[91m B \x1b[92m C \x1b[93m D \x1b[94m E \x1b[95m F \x1b[96m G \x1b[97m H \x1b[0m")
	print(u"\x1b[40m A \x1b[41m B \x1b[42m C \x1b[43m D \x1b[44m E \x1b[45m F \x1b[46m G \x1b[47m H \x1b[0m")
	print(u"\x1b[100m A \x1b[101m B \x1b[102m C \x1b[103m D \x1b[104m E \x1b[105m F \x1b[106m G \x1b[107m H \x1b[0m")

if __name__=='__main__':
	print_styles()
	print_colors_basic()
	print_colors_swatch()
	
	text = '>>> ' + bluel('hello')+' '+redl('world')
	print(parts_strcolor_text(text, fab=list))
	print(parts_str_color_text(text))
	print(len_color_text(text))
	
	# reveal debugging
	msg = cyanl('{} {} !!!'.format('Hello',blueb('World')))
	msg_dbg = show_color_ids(msg)
	print(msg)
	print(msg_dbg)
	
	print('abc\r\x1b[32Chello world')
#endregion
