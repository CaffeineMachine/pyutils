from __future__ import annotations
from pyutils.utilities.numutils import enum_bits, flags_of_masks

from pyutils.structures.strs import F, S

import re
from dataclasses import dataclass, field
from typing import *
from collections import namedtuple

from pyutils.utilities.iterutils import affix
from pyutils.patterns.accessors import KK
from pyutils.output.colorize import *
from pyutils.structures.maps import *
from pyutils.structures.calls import *

#region defs
Value						= bool|int
StatesType					= list|dict

class DefsColorMap:
	#region scopes
	class Bits:
		R,G,B,DK,LT			= RED,GREEN,BLUE,DARK,LIGHT = enum_bits(5)
	class Strs:
		j_cch,j_kk			= '', '_'
		fmt_x_dk			= F/u'\x1b[{}m'
		fmt_x_lt			= F/u'\x1b[{};1m'
		fmt_xx_bg			= F/u'\x1b[48;5;{}m'
		fmt_xx_fg			= F/u'\x1b[38;5;{}m'
	BB,SS					= Bits,Strs
	# class Fore:
	# 	rmbcg				= EnumScale([red, magenta, blue, cyan, green])
	# 	rmbcg_l				= EnumScale([redl, magental, bluel, cyanl, greenl])
	# 	rrygg				= EnumScale([redl, red, yellow, green, greenl])
	# 	ryygg				= EnumScale([red, yellow, yellowl, green, greenl])
	# 	mbcg				= EnumScale([magenta, blue, cyan, green])
	# 	mbcg_l				= EnumScale([magental, bluel, cyanl, greenl])
	# 	cbmry				= EnumScale([cyan, blue, magenta, red, yellow])
	# 	cbmry_l				= EnumScale([cyanl, bluel, magental, redl, yellowl])
	# 	rygg				= EnumScale([red, yellow, green, greenl])
	# 	rygb				= EnumScale([red, yellow, green, blue])
	# 	ryg					= EnumScale([red, yellow, green])
	# 	ryg_l				= EnumScale([redl, yellowl, greenl])
	# 	rg					= EnumScale([red, green])
	# 	rg_l				= EnumScale([redl, greenl])
	# 	cg					= EnumScale([normal, green])
	# 	cb					= EnumScale([normal, blue])
	# class Back:
	# 	rmbcg				= EnumScale([redb, magentab, blueb, cyanb, greenb])
	# 	rmbcg_l				= EnumScale([redl, magental, bluel, cyanl, greenl])
	# 	ryscg				= EnumScale([redb, yellowb, grayb, cyanb, greenb])
	# 	rrygg				= EnumScale([redlb, redb, yellowb, greenb, greenlb])
	# 	ryygg				= EnumScale([redb, yellowb, yellowlb, greenb, greenlb])
	# 	rygg				= EnumScale([redb, yellowb, greenb, greenlb])
	# 	rygb				= EnumScale([redb, yellowb, greenb, blueb])
	# 	ryg					= EnumScale([redb, yellowb, greenb])
	# 	ryg_l				= EnumScale([redlb, yellowlb, greenlb])
	# 	rg					= EnumScale([redb, greenb])
	# 	rg_l				= EnumScale([redlb, greenlb])
	# 	cg					= EnumScale([normal, greenb])
	# 	cb					= EnumScale([normal, blueb])
	
	Fore = fg				= lazy(lambda _: Obj(
		rmbcg				= EnumScale([red, magenta, blue, cyan, green]),
		rmbcg_l				= EnumScale([redl, magental, bluel, cyanl, greenl]),
		rrygg				= EnumScale([redl, red, yellow, green, greenl]),
		ryygg				= EnumScale([red, yellow, yellowl, green, greenl]),
		mbcg				= EnumScale([magenta, blue, cyan, green]),
		mbcg_l				= EnumScale([magental, bluel, cyanl, greenl]),
		cbmry				= EnumScale([cyan, blue, magenta, red, yellow]),
		cbmry_l				= EnumScale([cyanl, bluel, magental, redl, yellowl]),
		rygg				= EnumScale([red, yellow, green, greenl]),
		rygb				= EnumScale([red, yellow, green, blue]),
		ryg					= EnumScale([red, yellow, green]),
		ryg_l				= EnumScale([redl, yellowl, greenl]),
		rg					= EnumScale([red, green]),
		rg_l				= EnumScale([redl, greenl]),
		cg					= EnumScale([normal, green]),
		cb					= EnumScale([normal, blue]),
		
		size_scale_map		= {},
	))
	Back = bg				= lazy(lambda _: Obj(
		rmbcg				= EnumScale([redb, magentab, blueb, cyanb, greenb]),
		rmbcg_l				= EnumScale([redl, magental, bluel, cyanl, greenl]),
		ryscg				= EnumScale([redb, yellowb, grayb, cyanb, greenb]),
		rrygg				= EnumScale([redlb, redb, yellowb, greenb, greenlb]),
		ryygg				= EnumScale([redb, yellowb, yellowlb, greenb, greenlb]),
		rygg				= EnumScale([redb, yellowb, greenb, greenlb]),
		rygb				= EnumScale([redb, yellowb, greenb, blueb]),
		ryg					= EnumScale([redb, yellowb, greenb]),
		ryg_l				= EnumScale([redlb, yellowlb, greenlb]),
		rg					= EnumScale([redb, greenb]),
		rg_l				= EnumScale([redlb, greenlb]),
		cg					= EnumScale([normal, greenb]),
		cb					= EnumScale([normal, blueb]),
		
		size_scale_map		= {},
	))
	
	sz						= 0x06
	ndclrs					= Obj(
		blue				= sz**0,
		green				= sz**1,
		red					= sz**2,
		gray				= 0x01, )
	nclrs					= Obj(
		color0				= (color0:=0x10),
		black				= color0,
		red					= color0 + ((sz-1)*ndclrs.red),
		green				= color0 + ((sz-1)*ndclrs.green),
		blue				= color0 + ((sz-1)*ndclrs.blue),
		yellow				= color0 + ((sz-1)*ndclrs.red)   + ((sz-1)*ndclrs.green),
		cyan				= color0 + ((sz-1)*ndclrs.green) + ((sz-1)*ndclrs.blue),
		magenta				= color0 + ((sz-1)*ndclrs.blue)  + ((sz-1)*ndclrs.red),
		white				= color0 + ((sz-1)*ndclrs.red)   + ((sz-1)*ndclrs.green)  + ((sz-1)*ndclrs.blue),
		gray9				= 0xff,
		gray0				= color0 + sz**3, )	# = 232
	kk_prime				= S*'red  blue  green'
	kk_nnclr				= S*'nbr  nbmr  nbcg  nbcmr  bcmr  gymr  gcbmr  gbr'
	kclrs					= None
	nnclrs					= lazy(lambda _: gen_nn_clrs())
	#endregion
	#region colls
	scale_palette			= {}
	empty_ka				= {}
	grade_scale_map 		= grades_refs = {}
	# actionable			= GradeScale('take_action')
	# unknown				= GradeScale('unknown')  # todo: unknown is a possible initial state for all grades
	#endregion
	#region utils
	@classmethod
	def register_grades(cls, *grade_scales):
		for grade_scale in grade_scales:
			for grade in grade_scale:
				cls.grades_refs[grade.name] = grade_scales
	@classmethod
	def initialize(cls):
		# gen_nn_clrs()
		cls.Fore=cls.fg=fg	= cls.Fore
		cls.Back=cls.bg=bg	= cls.Back
		# cls.fg.size_scale_map	= {len(scale): scale for scale in (cb, rg, ryg, rygg, rmbcg)}
		# cls.bg.size_scale_map	= {len(scale): scale for scale in (cb, rg, ryg, rygg, rmbcg)}
		
		cls.fg.size_scale_at= {len(scale): scale for scale in seqmap(cls.fg,'cb  rg  ryg  rygg  rmbcg')}
		cls.bg.size_scale_at= {len(scale): scale for scale in seqmap(cls.bg,'cb  rg  ryg  rygg  rmbcg')}
		
		cls.scale_palette		= dict(fg=cls.fg.size_scale_at, bg=cls.bg.size_scale_at)|{None:cls.fg.size_scale_at}
		cls.register_grades(*(grade for grade in cls.__dict__.values() if isinstance(grade, GradeScale)))
	#endregion
	...
DDCLKV = _D					= DefsColorMap
BBCLKV,SSCLKV = _B,_S		= _D.BB,_D.SS
#endregion

#region forms
_D.mask_rgb					= 0xf00, 0x0f0, 0x00f
iflag_rgb					= flags_of_masks(_D.mask_rgb)

def colorkey_of(*pa, r=0, g=0, b=0, key=None, hex=None, raw=no, nnclr_at=None, ev=None, **ka):
	# resolve inputs
	nnclr_at = nnclr_at or _D.nnclrs
	key,*_ = *pa, key
	hex = isnone(hex) and isstr(key) and key.startswith('#')
	color0 = not raw and _D.nclrs.color0 or 0
	
	# resolve input colors
	r,g,b = (
		(r,g,b)				if key is None else
		nnclr_at[key]		if istype(key, str|int) and key in nnclr_at else
		iflag_rgb(key)		if isint(key) else
		tints(L*key[-3:])		if isiter(key) else
		throw(locals()) )
	r,g,b  = not hex and (r,g,b) or (r//2,g//2,b//2)
	assert all(0 <= n < 8 for n in (r,g,b))
	
	# resolve output
	cl_out = _D.ndclrs.red*r + _D.ndclrs.green*g + _D.ndclrs.blue*b + color0
	cl_out = cl_out if not ev else ev(cl_out)
	return cl_out

def nn_color_interp(nd_clr, n_clr, size=256, stop=None):
	nd_clr,n_clr = istypethen(KClr,nd_clr,var='nd'), istypethen(KClr,n_clr,var='n')
	nd_clr,n_clr = _D.ndclrs.get(nd_clr,nd_clr), _D.nclrs.get(n_clr,n_clr), 
	nn_clr = [n_clr + (n*nd_clr) for n in range(size)]
	return nn_clr
def nn_color_interps(nd_clr, n_clrs, fabs=asis, fab=tuple, **ka):
	return fab(map(fabs, [nn_color_interp(nd_clr,n_clr, **ka) for n_clr in n_clrs]))

def gen_k_clrs():
	''' populate term color256 keys. (non-reentrant) '''
	if _D.kclrs:		return _D.kclrs
	R,G,B,DK,LT = seqmap(vars(_B), 'R G B DK LT')
	kclrs = [
		KClr(bb=0      , kk=S*'n  black  noir  '),
		KClr(bb=0|R|G|B, kk=S*'w  white        '),
		KClr(bb=0|R    , kk=S*'r  red          '),
		KClr(bb=0|R|G  , kk=S*'y  yellow       '),
		KClr(bb=0  |G  , kk=S*'g  green        '),
		KClr(bb=0  |G|B, kk=S*'c  cyan         '),
		KClr(bb=0    |B, kk=S*'b  blue         '),
		KClr(bb=0|R  |B, kk=S*'m  magenta      '),
		KClr(bb=0      , kk=S*'o  orange       '),
		KClr(bb=0      , kk=S*'v  violet       '),
		KClr(bb=0      , kk=S*'p  pink         '),
		KClr(bb=0      , kk=S*'s  silver       '),
		# KClr(bits=CB|R|G  , kk=S*'o  orange       '),
		# KClr(bits=CB|R  |B, kk=S*'v  violet       '),
		# KClr(bits=CB|R    , kk=S*'p  pink         '),
		# KClr(bits=CB|R|G|B, kk=S*'s  silver       '),
	]
	_D.kclrs = Obj(ij(zip([k.bb,*k.kk],gen(k)) for k in kclrs))
def gen_nn_clrs(kk_compose=_D.kk_nnclr):
	''' populate term color256 scales. (non-reentrant) '''
	if reused(gen_nn_clrs):		return _D.nnclrs
	
	# construct
	_D.nnclrs = NNClrs()
	gen_k_clrs()
	
	# populate all color scales:  => nnclrs
	clr_,clr_abc = _D.kclrs.black,_D.kclrs.white
	kk_prime = (2*_D.kk_prime)[:-1]
	for kk_abc in slide(kk_prime,3):
		# resolve inputs
		clr_a, clr_b, clr_c  = seqmap(_D.kclrs, kk_abc)
		clr_ab,clr_bc,clr_ac = seqmap(_D.kclrs, [clr_a|clr_b, clr_b|clr_c, clr_c|clr_a])
		
		# resolve keys; single prime incr
		k0_clrs = clr_,  clr_a,  clr_b,  clr_ab
		k1_clrs = clr_c, clr_ac, clr_bc, clr_abc
		nn_clrs = nn_color_interps(clr_c, k0_clrs, size=6)
		for k0,k1,n_clr in zip(k0_clrs,k1_clrs,nn_clrs):
			ch01,k01 = sof('{}{}',k0.ch,k1.ch), sof('{}_{}',k0.k,k1.k)
			_D.nnclrs[ch01] = _D.nnclrs[k01] = n_clr
		
		# resolve keys; double prime incr
		k0_clrs = clr_a, clr_ac
		k1_clrs = clr_b, clr_bc
		nn_clrs = nn_color_interps(-clr_a++clr_b, k0_clrs, size=6)
		for k0,k1,n_clr in zip(k0_clrs,k1_clrs,nn_clrs):
			ch01,k01 = sof('{}{}',k0.ch,k1.ch), sof('{}_{}',k0.k,k1.k)
			_D.nnclrs[ch01] = _D.nnclrs[k01] = n_clr
		
		# resolve keys; double prime incr
		k0_clrs = clr_,   clr_c
		k1_clrs = clr_ab, clr_abc
		nn_clrs = nn_color_interps(+clr_a++clr_b, k0_clrs, size=6)
		for k0,k1,n_clr in zip(k0_clrs,k1_clrs,nn_clrs):
			ch01,k01 = sof('{}{}',k0.ch,k1.ch), sof('{}_{}',k0.k,k1.k)
			_D.nnclrs[ch01] = _D.nnclrs[k01] = n_clr
	
	# todo: 
	#  nw = nn_color_interps('blue',['green'],size=6)
	#  nw_all = nn_color_interps('blue',['green'])
	
	kk_compose and cxpas(_D.nnclrs.compose,kk_compose)
	return _D.nnclrs

def join_nnclrs(nn_clrs, first=yes, fab=tuple, **ka):
	nn_clr = ij(nn_clrs)
	nn_clr = nn_clrs if not first else firsts(nn_clr)
	nn_clr = nn_clrs if not fab else fab(nn_clr,**ka)
	return nn_clr

def guage_perc(flt):
	enum_scl = EnumScale(_D.fg.rmbcg, upper=100., to_item=lambda val, fmtr: fmtr(val))
	out = enum_scl.resolve(flt)
	return out

def trunc_of_enum(flt, nums, *enums, nums_pos=None):
	inums = iter(nums)
	ind, level = next(enumerate(inums))
	for ind, num_high in inums:
		if flt < num_high: break
		level = num_high
	
	if enums or isint(num_pos):
		level = [e[ind] for e in enums]
		level.insert(num_pos, result)
	return level
def _iindex(flts, nums, enum=None, flt_pos=False, num_pos=False, enum_fab=None):
	enums = []
	for flt in flts:
		for ind, num_low in enumerate(nums, start=0):
			if flt < num_low: break
		
		if enums or type(num_pos) is int:
			level = [e[ind] for e in enums]
			level.insert(num_pos, num_low)
		elif flt_pos:
			level = flt
		elif num_pos:
			level = num_low
		elif enum:
			level = enum[ind]
		
		if enum_fab:
			level = enum_fab[ind](level)
		yield level

def ifloor_of(lvls=None,span=None,step=None,start=0,wrap=off,to_num=None,to_val=None,to_vals=None,at=-1,fabs=None,fab=None,vals=miss):
	''' digitize vals or convert from continuous flts to discrete nums with optional translation into
		output transforms are applied in arg order. 
		:param lvls: 
		:param span: 
		:param step: 
		:param start: 
		:param wrap:    
		
		:param to_num:  transform applied to all input vals to get nums
		:param to_val:  transform applied to all output vals
		:param to_vals: transforms for each level applied to all output vals at that level
		:param at:      transform which selects output from the nth,lvl,val tuple result
		:param fabs:    transform applied to each output iterable sub value
		:param fab:     transform applied to each output
		
		:param vals:    vals to be floored
	'''
	# resolve inputs
	assert not (step and span and lvls)
	if isiter(lvls):
		lvls,to_vals = (
			zip(sorted(lvls.items()))		if isdict(lvls) else
			zip(sorted(zip(lvls,to_vals)))	if to_vals else
			(sorted(lvls),None) )
	else:
		assert step or (steps:=len(lvls or to_vals or ()))
		span = span or lvls or steps
		step = step or steps-1 and span/(steps-1)
	
	# eval each   # todo: is it neccesary to switch to miss and allow None as valid argument
	def ifloor_steps_of(nums=None,vals=None,numvals=None):
		numvals = numvals or (nums and vals) and zip(nums,vals) or dupls(nums or vals,2)
		for num,val in numvals:
			nth = max(0, step and int((num - start) / step))
			nth = min(nth, steps-1) if not wrap else nth % steps
			lvl = nth * step + start
			yield nth,lvl,num,val
	def ifloor_lvls_of(nums=None,vals=None,numvals=None):
		numvals = numvals or (nums and vals) and zip(nums,vals) or dupls(nums or vals,2)
		for num,val in numvals:
			for nth,lvl in enumerate(lvls):
				if num < lvl: break	# stop before first lvl above
			yield nth,lvl,num,val
	def fab_ifloor_of(nums=None,vals=None,numvals=None):
		nums,vals = (vals is None and nums is not None) and (None,nums) or (nums,vals)
		nums,vals,is_coll = iscoll(vals) and (nums,vals,yes) or (isnone(nums,orput=(nums,)),(vals,),no)
		pa_vv = (nums,vals,numvals) if not to_num else (None,None,((to_num(val),val) for val in vals))
		
		vv = ifloor_vv(*pa_vv)
		vv = vv if not to_val else ((nth,lvl,num,to_val(val)) for nth,lvl,num,val in vv)
		vv = vv if not to_vals else ((nth,lvl,to_vals and to_vals[nth],num,to_vals[nth](val)) for nth,lvl,num,val in vv)
		vv = vv if at is None else (v_out[at] for v_out in vv)
		vv = vv if not fabs else map(fabs,vv)
		vv = vv if not fab else fab(vv)
		vv = vv if is_coll else first(vv)
		return vv
	
	# resolve output
	ifloor_vv = lvls and ifloor_lvls_of or ifloor_steps_of
	ifloor = (to_num or to_vals or fabs or fab or at is not None) and fab_ifloor_of or ifloor_vv
	out = vals is miss and ifloor or ifloor(vals)
	return out
def gauge_of(to_vals=None,span=None, vals=miss,**ka):	return ifloor_of(to_vals=to_vals, span=span, vals=vals, **ka)

reentry_clrs				= redef(reentrys, to_nth=lambda n: NNClrs._the.bcmr.fg[n*3-2])
reentry_logs				= redef(reentrys, to_nth=lambda n: NNClrs._the.bcmr.fg[n*3-2])
itrunc_of_enum = ifloor		= ifloor_of
#endregion

#region ops # todo: RELOCATE Implements
import operator as op
from copy import copy
from pyutils.defs.alldefs import *

class Implements:
	_real_name				= 'value'
	#region forms
	@classmethod
	def _get_real(cls, obj):
		return getattr(obj, cls._real_name) if isinstance(obj, cls) else obj
	def _iset_real(self, value):
		setattr(self, self._real_name, value)
		return self
	
	def __repr__(self, *pa):
		return repr(self._get_real(self))
	
	def __eq__(self, *pa):		return op.eq(self._get_real(self), *map(self._get_real, pa))
	def __lt__(self, *pa):		return op.lt(self._get_real(self), *map(self._get_real, pa))
	def __add__(self, *pa):		return copy(self)._iset_real(op.add(self._get_real(self), *map(self._get_real, pa)))
	def __sub__(self, *pa):		return copy(self)._iset_real(op.sub(self._get_real(self), *map(self._get_real, pa)))
	def __iadd__(self, *pa):	return self._iset_real(op.add(self._get_real(self), *map(self._get_real, pa)))
	def __isub__(self, *pa):	return self._iset_real(op.sub(self._get_real(self), *map(self._get_real, pa)))
	__radd__				= __add__
	#endregion
	...
#endregion

#region devcs
@dataclass
class Indexer:
	''' Reduces a value from a continuous range into a linear range of indices '''
	count: int				= 1
	upper: float			= 1.
	lower: float			= 0.
	offset: float			= 0.
	margin_top: float		= 1.
	margin_bot: float		= 0.
	to_item: object			= None
	#region utils
	def __post_init__(self):
		self.reset()
	def reset(self):
		assert 1.0 >= self.margin_top >= 0, f'margin_top of {self.margin_top} failed to satisfy 0 <= margin_top <= 1.'
		self.value_range = self.upper - self.lower
		self.margin_bot *= .999999
		self.rescale = (self.count+.000002-self.margin_top-self.margin_bot) / self.value_range
	def resolve(self, value):
		ind = into(self.lower, self.upper, value)
		ind = ind * self.rescale
		ind = min(int(ind + self.margin_bot + self.offset), self.count-1)
		return ind
	__call__				= resolve
	#endregion
	...
class Classifier(Indexer): pass

class Scale:
	#region utils
	def get(self, ind, upper=None):
		if isfloat(ind):
			ind = int(self.from_unit(ind))
		result = self[ind]
		return result
	def from_unit(self, value):
		result = value * (len(self)-1)
		return result
	def get_unit(self, value):
		ind = value / (len(self)-1.)
		result = self[int(ind)]
		return result
	#endregion
	...

@dataclass
class EnumScale(Indexer):
	enums: tuple			= ()
	#region forms
	def __init__(self, enums, *pa, count=None, **ka):
		self.enums = tuple(enums)
		super().__init__(len(self.enums), *pa, **ka)
	def __iter__(self):			yield from self.enums
	def __len__(self):			return len(self.enums)
	def __getitem__(self, ind):	return self.enums[ind]
	def resolve(self, value, upper=None):
		# ind = isintfab(ind, super().resolve, if_is=False)
		ind = super().resolve(value)
		result = enums = self.enums[ind],
		if self.to_item:
			to_item = self.to_item
			result = to_item(value, *enums)
		return result
	__call__				= resolve
	#endregion
	...
#endregion

#region devcs
class TColorKey:
	kk:list
	bb:int					= 0
	#region defs
	_n						= property(lambda self: keep0((_D.nclrs.get(k)  for k in self.kk),fill=None))
	_nd						= property(lambda self: keep0((_D.ndclrs.get(k) for k in self.kk),fill=None))
	_ch						= property(lambda self: keep0(self.kk, lambda k: len(k)==1))
	_k						= property(lambda self: keep0(self.kk, lambda k: len(k)!=1))
	#endregion
	#region forms
	def __init__(self,*pa,**ka):
		''' container for ...
			:var n:  
			:var kk: 
			:var bb: 
			:var k:  
			:var nd: color offset
			:var ch: 
		'''
		annoinit(self,*pa,**ka)
		self.__dict__.update(kk=tuple(self.kk), k=self._k, n=self._n, nd=self._nd, ch=self._ch)
		...
	def __repr__(self):			return sj('|', self.kk, fab='>{}')
	def __pos__(self):			return self.nd
	def __neg__(self):			return -self.nd
	def __or__(self,val):		return val|self.bb
	def __and__(self,val):		return val&self.bb
	#endregion
	...

class TColorLevel:
	#region forms
	def __init__(self,n,*pa,**ka):
		''' container for all elements corresponding to given color level *n*
			:var n:      color level
			:var fg:     foreground color repr
			:var bg:     background color repr
			:var fg_log: foreground color repr
			:var bg_log: background color repr
		'''
		self.n = int(n)
		self.fg = ColrFore(_S.fmt_xx_fg(self.n))
		self.bg = ColrBack(_S.fmt_xx_bg(self.n))
		self.fg_log,self.bg_log = self.fg+print, self.bg+print
	def __repr__(self):			return f'{self.n} ...'
	def __index__(self):		return self.n
	def __int__(self):			return self.n
	#endregion
	...
class TColorScale:
	nn:list
	key:str					= None
	cch:str					= None
	#region defs
	fmt_xx_bg				= F/'{}{}'//(_S.fmt_xx_bg/'{0}',' {0:>3} ')
	fmt_xx_fg				= F/'{}{}'//(_S.fmt_xx_fg/'{0}',' {0:>3} ')
	s_kk					= property(lambda self: sof('>{:>8} {:32.32s}',self.cch,self.key))
	s_bg					= property(lambda self: sj('',afx(whitel.code,icxpas(self.fmt_xx_bg,self.nn),reset.code)))
	s_fg					= property(lambda self: sj('',afx(whitel.code,icxpas(self.fmt_xx_fg,self.nn),reset.code)))
	s_all					= property(lambda self: sof('{}{}\n{s0:42}{}', self.s_kk,self.s_bg,self.s_fg,s0=''))
	_get_attrs				= S*'nn  fg  bg'
	#endregion
	#region forms
	def __init__(self,*pa,**ka):
		annoinit(self,*pa,**ka)
		self.__dict__.update(nn=tuple(self.nn))
		
		cch,key = self.cch, self.key or self.cch
		if key:
			j_key = _S.j_kk in key and _S.j_kk or _S.j_cch
			cch,key = j_key and (cch,key) or (cch or key,None)
			kkclr = tseqmap(_D.kclrs, key and key.split(_S.j_kk) or list(cch))
			self.cch,self.key = self.cch or sj(_S.j_cch, IKK.ch(kkclr)), key or sj(_S.j_kk, IKK.k(kkclr))
		self.prepare()  # FIXME: dont create all of this at once.  use lazy or something 
		...
	def __getitem__(self,key):		return self.nn[key]
	def __repr__(self):				return self.s_kk
	def __eq__(self,val):			return type(self) == type(val) and (self.cch or self.key) == (val.cch or val.key)
	def __hash__(self):				return hash(self.cch or self.key)
	@classmethod
	def of(cls,obj=miss,*pa,**ka):
		if istype(obj,cls):			return obj
		
		pa = obj is miss and pa or (obj,)+pa
		obj = cls(*pa,**ka)
		return obj
	def prepare(self):
		self.fg = cxpas((ColrFore,_S.fmt_xx_fg), self.nn)
		self.bg = cxpas((ColrBack,_S.fmt_xx_bg), self.nn)
		...
	def get(self,idx=None,at=None,fabs=None,fab=tuple):
		attrs = at is None and self._get_attrs or self._get_attrs[at]
		out = isstr(attrs) and self.__dict__[attrs] or [self.__dict__[attr] for attr in attrs]
		out = out if idx is not None else out[idx]
		out = out if not fabs else map(fabs,out)
		out = out if not fab else fab(out)
		return out
	#endregion
	...
class TColorScales(Vars):
	#region defs
	ss						= property(lambda self: (tclr.s_all for tclr in firsts(self.values())))
	s						= property(lambda self: sj('\n', self.ss))
	_the					= lazy(lambda _: gen_nn_clrs())
	#endregion
	#region forms
	def __contains__(self,key):		return super().__contains__(key)
	def __getattr__(self,attr):
		try:
			return self.__dict__[attr]
		except Exception as exc:
			raise AttributeError(exc)
	def __missing__(self,key):		return self.compose(key)
	def __setitem__(self,key,val,**ka):
		nnclr = NNClr.of(val,key=key,**ka)
		nnclr.cch and super().__setitem__(nnclr.cch,nnclr)
		nnclr.key and super().__setitem__(nnclr.key,nnclr)
		return nnclr
	def compose(self,key,store=yes):
		''' create composite scale from key components '''
		nnclr = self.get(key)
		if nnclr:	return nnclr
		
		# resolve inputs:  => kk
		j_key = _S.j_kk in key and _S.j_kk or _S.j_cch
		k2s = tslide(
			list(key)			if isntstr(key) else
			key.split(j_key)	if j_key else
			key, fabs=j_key.join )
		kk = [(k2 in self and (k2,1)) or (k2[::-1] in self and (k2[::-1],-1)) or throw(KeyError(k2)) for k2 in k2s]
		
		# resolve output:  => nnclr
		nn_clrs = [self[k][::step] for k,step in kk]
		nnclr = join_nnclrs(nn_clrs, fab=NNClr,key=key)
		store and self.update([[nnclr.key,nnclr],[nnclr.cch,nnclr]])
		return nnclr
	#endregion
	...

KClr,NClr,NNClr,NNClrs		= TColorKey, TColorLevel, TColorScale, TColorScales
nclr_of						= redef(colorkey_of, ev=NClr)
#endregion

#region grade devcs todo: obsolete; refactor & remove
GradeRef					= namedtuple('GradeRef', 'grade scale')
class Grade(float):
	#region defs
	scale					= None
	value					= property(float)
	#endregion
	#region forms
	def __new__(cls, value:float, name:str, scale:GradeScale=None):
		obj = float.__new__(cls, value)
		obj.name = name
		if scale:
			obj.scale = scale
		return obj
	def __repr__(self):
		return f'{self.name}:{float(self)}'
	def __hash__(self):
		return hash(float(self))
	def __index__(self):
		return int(self)
	def resolve_style(self, *pa, scale=None, **ka):
		self.scale = self.scale or Defs.grade_scale_map[self.name]
		scale = scale or self.scale
		result = scale.get_style(self, *pa, **ka)
		return result
	#endregion
	...

class GradeScale(Scale, tuple):
	''' translates a value within a continuous range into discrete value of a finite set '''
	#region defs
	styles					= None
	#endregion
	#region forms
	def __new__(cls, names:list, upper:float=1., styles:EnumScale=None):
		names = lss(names)
		assert len(names) >= 2, f'Not enough names: {names}'
		max_ind = len(names)
		# grades = tuple.__new__(cls, (Grade(ind/max_ind, name) for ind, name in enumerate(names)))
		grades = tuple.__new__(cls, (Grade(ind, name) for ind, name in enumerate(names)))
		[setattr(grade, 'scale', grades) for grade in grades]
		if styles:
			grades.styles = styles
		return grades
	# def resolve(self, value, palette=None):
	# 	self.scale = self.scale or Defs.scale_palette[palette][len(self)]
	# 	return self.scale.get_unit(value)
	def get_style(self, value, palette=None):
		if None is palette is self.styles:
			self.styles = self.styles or Defs.scale_palette[palette][len(self)]
		styles = self.styles if None is palette else Defs.scale_palette[palette][len(self)]
		return styles.get(value)
	__call__				= Scale.get
	#endregion
	...

@dataclass(repr=False)
class GradeTotal(Implements):
	grade:object
	total:int				= 0
	#region defs
	_real_name				= 'total'
	#endregion
	#region forms
	def __repr__(self):
		return f'<grade={self.grade}, total={self.total}>'
	def __hash__(self):
		return hash(self.grade)
	def __eq__(self, other):
		return self.grade == other.grade
	#endregion
	...

Degree						= namedtuple('Degree', 'lower upper')
@dataclass
class Degrees:
	''' operation to combine input states into a composite state '''
	uppers: tuple			# seq of ascending upper bounds; each consecutive pair makes a degree
	grade_totals: SetMap	= field(default_factory=lambda: SetMap(to_key=KK.grade, missing=GradeTotal))
	#region defs
	lowers					= property(lambda self: affix(0., self.uppers))  # todo: islice to remove the last item
	pairs					= property(lambda self: zip(self.lowers, self.uppers))
	degrees					= property(lambda self: map(Degree, self.lowers, self.uppers))
	totals					= property(lambda self: self.quantify())
	#endregion
	#region utils
	def count_grades(self, states):
		self.clear()
		for state in states:
			for grade_total in state.grade_totals:
				self.grade_totals[grade_total.grade] += grade_total
	def quantify(self):
		''' calculate totals within each degree from known grade_totals '''
		grade_totals = sorted(self.grade_totals)
		grade_totals = iter(grade_totals)
		grade_total = next(grade_totals, None)
		
		for degree in self.degrees:
			total = 0
			while grade_total is not None and grade_total.grade <= degree.upper:
				total += grade_total.total
				grade_total = next(grade_totals, None)
			yield (degree, total)
	__iter__				= quantify
	#endregion
	...
_D.initialize()
#endregion

#region tests
def test_nn_color(log=print):
	s_out = _D.nnclrs.s		# lazy define identical to below; supports being called 1st or 2nd or never 
	s_out = NNClrs._the.s	# lazy define identical to above; supports being called 1st or 2nd or never
	log and log(s_out)
	return s_out
def test_gauge(simple=yes, kk_nnclr = S*'nbr  nbcmr  nbmr  nbcg  bcmr  gbr'):
	import math
	
	# resolve inputs
	to_val = lambda val: whitel(f'{val:^7.3f}')
	ka = Vars(span=2, fab=list, to_val=to_val)|(simple and dict(fabs=F/'{}') or dict(at=K.all,fabs=F/'{0[0]:2} {0[1]:6.2f} {0[2]}'))
	nn = [ka.span*1.02 * (((math.cos(math.pi*n)*-.5)+.5)**1.1) for n in inum(.03125,0,2.001)]
	
	# resolve output
	print('with wrap OFF:')
	nn_heats = [gauge_of(_D.nnclrs[k_nnclr].bg,**ka, vals=nn[::2]) for k_nnclr in kk_nnclr]
	cxpas(print, icxpas(' '.join, zip(*nn_heats)))
	
	# resolve output
	print('\nwith wrap ON:')
	ka.span = 1
	nn_heats = [gauge_of(_D.nnclrs[k_nnclr].bg,**ka, vals=nn[:int(len(nn)/2)+1], wrap=on) for k_nnclr in kk_nnclr]
	cxpas(print, icxpas(' '.join, zip(*nn_heats)))
	...
def test_gauge_perc_old():
	enums = [*map(guage_perc, range(0,101,10))]
	[*map(print, enums)]

if __name__=='__main__':
	# test_nn_color()
	test_gauge()
	...
#endregion

#region notes
''' 
todo: consider letter associations for corresponding ansi colors below
dim			bright

### current
r-ed		p-ink
y-ellow		f-lax
g-reen		l-ime
c-yan		t-urquoise
b-lue		i-ce
m-agenta	h-eather
s-ilver		w-hite
n-oir		d-ark

o-range		a-mber
v-iolet

'''
#endregion
