''' Pykit-Quiz

Run regression tests for subject specific python snippets

Usage:
  quiz_<<scope>>.py [issue] [<sbj>...]

Options:
  -h --help                      Show this screen.
  -v --version                   Show version.
  
  # processing modes
  -i --issue <sbj>               administer quiz on a given subject|sbj then quit. [default: '']
  
  # show quiz info
  -s --show                      show identified content of quiz

'''
import os
import sys
import atexit
import re
import yaml
from dataclasses import dataclass
from docopt import docopt,Dict

from pyutils.defs.rsrc import p_loc_pykit
from pyutils.utilities.iterutils import *
from pyutils.utilities.callutils import *
from pyutils.utilities.alyzutils import *
from pyutils.utilities.clsutils import theclass
from pyutils.utilities.strutils import *
from pyutils.utilities.pathutils import *
from pyutils.structures.maps import *
from pyutils.structures.nums import *
from pyutils.structures.sets import *
from pyutils.output.colorize import *
from pyutils.output.joins import *
from pyutils.output.logs import *
from pyutils.patterns.props import lazy
from pyutils.patterns.event import Publisher
from pyutils.patterns.capsules import StasisViewCapsule

#region quiz defs
# todo: complete concepts summaries so that module naming scheme can become consistent
''' Quiz Concepts
quiz|qz:		top level of related scopes
scope:			nested code
subject|sbj:	named scope(s) of code with which could be matched by Quiz.incl 
section|sct:	parts of subject delimited by ':'
focus:			the current quiz subject based on most recent call to quiz(topic='a:subject')
topic:			current subject matching given Quiz.incl
'''

''' Quiz Concepts OLD
quiz|qz:		...
scope:			...
section|sct:	delimited by ':'
subject|sbj:	...
topic:			the current quiz subject based on most recent call to quiz(topic='a:subject')
version:		...
'''
@clscolls(no)
class DefsQuiz:
	#region scopes
	kk_params				= {'params', *ss('yaml  py  eval', 'params_{}')}
	class Args:
		ss_task_halt		=      show, help, version, = (
							  tss('show  help  version  ','py --{}') )
		ss_task				=      issue, *_ = (
							  tss('issue  ','py --{}') + ss_task_halt )
	class Keys:
		kk					=    pkg, quiz, sbj, dep, src,  = (
							  S*'pkg  quiz  sbj  dep  src  ' )	# todo: obsolete
		pfxs				= S*'>>>  >>    >    !    !    '	# todo: obsolete
		kk_evt				=    on_setup, on_begin, on_finish, on_next, on_topic, unset,  = (
							  S*'on_setup  on_begin  on_finish  on_next  on_topic  UNSET  ')
		pfx_at				= mapvars(kk,pfxs)
	class Strs:
		''' character classes quiz uses to parse subjects and subjects specifiers '''
		# element symbols
		kk					= S*'pkg  quiz  sbj  sect  dep  src  explicit  template  '
		pfxs = cch 			=    pkg, quiz, sbj, sect, dep, src, explicit, template, = (
							  S*'>>>  >>    >    :     !    !    =         @         ' )
		k_at = k 			= mapvars(kk,kk)
		pfx_at = pfx		= mapvars(kk,cch)
		
		# topic char classes
		pfx_explicit		= pfx.explicit		# prefix to explicit sections which are ignored by wildcard specifier
		pfx_template		= '!'				# prefix to template sections
		pfx_template		= pfx.template		# prefix to template sections
		pfxs				= pfx_explicit+pfx_template
		
		# topic specifier char classes
		subj_wildcard		= '*'				# match on any section except explicit sections
		subj_or				= '|'				# match on another section at current scope
		subj_scopedown		= sect				# separator between parent-to-sub section scopes
		topic_or			= '+'				# match on another topic. resets to root scope.
		spec_ops			= '.*:|+'			# topics specifier operations
		j_vers				= '.'
		valid_subj			= r'\w\d '			# separator between parent and sub scope
		valid_topic_spec	= valid_subj+spec_ops+pfxs
		
		# rx patterns
		fmt_rxnest			= '{next}(:{nest})?'
		rx_fill				= r'[^{}].*'.format(pfx_explicit)  # todo
		rx_any				= '.*'
		rx_sect				= '[\w ]*'			# explicit rx char class for sect of subject
		rxcls_sect			= '$wh*'			# implicit rx char class for sect of subject
	class Vals:
		vv_modes			= normal, statis, = (
							  0x00,   0x01    )
		evts_stasis			= lazy(lambda _: {KKQZ.on_begin, KKQZ.on_finish})
		notify_once			= lazy(lambda _: {KKQZ.on_setup, KKQZ.on_begin, KKQZ.on_finish})
	class Paths:
		params_yaml			= p_loc_pykit/'quiz/quiz-params.yaml'
		params_py			= p_loc_pykit/'quiz/quiz-params.py'
	class Ctls:
		pp_quiz_hooks		= p_loc_pykit/'quiz/hooks/*_hooks.py'
		on_quiz_cmd			= None
		process_task		= None
		fmt_incl_default	= None
		incls_default		= None
		_doc				= __doc__
	class docopt:
		rx_word				= r'\w+'
		rrx					= RXAt(	 'dash  flag     arg         ',
									r'\-+   [\w\-]+  <\w+>|[A-Z]+', fmt='{}?{}|{}')  # flags are prefixed dash(es)
		kgrp_at_flag		= FullVars('arg',{False:'off',True:'on'})
		
		key_of_opt			= lambda opt,rx_word=r'\w+',j='_': j.join(keep(RX.findall(rx_word,opt))).lower()
		resolve_flag		= lambda flag_dash,flag_key: flag_dash != flag_key and True or flag_dash
		
	AA,KK,SS,VV,PP			= Args,Keys,Strs,Vals,Paths
	#endregion
	...

DDQZ = _D					= DefsQuiz
_dd=AAQZ,KKQZ,SSQZ,VVQZ,PPQZ= DDQZ.AA,DDQZ.KK,DDQZ.SS,DDQZ.VV,DDQZ.PP
_A,_K,_S,_V,_P				= _dd
_i							= _D.i
#endregion

#region quiz utils
_atpfx						= 0

def quiz_params_of(params=None,params_yamls=(PPQZ.params_yaml,),params_py=None,params_eval=None):
	''' get decoded & merged *params* from various sources
		:param params: dict containing params
		:param params_yamls: yaml file path containing params
		:param params_py: python file path containing params
		:param params_eval: str of python code containing params
		
		Notes:
		- *params* are user configured external variables provided to quiz executions
		- accessible via quiz.params
		todo: params should update dynamically
		todo: should be configurable to apply specific variables to specific subjects
		todo: would be nice to execute a subject multiple times per each set of variables
	'''
	params_out = FullVars(None,params or map0)
	for p_yaml in ss(params_yamls):
		params_out.update(yaml.safe_load(Path(p_yaml).open()) or ())
	params_py	and params_out.update(exec(open(params_py).read()))
	params_eval	and params_out.update(eval(params_eval))
	return params_out

def discover_quiz_topics(glob):
	''' identify all available quizzes by topic in the given *glob* file structure.
		todo
	'''
def focus_of_topics(topics,vers_at_topic=None,j_vers=_S.j_vers):
	''' determine the focus of given quiz topics. exclude all templated topics unless there is no other focus. 	'''
	j_sects = _S.subj_scopedown
	topics_at = Vars(explicit=[],template=[],other=[])
	topics_at.update({_S.pfx_explicit:topics_at.explicit,_S.pfx_template:topics_at.template})
	for topic in topics:
		# resolve version:  sects => vers,s_topic_vers
		sects = *_,sect = topic.split(_S.subj_scopedown)
		vers = keepfirst((vers_at_topic.get(j_sects.join(sects[:-idx or None])) for idx in range(len(sects))),fill=())
		s_topic_vers = j_vers.join([topic,*keep(vers)])
		
		# resolve topics coll and append s_topic_vers
		pfx = sect[_atpfx]
		topics = topics_at.get(pfx,topics_at.other)
		topics.append(s_topic_vers)
	# resolve output:  => focus
	focus = _S.topic_or.join(topics_at.explicit or topics_at.other or topics_at.template)
	return focus

# fixme: rename; replace 'tags' with 'subject'
def sctxtags_of(tags, tags_prev=None, sep:str=_S.subj_scopedown, join:[str,bool]=True):
	tags_intor = [t if isntstr(t) else t.split(sep) for t in (tags, tags_prev)]
	tags = stack_alt(tags_intor)
	tags = tags if not join else ((isstr(join) and join.join) or (istrue(join) and sep.join) or join)(tags)
	return tags
def ctxtags_of(tags_to, tags_in=(), sep:str=_S.subj_scopedown, join:[str,bool]=tuple):
	join = join.join if isstr(join) else sep.join if isnorm(join) else join
	
	tags = [t if isntstr(t) else t.split(sep) for t in (tags_to, tags_in)]
	tags = tuple(stack_alt(tags))
	tags = tags if not join else join(tags)
	return tags

# todo: replace with iterutils.istacked
def stack_alt(seqs, expand=None, fill=None, cond=bool):
	ends = genof(nothing)
	iseqs = (
		lcallpas(ijoin, seqs, par=(ends,))		if expand is True else
		seqs									if expand is False else
		[seqs[0], *icallpas(ijoin, seqs[1:], par=(ends,))]  )
	
	# ...
	for iseq in zip(*iseqs):
		item_out, matched = nothing, False
		
		for item in iseq:
			item_out = item
			if item is not nothing and cond(item):
				break
		if item_out is nothing: break
		yield item_out

def rx_of_nest(rx, *rx_pa, fmt_rxnest=_S.fmt_rxnest, sep=None):
	# resolve inputs
	fmt_rxnest = Fmt(fmt_rxnest)
	rx_pal,rx_pam,rx_par = sep and tss(rx,sep=':') or rx_pa,(),()
	rx_pam = rx_pal and rx_par and (':([\w ]+:)*',) or ()
	rxs = *rx_pal,*rx_pam,*rx_par
	
	# resolve output
	rx_nest,*rxs = rxs[::-1]
	for rx_next in rxs:
		rx_nest = fmt_rxnest(next=rx_next,nest=rx_nest)
	return rx_nest
def rxsbj_of_rx(*pa, sep_sect=_S.pfx.sect, rx_pfx=True, rx_end=None, as_tmpl_rxcls=True, **ka):
	rx_pfx = isstr(rx_pfx,orput=rx_pfx and '^' or '')
	rx_end = isstr(rx_end,orput=as_tmpl_rxcls and '$$' or '$')
	rx_sbj = rx_pfx + rx_of_nest(*pa,**ka,sep=sep_sect) + rx_end
	rx_sbj = rx_sbj if not as_tmpl_rxcls else rx_of_tmplrxcls(rx_sbj, rxcls_getr=istrue(as_tmpl_rxcls,put=None))
	return rx_sbj
def rxsbjs_of_rrx(rrx, sep_sect=_S.pfx.sect, sep_sbj=_S.pfx.sbj, as_tmpl_rxcls=True, **ka):
	'''
		:param rrx: regular expressions to merge (regex sequence)
		:param sep: signals that given *rrx* is a string that need to be split with *sep* to get input rx seq.
		:param as_tmpl_rxcls: reformat result with given template regex class getter. use default rxcls when given True
	'''
	# todo: need to optimize merging of regular expressions.
	#   curr naive rrx merge:  'x:y:a $|x:y:b $|x:z:c $|x:z:d $'
	#   ideal rrx merge:       'x(:y(:a |:b )|:z(:c |:d ))$'
	# todo: strip all except subjects
	# resolve inputs
	ka = dict(ka,sep_sect=sep_sect,as_tmpl_rxcls=False,rx_end=as_tmpl_rxcls and '$$' or '$')
	
	# resolve output
	rrx_out = tj(cxpas(tss, collany(rrx),sep=sep_sbj))
	rx_out = '|'.join(rxsbj_of_rx(rx,**ka) for rx in rrx_out)
	rx_out = rx_out if not as_tmpl_rxcls else rx_of_tmplrxcls(rx_out, rxcls_getr=istrue(as_tmpl_rxcls,put=None))
	_S.rx_any in rx_out and pyellowl('QUIZ - WARNING:\n'
		f'  greedy pattern matching in {rx_out!r} may activate on more subjects than expected.\n'
		f'  consider replaceing {_S.rx_any!r} with {_S.rxcls_sect!r} or its equivalent {_S.rx_sect!r} ')
	return rx_out
def rxquiz_of_rrx(rrx, *pa, incls=None, fmt_incl=None, **ka):
	# resolve inputs
	incls = tss(incls or _D.Ctls.incls_default or ())
	fmt_incl = fmt_incl or _D.Ctls.fmt_incl_default or _S.pfx_at.sbj.join(afx('{}',incls))
	
	# resolve output
	rrx_out = fmt_incl.format(rrx)
	rx_out = rxsbjs_of_rrx(rrx_out, *pa, **ka)
	return rx_out

def quiz(sbj=None,*_pa_quiz,name='quiz',ka_sbj=map0,pa_quiz=(),ka_quiz=map0,**_ka_quiz):
	''' get or create default Quiz then query as quiz.next(*,**). simplify worlflow by dropping assignment prereq. '''
	# resolve inputs
	pa_quiz,ka_quiz = pa_quiz or _pa_quiz, ka_quiz or _ka_quiz
	ka_quiz = bump_fqka(ka_quiz)
	
	# resolve quiz context
	result = quiz = isnttype(Quiz._the,Quiz) and Quiz(*pa_quiz,**ka_quiz) or Quiz._the
	name and globals().__setitem__(name,quiz)
	
	# resolve is on_topic  (should this code block be executed?)
	if sbj:
		result = quiz(sbj,**ka_sbj)
	return result

# rxquiz_of_rx,rxquiz_of_rrx	= rxsbj_of_rx,rxsbjs_of_rrx
get_quiz					= quiz
#endregion

#region quiz cmd utils
def groups_of_docopts(opt_at, rrx=_D.docopt.rrx, to_key=_D.docopt.key_of_opt,resolve_flag=_D.docopt.resolve_flag):
	''' simplify docopts output
		1. remove non-word symbols normalizing opt as attribute names
		2. group opts into sub dicts: on-flags, off-flags or args
	'''
	# grps = Vars(all=Vars(),on=Vars(),off=Vars(),arg=Vars())
	grps = Vars(all=Vars(),on=Vars(),off=Vars(),arg=Vars())
	for opt in opt_at:
		opt_subs = RX.search(rrx.rx,opt,key=dict,fab=Vars)
		k_opt = to_key(opt)
		if k_opt in grps.all: continue  # already handles
		elif opt_subs.arg is not None:
			grps.all[k_opt] = grps.arg[k_opt] = opt_at[opt]
		elif opt_subs.flag is not None:
			flag = (
				resolve_flag(opt_at[opt],opt_at[k_opt])	if opt!=k_opt and opt in opt_at and k_opt in opt_at else  # conflict
				opt_at[opt]  )
			k_grp = _D.docopt.kgrp_at_flag[flag]
			grps.all[k_opt] = grps[k_grp][k_opt] = flag
		else:
			throw(f'Unrecognized opt: {opt!r}  Looking for rx: {rrx.rx!r}')
	return grps
def opts_of_quiz_cmd(cmd=None,cmd_args=None,doc=None, version='v0.0.0',groups=False,log=False):
	# resolve inputs
	if cmd_args:
		p_py = ''
	else:
		cmd = cmd or sys.argv
		p_py,*cmd_args = isstr(cmd,fab=lss,orput=cmd)
	
	# resolve output
	cmd_opts = Dict(docopt(doc or _D.Ctls._doc, cmd_args, version=version,help=False), p_py=p_py)
	log and log(cmd_opts)
	cmd_opts = groups_of_docopts(cmd_opts)
	# cmd_opts.args = cmd_args
	cmd_opts.update(_cmd=cmd,args=cmd_args)
	cmd_opts = cmd_opts if groups else cmd_opts.all
	return cmd_opts

def on_quiz_cmd(*pa,cmd_opts=None,**ka):
	# resolve inputs
	cmd_opts = cmd_opts or opts_of_quiz_cmd(*pa,**ka, groups=True)
	
	# enact command
	DDQZ.Ctls.process_task(cmd_opts)
	return cmd_opts

def halt_on_tasks(cmd_opts,halt_tasks='help  version  show'):
	''' simply return cmd_opts '''
	(halt,task),*_ = *tkeep(((cmd_opts.get(task),task) for task in ss(halt_tasks)),at=1),(no,no)  # cmd_opts.help or cmd_opts.version or cmd_opts.show
	halt and exit(f'DONE with {task!r}')
	return cmd_opts

def issue_hooks(pp_hooks=None,quiz=None,sep_hooks=','):
	# FIXME: MUST PREVENT REENTRY
	pp_hooks = pp_hooks or DDQZ.Ctls.pp_quiz_hooks
	pp_hooks_out = _i(like_pp(pp_hooks))
	assert pp_hooks_out
	for p_hook in pp_hooks_out:
		pycode_hook = compile(p_hook.read_text(),str(p_hook),'exec')
		exec(pycode_hook,quiz._frame.f_globals)

_D.Ctls.on_quiz_cmd					= on_quiz_cmd
_D.Ctls.process_task				= halt_on_tasks
#endregion

#region quiz devcs
class FinishQuiz(Exception): pass

class Topic(Sets):
	def __init__(self,incl=None,to_rx=rxquiz_of_rrx,**ka):
		self._incl = SJ.j_(ss(incl))
		incl = incl if not to_rx or not incl else to_rx(incl)
		super().__init__(incl=incl,**ka)
	def __repr__(self):					return f'{nameof(self)}({self._incl!r})'
	def contain(self, sects:str=''):
		found = True
		if found and self.excl:
			found = not re.search(self.excl, sects)
		if found and self.incl:
			found = re.search(self.incl, sects)
		return found

@thecls
class Quiz(Topic,Publisher):
	''' Facilitate regression tests on python snippets
		Features:
		- targeting execution context to be ran (akin to an .ipynb cell) is done via *incl* when given a regex str
		- emulates basic jupyter notebook operations within regular python files
		- interprets cli arguments; see help in __doc__ source header (or run command './quiz_<scope>.py --help')
		- ipython scripts can be used for bash commands and magics; just make use of .ipy extension
			Note: ipy CAN import py; py CANNOT import ipy
		- todo: generate a jupyter notebook (./quiz_<scope>.ipynb)
		- todo: support markdown detection
	'''
	#region defs
	_the = _curr			= FullVars(None)
	_the_publ				= Publisher()  # all Quiz instances emit events to both this publisher and itself
	_j_focus				= Joins(_S.topic_or,_S.subj_scopedown)
	_ms_kk_j				= 'topic vers ;.;'  # a splitable multistr
	SS = Strs				= SSQZ
	active_cells			= property(lambda self: self.incl)
	vals = vv				= property(lambda self: (getattr(self,k) for k in self.kk))
	qual = s_vv				= property(lambda self: self.j_vers.join(keep(self.vv)))
	focus					= property(lambda self: focus_of_topics(self.sbjs,self.vers_at,self.j_vers))
	s_focus					= property(lambda self: self._j_focus(self.focus))
	can_setup				= property(lambda self: _K.on_setup in self.notified and _K.on_begin not in self.notified)
	can_begin				= property(lambda self: _K.on_begin in self.notified and _K.on_next not in self.notified)
	can_finish				= property(lambda self: _K.on_finish in self.notified)
	can_reissue				= property(lambda self: self.n_issues)
	subject					= property(lambda self: self)
	
	_kk_sig					= S*'p_quiz  _incl  focus  topics  sbjs  cmd'
	sig						= property(lambda self: intomap(vars(self)|dict(cmd=self.cmd_opts._cmd), self._kk_sig))
	s_sig					= property(lambda self: yaml.dump(self.sig))
	#endregion
	#region forms
	def __init__(self, incl=None, to_rx=rxquiz_of_rrx, cmd_args=None, **ka):
		''' ctor
			:param incl: subject inclusions regex|rx pattern
			:param to_rx: regex converter for incl & excl
			:param cmd_args: command line string to parse
		'''
		# perform base ctors
		Publisher.__init__(self)
		Topic.__init__(self,incl=incl,to_rx=to_rx)
		
		# resolve inputs:  sys.argv,incl,excl => ...
		self._frame = bump_fqka(ka,2, fquery=fframe)
		self.p_quiz = self._frame.f_locals['__file__']
		cmd_args = cmd_args and cmd_args.format(quiz=self.p_quiz,sbjs=incl)
		self.cmd_opts = self.opts_cli = opts_of_quiz_cmd(cmd_args, groups=True)
		self.p_quiz = os.path.realpath(self.p_quiz or self.cmd_opts.all.p_py)
		self.params = quiz_params_of(**{k:ka.pop(k) for k in [*ka] if k in _D.kk_params})
		
		# init members
		self.__dict__.update(ka)
		self.sbj = _K.unset
		self.vers = ''
		self.tags = ''
		self.s_vers = ''
		self.kk,[self.j_vers] = sss_of_zs(self._ms_kk_j)
		self.sbjs = []
		self.topics = []
		self.mode = _V.normal
		self.on_topic = False
		self.vers_at = dict(_j_vers=self.j_vers)
		self.notified = set()
		self.modes = FullVars(None)
		self._in_statis = StasisViewCapsule(self)						# while in stasis prevent modification to self
		
		# setup for events
		atexit.register(self.on_finish)
		self.notify(_K.on_setup)
	def __init_after__(self,*pa,**ka):
		self.sbj = KKQZ.unset
	
	def __getitem__(self, key):		return getattr(self,key)
	def keys(self):					yield from self.kk
	#endregion
	#region event utils
	def notify(self,event,*pa,**ka):
		if event not in (_V.notify_once & self.notified):
			self.sbj = self.sbj == _K.unset and '='+event or self.sbj
			self.notified.add(event)
			mode = event in _V.evts_stasis and _V.statis
			self.mode |= mode
			super().notify(event,self,*pa,**ka)							# notify direct subscriber hooks
			self.__class__._the_publ.notify(event,self,*pa,**ka)		# notify indiscriminate subscriber hooks
			self.mode &= ~mode
	def on_begin(self):			self.notify(_K.on_begin)
	def on_finish(self):		self.notify(_K.on_finish)
	def finish(self):			raise FinishQuiz()
	
	def next(self, sects:str=None,vers='',ver='',fmt_vers=None):
		# update mode & event hooks
		self.sbj == _K.unset and self.notify(_K.on_begin)
		self = not self.mode & _V.statis and self or self._in_statis	# while in stasis prevent modification to self
		
		# resolve current topic:  => sects,topic
		_sects,_sects_prev = sects,self.tags
		self.sects = sects = self.tags = ctxtags_of(sects, self.tags)
		self.sbj = _S.subj_scopedown.join(sects)
		topic_up = _S.subj_scopedown.join(sects[:-1])
		
		# resolve current version:  => vers
		assert not vers or not ver
		if ver:
			vers_up = self.vers_at.get(topic_up, ())
			vers = *vers_up,ver											# append given *ver* to parent section *vers*
		if vers:
			self.vers_at[self.sbj] = tss(vers)							# when given record *vers* of current topic (or sects)
		
		# resolve output:  => on_topic
		self.on_topic = self.sbj in self.subject								# determine whether to execute this quiz section
		self.on_topic and self.topics.append(self.sbj)
		self.sbjs.append(self.sbj)
		self.notify(_K.on_next, sbj=self.sbj, on_topic=self.on_topic)
		self.on_topic and self.notify(_K.on_topic, sbj=self.sbj)
		return self.on_topic
	
	__call__ = __next__		= next
	#endregion
	#region io utils
	def flush(self):			raise NotImplementedError()
	def commit(self):			raise NotImplementedError()
	def capture_output(self):	raise NotImplementedError()
	def compare_outputs(self):	raise NotImplementedError()
	#endregion
	...

Qz							= Quiz
#endregion

#region notes
'''
on_setup
on_begin: quiz begins on first call to next()
on_finish: quiz finished
on_next
on_topic

'''
#endregion
