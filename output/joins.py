from dataclasses import dataclass

from pyutils.utilities.alyzutils import *
from pyutils.utilities.iterutils import *
from pyutils.utilities.maputils import subclsof
from pyutils.patterns.props import *
from pyutils.patterns.accessors import *
from pyutils.structures.calls import *
from pyutils.structures.strs import *
from pyutils.structures.maps import *

#region join devcs
class DefsJoins:
	#region coll defs
	i					= tuple
	sjj_kkvnl_at		= dict()
	
	#endregion
	...
	
DDJ = DD				= DefsJoins
_i						= DDJ.i
#endregion

#region join devcs
class SJoinStep(FabStep):
	''' ... '''
	def __post_init__(self):
		self.fab = isstr(self.fab,fab=SJoin)
		super().__post_init__()
class SJoinSteps(FabSteps, step_cls=SJoinStep): 	''' ... '''
class SJoinWalk(FabWalk, step_cls=SJoinStep): 		''' ... '''

isj_fabs_of				= redef(ifabs_of_icls, cls_step=SJoinStep)
sj_any_of				= redef(fab_any_of, cls_step=SJoinStep)
#endregion

#region join devcs
class SJoin:
	''' Define a str.join transform on a sequence. Applies sep as separator, and apply open and close to each end.
		Coerces each in seq to str by default.
	'''
	to_str			= str
	def __init__(self, sep=None, open=None, close=None, *, tab=None, args_from=None, esc_fmt=False, to_str=nothing):
		''' ctor
			:param sep:
			:param open:
			:param close:
			:param args_from:
			:param esc_fmt:
			:param to_str:
		'''
		if args_from is not None:
			assert sep is None, f'got mutually exclusive args sep:{sep} and args_from:{args_from}'
			if sep or open or close:
				raise ValueError('Ambiguous arguments. args_from and any in {sep, open, close}')
			args_from = StrsFrom(args_from)
			sep, open, close, *_ = args_from + (['']*3)
		self.sep = ', ' if sep is None else sep
		self.open = '' if open is None else open
		self.close = '' if close is None else close
		self.tab = isnone(tab,put='\n' in self.sep and '  ')  # fixme: hardcoded
		self.esc_fmt = esc_fmt
		to_str is not nothing and setattr(self, 'to_str', isstr(to_str, put=to_str.format))
	def join(self, seq, to_str=None,deep=None,**ka):
		tab = deep is not None and self.tab and (deep*self.tab)
		seq = (
			seq						if to_str is False or (to_str is None and not self.to_str) else
			map(self.to_str,seq)	if to_str is None else
			map(to_str.format,seq)	if isstr(to_str) else
			map(str,seq)			if to_str is True else
			map(to_str,seq) )
		pfx,sep = (
			(self.open,self.sep)			if not tab else
			(self.open+'\n'+tab,self.sep+tab) )
		out = sjoininter(seq, sep, pfx=pfx or None, sfx=self.close or None)
		if self.esc_fmt:
			out = out.replace('{', '{{').replace('}', '}}')
		return out
	def __iter__(self): return iter([self.open, self.sep, self.close])
	def format(self, format_str=None, items='abc'):
		result = f'{self.open}{self.sep.join(items)}{self.close}'
		if format_str:
			result = '{:{}}'.format(result, format_str)
		return result
	def __repr__(self):
		result = f'{self.__class__.__name__}({str(self)!r})'
		return result
	
	# method aliases
	__call__		= join
	__str__ = __format__ = format
	
	# properties
	__name__		= property(repr)
	
def SJoinSep(args_from=None, esc_fmt=False, to_str=nothing, *, sep=None, **ka):
	return SJoin(args_from=args_from, sep=sep, esc_fmt=esc_fmt, to_str=to_str, **ka)

class SJoins:
	''' Join any dggree of nexted iterators '''
	def __init__(self, *join_args:str, esc_fmt_index=None, sep_args=True, to_seq=None, **ka):
		''' ctor
			:param join_args: each join arg. any degree of nested iterators can be joined given enough join_args
			:param esc_fmt_index: join index at which to apply string format inputs, when True format last join
		'''
		self.joins = [(sep_args and SJoinSep or SJoin)(join_arg, **ka) for join_arg in join_args]
		self.to_seq = to_seq
		if esc_fmt_index or esc_fmt_index == 0:
			esc_fmt_index = esc_fmt_index is True and -1 or esc_fmt_index
			self.joins[esc_fmt_index].esc_fmt = True
	def __repr__(self):
		result = f'{self.__class__.__name__} {xseq(self.joins, SJ.j_tuple, [repr, str])}'
		return result
	def join(self, seq, *fmt_pa, deep=None, deepest=None, **fmt_ka):
		seq = seq if not self.to_seq else self.to_seq(seq)
		s_out = xseq(seq, *self.joins, deep=deep)
		if fmt_pa or fmt_ka:
			s_out = s_out.format(*fmt_pa, **fmt_ka)
		return s_out
	
	# method aliases
	__call__		= join
	
	# properties
	__name__		= property(repr)

class SJoinsOf:
	def __init__(self, *join_args:str, esc_fmt_index=False, sep_args=True, to_seq=None, **ka):
		''' ctor
			:param join_args: each join arg. any degree of nested iterators can be joined given enough join_args
			:param esc_fmt_index: join index at which to apply string format inputs, when True format last join
		'''
		self.join_at = [(sep_args and SJoinSep or SJoin)(join_arg, **ka) for join_arg in join_args]
		self.to_seq = to_seq
		if esc_fmt_index or esc_fmt_index == 0:
			esc_fmt_index = esc_fmt_index is True and -1 or esc_fmt_index
			self.joins[esc_fmt_index].esc_fmt = True
	def __repr__(self):
		result = f'{self.__class__.__name__} {xseq(self.joins_strs, SJ.j_tuple, [repr, str])}'
		return result
	def join(self, seq, *fmt_pa, **fmt_ka):
		seq = seq if not self.to_seq else self.to_seq(seq)
		s_out = xseq(seq, *self.joins)
		if fmt_pa or fmt_ka:
			s_out = s_out.format(*fmt_pa, **fmt_ka)
		return s_out

class SJoinStrs(SJoin):	to_str = None
class ReprSJoin(SJoin):	to_str = repr
#endregion

#region other devcs todo: eliminate
class Formatted:
	def __iter__(self, str_fmtr=None, repr_fmtr=None):
		self.str_fmtr = str_fmtr or repr_fmtr
		self.repr_fmtr = repr_fmtr or str_fmtr
	def __str__(self): pass
	def __repr__(self): pass

@dataclass
class Unpack:
	op: callable
	def unpack(self, args=None, ka=None):
		result = self.op(*(args or []), **(ka or {}))
		return result
	__call__		= unpack
#endregion

#region join presets
_to_ikv				= dict(to_seq=items_of)

class DefsSJoins:
	j				= SJoin(	'')
	j_iter			= SJoin(	',')
	j_dot			= SJoin(	'.')
	j_space			= SJoin(	' ')
	j_spaces		= SJoin(	'  ')
	j_tab			= SJoin(	'\t')
	j_lines			= SJoin(	'\n')
	j_iter_			= SJoin(	', ')
	j_tuple			= SJoin(	', ',			'(',		')')
	j_list			= SJoin(	', ',			'[',		']')
	j_set			= SJoin(	', ',			'{',		'}')
	j_vector		= SJoin(	',',			'<',		'>')
	j_rrx			= SJoin(	'|','^(', ')$')
	j_values_nl		= SJoin(	',\n')
	j_tuple_nl		= SJoin(	',\n ','(',',)')
	j_list_nl		= SJoin(	',\n ','[',',]')
	j_set_nl		= SJoin(	',\n ','{',',}')
	j_vector_nl		= SJoin(	',\n ','<',',>')
	
	jj				= SJoins(	'', '')
	jj_keyargs		= SJoins(	', ',			'=')
	jj_dict			= SJoins(	'_, _{_,}',		':',					**_to_ikv)
	jj_dictcls		= SJoins(	'_, _dict(_)',	'=',					**_to_ikv)
	jj_obj_fmt		= SJoins(	'_, _{}(_)',	'=')
	jj_keyvals		= SJoins(	', ',			':')
	jj_iter_nl		= SJoins(	'\n',			',')
	jj_space_nl		= SJoins(	'\n',			' ')
	jj_spaces_nl	= SJoins(	'\n',			'  ')
	jj_parag_nl		= SJoins(	'_\n _"_"',		' ')
	jj_list_nl		= SJoins(	'_,\n _[_]',	', ')
	jj_tuple_nl		= SJoins(	'_,\n _(_)',	', ')
	jj_set_nl		= SJoins(	'_,\n _{_}',	', ')
	jj_dict_nl		= SJoins(	'_,\n _{_, }',	': ',					**_to_ikv)
	jj_lists_nl		= SJoins(	'_,\n _[_]',	'_, _[_]')
	jj_tuples_nl	= SJoins(	'_,\n _[_]',	'_, _(_)')
	jj_sets_nl		= SJoins(	'_,\n _[_]',	'_, _{_}')
	jj_dicts_nl		= SJoins(	'_,\n _[_]',	'_: _{_, }',			**_to_ikv)
	jj_keyvals_nl	= SJoins(	',\n',			':')
	jj_map_nl		= SJoins(	'_,\n _{_,}',	':')
	
	jjj_dicts_nl	= SJoins(	',\n',			'_: _{_},',		', ',	**_to_ikv)
	
	any_at			= Vars(
		_sjoin		= set(),
		default		= sj_any_of([
						#	qlfy		: [fab_i,		fab, ]
						{	dict		: (dict.items,	jj_dict_nl), },
						[	[Has._sjoin	, (None,		'{!r}'.format)],
							[ischrs		, (None,		'{!r}'.format)],
							[isiter		, (asis,		j_list)],
							[always		, (None,		'{!r}'.format)], ], ]),
		default_nl	= sj_any_of([
						#	qlfy		: [fab_i,		fab, ]
						{	dict		: (dict.items,	jj_dict_nl), },
						[	[Has._sjoin	, (None,		'{!r}'.format)],
							[ischrs		, (None,		'{!r}'.format)],
							[isiter		, (asis,		j_list_nl)],
							[always		, (None,		'{!r}'.format)], ], ]),
					)
	any,any_nl		= any_at.default, any_at.default_nl
	
	#region aliases
	# Note: this can serve as a reference of pykit/pyutils naming conventions;
	#       nl|newline is unique to joins module;  globally the aliases mean: n|num, l|list
	(	ji,			_,			j_,			_,			jnl,		ji_,		jt,			jl,
		jh,			jvec,		jinl,		jtnl,		jlnl,		jhnl,		jvecnl,		jjka,
		jjd,		_,			jjkvs,		jjd,		jjinl,		jjlnl,		jjdnl,		jjkvsnl,
		j__,		) = (
		
		j_iter,		j_dot,		j_space,	j_tab,		j_lines,	j_iter_,	j_tuple,	j_list,
		j_set,		j_vector,	j_values_nl,j_tuple_nl,	j_list_nl,	j_set_nl,	j_vector_nl,jj_keyargs,
		jj_dict,	jj_obj_fmt,	jj_keyvals,	jj_dict, 	jj_iter_nl,	jj_list_nl,	jj_dict_nl,	jj_keyvals_nl,
		j_spaces,	)
	
	(	jv_values,	jv_tuple,	jv_list,	jv_set,		jv_vector,	jjv_keyvals,jjv_map,	) = (
		j_values_nl,j_tuple_nl,	j_list_nl,	j_set_nl,	j_vector_nl,jj_dict_nl,	jj_keyvals_nl, )
	
	j_comma	= j_posargs=jpa		= j_iter
	j_items = j_posargs_=jpa_	= j_iter_
	j_words,jj_words_nl			= j_space,jj_space_nl
	#endregion
	...

_sjoin_print					= lambda sjoin, seq, *pa, **ka: print(sjoin(seq, *pa, **ka))
DefsPrintJoins:DefsSJoins		= subclsof(DefsSJoins, 'DefsPrintJoins', lambda j_mbr: _sjoin_print.__get__(j_mbr))

SJ = J							= DefsSJoins
PJ								= DefsPrintJoins
Join,Joins						= SJoin,SJoins  # todo: eliminate
#endregion

#region nested join presets
#endregion

#region notes
''' Define a str.join transform on a sequence. Applies sep as separator, and apply open and close to each end.
	Removes the default str coercion for each item making the default options slightly more performant.
	Identical to Join(..., to_str=None)
'''
#endregion
