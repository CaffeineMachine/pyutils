import sys
from dataclasses import dataclass, field

# from pyutils.experimental.inspect.traceprint import classsig
# from pyutils.experimental.inspect import traceprint
from pyutils.patterns.decor import *
from pyutils.extend.datastructures import XDict
import copy
import types
import re
from time import sleep
from pprint import pprint as pp


class Exceptions(object):
	@staticmethod
	def set_excepthook():
		sys.excepthook = Exceptions.on_post_mortem

	@staticmethod
	def on_post_mortem(type_, value, tb):
		import traceback
		import pdb
		traceback.print_exception(type_, value, tb)
		pdb.post_mortem(tb)
		Exceptions.archive_post_mortem(tb)

	@staticmethod
	def on_pre_post_mortem():
		''' detect and break on a postmortem that was previously encountered and archived '''
		pass

	@staticmethod
	def archive_post_mortem(tb):
		''' detect and break on a postmortem that was previously encountered and archived '''
		pass

	@staticmethod
	def analyze_past_post_mortems():
		''' detect and break on a postmortem that was previously encountered and archived '''
		pass

class StdFormatError(Exception):
	''' '''
	# def __init__(self):
	# 	pass

class ErrorInfo(Exception):
	_msg = 'Failed a given assertion.'
	_inputs = 'inputs: *{_args}, **{_kws}'
	_sig = 'line signature: {line_sig}'
	_fmt_sig = '\n| - '.join(['{_msg}',  _sig])
	_fmt_inputs = '\n| - '.join(['{_msg}', _inputs])
	_fmt_detail = '\n| - '.join(['{_msg}', _inputs, _sig])

	def __init__(self, assertion=None, msg=_msg, msg_fmt=None, frame=None, **kws):
		super(ErrorInfo, self).__init__()
		self._msg = msg
		self._msg_fmt = msg_fmt or ErrorInfo._fmt_detail
		self._assertion = assertion
		self._args = ()
		self._kws = kws
		self.frame = frame
		self.set_frame_info(frame)

	def set_frame_info(self, frame=None):
		self.frame = frame or self.frame
		if self.frame:
			self.line_sig = traceprint.classsigline(frame=self.frame)
			self.func_sig = traceprint.classsigline(frame=self.frame)
			# self.class_name = traceprint.classsigline(frame=self.frame)
			# self.func = traceprint.classsigline(frame=self.frame)
			# self.line = traceprint.classsigline(frame=self.frame)

	def __str__(self):
		result = self.format(self.frame)
		return result
	def __repr__(self):
		result = self.format(self.frame)
		return result

	def format(self, frame=None):
		''' '''
		self.set_frame_info(frame or self.frame or sys._getframe(1))
		self.message = self._msg_fmt.format(**self.__dict__)
		return self.message

	def validate(self, *args, **kws):
		kws = (XDict(self._kws) | kws)
		valid = self._assertion(*args, **kws)
		if not valid:
			exception = copy.copy(self)
			exception._args = args
			exception._kws = kws
			exception.set_frame_info(exception.frame or sys._getframe(1))
			raise exception

	def valid(self, *args, **kws):
		result = self._assertion(*args, **kws)
		return result


class UnexpectedTypeError(object): pass
class InvalidTypeError(StdFormatError):
	msg_fmt = '{class_sig}: Invalid variable type.\n\tvar: \'{name}\'\n\ttype: {obj_type}'
	msg_fmt_expected = '\n\tExpected: {expected}'
	msg_fmt_details = '\n\t{details}'

	def __init__(self, obj=None, name=None, expected=None, details=None, frame=None, **kws):
		super(InvalidTypeError, self).__init__()
		members = XDict(locals()) | self.__dict__
		members |= (XDict(locals()) - [None]) ^ ['self']
		self.__dict__.update(members)
		if len(kws):
			self.name, self.obj = kws.items()[0]
		self.obj_type = isinstance(self.obj, type) and self.obj or type(self.obj)
		self.class_sig = traceprint.classsigline(frame=frame, depth=1)
		pass

	def __str__(self):
		result = self.format(self.frame)
		return result
	def __repr__(self):
		result = self.format(self.frame)
		return result

	@staticmethod_self_or_none
	def format(self, frame=None, *args, **kws):
		''' Usage:
		'''
		frame = frame or sys._getframe(1)
		self = self and copy.copy(self) or InvalidTypeError()
		self.__init__(frame=frame, *args, **kws)

		self.message = self.msg_fmt.format(**self.__dict__)
		self.message += self.expected and self.msg_fmt_expected.format(**self.__dict__) or ''
		self.message += self.details and self.msg_fmt_details.format(**self.__dict__) or ''
		self.message = re.sub(r'<(type|class) \'(\w+)\'>', r'\1', self.message)
		return self.message+'\n'

	@staticmethod_self_or_none
	def validate(self, frame=None, *args, **kws):
		frame = frame or sys._getframe(1)
		self = self and copy.copy(self) or InvalidTypeError()
		self.__init__(frame=frame, *args, **kws)
		if not self.valid(frame=frame):
			raise self

	@staticmethod_self_or_none
	def valid(self, verbose=False, frame=None, *args, **kws):
		frame = frame or sys._getframe(1)
		self = self and copy.copy(self) or InvalidTypeError()
		self.__init__(frame=frame, *args, **kws)

		result = issubclass(self.obj_type, self.expected)
		if verbose and not result:
			print(self)
		return result


def ignore(error, func, *args, on_error=None, **kws):
	''' suppress exceptions of type errors '''
	try:
		result = func(*args, **kws)
	except error as e:
		result = on_error(e) if hasattr(on_error, '__call__') else on_error
	return result
	
def raises(error, func, *args, on_no_error=None, **kws):
	''' assert that an error was raised and that it was of type errors '''
	unexpected_errors = (Exception)
	note = ''
	try:
		func(*args, **kws)
	except error as e:
		return e
	except unexpected_errors as e:
		note = f'\n\tInstead raised {e!r}.'
		if on_no_error:
			result = on_no_error
			if hasattr(on_no_error, '__call__'):
				result = on_no_error()
			return result
		
	raise Exception(f'Failed to raise expected error(s): {error}{note}')

class Errors(list):
	ignore = ignore
	raises = raises

''' 
possible implementations of debugging on exception:

def enter_debugger(f):
	@functools.wraps(f)
	def wrapper(*args, **kws):
		try:
			return f(*args, **kws)
		except exceptions:
			pdb.post_mortem(sys.exc_info()[2])
	return wrapper

def info(type, value, tb):
	if hasattr(sys, 'ps1') or not sys.stderr.isatty():
		# we are in interactive mode or we don't have a tty-like
		# device, so we call the default hook
		sys.__excepthook__(type, value, tb)
	else:
		import traceback, pdb
		# we are NOT in interactive mode, print( the exception...)
		traceback.print_exception(type, value, tb)
		#print(
		# ...then start the debugger in post-mortem mode.)
		pdb.post_mortem(tb)
'''

if __name__=='__main__':
	class Foo(object):
		def named_scope(self):
			# err2 = ErrorInfo()
			ErrorInfo(isinstance, msg='isinstance(var, int)').validate(0, int)
			ErrorInfo(lambda var: isinstance(var, int), msg='isinstance(var, int)').validate(0)
			ErrorInfo(isinstance, msg='isinstance(var, int)').validate(0, str)

			return



			err1 = InvalidTypeError(input=5, expected=(dict, list), details='Fatal!!!')
			# print('to_string: ' + str(InvalidTypeError(input=5, expected=(dict, list))))

			print('valid?: %s' % err1.valid(verbose=True, my_int1=10))
			# returns False and prints 'Invalid type:int for var:'my_int1' in func Foo.named_scope:113.  Expected:(dict, list)  Fatal!!!'

			print('valid?: %s' % err1.valid(verbose=True, my_list=[]))
			# returns True and dows not print a message

			sleep(.2)
			err1.validate(verbose=True, my_int2=100)
			# raises Exception => __main__.InvalidTypeError: Invalid type:int for var:'my_int2' in func Foo.named_scope:116.  Expected:(dict, list)  Fatal!!!

	Foo().named_scope()

