from __future__ import print_function

import os
import sys
import re
from copy import deepcopy
from dataclasses import dataclass, field
from datetime import datetime

from pyutils.defs.rsrc import *
from pyutils.utilities.pathutils import *
from pyutils.patterns.idfactory import *
from pyutils.patterns.decor import *
from pyutils.time.timestamp import *
from pyutils.time.thread import *
from pyutils.structures.strs import *
from pyutils.output.colorize import *
from pyutils.extend.datastructures import XDict

#region defs
if sys.version_info.major != 2:
	import io
	file = io.TextIOBase

class DefsLog:
	p_logconfig				= p_loc/'logconfig.yaml'
	
DDLog = DD					= DefsLog
Color						= DefsColorize
#endregion

#region log utils
def bootstrap_logconfig():
	''' generate baseline logconfig '''
	import yaml
	yaml_logconfig = '''
		default_out_dir: %s/logs/
		log_inputs: null
		log_outputs:
		  stderr:
		    out_files:
		    - stdout
		    show:
		    - catastrophic
		    - error
		  stdout:
		    hide:
		    - catastrophic
		    - error
		    out_files:
		    - stdout
		loggers: null
		tag_color_map:
		  back: {}
		  fore: {}
	'''.replace('\t','') % p_home
	logconfig = yaml.safe_load(yaml_logconfig)
	return logconfig
def save_logconfig(logconfig=None,path=None,log=no):
	import yaml
	if log is True:
		from pprint import pprint as log
	# resolve inputs
	logconfig = logconfig or bootstrap_logconfig()
	p_logconfig = P(path or DD.p_logconfig)
	
	# write output
	s_logconfig = yaml.safe_dump(logconfig)
	p_logconfig.write_text(s_logconfig)
	log and log(s_logconfig)

def fwrite(msg, file, mode='a'):
	''' one liner write to file '''
	with open(file, mode) as file_stream:
		file_stream.write(msg)

def log(*pa,**ka):
	logr = Log()  # instantiate default static instance
	log = globals()['log'] = Log._the.log
	out = tlog(*pa,**ka)
	return out
def tlog(*pa,**ka):
	logr = TLog()
	tlog = globals()['tlog'] = TLog._the.log
	out = tlog(*pa,**ka)
	return out

def test_log_all():
	'''
		# usage example:
		# configure presumed default tag colors dynamically in code
		Log.add_tag_color(
			catastrophic = 'red',
			error = 'redl',
			warning = 'yellow',
			notice = 'green',
			info = 'cyan',
			debug = 'blue')
		Log.add_tag_color(n='redb', s='blueb', e='black', w='whitel')
		Log.save()
	'''
	# basic log level printing
	log('This is a log')
	trace('This is a trace')
	debug('This is a debug')
	info('This is a info')
	notice('This is a notice')
	warn('This is a warning')
	error('This is a error')
	catastrophic('This is a catastrophic')
	
	logconfig = '/'.join(__file__.split('/')[-1:]+['logconfig.yaml'])
	Log.load(logconfig)
	Log.add_tag_color(n='redb', s='blueb', e='black', w='whitel')
	
	LogInput('ne', tags='n e')
	LogInput('nw', tags='n w')
	LogInput('sw', tags='s w')
	LogInput('se', tags='s e')
	
	log('russia', id='ne')
	log('canada', id='nw')
	log('brazil', id='sw')
	log('japan',  id='se')
	exit()
#endregion

#region loglvl devcs
class Level(object):
	''' log level tags '''
	all = (
		c, e, w, n, i, d, t ) = (
		catastrophic, error, warn, notice, info, debug, trace ) = (
		'catastrophic', 'error', 'warn', 'notice', 'info', 'debug', 'trace' )
	all_set = set(all)
class Levels(object):
	''' log level sets '''
	all = (
		c, e, w, n, i, d, t ) = (
		catastrophic, error, warn, notice, info, debug, trace ) = [
		set(list(Level.all[:index+1])) for index in range(len(Level.all))]
class LevelsLE(object):
	''' log level sets inverted (less than or equal to given tag) '''
	all = (
		c, e, w, n, i, d, t ) = (
		catastrophic, error, warn, notice, info, debug, trace ) = [
		set(list(Level.all[index:])) for index in range(len(Level.all))]
#endregion

#region log devcs
class LogInput(IdFactory):
	''' Preset configuration used to modify an ingress message with reformats or replacements '''
	#region defs
	tag_delim = ' '
	default_id = None
	_default_format = None
	#endregion
	#region forms
	def __init__(self, id=None, tags='', format=None, formatter=None, **kws):  #prefixes=None, suffixes=None):
		''' ctor '''
		# prevent double initialization in the case that self was retrieved from dict of existing instances
		if hasattr(self, 'id'): return
		
		# initialize members
		super(LogInput, self).__init__(id)
		delim = LogInput.tag_delim
		self.tags = set(LStrs(f'{id}{LogInput.tag_delim}{tags}', LogInput.tag_delim))
		self.prefixes = kws.get('prefixes', {'emit':'\n'})  # todo: refactor to be simpler
		self.suffixes = kws.get('suffixes', {})				# todo: refactor to be simpler
		self.subs = kws.get('subs', {})						# todo: refactor to be simpler
		self.fmt = format or self._default_format
		self.fmtr = formatter
	def log(self, msg='', tags=None, end=None):
		log(msg, tags, id=str(self.id), end=end)
	# method aliases
	__call__				= log
	#endregion
	...

class TLogInput(LogInput):
	_default_format			= '{now:%Y-%d-%m+%H:%M:%S} {level:.12} {0}'

@dataclass(unsafe_hash=True)
class LogWriter:
	name: str				= 'log_writer'
	handle: any				= field(default=None, hash=False)
	#region utils
	def write(self, msg, **kws):
		print(msg, file=self.handle, **kws)
	#endregion
	...

@dataclass(unsafe_hash=True)
class FileLogWriter(LogWriter):
	mode: str				= 'w'
	#region defs
	_std_files				= {'stdout':sys.stdout, 'stderr':sys.stderr}
	#endregion
	#region forms
	def __post_init__(self):
		self.handle = self.handle or FileLogWriter._std_files.get(self.name) or open(self.name, self.mode)
	def __del__(self):
		if self.handle and self.name not in FileLogWriter._std_files:
			self.handle.close()
	def __str__(self):
		return self.name
	#endregion
	...

class LogOutput(IdFactory):
	''' Configures applicability of an egress message with given tags to a set of out files '''
	#region defs
	_log					= print
	SOAS = set_of_all_sets	= {'{*}'}
	default_out_file_fmt	= '{out_dir}{uid}{name}-default.log'
	default_format			= '{}'
	writer_factory			= FileLogWriter
	#endregion
	#region forms
	def __init__(self, id=None, show=None, hide=None, out_files=None, writer_factory=None, tags=None, format=None):
		''' ctor '''
		# prevent double initialization in the case that self was retrieved from dict of existing instances
		if hasattr(self, 'id'): return
		
		# initialize members
		super(LogOutput, self).__init__(id)
		self.show = set(show) if show is not None else set()
		self.hide = set(hide) if hide is not None else set()
		self.fmt = format or self.default_format
		self.out_files_implicit = [LogOutput.default_out_file_fmt.format(
			out_dir=Log.the().default_out_dir,
			# uid=(st_of() + '-') and '',
			uid=st_of() + '-',
			name=self.id)]
		self.out_files = out_files or self.out_files_implicit
		self.tags = tags
		self.file_handles = {}
		
		if writer_factory:
			self.writer_factory = writer_factory
		if list(LogOutput.set_of_all_sets)[0] in self.show:
			self.show = LogOutput.set_of_all_sets
		if list(LogOutput.set_of_all_sets)[0] in self.hide:
			self.hide = LogOutput.set_of_all_sets
	def __str__(self):
		''' string representation should have show/hide tags '''
		showing = ' +'.join(['']+[self.id]+list(self.show) or [])
		hiding = ' -'.join(self.hide and ['']+list(self.hide) or [])
		return 'LogOutput(filter=\'%s\')' % (showing + hiding).strip()
	def serialize_simple(self):
		''' simple logger instance representation defined in terms of only dicts and lists '''
		result = {}
		for member,value in self.__dict__.items():
			if ((member in ('show', 'hide') and value)
			or (member == 'out_files' and self.out_files != self.out_files_implicit)):
				result[member] = list(value)
		return result or None
	def should_show(self, msg_tags):
		''' does msg have any tags to be shown and no hidden tags '''
		# show message if: A. hide and msg tags DO NOT overlap ...
		must_hide = self.hide.intersection(msg_tags)
		if must_hide: return not must_hide
		
		# ... and show message if: B. show and msg tags DO overlap
		allow_show = (
			not self.show                              # show any when show is empty/None or -
			or self.show is LogOutput.set_of_all_sets  # show msg when logger is explicitly configured to or -
			or self.show.intersection(msg_tags))       # show msg when logger tags match atleast one message tag
		return allow_show
	def _open(self, out_file):
		''' return existing out_file writer; otherwise attempt to open out_file before returning it '''
		try:
			writer = self.writer_factory(out_file)
			return writer
		except Exception as e:
			print(yellow('pyutils.log: %s' % e))
			print(yellow(f'pyutils.log: Failed to open out file \'{out_file}\' for {self}'))
	def log(self, msg, tags, now, level, color, writers, written=None, **kws):  # todo: only msg is required; add defaults
		# exit condition: msg has no tags to be shown or a hidden tag
		if not self.should_show(tags): return
		
		# message will be printed; write to logger out_files which have not yet received message
		written = written or set()
		output_msg = color_fmt(self.fmt.format(msg, now=now, level=level), color)
		for out_file in (set(self.out_files) - written):
			# get file handle
			writer = writers.get(out_file) or writers.setdefault(out_file, self._open(out_file))
			if not writer: continue
			
			# write out to file and record to prevent duplicate writes
			writer.write(output_msg, **kws)
			written.add(out_file)
	#endregion
	...

class TLogOutput(LogOutput):
	default_format			= '{now:%Y-%d-%m+%H:%M:%S} {level:.12} {0}'

class Log:
	''' class to process log events. compares log tags and configured logger tags to determine
		when to show a log. Maintains, updates and provides access for all logging configuration.
	'''
	#region defs
	_the					= None
	_default_log_input		= LogInput(None)
	Input = input_cls		= LogInput
	Output = output_cls		= LogOutput
	id_fill = default_id	= None
	hide_tags				= {'hidden'} | LevelsLE.debug
	default_out_dir			= '~/logs/'
	color_tag_prefix		= 'color_' # 'c_'
	exportable_members		= ('default_out_dir', 'log_inputs', 'loggers', 'tag_color_map', 'recent_tags')
	#endregion
	#region forms
	def __init__(self):
		''' ctor '''
		self.set_the()
		
		# initialize members
		self.config_path = None
		self.default_out_dir = None
		self.loggers = None
		self.log_inputs = None
		self.log_outputs = None
		self.tag_color_map = None
		self.check_interval = 1
		self._config_sig = None
		self._worker = None
		self._file_handles = {}
		self.tag_color_map = {Color.fore:{}, Color.back:{}}
		
		# functions below will provide defaults
		self.set_members()
		self._init_tag_color_map()
		self.load()
	@classmethod
	def the(cls,*pa,**ka):
		cls._the = cls._the or cls(*pa,**ka)
		return cls._the
	def set_the(self,force=no):
		cls = clsof(self)
		cls._the = force and self or (cls._the or self)
		return cls._the
	@staticmethod_self_or_none
	def set_members(self, default_out_dir=None, check_interval=None):
		self = self or Log._the
		
		# self.default_out_dir = default_out_dir or Log.default_out_dir
		# self.default_out_dir = re.sub(r'^~', os.environ['HOME'], self.default_out_dir)
		self.default_out_dir = os.path.expanduser(default_out_dir or Log.default_out_dir)
		
		# load configuration from file and optionally poll for reconfiguration
		if check_interval != self.check_interval and check_interval is not None:
			self.check_interval = check_interval
			if self._worker:
				self._worker.stop()
			if check_interval:
				self._worker = RepeatTimer.start_new(min(1, self.check_interval), Log.load)
	#endregion
	#region utils
	@staticmethod
	def _load():
		Log.load()
	@staticmethod_self_or_none
	def load(self=None, config_path=None):
		''' load logging configuration from yaml file '''
		import yaml
		self = self or Log._the
		_LogOutput = self.output_cls
		
		# validate config path
		self.config_path = (
			config_path	or
			self.config_path or
			re.findall(u'(^.+[/\\\\])[^/]*', __file__)[0] + 'logconfig.yaml'  )
		# self.config_path = re.sub(r'^~', os.environ['HOME'], self.config_path)
		self.config_path = os.path.expanduser(self.config_path)
		if not os.path.isfile(self.config_path):
			print(yellow('pyutils.LogConfig: Failed to find file to load.  config_path: %s' % self.config_path))
			self.log_outputs = dict(
				stdout = _LogOutput('stdout', hide=Levels.error, out_files=['stdout']),
				stderr = _LogOutput('stderr', show=Levels.error, out_files=['stdout']),
			)
			self.save()
			return
		
		# exit condition; config_file has not changed
		config_sig = os.stat(self.config_path)
		if self._config_sig == config_sig: return
		
		# read yaml configuration file and update members
		self._config_sig = config_sig
		with open(self.config_path, 'r') as config_file:
			raw_content = config_file.read()
		config = yaml.full_load(raw_content)
		self.default_out_dir = config['default_out_dir']
		
		# interpret log_outputs
		for id, logger in config['log_outputs'].items():
			# unless extracted IdFactory subclass constructors retrieve existing config instance
			_LogOutput.cls_pop_id(id)
			config['log_outputs'][id] = _LogOutput(id=id, **(logger or {}))
		
		# update members
		self.__dict__.update(config)
		
		self._init_tag_color_map()
		if 'tag_color_map' in config and Color.fore in config['tag_color_map']:
			self.add_tag_color(**config['tag_color_map'][Color.fore])
		if 'tag_color_map' in config and Color.back in config['tag_color_map']:
			self.add_tag_color(**config['tag_color_map'][Color.back])
		
		print('Pyutils %s reconfigured' % self.__class__.__name__)
		return self
	@staticmethod_self_or_none
	def save(self, config_path=None):
		''' export current logging configuration to yaml file '''
		import yaml
		
		self = self or Log._the
		
		# validate config path; create directory if needed
		self.config_path = config_path or self.config_path
		config_dir = os.path.dirname(self.config_path)
		if not os.path.exists(config_dir) and config_dir:
			os.makedirs(config_dir)
		
		# export configuration to yaml file
		export_data = XDict(self.__dict__) & Log.exportable_members
		export_data['log_outputs'] = {name:value.serialize_simple() for name,value in self.log_outputs.items()}
		export_data['tag_color_map'] = deepcopy(export_data['tag_color_map'])
		color_names, color_codes = list(Color.tags), list(Color.tags.values())
		for layer,layer_color_map in export_data['tag_color_map'].items():
			# export_data['tag_color_map'][layer] = XDict(layer_color_map) - map(lambda k: 'color_'+k, Color.tags)
			layer_color_map = XDict(layer_color_map) - map(lambda k: 'color_'+k, Color.tags)
			for tag,color in layer_color_map.items():
				if color in color_codes:
					layer_color_map[tag] = color_names[color_codes.index(color)]
			export_data['tag_color_map'][layer] = layer_color_map
		
		with open(self.config_path, 'w') as config_file:
			yaml.dump(dict(export_data), config_file, default_flow_style=False)
		self._config_sig = os.stat(self.config_path)
	def _init_tag_color_map(self):
		for layer, layer_tag_colors in Color.layers.items():
		# for layer, layer_tag_colors in dict(fore=Fore.__dict__, back=Back.__dict__).items():
			for tag, color in layer_tag_colors.items():
				self.tag_color_map[layer][Log.color_tag_prefix+tag] = color
	def add_io(self, *inputs_outputs):
		''' Add new an ingress or egress configuration to this log instance. Expects LogInput or LogOutput args. '''
		for io_conf in inputs_outputs:
			(isinstance(io_conf, LogInput) and self.log_inputs or self.log_outputs).add(io_conf)
	def remove_io(self, *inputs_outputs):
		''' Remove existing an ingress or egress configuration to this log instance. Expects LogInput or LogOutput args or id. '''
		for io_conf in inputs_outputs:
			(isinstance(io_conf, LogInput) and self.log_inputs or self.log_outputs).remove(io_conf)
	@staticmethod_self_or_none
	def add_tag_color(self, **tag_color_kws):
		self = self or Log._the
		for tag, color in tag_color_kws.items():
			for index, (layer, layer_tag_colors) in enumerate(Color.layers.items()):
				if color in layer_tag_colors:# or (self.color_tag_prefix + color) in layer_tag_colors:
					self.tag_color_map[layer][tag] = Color.tags[color]
				elif color in layer_tag_colors.values():
					self.tag_color_map[layer][tag] = color
	@staticmethod_self_or_none
	def remove_tag_color(this, *args):
		self = this or Log._the
		for tag in args:
			for layer in Color.layers:
				if tag in self.tag_color_map[layer]:
					self.tag_color_map[layer].pop(tag)
	def log(self, msg, *tags, id=None, **ka):
		''' write message to applicable logger files based on message tag tags '''
		
		# reformat tags as needed
		tags = tags or set()
		if isinstance(tags, (list, tuple)):
			tags = set(tags)
		elif isinstance(tags, str):
			tags = set(tags.split(LogInput.tag_delim))
		log_input = id is not None and self.input_cls(id) or self._default_log_input
		tags |= log_input.tags
		
		# do not show logs with hidden tags
		if tags & Log.hide_tags:
			return
		
		# define log elements
		msg = isinstance(msg, str) and msg or f'{msg!r}' # pfmt(msg)
		now = datetime.now()
		level = next(iter(Level.all_set & tags), '').upper()  # take the first tag matching a log level
		
		# augment message with LogInput configured  substitutions
		if log_input.prefixes and tags & set(log_input.prefixes):
			msg = ''.join([text for tag, text in log_input.prefixes.items() if tag in tags]) + msg
		if log_input.suffixes and tags & set(log_input.suffixes):
			msg = msg + ''.join([text for tag, text in log_input.suffixes.items() if tag in tags])
		if log_input.fmtr:
			msg = log_input.fmtr(msg, now=now, level=level)
		elif log_input.fmt:
			msg = log_input.fmt.format(msg, now=now, level=level)
		
		# apply message reformatting
		tags_str = ' '.join(tags)
		color_tags = set(re.findall(fr'{self.color_tag_prefix}\w+', tags_str))
		config_color_set_fore = set(self.tag_color_map[Color.fore])  # todo: intensive op for each log
		config_color_set_back = set(self.tag_color_map[Color.back])  # todo: intensive op for each log
		text_colors =  list((color_tags & config_color_set_fore) or	(tags & config_color_set_fore))[:1]
		text_colors += list((color_tags & config_color_set_back) or	(tags & config_color_set_back))[:1]
		
		col = ''
		if text_colors:
			for tag in text_colors:
				col += self.tag_color_map[Color.fore].get(tag,'')
				col += self._the.tag_color_map[Color.back].get(tag,'')
		
		# process message for each logger configuration
		out_files_writen = set()
		for logger in self.log_outputs.values():
			logger.log(msg, tags,
				now=now, level=level, color=col,
				writers=self._file_handles, written=out_files_writen, **ka)
	@classmethod
	def set_loglevel(cls, loglevels):
		''' Make log level selection from options available in Level or Levels.  example: set_loglevel('debug') '''
		if loglevels in Level.all_set:
			loglevels = getattr(Levels, loglevels)
		if not isinstance(loglevels, (set, list)):
			raise TypeError(f'{cls.__name__}.set_loglevel(): invalid type for loglevel: {loglevels}')
		cls.hide_tags -= set(loglevels)
		cls.hide_tags |= Level.all_set - set(loglevels)
		cls.hide_tags = cls.hide_tags
	# easy access to standard log functions
	@classmethod
	def fatal(cls,msg,*tags,id=id_fill,end=None):	cls.the().log(msg,*(*tags,'catastrophic'),id=id,end=end)
	@classmethod
	def error(cls,msg,*tags,id=id_fill,end=None):	cls.the().log(msg,*(*tags,'error',), id=id, end=end)
	@classmethod
	def warn (cls,msg,*tags,id=id_fill,end=None):	cls.the().log(msg,*(*tags,'warning',), id=id, end=end)
	@classmethod
	def note (cls,msg,*tags,id=id_fill,end=None):	cls.the().log(msg,*(*tags,'notice',), id=id, end=end)
	@classmethod
	def info (cls,msg,*tags,id=id_fill,end=None):	cls.the().log(msg,*(*tags,'info',), id=id, end=end)
	@classmethod
	def debug(cls,msg,*tags,id=id_fill,end=None):	cls.the().log(msg,*(*tags,'debug',), id=id, end=end)
	@classmethod
	def trace(cls,msg,*tags,id=id_fill,end=None):	cls.the().log(msg,*(*tags,'trace',), id=id, end=end)
	
	f, e, w, n, i, d, t 	= fatal, error, warn, note, info, debug, trace
	catastrophic,notice=c,_	= fatal, note
	#endregion
	...

class TLog(Log):
	''' Alternative implementation with a timestamped messages typical seen in other prominent loggers '''
	#region defs
	Input = input_cls		= TLogInput
	_default_log_input		= TLogInput(None)
	_the = None
	#endregion
	...

fatal,      error,      warn,      note,      info,      debug,      trace,      = (
Log.fatal,  Log.error,  Log.warn,  Log.note,  Log.info,  Log.debug,  Log.trace,  )
tfatal,     terror,     twarn,     tnote,     tinfo,     tdebug,     ttrace,     = (
TLog.fatal, TLog.error, TLog.warn, TLog.note, TLog.info, TLog.debug, TLog.trace, )
catastrophic,tcatastrophic	= Log.fatal, TLog.fatal,
notice,tnotice				= Log.note, TLog.note,
#endregion

#region inline tests
if __name__=='__main__':
	# test_log_all()
	# bootrstrap_logconfig()
	save_logconfig()
#endregion

