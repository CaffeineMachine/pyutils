import math
from pprint import pformat as pfmt
from pyutils.output.joins import *
from pyutils.output.fmtrs import *
from pyutils.utilities.iterutils import *

# from pymatics.store.trackrecords import time_run
# fp_pln('from pymatics.store.trackrecords import time_run')


log = None
# log = print

class Columns(list):
	def __init__(self, *args, widths=None, join=None, **kws):
		self.join_items = join or JoinStrs(' ')
		self.columns = None
		self.widths = None
		super().__init__(*args, **kws)
		self.measure_rows()
	def measure_rows(self, rows=None):
		xseq(rows or self, list, self.measure_row)
	def measure_row(self, row):
		if not self.columns:
			self.columns = len(row)
			self.widths = [0] * self.columns
		elif len(row) != self.columns:
			raise Exception('Column Size mismatch')
		
		self.widths = [max(width, len(item)) for width, item in zip(self.widths, row)]
	def __format__(self, format_spec=''):
		fmt_delim = '{}'+self.join_items.delim
		# result = '\n'.join([f'{row}' for row in self])
		
		# margin = self.join_items.delim
		# fmt_row = ''.join(as_seq(self.widths, lambda width: '{{:{}}}'.format(width+len(self.join_items.delim)+1)))
		# lines = [fmt_row.format(*as_seq(row[:-1], fmt_delim.format), row[-1]) for row in self]
		# fmt_row = self.join_items(lseq(self.widths, '{{:{}}}'.format))
		# fmt_row = xseq(self.widths, self.join_items, '{{:{}}}'.format)
		fmt_row = xseq(self.widths, self.join_items, '{{:{}}}'.format)
		lines = [fmt_row.format(*row) for row in self]
		result = '\n'.join(lines)
		return result
	__str__ = __format__
Cols = Columns

class Flow: pass

class Wrap:
	def __init__(self, content):
		self.content = content
	def __format__(self, format_spec=''):
		result = ''
		return result
	__str__ = __repr__ = __format__

class StrCache:
	def __init__(self, formatter=None):
		self.formatter = formatter or repr
		self._formatted = None
	@property
	def formatted(self):
		if self._formatted is None:
			self._formatted = self.format()
		return self._formatted
	def __str__(self):
		if self._formatted is None:
			self._formatted = self.format()
		return self._formatted
	__repr__ = __str__
	def __repr__(self):
		if self._formatted is None:
			self._formatted = self.format()
		return self._formatted

class Form:
	''' form implements default behaviors including:
	 	- caching the result and
	 	- result reevaluating after content modifications
	 '''
	_content = None
	_result = None
	to_str = str
	
	def __init__(self, items=None, to_str=None, show=None, **kws):
		self.items = items or []
		self.to_str = to_str or self.to_str
		self.reset(**kws)
		super().__init__()
		if show: # or (self.__class__ is Form and show is None):
			# show = (show == True) and print or show
			# show(self)
			getattr(show, '__call__', print)(self)
	def __iter__(self): return iter(self.items)
	def reset(self, **kws):
		log and log('Form.reset()')
		self.__dict__.update(kws)
		self._result = self.format()
		return self._result
	def format(self, *args, **kws): raise NotImplemented()
	def __str__(self): return str(self._result)
	__format__ = format
	__repr__ = __str__
	
class FlowForm(Form): pass
class OverflowForm(Form):
	_overflow = '\n'
	width = 120
	
	def __init__(self, items=None, width=width, **kws):
		Form.__init__(self, items=items or [], width=width, **kws)
	@staticmethod
	def overflow(items:list, wrap:int=None, limit:int=None, join:Join=None)->list:
		flows, flow = [], []
		delim_width = 0 # len(join.delim)
		flow_line_width = -delim_width
		overflow_count = 0
		for item in items:
			flow_line_width += len(str(item)) + delim_width
			if flow_line_width and wrap < flow_line_width:
				overflow_count += 1
				if overflow_count == limit: break
				flows.append(flow)
			flow.append(item)
			
		return flows, overflow_count
	def format(self, items=None, **kws):
		items = items or self.items
		if isinstance(items, str):
			return items
		
		items = [self.to_str(item) for item in items]
		delim = ' '
		if self.width is not None:
			cum_len = len(items)
			for item in items[1:]:
				cum_len += len(item) + 1
				if self.width < cum_len:
					delim = '\n'
					break
		result = delim.join(items)
		return result
	
class FlowsForm: pass
class ColumnsMeta:
	def __init__(self, items=None, *args, widths=None, **kws):#, rows=None):
		log and log('ColumnsMeta()')
		self.widths = widths
		self.linebreaks = None
		self.columns = None
		super().__init__(items, *args, **kws)
		self.measure_rows()
	def measure_rows(self, rows=None):
		log and log('ColumnsMeta.measure_rows()')
		lseq(rows or self, self.measure_row)
		self._result = self.format()
	def measure_row(self, row):
		log and log('ColumnsMeta.measure_row()')
		if not self.columns:
			self.columns = len(row)
			self.widths = [1] * self.columns
			self.linebreaks = [0] * self.columns
		elif len(row) != self.columns:
			raise Exception('Column Size mismatch')
		
		self.widths = [max(width, len(str(item))) for width, item in zip(self.widths, row)]
		self.linebreaks = [max(lnbrk, str(item).count('\n')) for lnbrk, item in zip(self.linebreaks, row)]
		
class ColumnsForm(ColumnsMeta, Form):
	_overflow = '\n'
	_overflows = None
	_width = None
	_fmt_row = None
	
	def __init__(self, items=None, *args, width=_width, widths=None, join_items=' ', join_rows='\n', **kws):
		log and log('ColumnsForm()')
		# self.items = items
		self.width = width
		self.join_items = Join(join_items)
		self.join_rows = Join(join_rows)
		self._delims = None
		super().__init__(items, *args, **kws)
	@property
	def overflows(self):
		log and log('ColumnsForm.overflows()')
		if self._overflows is not None:
			return self._overflows

		if self.width is None:
			self._overflows = [''] * self.columns
		else:
			self._overflows = []
			delim_width = len(self.join_items.delim)
			flow_line_width = -delim_width
			for ind, (width, lnbrk) in enumerate(zip(self.widths, self.linebreaks)):
				flow_line_width += width + delim_width
				did_overflow = self.width < flow_line_width or lnbrk
				self._overflows.append(did_overflow and self._overflow or '')
				if did_overflow:
					flow_line_width = 0
			self._overflows[0] = ''
		return self._overflows
	@property
	def fmt_row(self):
		log and log('ColumnsForm.fmt_row()')
		if self._fmt_row is not None:
			return self._fmt_row
		
		# self._fmt_row = lseq(zip(self.overflows, self.widths), Fstr('{}{{:.{}s}}'), self.join_items)
		self._fmt_row = xseq(zip(self.overflows, self.widths), self.join_items, Fstr('{0}{{:{1}.{1}}}'))
		return self._fmt_row
	def format(self, items=None):
		log and log('ColumnsForm.format()')
		if not self.widths or not self.items: return None
		
		width_fmtr = Fstr(self.fmt_row)
		result = xseq(items or self.items, self.join_rows, width_fmtr, str)
		return result
	
	
class BlockForm: pass

class ILessForm(Form):
	''' Display only leading and trailing ends of a long sequence of items '''
	_head_tail = 10
	
	def __init__(self, items=None, head_tail=_head_tail, join_items=None, join_fmt='{},', gap='...{space}', wrap=120):
		self.join_items = join_items
		self.join_fmt = join_fmt
		self.gap = gap
		self.wrap = wrap
		if isinstance(head_tail, (int, float)):
			head_tail = [math.ceil(head_tail/2.), math.floor(head_tail/2.)]
		else:
			head_tail = list(head_tail)
		if isinstance(head_tail[0], (int, float)):
			head_tail[0] = slice(head_tail[0])
		if isinstance(head_tail[-1], (int, float)):
			head_tail[-1] = slice(-head_tail[-1], None)
		self.head_tail = head_tail
		
		super().__init__(items=items)
	def format(self, items=None):
		items = items or self.items
		total = len(items)
		head_tail = self.head_tail
		wrap = self.wrap
		
		# if overlapping ends then display all items
		if 2 <= len(head_tail) and head_tail[-1].start < 0 and head_tail[-1].start+len(items) <= head_tail[0].stop:
			parts = [items]
		else:
			parts = []
			for h_t in head_tail:
				if not isinstance(h_t, slice):
					raise TypeError(f'All inner indexes must be a slice. Got {head_tail}')
				part = items[h_t]
				if part:
					parts.append(part)
		
		# determine whether to use horz or vert item character spacing if undefined
		join_items = SJoinStrs(self.join_items)
		overflow_char = ' '
		if self.join_items is None:
			flows, overflow_count = OverflowForm.overflow(parts, wrap=wrap, limit=1)
			overflow_char = overflow_count and '\n' or ' '
			join_items = SJoinStrs(overflow_char)
		overflow_char = '\n' in join_items.delim and '\n' or ' '
		gap = SJoinStrs(join_items.delim + self.gap.format(space=overflow_char))
		
		# result = lseq(parts, join_items, gap)
		result = xseq(parts, gap, join_items)
		return result

class LessForm(ILessForm):
	''' Display only leading and trailing ends of a long str.
		Behaves like ILessForm with exception of assuming input is a str which gets wrapped.
	'''
	# def __init__(self, *pa, **ka): return time_run(self.init, *pa, **ka)
	# def init(self, text, *args, wrap=120, join_items='ignore kw', to_str=None, **kws):
	def __init__(self, text, *args, wrap=120, join_items='ignore kw', to_str=None, **kws):
		if to_str or not isinstance(text, str):
			to_str = to_str or str
			text = to_str(text)
		text = wrap_lines(text, wrap=wrap)
		super().__init__(text, *args, join_items='\n', **kws)
	
def wrap_lines(text, join=None, wrap=120):
	lines = []
	orig_lines = text.split('\n')
	for line in orig_lines:
		while line:
			line_segment = line[:wrap]
			line = line[wrap:]
			lines.append(line_segment)
	if join:
		lines = join.join(lines)
	return lines
