from docopt import docopt,Dict

from pyutils.structures.maps import *

#region opt utils
class DefsOpts:
	class docopt:
		rrx						= RXAt(	 'dash  flag     arg         ',
										r'\-+   [\w\-]+  <\w+>|[A-Z]+', fmt='{}?{}|{}')  # flags are prefixed dash(es)
		rx_word					= r'\w+'
		key_of_opt				= lambda opt,rx_word=r'\w+',j='_': j.join(keep(RX.findall(rx_word,opt))).lower()
		resolve_flag			= lambda flag_dash,flag_key: flag_dash != flag_key and True or flag_dash

DDOpts = DD						= DefsOpts
#endregion

#region opt utils
def groups_of_docopts(opt_at, rrx=DD.docopt.rrx, to_key=DD.docopt.key_of_opt,resolve_flag=DD.docopt.resolve_flag):
	''' simplify docopts output
		1. remove non-word symbols normalizing opt as attribute names
		2. group opts into sub dicts: on-flags, off-flags or args
	'''
	grps = Vars(all=Vars(),on=Vars(),off=Vars(),arg=Vars())
	for opt in opt_at:
		opt_subs = RX.search(rrx.rx,opt,key=dict,fab=Vars)
		k_opt = to_key(opt)
		if k_opt in grps.all: continue  # already handles
		elif opt_subs.arg is not None:
			grps.all[k_opt] = grps.arg[k_opt] = opt_at[opt]
		elif opt_subs.flag is not None:
			flag = (
				resolve_flag(opt_at[opt],opt_at[k_opt])	if opt!=k_opt and opt in opt_at and k_opt in opt_at else  # conflict
				opt_at[opt]  )
			k_grp = DD.docopt.kgrp_at_flag[flag]
			grps.all[k_opt] = grps[k_grp][k_opt] = flag
		else:
			throw(f'Unrecognized opt: {opt!r}  Looking for rx: {rrx.rx!r}')
	return grps
# def opts_of_quiz_cmd(cmd=None,cmd_args=None,doc=None, version='v0.0.0',groups=False,log=False):
# 	# resolve inputs
# 	# cli_args = ()
# 	if cmd_args:
# 		p_py = ''
# 	else:
# 		cmd = cmd or sys.argv
# 		p_py,*cmd_args = isstr(cmd,fab=lss,orput=cmd)
#
# 	# resolve output
# 	cmd_opts = Dict(docopt(doc or DDQZ.Ctls._doc, cmd_args, version=version,help=False), p_py=p_py)
# 	log and log(cmd_opts)
# 	cmd_opts = groups_of_docopts(cmd_opts)
# 	cmd_opts.args = cmd_args
# 	cmd_opts = cmd_opts if groups else cmd_opts.all
# 	return cmd_opts
#endregion
