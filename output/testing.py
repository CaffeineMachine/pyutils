import traceback
from typing import List, Union

from pyutils.output.forms import *
from pyutils.structures.strs import *
from pyutils.output.colorize import *
from pyutils.utilities.oputils import *
from pyutils.structures.maps import *

#region test defs
_NS					= Vars()
class DefsTest:
	#region namespaces
	undefined		= 'undefined'  # todo: eliminate
	class strs:
		kk			=      ne, eq, qual, val,  = (
					  tss('ne  eq  qual  val  ') )
		ops			=      op_ne, op_eq,  = (
					  tss('!=     ==  ') )
		unset		= 'UNSET'
	SS				= strs
	#endregion
	#region colls
	test_states		= (default, running, finished, passed, failed, warning, error) = range(7)
	vv				= ne,    eq,   = (
					  False, True,   )
	colr_at=_NS.c_at= aliasremap(Vars(
						ne		= redl,
						eq		= greenl,
						qual	= white,
						val		= whitel,
						ne_b	= blacklb,
						eq_b	= blackb,
					),keys_in='ne eq',keys_to=vv)
	op_at			= mapvars(SS.kk,SS.ops)
	opc_at			= Vars({k:_NS.c_at[k](op) for k,op in op_at.items()})
	fmt_qual		= Fmt('{} {{op}} {}'.format(*callsarg(iseqmap(colr_at,'qual  val'),'{{}}')))
	fmt_qual_at		= dict({
						ne		: Fmt(colr_at.ne_b(fmt_qual(op=opc_at.ne))),
						eq		: Fmt(colr_at.eq_b(fmt_qual(op=opc_at.eq))),
					})
	#endregion
	...
DDTest = DD			= DefsTest
Defs				= DefsTest
#endregion

#region test utils
name_of				= lambda obj: getattr(obj, '__name__', obj)
name_of_func		= lambda func: getattr(func, '__name__', func.__class__.__name__)

def fmt_qual(is_eq,qual,val,*pa,fmt_qual_at=DD.fmt_qual_at,less=False,log=None,fmt_post=None,**ka):
	fmt_qual = fmt_qual_at[is_eq]
	s_out = fmt_qual(qual,val)
	s_out = s_out if not fmt_post else iscall(fmt_post,orvar='format')(s_out,*pa,**ka)
	log and log(s_out)
	return s_out

def eq_of(qual,val=miss,*pa_qual,on_qual=None,on_ne=None,eq_as_rx=False,multi=False,fab=None,**ka_qual):
	# resolve input
	qual_in = qual
	qual = (
		qual						if iscall(qual) else
		qual.__contains__			if isset(qual) else
		qual.search					if istype(qual,Rxc) else
		re.compile(qual).search		if eq_as_rx else
		eq.__get__(qual) )
	
	# resolve output
	def eq_of_val(val):
		vals = val if multi else [val]
		result = []
		for val in vals:
			is_eq = qual(val)
			on_qual and on_qual(is_eq,qual_in,val,*pa_qual,**ka_qual)
			on_ne and not is_eq and on_ne(val)
			# yield is_eq,val
			result.append((is_eq,val))
		result = result if multi else result[0]
		return result
	result = val is miss and eq_of_val or eq_of_val(val)
	result = result if not fab else fab(result)
	return result
pfmt_qual			= redef(fmt_qual,log=print)

qual_log			= redef(eq_of,on_qual=pfmt_qual)
qual_logerr			= redef(eq_of,on_ne=throw,on_qual=pfmt_qual)
qual_err			= redef(eq_of,on_ne=throw)
qual_warn			= redef(eq_of,on_ne=print)
qual_logs			= redef(eq_of,on_qual=pfmt_qual,multi=True)
qual_logerrs		= redef(eq_of,on_ne=throw,on_qual=pfmt_qual,multi=True)
qual_errs			= redef(eq_of,on_ne=throw,multi=True)
qual_warns			= redef(eq_of,on_ne=print,multi=True)
#endregion

#region test devcs
class Params:
	def __init__(self, *args, **kws):
		self.cache_args = args
		self.cache_kws = kws
	def invoke(self, func:callable):
		result = func(*self.cache_args, **self.cache_kws)
		return result
	__call__		= invoke
	
class IArgs(Params):
	def invoke(self, func:callable):
		for args, kws in self:
			result = func(*self.cache_args, **self.cache_kws)
			yield result
class ZipIArgs(IArgs):
	def __iter__(self):
		for ind, args in enumerate(zip(self.cache_args)):
			kws = iitems(self.cache_kws, lambda key, values: [key, values[ind]])
			yield args, kws
class PermuteIArgs(IArgs):
	def __iter__(self):
		args, kws = [], {}
		for ind, arg in enumerate(zip(self.cache_args)):
			kws = iitems(self.cache_kws, lambda key, values: [key, values[ind]])
			yield args, kws
class DemuxIArgs(IArgs):
	def __init__(self, params_seq, columns=None):
		self.params_seq = params_seq
		self.columns = columns or []
	def __iter__(self):
		args, kws = [], {}
		for params in self.params_seq:
			args, kws = list(params), {}
			for key in reversed(self.columns):
				kws[key] = args.pop(-1)
			yield args, kws

class TestRun:
	call_arg0 = lambda arg0_func, *args, **kws: arg0_func(*args, **kws)
	_head_tail = (10, 5)
	
	def __init__(self, largs, iargs, rargs,
			name_fmt:str=None, item_fmt:Union[str,int]=None, func:callable=None,
			head_tail=_head_tail, proc_rawout=None, **kargs):
		'''
			:param largs:
			:param iargs:
			:param rargs:
			:param kargs:
			:param name_fmt:
			:param item_fmt:
			:param func:
			:param head_tail:
		'''
		self.args = list(chain(func and [func] or [], largs, iargs, rargs))
		self.func = func or self.args.pop(0)
		self.kargs = kargs
		self.head_tail = head_tail
		self.raw_output = None
		self.proc_rawout = proc_rawout
		self.output = ''
		self.status = Defs.default
		self.name = self.default_name(name_fmt, item_fmt, max(0, len(largs)-bool(not func)), len(rargs), kargs.keys())
	def __len__(self):	return 2
	def __iter__(self):	return iter([whitel(str(self)), (blue if self else red)(self.output)])
	def __str__(self):	return self.name
	def __bool__(self):	return self.status in (Defs.finished, Defs.passed, Defs.warning)
	def default_name(self, name_fmt:str=None, item_fmt:Union[str,int]=None, n_largs=0, n_rargs=0, keys=None):
		func_name = name_of_func(self.func or self.args[0])  #.replace('{', '{{').replace('}', '}}')
		args = self.args[n_largs:-n_rargs or None]
		item_fmt = item_fmt or 30
		name_fmt = name_fmt or '{} ' + SJ.j_tuple(
			[f'largs{ind}' for ind in range(n_largs)] +
			[f'{{!r:.{item_fmt}}}' if isinstance(item_fmt, int) else item_fmt] * len(args) +
			[f'rargs{ind}' for ind in range(n_rargs)])
		result = name_fmt.format(func_name, *args)
		return result
	def run(self, test_data=None):
		func = self.func or self.__class__.call_arg0
		self.status = Defs.running
		try:
			self.raw_output = func(*self.args, **self.kargs)
			self.raw_output = self.proc_rawout(self.raw_output) if self.proc_rawout else self.raw_output
			self.status = Defs.finished
		except Exception as e:
			self.raw_output = red(traceback.format_exc())
			self.status = Defs.failed
			
		self.output = str(LessForm(str(self.raw_output), self.head_tail))
		return self

class _AttrList:
	def __init__(self, obj, attr):
		self.obj = obj
		self.attr = attr
	def __iter__(self):
		return iter((getattr(item, self.attr) for item in self.obj))
	def __getitem__(self, index):
		subset = self.obj[index]
		if isinstance(index, slice):
			result = [getattr(item, self.attr) for item in subset]
		else:
			result = getattr(subset, self.attr)
		return result

class TestRuns(list):
	def __init__(self, largs:tuple=None, iargs:List[list]=None, rargs:tuple=None, *,
			func:callable=None, name:str=None, head_tail=(10, 5), 
			names_fmt:str=None, items_fmt:str=None, **kargs):
		'''	TestRuns(name, [largs:tuple], iargs, [rargs:tuple, [func:callable, ... [, **kwargs]]])
		
			:param name: name of test collection shown as header
			:param largs: (optional) tuple with leading (left-hand) args unchanged for all test.
				assigned as iargs for non-tuple inputs
			:param iargs: list or Iargs() containing args entries defining each test
			:param rargs: (optional) tuple with static trailing (right-hand) args unchanged for all test
			:param func: func to run unchanged for all test. if omitted the 1st positional arg serves as func.
				computed as (largs + iargs[N] + rargs)[0]
			:param kwargs: unrecognized kws are passed unchanged for all test
		'''
		# interpret largs as omitted when it is not a tuple; shift right positional largs & iargs into iargs & rargs
		# if not isinstance(name, str):
		# 	rargs = iargs
		# 	iargs = largs
		# 	largs = name
		if not isinstance(largs, tuple) and isinstance(iargs, (tuple, type(None))) and rargs is None:
			rargs = iargs or ()
			iargs = largs
			largs = ()
		self.largs = largs or ()
		self.iargs = iargs
		self.rargs = rargs or ()
		self.kargs = kargs
		self.func = func
		self.head_tail = head_tail
		self.names_fmt = names_fmt
		self.items_fmt = items_fmt
		self.data_fmt = self.format_data()
		self.name = '\n  '.join((name is not None and [name] or []) + [self.format_data()])
		assert isinstance(self.largs, tuple) and isinstance(self.rargs, tuple), (
			f'largs and rargs must be tuples. Got {self.largs} & {self.rargs}')
		super().__init__()
	test_runs	= property(lambda self: self)
	def __getattr__(self, attr):
		''' permits access in the format test_runs.states[:5] '''
		if attr in Strs('names raw_outputs outputs states'):
			result = self.__dict__.setdefault(attr, _AttrList(self, attr.rstrip('s')))
		else:
			result = super().__getattr__(attr)
		return result
	def format_data(self, ):
		first_arg_ind = 0 if self.func else 1
		largs = [[f' larg{ind}', repr(arg)] for ind, arg in enumerate(self.largs[first_arg_ind:])]
		rargs = [[f' rarg{ind}', repr(arg)] for ind, arg in enumerate(self.rargs)]
		kargs = [[f' {key}', repr(arg)] for key, arg in self.kargs.items() if key not in ['proc_rawout']]
		result = f'test_data = {SJ.jj_dict(largs + rargs + kargs)}'
		return result
	def __bool__(self):
		result = bool(len(self)) # and all(self)
		return result
	def set_iargs_zip(self, *args, **kws):
		self.params = ZipIArgs(*args, **kws)
	def set_iargs_demux(self, *args, **kws):
		self.params = DemuxIArgs(*args, **kws)
	def set_iargs_permute(self, *args, **kws):
		self.params = PermuteIArgs(*args, **kws)
	def run_all(self, run_flags=0):
		self.clear()
		if not isinstance(self.iargs, Params):
			self.iargs = DemuxIArgs(self.iargs)
		for args, kargs in self.iargs:
			test = TestRun(
				self.largs, args, self.rargs,
				**self.kargs, **kargs,
				func=self.func,	head_tail=self.head_tail, name_fmt=self.names_fmt, item_fmt=self.items_fmt,  )
			self.append(test)
			test.run()
			if not test:
				break
		return self
	def run(self, *args, **kws):
		test = TestRun(*self.largs, *args, *self.rargs, **self.kwargs, head_tail=self.head_tail, **kws)
		self.test_runs.append(test)
		test.run()
		return test

class TestRunsForm(Form):
	_width = None
	_header = 'Testing: {}'
	
	def __init__(self, *args, join_rows='\n\n', show=None, **kws):
		log and log('TestRunsForm()')
		self.tests = TestRuns(*args, **kws)
		self.tests.run_all()
		
		self.body = ColumnsForm(items=self.tests, width=120, join_items='  =>  ', join_rows=join_rows)
		self.join_rows = Join(join_rows)
		
		items = [self.tests.name, self.body]
		super().__init__(items=items, show=show)
	def __getitem__(self, index):
		return self.tests[index]
	def format(self, items=None):
		log and log('TestRunsForm.format()')
		items = items or self.items
		
		result = self.join_rows(items)
		return result

class TestsForm(Form):
	_width = None
	_header = 'Testing: {}'
	def __init__(self, testing=None, tests=None, header=None, footer='', join_rows='\n\n'):
		log and log('TestsForm()')
		tests = ColumnsForm(items=tests, width=120, join_items='  =>  ', join_rows=join_rows)
		self.join_rows = Join(join_rows)
		
		header = header or self._header.format(testing)
		items = [header, tests] + (footer and [footer] or [])
		super().__init__(items=items)
	def format(self, items=None):
		log and log('TestsForm.format()')
		items = items or self.items
		
		result = self.join_rows(self.items)
		return result
#endregion
