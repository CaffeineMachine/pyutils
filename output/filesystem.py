import os
from filelock import FileLock
from watchdog.events import RegexMatchingEventHandler
from watchdog.observers import Observer

from pyutils.defs.colldefs import xlist
from pyutils.defs.rsrc import p_loc_pykit
from pyutils.utilities.alyzutils import fq_kfuncpln
from pyutils.utilities.callutils import *
from pyutils.utilities.clsutils import *
from pyutils.utilities.iterutils import *
from pyutils.utilities.maputils import *
from pyutils.utilities.pathutils import *
from pyutils.patterns.event import Publisher
from pyutils.patterns.props import *
from pyutils.output.logs import *
from pyutils.structures.strs import *
from pyutils.time.timedefs import *

#region resource defs
try:
	import fcntl
except ImportError:
	fcntl = None

@clscolls(no)
class DefsResource:
	#region namespaces
	class Strs:
		sz_sig				= 12
		ch_header = ch_hdr	= '#'
	
	SS						= Strs
	#endregion
	#region colls
	defs_row				= lazy(lambda _: dict(
		encode				= sjoin_rows,
		decode				= rows_of_str,
		kk_store			= Rows.kk_store, ))
	#endregion
	...

DDRes = _D					= DefsResource
SSRes = _S					= DDRes.SS
_i,_map,_zip,_afx,_sfx		= _D.i, *collectors([map,zip,afx,sfx],_D.i)
#endregion

#region row utils
_D.fmts_at					= Obj(
	seq						= (_fmt_seq:='{{0[{n}]:{fm}}}'.format),
	map						= (_fmt_map:='{{0[{k}]:{fm}}}'.format),
	obj						= (_fmt_obj:='{{0.{k}:{fm}}}'.format),
)|{no:_fmt_seq,yes:_fmt_map}
def ref_row(row=None, kk=None,kkss=None,j_row=tuple, **ka):
	''' get ->k_track key of given *row* with *trr_vars* given as ** '''
	if kk is None:
		assert kkss
		kk,_ = kkss_of(kkss)
	k_track = isdict(row) and iseqmap(row,kk) or row[:len(kk)]
	k_track = k_track if j_row is None else (isstr(j_row) and j_row.join or j_row)(k_track)
	return k_track
def sign_row(name, size=_S.sz_sig, **ka):
	''' get ->sig_track signature of given *row* with *trr_vars* given as ** '''
	sig = name[-size:].ljust(size,'_')
	return sig

def rep_row(row=None,j_row=',',kk=None,**_):
	''' get ->str repr of given *row* and *trr_vars* given as ** '''
	ss_row = row and seqmap(row, kk or row._kk) or kk
	s_row = j_row.join(ss_row)
	return s_row
def rep_rows(rows,j_row=',',j_rows='\n',kk_row=None,kkss=None,hdr=on,fmt_attrs='seq',fm='',fm_at=map0,**_):
	''' get ->str repr of given *rows* with *trr_vars* given as **
		:param rows:   input rows to be joined. each row must be a coll. (a coll has __getitem__() ie: list or dict)
		:param j_row:  str to join values in a row. default is ','
		:param j_rows: str to join rows. default is '\n'
		:param kk_row: ordered keys of each value to show for each row
		:param kkss:   names of keys & vals. can fill in *kk_row*
		:param hdr:    enable inclusion of header before rows. default is True
		:param fmt:    formatter to apply to all vals in row
		:param fm:     default string form specifier
		:param fm_at:  mapped string form specifier per key
	'''
	# resolve inputs
	j_row,j_rows = isstr(j_row,var='join'), isstr(j_rows,var='join')
	kk_row = kk_row and tss(kk_row) or kkss and tj(kkss_of(kkss)) or throw(f'got neither of requisite args {kk_row=}, {kkss=}')
	fmt = _D.fmts_at.get(fmt_attrs,fmt_attrs)
	fmt_row = Fmt(j_row(fmt if isntcall(fmt) else (fmt(n=n,k=k,fm=fm_at.get(k,fm)) for n,k in iidx(kk_row))))
	
	# resolve output
	ss_rows = _map(fmt_row, (irow :=_i(rows)))
	ss_rows = _afx(hdr and _S.ch_hdr+j_row(kk_row) or (), ss_rows, '')  # '' adds newline at end
	s_rows = j_rows(ss_rows)
	return s_rows

def scan_rows(s_rows,j_rows='\n',j_row=',',kk_row=True,kkss=None,hdr=_S.ch_hdr,pad=no,fabs=no,fab_rows=None,**ka_rows):
	''' get ->rows for given str *s_rows* with *ka_rows* given as kwargs
		:param s_rows:   input rows to be joined. each row must be a coll. (a coll has __getitem__() ie: list or dict)
		:param j_rows:   str to join values in a row. default is ','
		:param j_row:    str to join rows. default is '\n'
		:param kk_row:   ordered keys of each value to show for each row
		:param kkss:     names of keys & vals. can fill in *kk_row*
		:param hdr:      enable inclusion of header beforw rows. default is True
		:param pad:      ...
		:param fabs:     ...
		:param fab_rows: ...
	'''
	# resolve inputs
	if (istype(s_rows,P) or (isstr(s_rows) and (len(s_rows) < 1000)) and P(s_rows).is_file()):
		s_rows = P(s_rows).read_text()
	kk_row = (
		tss(kk_row)			if isiter(kk_row_in:=kk_row) else
		kkss and kkss_of(kkss,fab=tj) )
	fab_rows = fab_rows is None and ka_rows.get('fab') or fab_rows
	
	# extract header:  => kk_row
	has_hdr,s_hdr = isstr(hdr) and (shas([hdr]),hdr) or (hdr,'')
	ss_rows = s_rows.split(j_rows)												# split s_rows into ss_rows
	while ss_rows and not (s_row:=ss_rows[0]) or (has_hdr and has_hdr(s_row)):
		kk_row = kk_row_in and s_row.strip(s_hdr or j_row).split(j_row)
		ss_rows.pop(0)
	
	# resolve output:  => rows
	rows = _i(s_row.split(j_row) for s_row in ss_rows if s_row)					# split each s_row into ss_row
	rows = rows if not pad else map(tuple, map(str.rstrip, rows))
	rows = rows if not kk_row else (mapeach(kk_row,ss_row) for ss_row in rows)	# given kk_row map each k_row->s_row
	rows = rows if not fabs else _map(fabs,rows)								# given fabs convert each row
	rows = rows if not fab_rows else fab_rows(rows)								# given fab_rows convert rows
	return rows

def scan_rows_cascaded(*pa,fab=tuple,**ka):
	rows = scan_rows(*pa,**ka)
	rows = cascade_vvs(rows,fab=fab)
	return rows

k_of_vv,sig_of_vv			= ref_row,sign_row
sj_vv,sj_ivv				= rep_row,rep_rows
ivv_of_s					= scan_rows
sjoin_row					= rep_row
sjoin_rows					= rep_rows
rows_of_str					= scan_rows
#endregion

#region data devcs
class Store:
	ka_store: dict			= None
	kk_store				= None
	ka_store_new			= property(lambda self: {k:getattr(self,k,None) for k in self.kk_store})
	def __init_subclass__(cls, encode=None, decode=None, **ka):
		encode and setattr(cls,'encode', staticfunc(encode))
		decode and setattr(cls,'decode', staticfunc(decode))
		infuse(cls,ka)
	def __init__(self,*pa,**ka):
		annoinit(self,*pa,**ka)
		self.ka_store = self.ka_store if isdict(self.ka_store) else self.ka_store_new
		...
	def decode(self,data):		return data
	def encode(self,stored):	return stored
	def load(self):				raise NotImplementedError()
	def save(self):				raise NotImplementedError()
	
class FileStore(Store):
	path: P					= None
	p_of					= lambda key,fmt=fmt_of('{}/{}.csv'),dir=p_loc_pykit: P(fmt(dir,nameof(key).lower()))
	def __init_subclass__(cls, p_of=None, **ka):
		super().__init_subclass__(**ka)
		cls.p_of = staticfunc(p_of or getattr(cls,'p_of',None) or throw(AttributeError('p_of')))
	def __init__(self,*pa,**ka):
		# annoinit(self,*pa,**ka)
		super().__init__(*pa,**ka)
		p_of = self.p_of
		self.path = P(self.path or p_of(self))
		...
	def load(self, path=None, **ka):
		path = path or self.path
		stored = None
		if okay_p(path):
			data = path.read_text()
			stored = self.decode(data, **dict(self.ka_store,**ka))
		return stored
	def save(self, stored=None, trial=2, path=None, **ka):
		path = path or self.path
		data = self.encode(stored or iter(self), **dict(self.ka_store,**ka))
		trial >= 1 and print('',fq_kfuncpln(),'WRITE -> %s'%path,data,sep='\n')
		trial != 1 and path.write_text(data)
		return data

class Row(xlist):
	#region	defs
	vv						= property()
	#endregion
	#region	form
	def __init_subclass__(cls, kk=None, fmt=None, **ka):
		assert kk is not None
		cls.kkss = cls.ssk,cls.ssv = kkss(kk)
		cls._n_k = len(tj(cls.kkss))
		cls.fmt = fmtr(fmt)
		super().__init_subclass__(kk=cls.ssk+cls.ssv)
		...
	def __init__(self,*pa,**ka):
		super().__init__(*pa,**ka)
		self._hash = hash(tuple(self[:len(self.ssk)]))
	#endregion
	#region	utils
	def __hash__(self):		return self._hash
	def __eq__(self,other):	return istype(other, self.__class__) and self._hash == other._hash
	def __repr__(self):		return self.fmt(self)
	#endregion
	...

class Rows:
	ivv: list				= None
	fabs					= Row
	kk_store				= S*'j_rows  j_row  kk_row  kkss  hdr  pad  fabs  fab'  # todo: maybe just detect from func args
	def __init__(self,*pa,**ka):
		annoinit(self,*pa,**ka)
		self.did_sort = True
		super().__init__(*pa,**ka)
	def __iter__(self):
		not self.did_sort and self.codes.sort()
		self.did_sort = True
		return iter(self.codes)
	def update(self, vals):		self.new.extend(vals)
	def add(self, val):			self.new.append(val)
#endregion

#region path devcs
class PathWatcher(RegexMatchingEventHandler,Publisher):
	def __init__(self, path, *_cbs, cbs=(), rxs=[r'.*\.py$'], recursive=False, start=True, kk_into=(),args=None,backoff=.2,**ka):
		self.worker = Observer()
		self.path = Path(path)
		self.recursive = recursive
		self.backoff = backoff
		self.convert_args = convert_args_into(*kk_into,args=args)
		self.t_at_evt = {}
		# if self.path.is_file():
		# 	self.path,rxs = path.parent,[f'.*{path.name}$']
			
		RegexMatchingEventHandler.__init__(self,**ka)
		Publisher.__init__(self)
		
		# setup events handler
		self.worker.schedule(self, str(self.path), recursive=self.recursive)
		cbs = cbs or _cbs
		cbs and self.hook(*collany(cbs))
		start and self.start()
		start and iscall(start) and start(f'Started monitoring: {self.path}')
	
	def start(self):		return self.worker.start()
	def stop(self):			return self.worker.unschedule_all()
	
	def on_modified(self,event):
		if event.src_path != self.path.str: return
		
		t_now,t_back = T.now(), self.t_at_evt.get(event.key,T.min)
		if self.backoff and self.backoff >= (t_now-t_back).total_seconds(): return
		
		# pa,ka = self.convert_args(event.src_path,event)  # todo:
		pa,ka = [event.src_path],{}
		self.notify(*pa,**ka)
		self.t_at_evt[event.key] = t_now

class XFileLock(FileLock):
	file_start_pos, file_seek_pos, file_end_pos = range(3)
	_max_preview 			= 1000
	def __init__(self, *file_path_parts):
		''' args: file_path_parts; ctor('path/to/file') == ctor('path', 'to', 'file') '''
		# todo: allow ([dir0, dir1, ...,] name) args and address improper dir/name detection
		self.dir = ''.join(file_path_parts[:-1])
		self.name = file_path_parts[-1]
		self.mode = 'r'
		self.stream = None
		self.size = len(self)
		self.stat = None
		self.preview = None
		self.lock = None
		FileLock.__init__(self, self.dir+self.name, timeout=0)
	def __len__(self):
		self.size = 0
		if os.path.exists(self.dir + self.name):
			with open(self.dir + self.name, 'r') as self.stream:
				self.stream.seek(0, 2)
				self.size = self.stream.tell()
		return self.size
	def __str__(self):
		''' return cached content preview of file up to _max_preview bytes '''
		stat = os.stat(self.dir+self.name)
		if self.stat == stat: return self.preview  # show cached result until file changes

		self.preview = self[:self._max_preview]
		return self.preview
	def __getitem__(self, *args):
		'''	simplified file read access using python slices convention
			usage:
			with open('test.txt','w') as f:
				f.write('hello\nworld')
			XRFile()[:5] 		# => 'hello'
			XRFile()[-5:] 		# => 'world'
			XRFile()[-100:] 	# => 'hello\nworld'
			XRFile()[100]		# => ''
		'''
		# handle exit conditions; coerce indices to start & stop
		if not os.path.exists(self.dir + self.name):
			raise OSError('File not found.')
		elif isinstance(args[0], slice):
			start, stop = args[0].start, args[0].stop
		elif isinstance(args[0], int):
			start, stop = args[0], args[0]
		else:
			raise TypeError(arg0=args[0], expected=[slice,int])
		
		# open and read the given slice portion of file
		result = ''
		with open(self.dir + self.name, self.mode) as self.stream:
			self.stream.seek(0, 2)
			self.size = self.stream.tell()
			# print('start, stop, size = %s' % [start, stop, self.size])
			start = start  if (start is not None)  else 0
			start = start  if (0 <= start)         else max(0, self.size+start)
			stop = stop    if (stop is not None)   else self.size
			stop = stop    if (0 <= stop)          else min(self.size, stop - start)
			read_count = min(self.size, stop - start)
			# print('start, stop, read_count = %s' % [start, stop, read_count])
			self.stream.seek(start, XFile.file_start_pos)
			result = self.stream.read(read_count)
		
		return result
	def test_locked(self):
		try:
			with self:
				locked = False
		except:
			locked = True

		return locked
	def _acquire(self):
		''' acquire lock on file; reimplmented using FileLock source without file truncation '''
		open_mode = os.O_RDWR | os.O_CREAT
		fd = os.open(self._lock_file, open_mode)

		try:
			fcntl.flock(fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
		except (IOError, OSError):
			os.close(fd)
		else:
			self._lock_file_fd = fd
		return None
	# method aliases
	__repr__ = __str__

class XFileR(XFileLock): pass

class XFileRW(XFileR):
	def __setitem__(self, key, value): pass
	def write(self, content):
		with open(self.dir + self.name, 'w') as self.stream:
			result = self.stream.write(content)

		return result

PWatcher					= PathWatcher
XFile						= XFileRW
#endregion

#region inline test
if __name__=='__main__':
	# Usages:
	# tested & working
	with open('test.txt','w') as f:
		f.write('hello\nworld')
	f = XFile('test.txt')
	print(f)

	print('len(f) = \'%s\'' % len(f))		# => 11
	print('f[:5] = \'%s\'' % f[:5]) 		# => 'hello'
	print('f[-5:] = \'%s\'' % f[-5:]) 		# => 'world'
	print('f[:] = \'%s\'' % f[:]) 			# => 'hello\nworld'
	print('f[-100:] = \'%s\'' % f[-100:]) 	# => 'hello\nworld'
	print('f[100] = \'%s\'' % f[100]) 		# => 'hello\nworld'
#endregion