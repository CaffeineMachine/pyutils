import sys
import atexit
import traceback
from dataclasses import dataclass

from pyutils.utilities.alyzutils import fframe, bump_fqka, fp_pln_warn
from pyutils.utilities.callutils import cxpas
from pyutils.utilities.iterutils import *
from pyutils.utilities.maputils import *
from pyutils.output.colorize import redl

#region logs defs
class DefsLogs:
	into_icoll				= tss('iter'), tss('i')
	logr_at					= {}
	to_logr					= None
	
DDLog = DD					= DefsLogs
#endregion

#region basic forms  # todo: relo
def kaobj_of_call(call,obj=miss,to_key=None):
	if obj is miss:		return kaobj_of_call.__get__(call)
	
	to_key = isstr(to_key,var='format')
	kk_args = call.__code__.co_varnames if not to_key else map(to_key,call.__code__.co_varnames)  # todo: replace with fargkeys
	# ka = {k:getattr(obj,k) for k in kk_args if hasattr(obj,k)}
	ka = dict(keep(((k,getattr(obj,k,miss)) for k in kk_args), miss.__ne__, at=1))
	return ka

def icolls_of(collect=on,iters=(iter,),**ka):
	'''
		:param collect:
		:param iters:
	'''
	icoll = collect and list or iter
	icolls = iters and collectors(iters,icoll)  # colls = [map,zip,affix,suffix]
	return icolls
def icollat_of(*pa,into=DD.into_icoll,alias=True,nameof='{.__qualname__}'.format,**ka):
	icolls = icolls_of(*pa,**ka)
	icoll_at = mapseq(icolls,to_key=nameof)
	icoll_at = icoll_at if not into else intoremap(icoll_at,kk_into=into,alias=alias)
	return icoll_at

kaobj_of_cx					= kaobj_of_call
#endregion

#region mode forms
def iters_of_dbg(dbg): raise NotImplementedError()
def logrs_of_nnlvl(nn_lvl=1, n_lvls=5, logr=print, logr_of=None, **ka):
	''' create *n_lvls* total ->*logr*s with each [de]activated according to given *nn_lvl* loglevels
		:param nn_lvl: verbosity level. activates logger lvls <= nn_lvl; usually 0=quiet 1=notice 2=info 3=debug 4=trace
		:param n_lvls: total loggers to generate
		:param logr: default logger for all loglevels
		:param logr_of: assigned loggers to each loglevel
	'''
	# resolve inputs
	nn_lvl = (
		nn_lvl							if istype(nn_lvl, (range,set)) else
		range(nn_lvl[0],nn_lvl[1]+1)	if isiter(nn_lvl) else
		range(nn_lvl+1)					if isint(nn_lvl) else
		
		throw(ValueError(f'expected nn_lvl as int|list|tuple|set|range of ints: Got nn_lvl={nn_lvl}({type(nn_lvl)})')) )
	n_lvls = max(n_lvls,*nn_lvl)
	logrs = logr_of and cxpas(logr_of,range(n_lvls)) or [logr]*n_lvls
	
	# resolve output
	logr_ns = [logr_n if n in nn_lvl else None for n,logr_n in iidx(logrs,start=1)]
	return logr_ns
#endregion

#region mode adorn utils
_adorn_obj_types			= type,tuple,list
_has_arg0					= lambda types,*pa,: pa and istype(pa[0],types) # todo: relo
_has_arg0_adorn				= _has_arg0.__get__(_adorn_obj_types)

def adorn_colls(*pa, objs=None, collect=on, log=on,fmt_attr='{}',fmt_name='_{}',add_var=on,local_vars=None,**ka):
	local_vars = local_vars or (add_var and bump_fqka(fquery=fframe).f_locals) or None
	if pa:
		objs,collect,*_ = _has_arg0_adorn(*pa) and (*pa,collect) or (objs,*pa,collect)
	icolls_at = icollat_of(collect,**ka)
	
	# resolve call or call result
	def call_post(objs):
		objs = collany(objs)
		s_objs = ' '.join(tss(objs, nameof))
		collect and log and istrue(log,put=fp_pln_warn)('collect iters in <{}>'.format(s_objs),frame_ofs=3)
		
		objs = obj_out,*_ = objs + tkeep([local_vars])
		for obj in objs:
			set,fmt = isdict(obj,put=(dict.__setitem__,fmt_name),orput=(setattr,fmt_attr))
			for key,icoll in icolls_at.items():
				set(obj,fmt.format(key),icoll)
		return obj_out
	stage = objs is None and call_post or call_post(objs)
	return stage

def adorn_logrs(objs=None, attrs='log  l', std_attrs=True, fmt='{attr}{lvl}', **_ka):
	''' on each in *objs* apply *n_lvls* total ->*logr*s with each [de]activated according to given *nn_lvl* loglevels
		:param objs:
		:param attrs:
		:param std_attrs:
		:param fmt:
		:param nn_lvl: verbosity level. activates logger lvls <= nn_lvl
		:param n_lvls: total loggers to generate
		:param logr: default logger for all loglevels
	'''
	def adorn_logrs_of_objs(objs):
		objs = obj_out,*_ = collany(objs)
		for obj in objs:
			# resolve args for logrs_of_nnlvl()
			ka_logr = DD.to_logr and dict(logr=DD.to_logr(obj,**bump_fqka(_ka,2,other=on))) or map0
			ka_obj = kaobj_of_cx(logrs_of_nnlvl, obj, to_key='_{}')  # use class mbr as arg if formatted name is present
			ka = dict(ka_logr,**ka_obj,**_ka)
			
			# resolve logrs
			logrs = logrs_of_nnlvl(**ka)
			log,trace = logrs[0],logrs[-1]
			
			for attr in ss(attrs):
				nth_log = 0
				for lvl,logr in iidx(logrs,start=1):
					setattr(obj,fmt.format(attr,lvl,attr=attr,lvl=lvl),logr)
					nth_log = logr and lvl or nth_log
				setattr(obj,'nth_log',nth_log)
			if std_attrs:
				obj.l,obj.log,obj.trace = log,log,trace
			
			# notify
		return obj_out
	# give call or call result
	stage = objs is None and adorn_logrs_of_objs or adorn_logrs_of_objs(objs)
	return stage

def adorn_modes(objs=None,**ka):
	# todo
	def call_post(objs):
		objs = obj_out,*_ = collany(objs)
		objs_out = adorn_logrs(objs,**ka)
		objs_out = adorn_colls(objs_out,**ka)
		return obj_out
	# give call or call result
	stage = objs is None and call_post or call_post(objs)
	return stage

clslogrs,clscolls,clsmodes	= adorn_logrs,adorn_colls,adorn_modes
clslogs						= clslogrs  # todo: make default
#endregion

#region log utils
def store_stdout(*pa,**ka):		return StoreLogger(*pa,**ka,hook=True)

def set_logr(obj=None, logrs=None, attrs='log  l', std_attrs=True, fmt='{attr}{lvl}', **_ka):
	def adorns_of_obj(obj):
		log,trace = logrs[0],logrs[-1]
		for attr in ss(attrs):
			for lvl,logr in iidx(logrs,start=1):
				setattr(obj,fmt.format(attr=attr,lvl=lvl),logr)
			if std_attrs:
				obj.l,obj.log,obj.trace = log,log,trace
		return obj
	stage = obj is None and adorns_of_obj or adorns_of_obj(obj)
	return stage
#endregion

#region log devcs
@dataclass
class SparceLogger:
	mask: any			= every_nth(5)
	to_str: any			= None
	logger: any			= print
	show_after:bool		= False
	#region utils
	def __post_init__(self):
		self.mask = iter(isint(self.mask, fab=every_nth))
		self.next()
	def next(self):
		self.show = next(self.mask, self.show_after)
		return self.show
	def log(self, *msg_pa,**msg_ka):
		if self.show:
			self.logger(*msg_pa,**msg_ka)
		self.next()
	
	__call__			= log
	#endregion
	...

@dataclass
class StoreLogger:
	''' stores log msgs to buffer. only flushes msg to output when buffer is full.
		
		when program uses very verbose logging replacing print with this logger can
		speed up execution by 4X or more with larger buffer_size
	'''
	buffer_size: int	= 1000
	path_out: str		= None
	hook: bool			= False
	join_msgs: iscall	= '' # '\n'
	#region setup
	def __post_init__(self):
		self.buffer_msg = []
		self.buffer_count = 0
		self.file = None
		self.stdout = None
		self.exc = None
		self.hook and self.capture(self.path_out)
		self.join_msgs = isstr(self.join_msgs) and self.join_msgs.join or self.join_msgs
	def capture(self, path_out=None):
		if not self.file:
			path_out = path_out or self.path_out
			self.file = path_out and open(str(path_out), 'w')
		if not self.stdout:
			self.stdout = sys.stdout
			sys.stdout = self
			sys.excepthook = self._excepthook
			atexit.register(self._release_onexit)
	def release(self, exc=True, unregister=True):
		self.flush(exc=exc)
		if self.stdout:
			sys.stdout = self.stdout
			self.stdout = None
			unregister and atexit.unregister(self._release_onexit)
		self.file and self.file.close()
		self.file = None
	def _release_onexit(self):
		self.release(unregister=False)
	def _excepthook(self, *pa):
		self.exc = pa
	#endregion
	#region utils
	def log(self, msg='', force=False):
		if self.buffer_size:
			self.buffer_count = 0 if force else (self.buffer_count+1) % self.buffer_size
			self.buffer_msg.append(msg)
			if self.buffer_count: return
			
			msg = self.join_msgs(self.buffer_msg)
			self.buffer_msg.clear()
		self.stdout and self.stdout.write(msg)
		self.file and self.file.write(msg)
	def log_exc(self, **ka):
		if self.exc is None: return
		err_msg = redl(''.join(traceback.format_exception(*self.exc)))
		self.stdout and self.stdout.write(err_msg)
		self.file and self.file.write(err_msg)
	def flush(self, exc=False):
		self.log(force=True)
		exc and self.log_exc()
		self.stdout and self.stdout.flush()
		self.file and self.file.flush()
	
	__call__			= log
	write,write_exc		= log,log_exc
	#endregion
	...

@dataclass
class LogSet:
	log: iscall			= None
	l: iscall			= None

SparceLog				= SparceLogger
#endregion
